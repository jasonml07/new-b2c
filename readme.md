# TravelRez - B2C

* Connects to B2B system
* Books tour


#### Tour Development v2

* cd to `react`
* Install package with `npm install` or `yarn`
* Run `yarn serve` to start development server
* Build with development mode `yarn develop`
* Build with production mode `yarn build`



#### Server Deployment

* Run `ssh -i <keys> <username>@<domain>`
* Inside servers terminal run:
  * `eval $(ssh-agent)`
  * `ssh-add ~/<keys>`
* cd `/opt/apps/new-b2b`
* `git fetch origin`
* `git checkout tags/<tag id> -b release/<tag id>`


