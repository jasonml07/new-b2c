const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fileExists = require('file-exists');
const each = require('foreach');
const path = require('path');
const mergeArray = require('merge-array');


const ExtractNormalCSS = new ExtractTextPlugin('./compass/stylesheets/external-tours.css');
const ExtractThemeCSS = new ExtractTextPlugin('./compass/stylesheets/theme.css');
const ExtractTheme2CSS = new ExtractTextPlugin('./theme/theme-2.css');
const ExtractTheme9CSS = new ExtractTextPlugin('./theme/theme-9.css');

const templateDestination = path.resolve(__dirname, '../../../application/views/externalTours');
const projectDir = path.resolve(__dirname, '../../../' );

var exports = {
	entry: './app.js',
	watchOptions: {
        poll: true
    },
	output: {
		filename: './js/external-tours.js',
		path: projectDir + '/static'
	},
	externals: {
		'config': `{
			PRODUCTION: true
		}`,
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['es2015'],
						}
					}
				]
			},
			{
				test: /\.scss$/,
				include: path.resolve(__dirname, 'css/external-tours.scss'),
				use: ExtractNormalCSS.extract({
					fallback: 'style-loader',
					use: ['css-loader', 'sass-loader'],
				})
			},
			{
				test: /\.scss$/,
				include: path.resolve(__dirname, 'css/theme.scss'),
				use: ExtractThemeCSS.extract({
					fallback: 'style-loader',
					use: ['css-loader', 'sass-loader'],
				})
			},
			{
				test: /\.scss$/,
				include: path.resolve(__dirname, 'css/theme-2.scss'),
				use: ExtractTheme2CSS.extract({
					fallback: 'style-loader',
					use: ['css-loader', 'sass-loader'],
				})
			},
			{
				test: /\.scss$/,
				include: path.resolve(__dirname, 'css/theme-9.scss'),
				use: ExtractTheme9CSS.extract({
					fallback: 'style-loader',
					use: ['css-loader', 'sass-loader'],
				})
			},
			{
				test: /\.pug$/,
				include: path.resolve(__dirname, 'templates'),
				use: ['html-loader',{
					loader:'pug-html-loader',
					options:{
						pretty:true
					}
				}] // use pug-html-loader?pretty to format html
			},
		]
	},
	plugins: [
		ExtractNormalCSS,
		ExtractThemeCSS,
		ExtractTheme2CSS,
		ExtractTheme9CSS,
		// new HtmlWebpackPlugin({
		// 	inject: false,
		// 	filetype: 'pug',
		// 	filename: 'dist/template/index.html',
		// 	template: 'templates/body.pug'
		// })
	],
};

var templateList = [
	'body',
	{
		'review': [
			'index'
		],
		'template': [
			'body-checkout-errors',
			'body-checkout-items',
			'body-item-selections',
			'body-preview-selection',
			'body-products-footer',
			'header-shopping-cart',
			'body-item-selection-prices',
			'body-selection',
			'airline-used',
		]
	}
];

var templateDirs = [];

function loadTemplates ( templates, dir ) {
	var rootDir = 'templates/';
	var dir = dir || false;

	each(templates, function (value, key) {

		if ( typeof value == 'object' ) {
			each(value, function (subValue, subKey) {
				loadTemplates( subValue, (!!dir ? dir + '/' + subKey : subKey));
			});
		} else {
			var filePath = (!!dir) ? dir + '/' + value : value;
			templateDirs.push(new HtmlWebpackPlugin({
				inject: false,
				filetype: 'pug',
				filename: templateDestination + '/' + filePath + '.php',
				template: rootDir + filePath + '.pug'
			}));
			/*fileExists(rootDir + filePath + '.pug', function ( err, exists ) {
				if ( !exists )
				{
					console.error('File does not exists:: ' + filePath);
				} else {
					templateDirs.push(new HtmlWebpackPlugin({
						filetype: 'pug',
						filename: './dist/' + filePath,
						template: rootDir + filePath
					}));
				}
			});*/

		}
	});
}

loadTemplates( templateList );
// console.log( templateDirs);
mergeArray(exports.plugins, templateDirs);

// console.log( exports.plugins );

module.exports = exports;