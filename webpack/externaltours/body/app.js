require('./css/external-tours.scss');
require('./css/theme.scss');
require('./css/theme-2.scss');
require('./css/theme-9.scss');

import { SHOPPING_CART } from './js/shopping-cart.js';
import { MODALS } from './js/modals.js';
import { ITEM_SELECTION } from './js/item-selection.js';
import { _PRODUCT } from './js/products.js';
import { LIST_NAVIGATION } from './js/global.js';

import { GRID_LIST } from './js/grid-list.js';
import { LIST } from './js/list.js';
import { CALENDAR } from './js/calendar.js';
import { HELPER } from './js/helper.js'
// console.log('app.js',JSON.parse(JSON.stringify(PRODUCTS))); 
_PRODUCT.init( PRODUCTS );

GRID_LIST.init(  );
LIST.init();
CALENDAR.init();

ITEM_SELECTION.init();
MODALS.init();
SHOPPING_CART.init();

if(DATE){
	const split=DATE.split('-')
	// console.log(DATE, new Date(split[2],parseInt(split[0])-1,split[1]))

	// console.log($('[calendar-container]').datepicker('getDate'))
	$('[calendar-container]').datepicker('setDate',new Date(split[2],parseInt(split[0])-1,split[1]))
	// mm/dd/yyyy
	let $ele=$('[calendar-container]').find('[data-month="'+(parseInt(split[0])-1)+'"][data-day="'+parseInt(split[1])+'"][data-year="'+split[2]+'"]')

	if ($ele.length&&$ele.hasClass('calendar-product')) {
		$ele.click()
	}
	// HELPER.setDataSelections(DATE)
}

var ARIAS={
	'product-calendar-view':'Calendar',
	'product-list-view':'List',
	'product-compact-view':'Compact',
	'product-fill-information':'Checkout',
	'product-item-selection':'Cabins',
	// 'product-list':'Availabilities'
}
window.dataLayer = window.dataLayer || [];
function addToDataLayer (code){
	// console.log('code',code)
	window.dataLayer.push({
		'event': 'customPageview',
		'pagePath': window.location.href.replace('externaltours?','externaltours-'+ARIAS[code].toLowerCase()+'?'), // This should send the full url of a page. 
		'pageTitle': 'Euriope Holidays Tours | '+ARIAS[code]
	});
}
(function ($) {
	addToDataLayer('product-calendar-view')
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		// console.log(e.target)
		e.target // newly activated tab
		var targetAria=$(e.target).attr('aria-controls').trim()
		if (targetAria in ARIAS) {
			// console.log(ARIAS[targetAria])
			addToDataLayer(targetAria)
		}
		if (targetAria=='product-list') {
			var $list=$('#list-navigation').find('li.active')

			if ($list.length) {
				targetAria=$($list[0]).find('a').attr('aria-controls').trim()
				addToDataLayer(targetAria)
			}
		}
	})
	$( window ).resize(function () {
			let width = $( this ).width();
			if ( width < 992 )
			{
					// LIST_NAVIGATION.LIST.click();
					CALENDAR.set_month(1)
			} else {
				CALENDAR.set_month(2)
			}
	});
	$( window ).resize();
})(jQuery);

