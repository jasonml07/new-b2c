const $flowNavigation = $( '#flow-navigation' );
const $listNavigation = $( '#list-navigation' );

/**
 * [FLOW_NAVIGATION description]
 * @type {Object}
 */
export var FLOW_NAVIGATION = {
    PRODUCTS                : $flowNavigation.find('[aria-controls="product-list"]'),
    ITEM_SELECTION          : $flowNavigation.find('[aria-controls="product-item-selection"]'),
    PRODUCT_FILL_INFORMATION: $flowNavigation.find('[aria-controls="product-fill-information"]'),
};
/**
 * [FLOW_NAVIGATION description]
 * @type {Object}
 */
export var LIST_NAVIGATION = {
    CALENDAR: $listNavigation.find('[aria-controls="product-calendar-view"]'),
    LIST    : $listNavigation.find('[aria-controls="product-list-view"]'),
    COMPACT : $listNavigation.find('[aria-controls="product-compact-view"]'),
};