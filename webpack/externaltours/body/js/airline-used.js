export var AIRLINE_USED = {
	eleContainer: null,
	MODAL: null,
	eleTemplate: null,

	init: function () {
		this.eleContainer = $('[airline-used]');
		this.MODAL = new BootstrapDialog({
			title: '<strong>Airline used - subject to availability <span class="fa fa-plane"></span></strong>',
		});
		this.eleTemplate = $( '<div></div>' ).load( BASE_URL + '/externaltours/template/airline-used', function () {
			AIRLINE_USED.eleContainer.click(function () {
				console.info('ELEMENT CLICKED');
				AIRLINE_USED.MODAL.setMessage( AIRLINE_USED.eleTemplate );
				AIRLINE_USED.MODAL.open();
			});
		});
	}
};