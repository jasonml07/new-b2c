export var _PRODUCT = {
    PRODUCTS: null,

    init: function ( products ) {
        this.PRODUCTS = products;
    },
    /**
     * [getPackages description]
     * @return {[type]} [description]
     */
    getPackages: function ( key ) {
        return this.packageOrBills(key, 0);
    },
    /**
     * [getExtrabills description]
     * @param  {[type]} key [description]
     * @return {[type]}     [description]
     */
    getExtrabills: function ( key ) {
        return this.packageOrBills(key, 1);
    },
    /**
     * [packageOrBills description]
     * @param  {[type]} key  [description]
     * @param  {[type]} type [description]
     * @return {[type]}      [description]
     */
    packageOrBills: function ( key, type ) {
        var data = [];

        $.each(this.getProduct( key ).product_supplements, function ( key, item ) {
            if ( item.minguest == type )
            {
                data.push( item );
            }
        });

        return data;
    },
    /**
     * [getProduct description]
     * @param  {[type]} key [description]
     * @return {[type]}     [description]
     */
    getProduct: function ( key ) {
        return this.PRODUCTS[ key ];
    },
    /**
     * [getItems description]
     * @param  {[type]} key [description]
     * @return {[type]}     [description]
     */
    getItems: function ( key ) {
        return this.getProduct( key ).supplement_avail_name_data;
    },
    /**
     * [getSupplements description]
     * @param  {[type]} key [description]
     * @return {[type]}     [description]
     */
    getSupplements: function ( key ) {
        return this.getProduct( key ).sub_supplement_avail_name_data;
    },
    /**
     * [getSupplement description]
     * @param  {[type]} productKey [description]
     * @param  {[type]} key        [description]
     * @return {[type]}            [description]
     */
    getSupplement: function ( productKey, key ) {
        return this.getSupplements( productKey )[ key ];
    },
}