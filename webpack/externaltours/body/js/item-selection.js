import { ITEM_CONTAINER } from './item-container.js';
import { PACKAGE_CONTAINER } from './package-container.js';
import { EXTRABILL_CONTAINER } from './extrabills-container.js';
import { HELPER } from './helper.js';
import { FLIGHT_OPTION, FLIGHT_DETAILS } from './flight.js';
import { MODALS } from './modals.js';
import { COUPON } from './coupon.js';
import { AIRLINE_USED } from './airline-used.js';

const $productItemSelection = $( '#product-item-selection' );

export var ITEM_SELECTION = {
    eleContainer: $productItemSelection,
    START_DATE:null,
    END_DATE:null,
    DURATION:0,
    ITEM_CONTAINER: null,
    PACKAGE_CONTAINER: null,
    EXTRABILL_CONTAINER: null,
    OVERALL_SUBTOTAL: null,

    init: function () {
    },
    /**
     * [initializeContainers description]
     * @return {[type]} [description]
     */
    initializeContainers: function () {
        // console.log('initializeContainers')
        ITEM_CONTAINER.eleContainer = this.eleContainer.find('#item-container');
        ITEM_CONTAINER.init();

        PACKAGE_CONTAINER.eleContainer = this.eleContainer.find('#packages-container');
        PACKAGE_CONTAINER.init();

        EXTRABILL_CONTAINER.eleContainer = this.eleContainer.find('#extrabills-container');
        EXTRABILL_CONTAINER.init();

        COUPON.eleContainer = this.eleContainer.find('#coupon-container');
        COUPON.init(this.START_DATE, this.END_DATE, this.DURATION);

        this.OVERALL_SUBTOTAL = this.eleContainer.find('#overall-total');


        FLIGHT_OPTION.INCLUDED = null;
        FLIGHT_DETAILS.READED_TERM = false;
        // console.log(CUSTOM_SETTING);
        AIRLINE_USED.init();

        if (CUSTOM_SETTING) {
          if (CUSTOM_SETTING.DISABLE_FLIGHT) {
            FLIGHT_OPTION.INCLUDED=false;
          }
        }
    },
    /**
     * [load description]
     * @param  {[type]}   data     [description]
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
    load: function ( data, callback ) {
        this.START_DATE=data.tour.SELECTED_DATE
        this.END_DATE=data.tour.END_DATE
        this.DURATION=data.tour.DURATION
        this.eleContainer.load(BASE_URL + 'externaltours/template/body-item-selections', data, callback);
    },
    /**
     * [initializeButton description]
     * @return {[type]} [description]
     */
    initializeButtons: function () {
        this.eleContainer.find('[back-to-list]').click(function () {
            HELPER.productList();
            COUPON.reset();
            COUPON.show();
        });
        this.eleContainer.find('#checkout-items').click(function () {

            // if( FLIGHT_OPTION.INCLUDED == null )
            // {
            //     MODALS.NO_FLIGHT_SELECTED.open();
            //     return false;
            // }
            HELPER.proceedToCheckout();
        });
        this.eleContainer.find('#preview-items').click(function () {
            var data = ITEM_SELECTION.overAllTotal(true);

            if ( ! ('items' in data) || data.items.passengers == 0 )
            {
                MODALS.NO_PASSENGERS.open();
                return false;
            }

            var template = $('<div></div>').load(BASE_URL + 'externaltours/template/body-preview-selection', data, function ( responseText, textStatus ) {
                MODALS.PREVIEW_SELECTIONS.setMessage(template.html());
                MODALS.PREVIEW_SELECTIONS.open();
            });
        });
    },
    /**
     * [overAllTotal description]
     * @return {[type]} [description]
     */
    overAllTotal: function ( dataOnly ) {
        var dataOnly = dataOnly || false;
        var overAllTotal = 0;
        var items = ITEM_CONTAINER.getSubtotal();
        var extrabill = EXTRABILL_CONTAINER.getSubtotal();
        var packages = PACKAGE_CONTAINER.getSubtotal();
        var flight = FLIGHT_DETAILS.getSubtotal();
        var passengers=items.passengers;
        var prices = {
            product: 0,
            flight: 0
        };

        overAllTotal += items.subtotal;
        overAllTotal += extrabill.subtotal;
        overAllTotal += packages.subtotal;

        prices.product = overAllTotal;

        if ( FLIGHT_OPTION.INCLUDED )
        {
            prices.flight = flight.subtotal;
            overAllTotal += flight.subtotal;
        }

        var isOkay = HELPER.isItOkayToLoad( {items: items} );
        
        var TOUR = HELPER.getGlobalTourInfo();
        var tour = {};
        tour.duration = TOUR.DURATION,
        tour.start = $.datepicker.formatDate('dd-mm-yy', TOUR.SELECTED_DATE);
        tour.end = $.datepicker.formatDate('dd-mm-yy', TOUR.END_DATE);
        var data = {
            passengers:passengers,
            tour: tour,
            items: items,
            extrabill: extrabill,
            packages: packages,
            flight: (FLIGHT_OPTION.INCLUDED ? flight : {}),
            hasFlight: FLIGHT_OPTION.INCLUDED,
            includesFlight: FLIGHT_OPTION.PLEASE_INCLUDE,
            isDiscounted: false,
            prices: prices,
            total: overAllTotal,
            IS_AGENT: IS_AGENT,
        };

        if ( !isOkay )
        {
            COUPON.hide();
            COUPON.reset();
        } else {
            COUPON.show();
            if ( COUPON.isDiscounted )
            {
                // if (true) {}
                // data.isDiscounted = true;
                data.discounted = COUPON.getDiscount( data );
                data.isDiscounted=COUPON.isDiscounted;
            }
            if (FLIGHT_OPTION.INCLUDED==false&&COUPON.isDiscounted) {
                // console.log('overall-discounted',data.discounted);
                if (data.discounted.category=='flight') {
                    data.isDiscounted=false;
                    data.discounted={};
                    COUPON.reset();
                    COUPON.show();
                }
            }
        }
        // this.OVERALL_SUBTOTAL.text( $.number(overAllTotal, 2) );
        // console.log('overall-total',overAllTotal);
        // console.log('overall-data',data);
        this.loadPrices( data );
        if ( dataOnly )
        {
            return data;
        }
    },
    /**
     * [loadPrices description]
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    loadPrices: function ( data ) {
        // console.log(data);
        $(this.eleContainer.find('#total-booking-price')).load(BASE_URL + 'externaltours/template/body-item-selection-prices', data, function () {

        });
    }
};