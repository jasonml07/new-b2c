import { LIST_NAVIGATION } from './global.js'
import { HELPER } from './helper.js';

export var CALENDAR = {
	eleContainer: $( LIST_NAVIGATION.CALENDAR.attr('href') ),
	SHOW_LENGTH: true,
	ACTIVE_KEY:null,

	set_month(num){
		this.eleContainer.find('[calendar-container]').datepicker('option','numberOfMonths',num)
	},
	init: function () {
		let lastDate = new Date(PRODUCTS[PRODUCTS.length - 1].fromYear, (PRODUCTS[PRODUCTS.length - 1].fromMonth + 1), 1);
		let tmp_lastDate = new Date(lastDate);
		tmp_lastDate.setMonth(tmp_lastDate.getMonth() + 1);
		lastDate.setDate(tmp_lastDate.getDate() - 1);
		this.eleContainer.find('[calendar-container]')
			.datepicker({
				inline     : true,
				prevText   : 'Previous',
				nextText   : 'Next',
				minDate    : new Date(PRODUCTS[0].fromYear, PRODUCTS[0].fromMonth - 1, 1),
				maxDate    : lastDate,
				defaultDate: [PRODUCTS[0].fromMonth, 1, PRODUCTS[0].fromYear].join('/'),
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				numberOfMonths: 2,
				onSelect: function ( dateText, dp ) {
					let $ele = CALENDAR.eleContainer.find('[data-start="' + dateText.replace(/\//g, "-") + '"]');
					if( $ele.data('start') == undefined ) { CALENDAR.SHOW_LENGTH = false; return false; }
					CALENDAR.SHOW_LENGTH = true;
					$ele.addClass('active');
					CALENDAR.ACTIVE_KEY=$ele.data('key');
					// console.log($ele.data())
					HELPER.setDataSelections( $ele.data() );

				},
				onChangeMonthYear: function () {
					CALENDAR.listProducts();
					CALENDAR.setActive(CALENDAR.ACTIVE_KEY);
					CALENDAR.range();
				},
				afterShow: function ( inst ) {
					CALENDAR.listProducts();
					CALENDAR.setActive(CALENDAR.ACTIVE_KEY);
					CALENDAR.range();
				}
			});
	},
	/**
	 * [listProducts description]
	 * @return {[type]} [description]
	 */
	listProducts: function () {
		$.each(PRODUCTS, function ( key, product ) {
			let $days = CALENDAR.eleContainer.find('td[data-month="' + (product.fromMonth - 1) + '"][data-year="' + product.fromYear + '"]');
			if( $days.length )
			{
				$.each($days, function (dayKey, day) {
					let $day = $( day );
					let dayDate = $day.find('a').text();
					if( dayDate == product.fromDay && $day.attr('data-product-key') == undefined )
					{
						// console.log(product);
						var optionCls='';
						if (product.options.length) {
							optionCls='option-'+product.options.join('-');
						}
						$day.attr('data-key', key);
						$day.attr('data-start', $.datepicker.formatDate('mm-dd-yy', new Date(product.fromYear, (product.fromMonth - 1), product.fromDay)));
						$day.attr('data-end', [product.toMonth, product.toDay, product.toYear].join('-'));
            $day.addClass('calendar-product cursor-pointer');
            $day.find('a').addClass('calendar-day');
            if ($day.find('.calendar-indicator').length==0) {
            	$day.append('<span class="calendar-indicator '+optionCls+'"></span>');
            	$day.append('<div class="calendar-price"><span class="fa fa-usd"></span>' + product.displayPrice + '</div>');
            }
					}
				});
			}
		});
	},
	/**
	 * [setActive description]
	 * @param {[type]} key [description]
	 */
	setActive: function ( key ) {
		this.eleContainer.find('td[data-key]').removeClass('ui-datepicker-current-day');
		this.eleContainer.find('td[data-key]').removeClass('active');
		this.eleContainer.find('td[data-key]').find('.ui-state-active').removeClass('ui-state-active');
		this.eleContainer.find('td[data-key="' + key + '"]').addClass('ui-datepicker-current-day');
		this.eleContainer.find('td[data-key="' + key + '"]').addClass('active');
	},
	/**
     * [range description]
     * @param  {[type]} date     [description]
     * @param  {[type]} duration [description]
     * @return {[type]}          [description]
     */
    range: function (  ) {
    	if( ! CALENDAR.SHOW_LENGTH ) return false;

    	CALENDAR.eleContainer.find('td.calendar-on-range').removeClass('calendar-on-range');

    	let data = HELPER.getGlobalTourInfo();
        if ( data.DURATION > 0 )
        {
            var old      = data.DURATION;
            var old_date = new Date(data.END_DATE);

            while ( old > -1 )
            {
                // console.log( old_date.getMonth(), old_date.getFullYear(), old_date.getDate());
                let $ele = this.eleContainer.find('td[data-month="'+ old_date.getMonth() +'"][data-year="' + old_date.getFullYear() + '"][data-day="' + old_date.getDate() + '"]')

                if( !$ele.hasClass('calendar-product') )
                {
                	$ele.addClass('calendar-on-range');
                }
                old_date.setDate(old_date.getDate() - 1);
                old--;
            }
        }
    }
};