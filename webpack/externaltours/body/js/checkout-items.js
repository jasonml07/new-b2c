import { HELPER } from './helper.js';
import { ITEM_SELECTION } from './item-selection.js';
import { SHOPPING_CART } from './shopping-cart.js';
import { COUPON } from './coupon.js';
import { MODALS } from './modals.js';
require('./stickySidebar.js');

const $checkoutItemInformation = $( '#product-fill-information' );
const CONFIG = require('config');

export var CHECKOUT_ITEMS = {
	eleContainer: $checkoutItemInformation,
	eleVoucher: null,
	elePaymentMethods: null,
	elePassengers: null,
	eleCancelBtn: null,
	eleSelectBtn: null,
	eleBookBtn: null,
	elePayLaterBtn: null,

	PAYMENT_TYPE: {'payment-method-credit-card': 'CREDITCARD', 'payment-method-paypal': 'PAYPAL', 'payment-method-enett': 'E-NETT', 'payment-method-bank-deposit': 'BANKDEPOSIT'},
	PAYABLE_TYPE: ['CREDITCARD', 'PAYPAL', 'E-NETT'],
	PARAMETERS: {},
	RESPONSE: {},
	IS_PAYLATER: false,

	init: function () {
		$('#sidebar').stickySidebar({
			contentSelector: '#content',
			footerAddOnHieghts: 'div.product-details',
			headerAddOnHeights: '.product-header',
			footerThreshold: 50,
			sidebarTopMargin: 0
		});
		this.elePayLaterBtn.click(function () {
			let $this = $( this );
			let $icon = $this.find('.fa');
			let isPayLater = $this.find('#pay-later').prop('checked');

			// console.log('isPayLater', isPayLater);
			CHECKOUT_ITEMS.IS_PAYLATER = isPayLater;
			$icon.removeClass('fa-square-o').removeClass('fa-check-square-o');
			if ( isPayLater )
			{
				$icon.addClass('fa-check-square-o'); 
				CHECKOUT_ITEMS.elePaymentMethods.find('.payment-container .tab-content').waitMe({
					effect : '',
					text : 'You can pay later after your booking.',
					bg : 'rgba(255,255,255,0.7)',
					color : '#000',
					maxSize : '',
					textPos : 'vertical',
					fontSize : '',
					source : ''
				});
			} else {
				$icon.addClass('fa-square-o');
				CHECKOUT_ITEMS.elePaymentMethods.find('.payment-container .tab-content').waitMe('hide');
			}
		});
		this.elePassengers.find('[data-imitate]:not([data-imitate=""])').change(function () {
			var $this = $( this );
			CHECKOUT_ITEMS.eleVoucher.find( '[name="' + $this.data('imitate') + '"]' ).val( $this.val() );
		});
		this.eleCancelBtn.click(function () {
			SHOPPING_CART.show();
			COUPON.reset();
			COUPON.show();
			HELPER.productList();
		});
		this.eleSelectBtn.click(function () {
			SHOPPING_CART.show();
			HELPER.itemSelection();
		});
		this.eleBookBtn.click(function () {
			let data = ITEM_SELECTION.overAllTotal(true);
			// console.log(data);
			let parameters = CHECKOUT_ITEMS.getData( data );

			if ( parameters.isPayable )
			{
				let payments = {'CREDITCARD': CHECKOUT_ITEMS.getCreditCardData, 'PAYPAL': CHECKOUT_ITEMS.getPayPalData, 'E-NETT': CHECKOUT_ITEMS.getENettData};
				if ( parameters.payment_type in payments )
				{
					parameters.payment = payments[ parameters.payment_type ]( data );
				}
			}
			CHECKOUT_ITEMS.wait();

			CHECKOUT_ITEMS.validate( parameters, function ( response ) {
				CHECKOUT_ITEMS.wait(true);

				let extraData = CHECKOUT_ITEMS.getExtraData( data );

				$.each(extraData, function ( key, item ) { parameters[ key ] = item; });
				parameters.total = CHECKOUT_ITEMS.totalPrice( data );
				parameters._discounted_data = data.discounted;
				CHECKOUT_ITEMS.PARAMETERS = parameters;
				CHECKOUT_ITEMS.RESPONSE = response;

				CHECKOUT_ITEMS.loadPreview( parameters );
			});
		});
	},
	/**
	 * [loadPreview description]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	loadPreview: function ( data ) {
		let $template = $( '<div></div>' ).load(BASE_URL + 'externaltours/template/body-preview-selection', data, function () {
			CHECKOUT_ITEMS.wait(true);

			MODALS.PREVIEW_SELECTIONS.setMessage($template);
			MODALS.PREVIEW_SELECTIONS.open();
		});
	},
	/**
	 * [validate description]
	 * @param  {[type]}   data   [description]
	 * @param  {Function} callback [description]
	 * @return {[type]}         [description]
	 */
	validate: function ( data, callback ) {
		$.post(BASE_URL + 'externalToursAddGuest/validate', data)
		.done( callback )
		.fail(function ( error ) {
			CHECKOUT_ITEMS.wait(true);
			let errors = error.responseJSON.errors;

			CHECKOUT_ITEMS.eleContainer.find('[error-container]').addClass('sr-only');
			$.each(errors, function ( key, items ) {
				CHECKOUT_ITEMS.eleContainer.find('[error-container][data-type="' + key + '"]').removeClass('sr-only');
				CHECKOUT_ITEMS.eleContainer.find('[error-container][data-type="' + key + '"]').load(BASE_URL + 'externaltours/template/body-checkout-errors', {data: items}, function () {

				});
			});

			MODALS.CHECKOUT_ERRORS.setMessage( errors.main );
			MODALS.CHECKOUT_ERRORS.open();
		});
	},
	/**
	 * [book description]
	 * @param  {[type]} data     [description]
	 * @param  {[type]} response [description]
	 * @return {[type]}          [description]
	 */
	book: function ( dialog ) {
		dialog.close();
		CHECKOUT_ITEMS.wait();

		let paymentCallback = {'PAYPAL': CHECKOUT_ITEMS.bookWithPayPal};
		if ( CHECKOUT_ITEMS.PARAMETERS.payment_type in paymentCallback )
		{
			paymentCallback[ CHECKOUT_ITEMS.PARAMETERS.payment_type ]( CHECKOUT_ITEMS.PARAMETERS, CHECKOUT_ITEMS.RESPONSE );
			return false;
		}
		if (CHECKOUT_ITEMS.PARAMETERS.payment_type=='CREDITCARD') {
			CHECKOUT_ITEMS.PARAMETERS.payment['token']=CHECKOUT_ITEMS.RESPONSE.CREDITCARD['TOKEN'];
		}
		// console.log(JSON.parse(JSON.stringify(CHECKOUT_ITEMS.PARAMETERS)));
		$.post(BASE_URL + 'externalToursAddGuest/createBooking', CHECKOUT_ITEMS.PARAMETERS)
		.done(function ( response ) {
			// console.log(response);
			window.location.href = 'externaltours/review?book=' + response.bookingId + '&item=' + response.itemId;
		})
		.fail(function ( error ) {
			CHECKOUT_ITEMS.wait(true);
			var resJSON=error.responseJSON;

			if (resJSON.success) {
				MODALS.CHECKOUT_ERRORS.setMessage( resJSON.message );
				MODALS.CHECKOUT_ERRORS.open();
			} else {
				MODALS.ERROR_UPON_REQUEST.open();
			}
		});
	},
	/**
	 * [bookWithPayPal description]
	 * @param  {[type]} data     [description]
	 * @param  {[type]} response [description]
	 * @return {[type]}       [description]
	 */
	bookWithPayPal: function ( data, response ) {
		data.reference = response.PAYPAL.REFERENCE;
		data.token = response.PAYPAL.TOKEN;

		$.post(BASE_URL + 'externalToursAddGuest/savePayPalData', data)
		.done(function ( response ) {
			window.location.href = response.REDIRECT;
		})
		.fail(function( error ) {
			CHECKOUT_ITEMS.wait(true);
			MODALS.NETWORK_ERROR.open();
		});
	},
	/**
	 * [totalPrice description]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	totalPrice: function ( data ) {
		return data.isDiscounted ? data.discounted.prices.total : data.total;
	},
	/**
	 * [getExtraData description]
	 * @param  {[type]} data [description]
	 * @return {[type]}   [description]
	 */
	getExtraData: function ( data ) {
		// console.log('getExtraData',JSON.parse(JSON.stringify(data)));
		let itemKeys = {'extrabill': {name: 'extrabills'}, 'items': {name: 'products'}, 'packages': {name: 'packages'}};
		let inclusions = '';
		let supplements = {};
		let items = {extrabills: [], products: [], packages: [], supplements: []};
		let flight = {};
		let costings = {
			originalCurrency : '',
			convertedCurrency: CONVERTION_CURRENCY,
			rateOfConvertion : 0,

			OriginalBasePrice : 0,
			convertedBasePrice: 0,

			commission        : 0,
			commInAmount      : 0,
			commAmt           : 0,
			gross_amount      : 0,
			markup_amount     : 0,
			total_less_amount : 0,
			due_amount        : 0,
			profit_amount     : 0,
			
			comm_pct_amount  : 0,
			markup_pct_amount: 0,
			
			discount_pct   : 0,
			discount_amount: 0,
		};
		let final_costing=JSON.parse(JSON.stringify(costings));

		$.each(itemKeys, function ( itemKey, itemName ) {
			/*if (itemKey.toLowerCase()=='extrabill') {
				return true;
			}*/
			if ( itemKey in data && data[ itemKey ].items.length > 0 )
			{
				let dataItem = data[ itemKey ];
				inclusions += itemName.name.toUpperCase() + ': ';
				inclusions += dataItem.inclusions + '<br />';
				
				
				$.each( JSON.parse(JSON.stringify(dataItem.items)), function ( ket, item ) {
					if ( 'supplement' in item )
					{
						let supplement = item.supplement;
						if ( supplement.key in supplements )
						{
							supplements[ ket+'-'+supplement.key ].quantity += supplement.quantity;
							supplements[ ket+'-'+supplement.key ].subtotal += supplement.subtotal;
						} else {
							supplements[ ket+'-'+supplement.key ] = supplement;
						}
					}
					// console.log(JSON.parse(JSON.stringify(item.costings))); 
					if (data.isDiscounted&&data.discounted.data.category.toLowerCase()=='product') {
						switch(data.discounted.data.type.toLowerCase()){
							case'percentage':
								item.costings.discount_pct=data.discounted.data.value;
								break;
						}
					}
					let costing = CHECKOUT_ITEMS.costing( JSON.parse(JSON.stringify(item.costings)),data.discounted);

					// console.log(JSON.parse(JSON.stringify(costing)));
					$.each(['basePrice',
						'converted',
						'net_amount_agency',
						'markup_amount',
						'gross_amount',
						'commission_amount',
						'discount_amount',
						'total_less_amount',
						'due_amount',
						'profit_amount',
						],function(_key,_val){
						if (costing.hasOwnProperty(_val)) {
							costing[_val]=costing[_val]*parseInt(item.quantity);
						}
					});

					costings['OriginalBasePrice']  += costing.basePrice;
					costings['convertedBasePrice'] += costing.converted;
					// For the item costing only
					if (itemKey=='items') {
						if ( costings['discount_pct'] == 0 ) {
							costings.discount_pct = costing.discount_pct;
							final_costing.discount_pct = costing.discount_pct;
						}
						if ( costings['comm_pct_amount'] == 0 ) {
							costings.comm_pct_amount = costing.commission_pct;
							final_costing.comm_pct_amount = costing.commission_pct;
						}
						if ( costings['markup_pct_amount'] == 0 ) {
							costings.markup_pct_amount = costing.markup_pct;
							final_costing.markup_pct_amount = costing.markup_pct;
						}
						if ( costings['originalCurrency'] == '' ) {
							costings.originalCurrency = costing.fromCurrency;
							final_costing.originalCurrency = costing.fromCurrency;
						}
						if ( costings['rateOfConvertion'] == 0 && 'rate' in costing ) {
							costings.rateOfConvertion = costing.rate;
							final_costing.rateOfConvertion = costing.rate;
						}
					}
					// Updating item and extrabill costings
					item.costings=JSON.parse(JSON.stringify(costing));
					item.costings['commission']        = parseFloat(costing.commission_amount);
					item.costings['markup_amount']     = parseFloat(costing.markup_amount);
					item.costings['total_less_amount'] = parseFloat(costing.total_less_amount);
					item.costings['profit_amount']     = parseFloat(costing.profit_amount);
					item.costings['gross_amount']      = parseFloat(costing.gross_amount);
					item.costings['due_amount']        = parseFloat(costing.due_amount);
					item.costings['totalDueAmt']       = parseFloat(costing.due_amount);
					item.costings['discInPct']         = parseFloat(costing.discount_pct);
					item.costings['discInAmount']      = parseFloat(costing.discount_amount);
					item.costings['discAmt']           = parseFloat(costing.discount_amount);

					/*if (data.isDiscounted&&data.discounted.data.category.toLowerCase()=='product') {
						switch(data.discounted.data.type.toLowerCase()){
							case'percentage':
								item.costings.discInPct      = data.discounted.data.value;
								item.costings.discInAmount   = (item.costings.gross_amount*(data.discounted.data.value/100));
								item.costings.totalLessAmt   =(item.costings.commission+item.costings.discInAmount);
								item.costings.totalDueAmt    =(item.costings.gross_amount-item.costings.totalLessAmt);
								item.costings.totalProfitAmt =(item.costings.markup_amount-item.costings.totalLessAmt);
								break;
						}
					}*/
					// Only add to the item costings
					if (itemKey.toLowerCase()=='packages'||itemKey.toLowerCase()=='items') {
						costings.commission        += costing.commission_amount;
						costings.markup_amount     += costing.markup_amount;
						costings.total_less_amount += costing.total_less_amount;
						costings.profit_amount     += costing.profit_amount;
						costings.due_amount        += costing.due_amount;
						costings.gross_amount      += costing.gross_amount;
						costings.discount_amount   += costing.discount_amount;
						/*if ( ('isMarkup' in item.costings) == false || ( 'isMarkup' in item.costings && item.costings.isMarkup == true ) )
						{
							costings['commission']        += ((parseFloat(item.costings['commission'])       * item.quantity));
							costings['markup_amount']     += (parseFloat(item.costings['markup_amount'])     * item.quantity);
							costings['total_less_amount'] += (parseFloat(item.costings['total_less_amount']) * item.quantity);
							costings['profit_amount']     += (parseFloat(item.costings['profit_amount'])     * item.quantity);
							costings['due_amount']        += dueAmout;
							costings['gross_amount']      += grossAmount;
						} else if ( 'isMarkup' in item.costings && item.costings.isMarkup == false ) {
							grossAmount = item.costings.convertedBasePrice * item.quantity;
							dueAmout    = grossAmount;
							costings['due_amount']   += dueAmout;
							costings['gross_amount'] += grossAmount;
						}*/
					}
					final_costing.OriginalBasePrice  += costing.basePrice;
					final_costing.convertedBasePrice += costing.converted;
					final_costing.commission         += costing.commission_amount;
					final_costing.markup_amount      += costing.markup_amount;
					final_costing.total_less_amount  += costing.total_less_amount;
					final_costing.profit_amount      += costing.profit_amount;
					final_costing.due_amount         += costing.due_amount;
					final_costing.gross_amount       += costing.gross_amount;
					final_costing.discount_amount    += costing.discount_amount;

					
					// item.costings.due_amount = dueAmout;
					items[ itemName.name ].push( item );
				});
			}
		});
		if (!jQuery.isEmptyObject(supplements)) {
			inclusions += 'SUPPLEMENTS: ';
		}
		// console.log('Supplements',JSON.parse(JSON.stringify(supplements)));
		$.each(supplements, function ( key, item ) {
			inclusions += item.name + ' X' + item.quantity + ' ($' + $.number(item.price,2) + '),';
			// console.log('Supplement',JSON.parse(JSON.stringify(item.costings)));
			// console.log('Supplement Key',JSON.parse(JSON.stringify(supplements[ key ].costings)));

			if (data.isDiscounted&&data.discounted.data.category.toLowerCase()=='product') {
				switch(data.discounted.data.type.toLowerCase()){
					case'percentage':
						item.costings.discount_pct=data.discounted.data.value;
						break;
				}
			}

			let costing = CHECKOUT_ITEMS.costing( JSON.parse(JSON.stringify(item.costings)),data.discounted );

			/*costings['OriginalBasePrice']  += (parseFloat(item.costings['OriginalBasePrice'])   * parseInt(item.quantity));
			costings['convertedBasePrice'] += ((parseFloat(item.costings['convertedBasePrice']) * parseInt(item.quantity)));

			costings['commission']        += ((parseFloat(costing['commission'])       * item.quantity));
			costings['gross_amount']      += ((parseFloat(costing['gross_amount'])     * item.quantity));
			costings['markup_amount']     += (parseFloat(costing['markup_amount'])     * item.quantity);
			costings['total_less_amount'] += (parseFloat(costing['total_less_amount']) * item.quantity);
			costings['due_amount']        += ((parseFloat(costing['due_amount'])       * item.quantity));
			costings['profit_amount']     += (parseFloat(costing['profit_amount'])     * item.quantity);*/

			// supplements[ key ].costings.due_amount = parseFloat(costing['due_amount']) * item.quantity;
			// items.supplements.push(supplements[ key ]);
			$.each(['basePrice',
				'converted',
				'net_amount_agency',
				'markup_amount',
				'gross_amount',
				'commission_amount',
				'discount_amount',
				'total_less_amount',
				'due_amount',
				'profit_amount',
				],function(_key,_val){
				if (costing.hasOwnProperty(_val)) {
					costing[_val]=costing[_val]*parseInt(supplements[ key ].quantity);
				}
			});
			let supplement = {};
			supplement.supplement_name = supplements[ key ].name;
			supplement.supplement_qty = supplements[ key ].quantity;
			supplement.supplement_voucher_description = '';
			supplement.supplement_summary_description = '';
			supplement.supplement_costings = {
				fromCurrency         : costing.fromCurrency,
				fromRate             : 1,
				toCurrency           : costing.toCurrency,
				toRate               : costing.rate,
				originalAmount       : costing.basePrice,
				convertedBasePrice   : costing.converted,
				orginalNetAmt        : costing.converted,
				netAmountInAgency    : costing.net_amount_agency,
				markupInAmount       : costing.markup_amount,
				markupInPct          : costing.markup_pct,
				grossAmt             : costing.gross_amount,
				commInPct            : costing.commission_pct,
				commInAmount         : costing.commission_amount,
				commAmt              : costing.commission_amount,
				discInPct            : costing.discount_pct,
				discInAmount         : costing.discount_amount,
				discAmt              : costing.discount_amount,
				totalLessAmt         : costing.total_less_amount,
				totalDueAmt          : costing.due_amount,
				totalProfitAmt       : costing.profit_amount,
				gstInPct             : 0,
				gstInAmount          : 0,
				gstAmt               : 0,
			};
			final_costing.OriginalBasePrice  += costing.basePrice;
			final_costing.convertedBasePrice += costing.converted;
			final_costing.commission         += costing.commission_amount;
			final_costing.markup_amount      += costing.markup_amount;
			final_costing.total_less_amount  += costing.total_less_amount;
			final_costing.profit_amount      += costing.profit_amount;
			final_costing.due_amount         += costing.due_amount;
			final_costing.gross_amount       += costing.gross_amount;
			final_costing.discount_amount    += costing.discount_amount;
			/*if (data.isDiscounted&&data.discounted.data.category.toLowerCase()=='product') {
				switch(data.discounted.data.type.toLowerCase()){
					case'percentage':
						supplement.supplement_costings.discInPct = data.discounted.data.value;
						supplement.supplement_costings.discInAmount = (supplement.supplement_costings.grossAmt*(data.discounted.data.value/100));
						supplement.supplement_costings.totalLessAmt=(supplement.supplement_costings.commInAmount+supplement.supplement_costings.discInAmount);
						supplement.supplement_costings.totalDueAmt=(supplement.supplement_costings.grossAmt-supplement.supplement_costings.totalLessAmt);
						supplement.supplement_costings.totalProfitAmt=(supplement.supplement_costings.markupInAmount-supplement.supplement_costings.totalLessAmt);
						break;
				}
			}*/
			/*$.each(['originalAmount',
				'convertedBasePrice',
				'orginalNetAmt',
				'netAmountInAgency',
				'markupInAmount',
				// 'markupInPct',
				'grossAmt',
				// 'commInPct',
				'commInAmount',
				'commAmt',
				// 'discInPct',
				'discInAmount',
				'discAmt',
				'totalLessAmt',
				'totalDueAmt',
				'totalProfitAmt',
				],function(key,val){
					if (supplement.supplement_costings.hasOwnProperty(val)) {
						supplement.supplement_costings[val]=supplement.supplement_costings[val]*supplement.supplement_qty;
					}
			});*/
			/*supplement.supplement_costings.originalAmount
			supplement.supplement_costings.convertedBasePrice
			supplement.supplement_costings.orginalNetAmt
			supplement.supplement_costings.netAmountInAgency
			supplement.supplement_costings.markupInAmount
			supplement.supplement_costings.markupInPct
			supplement.supplement_costings.grossAmt
			supplement.supplement_costings.commInPct
			supplement.supplement_costings.commInAmount
			supplement.supplement_costings.commAmt
			supplement.supplement_costings.discInPct
			supplement.supplement_costings.discInAmount
			supplement.supplement_costings.discAmt
			supplement.supplement_costings.totalLessAmt
			supplement.supplement_costings.totalDueAmt
			supplement.supplement_costings.totalProfitAmt*/
			supplements[ key ] = supplement;
			items.supplements.push(supplement);
		});

		if ( data.hasFlight )
		{
			flight = data.flight;
			flight.discount = 0;

			// inclusions += '<br />';
			inclusions += flight.inclusions;
		}
		if ( data.isDiscounted )
		{
			flight.discount = data.discounted.difference.flight;
			/*if (data.discounted.data.category.toLowerCase()=='product') {
				switch(data.discounted.data.type.toLowerCase()){
					case'percentage':
						costings.discount_amount  = (costings.gross_amount *(data.discounted.data.value/100));
						costings.discount_pct = data.discounted.data.value;
						costings.total_less_amount=(costings.commission+costings.discount_amount);
						costings.due_amount=(costings.gross_amount-costings.total_less_amount);
						costings.profit_amount=(costings.markup_amount-costings.total_less_amount);
						break;
				}
			}*/

			inclusions += '<br />';
			inclusions += data.discounted.message;
		}
		
		let temp_items = {};
		$.each( items, function ( key, items ) {
			if ( items.length > 0 )
			{
				temp_items[ key ] = items;
			}
		});

		return {
			items: temp_items,
			flight: flight,
			inclusion: inclusions,
			supplements: supplements,
			costings: costings,
			final_costing: final_costing,
			tour: data.tour,
			PROMOCODE: COUPON.CODE,
			IS_AGENT: data.IS_AGENT
		}
		
	},
	/**
	 * [getData description]
	 * @return {[type]} [description]
	 */
	getData: function ( data ) {
		let parameters = {};

		parameters.passengers = this.elePassengers.serializeObject().passengers;
		parameters.voucher = this.eleVoucher.serializeObject();

		parameters.previous_link = PREVIOUS_LINK;
		parameters.product = PRODUCT;

		parameters.payment_type = this.PAYMENT_TYPE[ this.elePaymentMethods.find('.tab-pane.active').attr('id') ];
		if( this.IS_PAYLATER ) { parameters.payment_type = 'PAYLATER' }
		parameters.isPayable = ($.inArray(parameters.payment_type, this.PAYABLE_TYPE) > -1);

		parameters.isDiscounted = data.isDiscounted;
		// parameters.includeFlight = data.hasFlight;
		parameters.includeFlight = false;
		parameters.policy_read = this.eleContainer.find('#checkout-policy').prop('checked');
		return parameters;
	},
	/**
	 * [getCreditCardData description]
	 * @param  {[type]} data [description]
	 * @return {[type]}   [description]
	 */
	getCreditCardData: function ( data ) {
		let payment = CHECKOUT_ITEMS.elePaymentMethods.find('#payment-method-credit-card-data').serializeObject();
		let total = ( data.isDiscounted ) ? data.discounted.prices.total : data.total;

		payment.payment_amount = total;
		payment.amount = total;

		return payment;
	},
	/**
	 * [getCreditCardData description]
	 * @param  {[type]} data [description]
	 * @return {[type]}   [description]
	 */
	getPayPalData: function ( data ) {
		let payment = CHECKOUT_ITEMS.elePaymentMethods.find('#payment-method-paypal-data').serializeObject();
		let total = ( data.isDiscounted ) ? data.discounted.prices.total : data.total;

		payment.payment_amount = total;

		return payment;
	},
	/**
	 * [getCreditCardData description]
	 * @param  {[type]} data [description]
	 * @return {[type]}   [description]
	 */
	getENettData: function ( data ) {
		let payment = CHECKOUT_ITEMS.elePaymentMethods.find('#payment-method-enett-data').serializeObject();
		let total = ( data.isDiscounted ) ? data.discounted.prices.total : data.total;

		payment.amount = total;
		payment.payment_amount = total;

		return payment;
	},
	/**
	 * [costing description]
	 * @param  {[type]} converted    [description]
	 * @param  {[type]} commisionPCT [description]
	 * @param  {[type]} markupPTC    [description]
	 * @return {[type]}              [description]
	 */
	costing: function ( costing, discount ) {
		if (typeof discount !='undefined'&& typeof discount.data.commission != 'undefined') {
			costing.commission_pct=discount.data.commission;
			costing.commission_amount=costing.gross_amount*this.convert_decimal(parseFloat(costing.commission_pct));
		}
		costing.discount_amount   = costing.gross_amount*this.convert_decimal(parseFloat(costing.discount_pct));
		costing.total_less_amount = costing.commission_amount+costing.discount_amount;
		costing.due_amount        = costing.gross_amount - costing.total_less_amount;
		costing.profit_amount     = costing.markup_amount - costing.total_less_amount;
		// costing.due_amount=Math.round(costing.due_amount);
		return costing;
	},
	/**
	 * [wait description]
	 * @return {[type]} [description]
	 */
	wait: function ( isHidden ) {
		var isHidden = isHidden || false;

		if ( isHidden ) { this.eleContainer.waitMe('hide'); return false; }

		this.eleContainer.waitMe({
			effect : 'bounce',
			text : 'Your request has been processed <br />Please wait . . . <br />You will be redirected once the request is done.',
			bg : 'rgba(255,255,255,0.7)',
			color : '#000',
			maxSize : '',
			textPos : 'vertical',
			fontSize : '',
			source : ''
		});
	},
	/**
	 * [convert_decimal description]
	 * @param  {[type]} number [description]
	 * @return {[type]}        [description]
	 */
	convert_decimal: function ( number ) {
		return ( parseFloat(number) / 100 );
	},
	/**
	 * [load description]
	 * @param  {[type]} data [description]
	 * @return {[type]}   [description]
	 */
	load: function ( callback ) {
		var callback = callback || false;
		let data = ITEM_SELECTION.overAllTotal(true);

		this.eleContainer.load(BASE_URL + 'externaltours/template/body-checkout-items', data, function () {
			CHECKOUT_ITEMS.elePassengers = CHECKOUT_ITEMS.eleContainer.find('#checkout-passengers');
			CHECKOUT_ITEMS.eleVoucher = CHECKOUT_ITEMS.eleContainer.find('#checkout-voucher');
			CHECKOUT_ITEMS.elePaymentMethods = CHECKOUT_ITEMS.eleContainer.find('#payment-methods');
			CHECKOUT_ITEMS.eleCancelBtn = CHECKOUT_ITEMS.eleContainer.find('#checkout-cancel');
			CHECKOUT_ITEMS.eleSelectBtn = CHECKOUT_ITEMS.eleContainer.find('[checkout-selections]');
			CHECKOUT_ITEMS.eleBookBtn = CHECKOUT_ITEMS.eleContainer.find('[proceed-booking]');
			CHECKOUT_ITEMS.elePayLaterBtn = CHECKOUT_ITEMS.eleContainer.find('#pay-later-container');
			CHECKOUT_ITEMS.init();
			
			if( !!callback ) {
				callback();
			}
			// SHOPPING_CART.hide();
			HELPER.formInformation();
			// console.log('CONFIG.PRODUCTION',CONFIG.PRODUCTION)
			if ( ! CONFIG.PRODUCTION )
			{
				CHECKOUT_ITEMS.testInputs();
			}
		});
	},      
	/**
	 * [testInputs description]
	 * @return {[type]} [description]
	 */
	testInputs: function () {
		this.elePassengers.find('input, select').each(function (key, ele) {
			let $this = $( ele );
			if ( $this.get(0).tagName.toLowerCase() == 'select' )
			{
				let $options = $this.find('option');
				$options = $.grep( $options, function ( n, i ) {
					return ( $( n ).val() != '' );
				});

				let choices = [];
				$.each( $options, function ( key, option) {
					let $ele = $( option );
					choices.push( $ele.val() );
				});

				try {
					let random = Math.floor(Math.random() * choices.length) + 1;

					$this.val( choices[ ( random - 1 ) ] );
				} catch ( e ) { }
			} else {
				$this.val( faker.name.firstName() );
			}
			if ( $this.data('imitate') != undefined ) { $this.change(); }
		});

		this.eleVoucher.find('[name="email"]').val(faker.internet.email());
		this.eleVoucher.find('[name="phonenum"]').val(faker.phone.phoneNumber());
		
		this.elePaymentMethods.find('[name="nm_card_holder"]').val('Test Test');
		this.elePaymentMethods.find('[name="no_credit_card"]').val('4564710000000004');
		this.elePaymentMethods.find('[name="no_cvn"]').val('847');
		this.elePaymentMethods.find('[name="dt_expiry_month"]').val('02');
		this.elePaymentMethods.find('[name="dt_expiry_year"]').val('19');
	}
};