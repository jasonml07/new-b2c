import { HELPER } from './helper.js';
import { MODALS } from './modals.js';
import { ITEM_SELECTION } from './item-selection.js';

const $ShoppingCart = $( '#shopping-cart' );
const $ShoppingCartBtn = $( '#shopping-cart-button' );

/**
 * [SHOPPING_CART description]
 * @type {Object}
 */
export var SHOPPING_CART = {
	eleMenuContainer: $ShoppingCart,
	eleButtonContainer: $ShoppingCartBtn,

	init: function () {
	    this.eleButtonContainer.click(function () {
	        let data = ITEM_SELECTION.overAllTotal(true);

	        let isItOkay = HELPER.isItOkayToLoad(data);
	        if ( ! isItOkay ) {
	            MODALS.NO_PASSENGERS.open();
	            return false;
	        }

	        let template = $('<div></div>').load(BASE_URL + 'externaltours/template/body-selection', data, function ( responseText, textStatus ) {
	            MODALS.SELECTIONS.setMessage(template.html());
	            MODALS.SELECTIONS.open();
	        });
	    });
	},

	/*
     * [load description]
     * @return {[type]} [description]
    */
    load: function ( data ) {
        let isItOkay = HELPER.isItOkayToLoad( data );
        if ( !isItOkay ) {
            SHOPPING_CART.empty();
            return false;
        }

        this.eleButtonContainer.find('.label-danger').text(data.items.passengers);
        this.eleMenuContainer.load(BASE_URL + 'externaltours/template/header-shopping-cart?empty=false', data, function () {

        });
    },
    /**
     * [empty description]
     * @return {[type]} [description]
    */
    empty: function () {
        this.eleButtonContainer.find('.label-danger').text('0');
        this.eleMenuContainer.load( BASE_URL + 'externaltours/template/header-shopping-cart?empty=true');
    },
    /**
     * [show description]
     * @return {[type]} [description]
     */
    show: function () {
        this.eleMenuContainer.removeClass('sr-only');
        this.eleButtonContainer.removeClass('sr-only');
    },
    /**
     * [show description]
     * @return {[type]} [description]
     */
    hide: function () {
        this.eleMenuContainer.addClass('sr-only');
        this.eleButtonContainer.addClass('sr-only');
    }
}