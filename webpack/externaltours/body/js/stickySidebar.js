(function ( $ ) {
 
$.fn.stickySidebar = function( options ) {
 
var config = $.extend({
headerSelector: 'header',
headerAddOnHeights: null,
navSelector: 'nav',
contentSelector: '#content',
footerSelector: 'footer',
footerAddOnHieghts: null,
sidebarTopMargin: 20,
footerThreshold: 40
}, options);
 
var fixSidebr = function() {
 // console.log($(config.footerSelector));
 		var footerAddOnHieghts = config.footerAddOnHieghts;
 		var headerAddOnHeights = config.headerAddOnHeights;

		var sidebarSelector = $(this);
		var viewportHeight = $(window).height();
		var viewportWidth = $(window).width();
		var documentHeight = $(document).height();
		var headerHeight = $(config.headerSelector).outerHeight();
		var navHeight = $(config.navSelector).outerHeight();
		var sidebarHeight = sidebarSelector.outerHeight();
		var sidebarWidth = sidebarSelector.parent().outerWidth();
		var contentHeight = $(config.contentSelector).outerHeight();
		var footerHeight = $(config.footerSelector).outerHeight();
		var scroll_top = $(window).scrollTop();
		var fixPosition = contentHeight - sidebarHeight;

		headerHeight += addOnHeightSummision(headerAddOnHeights);
		footerHeight += addOnHeightSummision(footerAddOnHieghts);

		var breakingPoint1 = headerHeight + navHeight;
		var breakingPoint2 = documentHeight - (sidebarHeight + footerHeight + config.footerThreshold);
		// calculate
		if ( (contentHeight > sidebarHeight) && (viewportHeight > sidebarHeight) ) {
		 
				if (scroll_top < breakingPoint1) {
				 
				sidebarSelector.removeClass('sticky');
				sidebarSelector.css('width','');
		 
		} else if ((scroll_top >= breakingPoint1) && (scroll_top < breakingPoint2)) {
		 
				sidebarSelector.addClass('sticky').css('top', config.sidebarTopMargin);
				sidebarSelector.addClass('sticky').css('width', sidebarWidth);
		 
		} else {
		 
		var negative = breakingPoint2 - scroll_top;
		sidebarSelector.addClass('sticky').css('top',negative);
		 
		}
 
	}
};

var addOnHeightSummision = function ( addOnHeight ) {
	let totalHeight = 0;
	if ( addOnHeight != null ) {
		if ( typeof addOnHeight === 'string' ) {
			totalHeight += $( addOnHeight ).outerHeight();
		} else {
			$.each(addOnHeight, function (key, ele) {
				if ( typeof ele === 'string' ) {
					totalHeight += $( ele ).outerHeight();
				} else {
					totalHeight += parseInt(ele);
				}
			});
		}
	}

	return totalHeight;
}
 
return this.each( function() {
	$(window).on('scroll', $.proxy(fixSidebr, this));
	$(window).on('resize', $.proxy(fixSidebr, this))
	$.proxy(fixSidebr, this)();
});
 
};
 
}( jQuery ));