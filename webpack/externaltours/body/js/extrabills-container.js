import { ITEM_CONTAINER } from './item-container.js';
import { HELPER } from './helper.js';

export var EXTRABILL_CONTAINER = {
    eleContainer: null,

    init: function () {

    },
    /**
     * [getSubtotal description]
     * @return {[type]} [description]
     */
    getSubtotal: function () {
        var passengers = ITEM_CONTAINER.getSubtotal().passengers;
        var items = [];
        var subtotal = 0;
        var inclusions = [];

        this.eleContainer.find('[data-extrabill]').each(function () {
            var $this = $( this );
            var data = $this.data();
            // console.log(HELPER.parseJSON(data.costings));
            data.costings = HELPER.parseJSON(data.costings);
            data.subtotal = parseFloat(data.price) * passengers;
            data.quantity = passengers;

            subtotal += data.subtotal;

            inclusions.push( data.name + ' X' + data.quantity + ' ($' + $.number(data.price, 2) + ')' );
            items.push( data );
        });

        return {
            items: items,
            inclusions: inclusions.join(', '),
            subtotal: subtotal
        };
    }
};