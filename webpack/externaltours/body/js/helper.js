import { FLOW_NAVIGATION } from './global.js';
import {_PRODUCT} from './products.js';
import { ITEM_SELECTION } from './item-selection.js';
import { ITEM_CONTAINER } from './item-container.js';
import { CHECKOUT_ITEMS } from './checkout-items.js';
import { MODALS } from './modals.js';
import { FLIGHT_OPTION, FLIGHT_DETAILS } from './flight.js';
import { SHOPPING_CART } from './shopping-cart.js';

import { CALENDAR } from './calendar.js';
import { LIST } from './list.js';
import { GRID_LIST } from './grid-list.js';

export var HELPER = {
    eleProductContainer: $( '.products-content' ),

    SELECTED_DATE: null,
    END_DATE     : null,
    DURATION     : 0,
    PRODUCT_KEY  : 0,
    /**
     * [getDateDuration description]
     * @param  {[type]} start [description]
     * @param  {[type]} end   [description]
     * @return {[type]}       [description]
     */
    setGlobalTourInfo: function ( start, end, key ) {
        this.PRODUCT_KEY   = key;
        this.SELECTED_DATE = new Date( start );
        this.END_DATE      = new Date( end );

        var timeDiff  = Math.abs( start.getTime() - end.getTime() );
        this.DURATION = Math.ceil( timeDiff / (1000 * 3600 * 24) );
    },
    /**
     * [getGlobalTourInfo description]
     * @return {[type]} [description]
     */
    getGlobalTourInfo: function ( format_date ) {
        var format_date = format_date || false;
        var SELECTED_DATE = this.SELECTED_DATE;
        var END_DATE      = this.END_DATE;
        if ( format_date )
        {
            SELECTED_DATE = $.datepicker.formatDate('dd-mm-yy', new Date(this.SELECTED_DATE));
            END_DATE      = $.datepicker.formatDate('dd-mm-yy', new Date(this.END_DATE));
        }
        return {
            SELECTED_DATE: SELECTED_DATE,
            END_DATE: END_DATE,
            DURATION: this.DURATION
        };
    },
    /**
     * Trigger an event or status in all list view
     * @return {[type]} [description]
     */
    hookItemSelected: function () {
        var packages    = _PRODUCT.getPackages( this.PRODUCT_KEY );
        var extraBills  = _PRODUCT.getExtrabills( this.PRODUCT_KEY );
        var items       = _PRODUCT.getItems( this.PRODUCT_KEY );
        var supplements = _PRODUCT.getSupplements( this.PRODUCT_KEY );
        var tour        = this.getGlobalTourInfo( true );
        ITEM_SELECTION.load( {phoneDetails:PRODUCT.phoneDetails,packages: packages, extraBills: extraBills, items: items, supplements: supplements, tour: tour}, function (  ) {
            HELPER.itemSelection();
            HELPER.initialize();
        });

    },
    /**
     * [initialize description]
     * @return {[type]} [description]
     */
    initialize: function () {
        ITEM_SELECTION.initializeButtons();
        ITEM_SELECTION.initializeContainers();
        FLIGHT_OPTION.init();
        FLIGHT_DETAILS.init();
        SHOPPING_CART.empty();
        SHOPPING_CART.show();

        if (CUSTOM_SETTING) {
          if (CUSTOM_SETTING.DISABLE_FLIGHT) {
            FLIGHT_OPTION.toggle(false);
          }
          if (CUSTOM_SETTING.HIDE_FLIGHT) {
            FLIGHT_OPTION.show(false); 
          }
        }
    },
    /**
     * [returnToProductList description]
     * @return {[type]} [description]
     */
    productList: function () { FLOW_NAVIGATION.PRODUCTS.click(); this.locate( '.products-content' ); },
    /**
     * [itemSelection description]
     * @param  {[type]} ) {            FLOW_NAVIGATION.ITEM_SELECTION.click( [description]
     * @return {[type]}   [description]
     */
    itemSelection: function () { FLOW_NAVIGATION.ITEM_SELECTION.click(); this.locate( '.products-content' ); },
    /**
     * [formInformation description]
     * @param  {[type]} ) {            FLOW_NAVIGATION.PRODUCT_FILL_INFORMATION.click( [description]
     * @return {[type]}   [description]
     */
    formInformation: function () { FLOW_NAVIGATION.PRODUCT_FILL_INFORMATION.click(); this.locate( '.products-content' ); },
    /**
     * [parseJSON description]
     * @param  {[type]} jsonString [description]
     * @return {[type]}            [description]
     */
    parseJSON: function ( jsonString ) {
        var costing = jsonString;
        try {
            costing = jsonString.replace(/(\{|,)\s*(.+?)\s*:/g, '$1 "$2":');
            costing = $.parseJSON( costing );
        } catch ( e ) { }

        return costing;
    },
    /**
     * [getPassengers description]
     * @return {[type]} [description]
     */
    getPassengers: function () {
        return ITEM_CONTAINER.getSubtotal().passengers;
    },
    /**
     * [isInRange description]
     * @param  {[type]}  start   [description]
     * @param  {[type]}  end     [description]
     * @param  {[type]}  current [description]
     * @return {Boolean}         [description]
     */
    isInRange: function ( start, end, current ) {
        var current = current || new Date();

        current = $.datepicker.formatDate('yy-mm-dd', current);

        start   = $.datepicker.parseDate('yy-mm-dd', start);
        end     = $.datepicker.parseDate('yy-mm-dd', end);
        current = $.datepicker.parseDate("yy-mm-dd", current);

        if( (current <= end && current >= start) )
        {
            return true;
        }
        return false;
    },
    /**
     * [updateDisplay description]
     * @return {[type]} [description]
     */
    updateDisplay: function () {
        var data = ITEM_SELECTION.overAllTotal(true);
        
        SHOPPING_CART.load( data );
    },
    /**
     * [isItOkayToLoad description]
     * @param  {[type]}  data [description]
     * @return {Boolean}      [description]
     */
    isItOkayToLoad: function ( data ) {
        var data = data || false;
        if ( ! data || ! ('items' in data) || data.items.passengers == 0 ) { return false; }
        return true;
    },
    /**
     * [proceedToCheckout description]
     * @return {[type]} [description]
     */
    proceedToCheckout: function ( dialog ) {
        var dialog = dialog || false;
        var isItOkay = HELPER.isItOkayToLoad( ITEM_SELECTION.overAllTotal(true) );
        if ( !isItOkay ) {
            MODALS.NO_PASSENGERS.open();
            return false;
        }
        if( FLIGHT_OPTION.INCLUDED && ! FLIGHT_DETAILS.READED_TERM ) {
            MODALS.NO_FLIGHT_TERM.open();
            return false;
        }
        if ( !!dialog ) { dialog.close(); }
        CHECKOUT_ITEMS.load();
    },
    /**
     * [locate description]
     * @param  {[type]} dialog [description]
     * @return {[type]}        [description]
     */
    locate: function ( selector ) {
        let $selector = $( selector );
        $('html, body').animate({ scrollTop: $selector.offset().top - 50}, 500);
    },
    /**
     * [setDataSelections description]
     * @param {[type]} data [description]
     */
    setDataSelections: function ( data ) {
        // console.log('setDataSelections',data)
        let split = data.start.split('-');
        let start = new Date( split[2], (parseInt(split[0]) - 1), split[1] );
        split = data.end.split('-');
        let end = new Date( split[2], (parseInt(split[0]) - 1), split[1] );

        this.setGlobalTourInfo( start, end, parseInt(data.key));
        this.setSelectedProductActive( data.key );
        
        this.hookItemSelected();
    },
    /**
     * [setSelectedProductActive description]
     * @param {[type]} key [description]
     */
    setSelectedProductActive: function ( key ) {
        LIST.clearSelection();
        LIST.setActive( key );

        GRID_LIST.clearSelection();
        GRID_LIST.setActive( key );

        CALENDAR.setActive( key );
        CALENDAR.range();
    }
};