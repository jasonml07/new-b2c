import { LIST_NAVIGATION } from './global.js'
import { HELPER } from './helper.js';

export var LIST = {
	eleContainer: $( LIST_NAVIGATION.LIST.attr('href') ),

	init: function () {
		this.eleContainer.find('[data-product-button]').click(function ( e ) {
			e.preventDefault();
			HELPER.setDataSelections( $(this).data() );
		});
	},
	/**
	 * [clearSelection description]
	 * @return {[type]} [description]
	 */
	clearSelection: function () {
		this.eleContainer.find('[data-product-button]').removeClass('btn-success').addClass('btn-danger');
	},
	/**
	 * [setActive description]
	 * @param {[type]} key [description]
	 */
	setActive: function ( key ) {
		this.eleContainer.find('[data-product-button][data-key="' + key + '"]').removeClass('btn-danger').addClass('btn-success');
	}
};