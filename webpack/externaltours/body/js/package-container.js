import { HELPER } from './helper.js';
import { MODALS } from './modals.js';

export var PACKAGE_CONTAINER = {
    eleContainer: null,

    init: function () {
        this.eleContainer.find('[data-selection-button]').click(function () {
            var $this = $( this );
            var $parent = $this.closest('div.input-group');
            var $selection = $parent.find('[data-package]');
            
            var quantity = parseInt($selection.val());

            switch( $this.data('selectionButton') )
            {
                case 'subtract':
                    if ( quantity > 0 ) { quantity--; }
                    break;
                case 'add':
                    quantity++;
                    break;
            }
            $selection.val(quantity);
            $selection.change();
        });
        this.eleContainer.find('[data-package]').change(function () {
            var $this = $( this );
            var quantity = parseInt($this.val());
            var passengers = HELPER.getPassengers();
            var data = $this.data();

            data.costings = HELPER.parseJSON( data.costings );
            // console.log('Package Costing',data.costings);
            if ( passengers == 0 )
            {
                MODALS.NO_PASSENGERS.open();
                $this.val( 0 );
            } else if ( isNaN(quantity) ) {
                MODALS.UNDEFINED_NUMBER.open();
                $this.val( 0 );
            } else if ( quantity > passengers ) {
                MODALS.PACKAGE.close();
                MODALS.PACKAGE.setMessage('The number of selection exceed from the number of passengers.');
                MODALS.PACKAGE.open();

                $this.val( passengers );
            }
            quantity = parseInt( $this.val() );

            var subtotal = quantity * data.price;
            data.subtotal = subtotal;
            data.quantity = quantity;

            HELPER.updateDisplay();
        });
    },
    /**
     * [getSubtotal description]
     * @return {[type]} [description]
     */
    getSubtotal: function () {
        var items = [];
        var subtotal = 0;
        var inclusions = [];

        this.eleContainer.find('[data-package]').each(function () {
            var $this = $( this );
            var quantity = parseInt( $this.val() );

            if ( quantity > 0 )
            {
                items.push( $this.data() );
                inclusions.push($this.data('name') + ' X' + $this.data('quantity') + ' ($' + $.number($this.data('price'), 2) + ')');
                subtotal += $this.data('subtotal');
            }
        });
        return {
            items: items,
            inclusions: inclusions.join(', '),
            subtotal: subtotal
        };
    },
    /**
     * When the total passenger is not equal to the number of selection
     * Then it will equalize the selection
     * @return {[type]} [description]
     */
    resetSelections: function () {

    }
};