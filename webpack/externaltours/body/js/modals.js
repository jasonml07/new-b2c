import { CHECKOUT_ITEMS } from './checkout-items.js';
import { HELPER } from './helper.js';

export var MODALS = {
    PACKAGE: null,
    NO_PASSENGERS: null,
    UNDEFINED_NUMBER: null,
    PREVIEW_SELECTIONS: null,
    CHECKOUT_PROBLEMS: null,
    NO_FLIGHT_SELECTED: null,
    NO_FLIGHT_TERM: null,
    NETWORK_ERROR: null,
    ERROR_UPON_REQUEST: null,
    SELECTIONS: null,

    closeButton: {
        label: '<span class="hidden-xs">Close</span><span class="visible-xs fa fa-times"></span>',
        action: function ( dialog ) {
            dialog.close();
        }
    },

    init: function () {
        this.SELECTIONS = new BootstrapDialog({
            type: BootstrapDialog.TYPE_INFO,
            cssClass: 'review-modal',
            title: '<strong>Tour Details</strong>',
            closable: false,
            buttons: [{
                label: '<span class="hidden-xs">Close</span><span class="visible-xs fa fa-times"></span>',
                cssClass: 'pull-left',
                action: function ( dialog ) { dialog.close(); }
            }, {
                label: '<strong><span class="hidden-xs">Proceed Checkout&nbsp;</span><span class="fa fa-shopping-cart"></span></strong>',
                cssClass: 'btn-lg btn-primary',
                action: HELPER.proceedToCheckout
            }]
        });
        this.ERROR_UPON_REQUEST = new BootstrapDialog({
            type: BootstrapDialog.TYPE_DANGER,
            title: '<strong>Oops, something wen\'t wrong!!</strong>',
            closable: false,
            message: 'Something wrong upon processing your request.',
            buttons: [MODALS.closeButton]
        });
        this.CHECKOUT_ERRORS = new BootstrapDialog({
            type: BootstrapDialog.TYPE_DANGER,
            title: '<strong>Oops, Something wen\'t wrong.</strong>',
            closable: false,
            buttons: [MODALS.closeButton],
            onshown: MODALS.locate
        });
        this.NETWORK_ERROR = new BootstrapDialog({
            type: BootstrapDialog.TYPE_DANGER,
            title: '<strong>Oops, Something wen\'t wrong.</strong>',
            closable: false,
            message: 'The process was suddenly stopped.',
            buttons: [MODALS.closeButton]
        });
        this.NO_FLIGHT_TERM = new BootstrapDialog({
            type: BootstrapDialog.TYPE_DANGER,
            title: '<strong>Oops, You left something</strong>',
            closable: false,
            message: 'Please read the terms and condition of the flight. <a href="#" data-locate="[name=&quot;hasReadTems&quot;]" class="cursor-pointer">Click here</a>',
            buttons: [MODALS.closeButton],
            onshown: MODALS.locate
        });
        this.NO_FLIGHT_SELECTED = new BootstrapDialog({
            type: BootstrapDialog.TYPE_DANGER,
            title: '<strong>Oops, You left something.</strong>',
            closable: false,
            message: 'Please choose an option whether to include a flight. <a href="#" data-locate="#fligth-option" class="cursor-pointer">Click here</a>',
            buttons: [MODALS.closeButton],
            onshown: MODALS.locate
        });
        this.CHECKOUT_PROBLEMS = new BootstrapDialog({
            type: BootstrapDialog.TYPE_DANGER,
            title: '<strong>Oops, Something wen\'t wrong.</strong>',
            closable: false,
            onshown: function ( dialog ) {
                
            },
            buttons: [{
                label: '<span class="hidden-xs">Close</span><span class="visible-xs fa fa-times"></span>',
                cssClass: 'pull-left',
                action: function ( dialog ) { dialog.close(); }
            }]
        });
        this.PREVIEW_SELECTIONS = new BootstrapDialog({
            type: BootstrapDialog.TYPE_INFO,
            title: '<strong class="text-center">Review of your Tour.</strong>',
            closable: false,
            cssClass: 'review-modal',
            buttons: [{
                    label: '<span class="hidden-xs">Close</span><span class="visible-xs fa fa-times"></span>',
                    cssClass: 'pull-left',
                    action: function ( dialog ) { dialog.close(); }
                },
                {
                    label: '<strong>Confirm</strong></i>',
                    cssClass: 'btn-danger',
                    action: CHECKOUT_ITEMS.book
                }
            ]
        });
        this.UNDEFINED_NUMBER = new BootstrapDialog({
            type: BootstrapDialog.TYPE_WARNING,
            title: '<strong>Unknown selection</strong>',
            message: 'Please fill the right input value.',
            closable: false,
            buttons: [MODALS.closeButton]
        });
        this.NO_PASSENGERS = new BootstrapDialog({
            type: BootstrapDialog.TYPE_WARNING,
            title: '<strong>Lack of Passengers</strong>',
            message: 'Please select some items before selecting packages and flight. <a data-locate="#item-container" class="cursor-pointer">Click here</a>',
            closable: false,
            onshown: MODALS.locate,
            buttons: [MODALS.closeButton]
        });
        this.PACKAGE = new BootstrapDialog({
            type: BootstrapDialog.TYPE_DANGER,
            title: '<strong>Oops, Something wen\'t wrong.</strong>',
            closable: false,
            buttons: [MODALS.closeButton]
        });
    },
    /**
     * [locate description]
     * @param  {[type]} dialog [description]
     * @return {[type]}        [description]
     */
    locate: function ( dialog ) {
        dialog.getModalBody().find('[data-locate]').click(function () {
            dialog.close();
            var $this = $( this );
            var selector = $this.data('locate');
            $('html, body').animate({ scrollTop: $( selector ).offset().top - 100}, 500);
        });
    }
};