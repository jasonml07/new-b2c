import { HELPER } from './helper.js';
import { FLIGHT_DETAILS, FLIGHT_OPTION } from './flight.js';

export var ITEM_CONTAINER = {
    eleContainer: null,
    eleBody: null,
    eleFooter: null,
    

    init: function () {

        this.eleFooter = this.eleContainer.find('tfoot');
        this.eleBody = this.eleContainer.find('tbody');

        this.eleBody.find('[data-selection]').change(function () {
            var $this = $( this );
            var pax = parseInt($this.val());
            var subtotal = pax * $this.data('price');
            var minguest = parseInt($this.data('minguest'));
            var $eleTableRow = $this.closest('tr');
            var previous = {
                pax: 0,
                price: 0,
                supplement: 0
            };
            $eleTableRow.removeData('supplement');

            if ( minguest > 1 )
            {
                var $eleSupplement = $eleTableRow.find('[data-item-supplement]');
                var nearestPax = (pax / minguest);
                var fixNearestPax = Math.floor(nearestPax);
                var isDouble = ((nearestPax % 1 ) != 0);
                
                if( isDouble )
                {
                    var supplement = {
                        key: $eleSupplement.val(),
                        name: $eleSupplement.data('name'),
                        price: $eleSupplement.data('price'),
                        quantity: (pax - (fixNearestPax * minguest)),
                        subtotal: 0,
                        costings: HELPER.parseJSON($eleSupplement.data('costings'))
                    };
                    supplement.subtotal = (parseFloat(supplement.price) * supplement.quantity)

                    subtotal += supplement.subtotal;
                    previous.supplement = supplement;

                    $eleTableRow.data('supplement', supplement);
                    $eleTableRow.find('.supplement-label').addClass('active');
                } else {
                    $eleTableRow.find('.supplement-label').removeClass('active');
                }
            }
            previous.pax = pax;
            previous.price = subtotal;
            $this.data('previous', previous);
            $eleTableRow.data('subtotal', subtotal);
            $eleTableRow.data('quantity', pax);

            // ITEM_CONTAINER.footer( pax, subtotal );
            
            ITEM_CONTAINER.footer( ITEM_CONTAINER.getSubtotal( ) );
            FLIGHT_DETAILS.updateData();

            HELPER.updateDisplay();
        });
    },
    /**
     * [getSubtotal description]
     * @return {[type]} [description]
     */
    getSubtotal: function (  ) {
        var subtotal = 0;
        var passengers = 0;
        var items = [];
        var inclusions = [];

        this.eleBody.find('tr').each(function ( key, item ) {
            var $this = $( item );
            var data = $this.data();
            $this.data('costings', HELPER.parseJSON(data.costings));

            if ( 'subtotal' in data && data.subtotal > 0 )
            {
                items.push( data );
                inclusions.push(data.name + ' X' + data.quantity + ' ($' + $.number(data.price, 2) + ')');

                subtotal += data.subtotal;
                passengers += data.quantity;
            }
        });
        return {
            items: items,
            subtotal: subtotal,
            passengers: passengers,
            inclusions: inclusions.join(', ')
        };
    },
    /**
     * [footer description]
     * @return {[type]} [description]
     */
    footer: function ( data ) {
        var data = data || false;
        var $eleSubtotal = this.eleFooter.find('[data-subtotal]');
        var $eleSubPax = this.eleFooter.find('[data-passengers]');

        if( ! data )
        {
            return {
                subtotal: $eleSubtotal.data('subtotal'),
                passengers: $eleSubPax.data('passengers'),
            };
        }

        $eleSubtotal.data( 'subtotal', data.subtotal ).text( $.number(data.subtotal, 2) );
        $eleSubPax.data( 'passengers', data.passengers ).text( data.passengers );
    },
    /**
     * [reloadFooter description]
     * @return {[type]} [description]
     */
    reloadFooter: function () {
        this.eleFooter.load(BASE_URL + 'externaltours/template/body-products-footer');
    }
};