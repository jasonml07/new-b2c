import { HELPER } from './helper.js';
import { FLIGHT_OPTION, FLIGHT_DETAILS } from './flight.js';
import { MODALS } from './modals.js';

export var COUPON = {
	eleContainer: null,
	isDiscounted: false,
	
	DISCOUNT: null,
	CODE: null,
	START_DATE:null,
	END_DATE:null,
	DURATION:null,

	dialog: new BootstrapDialog({
        size: BootstrapDialog.SIZE_SMALL,
        title: '<strong class="text-center">DISCOUNT</strong>',
        cssClass: 'promo-code-modal text-center',
        type: BootstrapDialog.TYPE_SUCCESS,
        message: ''
        	+'<div class="bs-callout bs-callout-primary text-left"> Note: Promo Code and associated discounts apply to either the land or the flight product only. </div>'
        	+'<div class="form-group">'
			+'    <label for="promo-code-input" class="text-primary control-label">ENTER YOUR PROMO CODE</label>'
			+'    <input type="text" id="promo-code-input" class="form-control" aria-describedby="promo-error-message" />'
			+'    <span id="promo-error-message" class="help-block"></span>'
			+'</div>'
			,
        closable: false,
        buttons: [{
            label: 'Close',
            cssClass: 'pull-left',
            action: function ( dialog ) { dialog.close(); }
        }, {
            label: '<strong>Verify</strong>',
            cssClass: 'btn-success',
            action: function ( dialog ) {
            	let $input = dialog.getModalBody().find('#promo-code-input');
            	dialog.getModalBody().waitMe({
            		effect : 'pulse',
					text : 'Your request is on process <br />Please wait wait. . .',
					bg : 'rgba(255,255,255,0.7)',
					color : '#000',
					textPos : 'vertical',
            	});
            	dialog.enableButtons(false);
            	// console.log('PRODUCT',PRODUCT);
                $.get(BASE_URL + 'externaltours/validate_promo/' + $input.val() +'?product='+PRODUCT.id+'&start_date='+COUPON.START_DATE+'&end_date='+COUPON.END_DATE+'&duration='+COUPON.DURATION)
                .done(function ( response ) {
                	dialog.getModalBody().waitMe('hide');
                	dialog.enableButtons(true);
                	COUPON.DISCOUNT = response.DISCOUNT;
                	COUPON.CODE = $input.val();
                	COUPON.isDiscounted = true;
                	HELPER.updateDisplay();
                	COUPON.hide();
                	dialog.close();
                })
                .fail(function ( response ) {
                	dialog.getModalBody().waitMe('hide');
                	dialog.enableButtons(true);
                	let $form = dialog.getModalBody().find('.form-group');
                	$form.addClass('has-error');
                	$form.find('#promo-error-message').text( response.responseJSON.MESSAGE );
                	COUPON.reset();
                });
            }
        }]
    }),

	init: function (start_date, end_date, duration) {
		this.START_DATE=start_date
		this.END_DATE=end_date
		this.DURATION=duration
		this.eleContainer.find('[coupon-button]').click(function () {
			// console.log('FLIGHT_OPTION.INCLUDED',);
			// if (FLIGHT_OPTION.INCLUDED==null) {
			// 	MODALS.NO_FLIGHT_SELECTED.open();
			// 	return true;
			// }
			COUPON.dialog.open();
		});
		this.hide();
	},
	/**
	 * [parseDiscount description]
	 * @return {[type]} [description]
	 */
	getDiscount: function ( data ) {
		// console.log('getDiscount',JSON.parse(JSON.stringify(data)));
		let discount = this.DISCOUNT;
		let discountedPrices = { flight: 0, product: 0, total: 0 };

		let callback = null;
		let result = 0;
		let callbackParam = { passengers: data.items.passengers, price: 0 };
		if (data.passengers<discount.pax) {
			this.isDiscounted=false;
		}
		if ( ! this.isDiscounted ) {
			return {
				prices: discountedPrices,
				difference: discountedPrices
			};
		}

		if ( $.isArray(discount.category) )
		{
			$.each( discount.category, function( key, value ) {
				callbackParam.price = data.prices[ value ];
				callback = COUPON.discountCallback( discount.type[ key ] );
				result = callback( callbackParam, discount.value[ key ] );
				discountedPrices[ value ] = result;
			});
		} else {
			callbackParam.price = data.prices[ discount.category ];
			callback = COUPON.discountCallback( discount.type );
			result = callback( callbackParam, discount.value );
			discountedPrices[ discount.category ] = result;
		}

		$.each(discountedPrices, function ( key, value ) {
			if ( key != "total" )
			{
				if ( value < 1 )
				{
					discountedPrices.total += data.prices[ key ];
				} else {
					discountedPrices.total += value;
				}
			}
		});
		// discountedPrices.total=Math.round(discountedPrices.total);
		return {
			prices: discountedPrices,
			difference: COUPON.difference( discountedPrices, data.prices ),
			message: discount.message,
			category: discount.category,
			data:discount
		};
	},
	/**
	 * Get the differences of prices
	 * @param  {[type]} discounted [description]
	 * @param  {[type]} prices     [description]
	 * @return {[type]}            [description]
	 */
	difference: function ( discounted, prices ) {
		let diff = {flight: 0, product: 0, total: 0};
		$.each(prices, function ( key, price) {
			diff[ key ] = (discounted[ key ] < 1 ) ? 0: price - discounted[ key ];
			diff.total += diff[ key ];
		});

		return diff;
	},
	/**
	 * [discountByType description]
	 * @param  {[type]} type [description]
	 * @return {[type]}      [description]
	 */
	discountCallback: function ( type ) {
		let callback = null;
		switch ( type ) {
			case 'percentage':
				callback = this.convertPercentage;
				break;
			case 'lessamountperson':
				callback = this.convertLessAmountPerson;
				break;
			case 'amountperson':
				callback = this.convertAmoutPerson;
				break;
			case 'percentperson':
				callback = this.convertPercentPerson;
				break;
		}

		return callback;
	},
	/**
	 * [convertPercentage description]
	 * @param  {[type]} type  [description]
	 * @param  {[type]} price [description]
	 * @return {[type]}       [description]
	 */
	convertPercentage: function ( data, percent ) {
		// console.log(data, percent)
		let result = data.price * (percent / 100);
		// console.log('result',data.price,result);
		return data.price - result;
	},
	/**
	 * [convertLessAmountPerson description]
	 * @param  {[type]} data  [description]
	 * @param  {[type]} price [description]
	 * @return {[type]}       [description]
	 */
	convertLessAmountPerson: function ( data, price) {
		let lessAmount = data.passengers * price;
		return data.price - lessAmount;
	},
	/**
	 * [convertLessAmountPerson description]
	 * @param  {[type]} data  [description]
	 * @param  {[type]} price [description]
	 * @return {[type]}       [description]
	 */
	convertAmoutPerson: function ( data, amount ) {
		let total = data.passengers * amount;
		return total;
	},
	/**
	 * [show description]
	 * @return {[type]} [description]
	 */
	show: function () {
		if ( ! this.isDiscounted ) { this.eleContainer.show(); }
	},
	/**
	 * [show description]
	 * @return {[type]} [description]
	 */
	hide: function () {
		this.eleContainer.hide();
	},
	/**
	 * [show description]
	 * @return {[type]} [description]
	 */
	reset: function () {
		this.DISCOUNT = null;
		this.isDiscounted = false;
	}
};