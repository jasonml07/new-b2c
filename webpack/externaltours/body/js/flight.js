import { HELPER } from './helper.js';
import { ITEM_CONTAINER } from './item-container.js';
import { MODALS } from './modals.js';
/**
 * [FLIGHT_OPTION description]
 * @type {Object}
 */
export var FLIGHT_OPTION = {
    eleContainer: null,
    // INCLUDED: null,
    INCLUDED: null,

    PLEASE_INCLUDE: false,

    init: function ( ) {
        this.eleContainer = $( '#fligth-option' );
        this.eleContainer.find('input[name="hasFlight"]').change(function () {
            var passengers = ITEM_CONTAINER.getSubtotal().passengers;
            var $this = $( this );

            if ( passengers == 0 && !CUSTOM_SETTING.hasOwnProperty('DISABLE_FLIGHT') )
            {
                MODALS.NO_PASSENGERS.open();
                FLIGHT_OPTION.eleContainer.find('label').removeClass('active');
                FLIGHT_OPTION.INCLUDED = null;
                $this.prop('checked', false);
                return false;
            }

            switch( $this.val().toLowerCase().trim() )
            {
                case 'true':
                    FLIGHT_OPTION.INCLUDED = true;
                    break;
                case 'false':
                default :
                    FLIGHT_OPTION.INCLUDED = false;
            }
            if ( FLIGHT_OPTION.INCLUDED )
            {
                FLIGHT_DETAILS.show();
            } else { FLIGHT_DETAILS.hide(); }
            HELPER.updateDisplay();
        });
        this.eleContainer.find('input[name="includesFlight"]').change(function () {
            const $this = $( this );
            const value = $this.val().toLowerCase().trim()
            // console.log('Change value', value)
            switch(value) {
                case 'true':
                    window.open('https://secl-au-ifly.azurewebsites.net', '_blank')
                    FLIGHT_OPTION.PLEASE_INCLUDE = true
                    break;
                case 'false':
                default:
                    FLIGHT_OPTION.PLEASE_INCLUDE = false
            }
        })

    },
    toggle: function (isIncluded){
      this.INCLUDED=isIncluded;
      var $input=this.eleContainer.find('input[name="hasFlight"][value="'+isIncluded+'"]');
      if ($input.length) {
        $input.parent().click();
      }
    },
    show: function (isShow){
      if (isShow) {
        this.eleContainer.show();
      } else {
        this.eleContainer.hide();
      }
    }
};

/**
 * [FLIGHT_DETAILS description]
 * @type {Object}
 */
export var FLIGHT_DETAILS = {
    DEPARTURE_MONTH: DEPARTURE_MONTH,
    eleContainer: null,
    READED_TERM: false,

    init: function (  ) {
        var tourData = HELPER.getGlobalTourInfo();
        this.eleContainer = $( '#flight-details' );

        var today = new Date();
        today.setDate(today.getDate() + 2);
        var minDate = new Date( tourData.SELECTED_DATE );
        minDate.setDate(minDate.getDate() - 1);
        var maxDate = new Date( tourData.END_DATE );
        maxDate.setDate(maxDate.getDate() + 1);

        this.eleContainer.find('#flight-departure-date').datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: today,
            maxDate: minDate,
            defaultDate: minDate,
            onSelect: this.updateData()
        });
        this.eleContainer.find('#flight-return').datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: maxDate,
            defaultDate: maxDate,
        });
        this.eleContainer.find('[name="hasReadTems"]').change(function () {
            var $this = $( this );
            FLIGHT_DETAILS.READED_TERM = $this.prop('checked');
        });
    },
    /**
     * [getMonthPrice description]
     * @param  {[type]} month [description]
     * @return {[type]}       [description]
     */
    getMonthPrice: function ( month ) {
        var price = 0;
        if ( month.toUpperCase() in this.DEPARTURE_MONTH )
        {
            price = this.DEPARTURE_MONTH[ month.toUpperCase() ];
        }
        return price;
    },
    show: function () { this.eleContainer.collapse('show'); },
    hide: function () { this.eleContainer.collapse('hide'); },

    /**
     * [updateData description]
     * @return {[type]} [description]
     */
    updateData: function () {
        var passengers = ITEM_CONTAINER.getSubtotal().passengers;
        var date = this.eleContainer.find('#flight-departure-date').val().split('/');
        var month = $.datepicker.formatDate('MM', new Date(date[2],(parseInt(date[1]) - 1),date[0]));
        var price = this.getMonthPrice( month );
        var totalPrice = price * passengers;
        
        this.eleContainer.find('#flight-passengers').val( passengers );
        this.eleContainer.find('[name="perPerson"]').val( price );
        this.eleContainer.find('#flight-price').val( $.number(totalPrice, 2) );
        this.eleContainer.find('[name="price"]').val( totalPrice );
    },
    /**
     * [getSubtotal description]
     * @return {[type]} [description]
     */
    getSubtotal: function () {
        var data = this.eleContainer.find('form').serializeObject();

        var subtotal = parseInt(data.price);
        var passengers = parseInt(data.pax);
        var price = parseInt(data.perPerson);

        return {
            city: data.city,
            class: data.class,
            departure: data.departure,
            return: data.return,
            subtotal: subtotal,
            price: price,
            quantity: passengers,
            inclusions: 'Return flight price ($' + $.number(price, 2) + ' X' + passengers + ' = $' + $.number(subtotal, 2) + ')'
        };
    }
};