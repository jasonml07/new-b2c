# Webpack tour


### Installation

- Run `yarn` or `npm install` to install its packages
- Run `yarn prod` or `npm run prod` to build project on production mode
- Run `yarn dev` or `npm run dev` to develop and watch changes of the files
- Run `yarn build:dev` or `npm run build:dev` to build on development mode