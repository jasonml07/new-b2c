<?php
require_once('tools.class.php');
require_once('document.class.php');
require_once('journal.class.php');
class Item{
	public function createInvoiced($bookingID,$itemDatas){
		$tools = new Tools; 
		$connection = new MongoClient();
		$db = $connection->db_system;

		$today = new MongoDate(strtotime("now"));

		$invoiceID = $tools->getNextID("invoice");

		$bookingData = $db->booking->findOne(array("booking_id"=>$bookingID));
		$agencyData = $db->agency->findOne(array("agency_code"=>$bookingData['booking_agency_code']));
		foreach($itemDatas as $key=>$row){
			
			
			$invoiceItem = array();
			$invoiceItem['invoiceId'] = (int)$invoiceID;
			$invoiceItem['itemId'] = (int)$row['itemId'];
			$invoiceItem['itemCode'] = $row['itemCode'];
			$invoiceItem['itemName'] = $row['itemName'];
			$invoiceItem['itemServiceCode'] = $row['itemServiceCode'];
			$invoiceItem['itemServiceName'] = $row['itemServiceName'];
			$invoiceItem['itemCostings'] = $row['itemCostings'];
			$invoiceItem['created_datetime'] = $today;

			$invoiceDATA = array();
			$invoiceDATA['itemCostings'] = $row['itemCostings'];
			$invoiceDATA['invoiceId'] = (int)$invoiceID;
			$invoiceDATA['itemId'] = (int)$row['itemId'];
			$invoiceDATA['itemName'] = $row['itemName'];
			$invoiceDATA['booking_id'] = (int)$bookingID;
			$journal = new Journal; 
			$journal->invoice($invoiceDATA,true);

			$db->invoice_items->insert($invoiceItem);
			$db->items->update(array("itemId"=>(int)$row['itemId']),array('$set' => array('invoiceRef' => $invoiceID."-".$bookingID)));


			$paxArray = array();
	   		$whereData = array('item_id'=>(int)$row['itemId']);

			$dataCollectionsResults2 = $db->item_guest->find($whereData);
			try {
				$dataArray2 = iterator_to_array($dataCollectionsResults2);
				 foreach($dataArray2 as $key2 => $row2){
				 	$row2['guestId'] = $key2;
				 	array_push($paxArray, $row2);

				 }
			} catch (Exception $e) {
			   $itemDatas = array();
			}
			//$dataArray[$key]['itemGuests'] = $paxArray;
			$itemDatas[$key]['itemGuests'] = $paxArray;



		}
		



		//print_r($itemDatas);

		$insertData = array();
		$insertData['invoiceId'] = (int)$invoiceID;
		$insertData['bookingId'] = $bookingID;
		$insertData['invoice_reference'] = $invoiceID."-".$bookingID;
		$insertData['booking_agency_code'] = $bookingData['booking_agency_code'];
		$insertData['booking_agency_name'] = $bookingData['booking_agency_name'];
		$insertData['booking_services_start_date'] = $bookingData['booking_services_start_date'];
		$insertData['booking_agency_consultant_code'] = $bookingData['booking_agency_consultant_code'];
		$insertData['booking_agency_consultant_name'] = $bookingData['booking_agency_consultant_name'];
		$insertData['is_active'] = 1;
		$insertData['documentId'] = "";
		$insertData['created_datetime'] = $today;
		$db->invoice->insert($insertData);

		$invoiceData  = $db->invoice->findOne(array("invoice_reference"=>$invoiceID."-".$bookingID));

		$invoiceData['invoiceItems']= array();

		$resData = $db->invoice_items->find(array("invoiceId"=>(int)$invoiceID));

		try {
			$dataArray2 = iterator_to_array($resData);
			
		} catch (Exception $e) {
		   $dataArray2 = array();
		}

		foreach($dataArray2 as $row){

			array_push($invoiceData['invoiceItems'],$row);
		}
		

		$doc =  new Document();

		$content = $doc->generateInvoiceDoc($itemDatas,$bookingData,$agencyData,$invoiceData);

		
		$docID = $doc->generateDocs($content,"INVOICE","invoice-".$invoiceID."-".$bookingID.".pdf","INV",$invoiceID."-".$bookingID);


		
		$db->invoice->update(
					    array("invoice_reference" => $invoiceID."-".$bookingID),
					    array('$set' => array('documentId' => $docID))
					);

		

		

		$connection->close();
		return true;
	}
	public function createCostings($bookingID,$itemDatas){
		$tools = new Tools; 
		$connection = new MongoClient();
		$db = $connection->db_system;

		$today = new MongoDate(strtotime("now"));
		$costingsID = $tools->getNextID("costings");

		$bookingData = $db->booking->findOne(array("booking_id"=>$bookingID));
		$agencyData = $db->agency->findOne(array("agency_code"=>$bookingData['booking_agency_code']));

		$itemIds = array();
		foreach($itemDatas as $key=>$row){
			array_push($itemIds,(int)$row['itemId']);
			$costingsItem = array();
			$costingsItem['costingsId'] = (int)$costingsID;
			$costingsItem['itemId'] = (int)$row['itemId'];
			$costingsItem['itemCode'] = $row['itemCode'];
			$costingsItem['itemName'] = $row['itemName'];
			$costingsItem['itemServiceCode'] = $row['itemServiceCode'];
			$costingsItem['itemServiceName'] = $row['itemServiceName'];
			$costingsItem['itemCostings'] = $row['itemCostings'];
			$costingsItem['created_datetime'] = $today;

			$db->costings_items->insert($costingsItem);

			$paxArray = array();
	   		$whereData = array('item_id'=>(int)$row['itemId']);

			$dataCollectionsResults2 = $db->item_guest->find($whereData);
			try {
				$dataArray2 = iterator_to_array($dataCollectionsResults2);
				 foreach($dataArray2 as $key2 => $row2){
				 	$row2['guestId'] = $key2;
				 	array_push($paxArray, $row2);

				 }
			} catch (Exception $e) {
			   $itemDatas = array();
			}
			//$dataArray[$key]['itemGuests'] = $paxArray;
			$itemDatas[$key]['itemGuests'] = $paxArray;



			

		}


		//getReceiptAmount



		$receiptedItems = $db->item_receipt->find(array("item_id"=>array("\$in"=>$itemIds),"is_refunded"=>0));

	 	

	 	$totalDepositedAmount = 0;
	 	$refundAmount = 0;
	 	try {
			$dataArrayReceiptDataItems = iterator_to_array($receiptedItems);
			#print_r($dataArrayReceiptDataItems);
			$checkedReceipt = array();
			 foreach($dataArrayReceiptDataItems as $key2 => $row2){
			 	//echo $row2['amount_paid']."<br>";

			 	$totalDepositedAmount +=(float)$row2['amount_paid'];
			 	//echo
			 	
			 	$receiptedDatas = $db->receipt->findOne(array("receiptId"=>(int)$row2['receiptId'],"is_refunded"=>0));
			 	#print_r($receiptedDatas);
			 	if(!in_array((int)$row2['receiptId'], $checkedReceipt) ){
			 		//print_r($receiptedDatas);
			 		$refundAmount += (float)$receiptedDatas['receiptRefundAmout'];
			 		array_push($checkedReceipt,(int)$row2['receiptId'] );

			 		
			 	}
			 	//print_r($receiptedDatas);

			 	
			 	
				
			 }
		} catch (Exception $e) {
		   
		}

		$totalAmountAvailable = $totalDepositedAmount+$refundAmount;








		



		//print_r($itemDatas);

		$insertData = array();
		$insertData['costingsId'] = (int)$costingsID;
		$insertData['bookingId'] = $bookingID;
		$insertData['costings_reference'] = $costingsID."-".$bookingID;
		$insertData['booking_agency_code'] = $bookingData['booking_agency_code'];
		$insertData['booking_agency_name'] = $bookingData['booking_agency_name'];
		$insertData['booking_services_start_date'] = $bookingData['booking_services_start_date'];
		$insertData['booking_agency_consultant_code'] = $bookingData['booking_agency_consultant_code'];
		$insertData['booking_agency_consultant_name'] = $bookingData['booking_agency_consultant_name'];
		$insertData['documentId'] = "";
		$insertData['created_datetime'] = $today;
		$db->costings->insert($insertData);

		$costingsData  = $db->costings->findOne(array("costings_reference"=>$costingsID."-".$bookingID));

		$costingsData['costingsItems']= array();

		$resData = $db->costings_items->find(array("costingsId"=>(int)$costingsID));





		try {
			$dataArray2 = iterator_to_array($resData);
			
		} catch (Exception $e) {
		   $dataArray2 = array();
		}

		foreach($dataArray2 as $row){

			array_push($costingsData['costingsItems'],$row);
		}
		

		//deposits
		$depositCollection = $db->deposits->find(array("deposited_booking_id"=>(int)$bookingID,"deposited_is_paid"=>(int)0));
		
		$depositData = array('',0);
		$depositDueDateStr = '';
		$depositDueAmount = 0;
		try {
			
			$dataArrayDep = iterator_to_array($depositCollection);
			
			$isFirst = true;
			foreach($dataArrayDep as $keyDep => $valDep){
				if($isFirst){
					$depositDueDate = $valDep["deposited_due_date"]->sec;
					$depositDueAmount = $valDep["deposited_required_amount"];
					$depositDueDateStr =  date('d, M Y', $depositDueDate);
					
					$isFirst = false;
				}else{
					if($depositDueDate>$valDep["deposited_due_date"]->sec){
						$depositDueDate = $valDep["deposited_due_date"]->sec;
						$depositDueAmount = $valDep["deposited_required_amount"];
						$depositDueDateStr =  date('d, M Y', $depositDueDate);
					}
				}
				
			}

			
			$depositData = array($depositDueDateStr,$depositDueAmount);
			
		} catch (Exception $e) {
		   $depositData = array('',0);
		}

		$doc =  new Document();

		$content = $doc->generateCostingsDoc($itemDatas,$bookingData,$agencyData,$costingsData,$totalAmountAvailable,$depositData);

		
		$docID = $doc->generateDocs($content,"COSTINGS","costings-".$costingsID."-".$bookingID.".pdf","COS",$costingsID."-".$bookingID);


		
		$db->costings->update(
					    array("costings_reference" => $costingsID."-".$bookingID),
					    array('$set' => array('documentId' => $docID))
					);

		

		

		$connection->close();
		return true;
	}
	public function createQuote($bookingID,$itemDatas){
		$tools = new Tools; 
		$connection = new MongoClient();
		$db = $connection->db_system;

		$today = new MongoDate(strtotime("now"));

		$quoteID = $tools->getNextID("quote");

		$bookingData = $db->booking->findOne(array("booking_id"=>$bookingID));
		$agencyData = $db->agency->findOne(array("agency_code"=>$bookingData['booking_agency_code']));

		$itemIds = array();
		foreach($itemDatas as $key=>$row){
			array_push($itemIds,(int)$row['itemId']);
			$costingsItem = array();
			$costingsItem['quoteId'] = (int)$quoteID;
			$costingsItem['itemId'] = (int)$row['itemId'];
			$costingsItem['itemCode'] = $row['itemCode'];
			$costingsItem['itemName'] = $row['itemName'];
			$costingsItem['itemServiceCode'] = $row['itemServiceCode'];
			$costingsItem['itemServiceName'] = $row['itemServiceName'];
			$costingsItem['itemCostings'] = $row['itemCostings'];
			$costingsItem['created_datetime'] = $today;

			$db->quote_items->insert($costingsItem);

			

			

		}


		

		



		//print_r($itemDatas);

		$insertData = array();
		$insertData['quoteId'] = (int)$quoteID;
		$insertData['bookingId'] = $bookingID;
		$insertData['quote_reference'] = $quoteID."-".$bookingID;
		$insertData['booking_agency_code'] = $bookingData['booking_agency_code'];
		$insertData['booking_agency_name'] = $bookingData['booking_agency_name'];
		$insertData['booking_services_start_date'] = $bookingData['booking_services_start_date'];
		$insertData['booking_agency_consultant_code'] = $bookingData['booking_agency_consultant_code'];
		$insertData['booking_agency_consultant_name'] = $bookingData['booking_agency_consultant_name'];
		$insertData['documentId'] = "";
		$insertData['created_datetime'] = $today;
		$db->quote->insert($insertData);

		$quoteData  = $db->quote->findOne(array("quote_reference"=>$quoteID."-".$bookingID));

		$quoteData['quoteItems']= array();

		
		

		

		$doc =  new Document();

		$content = $doc->generateQuoteDoc($itemDatas,$bookingData,$agencyData,$quoteData);

		
		$docID = $doc->generateDocs($content,"QUOTATION","quote-".$quoteID."-".$bookingID.".pdf","QUO",$quoteID."-".$bookingID);


		
		$db->quote->update(
					    array("quote_reference" => $quoteID."-".$bookingID),
					    array('$set' => array('documentId' => $docID))
					);

		

		

		$connection->close();
		return true;
	}
	public function createReceipt($bookingID,$datas){

		$tools = new Tools; 
		$connection = new MongoClient();
		$db = $connection->db_system;

		$today = new MongoDate(strtotime("now"));

		$receiptID = $tools->getNextID("receipt");
		$bookingData = $db->booking->findOne(array("booking_id"=>$bookingID));
		$agencyData = $db->agency->findOne(array("agency_code"=>$bookingData['booking_agency_code']));
		//print_r($datas);
		foreach($datas->receiptItemData as $key=>$row){
			//echo $row->amountPaid;

			$receiptItem = array();
			$receiptItem['amount_paid'] =  (float) $row->amountPaid;
			$receiptItem['receiptId'] = (int)$receiptID;
			$receiptItem['item_id'] = (int)$row->itemID;
			$receiptItem['itemCode'] = $row->itemCode;
			$receiptItem['itemName'] = $row->itemName;
			$receiptItem['itemServiceCode'] = $row->itemServiceCode;
			$receiptItem['itemServiceName'] = $row->itemServiceName;
			$receiptItem['itemCostings'] =  $row->itemCostings;
			$receiptItem['is_refunded'] =  0;
			$receiptItem['itemNaturalDueAmount'] =   (float)$row->itemNaturalDueAmount;
			$receiptItem['DisplayAmount'] =   (float) $row->DisplayAmount;
			$receiptItem['itemTotalAmountToAllocate'] =  $row->itemTotalAmountToAllocate;

			
			$receiptItem['created_datetime'] = $today;

			$db->item_receipt->insert($receiptItem);

			if((float)$row->itemNaturalDueAmount <= (float)$row->amountPaid){
				$db->items->update(array("itemId" => (int)$row->itemID),array('$set' => array('itemIsReceipt' => 1)));
			}
		}

		$doc =  new Document();
		//print_r($datas->receiptItemData);
		$content = $doc->generateReceiptDoc($datas->receiptItemData,$bookingData,$agencyData,$datas->receiptData,(int)$receiptID.'-'.$bookingID);

		$docID = $doc->generateDocs($content,"RECEIPT","receipt-".$receiptID."-".$bookingID.".pdf","RCP",$receiptID."-".$bookingID);

		//checkDeposited $datas->receiptData->receiptAmout

		$amountTemporayPaid = (float)$datas->receiptData->receiptAmout;

		$depositCollection = $db->deposits->find(array("deposited_booking_id"=>(int)$bookingID,"deposited_is_paid"=>(int)0));
		
		try {
			
			$dataArrayDep = iterator_to_array($depositCollection);
			
			
			foreach($dataArrayDep as $keyDep => $valDep){
				if((float)$valDep["deposited_required_amount"]<=$amountTemporayPaid){
					$amountTemporayPaid = $amountTemporayPaid-(float)$valDep["deposited_required_amount"];
					$db->deposits->update(array("deposited_id"=>(int)$valDep["deposited_id"]),array('$set'=>array("deposited_is_paid"=>1)));
				}
			}

			
			$depositData = array($depositDueDateStr,$depositDueAmount);
			
		} catch (Exception $e) {
		   $depositData = array('',0);
		}


		//END checkDeposited $datas->receiptData->receiptAmout


		$insertData = array();
		$insertData['receiptId'] = (int)$receiptID;
		$insertData['bookingId'] = $bookingID;
		$insertData['receipt_reference'] = (int)$receiptID."-".$bookingID;

		$insertData['receiptTypeName'] = $datas->receiptData->receiptTypeName;
        $insertData['receiptDescription'] = $datas->receiptData->receiptDescription;
        $insertData['receiptReferenceNumber'] = $datas->receiptData->receiptReferenceNumber;
        $insertData['receiptChequeNumber'] = $datas->receiptData->receiptChequeNumber;
        $insertData['receiptABAnumber'] = $datas->receiptData->receiptABAnumber;
        $insertData['receiptAccountChequeNumber'] = $datas->receiptData->receiptAccountChequeNumber;
        $insertData['receiptDrawersName'] = $datas->receiptData->receiptDrawersName;
        $insertData['receiptBankCode'] = $datas->receiptData->receiptBankCode;
        $insertData['receiptBankName'] = $datas->receiptData->receiptBankName;
        $insertData['receiptCCTypeCode'] = $datas->receiptData->receiptCCTypeCode;
        $insertData['receiptCCTypeName'] = $datas->receiptData->receiptCCTypeName;
        $insertData['receiptCCNumber'] = $datas->receiptData->receiptCCNumber;
        $insertData['receiptCCExpiryDate'] = $datas->receiptData->receiptCCExpiryDate;
        $insertData['receiptCCSecurityCode'] = $datas->receiptData->receiptCCSecurityCode;
        $insertData['receiptDisplayAmout'] = $datas->receiptData->receiptDisplayAmout;
        $insertData['receiptAmout'] = $datas->receiptData->receiptAmout;
        $insertData['receiptDepositedAmout'] = $datas->receiptData->receiptDepositedAmout;
        $insertData['receiptRefundAmout'] = $datas->receiptData->receiptRefundAmout;
      

		$insertData['booking_agency_code'] = $bookingData['booking_agency_code'];
		$insertData['booking_agency_name'] = $bookingData['booking_agency_name'];
		$insertData['booking_services_start_date'] = $bookingData['booking_services_start_date'];
		$insertData['booking_agency_consultant_code'] = $bookingData['booking_agency_consultant_code'];
		$insertData['booking_agency_consultant_name'] = $bookingData['booking_agency_consultant_name'];
		$insertData['documentId'] = $docID;
		$insertData['is_active'] = 1;
		$insertData['is_refunded'] = 0;
		$insertData['created_datetime'] = $today;
		$db->receipt->insert($insertData);

		$connection->close();
		return true;

	}
	public function createVoucher($bookingID,$datas){

		$tools = new Tools; 
		$connection = new MongoClient();
		$db = $connection->db_system;

		$today = new MongoDate(strtotime("now"));

		$voucherID = $tools->getNextID("voucher");

		$bookingData = $db->booking->findOne(array("booking_id"=>$bookingID));
		$agencyData = $db->agency->findOne(array("agency_code"=>$bookingData['booking_agency_code']));
		$supplierData = $db->main_supplier->findOne(array("supplier_code"=>$datas['itemSupplierCode']));
		

		$doc =  new Document();

		$content = $doc->generateVoucherDoc($datas,$bookingData,$agencyData,$supplierData,(int)$voucherID.'-'.$bookingID);

		$docID = $doc->generateDocs($content,"VOUCHER","voucher-".$voucherID."-".$bookingID.".pdf","VCH",$voucherID."-".$bookingID);

		$insertData = array();
		$insertData['voucherId'] = (int)$voucherID;
		$insertData['bookingId'] = $bookingID;
		$insertData['itemId'] = (int)$datas['itemId'];
		$insertData['voucher_reference'] = (int)$voucherID."-".$bookingID;
		$insertData['documentId'] = $docID;
		$insertData['created_datetime'] = $today;
		$db->item_vouchers->insert($insertData);

		$connection->close();
		return true;
	 	
	 }

	 public function createItinerary($bookingID,$datas){

		$tools = new Tools; 
		$connection = new MongoClient();
		$db = $connection->db_system;

		$today = new MongoDate(strtotime("now"));

		$itineraryID = $tools->getNextID("itinerary");

		$bookingData = $db->booking->findOne(array("booking_id"=>$bookingID));
		$agencyData = $db->agency->findOne(array("agency_code"=>$bookingData['booking_agency_code']));
		$supplierData = $db->main_supplier->findOne(array("supplier_code"=>$datas['itemSupplierCode']));
		

		$doc =  new Document();

		$content = $doc->generateItineraryDoc($datas,$bookingData,$agencyData,(int)$itineraryID.'-'.$bookingID);

		$docID = $doc->generateDocs($content,"ITINERARY","itinerary-".$itineraryID."-".$bookingID.".pdf","ITR",$itineraryID."-".$bookingID);

		$insertData = array();
		$insertData['itineraryId'] = (int)$itineraryID;
		$insertData['bookingId'] = $bookingID;
		$insertData['itinerary_reference'] = (int)$itineraryID."-".$bookingID;
		$insertData['documentId'] = $docID;
		$insertData['created_datetime'] = $today;
		$db->itinerary->insert($insertData);

		$connection->close();
		return true;
	 	
	 }
	 public function reEvaluateCancellation($item,$dataItem){
	 	$connection = new MongoClient();
		$db = $connection->db_system;
	 	$today = new MongoDate(strtotime("now"));
	 	if($item['itemIsInvoiced']){
	 		$invRef = $item['invoiceRef'];
	 		$invArr = explode("-", $invRef);
	 		$db->invoice->update(
				    array("invoice_reference" => $invRef),
				    array('$set' => array('is_active' => 0))
				);
	 		$itemIds = array();
			$invoiceDataItemsReults = $db->invoice_items->find(array("invoiceId"=>(int)$invArr[0]),array("_id"=>0));
			try {
				$dataArrayinvoiceDataItems = iterator_to_array($invoiceDataItemsReults);
				#print_r($dataArrayinvoiceDataItems);
				 foreach($dataArrayinvoiceDataItems as $key2 => $row2){
				 	if((int)$row2['itemId'] != (int)$item['itemId']){
				 		array_push($itemIds, (int)$row2['itemId']);
				 	}
				 	
				 }
			} catch (Exception $e) {
			   
			}

			if(isset($dataItem["itemId"])){
		 		
		 		array_push($itemIds, (int)$dataItem["itemId"]);
		 	}

		 	if(!empty($itemIds)){
				$whereData = array('bookingId'=>(int)$invArr[1],"itemId"=>array("\$in"=>$itemIds));
				$dataCollectionsResults = $db->items->find($whereData);

				try {
					$dataArray= iterator_to_array($dataCollectionsResults);

					foreach($dataArray as $key => $row){
				   		foreach($row['itemSupplements'] as $key2 => $row2){
							if((int)$row2['supplement_qty'] >0){

								$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
								$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
								$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
								$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
								$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
								$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
								$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
								$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
								$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
								$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
								$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
									
							}
				            
						}
					}
					
				} catch (Exception $e) {
				  	$dataArray = array();
				}
				$dataItemToInvoice = array();

				if(count($dataArray) !=0){
					$isInvoiced = self::createInvoiced((int)$invArr[1],$dataArray);
				}
			}

	 	}

	 	$receiptedItems = $db->item_receipt->find(array("item_id"=>(int)$item['itemId'],"is_refunded"=>0));

	 	

	 	$totalDepositedAmount = 0;
	 	$refundAmount = 0;
	 	try {
			$dataArrayReceiptDataItems = iterator_to_array($receiptedItems);
			//print_r($dataArrayReceiptDataItems);
			$checkedReceipt = array();
			 foreach($dataArrayReceiptDataItems as $key2 => $row2){
			 	//echo $row2['amount_paid']."<br>";

			 	$totalDepositedAmount +=(float)$row2['amount_paid'];
			 	//echo
			 	$db->item_receipt->update(
					    array("_id" => new MongoID($row2['_id'])),
					    array('$set' => array('is_refunded' => 1))
					);

			 	$receiptedDatas = $db->receipt->findOne(array("receiptId"=>(int)$row2['receiptId'],"is_refunded"=>0));
			 	
			 	if(!in_array((int)$row2['receiptId'], $checkedReceipt) ){
			 		//print_r($receiptedDatas);
			 		$refundAmount += (float)$receiptedDatas['receiptRefundAmout'];
			 		array_push($checkedReceipt,(int)$row2['receiptId'] );

			 		$db->receipt->update(
					    array("receiptId" => (int)$row2['receiptId']),
					    array('$set' => array('is_refunded' => 1))
					);

			 	}
			 	//print_r($receiptedDatas);

			 	
			 	
				
			 }
		} catch (Exception $e) {
		   
		}

		$totalAmountAvailable = $totalDepositedAmount+$refundAmount;

		if(isset($dataItem["itemId"])){
			if($totalAmountAvailable >0){
				$toPostDataTemp = (object) array(
					"receiptData" => (object) array(
							"receiptTypeCode"=> "CH",
							"receiptTypeName"=> "Cash (Cancellation)",
							"receiptDescription" => "Cancellation",
				            "receiptReferenceNumber" => "Cancellation ".date('Y-m-d'),
				            "receiptChequeNumber" => "",
				            "receiptABAnumber" => "",
				            "receiptAccountChequeNumber" => "",
				            "receiptDrawersName" => "",
				            "receiptBankCode" => "",
				            "receiptBankName" => "",
				            "receiptCCTypeCode" => "",
				            "receiptCCTypeName" => "",
				            "receiptCCNumber" => "",
				            "receiptCCExpiryDate" => "",
				            "receiptCCSecurityCode" => "",
				            "receiptDisplayAmout" => (float)$dataItem['itemCostings']['grossAmt'],
				            "receiptAmout" => (float)$dataItem['itemCostings']['grossAmt'],
				            "receiptDepositedAmout" => (float)$totalAmountAvailable,
				            "receiptRefundAmout" => (float)$totalAmountAvailable- (float)$dataItem['itemCostings']['grossAmt']

					),

					"receiptItemData"=>array(
		            		(object)array(
		            			"itemID" => (int)$dataItem['itemId'],
			                    "itemCode" => $dataItem['itemCode'],
			                    "itemName" => $dataItem['itemName'],
			                    "itemCostings" => $dataItem['itemCostings'],
			                    "itemServiceCode" => $dataItem['itemServiceCode'],
			                    "itemService" => $dataItem['itemServiceName'],
			                    "itemGuest" => array(),
			                    "itemStartDate" => $dataItem['itemStartDate'],
			                    "itemEndDate" =>$dataItem['itemEndDate'],
			                    "amountPaid" => (float)$dataItem['itemCostings']['grossAmt'],
			                    "itemNaturalDueAmount" => (float)$dataItem['itemCostings']['grossAmt'],
			                    "DisplayAmount" => (float)$dataItem['itemCostings']['grossAmt'],
			                    "itemTotalAmountToAllocate" => 0

		            		)

		            	)
				);
				//$dataRes = (object) $toPostDataTemp;
				$db->items->update(
				    array("itemId" => (int)$item['itemId']),
				    array('$set' => array('itemIsReceipt' => 0))
				);

				self::createReceipt((int)$dataItem['bookingId'],$toPostDataTemp);

			}
			

		}



	 }
	 public function reEvaluate($itemID,$item,$resPOST){
	 	$connection = new MongoClient();
		$db = $connection->db_system;
	 	$isCostingsChanged = false;
	 	if($item['itemCostings']!=$resPOST['itemCostings'] && $item['itemCostings']['grossAmt'] != 0){
	 		$isCostingsChanged = true;

	 	}
	 	if($item['itemSupplements']!=$resPOST['itemSupplements']){
	 		$isCostingsChanged = true;

	 	}
	 	if($isCostingsChanged){
	 		//isInvoiced
	 		if($item['itemIsInvoiced']){
		 		$today = new MongoDate(strtotime("now"));

		 		$invRef = $item['invoiceRef'];
		 		$invArr = explode("-", $invRef);
		 		$db->invoice->update(
					    array("invoice_reference" => $invRef),
					    array('$set' => array('is_active' => 0))
					);

		 		$itemIds = array();
				$invoiceDataItemsReults = $db->invoice_items->find(array("invoiceId"=>(int)$invArr[0]),array("_id"=>0));
				try {
					$dataArrayinvoiceDataItems = iterator_to_array($invoiceDataItemsReults);
					#print_r($dataArrayinvoiceDataItems);
					 foreach($dataArrayinvoiceDataItems as $key2 => $row2){

					 	array_push($itemIds, (int)$row2['itemId']);

					 	$invoiceDATA = array();
						$invoiceDATA['itemCostings'] = $row2['itemCostings'];
						$invoiceDATA['invoiceId'] = (int)$invoiceID;
						$invoiceDATA['itemId'] = (int)$row2['itemId'];
						$invoiceDATA['itemName'] = $row2['itemName'];
						$invoiceDATA['booking_id'] = (int)$invArr[1];
						$journal = new Journal; 
						$journal->invoice($invoiceDATA,false);

					 }
				} catch (Exception $e) {
				   
				}

				if(!empty($itemIds)){
					$whereData = array('bookingId'=>(int)$invArr[1],"itemId"=>array("\$in"=>$itemIds));
					$dataCollectionsResults = $db->items->find($whereData);

					try {
						$dataArray= iterator_to_array($dataCollectionsResults);

						foreach($dataArray as $key => $row){
			   		
			   		
					   		

							foreach($row['itemSupplements'] as $key2 => $row2){
								if((int)$row2['supplement_qty'] >0){

									$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
									$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
									$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
									$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
									$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
									$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
									$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
									$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
									$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
									$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
									$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
										
								}
					            
							}

					   }
						
					} catch (Exception $e) {
					  	$dataArray = array();
					}
					$dataItemToInvoice = array();

					if(count($dataArray) !=0){
						$isInvoiced = self::createInvoiced((int)$invArr[1],$dataArray);
					}
				}

		 	}
		 	//check if theres a refund amount on this item
		 	
		 	

		 	
		 	$receiptedItems = $db->item_receipt->find(array("item_id"=>(int)$item['itemId'],"is_refunded"=>0));

		 	$ItemData = $db->items->findOne(array("itemId"=>(int)$item['itemId']));
		 	//print_r($ItemData);
		 	$paxArray = array();
	   		$whereData = array('item_id'=>(int)$item['itemId']);

			$dataCollectionsResults2 = $db->item_guest->find($whereData);
			try {
				$dataArray2 = iterator_to_array($dataCollectionsResults2);
				 foreach($dataArray2 as $key2 => $row2){
				 	array_push($paxArray, $row2);

				 }
			} catch (Exception $e) {
			   
			}



		 	$totalDepositedAmount = 0;
		 	$refundAmount = 0;
		 	try {
				$dataArrayReceiptDataItems = iterator_to_array($receiptedItems);
				//print_r($dataArrayReceiptDataItems);
				$checkedReceipt = array();
				 foreach($dataArrayReceiptDataItems as $key2 => $row2){
				 	//echo $row2['amount_paid']."<br>";

				 	$totalDepositedAmount +=(float)$row2['amount_paid'];
				 	//echo
				 	$db->item_receipt->update(
						    array("_id" => new MongoID($row2['_id'])),
						    array('$set' => array('is_refunded' => 1))
						);

				 	$receiptedDatas = $db->receipt->findOne(array("receiptId"=>(int)$row2['receiptId'],"is_refunded"=>0));
				 	
				 	if(!in_array((int)$row2['receiptId'], $checkedReceipt) ){
				 		//print_r($receiptedDatas);
				 		$refundAmount += (float)$receiptedDatas['receiptRefundAmout'];
				 		array_push($checkedReceipt,(int)$row2['receiptId'] );

				 		$db->receipt->update(
						    array("receiptId" => (int)$row2['receiptId']),
						    array('$set' => array('is_refunded' => 1))
						);

				 	}
				 	//print_r($receiptedDatas);

				 	
				 	
					
				 }
			} catch (Exception $e) {
			   
			}
			$totalDueAmountPOST = (float)$resPOST['itemCostings']['totalDueAmt'];
			$totalGrosAmountPOST = (float)$resPOST['itemCostings']['grossAmt'];
			foreach($resPOST['itemSupplements'] as $key2 => $row2){
				if((int)$row2['supplement_qty'] >0){

					$totalGrosAmountPOST += (float)$row2['supplement_costings']['markupInAmount'];
					$totalDueAmountPOST  += (float)$row2['supplement_costings']['totalDueAmt'];
						
				}
	            
			}
			$amountToAllocate = $totalDueAmountPOST-$totalDepositedAmount;
			$totalAmountAvailable = $totalDepositedAmount+$refundAmount;
			
			if($totalAmountAvailable>=$totalDueAmountPOST){
				//ite needs to be receipted again

				$toPostDataTemp = (object) array(
						"receiptData" => (object) array(
								"receiptTypeCode"=> "CH",
								"receiptTypeName"=> "Cash (Ammendments)",
								"receiptDescription" => "Ammended",
					            "receiptReferenceNumber" => "Ammended ".date('Y-m-d'),
					            "receiptChequeNumber" => "",
					            "receiptABAnumber" => "",
					            "receiptAccountChequeNumber" => "",
					            "receiptDrawersName" => "",
					            "receiptBankCode" => "",
					            "receiptBankName" => "",
					            "receiptCCTypeCode" => "",
					            "receiptCCTypeName" => "",
					            "receiptCCNumber" => "",
					            "receiptCCExpiryDate" => "",
					            "receiptCCSecurityCode" => "",
					            "receiptDisplayAmout" => $totalGrosAmountPOST,
					            "receiptAmout" => $totalGrosAmountPOST,
					            "receiptDepositedAmout" => (float)$totalAmountAvailable,
					            "receiptRefundAmout" => (float)$totalAmountAvailable- $totalGrosAmountPOST

						),

						"receiptItemData"=>array(
			            		(object)array(
			            			"itemID" => (int)$item['itemId'],
				                    "itemCode" => $ItemData['itemCode'],
				                    "itemName" => $ItemData['itemName'],
				                    "itemCostings" => $resPOST['itemCostings'],
				                    "itemServiceCode" => $ItemData['itemServiceCode'],
				                    "itemService" => $ItemData['itemServiceName'],
				                    "itemGuest" => $paxArray,
				                    "itemStartDate" => $ItemData['itemStartDate'],
				                    "itemEndDate" =>$ItemData['itemEndDate'],
				                    "amountPaid" => $totalGrosAmountPOST,
				                    "itemNaturalDueAmount" => $totalGrosAmountPOST,
				                    "DisplayAmount" => $totalGrosAmountPOST,
				                    "itemTotalAmountToAllocate" => 0

			            		)

			            	)
					);
					//$dataRes = (object) $toPostDataTemp;
					$db->items->update(
					    array("itemId" => (int)$item['itemId']),
					    array('$set' => array('itemIsReceipt' => 0))
					);

					self::createReceipt((int)$ItemData['bookingId'],$toPostDataTemp);


			}else{



				$toPostDataTemp = (object) array(
						"receiptData" => (object) array(
								"receiptTypeCode"=> "CH",
								"receiptTypeName"=> "Cash (Amendments)",
								"receiptDescription" => "Amended",
					            "receiptReferenceNumber" => "Ammended ".date('Y-m-d'),
					            "receiptChequeNumber" => "",
					            "receiptABAnumber" => "",
					            "receiptAccountChequeNumber" => "",
					            "receiptDrawersName" => "",
					            "receiptBankCode" => "",
					            "receiptBankName" => "",
					            "receiptCCTypeCode" => "",
					            "receiptCCTypeName" => "",
					            "receiptCCNumber" => "",
					            "receiptCCExpiryDate" => "",
					            "receiptCCSecurityCode" => "",
					            "receiptDisplayAmout" => $totalGrosAmountPOST,
					            "receiptAmout" => $totalGrosAmountPOST,
					            "receiptDepositedAmout" => (float)$totalAmountAvailable,
					            "receiptRefundAmout" => 0

						),

						"receiptItemData"=>array(
			            		(object)array(
			            			"itemID" => (int)$item['itemId'],
				                    "itemCode" => $ItemData['itemCode'],
				                    "itemName" => $ItemData['itemName'],
				                    "itemCostings" => $resPOST['itemCostings'],
				                    "itemServiceCode" => $ItemData['itemServiceCode'],
				                    "itemService" => $ItemData['itemServiceName'],
				                    "itemGuest" => $paxArray,
				                    "itemStartDate" => $ItemData['itemStartDate'],
				                    "itemEndDate" =>$ItemData['itemEndDate'],
				                    "amountPaid" => (float)$totalAmountAvailable,
				                    "itemNaturalDueAmount" => (float)$totalDueAmountPOST,
				                    "DisplayAmount" => $totalGrosAmountPOST,
				                    "itemTotalAmountToAllocate" => 0

			            		)

			            	)
					);
					//$dataRes = (object) $toPostDataTemp;
					$db->items->update(
					    array("itemId" => (int)$item['itemId']),
					    array('$set' => array('itemIsReceipt' => 0))
					);

					self::createReceipt((int)$ItemData['bookingId'],$toPostDataTemp);




			}

	 	}
	 	
	 	$connection->close();
		return true;
	 }

}

?>