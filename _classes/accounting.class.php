<?php
session_start();
require_once('tools.class.php');
class Accounting{
		public static function addEntry($accountEntry,$amount,$isDebit,$referenceID){
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			$journalID = $tools->getNextID("journal");

			$dt = new DateTime(date('Y-m-d'));
			$ts = $dt->getTimestamp();
			$today = new MongoDate($ts);

			$insertData = array();
			$insertData["journalId"] = (int)$journalID;
			$insertData["accountId"] = $accountEntry;
			$insertData["referenceId"] = $referenceID;
			$insertData["debit"] = 0;
			$insertData["credit"] = 0;
			if($isDebit){
				$insertData["debit"] = (float)$amount;
			}else{
				$insertData["credit"] = (float)$amount;
			}
			$insertData["currency"] = "AUD";
			$insertData['created_datetime'] = $today;
			$insertData['updated_datetime'] = $today;
			$insertData['created_by'] = $_SESSION['sys_consultant_name'];
			$db->journal->insert($insertData);

			$connection->close();
		}

	}
?>