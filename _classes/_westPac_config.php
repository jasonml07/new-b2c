<?php
// -------------------------------------------------------------------------
// Configuration file.  Ensure that only the user thant PHP runs under can
// access this file on your web server disk.
// -------------------------------------------------------------------------
$isLive = true;
$TAILORED = true;

// Replace _YOUR_ENCRYPTION_KEY_ with your encryption key from the PayWay web site.
$encryptionKey =  '4aQ0FWxXkxjEPeoDjXwh/Q==';

// Replace _LOG_DIR_ with the directory to log debug messages to.
$logDir =  './logs';

// Replace _BILLER_CODE_ with your biller code from the PayWay web site.
$billerCode =  '172817';

// Replace _USERNAME_ with your PayWay Net username from the PayWay web site.
$username =  'Q17281';

// Replace _PASSWORD_ with your PayWay Net password from the PayWay web site.
$password =  'N4mk67axi';

// Replace _CA_FILE_ with the full path location of the cacerts.crt file.
$caCertsFile =  'cacerts.crt';

// Change these parameters if your server requires a proxy server to connect
// to the PayWay server
$proxyHost = null;
$proxyPort = null;
$proxyUser = null;
$proxyPassword = null;

// Change this to your merchant ID when you are ready to go live
$merchantId = 'TEST';
//$merchantId = '24206542';

// Change this to your configured paypal email address, if applicable
$paypalEmail = 'test@example.com';

// Leave this entry as is
$payWayBaseUrl = 'https://www.payway.com.au/';

if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[strtolower(str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))))] = $value;
               
           }
       }
       return $headers;
    }
}
?>