<?php

session_start();
set_time_limit(0);
require_once('_supplier_config.php');


class HotelBedsClient{
	
	
	public static function get_available_hotels($params){
		if(HB_LIVE){
			$xml_URL = HB_URL;
			
			$user =HB_USER;
			$pass = HB_USER;
		}else{
			$xml_URL = HB_URL_TEST;
			
			$user =HB_USER;
			$pass = HB_PASS;
		}
		
		$token = date('Ymdhis');
		$req = '<HotelValuedAvailRQ echoToken="'.$_SESSION['sys_agency_code_B2B'].$token.'" sessionId="'.$token.'" ';
		$req .= '  xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages HotelValuedAvailRQ.xsd" >';
		
		$req .= '  <Language>ENG</Language> ';
		$req .= '  <Credentials> ';
		$req .= '    <User>'.$user.'</User> ';
		$req .= '    <Password>'.$pass.'</Password>';
		$req .= '  </Credentials> ';
		$req .= '  <PaginationData itemsPerPage="100"  pageNumber="1"/> ';

		$dateFrom = str_replace('-', '', $params->date_start);
		$dateTo = str_replace('-', '', $params->date_end);

		

		$req .= '  <CheckInDate date="'.$dateFrom.'"/>';
		$req .= '  <CheckOutDate date="'.$dateTo.'"/> ';
		$req .= '  <Destination code="'.strtoupper($params->desitnation_code).'" type="SIMPLE"/> ';
		if($params->product_code){
			$req .= '  <HotelCodeList withinResults="Y"><ProductCode>'.$params->product_code.'</ProductCode></HotelCodeList>';
		
		}
		
		$req .= '  <OccupancyList> ';



		
		
		foreach($params->pax as $key=>$value){
			$childNum = $value->child;
			
			$req .= '  <HotelOccupancy> ';
			$req .= '      <RoomCount>1</RoomCount> ';
			$req .= '      <Occupancy> ';
			$req .= '	   <AdultCount>'.$value->adult.'</AdultCount> ';
			$req .= '	   <ChildCount>'.$childNum.'</ChildCount> ';
			if($value->child>0){
				$req .= '     <GuestList> ';
				for($x=0;$x<$value->child;$x++){
					$req .= '<Customer type="CH"><Age>'.$value->childAges[$x].'</Age></Customer>';
				}

				$req .= '     </GuestList> ';
			}
			$req .= '     </Occupancy> ';
			$req .= '  </HotelOccupancy> ';
		}
		$req .= '  </OccupancyList> ';
		$req .= '<ShowCancellationPolicies>Y</ShowCancellationPolicies>';
		$req .= ' </HotelValuedAvailRQ> ';

		
		$postdata = http_build_query(
			    array(
			        'xml_request' => $req
			    )
			);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded\r\nAccept-Encoding: gzip\r\n',
		        'content' => $postdata
		    )
		);

		$context  = stream_context_create($opts);

		$result = file_get_contents($xml_URL, false, $context);
		
		

		if($result!=""){
			$xml = simplexml_load_string($result);

			//$json = json_encode($xml);
			//$hotels = json_decode($json,TRUE);
			
			#print_r($hotels);
			
			
				
			return $xml;
					
		}else{
			return;
		}
		
	}
	public static function serviceAdd($roomDetails,$paxDetails){

		if(HB_LIVE){
			$xml_URL = HB_URL;
			
			$user =HB_USER;
			$pass = HB_PASS;
		}else{
			$xml_URL = HB_URL_TEST;
			
			$user =HB_USER;
			$pass = HB_PASS;
		}
		


		$req2 = '<ServiceAddRQ echoToken="'.$roomDetails->echoToken.'" version="2013/04" '; 
		$req2 .= '  xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages ServiceAddRQ.xsd">';
		
		$req2 .= '  <Language>ENG</Language> ';
		$req2 .= '  <Credentials> ';
		$req2 .= '    <User>'.$user.'</User> ';
		$req2 .= '    <Password>'.$pass.'</Password>';
		$req2 .= '  </Credentials> ';
		$req2 .= '  <Service  availToken="'.$roomDetails->availableToken.'" xsi:type="ServiceHotel"> ';
		$req2 .= '  	<ContractList>';
		$req2 .= '  		<Contract>';
		$req2 .= '  			<Name>'.$roomDetails->contractName.'</Name>';
		$req2 .= '  			<IncomingOffice code="'.$roomDetails->contractOficeCode.'" />';
		$req2 .= '  		</Contract>';
		$req2 .= '  	</ContractList>';
		$req2 .= '  	<DateFrom date="'.$roomDetails->dateFrom.'"/>';
		$req2 .= '  	<DateTo date="'.$roomDetails->dateTo.'"/>';
		$req2 .= '	<Currency code="EUR"/>';
		$req2 .= '	<HotelInfo xsi:type="ProductHotel">';
		$req2 .= '  		<Code>'.$roomDetails->hotelCode.'</Code>';
		$req2 .= '  		<Destination code="'.$roomDetails->destinationCode.'" type="'.$roomDetails->destinationType.'" />';
		$req2 .= '	</HotelInfo>';

		$countRoomDetails = count($roomDetails->roomDetails);
		
		for($x=0;$x<$countRoomDetails;$x++){

			$req2 .= '	<AvailableRoom>';
			$req2 .= '		<HotelOccupancy>';
			$req2 .= '			<RoomCount>1</RoomCount>';
			$req2 .= '			<Occupancy>';
			$req2 .= '					  <AdultCount>'.$roomDetails->roomDetails[$x]->paxes->adult.'</AdultCount>';
			$req2 .= '					  <ChildCount>'.$roomDetails->roomDetails[$x]->paxes->child.'</ChildCount>';
			$req2 .= '					  <GuestList>';

			$ctrChild = 0;
			for($a=0;$a<count($paxDetails[$x]);$a++){
				if($paxDetails[$x][$a]['type'] == "adult"){
					$req2 .= '					<Customer type="AD">';
				}else{
					$req2 .= '					<Customer type="CH">';
					$req2 .= '					<Age>'.$roomDetails->paxes[$x]->childAges[$ctrChild].'</Age>';
					$ctrChild++;
					
				}
				$req2 .= '					<Name>'.$paxDetails[$x][$a]['title'].' '.$paxDetails[$x][$a]['fname'].'</Name>';
				$req2 .= '					<LastName>'.$paxDetails[$x][$a]['lname'].'</LastName>';
				$req2 .= '					</Customer>';

			}

			$req2 .= '					  </GuestList>';
			$req2 .= '			</Occupancy>';
			$req2 .= '		</HotelOccupancy>';
			$req2 .= '		<HotelRoom  onRequest="'.$roomDetails->roomDetails[$x]->onRequest.'" SHRUI="'.$roomDetails->roomDetails[$x]->SHRUI.'">';
			$req2 .= '				<Board code="'.$roomDetails->roomDetails[$x]->boardCode.'" type="'.$roomDetails->roomDetails[$x]->boardType.'"/>';
			$req2 .= '				<RoomType characteristic="'.$roomDetails->roomDetails[$x]->roomChar.'" code="'.$roomDetails->roomDetails[$x]->roomCode.'" type="'.$roomDetails->roomDetails[$x]->roomType.'"/>';
			$req2 .= '		</HotelRoom>';
			$req2 .= '	</AvailableRoom>';
		
		
		}
		$req2 .= '  </Service> ';
		$req2 .= '	</ServiceAddRQ>';

		
		$postdata = http_build_query(
			    array(
			        'xml_request' => $req2
			    )
			);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded\r\nAccept-Encoding: gzip\r\n',
		        'content' => $postdata
		    )
		);

		$context  = stream_context_create($opts);

		$result = file_get_contents($xml_URL, false, $context);
		
		
		if($result!=""){
			$xml = simplexml_load_string($result);
			
			
			
				
			return $xml;
					
		}else{
			return;
		}
		
	}
	public static function purchaseConfirm($item){
		if(HB_LIVE){
			$xml_URL = HB_URL;
			
			$user =HB_USER;
			$pass = HB_PASS;
		}else{
			$xml_URL = HB_URL_TEST;
			
			$user =HB_USER;
			$pass = HB_PASS;
		}
		
		$detailsCred = explode("|", $item["itemSuplierResReq"]);

		$req2 = '<PurchaseConfirmRQ echoToken="'.$detailsCred[0].'" version="2013/04" '; 
		$req2 .= '  xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseConfirmRQ.xsd">';
		
		$req2 .= '  <Language>ENG</Language> ';
		$req2 .= '  <Credentials> ';
		$req2 .= '    <User>'.$user.'</User> ';
		$req2 .= '    <Password>'.$pass.'</Password>';
		$req2 .= '  </Credentials> ';

		$req2 .= '  <ConfirmationData  purchaseToken="'.$detailsCred[1].'"> ';

		$req2 .= '  <Holder type="AD"> ';
		foreach($item['otherDetails']['roomList'] as $room){
			foreach($room['guestList'] as $guest){
				if($guest['CustomerType'] == "AD"){
					$req2 .= '<Name>'.$guest['Name'].'</Name>';
					$req2 .= '<LastName>'.$guest['LastName'].'</LastName>';
					break;
				}
			}
			break;
		}
		$req2 .= '  </Holder> ';
		
		$req2 .= '  <AgencyReference>'.$item["bookingId"].'|'.$item["itemId"].'</AgencyReference>';

		$req2 .= ' 	<ConfirmationServiceDataList>';

		$req2 .= ' 		<ServiceData xsi:type="ConfirmationServiceDataHotel" SPUI="'.$detailsCred[2].'">';

		$req2 .= '			<CustomerList>';

		foreach($item['otherDetails']['roomList'] as $room){
			foreach($room['guestList'] as $guest){
				$req2 .= '			  <Customer type="'.$guest['CustomerType'].'">';
				$req2 .= ' 				<CustomerId>'.$guest['CustomerId'].'</CustomerId>';
				$req2 .= ' 				<Age>'.$guest['Age'].'</Age>';
				$req2 .= ' 				<Name>'.$guest['Name'].'</Name>';
				$req2 .= ' 				<LastName>'.$guest['LastName'].'</LastName>';
				$req2 .= ' 			  </Customer>';
			}
			
			
		}
		
		$req2 .= ' 			</CustomerList>';

		$req2 .= ' 		</ServiceData>';
		$req2 .= ' 	</ConfirmationServiceDataList>';

		$req2 .= '  </ConfirmationData>';
			
		$req2 .= '	</PurchaseConfirmRQ>';

		
		$postdata = http_build_query(
			    array(
			        'xml_request' => $req2
			    )
			);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded\r\nAccept-Encoding: gzip\r\n',
		        'content' => $postdata
		    )
		);

		$context  = stream_context_create($opts);

		$result = file_get_contents($xml_URL, false, $context);
		
		
		if($result!=""){
			$xml = simplexml_load_string($result);
			
			
			
				
			return $xml;
					
		}else{
			return;
		}

	}
	public static function purchaseCancel($purchaseRef){
		
		if(HB_LIVE){
			$xml_URL = HB_URL;
			
			$user =HB_USER;
			$pass = HB_PASS;
		}else{
			$xml_URL = HB_URL_TEST;
			
			$user =HB_USER;
			$pass = HB_PASS;
		}
		
		$detailsCred = explode("-", $purchaseRef);

		$req2 = '<PurchaseCancelRQ version="2013/04" '; 
		$req2 .= '  xmlns="http://www.hotelbeds.com/schemas/2005/06/messages" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages PurchaseCancelRQ.xsd" type="C">';
		
		$req2 .= '  <Language>ENG</Language> ';
		$req2 .= '  <Credentials> ';
		$req2 .= '    <User>'.$user.'</User> ';
		$req2 .= '    <Password>'.$pass.'</Password>';
		$req2 .= '  </Credentials> ';

		$req2 .= '  <PurchaseReference> ';
		$req2 .= '<FileNumber>'.$detailsCred[1].'</FileNumber>';
		$req2 .= '<IncomingOffice code="'.$detailsCred[0].'"/>';
		

		$req2 .= '  </PurchaseReference>';
			
		$req2 .= '	</PurchaseCancelRQ>';

		//echo $req2;
		$postdata = http_build_query(
			    array(
			        'xml_request' => $req2
			    )
			);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded\r\nAccept-Encoding: gzip\r\n',
		        'content' => $postdata
		    )
		);

		$context  = stream_context_create($opts);

		$result = file_get_contents($xml_URL, false, $context);
		//echo $result;
		
		if($result!=""){
			$xml = simplexml_load_string($result);
			
			
			
				
			return $xml;
					
		}else{
			return;
		}

	}
	
	public static function makeBooking($items_pax,$process_id){
	
		if(HP_LIVE){
			$JSON_URL = HP_URL;
		}else{
			$JSON_URL = HP_URL_TEST;
		}
		/*
		&agencyReferenceNumber=&leadTravellerInfo[paxInfo][paxType]=Adult&leadTravellerInfo[paxInfo][title]=Mr&leadTravellerInfo[paxInfo][firstName]=John&leadTravellerInfo[paxInfo][lastName]=DOE&leadTravellerInfo[nationality]=UK&otherTravellerInfo[0][title]=Mr&otherTravellerInfo[0][firstName]=Ahmetr&otherTravellerInfo[0][lastName]=AY&otherTravellerInfo[1][title]=Mr&otherTravellerInfo[1][firstName]=Mehmet&otherTravellerInfo[1][lastName]=YILDIZ&preferences=nonSmoking&note=test%20note
		*/
		$result = ""; 
		$fields = array(
			'method' => "makeHotelBooking",
			'apiKey' => HP_API,
			'processId' => $process_id
			
		);
		
		
		
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= urlencode($key).'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');
		$params = $fields_string;
		$isFirst = true;
		$strUrl = "";
		$ctrLead = 0;
		$ctr =0;
		foreach($items_pax as $item_pax){
			$title = $item_pax->title;
			switch($title){
				case 'Prof': {
					$title = 'Mr';
					break;
				}
				case 'Engr': {
					$title = 'Mr';
					break;
				}
				case 'Dr': {
					$title = 'Mr';
					break;
				}
			}
			if($isFirst){
				$isFirst = false;
				$strUrl .="&leadTravellerInfo[paxInfo][paxType]=Adult";
				$strUrl .="&leadTravellerInfo[paxInfo][title]=".urlencode($title);
				$strUrl .="&leadTravellerInfo[paxInfo][firstName]=".urlencode($item_pax->fname);
				$strUrl .="&leadTravellerInfo[paxInfo][lastName]=".urlencode($item_pax->lname);
				$strUrl .="&leadTravellerInfo[nationality]=".HP_NATIONALITY;
			}else{
				$strUrl .="&otherTravellerInfo[$ctr][title]=".urlencode($title);
				$strUrl .="&otherTravellerInfo[$ctr][firstName]=".urlencode($item_pax->fname);
				$strUrl .="&otherTravellerInfo[$ctr][lastName]=".urlencode($item_pax->lname);
				$ctr++;
			}
			
		}
		$strUrl .= "&preferences=nonSmoking&note=".urlencode("Booked by Eastern Eurotours through HotelsPro");
		
		$url_hotels = $JSON_URL.$params.$strUrl;
		//echo $url_hotels;
		$result = file_get_contents($url_hotels, 0);
		$res = array();
		if($result!=""){
			
			$hotels = json_decode($result);
			#print_r($hotels);
			
			
				
			return $hotels;
					
		}else{
			return "";
		}
		
		

	}
	public static function set_cancel_item($process_id){
		if(HP_LIVE){
			$JSON_URL = HP_URL;
		}else{
			$JSON_URL = HP_URL_TEST;
		}
		/*
		&agencyReferenceNumber=&leadTravellerInfo[paxInfo][paxType]=Adult&leadTravellerInfo[paxInfo][title]=Mr&leadTravellerInfo[paxInfo][firstName]=John&leadTravellerInfo[paxInfo][lastName]=DOE&leadTravellerInfo[nationality]=UK&otherTravellerInfo[0][title]=Mr&otherTravellerInfo[0][firstName]=Ahmetr&otherTravellerInfo[0][lastName]=AY&otherTravellerInfo[1][title]=Mr&otherTravellerInfo[1][firstName]=Mehmet&otherTravellerInfo[1][lastName]=YILDIZ&preferences=nonSmoking&note=test%20note
		*/
		$result = ""; 
		$fields = array(
			'method' => "cancelHotelBooking",
			'apiKey' => HP_API,
			'trackingId' => $process_id
			
		);
		
		
		
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= urlencode($key).'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');
		$params = $fields_string;
		$url_hotels = $JSON_URL.$params;
		$result = file_get_contents($url_hotels, 0);
		$res = array();
		if($result!=""){
			
			$hotels = json_decode($result);
			#print_r($hotels);
			
			
				
			return $hotels;
					
		}else{
			return "";
		}
			

	}
}

?>

