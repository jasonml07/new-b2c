<?php

session_start();
set_time_limit(0);
require_once('_westPac_config.php');


class PaymentWestpac{
	
	
	public static function getToken($parameters){
		global $proxyHost;
        global $proxyPort;
        global $proxyUser;
        global $proxyPassword;
        global $caCertsFile;
        global $payWayBaseUrl;

        // Find the port setting, if any.
        $payWayUrl = $payWayBaseUrl;
        $port = 443;
        $portPos = strpos( $payWayBaseUrl, ":", 6 );
        $urlEndPos = strpos( $payWayBaseUrl, "/", 8 );
        if ( $portPos !== false && $portPos < $urlEndPos )
        {
            $port = (int)substr( $payWayBaseUrl, ((int)$portPos) + 1, ((int)$urlEndPos));
            $payWayUrl = substr( $payWayBaseUrl, 0, ((int)$portPos))
                . substr( $payWayBaseUrl, ((int)$urlEndPos), strlen($payWayBaseUrl));
            //debugLog( "Found alternate port setting: " . $port );
        }
        $ch = curl_init( $payWayUrl . "RequestToken" );
       // debugLog( "Configuring token request to : " . $payWayUrl . "RequestToken" );

        if ( $port != 443 )
        {
            curl_setopt( $ch, CURLOPT_PORT, $port );
            //debugLog( "Set port to: " . $port );
        }

        curl_setopt( $ch, CURLOPT_FAILONERROR, true );
        curl_setopt( $ch, CURLOPT_FORBID_REUSE, true );
        curl_setopt( $ch, CURLOPT_FRESH_CONNECT, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  
        // Set proxy information as required
        if ( !is_null( $proxyHost ) && !is_null( $proxyPort ) )
        {
            curl_setopt( $ch, CURLOPT_HTTPPROXYTUNNEL, true );
            curl_setopt( $ch, CURLOPT_PROXY, $proxyHost . ":" . $proxyPort );
            if ( !is_null( $proxyUser ) )
            {
                if ( is_null( $proxyPassword ) )
                {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, $proxyUser . ":" );
                }
                else
                {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, 
                        $proxyUser . ":" . $proxyPassword );
                }
            }
        }
  
        // Set timeout options
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
  
        // Set references to certificate files
        curl_setopt( $ch, CURLOPT_CAINFO, __DIR__ . '/'.$caCertsFile );
  
        // Check the existence of a common name in the SSL peer's certificate
        // and also verify that it matches the hostname provided
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 1 );   
  
        // Verify the certificate of the SSL peer
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );
        
        // Build the parameters string to pass to PayWay
        $parametersString = '';
        $init = true;
        foreach ( $parameters as $paramName => $paramValue )
        {
            if ( $init )
            {
            		$init = false;
            }
            else
            {
                $parametersString = $parametersString . '&';
            }  
            $parametersString = $parametersString . urlencode($paramName) . '=' . urlencode($paramValue);
        }
        
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $parametersString );
        
       // debugLog( "Token Request POST: " . $parametersString );

        // Make the request  
        $responseText = curl_exec($ch);

        //debugLog( "Token Response: " . $responseText );

        // Check the response for errors
        $errorNumber = curl_errno( $ch );
        if ( $errorNumber != 0 )
        {
            trigger_error( "CURL Error getting token: Error Number: " . $errorNumber . 
                ", Description: '" . curl_error( $ch ) . "'" );
            echo "Errors: " . curl_error( $ch );
            //debugLog( "Errors: " . curl_error( $ch ) );
        }

        curl_close( $ch );

         // Split the response into parameters
        $responseParameterArray = explode( "&", $responseText );
        $responseParameters = array();
        foreach ( $responseParameterArray as $responseParameter )
        {
            list( $paramName, $paramValue ) = explode( "=", $responseParameter, 2 );
            $responseParameters[ $paramName ] = $paramValue;
        }

        if ( array_key_exists( 'error', $responseParameters ) )
        {
            trigger_error( "Error getting token: " . $responseParameters['error'] );
        }        
        else
        {
            return $responseParameters['token'];
        }
		
	}
	public static function pkcs5_unpad($text)
    {
        $pad = ord($text{strlen($text)-1});
        if ($pad > strlen($text)) return false;
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false;
        return substr($text, 0, -1 * $pad);
    }

    public static function decrypt_parameters( $base64Key, $encryptedParametersText, $signatureText )
    {
        $key = base64_decode( $base64Key );
        $iv = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
        $td = mcrypt_module_open('rijndael-128', '', 'cbc', '');

        // Decrypt the parameter text
        mcrypt_generic_init($td, $key, $iv);
        $parametersText = mdecrypt_generic($td, base64_decode( $encryptedParametersText ) );
        $parametersText = self::pkcs5_unpad( $parametersText );
        mcrypt_generic_deinit($td);

        // Decrypt the signature value
        mcrypt_generic_init($td, $key, $iv);
        $hash = mdecrypt_generic($td, base64_decode( $signatureText ) );
        $hash = bin2hex(self::pkcs5_unpad( $hash ) );
        mcrypt_generic_deinit($td);

        mcrypt_module_close($td);

        // Compute the MD5 hash of the parameters
        $computedHash = md5( $parametersText );

        // Check the provided MD5 hash against the computed one
        if ( $computedHash != $hash )
        {
            trigger_error( "Invalid parameters signature" );
        }

        $parameterArray = explode( "&", $parametersText );
        $parameters = array();

        // Loop through each parameter provided
        foreach ( $parameterArray as $parameter )
        {
            list( $paramName, $paramValue ) = explode( "=", $parameter );
            $parameters[ urldecode( $paramName ) ] = urldecode( $paramValue );
        }
        return $parameters;
    }
	
}

?>

