<?php
session_start();
require_once('tools.class.php');
require_once('accounting.class.php');
class Journal{
		public static function invoice($invoiceData,$isDebit = true){
			//print_r($invoiceData);
			$tools = new Tools; 
			$accounting = new Accounting; 
			$connection = new MongoClient();
			$db = $connection->db_system;

			$referenceID = $tools->getNextID("reference");

			$dt = new DateTime(date('Y-m-d'));
			$ts = $dt->getTimestamp();
			$today = new MongoDate($ts);

			$insertDataRef = array();
			$insertDataRef['referenceId'] = (int)$referenceID;
			$insertDataRef['invoiceId'] = (int)$invoiceData['invoiceId'];
			$insertDataRef['receiptId'] = 0;
			$insertDataRef['paymentId'] = 0;
			$insertDataRef['itemId'] = (int)$invoiceData['itemId'];
			$insertDataRef['bookingId'] = (int)$invoiceData['booking_id'];
			$insertDataRef['referenceType'] = "IN";
			$insertDataRef['description'] = $invoiceData['itemName'];
			$insertDataRef['created_datetime'] = $today;
			$insertDataRef['updated_datetime'] = $today;
			$insertDataRef['created_by'] = $_SESSION['sys_consultant_name'];


			$db->reference->insert($insertDataRef);

			//1-1214 => TLX Trade Debtors, Whsle/Trave
			$accounting->addEntry("1-1214",(float)$invoiceData['itemCostings']['totalDueAmt'] ,$isDebit,(int)$referenceID);
			//6-4001 Agents commission.
			$accounting->addEntry("6-4001",(float)$invoiceData['itemCostings']['totalLessAmt'] ,$isDebit,(int)$referenceID);

			//4-1703 TLX sales.
			$accounting->addEntry("4-1703",(float)$invoiceData['itemCostings']['grossAmt'] ,$isDebit,(int)$referenceID);


			$connection->close();
		}
		

	}
?>