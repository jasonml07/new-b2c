<?php
set_time_limit(0);
require_once('_supplier_config.php');

class RestelClient{
	public static function get_available_hotels($params) {
		if(RS_LIVE) {
			$XML_URL = RS_URL;
		} else {
			$XML_URL = RS_URL_TEST;
		}

		$result = "";

		$datefrom = explode("-", $params->date_start);		# yyyy-mm-dd
		$dateto = explode("-", $params->date_end);
		$dateF = $datefrom[1] . "/" . $datefrom[2] . "/" . $datefrom[0];
		$dateT = $dateto[1] . "/" . $dateto[2] . "/" . $dateto[0];
		$xml = '<?xml version="1.0" encoding="ISO-8859-1" ?>';
		$xml .= '<!DOCTYPE peticion SYSTEM "http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd">';
		$xml .= '<peticion>';
		$xml .= '<tipo>110</tipo>';
		$xml .= '<parametros>';
		$xml .= '<hotel>' . $params->product_code . '#</hotel>';
		$xml .= '<pais>' . substr($params->destination_code, 0, 2) . '</pais>';
		$xml .= '<provincia>' . $params->destination_code . '</provincia>';
		$xml .= '<radio>9</radio>';
		$xml .= '<fechaentrada>' . $dateF . '</fechaentrada>';
		$xml .= '<fechasalida>' . $dateT .'</fechasalida>';
		$xml .= '<marca></marca>';
		$xml .= '<afiliacion>' . RS_AFILIACIO . '</afiliacion>';
		$xml .= '<usuario>' . RS_CODUSU . '</usuario>';
		$xml .= '<comprimido>2</comprimido>';

		$romCtr = 1;
		foreach($params->pax as $key=>$value){
			$ctrAdult = 0;
			for($x=0;$x<$value->adult;$x++){
				$ctrAdult++;
			}
			$ctrChild = 0;
			for($x=0;$x<$value->child;$x++){
				$ctrChild++;
			}
			$xml .= '<numhab' . $romCtr . '>1</numhab' . $romCtr . '>';
			$xml .= '<paxes' . $romCtr . '>' . $ctrAdult . '-' . $ctrChild . '</paxes' . $romCtr . '>';
			$romCtr++;
		}

		$xml .= '<restricciones>0</restricciones>';
		$xml .= '<idioma>2</idioma>';
		$xml .= '<duplicidad>0</duplicidad>';
		$xml .= '<informacion_hotel>1</informacion_hotel>';
		$xml .= '</parametros>';
		$xml .= '</peticion>';
		$xml_encoded = urlencode($xml);
		$rs_hotel = $XML_URL . 'codigousu=' . RS_CODIGOUSU . '&clausu=' . RS_CLAUSU . '&afiliacio=' . RS_AFILIACIO . '&secacc=' . RS_SECACC . '&xml=' . $xml_encoded;
		$reader = new XMLReader();
		$response = [];

		$destination_code = $params->destination_code;
		$connection = new MongoClient();
		$db = $connection->db_system;

		$desDataCollection = $db->rs_destination->find(array("DestinationCode"=> $destination_code));
		$dataArray = iterator_to_array($desDataCollection);
		$country = "";
		foreach($dataArray as $key => $row){
			$country =  $row["CountryName"];
			break;
		}

		$typeRoom = [];
		$typeRoom['RO'] = "Room Only";
		$typeRoom['OB'] = "Room Only";
		$typeRoom['SA'] = "Room Only";
		$typeRoom['BB'] = "Bed and Breakfast";
		$typeRoom['AD'] = "Bed and Breakfast";
		$typeRoom['HB'] = "Half Board";
		$typeRoom['MP'] = "Half Board";
		$typeRoom['FB'] = "Full Board";
		$typeRoom['PC'] = "Full Board";
		$typeRoom['AI'] = "All Inclusive";
		$typeRoom['TI'] = "All Inclusive";
		$typeRoom["SC"] = "Self Catering";


		//load the selected XML file to the DOM
		$reader->open($rs_hotel);
		while ($reader->read()):
		 
			if ($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'hot'){
				$node = new SimpleXMLElement($reader->readOuterXML());
				$hotel = [];
				$cod = $node->cod->__toString();
				$hotel["HotelCode"] = $cod;
			 	$nom = $node->nom->__toString();
			 	$hotel["HotelName"] = $nom;
			 	$hotel["Country"] = $country;
			 	$pob = $node->pob->__toString();
			 	$hotel["City"] = $pob;
			 	$dir = $node->dir->__toString();
			 	$hotel["HotelAddress"] = $dir;
			 	$cat = $node->cat->__toString();
			 	$hotel["StarRating"] = $cat;
			 	$desc = $node->desc->__toString();
			 	$hotel["HotelInformation"] = [];
 				$hotel["HotelInformation"]["HotelDescription"] = $desc;
			 	$foto = $node->foto->__toString();
			 	$hotel["HotelImages"] = [$foto];
			 	$lat = $node->lat->__toString();
			 	$hotel["HotelLatitude"] = $lat;
			 	$lon = $node->lon->__toString();
			 	$hotel["HotelLongitude"] = $lon;

			 	$hotel["roomsAvailable"] = [];

			 	$res = $node->res;
			 	$paxes = $res->pax;
			 	$totalAmt = 0;
			 	$btCode = "";
			 	$currencyExc = $db->currency_ex->findOne(array("curr_from"=> "EUR"), array("_id"=>0));
			 	$euroToAUD = (float)$currencyExc["rate_to"];
			 	foreach($paxes as $pax) {
			 		$roomDetails = [];
			 		$btCode = "";
			 		$rooms = [];
			 		$pax_attrib = $pax->attributes()->cod;
			 		$totalAmount = 0;
			 		foreach($pax->hab as $hab) {
			 			$totalAMT = (float)($hab->reg->attributes()->prr->__toString());
			 			$totalAmount += (float)$totalAMT;
			 			$currency = $hab->reg->attributes()->div->__toString();
						$btCode = $hab->reg->attributes()->cod->__toString();
						$roomCategory = $hab->attributes()->desc->__toString();
						if ($btCode == "OB" || $btCode == "SA") {
							$btCode = "RO";
						}
						else if ($btCode == "AD") {
							$btCode = "BB";
						}
						else if ($btCode == "MP") {
							$btCode = "HB";
						}
						else if ($btCode == "PC") {
							$btCode = "FB";
						}
						else if ($btCode == "TI") {
							$btCode = "AI";
						}
						if (array_key_exists($btCode, $typeRoom)) {
							$btCode = "SC";
						}
						
						$room = [];
						$room["roomCategory"] = $roomCategory;
						$room["totalRoomRate"] = (float)$totalAMT;
						$room["paxes"] = $pax_attrib;
						$rooms[] = $room;
			 		}
			 		$roomDetails["currTo"] = 'AUD';
					$roomDetails["convertedTotalPrice"] = $totalAmount*$euroToAUD;
			 		$roomDetails["processId"] = "";
				    $roomDetails["hotelCode"] = $params->product_code;
				    $roomDetails["totalPrice"] = $totalAmount;
				    $roomDetails["currency"] = "EUR";
				    $roomDetails["boardType"] = $typeRoom[$btCode];
				    $roomDetails["rooms"] = $rooms;
				    $hotel["roomsAvailable"][] = $roomDetails;
			 	}

				//push those values to an array for evaluation
				$response[] = $hotel;
			}
		 
		endwhile;
		$connection->close();
		return $response;
	}
}
?>

