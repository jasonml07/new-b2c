<?php

error_reporting(E_ALL ^ E_NOTICE);
class Tools{

	public function getNextID($collection) {
	    $connection = new MongoClient();
		$db = $connection->db_system;
		$id = 0;
		//echo $collection;
		
        //$res = $db->counters->findAndModify(array("query"=>array("_id"=>1),"update"=>array("$inc"=>array("seq"=>1)),"new"=>true));
		$res = $db->counters->findAndModify(
			array('_id'=>$collection),
			//array('$set' => array("seq"=>array('$inc'=>array('seq'=>1)))),
			array('$inc'=>array('seq'=>1)),
			array("seq"=>1,"_id"=>0),
			array("new"=>true)
			);
		//print_r($res);
		
		$connection->close();
	    return $res['seq'];
	}
	public function dateDifference($startDate, $endDate) { 
		$sYear = substr($startDate, 0,4); 
		$sMonth = substr($startDate, 4,2); 
		$sDay = substr($startDate, 6,2); 
        $startDate = strtotime($sYear.'-'.$sMonth.'-'.$sDay); 
        $eYear = substr($endDate, 0,4); 
		$eMonth = substr($endDate, 4,2); 
		$eDay = substr($endDate, 6,2); 
        $endDate = strtotime($eYear.'-'.$eMonth.'-'.$eDay); 

       
        $diff = abs($endDate -  $startDate);

		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));    
        return array("year"=>$years, "months"=>$months, "days"=>$days); 
    } 
	public function multidimensional_search($parents, $searched) { 
	  if (empty($searched) || empty($parents)) { 
	    return false; 
	  } 

	  foreach ($parents as $key => $value) { 
	    $exists = true; 
	    foreach ($searched as $skey => $svalue) { 
	      $exists = ($exists && IsSet($parents[$key][$skey]) && $parents[$key][$skey] == $svalue); 
	    } 
	    if($exists){ return $key; } 
	  } 

	  return false; 
	} 
	public function roomCombination($array_lists,$array_pax,$size){
		
		if($size==1){
			return $array_lists;
		}
		
		//get ALL possible Match Per Room
		$newCombination = array();

		for($x=0;$x<$size;$x++){

			$array_paxAdult = $array_pax[$x]->adult;
			$array_paxChild = $array_pax[$x]->child+$array_pax[$x]->infant;
			
			foreach($array_lists as $room){
				//$numAvailRoom = $room['HotelRoom']['@attributes']['availCount'];
				$numRoomAdult = (int)$room->HotelOccupancy->Occupancy->AdultCount;
				$numRoomChild = (int)$room->HotelOccupancy->Occupancy->ChildCount;
				$numSizeCheck = $x+1;
				if($array_paxAdult == $numRoomAdult && $array_paxChild == $numRoomChild){
					if(isset($newCombination[$x])){
						array_push($newCombination[$x],$room);
					}else{
						$newCombination[$x] = array($room);
					}
					
				}
				
			}
		}


		//get ALL possible Combinations
		$possibleArrayCombination = array();
		if($size==3){
			for($x=0;$x<count($newCombination[0]);$x++){
				for($y=0;$y<count($newCombination[1]);$y++){
					for($z=0;$z<count($newCombination[2]);$z++){
						$allGood = true;
						
						$possibleArrayCombination[] = array($newCombination[0][$x],$newCombination[1][$y],$newCombination[2][$z]);
					}

				}
			}

		}else{
			for($x=0;$x<count($newCombination[0]);$x++){
				for($y=0;$y<count($newCombination[1]);$y++){
					
					$possibleArrayCombination[] = array($newCombination[0][$x],$newCombination[1][$y]);
					
				}
			}
		}

		/*$finalArrayCombination = array();

		//finalizing Combination by validating number of rooms Available

		foreach($possibleArrayCombination as $combination){
			$isGood = true;
			//checking Count
			for($x=0;$x<$size;$x++){
				$numAvailRoom = $combination[$x]['HotelRoom']['@attributes']['availCount'];
				if($numAvailRoom>$size){
					$ctrCheck =0;
					for($y=0;$y<$size;$y++){
						if($y==$x){
							continue;
						}
						if($combination[$x] == $combination[$y]){
							$ctrCheck++;
						}
					}

				}
			}
			

			if($isGood){
				$finalArrayCombination[] = $combination;
			}
		}

		return $finalArrayCombination;*/
		return $possibleArrayCombination;

	}
	public function combonitaions($array_lists, $size, $combinations = array()) {

      # if it's the first iteration, the first set 
      # of combinations is the same as the set of characters
      if (empty($combinations)) {
          $combinations = $array_lists;
      }

      # we're done if we're at size 1
      if ($size == 1) {
          return $combinations;
      }

      # initialise array to put new values in
      $new_combinations = array();

      # loop through existing combinations and character set to create strings
      foreach ($combinations as $combinationKey=>$combination) {
          foreach ($array_lists as $array_listKey=>$array_list) {
              $new_combinations[] = $combination.$array_list;
          }
      }

      # call same function again for the next iteration
      return self::combonitaions($array_lists, $size - 1, $new_combinations);

  }
	public function codeGeneRatorAndChecker($collectionName,$code) {

	    $connection = new MongoClient();
		$db = $connection->db_system;

		$collection = $db->selectCollection($collectionName);
		$countRow = $collection->find(array($collectionName."_code"=>$code))->count();

		if($countRow>0){
			$code = self::generateRandomString(10);
			self::codeGeneRatorAndChecker($collectionName,$code);
		}
		return $code;
		
	}
	public function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	public function generateCode($text){
		$text = str_replace(" ","",strtoupper(substr($text, 0, 11)));
	
		return $text;
	}
	public function moneyFormat($amt){
	
		$val = number_format($amt,0,'.','');
				return $val.".00";
		
	}
	public function utf8ize($d) {
	    if (is_array($d)) {
	        foreach ($d as $k => $v) {
	            $d[$k] = self::utf8ize($v);
	        }
	    } else {
	        return utf8_encode($d);
	    }
	    return $d;
	}
	public function stringCleaner($des){
			
			// Strip HTML Tags
		$clear = strip_tags($des);
		// Clean up things like &amp;
		$clear = html_entity_decode($clear);
		// Strip out any url-encoded stuff
		$clear = urldecode($clear);
		// Replace non-AlNum characters with space
		$clear = preg_replace('/[^A-Za-z0-9]/', ' ', $clear);
		// Replace Multiple spaces with single space
		$clear = preg_replace('/ +/', ' ', $clear);
		// Trim the string of leading/trailing space
		$clear = trim($clear);
		return $clear;
		
	}
	public function XMLtoArray($XML)
	{
		$xml_array = array();
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $XML, $vals);
		xml_parser_free($xml_parser);
		// wyznaczamy tablice z powtarzajacymi sie tagami na tym samym poziomie
		$_tmp='';
		foreach ($vals as $xml_elem) {
			$x_tag=$xml_elem['tag'];
			$x_level=$xml_elem['level'];
			$x_type=$xml_elem['type'];
			if ($x_level!=1 && $x_type == 'close') {
				if (isset($multi_key[$x_tag][$x_level]))
					$multi_key[$x_tag][$x_level]=1;
				else
					$multi_key[$x_tag][$x_level]=0;
			}
			if ($x_level!=1 && $x_type == 'complete') {
				if ($_tmp==$x_tag)
					$multi_key[$x_tag][$x_level]=1;
				$_tmp=$x_tag;
			}
		}
		// jedziemy po tablicy
		foreach ($vals as $xml_elem) {
			$x_tag=$xml_elem['tag'];
			$x_level=$xml_elem['level'];
			$x_type=$xml_elem['type'];
			if ($x_type == 'open')
				$level[$x_level] = $x_tag;
			$start_level = 1;
			$php_stmt = '$xml_array';
			if ($x_type=='close' && $x_level!=1)
				$multi_key[$x_tag][$x_level]++;
			while ($start_level < $x_level) {
				$php_stmt .= '[$level['.$start_level.']]';
				if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
					$php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
				$start_level++;
			}
			$add='';
			if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
				if (!isset($multi_key2[$x_tag][$x_level]))
					$multi_key2[$x_tag][$x_level]=0;
				else
					$multi_key2[$x_tag][$x_level]++;
				$add='['.$multi_key2[$x_tag][$x_level].']';
			}
			if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
				if ($x_type == 'open')
					$php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
				else
					$php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
				eval($php_stmt_main);
			}
			if (array_key_exists('attributes', $xml_elem)) {
				if (isset($xml_elem['value'])) {
					$php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
					eval($php_stmt_main);
				}
				foreach ($xml_elem['attributes'] as $key=>$value) {
					$php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
					eval($php_stmt_att);
				}
			}
		}
		return $xml_array;
	}
	public function recursive_array_search($needle,$array) { 
		$ctr = 0;
		foreach($array as $arr) { 
		   if(in_array($needle, $arr)){
			return array(true, $ctr);
			break;
		   }
		   
			 $ctr++;
		} 
		return array(false,0); 
	}
	public function paganation($display_array, $page) {
        $show_per_page = 9;

        $page = $page < 1 ? 1 : $page;

        // start position in the $display_array
        // +1 is to account for total values.
        $start = ($page - 1) * ($show_per_page + 1);
        $offset = $show_per_page + 1;

        $outArray = array_slice($display_array, $start, $offset);

        return $outArray;
    }
	
	public function upload_image_resize($width, $height,$file,$dir_path){
		/* Get original image x y*/
		list($w, $h) = getimagesize($file['tmp_name']);
		/* calculate new image size with ratio */
		$ratio = max($width/$w, $height/$h);
		$h = ceil($height / $ratio);
		$x = ($w - $width / $ratio) / 2;
		$w = ceil($width / $ratio);
		/* new file name */
		$now   = new DateTime;
		$encrypt = $now->format( 'YmdHis' );
		$path = $dir_path.$encrypt.$file['name'];
		/* read binary data from image file */
		$imgString = file_get_contents($file['tmp_name']);
		/* create image from string */
		$image = imagecreatefromstring($imgString);
		$tmp = imagecreatetruecolor($width, $height);
		imagecopyresampled($tmp, $image,
		0, 0,
		$x, 0,
		$width, $height,
		$w, $h);
		/* Save image */
		switch ($file['type']) {
			case 'image/jpeg':
				imagejpeg($tmp, $path, 100);
				break;
			case 'image/png':
				imagepng($tmp, $path, 0);
				break;
			case 'image/gif':
				imagegif($tmp, $path);
				break;
			default:
				exit;
				break;
		}
		return $encrypt.$file['name'];
		/* cleanup memory */
		imagedestroy($image);
		imagedestroy($tmp);
	}
	
}

?>