<?php

include("../_includes/mpdf/mpdf.php");
require_once('tools.class.php');
class Document{
	
	public function generateDocs($content,$title,$fileName,$type,$code){
	$html = '
		<html>

		<head>
		</head>
		<body>
		<div class="container">
				<div class="header">'.$title.'</div>
				<div>
		            <div class="logo">
		              <img style="height:50px;" src="../images/travelrez-logo.jpg" alt="logo" />
		            </div>
				
				</div>
			 '.$content.'

		</div>
		</body>
		</html>

		';
		
	$mpdf=new mPDF('c','A4', 11, 'Georgia, Sans', 5, 5, 5, 15,10,10);
	$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2015</span></div>');
	$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2015</span></div>','E');
	$stylesheet = file_get_contents('../lib/assets/css/pdf-style.css');
	$mpdf->WriteHTML($stylesheet,1); 
	$mpdf->WriteHTML(utf8_encode($html));


	//$mpdf->Output();
	$mpdf->Output('../_documents/'.$fileName);
	
	$tools = new Tools; 
	$connection = new MongoClient();
	$db = $connection->db_system;
	$documentID = $tools->getNextID("document");

	$dt = new DateTime(date('Y-m-d'));
	$ts = $dt->getTimestamp();
	$today = new MongoDate($ts);

	$insertData = array();
	$insertData['text'] = $html;
	$insertData['created_datetime'] = $today;
	$insertData['filename'] = $fileName;
	$insertData['type'] = $type;
	$insertData['code'] = $code;
	$insertData['documentId'] = $documentID;

	$db->documents->insert($insertData);

	return $documentID;
	}

	public function generateInvoiceDoc($itemdata,$bookingdata,$agencydata,$invoicedata){

	$tool = new Tools();
	$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
	$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
	$invoiceCreateDate =  date('d, M Y', $invoicedata['created_datetime']->sec);
	$content = '
		<div style="clear:both"></div>
		<div class="invoice-detail">
	
            <span class="invoice-date">
            	Invoice Date: <br> 
            	'.$invoiceCreateDate.'
            	<p class="address">
            		'.$agencydata["agency_name"].'<br>
					'.$agencydata["address"].', '.$agencydata['city'].', '.$agencydata['country'].'
					<br>
					Phone: (555) 555-5555
					<br>
					Business Fax:
				</p>
            </span> 

		 	  <div class="invoice-details">
		            <table id="meta">
		                <tr>
		                    <td class="meta-head">Invoice Reference #: </td>
		                    <td>'.$invoicedata['invoice_reference'].'</td>
		                </tr>
		                <tr>
		                    <td class="meta-head">Creation Date: </td>
		                    <td>'.$bookingCreatedDate.'</td>
		                </tr>
		                <tr>
							<td class="meta-head">Departure Date:</td>
		                    <td>'.$startDate.'</td>
		                </tr>
		                <tr>
		                    <td class="meta-head">Consultant:</td>
		                    <td><div>'.$bookingdata['booking_agency_consultant_name'].'</div></td>
		                </tr>

		            </table>
	          </div>

        </div>


		<table class="items">
		
		  <tr>
		      <th>Description</th>
		      <th>Date From</th>
		      <th>Date To</th>
		      <th>Gross Amt</th>
		      <th>Comm Amt</th>
		      <th>Disc Amt</th>
		      <th>Due Amt</th>
		  </tr>';
	$TgrossAmt = 0;
	$TnettAmt = 0;
	$TcommAmt = 0;
	$TdueAmt = 0;
	$TdiscAmt = 0;
	
	
	
    foreach($itemdata as $invoiceItem){   
	
		
		$grossAmt = (float) $invoiceItem['itemCostings']['grossAmt'];	
		$nettAmt = (float) $invoiceItem['itemCostings']['orginalNetAmt'];	
		$commAmt = (float) $invoiceItem['itemCostings']['commAmt'];		
		$dueAmt = (float) $invoiceItem['itemCostings']['totalDueAmt'];		
		$discAmt = (float) $invoiceItem['itemCostings']['discAmt'];		

		$TgrossAmt += (float)$grossAmt;
		$TnettAmt += (float)$nettAmt;
		$TcommAmt +=(float)$commAmt;
		$TdueAmt +=(float)$dueAmt;
		$TdiscAmt +=(float)$discAmt;
	  	$content .= '
	  		<tr class="item-row">
			    <td class="item-name">I#'.$invoiceItem['itemId'].'-'.$invoiceItem['itemName'].' | '.$invoiceItem['itemServiceName'].'<br>';

				foreach($invoiceItem['itemGuests'] as $guest){   	
					$content .= '<br><span style="font-size: 9px;"><b>Guests:</b><br>'.$guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'</span></td>' ;
						
				}	

				$content.='		
			      <td>'.$startDate =  date('d, M Y', $invoiceItem['itemStartDate']->sec).'</td>
			      <td>'.$startDate =  date('d, M Y', $invoiceItem['itemEndDate']->sec).'</td>
			      <td>$ '.$tool->moneyFormat($grossAmt).'</td>
			      <td><span >$ '.$tool->moneyFormat($commAmt).'</span></td>
			      <td>$ '.$tool->moneyFormat($discAmt).'</td>
			      <td>$ '.$tool->moneyFormat($dueAmt).'</td>
			</tr>';
	 }	 
			$content.= '
			  <tr class="item-row">
			  	  <td> <b>Invoice Total</b></td>
				  <td></td>
			  	  <td></td>
			  	  <td>$ '.$tool->moneyFormat($TgrossAmt).'</td>
			  	  <td>$ '.$tool->moneyFormat($TcommAmt).'</td>
			  	  <td>$ '.$tool->moneyFormat($TdiscAmt).'</td>
			  	  <td>$ '.$tool->moneyFormat($TdueAmt).'</td>
			  </tr>
			
			</table>
			';
	 	 
	$content .= '
		<div id="terms" >
		  <h5>HOW TO PAY</h5>
	

		 	<b>By Mail:</b>
			Please mail your cheque together with a copy of the invoice to Magic Tours International, PO Box 728, Surfers Paradise, QLD 4217
			<br><b>Please note:</b> All cheques should be made out to MAGICTOURS INTERNATIONAL

			<br>
			<b>Direct Deposit : </b>
			Payments can also be made by direct deposit into our company account. Please fax or e-mail a copy of the payment.

			<br>
			<b>Name:</b> Magic Tours International Pty. Ltd.
			Westpac Bank BSB: 034-215 ACCN: 462 080

			<br>
			<b>Credit Card: </b>
			Call 07 5526 2855 to pay via Bankcard, MasterCard, VISA. AMEX or Diners. Minimum payment $10.00.Maximum payment $10,000.00.


			<br>
			<b>Credit Card Fees:</b>
			Payments by credit card are subject to the following extra charges: Mastercard, Visa 1.8 %, AMEX, Diners 3.8 %
			Payments by cheque, direct deposit or in cash are not subject to the above charges!

			<br>
			<b>Enet:</b>
			Enet payments - 200141 Eastern Eurotours


		</div>';
	  
	 return $content;
	 
	 }

	 public function generateReceiptDoc($itemdata,$bookingdata,$agencydata,$receiptdata,$receiptRef){
		$tool = new Tools();
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
		$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
		$receiptCreateDate =  date('d, M Y');
		$content = '
		<div style="clear:both"></div>
				<div class="invoice-detail">
			
		            <span class="invoice-date">
		            	Receipt Date: <br> 
		            	'.$receiptCreateDate.'
		            	<p class="address">
		            		'.$agencydata["agency_name"].'<br>
							'.$agencydata["address"].', '.$agencydata['city'].', '.$agencydata['country'].'
							<br>
							Phone: (555) 555-5555
							<br>
							Business Fax:
						</p>
		            </span> 

					 	  <div class="invoice-details">
					            <table id="meta">
					                <tr>
					                    <td class="meta-head">Receipt Reference:#: </td>
					                    <td>'.$receiptRef.'</td>
					                </tr>
					                <tr>
					                    <td class="meta-head">Receipt Date:</td>
					                    <td>'.$receiptCreateDate.'</td>
					                </tr>
					                <tr>
										<td class="meta-head">Receipt Type:</td>
					                    <td>'.$receiptdata->receiptTypeName.'</td>
					                </tr>
					                <tr>
					                    <td class="meta-head">Total Amount Paid:</td>
					                    <td><div>'.$receiptdata->receiptDepositedAmout.'</div></td>
					                </tr>
					                <tr>
					                    <td class="meta-head">Refund Amount:</td>
					                    <td><div>'.$receiptdata->receiptRefundAmout.'</div></td>
					                </tr>

					            </table>
				          </div>

       			</div>

	<table class="items">
		
		  <tr>
		      <th>Description</th>
		      <th>Display amt </th>
		      <th>Natural Due amt</th>
		      <th>Amount Paid</th>

		  </tr>';
	$TdisplayAmount = 0;
	$TitemNaturalDueAmount = 0;
	$TamountPaid = 0;
	
	
	
	
    foreach($itemdata as $receiptItem){   
	
		
		$displayAmount = (float) $receiptItem->DisplayAmount;	
		$itemNaturalDueAmount = (float) $receiptItem->itemNaturalDueAmount;	
		$amountPaid = (float) $receiptItem->amountPaid;		
		
		$TdisplayAmount += (float)$displayAmount;
		$TitemNaturalDueAmount += (float)$itemNaturalDueAmount;
		$TamountPaid +=(float)$amountPaid;
		
	  	$content .= '
			<tr class="item-row">
				<td class="receipt-name">
				I#'.$receiptItem->itemID.'-'.$receiptItem->itemName.' | '.$receiptItem->itemService.'<br>';	

				foreach($receiptItem->itemGuest as $guest){   	
					$content .= '<br><span style="font-size: 9px;>'.$guest->guest_title.' '.$guest->guest_first_name.' '.$guest->guest_last_name.'</span>' ;
						
				}						

		 $content .= ' </td>
				<td><nobr>$ '.$tool->moneyFormat($displayAmount).'</nobr></td>
				<td><nobr>$ '.$tool->moneyFormat($itemNaturalDueAmount).'</nobr></td>
				<td><nobr>$ '.$tool->moneyFormat($amountPaid).'</nobr></td>
				
				
			</tr>
		';
	}
	$content .= '

				<tr class="item-row">
					<td>Receipt Total</td>
				    <td><nobr>$ '.$tool->moneyFormat($TdisplayAmount).'</nobr></td>
	                <td><nobr>$ '.$tool->moneyFormat($TitemNaturalDueAmount).'</nobr></td>
			        <td><nobr>$ '.$tool->moneyFormat($TamountPaid).'</nobr></td>
		
                </tr>

        </table>
	 
	 
	 </div>';
	 
	 
	 return $content;
	 
	 
	 }
	public function generateVoucherDoc($itemdata,$bookingdata,$agencydata,$supplierData,$voucherRef){
		$tool = new Tools();
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
		$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
		$voucherCreateDate =  date('d, M Y');
		$content = '
		<center>Note: This voucher must be presented at the time of check in / registration.</center>
		<table class="items">';
		 $content .= '
		  <tr>
		      <td colspan=4>Voucher To</td>
		    
		  </tr>';
		 $content .= '
		  <tr>
		      <td colspan=4>'.$itemdata['itemName'].'<br>
		     ';
		     $content .= 'From: <span>'.$itemdata['itemFromAddress'].', '.$itemdata['>itemFromCity'].', '.$itemdata['itemFromCountry'].'</span> ';
		    if($itemdata['itemToCity'] !="" && $itemdata['itemToCountry'] !=""){
		     $content .= ' <br>To: <span>'.$itemdata['itemToAddress'].', '.$itemdata['itemToCity'].', '.$itemdata['itemToCountry'].'</span> ';
		    }
		 $content .= ' </td>
		    
		  </tr>
		  ';

		  $content .= '
		  <tr>
		      <td colspan=4>Our Clients</td>
		    
		  </tr>';
		 $content .= '
		  <tr>';
		 $content .= '<td colspan=2>';
		 //print_r($itemdata);
		 foreach($itemdata['itemGuests'] as $guest){
		 	if((int)$guest['guest_is_lead'] ==1){
		 		 $content .= '<b>'.$guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'</b><br>';
		 		 break;
		 	}
		 }
		 $content .= '<b>Guests</b>';
		  foreach($itemdata['itemGuests'] as $guest){
		 	  $content .= '<b>'.$guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'</b><br>';
		 			
		 }

		 

		  $content .= '</td>';
		  $content .= '<td>';
		  $content .= '<b>Confirmation Reference:</b></br>';
		  $content .= '<b>Booking Reference:</b></br>';
		  $content .= '<b>Start Date:</b></br>';
		  $content .= '<b>End Date:</b></br>';
		  $content .= '</td >';
		  $content .= '<td>';
		  $content .= $itemdata['itemSuplierConfirmation']."</br>";
		  $content .= $voucherRef."</br>";
		  $content .= $itemdata['itemSuplierConfirmation']."</br>";
		  if($itemdata['itemStartDate']->sec){
		  	$content .=  date('d, M Y', $itemdata['itemStartDate']->sec)."</br>";
		  }else{
		  	$content .= "</br>";
		  }
		  if($itemdata->itemEndDate->sec){
		  	$content .=  date('d, M Y', $itemdata['itemEndDate']->sec)."</br>";
		  }else{
		  	$content .= "</br>";
		  }
		  
		
		  $content .= '</td>';
		    
		 
		    
		  $content .= '</tr> ';

		   $content .= '
		  <tr>
		      <td colspan=4>Please Provide The Following Prepaid Services</td>
		    
		  </tr>';
		  $content .= ' <tr>';
		   $content .= '<td colspan=2>';
		  $content .= '<b>Service Type:</b></br>';
		  $content .= '<b>Service Description:</b></br>';
		  $content .= '<b>Booked For:</b></br>';
		  $content .= '</td >';
		  $content .= '<td colspan=2>';
		  $content .= $itemdata['itemServiceName']."</br>";
		  $content .= $itemdata['itemName']." - ".$itemdata['itemInclusionDescription']."</br>";

		  $content .= "<br><b>Inclussions:</b></br>";
		  $content .= $itemdata['itemInclusionDescription']." ".count($itemdata['itemGuests'])." people</br>";
		  if($itemdata['itemExclusionDescription'] != ""){
		  	$content .= "<br><b>Exclussion:</b></br>";
		 	$content .= $itemdata['itemExclusionDescription']."</br>";
		  
		  }
		  if($itemdata['itemConditionsDescription'] != ""){
		  	$content .= "<br><b>Conditions:</b></br>";
		 	$content .= $itemdata['itemConditionsDescription']."</br>";
		  
		  }
		  $content .= '</td >';
		  $content .= '</tr> ';

		  $content .= '
		  <tr>
		      <td colspan=4>Description</td>
		    
		  </tr>';
		  $content .= '<tr> ';
		  $content .= '<td colspan=4>';
		  if($itemdata['itemSumarryDescription'] !=""){
		  	$content .= $itemdata['itemSumarryDescription'];

		  }else{
		  	 $content .= 'N/A';
		  }
		  $content .= '</td >';
		  $content .= '</tr> ';

		  $content .= '
		  <tr>
		      <td colspan=4>Service Provider: Please Forward Your Invoice To</td>
		    
		  </tr>';
		  $content .= '<tr> ';
		  $content .= '<td colspan=2>';
		  $content .= $itemdata['itemSupplierName'];
		   $content .= '</td >';
		    $content .= '<td colspan=2>';
		    $content .= "<br><b>Emergency:</b></br>";
		   // print_r($supplierData);
		    foreach($supplierData['contacts'] as $contacts){
		    	$content .= $contacts['phone_number'].";</br>";
		    }
		    
		   $content .= '</td >';
		  $content .= '</tr> ';

		 $content .= ' </table>';
		 return $content;
	}
}


?>