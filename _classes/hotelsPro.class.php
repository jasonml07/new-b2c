<?php
set_time_limit(0);
require_once('_supplier_config.php');


class HotelsProClient{
	
	
	public static function get_available_hotels($params){
		if(HP_LIVE){
			$JSON_URL = HP_URL;
		}else{
			$JSON_URL = HP_URL_TEST;
		}
		
		$result = ""; 
		
		
		$fields = array(
			'method' => "getAvailableHotel",
			'apiKey' => HP_API,
			'destinationId' => $params->desitnation_code,
			'checkIn' => $params->date_start,
			'checkOut' => $params->date_end,
			'clientNationality' => HP_NATIONALITY,
			'onRequest' => HP_AUTOREQUEST
			
		);
		
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= urlencode($key).'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');
		
		$str_room = "";
		
		foreach($params->pax as $key=>$value){
			
			$ctrPax = 0;
			for($x=0;$x<$value->adult;$x++){
				$str_room .= "&rooms[".$key."][".$ctrPax."][paxType]=Adult";
				$ctrPax++;
			}
			$ctrChildAge = 0;
			for($x=0;$x<$value->child;$x++){
				$str_room .= "&rooms[".$key."][".$ctrPax."][paxType]=Child";
				$str_room .= "&rooms[".$key."][".$ctrPax."][age]=".$value->childAges[$ctrChildAge];
				$ctrPax++;
				$ctrChildAge++;
			}
			for($x=0;$x<$value->infant;$x++){
				$str_room .= "&rooms[".$key."][".$ctrPax."][paxType]=Child";
				$str_room .= "&rooms[".$key."][".$ctrPax."][age]=1";
				$ctrPax++;
			}
		}

		
		
		
		$paramsString = $fields_string.$str_room;	
		$otherFilter = "";
	
		$otherFilter = "";
		$filterCounter = 0;
		/*if($filterStarRating !=""){
			$otherFilter .= "&filters[$filterCounter][filterType]=hotelStar&filters[$filterCounter][filterValue]=".$filterStarRating;
			$filterCounter++;
		}*/
		//print_r($params);
		if($params->product_code !=""){
			//echo $params->product_code;
			$otherFilter .= "&filters[$filterCounter][filterType]=hotelCode&filters[$filterCounter][filterValue]=".$params->product_code;
			$filterCounter++;
			
		}
		
		/*if($filterBoardType !=""){
			//print_r($board_config);
			$code = $board_config['HP'][$filterBoardType];
			$otherFilter .= "&filters[$filterCounter][filterType]=boardType&filters[$filterCounter][filterValue]=".$code;
			$filterCounter++;
			
		}*/
		//$otherFilter .= "&filters[$filterCounter][filterType]=resultLimit&filters[$filterCounter][filterValue]=1000";
			
		
		$temp_results = array();
		$url_hotels = $JSON_URL.$paramsString.$otherFilter;
		
		/*$ch = curl_init();

	    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url_hotels);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);       

	    $results = curl_exec($ch);
	    curl_close($ch);*/
		
		
		$result = file_get_contents($url_hotels, 0);
		
		if($result!=""){
			
			$hotels = json_decode($result);
			#print_r($hotels);
			
			
				
			return $hotels;
					
		}else{
			return;
		}
		
	}
	public static function get_allocated_hotel($search_id,$product_code){

		if(HP_LIVE){
			$JSON_URL = HP_URL;
		}else{
			$JSON_URL = HP_URL_TEST;
		}

		
		$result = ""; 
		
		
		$fields = array(
			'method' => "allocateHotelCode",
			'apiKey' => HP_API,
			'searchId' => $search_id,
			'hotelCode' => $product_code
			
		);

		
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= urlencode($key).'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');
		$params = $fields_string;
		$url_hotels = $JSON_URL.$params;
		
		$result = file_get_contents($url_hotels, 0);
		if($result!=""){
			
			$hotels = json_decode($result);
			#print_r($hotels);
			
			
				
			return $hotels;
					
		}else{
			return;
		}
		
	}
	public static function get_cancellation_policy($process_id){
		
		if(HP_LIVE){
			$JSON_URL = HP_URL;
		}else{
			$JSON_URL = HP_URL_TEST;
		}

		
		$result = ""; 
		
		$tempP = explode("-", $process_id);
		if($tempP[0]!='XI'){
			$process_id = "XI-".$process_id;
		}
		$fields = array(
			'method' => "getHotelCancellationPolicy",
			'apiKey' => HP_API,
			'trackingId' => $process_id
			
		);


		
		
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= urlencode($key).'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');
		$params = $fields_string;
		$url_hotels = $JSON_URL.$params;
		//echo $url_hotels;
		$result = file_get_contents($url_hotels, 0);
		if($result!=""){
			
			$hotelsCancellation = json_decode($result);
			#print_r($hotels);
			
			
				
			return $hotelsCancellation;
					
		}else{
			return;
		}	

	}
	
	public static function makeBooking($items_pax,$process_id){
	
		if(HP_LIVE){
			$JSON_URL = HP_URL;
		}else{
			$JSON_URL = HP_URL_TEST;
		}
		/*
		&agencyReferenceNumber=&leadTravellerInfo[paxInfo][paxType]=Adult&leadTravellerInfo[paxInfo][title]=Mr&leadTravellerInfo[paxInfo][firstName]=John&leadTravellerInfo[paxInfo][lastName]=DOE&leadTravellerInfo[nationality]=UK&otherTravellerInfo[0][title]=Mr&otherTravellerInfo[0][firstName]=Ahmetr&otherTravellerInfo[0][lastName]=AY&otherTravellerInfo[1][title]=Mr&otherTravellerInfo[1][firstName]=Mehmet&otherTravellerInfo[1][lastName]=YILDIZ&preferences=nonSmoking&note=test%20note
		*/
		$result = ""; 
		$fields = array(
			'method' => "makeHotelBooking",
			'apiKey' => HP_API,
			'processId' => $process_id
			
		);
		
		
		
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= urlencode($key).'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');
		$params = $fields_string;
		$isFirst = true;
		$strUrl = "";
		$ctrLead = 0;
		$ctr =0;
		foreach($items_pax as $item_pax){
			$title = $item_pax->title;
			switch($title){
				case 'Prof': {
					$title = 'Mr';
					break;
				}
				case 'Engr': {
					$title = 'Mr';
					break;
				}
				case 'Dr': {
					$title = 'Mr';
					break;
				}
			}
			if($isFirst){
				$isFirst = false;
				$strUrl .="&leadTravellerInfo[paxInfo][paxType]=Adult";
				$strUrl .="&leadTravellerInfo[paxInfo][title]=".urlencode($title);
				$strUrl .="&leadTravellerInfo[paxInfo][firstName]=".urlencode($item_pax->fname);
				$strUrl .="&leadTravellerInfo[paxInfo][lastName]=".urlencode($item_pax->lname);
				$strUrl .="&leadTravellerInfo[nationality]=".HP_NATIONALITY;
			}else{
				$strUrl .="&otherTravellerInfo[$ctr][title]=".urlencode($title);
				$strUrl .="&otherTravellerInfo[$ctr][firstName]=".urlencode($item_pax->fname);
				$strUrl .="&otherTravellerInfo[$ctr][lastName]=".urlencode($item_pax->lname);
				$ctr++;
			}
			
		}
		$strUrl .= "&preferences=nonSmoking&note=".urlencode("Booked by Eastern Eurotours through HotelsPro");
		
		$url_hotels = $JSON_URL.$params.$strUrl;
		//echo $url_hotels;
		$result = file_get_contents($url_hotels, 0);
		$res = array();
		if($result!=""){
			
			$hotels = json_decode($result);
			#print_r($hotels);
			
			
				
			return $hotels;
					
		}else{
			return "";
		}
		
		

	}
	public static function set_cancel_item($process_id){
		if(HP_LIVE){
			$JSON_URL = HP_URL;
		}else{
			$JSON_URL = HP_URL_TEST;
		}
		/*
		&agencyReferenceNumber=&leadTravellerInfo[paxInfo][paxType]=Adult&leadTravellerInfo[paxInfo][title]=Mr&leadTravellerInfo[paxInfo][firstName]=John&leadTravellerInfo[paxInfo][lastName]=DOE&leadTravellerInfo[nationality]=UK&otherTravellerInfo[0][title]=Mr&otherTravellerInfo[0][firstName]=Ahmetr&otherTravellerInfo[0][lastName]=AY&otherTravellerInfo[1][title]=Mr&otherTravellerInfo[1][firstName]=Mehmet&otherTravellerInfo[1][lastName]=YILDIZ&preferences=nonSmoking&note=test%20note
		*/
		$result = ""; 
		$fields = array(
			'method' => "cancelHotelBooking",
			'apiKey' => HP_API,
			'trackingId' => $process_id
			
		);
		
		
		
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= urlencode($key).'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');
		$params = $fields_string;
		$url_hotels = $JSON_URL.$params;
		$result = file_get_contents($url_hotels, 0);
		$res = array();
		if($result!=""){
			
			$hotels = json_decode($result);
			#print_r($hotels);
			
			
				
			return $hotels;
					
		}else{
			return "";
		}
			

	}
}

?>

