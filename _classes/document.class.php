<?php
error_reporting(E_ALL ^ E_NOTICE);
include("../_includes/mpdf/mpdf.php");
require_once('tools.class.php');
class Document{
	
	public function generateDocs($content,$title,$fileName,$type,$code){
	$html = '
		<html>

		<head>
		</head>
		<body>
		<div class="container">
				<div class="header">'.$title.'</div>
				<div>
		            <div class="logo">
		              <img style="height:50px;" src="../images/travelrez-logo.jpg" alt="logo" />
		              <img style="height:50px;" src="../images/logo.jpg" alt="logo" />
		            </div>
				
				</div>
			 '.$content.'

		</div>
		</body>
		</html>

		';
		
	$mpdf=new mPDF('c','A4', 11, 'Georgia, Sans', 5, 5, 5, 15,10,10);
	$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2015</span></div>');
	$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2015</span></div>','E');
	$stylesheet = file_get_contents('../lib/assets/css/pdf-style.css');
	$mpdf->WriteHTML($stylesheet,1); 
	$mpdf->WriteHTML(utf8_encode($html));


	//$mpdf->Output();
	$mpdf->Output('../../system_admin/_documents/'.$fileName);
	
	$tools = new Tools; 
	$connection = new MongoClient();
	$db = $connection->db_system;
	$documentID = $tools->getNextID("document");

	$dt = new DateTime(date('Y-m-d'));
	$ts = $dt->getTimestamp();
	$today = new MongoDate($ts);

	$insertData = array();
	$insertData['text'] = $html;
	$insertData['created_datetime'] = $today;
	$insertData['filename'] = $fileName;
	$insertData['type'] = $type;
	$insertData['code'] = $code;
	$insertData['documentId'] = $documentID;

	$db->documents->insert($insertData);

	return $documentID;
	}

	public function generateInvoiceDoc($itemdata,$bookingdata,$agencydata,$invoicedata){

	$tool = new Tools();
	if($bookingdata['booking_services_start_date']->sec){
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
	}else{
		$startDate =  "NA";
	}
	
	$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
	$invoiceCreateDate =  date('d, M Y', $invoicedata['created_datetime']->sec);
	$content = '
		<div style="clear:both"></div>
		<div class="invoice-detail">
	
            <span class="invoice-date">
            	Invoice Date: <br> 
            	'.$invoiceCreateDate.'
            	<p class="address">
            		'.$agencydata["agency_name"].'<br>
					<span style="font-size:10px;">'.$agencydata["address"].', <br>'.$agencydata['city'].', '.$agencydata['country'].', '.$agencydata['postCode'].'
					<br><br>
					Phone: '.$agencydata["phone"].'<br>
					Fax: '.$agencydata["fax"].'
					</span>
					
				</p>
            </span> 

		 	  <div class="invoice-details">
		            <table id="meta">
		                <tr>
		                    <td class="meta-head">Invoice Reference #: </td>
		                    <td>'.$invoicedata['invoice_reference'].'</td>
		                </tr>
		                <tr>
		                    <td class="meta-head">Creation Date: </td>
		                    <td>'.$bookingCreatedDate.'</td>
		                </tr>
		                
		                <tr>
		                    <td class="meta-head">Consultant:</td>
		                    <td><div>'.$bookingdata['booking_agency_consultant_name'].'</div></td>
		                </tr>

		            </table>
	          </div>

        </div>


		<table class="items">
		
		  <tr class="table-head">
		      <th width="10%">Description</th>
		      <th width="10%">Date From</th>
		      <th width="10%">Date To</th>
		      <th width="10%">Commission Amount</th>
		      <th width="10%">Discount Amount</th>
		      <th width="10%">Due Amount</th>
		  </tr>';
	$TgrossAmount = 0;
	$TnettAmount = 0;
	$TcommAmount = 0;
	$TdueAmount = 0;
	$TdiscAmount = 0;
	
	
	
    foreach($itemdata as $invoiceItem){   
	
		
		$grossAmount = (float) $invoiceItem['itemCostings']['grossAmt'];	
		$nettAmount = (float) $invoiceItem['itemCostings']['orginalNetAmt'];	
		$commAmount = (float) $invoiceItem['itemCostings']['commAmt'];		
		$dueAmount = (float) $invoiceItem['itemCostings']['totalDueAmt'];		
		$discAmount = (float) $invoiceItem['itemCostings']['discAmt'];		

		$TgrossAmount += (float)$grossAmount;
		$TnettAmount += (float)$nettAmount;
		$TcommAmount +=(float)$commAmount;
		$TdueAmount +=(float)$dueAmount;
		$TdiscAmount +=(float)$discAmount;
	  	$content .= '
	  		<tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			    <td style="width: 175px ; font-size: 12px;vertical-align: top;">'.$invoiceItem['itemName'].'<br><span style="font-size: 9px;"><b>'.$invoiceItem['itemServiceName'].'</b><br>'.$invoiceItem['itemInclusionDescription'].'</span>';
			    $content .= '<br><br><span style="font-size: 9px;"><b>Guests:</b><br></span>';
				foreach($invoiceItem['itemGuests'] as $guest){   	
					$content .= '<span style="font-size: 9px;">'.$guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'</span><br>' ;
						
				}	
				$content .= '</td>';
				 if($invoiceItem['itemStartDate'] != "" && $invoiceItem['itemStartDate'] != Null){

				 	if($invoiceItem['itemServiceName'] != "Service Fee"){
			    		$content.='	<td style="vertical-align: top; font-size: 12px;">'.$startDate =  date('d, M Y', $invoiceItem['itemStartDate']->sec).'</td>';
			    	}else{
			    		$content.='	<td style="vertical-align: top; font-size: 12px;"></td>';
			    	}
			    }else{
			    	$content.='	<td style="vertical-align: top; font-size: 12px;"></td>';
			    }
				
				if($invoiceItem['itemEndDate'] != "" && $invoiceItem['itemEndDate'] != Null){
					if($invoiceItem['itemServiceName'] != "Service Fee"){
			    		$content.='  <td style="vertical-align: top; font-size: 12px;">'.$endDate =  date('d, M Y', $invoiceItem['itemEndDate']->sec).'</td>';
			    	}else{
			    		$content.='	<td style="vertical-align: top; font-size: 12px;"></td>';
			    	}
			    }else{
			    	$content.='	<td></td>';
			    }

			    

			     $content.='
			      <td style="vertical-align: top; font-size: 12px;"><span >$ '.$tool->moneyFormat($commAmount).'</span></td>
			      <td style="vertical-align: top; font-size: 12px;">$ '.$tool->moneyFormat($discAmount).'</td>
			      <td style="vertical-align: top; font-size: 12px;">$ '.$tool->moneyFormat($dueAmount).'</td>
			</tr>';
	 }	 
			$content.= '
			  <tr style="border-style: solid;border-top-width: 1px;">
			  	  <td> <b>INVOICE TOTAL</b></td>
				  <td></td>
			  	  <td></td>
			  	  <td><b>$ '.$tool->moneyFormat($TcommAmount).'</b></td>
			  	  <td><b>$ '.$tool->moneyFormat($TdiscAmount).'</b></td>
			  	  <td><b>$ '.$tool->moneyFormat($TdueAmount).'</b></td>
			  </tr>
			
			</table>
			';
	 	 
	$content .= '
		<div id="terms" style="font-size: 10px;>
		  <h5>HOW TO PAY</h5>
	

		 	<b>By Mail:</b>
			Please mail your cheque together with a copy of the invoice to Magic Tours International, PO Box 728, Surfers Paradise, QLD 4217
			<br><b>Please note:</b> All cheques should be made out to MAGICTOURS INTERNATIONAL

			<br>
			<b>Direct Deposit : </b>
			Payments can also be made by direct deposit into our company account. Please fax or e-mail a copy of the payment.

			<br>
			<b>Name:</b> Magic Tours International Pty. Ltd.
			Westpac Bank BSB: 034-215 ACCN: 462 080

			<br>
			<b>Credit Card: </b>
			Call 07 5526 2855 to pay via Bankcard, MasterCard, VISA. AMEX or Diners. Minimum payment $10.00.Maximum payment $10,000.00.


			<br>
			<b>Credit Card Fees:</b>
			Payments by credit card are subject to the following extra charges: Mastercard, Visa 1.8 %, AMEX, Diners 3.8 %
			Payments by cheque, direct deposit or in cash are not subject to the above charges!

			<br>
			<b>Enet:</b>
			Enet payments - 200141 Eastern Eurotours


		</div>';
	  
	 return $content;
	 
	 }

	 public function generateCostingsDoc($itemdata,$bookingdata,$agencydata,$costingsdata,$totalAmountAvailable,$depositData){

	$tool = new Tools();
	if($bookingdata['booking_services_start_date']->sec){
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
	}else{
		$startDate =  "NA";
	}
	
	$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
	$costingsCreateDate =  date('d, M Y', $costingsdata['created_datetime']->sec);
	$content = '
		<div style="clear:both"></div>
		<div class="invoice-detail">
	
            <span class="invoice-date">
            	Costings Date: <br> 
            	'.$costingsCreateDate.'
            	<p class="address">
            		'.$agencydata["agency_name"].'<br>
					<span style="font-size:10px;">'.$agencydata["address"].', <br>'.$agencydata['city'].', '.$agencydata['country'].', '.$agencydata['postCode'].'
					<br><br>
					Phone: '.$agencydata["phone"].'<br>
					Fax: '.$agencydata["fax"].'
					</span>
				</p>
            </span> 

		 	  <div class="invoice-details">
		            <table id="meta">
		                <tr>
		                    <td class="meta-head">Costings Reference #: </td>
		                    <td>'.$costingsdata['costings_reference'].'</td>
		                </tr>
		                <tr>
		                    <td class="meta-head">Creation Date: </td>
		                    <td>'.$bookingCreatedDate.'</td>
		                </tr>
		                
		                <tr>
		                    <td class="meta-head">Consultant:</td>
		                    <td><div>'.$bookingdata['booking_agency_consultant_name'].'</div></td>
		                </tr>

		            </table>
	          </div>

        </div>

        <br>
		<table class="items">
		
		  <tr class="table-head">
		      <th  style="font-size: 10px;">Description</th>
		      <th style="font-size: 10px;">Date From</th>
		      <th style="font-size: 10px;">Date To</th>
		      <th style="font-size: 10px;">Status</th>
		      <th style="font-size: 10px;">Client Due Amount</th>
		      <th style="font-size: 10px;">Commission Amount</th>
		      <th style="font-size: 10px;">Discount Amount</th>
		      <th style="font-size: 10px;">Due Amount</th>
		  </tr>';
	$TgrossAmount = 0;
	$TnettAmount = 0;
	$TcommAmount = 0;
	$TdueAmount = 0;
	$TdiscAmount = 0;
	
	$dueDate = "";
	$isFirst = true;
    foreach($itemdata as $costingsItem){   
	
		if($isFirst){
			$dueDate = date('Y-m-d H:i:s', $costingsItem['itemStartDate']->sec);
			$dueDatet = date('d, M Y',strtotime($dueDate . " -60 day"));

			$testDate = $costingsItem['itemStartDate']->sec;
			$isFirst = false;
		}else{
			if($testDate>$costingsItem['itemStartDate']->sec){
				$testDate = $costingsItem['itemStartDate']->sec;
				$dueDate = date('d, M Y', $costingsItem['itemStartDate']->sec);
				$dueDatet = date('d, M Y',strtotime($dueDate . " -60 day"));
			}

		}
		$grossAmount = (float) $costingsItem['itemCostings']['grossAmt'];	
		$nettAmount = (float) $costingsItem['itemCostings']['orginalNetAmt'];	
		$commAmount = (float) $costingsItem['itemCostings']['commAmt'];		
		$dueAmount = (float) $costingsItem['itemCostings']['totalDueAmt'];		
		$discAmount = (float) $costingsItem['itemCostings']['discAmt'];		

		$TgrossAmount += (float)$grossAmount;
		$TnettAmount += (float)$nettAmount;
		$TcommAmount +=(float)$commAmount;
		$TdueAmount +=(float)$dueAmount;
		$TdiscAmount +=(float)$discAmount;

		if($costingsItem['itemServiceName'] != "Service Fee"){
			$content .= '
	  		<tr style="border-style: solid;border-bottom-width: 1px;font-size: 11px;">
			    <td style="width: 175px ; font-size: 12px;vertical-align: top;">'.$costingsItem['itemName'].'<br><span style="font-size: 9px;"><b>'.$costingsItem['itemServiceName'].'</b><br>'.$costingsItem['itemInclusionDescription'].'</span>';
			    
			    $content .= '<br><span style="font-size: 9px;"><b>Guests:</b><br></span>';
				foreach($costingsItem['itemGuests'] as $guest){   	
					$content .= '<span style="font-size: 9px;">'.$guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'</span><br>' ;
						
				}	
				$content .= '</td>';
			    if($costingsItem['itemStartDate'] != "" && $costingsItem['itemStartDate'] != Null){
			    	$content.='	<td style=" font-size: 11px;vertical-align: top;text-align: left; ">'.$startDate =  date('d, M Y', $costingsItem['itemStartDate']->sec);
			    	 $content .= '<br><br>'.$costingsItem['itemFromCity'].' '.$costingsItem['itemFromCountry'].'</span>';
			   
			    	$content .='</td>';
			    }else{
			    	$content.='	<td style=" font-size: 11px;vertical-align: top;text-align: left; ">';
			    	$content .= '<br><br>'.$costingsItem['itemFromCity'].' '.$costingsItem['itemFromCountry'].'</span>';
			   
			    	$content .='</td>';
			    }
				
				if($costingsItem['itemEndDate'] != "" && $costingsItem['itemEndDate'] != Null){
			    	$content.='  <td style=" font-size: 11px;vertical-align: top;text-align: left; ">'.$endDate =  date('d, M Y', $costingsItem['itemEndDate']->sec);
			    	if($costingsItem['itemToCity'] != $costingsItem['itemFromCity'] || $costingsItem['itemToCountry'] != $costingsItem['itemFromCountry']){
			    		$content .= '<br><br>'.$costingsItem['itemToCity'].' '.$costingsItem['itemToCountry'].'</span>';
			    	
			    	}
			    	$content .='</td>';
			    }else{
			    	$content.='	<td style=" font-size: 11px;vertical-align: top;text-align: left; ">'; 
			    	if($costingsItem['itemToCity'] != $costingsItem['itemFromCity'] || $costingsItem['itemToCountry'] != $costingsItem['itemFromCountry']){
			    		$content .= '<br><br>'.$costingsItem['itemToCity'].' '.$costingsItem['itemToCountry'].'</span>';
			    	
			    	}
			    	$content .='</td>';
			    }
			    $content .= '<td>';

				
				$str = "Quote";
				if(1 == (int)$costingsItem['itemIsCancelled']){
					$str = "Cancelled";

				}else if((int)$costingsItem['itemIsPaxAllocated']== 1 && (int)$costingsItem['itemIsConfirmed'] == 0 && (int)$costingsItem['itemIsCancelled'] == 0){
					$str = "Allocated";
				}
				else if((int)$costingsItem['itemIsPaxAllocated']  == 1 && (int)$costingsItem['itemIsConfirmed'] == 1 && (int)$costingsItem['itemIsCancelled'] == 0){
					$str = "Confirmed";
				}
				$content .= $str;

				$content .= '</td>';
			    

			     $content.='  <td style="font-size: 11px;vertical-align: top;text-align: left; " >$ '.$tool->moneyFormat($grossAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; " ><span >$ '.$tool->moneyFormat($commAmount).'</span></td>
			      <td style=" font-size: 11px;vertical-align: top;text-align: left; " >$ '.$tool->moneyFormat($discAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; " >$ '.$tool->moneyFormat($dueAmount).'</td>
			</tr>';
		}else{

			$content .= '
	  		<tr style="border-style: solid;border-bottom-width: 1px;font-size: 11px;">
			    <td style="width: 175px ; font-size: 11px;vertical-align: top;">I#'.$costingsItem['itemId'].'-'.$costingsItem['itemName'].' | FEE<br>';
			    
				

				$content.='		
			      <td></td>
			      <td></td>';
			      $content .= '<td>';

				
				$str = "Quote";
				if(1 == (int)$costingsItem['itemIsCancelled']){
					$str = "Cancelled";

				}else if((int)$costingsItem['itemIsPaxAllocated']== 1 && (int)$costingsItem['itemIsConfirmed'] == 0 && (int)$costingsItem['itemIsCancelled'] == 0){
					$str = "Allocated";
				}
				else if((int)$costingsItem['itemIsPaxAllocated']  == 1 && (int)$costingsItem['itemIsConfirmed'] == 1 && (int)$costingsItem['itemIsCancelled'] == 0){
					$str = "Confirmed";
				}
				$content .= $str;

				$content .= '</td>';

			     $content .= ' <td style="font-size: 11px;vertical-align: top;text-align: left; ">$ '.$tool->moneyFormat($grossAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; "><span >$ '.$tool->moneyFormat($commAmount).'</span></td>
			      <td style=" font-size: 11px;vertical-align: top;text-align: left; ">$ '.$tool->moneyFormat($discAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; ">$ '.$tool->moneyFormat($dueAmount).'</td>
			</tr>';

		}
	  	

	 }	 
			$content.= '
			  <tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			  	  <td style="font-size:12px;"> <b style="font-size:12px;">Costings Total</b></td>
				  <td></td>
			  	  <td></td>
			  	  <td></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left; "><b>$ '.$tool->moneyFormat($TgrossAmount).'</b></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left; "><b> $ '.$tool->moneyFormat($TcommAmount).'</b></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left; "><b>$ '.$tool->moneyFormat($TdiscAmount).'</b></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left;"><b>$ '.$tool->moneyFormat($TdueAmount).'</b></td>
			  </tr>';
			 $content.= '
			 <tr class="table-head">
		      <th colspan=8 style="text-align: left;font-size: 10px;">Costings Summary</th>
		    </tr>';

		  	$content.= '
			  <tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			  	  <td style="font-size:12px;"> <b style="font-size:12px;">Total Amount Due</b></td>
				  <td colspan=6></td>
			  	  <td style="font-size:12px;border-style: solid;border-left-width: 1px;"><b style="font-size:12px;">$ '.$tool->moneyFormat($TdueAmount).'</b></td>
			  </tr>';

			$content.= '
			  <tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			  	  <td style="font-size:12px;"> <b style="font-size:12px;">Amount Paid To Date</b></td>
				  <td colspan=6></td>
			  	  <td style="font-size:12px;border-style: solid;border-left-width: 1px;"><b style="font-size:12px;">$ '.$tool->moneyFormat($totalAmountAvailable).'</b></td>
			  </tr>';
			   $refund = $totalAmountAvailable-$TdueAmount;
			  if($refund<=0){
			  	$refund = 0;
			  }
			 $content.= '
			  <tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			  	  <td style="font-size:12px;"> <b style="font-size:12px;">Amount Refund</b></td>
				  <td colspan=6></td>
			  	  <td style="font-size:12px;border-style: solid;border-left-width: 1px;"><b style="font-size:12px;">$ '.$tool->moneyFormat($refund).'</b></td>
			  </tr>';

			  $dayDue = $TdueAmount-$totalAmountAvailable;
			  if($dayDue<=0){
			  	$dayDue = 0;
			  }
			  $content.= '
			  <tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			  	  <td style="font-size:12px;"> <b style="font-size:12px;">Deposit Due @'.$depositData[0].'</b></td>
				  <td colspan=6></td>
			  	  <td style="font-size:12px;border-style: solid;border-left-width: 1px;"><b style="font-size:12px;">$ '.$tool->moneyFormat($depositData[1]).'</b></td>
			  </tr>';
			 $content.= '
			  <tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			  	  <td style="font-size:12px;"> <b style="font-size:12px;">Balance Due @'.$dueDatet.'</b></td>
				  <td colspan=6></td>
			  	  <td style="font-size:12px;border-style: solid;border-left-width: 1px;"><b style="font-size:12px;">$ '.$tool->moneyFormat($dayDue).'</b></td>
			  </tr>';
			   

			$content.= '
			</table>
			';
	 	 

	$content .= '
		<div id="terms" style="font-size: 10px;" >

<h5>7 days Cancellation</h5>
Deposits should be paid within 7 days of confirmation or due date on invoice.  Automatic cancellation will occur unless prior arrangement made with your consultant.
 Please note: that a separate city tax will be charged to the customers upon arrival in many European Cities. This city tax is not included in the room rates and is an additional governmental charge which needs to be paid in the hotel. Children from 0-6 years are free of charge when sharing the existing bedding with the adults. 
<br><br>
www.easterneurotours.com.au
<br><br>
&bull; All prices are subject to change, based on availability and until paid in full.  <br>
&bull; A non-refundable deposit of $200.00 per person for hotels and 30 p/c per person for Escorted Tours and Cruising.<br>
&bull; Deposit is required within a week of confirmation. Payment is required upfront for all rail and ferry bookings.<br>
&bull; Prices quoted are for cash or cheque only - a surcharge will apply if paying by credit card.<br>
&bull; Please ensure all names are correct as per passport.<br>
&bull; Refer to our website or page 79 of our brochure for cancellation fees.<br>
&bull; Thank you for your booking and every booking is important to us.<br>
<br><br>

		  <h5>HOW TO PAY</h5>
	

		 	<b>By Mail:</b>
			Please mail your cheque together with a copy of the invoice to Magic Tours International, PO Box 728, Surfers Paradise, QLD 4217
			<br><b>Please note:</b> All cheques should be made out to MAGICTOURS INTERNATIONAL

			<br>
			<b>Direct Deposit : </b>
			Payments can also be made by direct deposit into our company account. Please fax or e-mail a copy of the payment.

			<br>
			<b>Name:</b> Magic Tours International Pty. Ltd.
			Westpac Bank BSB: 034-215 ACCN: 462 080

			<br>
			<b>Credit Card: </b>
			Call 07 5526 2855 to pay via Bankcard, MasterCard, VISA. AMEX or Diners. Minimum payment $10.00.Maximum payment $10,000.00.


			<br>
			<b>Credit Card Fees:</b>
			Payments by credit card are subject to the following extra charges: Mastercard, Visa 1.8 %, AMEX, Diners 3.8 %
			Payments by cheque, direct deposit or in cash are not subject to the above charges!

			<br>
			<b>Enet:</b>
			Enet payments - 200141 Eastern Eurotours


		</div>';
	  
	 return $content;
	 
	 }

	 public function generateQuoteDoc($itemdata,$bookingdata,$agencydata,$quotedata){

	$tool = new Tools();
	if($bookingdata['booking_services_start_date']->sec){
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
	}else{
		$startDate =  "NA";
	}
	
	$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
	$costingsCreateDate =  date('d, M Y', $quotedata['created_datetime']->sec);
	$content = '
		<div style="clear:both"></div>
		<div class="invoice-detail">
	
            <span class="invoice-date">
            	Quote Date: <br> 
            	'.$costingsCreateDate.'
            	<p class="address">
            		'.$agencydata["agency_name"].'<br>
					<span style="font-size:10px;">'.$agencydata["address"].', <br>'.$agencydata['city'].', '.$agencydata['country'].', '.$agencydata['postCode'].'
					<br><br>
					Phone: '.$agencydata["phone"].'<br>
					Fax: '.$agencydata["fax"].'
					</span>
				</p>
            </span> 

		 	  <div class="invoice-details">
		            <table id="meta">
		                <tr>
		                    <td class="meta-head">Quote Reference #: </td>
		                    <td>'.$quotedata['quote_reference'].'</td>
		                </tr>
		                <tr>
		                    <td class="meta-head">Creation Date: </td>
		                    <td>'.$bookingCreatedDate.'</td>
		                </tr>
		                
		                <tr>
		                    <td class="meta-head">Consultant:</td>
		                    <td><div>'.$bookingdata['booking_agency_consultant_name'].'</div></td>
		                </tr>

		            </table>
	          </div>

        </div>

        <br>
		<table class="items">
		
		  <tr class="table-head">
		      <th  style="font-size: 10px;">Description</th>
		      <th style="font-size: 10px;">Status</th>
		      <th style="font-size: 10px;">Date From</th>
		      <th style="font-size: 10px;">Date To</th>
		      <th style="font-size: 10px;">Client Due Amount</th>
		      <th style="font-size: 10px;">Commission Amount</th>
		      <th style="font-size: 10px;">Discount Amount</th>
		      <th style="font-size: 10px;">Due Amount</th>
		  </tr>';
	$TgrossAmount = 0;
	$TnettAmount = 0;
	$TcommAmount = 0;
	$TdueAmount = 0;
	$TdiscAmount = 0;
	
	$dueDate = "";
	$isFirst = true;
    foreach($itemdata as $quoteItem){   
	
		if($isFirst){
			$dueDate = date('Y-m-d H:i:s', $quoteItem['itemStartDate']->sec);
			$dueDatet = date('d, M Y',strtotime($dueDate . " -60 day"));

			$testDate = $quoteItem['itemStartDate']->sec;
			$isFirst = false;
		}else{
			if($testDate>$quoteItem['itemStartDate']->sec){
				$testDate = $quoteItem['itemStartDate']->sec;
				$dueDate = date('d, M Y', $quoteItem['itemStartDate']->sec);
				$dueDatet = date('d, M Y',strtotime($dueDate . " -60 day"));
			}

		}
		$grossAmount = (float) $quoteItem['itemCostings']['grossAmt'];	
		$nettAmount = (float) $quoteItem['itemCostings']['orginalNetAmt'];	
		$commAmount = (float) $quoteItem['itemCostings']['commAmt'];		
		$dueAmount = (float) $quoteItem['itemCostings']['totalDueAmt'];		
		$discAmount = (float) $quoteItem['itemCostings']['discAmt'];		

		$TgrossAmount += (float)$grossAmount;
		$TnettAmount += (float)$nettAmount;
		$TcommAmount +=(float)$commAmount;
		$TdueAmount +=(float)$dueAmount;
		$TdiscAmount +=(float)$discAmount;

		if($quoteItem['itemServiceName'] != "Service Fee"){
			$content .= '
	  		<tr style="border-style: solid;border-bottom-width: 1px;font-size: 11px;">
			    <td style="width: 175px ; font-size: 12px;vertical-align: top;">'.$quoteItem['itemName'].'<br><span style="font-size: 9px;"><b>'.$quoteItem['itemServiceName'].'</b><br>'.$quoteItem['itemInclusionDescription'].'</span><br>';
			    $numPax = (int)$quoteItem['itemAdultMin'] +(int)$quoteItem['itemChildMin'] +(int)$quoteItem['itemInfantMin'] ;
			    $content .= '<span style="font-size: 9px;"><b>Number of Pax:</b> '.$numPax.'</span>';
				$content .= '</td>';
				$content .= '<td>';

				/*
				var str = "Quote";
								if(1 ==Number(record.data.itemIsCancelled)){
									var str = "Cancelled";

								}else if(Number(record.data.itemIsPaxAllocated) == 1 && Number(record.data.itemIsConfirmed) == 0 && Number(record.data.itemIsCancelled) == 0){
									var str = "Allocated";
								}
								else if(Number(record.data.itemIsPaxAllocated) == 1 && Number(record.data.itemIsConfirmed) == 1 && Number(record.data.itemIsCancelled) == 0){
									var str = "Confirmed";
								}

				*/
				$str = "Quote";
				if(1 == (int)$quoteItem['itemIsCancelled']){
					$str = "Cancelled";

				}else if((int)$quoteItem['itemIsPaxAllocated']== 1 && (int)$quoteItem['itemIsConfirmed'] == 0 && (int)$quoteItem['itemIsCancelled'] == 0){
					$str = "Allocated";
				}
				else if((int)$quoteItem['itemIsPaxAllocated']  == 1 && (int)$quoteItem['itemIsConfirmed'] == 1 && (int)$quoteItem['itemIsCancelled'] == 0){
					$str = "Confirmed";
				}
				$content .= $str;

				$content .= '</td>';
			    if($quoteItem['itemStartDate'] != "" && $quoteItem['itemStartDate'] != Null){
			    	$content.='	<td style=" font-size: 11px;vertical-align: top;text-align: left; ">'.$startDate =  date('d, M Y', $quoteItem['itemStartDate']->sec);
			    	 $content .= '<br><br>'.$quoteItem['itemFromCity'].' '.$quoteItem['itemFromCountry'].'</span>';
			   
			    	$content .='</td>';
			    }else{
			    	$content.='	<td style=" font-size: 11px;vertical-align: top;text-align: left; ">';
			    	$content .= '<br><br>'.$quoteItem['itemFromCity'].' '.$quoteItem['itemFromCountry'].'</span>';
			   
			    	$content .='</td>';
			    }
				
				if($quoteItem['itemEndDate'] != "" && $quoteItem['itemEndDate'] != Null){
			    	$content.='  <td style=" font-size: 11px;vertical-align: top;text-align: left; ">'.$endDate =  date('d, M Y', $quoteItem['itemEndDate']->sec);
			    	//$content .= '<br><br>'.$costingsItem['itemToCity'].' '.$costingsItem['itemToCountry'].'</span>';
			    	$content .='</td>';
			    }else{
			    	$content.='	<td style=" font-size: 11px;vertical-align: top;text-align: left; ">'; 
			    	//$content .= '<br><br>'.$costingsItem['itemToCity'].' '.$costingsItem['itemToCountry'].'</span>';
			    	$content .='</td>';
			    }

			    

			     $content.='  <td style="font-size: 11px;vertical-align: top;text-align: left; " >$ '.$tool->moneyFormat($grossAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; " ><span >$ '.$tool->moneyFormat($commAmount).'</span></td>
			      <td style=" font-size: 11px;vertical-align: top;text-align: left; " >$ '.$tool->moneyFormat($discAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; " >$ '.$tool->moneyFormat($dueAmount).'</td>
			</tr>';
		}else{

			$content .= '
	  		<tr style="border-style: solid;border-bottom-width: 1px;font-size: 11px;">
			    <td style="width: 175px ; font-size: 11px;vertical-align: top;">I#'.$quoteItem['itemId'].'-'.$quoteItem['itemName'].' | FEE<br>';
			    
				$content .= '<td>';

				/*
				var str = "Quote";
								if(1 ==Number(record.data.itemIsCancelled)){
									var str = "Cancelled";

								}else if(Number(record.data.itemIsPaxAllocated) == 1 && Number(record.data.itemIsConfirmed) == 0 && Number(record.data.itemIsCancelled) == 0){
									var str = "Allocated";
								}
								else if(Number(record.data.itemIsPaxAllocated) == 1 && Number(record.data.itemIsConfirmed) == 1 && Number(record.data.itemIsCancelled) == 0){
									var str = "Confirmed";
								}

				*/
				$str = "Quote";
				if(1 == (int)$quoteItem['itemIsCancelled']){
					$str = "Cancelled";

				}else if((int)$quoteItem['itemIsPaxAllocated']== 1 && (int)$quoteItem['itemIsConfirmed'] == 0 && (int)$quoteItem['itemIsCancelled'] == 0){
					$str = "Allocated";
				}
				else if((int)$quoteItem['itemIsPaxAllocated']  == 1 && (int)$quoteItem['itemIsConfirmed'] == 1 && (int)$quoteItem['itemIsCancelled'] == 0){
					$str = "Confirmed";
				}
				$content .= $str;

				$content .= '</td>';

				$content.='		
			      <td></td>
			      <td></td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; ">$ '.$tool->moneyFormat($grossAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; "><span >$ '.$tool->moneyFormat($commAmount).'</span></td>
			      <td style=" font-size: 11px;vertical-align: top;text-align: left; ">$ '.$tool->moneyFormat($discAmount).'</td>
			      <td style="font-size: 11px;vertical-align: top;text-align: left; ">$ '.$tool->moneyFormat($dueAmount).'</td>
			</tr>';

		}
	  	

	 }	 
			$content.= '
			  <tr style="border-style: solid;border-bottom-width: 1px;font-size: 12px;">
			  	  <td style="font-size:12px;"> <b style="font-size:12px;">Quotation Total</b></td>
				  <td></td>
			  	  <td></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left; "><b>$ '.$tool->moneyFormat($TgrossAmount).'</b></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left; "><b> $ '.$tool->moneyFormat($TcommAmount).'</b></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left; "><b>$ '.$tool->moneyFormat($TdiscAmount).'</b></td>
			  	  <td style="font-size: 11px;vertical-align: top;text-align: left;"><b>$ '.$tool->moneyFormat($TdueAmount).'</b></td>
			  </tr>';
			 
			   

			$content.= '
			</table>
			';
	 	 

	$content .= '
		<div id="terms" style="font-size: 10px;" >

<h5>7 days Cancellation</h5>
Deposits should be paid within 7 days of confirmation or due date on invoice.  Automatic cancellation will occur unless prior arrangement made with your consultant.
 Please note: that a separate city tax will be charged to the customers upon arrival in many European Cities. This city tax is not included in the room rates and is an additional governmental charge which needs to be paid in the hotel. Children from 0-6 years are free of charge when sharing the existing bedding with the adults. 
<br><br>
www.easterneurotours.com.au
<br><br>
&bull; All prices are subject to change, based on availability and until paid in full.  <br>
&bull; A non-refundable deposit of $200.00 per person for hotels and 30 p/c per person for Escorted Tours and Cruising.<br>
&bull; Deposit is required within a week of confirmation. Payment is required upfront for all rail and ferry bookings.<br>
&bull; Prices quoted are for cash or cheque only - a surcharge will apply if paying by credit card.<br>
&bull; Please ensure all names are correct as per passport.<br>
&bull; Refer to our website or page 79 of our brochure for cancellation fees.<br>
&bull; Thank you for your booking and every booking is important to us.<br>
<br><br>

		  <h5>HOW TO PAY</h5>
	

		 	<b>By Mail:</b>
			Please mail your cheque together with a copy of the invoice to Magic Tours International, PO Box 728, Surfers Paradise, QLD 4217
			<br><b>Please note:</b> All cheques should be made out to MAGICTOURS INTERNATIONAL

			<br>
			<b>Direct Deposit : </b>
			Payments can also be made by direct deposit into our company account. Please fax or e-mail a copy of the payment.

			<br>
			<b>Name:</b> Magic Tours International Pty. Ltd.
			Westpac Bank BSB: 034-215 ACCN: 462 080

			<br>
			<b>Credit Card: </b>
			Call 07 5526 2855 to pay via Bankcard, MasterCard, VISA. AMEX or Diners. Minimum payment $10.00.Maximum payment $10,000.00.


			<br>
			<b>Credit Card Fees:</b>
			Payments by credit card are subject to the following extra charges: Mastercard, Visa 1.8 %, AMEX, Diners 3.8 %
			Payments by cheque, direct deposit or in cash are not subject to the above charges!

			<br>
			<b>Enet:</b>
			Enet payments - 200141 Eastern Eurotours


		</div>';
	  
	 return $content;
	 
	 }

	 public function generateReceiptDoc($itemdata,$bookingdata,$agencydata,$receiptdata,$receiptRef){
		$tool = new Tools();
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
		$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
		$receiptCreateDate =  date('d, M Y');
		$content = '
		<div style="clear:both"></div>
				<div class="invoice-detail">
			
		            <span class="invoice-date">
		            	Receipt Date: <br> 
		            	'.$receiptCreateDate.'
		            	<p class="address">
		            		'.$agencydata["agency_name"].'<br>
							<span style="font-size:10px;">'.$agencydata["address"].', <br>'.$agencydata['city'].', '.$agencydata['country'].', '.$agencydata['postCode'].'
							<br><br>
							Phone: '.$agencydata["phone"].'<br>
							Fax: '.$agencydata["fax"].'
							</span>
							
						</p>
		            </span> 

					 	  <div class="invoice-details">
					            <table id="meta">
					                <tr>
					                    <td class="meta-head">Receipt Reference:#: </td>
					                    <td>'.$receiptRef.'</td>
					                </tr>
					                <tr>
					                    <td class="meta-head">Receipt Date:</td>
					                    <td>'.$receiptCreateDate.'</td>
					                </tr>
					                <tr>
										<td class="meta-head">Receipt Type:</td>
					                    <td>'.$receiptdata->receiptTypeName.'</td>
					                </tr>
					                <tr>
					                    <td class="meta-head">Total Amount Paid:</td>
					                    <td><div>$'.$tool->moneyFormat($receiptdata->receiptDepositedAmout).'</div></td>
					                </tr>
					                <tr>
					                    <td class="meta-head">Refund Amount:</td>
					                    <td><div>$'.$tool->moneyFormat($receiptdata->receiptRefundAmout).'</div></td>
					                </tr>

					            </table>
				          </div>

       			</div>

	<table class="items">
		
		  <tr class="table-head">
		      <th>Description</th>
		      <th>Due Amount</th>
		      <th>Amount Paid</th>

		  </tr>';
	$TdisplayAmount = 0;
	$TitemNaturalDueAmount = 0;
	$TamountPaid = 0;
	
	
	//print_r($itemdata);
	
    foreach($itemdata as $receiptItem){   
	
		
		$displayAmount = (float) $receiptItem->DisplayAmount;	
		$itemNaturalDueAmount = (float) $receiptItem->itemNaturalDueAmount;	
		$amountPaid2 = (float) $receiptItem->amountPaid;		
		
		$TdisplayAmount += (float)$displayAmount;
		$TitemNaturalDueAmount += (float)$itemNaturalDueAmount;
		$TamountPaid +=(float)$amountPaid2;
		
	  	$content .= '
			<tr class="item-row">
				<td class="receipt-name">
				'.$receiptItem->itemName.' | '.$receiptItem->itemService.'<br>';	

				if(!empty($receiptItem->itemGuest)){
					foreach($receiptItem->itemGuest as $guest){   	
						$content .= '<br><span style="font-size: 9px;">'.$guest->guest_title.' '.$guest->guest_first_name.' '.$guest->guest_last_name.'</span>' ;
							
					}
				}
					
				
				
										

		 $content .= ' </td>
				<td valign="top"><nobr>$ '.$tool->moneyFormat($itemNaturalDueAmount).'</nobr></td>
				<td valign="top"> <nobr>$ '.$tool->moneyFormat($amountPaid2).'</nobr></td>
			</tr>
		';
	}

	$content .= '

				<tr class="item-row">
					<td><b>Receipt Total</b></td>
				    <td><nobr>$ '.$tool->moneyFormat($TitemNaturalDueAmount).'</nobr></td>
			        <td><nobr>$ '.$tool->moneyFormat($TamountPaid).'</nobr></td>
		
                </tr>

        </table>
	 
	 
	 </div>';
	 
	 
	 return $content;
	 
	 
	 }
	public function generateVoucherDoc($itemdata,$bookingdata,$agencydata,$supplierData,$voucherRef){
		$tool = new Tools();
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
		$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
		$voucherCreateDate =  date('d, M Y');
		$content = '
		<center>Note: This voucher must be presented at the time of check in / registration.</center>
		<table class="items">';
		 $content .= '
		  <tr>
		      <td colspan=4 class="voucher-header"><b>Voucher To</b></td>
		    
		  </tr>';
		 $content .= '
		  <tr class="voucher-row">
		      <td colspan=4>'.$itemdata['itemName'].'<br>

		     ';
		     $content .= ' <span>'.$itemdata['itemFromAddress'].', '.$itemdata['itemFromCity'].', '.$itemdata['itemFromCountry'].'</span> ';
		    if(($itemdata['itemToCity'] !="" && $itemdata['itemToCountry'] !="") || $itemdata['itemToAddress'] != "") {
		     $content .= ' <br><b>To:</b> <span>'.$itemdata['itemToAddress'].', '.$itemdata['itemToCity'].', '.$itemdata['itemToCountry'].'</span> ';
		    }

		    if($itemdata['itemPhone'] !=""){
		    	$content .= '  <br><br>

		 			Phone: '.$itemdata["itemPhone"].'<br>';
		    }

		 


		$content .= ' </td>
		    
		  </tr>
		  ';

		  $content .= '
		  <tr>
		      <td colspan=4 class="voucher-header"><b>Our Clients</b></td>
		    
		  </tr>';
		 $content .= '
		  <tr>';
		 $content .= '<td colspan=2>';
		 //print_r($itemdata);
		 foreach($itemdata['itemGuests'] as $guest){
		 	if((int)$guest['guest_is_lead'] ==1){
		 		 $content .= '<b>'.$guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'</b><br>';
		 		 break;
		 	}
		 }
		 $content .= '<br><b>Guests</b><br>';
		  foreach($itemdata['itemGuests'] as $guest){
		 	  $content .= $guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'<br>';
		 			
		 }

		 

		  $content .= '</td>';
		  $content .= '<td >';
		  $content .= '<table>';
			  $content .= '<tr>
			  		<td><b>Confirmation Reference:</b></td>
			  		<td>'.$itemdata['itemSuplierConfirmation'].'</td>
			  			  </tr>';
			  $content .= '<tr>

			  		<td><b>Booking Reference:</b></td>
			  		<td>'.$voucherRef.'</td>
			  			  </tr>';
			  $content .= '<tr>
			 	    <td><b>Start Date:</b></td>
			 	    <td>';
			 	    	if($itemdata['itemStartDate']->sec){
			  	$content .=  date('d, M Y', $itemdata['itemStartDate']->sec)." Time: ".$itemdata['itemStartTime']."</br>";
			  }
			 	$content .='</td>
			 			  </tr>';

			  $content .= '<tr>
			 	    <td><b>End Date:</b></td>
			 	    <td>';
			 	    	if($itemdata['itemEndDate']->sec){
			  	$content .=  date('d, M Y', $itemdata['itemEndDate']->sec)." Time: ".$itemdata['itemEndTime']."</br>";
			  }
			 	$content .='</td>
			 			  </tr>';
			  

		  $content .= '</table>';
		  $content .= '</td>';


		  $content .= '
		  <tr>
		  		 <td class="voucher-header" colspan=4><b>Please Provide The Following Prepaid Services:</b> </td>
		   </tr>';

		     $content .= ' <tr>';
		   $content .= '<td colspan=2>';
		  $content .= '<b>Service Type:</b><br>';
		  $content .= '<b>Service Description:</b><br><br>';
		  $content .= '<b>Booked For:</b><br>';
		  $content .= '</td >';
		  $content .= '<td colspan=2 style="">';
		  $content .= $itemdata['itemServiceName']."<br>";
		  $content .= $itemdata['itemName']." - ".$itemdata['itemInclusionDescription']."<br>";

		  $content .= "<b>Inclusions:</b><br>";
		  $content .= $itemdata['itemInclusionDescription']." ".count($itemdata['itemGuests'])." people<br>";
		  if($itemdata['itemExclusionDescription'] != ""){
		  	$content .= "<br><b>Exclusions:</b><br>";
		 	$content .= $itemdata['itemExclusionDescription']."<br>";
		  
		  }
		  if($itemdata['itemConditionsDescription'] != ""){
		  	$content .= "<br><b>Conditions:</b><br>";
		 	$content .= $itemdata['itemConditionsDescription']."<br>";
		  
		  }
		  $content .= '</td >';
		  $content .= '</tr> ';

		  $content .= '
		  <tr>
		      <td class="voucher-header" colspan=4><b>Description</b></td>
		   </tr>
		   <tr>
		      <td colspan="4" style="text-align: justify;">';

		  if($itemdata['itemSumarryDescription'] !=""){
		  	$content .= $itemdata['itemSumarryDescription'];

		  }else{
		  	 $content .= 'N/A';
		  }
		  $content .= '</td></tr>';



 			 $content .= '
		  <tr>
		      <td class="voucher-header" colspan=4><b>Service Provider<b></td>
		    
		  </tr>';
		  $content .= '<tr> ';
		  $content .= '<td colspan=2>';
		  $content .= $itemdata['itemSupplierName'];
		   $content .= '</td >';
		    $content .= '<td colspan=2>';
		    $content .= "<b>Emergency:</b><br>";

		     $content .= "Phone:  ".$supplierData['phone']."<br>";
		      $content .= "Fax:  ".$supplierData['fax']."<br>";
		      $content .= "Email:  ".$supplierData['email']."<br>";
		  
		   $content .= '</td >';
		  $content .= '</tr> ';
		  $content .= '</table>';
		 return $content;
	}

	public function generateItineraryDoc($itemdatas,$bookingdata,$agencydata,$voucherRef){
		$tool = new Tools();
		$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
		$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);
		$voucherCreateDate =  date('d, M Y');
		$content = '
		<table class="items">';
		 
		 foreach($itemdatas as $itemdata){
		  $content .= '<tr>
		      <td class="voucher-header" colspan=4>'.$itemdata['itemName'].'</td>
		   </tr>';
		 $content .= '
		  <tr class="voucher-row">
		      <td colspan=4>
		     ';
		     $content .= '<span>'.$itemdata['itemFromAddress'].', '.$itemdata['itemFromCity'].', '.$itemdata['itemFromCountry'].'</span> ';
		    if(($itemdata['itemToCity'] !="" && $itemdata['itemToCountry'] !="") || $itemdata['itemToAddress'] != "") {
		     $content .= ' <br><b>To:</b> <span>'.$itemdata['itemToAddress'].', '.$itemdata['itemToCity'].', '.$itemdata['itemToCountry'].'</span> ';
		    }
		 if($itemdata['itemPhone'] !=""){
		    	$content .= '  <br><br>

		 			Phone: '.$itemdata["itemPhone"].'<br>';
		    }

		 


		$content .= ' </td>
		    
		  </tr>
		  ';

		 
		 $content .= '
		  <tr>';
		 $content .= '<td colspan=2>';
		 //pr//int_r($itemdata);
		 foreach($itemdata['itemGuests'] as $guest){
		 	if((int)$guest['guest_is_lead'] ==1){
		 		 $content .= '<b>'.$guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'</b><br>';
		 		 break;
		 	}
		 }
		 $content .= '<br><b>Guests</b><br>';
		  foreach($itemdata['itemGuests'] as $guest){
		 	  $content .= $guest['guest_title'].' '.$guest['guest_first_name'].' '.$guest['guest_last_name'].'<br>';
		 			
		 }

		 

		  $content .= '</td>';
		  $content .= '<td>';
		  $content .= '<table>';
			 
			 
			  $content .= '<tr>
			 	    <td><b>Start Date:</b></td>
			 	    <td>';
			 	    	if($itemdata['itemStartDate']->sec){
			  	$content .=  date('d, M Y', $itemdata['itemStartDate']->sec)." Time: ".$itemdata['itemStartTime']."</br>";
			  }
			 	$content .='</td>
			 			  </tr>';

			  $content .= '<tr>
			 	    <td><b>End Date:</b></td>
			 	    <td>';
			 	    	if($itemdata['itemEndDate']->sec){
			  	$content .=  date('d, M Y', $itemdata['itemEndDate']->sec)." Time: ".$itemdata['itemEndTime']."</br>";
			  }
			 	$content .='</td>
			 			  </tr>';
			  

		  $content .= '</table>';

		  $content .= '</td>';


		  

		     $content .= ' <tr>';
		   $content .= '<td colspan=2>';
		  $content .= '<b>Service Type:</b><br>';
		  $content .= '<b>Service Description:</b><br><br>';
		  $content .= '<b>Booked For:</b><br>';
		  $content .= '</td >';
		  $content .= '<td colspan=2>';
		  $content .= $itemdata['itemServiceName']."<br>";
		  $content .= $itemdata['itemName']." - ".$itemdata['itemInclusionDescription']."<br>";

		  $content .= "<b>Inclusions:</b><br>";
		  $content .= $itemdata['itemInclusionDescription']." ".count($itemdata['itemGuests'])." people<br>";
		  if($itemdata['itemExclusionDescription'] != ""){
		  	$content .= "<br><b>Exclusions:</b><br>";
		 	$content .= $itemdata['itemExclusionDescription']."<br>";
		  
		  }
		  if($itemdata['itemConditionsDescription'] != ""){
		  	$content .= "<br><b>Conditions:</b><br>";
		 	$content .= $itemdata['itemConditionsDescription']."<br>";
		  
		  }
		  if($itemdata['item_supplier_reservation_req'] != ""){
		  	$content .= $itemdata['item_supplier_reservation_req']."<br>";
		  
		  }
		  $content .= '</td >';
		  $content .= '</tr> ';

		  $content .= '
		  <tr>
		      <td  colspan=4><hr><b>Description:</b></td>
		   </tr>
		   <tr>
		      <td colspan="4" style="text-align: justify;">';

		  if($itemdata['itemSumarryDescription'] !=""){
		  	$content .= $itemdata['itemSumarryDescription'];

		  }else{
		  	 $content .= 'N/A';
		  }
		  $content .= '</td></tr>';


		}
 			 
		  $content .= '</table>';
		 return $content;
	}
}


?>