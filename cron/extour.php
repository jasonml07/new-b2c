<?php
date_default_timezone_set('Australia/Melbourne');
session_start();
set_time_limit(0);
ini_set('memory_limit', '1024M');
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));

$connection = new MongoClient();
$db = $connection->db_system;



$curr = $db->currency_ex->findOne(array('curr_from'=>$_GET['curr']),array('_id'=>0));

$arrayResult = array();
foreach($_GET['basePrice'] as $key=>$val){


	$priceBase = (float)$val['basePrice'];
	$comPrice = $priceBase*((int)$_GET['commPct']/100);
	$lessNow = $priceBase-$comPrice;

	$convertedPrice = $lessNow*(float)$curr['rate_to'];

	//echo $convertedPrice."<br>";
	$markupPrice = $convertedPrice*((int)$_GET['markupPct']/100);
	//echo $markupPrice."<br>";
	$grossPrice = $convertedPrice+$markupPrice;
	$displayPrice = number_format(round($grossPrice), 2, '.', ',');

	$arrayResult[$key] = $displayPrice;
}



$_SESSION[$_GET['datetime']] =$arrayResult;

print json_encode($arrayResult);

$connection->close();


			

  	  		

?>