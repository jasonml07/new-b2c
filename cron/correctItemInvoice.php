<?php

require_once('../_classes/tools.class.php');

require_once('../_classes/journal.class.php');
$connection = new MongoClient();
$db = $connection->db_system;
$tools = new Tools();


$itemId = (int)$_GET['itemId'];

$whereData = array();
$whereData['itemId'] = $itemId;
$whereData['$or'] = array();
array_push($whereData['$or'],array("referenceType"=> "SC"));
array_push($whereData['$or'],array("referenceType"=> "IN"));
$paymentDataRes = $db->reference->find($whereData,array("_id"=>0));
$dataArray = iterator_to_array($paymentDataRes);


$refID =array();

foreach($dataArray as $key=>$row){
	array_push($refID, $row['referenceId']);
}

$db->reference->remove(array('referenceId'=>array("\$in"=>$refID)));
$db->journal->remove(array('referenceId'=>array("\$in"=>$refID)));

$today = new MongoDate(strtotime("now"));

$resultsItem = $db->items->find(array("itemId"=>$itemId));
$dataArray = iterator_to_array($resultsItem);
foreach($dataArray as $key2 => $row){
	if((int)$row2['supplement_qty'] >0){

		$row['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
		$row['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
		$row['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
		$row['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
		$row['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
		$row['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
		$row['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
		$row['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
		$row['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
		$row['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
		$row['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
			
	}
	if($row['itemServiceCode'] != "SFEE"){
		$journal = new Journal; 
		$journal->purchaseToSupplier($row);
	}
	$invoiceID = $tools->getNextID("invoice");

	$invoiceItem = array();
	$invoiceItem['invoiceId'] = (int)$invoiceID;
	$invoiceItem['itemId'] = (int)$row['itemId'];
	$invoiceItem['itemCode'] = $row['itemCode'];
	$invoiceItem['itemName'] = $row['itemName'];
	$invoiceItem['itemServiceCode'] = $row['itemServiceCode'];
	$invoiceItem['itemServiceName'] = $row['itemServiceName'];
	$invoiceItem['itemCostings'] = $row['itemCostings'];
	$invoiceItem['created_datetime'] = $today;

	$invoiceDATA = array();
	$invoiceDATA['itemCostings'] = $row['itemCostings'];
	$invoiceDATA['invoiceId'] = (int)$invoiceID;
	$invoiceDATA['itemId'] = (int)$row['itemId'];
	$invoiceDATA['itemName'] = $row['itemName'];
	$invoiceDATA['booking_id'] = (int)$row['bookingId'];

	$journal = new Journal; 
	$journal->invoice($invoiceDATA,false);


	//$db->items->update(array("itemId"=>(int)$row["itemId"]),array("\$set"=>array("itemIsReceipt"=>0)));
    
}

//print_r($itemIdsList);


$connection->close();
?>