<?php

require_once('../_classes/class.phpmailer.php');
$sender = "info@travelrez.net.au";
$addCC = array("info@travelrez.net.au",'chip@easterneurotours.com.au ','czarbaluran@travelrez.net.au','czarbaluran@gmail.com','sally@easterneurotours.com.au');

$connection = new MongoClient();
$db = $connection->db_system;


$resultsItem = $db->items->find(array("itemSupplierCode"=>"HBBEDLIVE","itemIsLive"=>1,"itemIsCancelled"=>0,"itemIsConfirmed"=>1,"itemIsReceipt"=>0));
$dataArray = iterator_to_array($resultsItem);

//echo date("Y-m-d");
$dStart = new DateTime(date("Y-m-d"));

$bookingsAgencData = array();

foreach($dataArray as $key=>$item){
	foreach($item["itemCancellation"] as $key2=>$cancellation){
		//echo date('Y-m-d', $cancellation['dateFrom']->sec);
		$dEnd  = new DateTime(date('Y-m-d', $cancellation['dateFrom']->sec));
		
		$dDiff = $dStart->diff($dEnd);
		$dateDIFFNUM = (int)$dDiff->days;
		if($dateDIFFNUM <=4){
		//if($dateDIFFNUM <=3){
			$bookingData = $db->booking->findOne(array("booking_id"=>(int)$item['bookingId']));
			$agencyData = $db->agency->findOne(array("agency_code"=>$bookingData['booking_agency_code']));
			if(!array_key_exists($agencyData['agency_code'], $bookingsAgencData)){
				$bookingsAgencData[$agencyData['agency_code']] = array();
				$bookingsAgencData[$agencyData['agency_code']]['bookings'] = array();
				$bookingsAgencData[$agencyData['agency_code']]["agencyEmails"] = array();
			}
			//print_r($agencyData);
			$bookingsAgencData[$agencyData['agency_code']]["agencyName"] = $bookingData['booking_agency_name'];
			
			if($agencyData['email'] != ""){
				if(!in_array($agencyData['email'], $bookingsAgencData[$agencyData['agency_code']]["agencyEmails"])){
					$agencyData['email'] = filter_var($agencyData['email'], FILTER_SANITIZE_EMAIL);
					if (!filter_var($agencyData['email'], FILTER_VALIDATE_EMAIL) === false) {
					   array_push($bookingsAgencData[$agencyData['agency_code']]["agencyEmails"],$agencyData['email']);
					}
					
				}
				
			}
			if($agencyData['b_email'] != ""){
				if(!in_array($agencyData['b_email'], $bookingsAgencData[$agencyData['agency_code']]["agencyEmails"])){
					$agencyData['b_email'] = filter_var($agencyData['b_email'], FILTER_SANITIZE_EMAIL);
					if (!filter_var($agencyData['b_email'], FILTER_VALIDATE_EMAIL) === false) {
					   array_push($bookingsAgencData[$agencyData['agency_code']]["agencyEmails"],$agencyData['b_email']);
					}
					
				}
			}

			$agencyConsultantData = $db->agency_consultants->find(array("agency_code"=>$bookingData['booking_agency_code']));
			$dataArrayCon = iterator_to_array($agencyConsultantData);
			foreach($dataArrayCon as $keyC=>$con){
				if(!in_array($con['username'], $bookingsAgencData[$agencyData['agency_code']]["agencyEmails"])){
					$con['username'] = filter_var($con['username'], FILTER_SANITIZE_EMAIL);
					if (!filter_var($con['username'], FILTER_VALIDATE_EMAIL) === false) {
					   array_push($bookingsAgencData[$agencyData['agency_code']]["agencyEmails"],$con['username']);
					}
					
				}
			}
			if(!array_key_exists((int)$item['bookingId'], $bookingsAgencData[$agencyData['agency_code']]['bookings'])){
				$bookingsAgencData[$agencyData['agency_code']]['bookings'][(int)$item['bookingId']] = array();
			}
			
			$itemdata = array();
			$itemdata['itemName'] = $item['itemName'];
			$itemdata['itemServiceName'] = $item['itemServiceName'];
			$itemdata['itemFromCountry'] = $item['itemFromCountry'];
			$itemdata['itemFromCity'] = $item['itemFromCity'];
			$itemdata['itemFromAddress'] =$item['itemFromAddress'];
			$itemdata['itemStartDate'] =date('M d, Y', $item['itemStartDate']->sec);
			$itemdata['itemEndDate'] =date('M d, Y', $item['itemEndDate']->sec);
			$itemdata['itemCancellationDate'] =$dEnd->format('M d, Y');
			array_push($bookingsAgencData[$agencyData['agency_code']]['bookings'][(int)$item['bookingId']],$itemdata);
		}
	}
	
}

$connection->close();
//print_r($bookingsAgencData);
foreach($bookingsAgencData as $key=>$agency){

	$strHTML = '<!DOCTYPE html>
				<html style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
				<head>
				<meta name="viewport" content="width=device-width">
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<title>Really Simple HTML Email Template</title>
				</head>
				<body bgcolor="#f6f6f6" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; -webkit-font-smoothing: antialiased; height: 100%; -webkit-text-size-adjust: none; width: 100% !important; margin: 0; padding: 0;">

				<!-- body -->
				<table class="body-wrap" bgcolor="#f6f6f6" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
				<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
				    <td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; clear: both !important; display: block !important; max-width: 600px !important; Margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">

				      <!-- content -->
				      
				      <div class="content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; display: block; max-width: 600px; margin: 0 auto; padding: 0;">
				      
				      <table style="width:100%;text-align:right;">
				      <tbody><tr><td><img src="http://208.97.188.14/images/TravelRez-logo3.png" style="float:right;height:100px;"></td><td></td></tr>
				      </tbody></table>

				      <table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
				<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">';
						if(empty($agency['agencyEmails'])){
							$strHTML .= '<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;color:red;">Note: This Agency doesnt have any emails. Please update this agency. And this notification will go directly to system or EET consultant.</p>';
				        }    
				       $strHTML .= '     <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">To:  '.$agency["agencyName"].'</p>
				            <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
				              Thank you for choosing TravelRez, some of your bookings is due to expire in the next 3 days '.date('M d, Y', strtotime(' +3 day')).'.               

				            </p>

				             <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
				              <table style="width:100%;margin: 0px;padding: 0;border:1px solid #ccc;"> 
				                <tr>
				                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: bold;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;">Booking #</td>
				                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: bold;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;">Item Details</td>
				                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: bold;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;">Service</td>
				                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: bold;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;">Start Date</td>
				                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: bold;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;">End Date</td>
				                </tr>';
                foreach($agency["bookings"] as $bookeyKEY => $items){
                	$isFirst = true;
                	foreach($items as $KEY => $val){
                		if($isFirst){
                			$strHTML .= '<tr>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;margin:0px; vertical-align:top;">'.$bookeyKEY.'</td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemName'].'</td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemServiceName'].'</td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemStartDate'].'</td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemEndDate'].'</td>
			                </tr>';
                			$isFirst = false;
                		}else{
                			$strHTML .= '<tr>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px; vertical-align:top;"></td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemName'].'</td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemServiceName'].'</td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemStartDate'].'</td>
			                  <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 1.6em; font-weight: normal;  padding: 2px;border-bottom:1px solid #ccc;margin:0px;vertical-align:top;">'.$val['itemEndDate'].'</td>
			                </tr>';
                		}
	                 
                	}
                }
                

             $strHTML .= ' </table>
				            </p>
				            
				            <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Please arrange payment before it expires and advise us accordingly.</p>
				            
				        
				<!-- /button --><p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Please don&rsquo;t hesitate to contact us on 1800 242 353 if you require further assistance.
				        </p>
				            <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">We look forward to receiving your payment and thank you for your continued support
				        </p>

				        <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">
				        Many Thanks Again
				        <br/><br/> 
				        Kind Regards
				        <br/><br/> 
				        Online Reservations
				        <br/> 
				        TravelRez, a division of Magic Tours International
				        </p>
				           
				          </td>
				        </tr></table>
				</div>
				      <!-- /content -->
				      
				    </td>
				    <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
				  </tr></table>
				<!-- /body --><!-- footer --><table class="footer-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; clear: both !important; width: 100%; margin: 0; padding: 0;"><tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
				<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
				    <td class="container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; clear: both !important; display: block !important; max-width: 600px !important; margin: 0 auto; padding: 0;">
				      
				    
				      
				    </td>
				    <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;"></td>
				  </tr></table>
				<!-- /footer -->
				</body>
				</html>
				';

			$mail = new PHPMailer;
			//Set who the message is to be sent from
			$mail->setFrom($sender,'TravelRez');
			//Set who the message is to be sent to
			if(!empty($agency['agencyEmails'])){
				foreach($agency['agencyEmails'] as $emailTo){
					$mail->addAddress($emailTo);	
				}
				
			}else{
				foreach($addCC as $emailTo){
					$mail->addAddress($emailTo);	
				}
				
			}
			
			
			
			foreach($addCC as $emailCC){
				$mail->AddCC($emailCC);
			}
			
			//Set the subject line
			$mail->Subject = 'Travelrez Notification: Your Booking is in Due Date!';
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			$mail->msgHTML($strHTML);
			if (!$mail->send()) {
			    $response = array ( "failure" => true);
			} else {
			    $response = array ( "success" => true);
			}

}

?>