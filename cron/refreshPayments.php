<?php

require_once('../_classes/tools.class.php');

require_once('../_classes/journal.class.php');
$connection = new MongoClient();
$db = $connection->db_system;
$tools = new Tools();
$journal = new Journal();

$bookindID = (int)$_GET['bookingID'];

//remove ALL ref and journal on all payments referenceId

$paymentDataRes = $db->reference->find(array("bookingId"=>$bookindID,"referenceType"=>"PYMTS"),array("_id"=>0));
$dataArray = iterator_to_array($paymentDataRes);


$refID =array();

foreach($dataArray as $key=>$row){
	array_push($refID, $row['referenceId']);
}

$db->reference->remove(array('referenceId'=>array("\$in"=>$refID)));
$db->journal->remove(array('referenceId'=>array("\$in"=>$refID)));

$paymentDataRes = $db->item_payments->find(array("bookingId"=>$bookindID,"is_cancelled"=>0));

//print_r($refDataRes);
$dataArray = iterator_to_array($paymentDataRes);
$totalPay = array();
$totalConvertedAmount = array();

foreach($dataArray as $key=>$row){
	if(!array_key_exists((int)$row['itemId'], $totalPay)){
		$totalPay[(int)$row['itemId']] = 0;
	}
	if(!array_key_exists((int)$row['itemId'], $totalConvertedAmount)){
		$totalConvertedAmount[(int)$row['itemId']] = 0;
	}
	$paymentID = $tools->getNextID("payments");
	$resPOST = array();
	$resPOST['itemId'] = (int)$row['itemId'];
	$resPOST['itemName'] = $row['itemName'];
	$resPOST['itemSupplierCode'] = $row['itemSupplierCode'];
	$resPOST['itemSupplierName'] = $row['itemSupplierName'];
	$resPOST['description'] = $row['description'];
	$resPOST['paymentId'] = (int)$paymentID;
	$resPOST['bookingId'] = (int)$row['bookingId'];

	$startDate =  date('Y-m-d H:i:s', $row['paymentsCreatedDate']->sec);
	$today = new MongoDate(strtotime((string)$startDate));

	$resPOST['paymentsCreatedDate'] = $today;
	$resPOST['paid_amount'] = (float)$row['paid_amount'];
	$resPOST['original_amount'] = (float)$row['original_amount'];
	$resPOST['converted_amount'] = (float)$row['converted_amount'];
	$resPOST['fromCurr'] = $row['fromCurr'];
	$resPOST['toCurr'] = $row['toCurr'];
	$resPOST['fromRate'] = (float)$row['fromRate'];
	$resPOST['toRate'] = (float)$row['toRate'];
	$resPOST['itemOriginalConvertedAmount'] = (float)$row['itemOriginalConvertedAmount'];
	$resPOST['is_cancelled'] = $row['is_cancelled'];
	$resPOST['cancelled_datetime'] = $row['cancelled_datetime'];
	$resPOST['refund_datetime'] = $row['refund_datetime'];
	$resPOST['is_refunded'] = $row['is_refunded'];

	$startDate1 =  date('Y-m-d H:i:s', $row['payment_date']->sec);

	$paymentDateStr = (string)date('d/m/Y', $row['payment_date']->sec);
	$paymentDate = new MongoDate(strtotime((string)$startDate1));

	$resPOST['payment_date'] = $paymentDate;

	$totalPay[(int)$row['itemId']] +=(float)$row['paid_amount'];
	$totalConvertedAmount[(int)$row['itemId']] +=(float)$row['converted_amount'];


	$originalToRate = $row['itemOriginalConvertedAmount']/$row['original_amount'];
	echo $originalToRate.'<br>';
	$origConversion = (float)$row['paid_amount']*$originalToRate;
	$secondConversion = (float)$row['converted_amount'];

	
	$gainloss = 0;
	/*if($totalPay[(int)$row['itemId']] == $resPOST['original_amount']){
		$gainloss = (float) $resPOST['itemOriginalConvertedAmount']-$totalConvertedAmount[(int)$row['itemId']]; 
	}else{
		$gainloss = 0;
		
	}*/

	$gainloss = $origConversion-$secondConversion; 
	//echo $gainloss;

	$resPOST['is_active'] = 1;
	$resPOST['gainloss'] = $gainloss;

	$db->item_payments->insert($resPOST);
	$journal->purchasePayment($resPOST,false, $paymentDateStr);

	$db->item_payments->remove(array('paymentId'=>(int)$row['paymentId']));


}

//remove all allocation

/*$refDataRes = $db->reference->find(array("referenceType"=>"ALLOCATION"));
//print_r($refDataRes);
$dataArray = iterator_to_array($refDataRes);
print_r($dataArray);
$referenceIDs = array();
foreach($dataArray as $key2 => $row){
	array_push($referenceIDs,$row['referenceId']);
}

$db->journal->remove(array("referenceId"=>array("\$in"=>$referenceIDs)));
$db->reference->remove(array("referenceId"=>array("\$in"=>$referenceIDs)));*/
//set is_forwarded to receipt
/*
$db->receipt->update(array(),array("\$set"=>array("is_forwarded"=>0)),array("multiple" => true));
//get All active and bank 


$receiptDataRes = $db->receipt->find(array("is_active"=>1,"receiptTypeName"=>"EFT (Bank)"));
$dataArray = iterator_to_array($receiptDataRes);

foreach($dataArray as $key2 => $row){
	$receiptDATA = array();
	$receiptDATA['amountPaid'] = (float)$row['receiptDepositedAmout'];
	$receiptDATA['receiptId'] = (int)$row['receiptId'];
	$receiptDATA['booking_id'] = (int)$row['bookingId'];
	$receiptDATA['description'] = $row['receiptDescription'];
	
	$startDate =  (string)date('d/m/Y', $row['receipt_datetime']->sec);
	//$startDate1 =   (string)date('d/m/Y', $row['created_datetime']->sec);

	if($startDate == '01/01/1970'){
		$startDate =  (string)date('d/m/Y', $row['created_datetime']->sec);

		$startDate1 =  date('Y-m-d H:i:s', $value['created_datetime']->sec);
		$mongDate = new MongoDate(strtotime((string)$startDate1));

		$db->receipt->update(array("receiptId"=>(int)$row['receiptId']),array("\$set"=>array("receipt_datetime"=>$mongDate)));
	}
	
	$journal->receiptBank($receiptDATA,false,$startDate);
	$db->receipt->update(array("receiptId"=>(int)$row['receiptId']),array("\$set"=>array("is_forwarded"=>1)));
}

$receiptDataRes = $db->receipt->find(array("is_active"=>1,"receiptTypeName"=>"Cash"));
$dataArray = iterator_to_array($receiptDataRes);
foreach($dataArray as $key2 => $row){
	$receiptDATA = array();
	$receiptDATA['amountPaid'] = (float)$row['receiptDepositedAmout'];
	$receiptDATA['receiptId'] = (int)$row['receiptId'];
	$receiptDATA['booking_id'] = (int)$row['bookingId'];
	$receiptDATA['description'] = $row['receiptDescription'];
	
	$startDate =  (string)date('d/m/Y', $row['receipt_datetime']->sec);
	//$startDate1 =   (string)date('d/m/Y', $row['created_datetime']->sec);

	if($startDate == '01/01/1970'){
		$startDate =  (string)date('d/m/Y', $row['created_datetime']->sec);

		$startDate1 =  date('Y-m-d H:i:s', $value['created_datetime']->sec);
		$mongDate = new MongoDate(strtotime((string)$startDate1));

		$db->receipt->update(array("receiptId"=>(int)$row['receiptId']),array("\$set"=>array("receipt_datetime"=>$mongDate)));
	}

	$journal->receiptBank($receiptDATA,false,$startDate);
	$db->receipt->update(array("receiptId"=>(int)$row['receiptId']),array("\$set"=>array("is_forwarded"=>1)));
}
*/

//print_r($dataArray);

$connection->close();
?>