<?php
date_default_timezone_set('Australia/Melbourne');
session_start();
set_time_limit(0);
ini_set('memory_limit', '1024M');
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));

$connection = new MongoClient();
$db = $connection->db_system;

function array_sort_by_column(&$array, $column, $direction = SORT_ASC) {
    $reference_array = array();

    foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
    }

    array_multisort($reference_array, $direction, $array);
}

$whereData = array("productId"=>(int)$_GET['id'],"is_active"=>1);
$arrayResults = $db->products->findOne($whereData,array('_id'=>0));

$currEx = array();
$tempArray = array();
foreach($arrayResults['productAvailability'] as $key=>$val){
	$dateArr = explode('/',$val['dateFrom']);
	$dateArr2 = explode('/',$val['dateTo']);
	$newDateFrom=date_create($dateArr[2]."-".$dateArr[1]."-".$dateArr[0]);
	$newDateTo=date_create($dateArr2[2]."-".$dateArr2[1]."-".$dateArr2[0]);

	//$newDateFrom = DateTime::createFromFormat('d/m/Y', $val['dateFrom']);
	//$newDateTo = DateTime::createFromFormat('d/m/Y', $val['dateTo']);
	
	if(!array_key_exists($val['fromCurrency'], $currEx)){
		$curr = $db->currency_ex->findOne(array('curr_from'=>$val['fromCurrency']),array('_id'=>0));
		//print_r($curr);
		$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

	}
	$priceBase = (float)$val['basePrice'];
	$comPrice = $priceBase*((int)$val['commPct']/100);
	$lessNow = $priceBase-$comPrice;

	$convertedPrice = $lessNow*$currEx[$val['fromCurrency']];


	$markupPrice = $convertedPrice*((int)$val['markupPct']/100);
	$grossPrice = $convertedPrice+$markupPrice;
	$displayPrice = round($grossPrice);

	$newAvailabilityFormat = array(
		"fromDayText" =>date_format($newDateFrom,'l'),
		"fromMonth" =>date_format($newDateFrom,'M'),
		"fromDay"=>date_format($newDateFrom,'d'),
		"fromYear"=>date_format($newDateFrom,'Y'),
		"toDayText" =>date_format($newDateTo,'l'),
		"toMonth" =>date_format($newDateTo,'M'),
		"toDay"=>date_format($newDateTo,'d'),
		"toYear"=>date_format($newDateTo,'Y'),
		"departureDate" =>$newDateFrom,
		"displayPrice" => $displayPrice,
		"grossPrice" => $grossPrice,
		"markupPrice" => $markupPrice,
		"comPrice" => $comPrice
		);
	array_push($tempArray,$newAvailabilityFormat);
	
}
array_sort_by_column($tempArray, 'departureDate');


$arrayResults['productAvailability'] = $tempArray;
$currEx = array();

foreach($arrayResults['productSupplements'] as $key=>$val){

	if(!array_key_exists($val['fromCurrency'], $currEx)){
		$curr = $db->currency_ex->findOne(array('curr_from'=>$val['fromCurrency']),array('_id'=>0));
		//print_r($curr);
		$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

	}

	$priceBase = (float)$val['basePrice'];
	$comPrice = $priceBase*((int)$val['commPct']/100);
	$lessNow = $priceBase-$comPrice;
	$convertedPrice = $lessNow*$currEx[$val['fromCurrency']];

	$markupPrice = $convertedPrice*((int)$val['markupPct']/100);
	$grossPrice = $convertedPrice+$markupPrice;
	$displayPrice = round($grossPrice);

	$newAvailabilityFormat = array(
		"supplement_name" => $val['supplement_name'],
		"displayPrice" => $displayPrice,
		"grossPrice" => $grossPrice,
		"markupPrice" => $markupPrice,
		"comPrice" => $comPrice
		);
	$arrayResults['productSupplements'][$key] = $newAvailabilityFormat;
}

$connection->close();


			

  	  		
print json_encode($arrayResults);
?>