<?php

require_once('../_classes/tools.class.php');

require_once('../_classes/journal.class.php');
$connection = new MongoClient();
$db = $connection->db_system;
$tools = new Tools();

$bookingID = (int)$_GET['bookingId'];

//convert is receipted to zero

$db->items->update(array("bookingId"=>$bookingID),array("\$set"=>array("itemIsReceipt"=>0)),array("multiple" => true));

$db->receipt->update(array("bookingId"=>$bookingID),array("\$set"=>array("is_allocated"=>0)),array("multiple" => true));

//print_r($itemIdsList);
//item_id db.item_allocated.find

$itemIdsList = array();
$resItems = $db->items->find(array("bookingId"=>$bookingID),array("_id"=>0));
$itemsDataArray = iterator_to_array($resItems);
foreach($itemsDataArray as $key1=>$row){

	
	array_push($itemIdsList ,$row['itemId']);
	

	
}

$db->item_allocated->remove(array("item_id"=>array("\$in"=>$itemIdsList)));

$db->allocate->remove(array("bookingId"=>$bookingID));
$connection->close();
?>