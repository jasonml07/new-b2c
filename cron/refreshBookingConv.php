<?php
error_reporting(0);

$connection = new MongoClient();
$db = $connection->db_system;
//$tools = new Tools();

$resultsinvoice = $db->invoice->find(array(),array("_id"=>0));
$invoiceDataArray = iterator_to_array($resultsinvoice);


foreach($invoiceDataArray as $key=>$val){

	$where = array("booking_id"=>(int)$val['bookingId'],"booking_conversion_date"=>array("\$exists"=>false));

	$bookingDataRes = $db->booking->find($where);
	//echo $bookingDataRes->count();

	$startDate =  date('Y-m-d H:i:s', $val['created_datetime']->sec);
	//echo $startDate."<br>";
	$mongDate = new MongoDate(strtotime((string)$startDate));

	//$today = new MongoDate(strtotime("now"));
	if($bookingDataRes->count() > 0){
		$db->booking->update(array("booking_id"=>(int)$val['bookingId']),array("\$set"=>array("booking_conversion_date"=>$mongDate)));
	}
}


$connection->close();
?>