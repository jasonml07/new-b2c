<?php
error_reporting(0);

require_once('../_classes/tools.class.php');
$connection = new MongoClient();
$db = $connection->db_system;



$dateStartRef = "01/07/2015";
				$xDate = explode("/", $dateStartRef);

				$DateSTR=  date('Y-m-d H:i:s',strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 00:00:00"));
				$Date = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 23:59:59"));



				$journalDataRes = $db->journal->find(array("accountId"=>'1-1105',"accounting_datetime"=>array( "\$lt" => $Date)), array("_id"=>0));


				$journalData = iterator_to_array($journalDataRes);


				print_r($journalData);



$xDate = explode("/", $_GET['startdate']);

$DateSTR=  date('Y-m-d H:i:s',strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 23:59:59"));
$Date = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 23:59:59"));



$journalDataRes = $db->journal->find(array("accountId"=>'1-1105',"accounting_datetime"=>array( "\$lt" => $Date)), array("_id"=>0));


$journalData = iterator_to_array($journalDataRes);

print_r($journalData);

$refIDs = array();
foreach ($journalData as $key => $value) {
	array_push($refIDs, $value['referenceId']);
}
//print_r($journalData);
$refDataRes = $db->reference->find(array("referenceId"=>array("\$in"=>$refIDs )));

$refDataRef = array();
$refData = iterator_to_array($refDataRes);
foreach ($refData as $key => $value) {
	$refDataRef[$value['referenceId']] = $value;
}
$begBalance = 0;
//print_r($refDataRef);
$connection->close();
?>
Movements of account 1-1105
<table border=1>
	<thead>
		<tr><th>Booking ID</th><th>Reference ID</th><th>Reference Type</th><th>Description</th><th>Date</th><th>Debit</th><th>Credit</th><th>Balance</th></tr>
	</thead>
	<tbody>
		
		<?php foreach($journalData as $key=>$row): ?>
		<tr>
			<td><?=$refDataRef[$row['referenceId']]['bookingId']; ?></td>
			<td><?=$row['referenceId']; ?></td>
			<td><?=$refDataRef[$row['referenceId']]['referenceType']; ?></td>
			<td><?=$refDataRef[$row['referenceId']]['description']; ?></td>
			<td>
				<?php 
				
				echo date('d, M Y', $row['accounting_datetime']->sec);;
				?>
			</td>
			<td><?=$row['debit']; ?></td>
			<td><?=$row['credit']; ?></td>
			<td>
				<?php 
				if((float)$row['debit'] != 0){
					$begBalance += (float)$row['debit'];
				}else{
					$begBalance -= (float)$row['credit'];
				}
				echo $begBalance." AUD";
				?>
			</td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan=7>Ending Balance </td>
			<td><?php echo $begBalance." AUD"; ?></td>
		</tr>
	</tbody>
</table>