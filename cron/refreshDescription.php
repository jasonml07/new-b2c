<?php

require_once('../_classes/tools.class.php');

require_once('../_classes/journal.class.php');
$connection = new MongoClient();
$db = $connection->db_system;
$tools = new Tools();
$journal = new Journal();



//remove ALL ref and journal on all payments referenceId

$paymentDataRes = $db->reference->find(array("referenceType"=>"PYMTS"),array("_id"=>0));
$dataArray = iterator_to_array($paymentDataRes);


$refID =array();

foreach($dataArray as $key=>$row){
	$exDesc = explode(': ', $row['description']);
	if(array_key_exists(1, $exDesc)){
		 $db->reference->update(array("referenceId"=>(int)$row['referenceId']),array("\$set"=>array("description"=>$exDesc[1])));
		//print_r($exDesc);
	}
	
	//array_push($refID, $row['referenceId']);
}

$paymentDataRes = $db->reference->find(array("referenceType"=>"RCPT"),array("_id"=>0));
$dataArray = iterator_to_array($paymentDataRes);


$refID =array();

foreach($dataArray as $key=>$row){
	$exDesc = explode('|', $row['description']);
	if(array_key_exists(1, $exDesc)){
		 $db->reference->update(array("referenceId"=>(int)$row['referenceId']),array("\$set"=>array("description"=>$exDesc[1])));
		//print_r($exDesc);
	}
	//array_push($refID, $row['referenceId']);
}





$connection->close();
?>