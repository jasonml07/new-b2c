<?php
$currencyList = array("EUR","USD","CHF","AED","DKK","NOK","GBP","CAD");


$yql_base_url = "http://query.yahooapis.com/v1/public/yql";  

$str = "";
$isFirst = true;

foreach($currencyList as $key=>$val){
	if($isFirst){
		$str .= '"'.$val.'AUD"';
		$isFirst = false;
	}else{
		$str .= ',"'.$val.'AUD"';
	}

}

$yql_query = 'select * from yahoo.finance.xchange where pair in ('.$str.')';
$yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
$yql_query_url .= "&env=store://datatables.org/alltableswithkeys&format=json";  
$curl = curl_init($yql_query_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);    
$json = curl_exec($curl); 
curl_close($curl);   
$arrRes = json_decode($json);

$connection = new MongoClient();
$db = $connection->db_system;

$updateData = array();
foreach($arrRes->query->results->rate as $key => $val){
	$updateData = array();
	$updateData['rate_to'] = $val->Rate;
	$currName = explode("/",$val->Name);
	
	$res = $db->currency_ex->update(array("curr_from"=> $currName[0]), array("\$set" => $updateData));
		if($res){
		echo $currName[0]. " Successfully updated .</br>";
		}

	$connection->close();
}

?>