<?php
error_reporting(0);
require_once('../_classes/tools.class.php');
$connection = new MongoClient();
$db = $connection->db_system;

$tool = new Tools();

$xDate = explode("/", $_GET['date']);
$DateSTR=  date('Y-m-d H:i:s',strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 23:59:59"));
$Date = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 23:59:59"));

$acc_chart_res = $db->sys_account_chart->find(array(), array("_id"=>0));
$acc_chartData = iterator_to_array($acc_chart_res);

$refChart = array();
foreach ($acc_chartData as $key => $value) {
	$refChart[$value['Account']] = $value;
}

$journalDataRes = $db->journal->find(array("accounting_datetime"=>array( "\$lt" => $Date)), array("_id"=>0));


$journalData = iterator_to_array($journalDataRes);
$trialBalanceResult = array();


$refDataRes = $db->reference->find(array("created_datetime"=>array( "\$lt" => $Date)), array("_id"=>0));

$refIds = array();
$refData = iterator_to_array($refDataRes);

$refDataRef = array();
foreach ($refData as $key => $value) {
	array_push($refIds, $value['referenceId']);
	$refDataRef[$value['referenceId']] = $value;
}


$journalDataRes2 = $db->journal->find(array("referenceId"=>array("\$in"=>$refIds)), array("_id"=>0));


$journalData2 = iterator_to_array($journalDataRes2);

$journalByRef = array();

foreach ($journalData2 as $key => $value) {
	if(array_key_exists($value['referenceId'], $journalByRef)){
		array_push($journalByRef[$value['referenceId']], $value);
	}else{
		$journalByRef[$value['referenceId']] = array($value);

	}
}


/*foreach ($journalData as $key => $value) {
	if(array_key_exists($value['accountId'], $trialBalanceResult)){
		
		
		if($refChart[$value['accountId']]['DrCr']=='Debit'){
			if($value['debit'] != 0){
				$trialBalanceResult[$value['accountId']]['debit'] += $value['debit'];
			}else{
				$trialBalanceResult[$value['accountId']]['debit'] -= $value['credit'];
			}
			
			//$trialBalanceResult[$value['accountId']]['debit'] += $value['credit'];
		}else{
			if($value['credit'] != 0){
				$trialBalanceResult[$value['accountId']]['credit'] += $value['credit'];
			}else{
				$trialBalanceResult[$value['accountId']]['credit'] -= $value['debit'];
			}
			//$trialBalanceResult[$value['accountId']]['credit'] += $value['debit'];
		}
	}else{
		if($refChart[$value['accountId']]['DrCr']=='Debit'){
			$resArr = array();
			$resArr['code'] = $value['accountId'];
			$resArr['name'] = $refChart[$value['accountId']]['AccountName'];
			$resArr['account_type'] = 'debit';

			if($value['debit'] != 0){
				$resArr['debit'] = $value['debit'];
			}else{
				$resArr['debit'] = 0-$value['credit'];
			}

			$resArr['credit'] = 0;
			$trialBalanceResult[$value['accountId']] = $resArr;
		}else{
			$resArr = array();
			$resArr['code'] = $value['accountId'];
			$resArr['name'] = $refChart[$value['accountId']]['AccountName'];
			$resArr['account_type'] = 'credit';
			$resArr['debit'] = 0;
			if($value['credit'] != 0){
				$resArr['credit'] = $value['credit'];
			}else{
				$resArr['credit'] = 0-$value['debit'];
			}
			$trialBalanceResult[$value['accountId']] = $resArr;
		}
	}
	//$refChart[$value['Account']] = $value;
}*/

foreach ($journalData as $key => $value) {
	if(array_key_exists($value['accountId'], $trialBalanceResult)){
		$trialBalanceResult[$value['accountId']]['debit'] += $value['debit'];
		$trialBalanceResult[$value['accountId']]['credit'] += $value['credit'];
		
		
	}else{
		$resArr = array();
		$resArr['code'] = $value['accountId'];
		$resArr['name'] = $refChart[$value['accountId']]['AccountName'];
		$resArr['account_type'] = 'debit';

		$resArr['debit'] = $value['debit'];
		$resArr['credit'] = $value['credit'];
		$trialBalanceResult[$value['accountId']] = $resArr;
	}
	//$refChart[$value['Account']] = $value;
}

//print_r($trialBalanceResult);
$connection->close();
$totalDebit = 0;
$totalCredit = 0;
?>


<table border=1>
	<thead>
		<tr><th>Booking ID</th><th>Reference ID</th><th>Reference</th><th>Reference Type</th><th>Description</th><th>Account Code</th><th>Account Name</th><th>Date</th><th>Debit</th><th>Credit</th></tr>
	</thead>
	<tbody>
		
		<?php foreach($journalByRef as $key1=>$row1): ?>

			<tr>
				<td><?=$refDataRef[$key1]['bookingId']; ?></td>
				<td><?=$key1; ?></td>
				<td style="font-size:9px;"><?php 
				echo "invoiceId: ".$refDataRef[$key1]['invoiceId']."<br>";
				echo "allocationId: ".$refDataRef[$key1]['allocationId']."<br>";  
				echo "paymentId: ".$refDataRef[$key1]['paymentId']."<br>";  
				echo "itemId: ".$refDataRef[$key1]['itemId']."<br>";  
				echo "bookingId: ".$refDataRef[$key1]['bookingId']."<br>";  
				?></td>
				<td><?=$refDataRef[$key1]['referenceType']; ?></td>
				<td style="font-size:9px;"><?=$refDataRef[$key1]['description']; ?></td>
				<td></td>
				<td></td>
				<td>
					
				</td>
				<td></td>
				<td></td>
				
			</tr>
			<?php 
			$totalDebitRef = 0;
			$totalCreditRef = 0;
			?>
			<?php foreach($row1 as $key=>$row): ?>

			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?=$refChart[$row['accountId']]['Account'] ?></td>
				<td style="font-size:11px;"><?=$refChart[$row['accountId']]['AccountName'] ?></td>
				<td>
					<?php 
					
					echo date('d, M Y', $row['accounting_datetime']->sec);;
					?>
				</td>
				<td><?=$row['debit']; ?></td>
				<td><?=$row['credit']; ?></td>
				
			</tr>
			<?php 
			$totalDebitRef += $row['debit'];
			$totalCreditRef += $row['credit'];
			?>
			<?php endforeach; ?>
			<?php
			$style = 'style="background-color: #ccc; color:#000;"';
			if($totalDebitRef != $totalCreditRef){
				$style = 'style="background-color: red; color:#fff;"';
			}
			?>
			<tr>
				<td <?=$style?>></td>
				<td <?=$style?>></td>
				<td <?=$style?>></td>
				<td <?=$style?>></td>
				<td <?=$style?>></td>
				<td <?=$style?>></td>
				<td <?=$style?>></td>
				<td <?=$style?>></td>
					
				</td>
				<td <?=$style?>><?=$totalDebitRef; ?></td>
				<td <?=$style?>><?=$totalCreditRef; ?></td>
				
			</tr>
		<?php endforeach; ?>
		
	</tbody>
</table>


<table border=1>
	<thead>
		<tr>
			<th>Acount Code</th>
			<th>Acount Name</th>
			<th>Type</th>
			<th>Debit</th>
			<th>Credit</th>
		</tr>
	</thead>
	<tbody>
		
		<?php foreach($trialBalanceResult as $key=>$row): ?>
		
		<?php 
		$typeAccount = $refChart[$row['code']]['DrCr'];

		?>
		<?php if($typeAccount == 'Debit'): ?>
		<?php 
		$totalDR = $row['debit']-$row['credit'];
		$totalCredit += $totalDR;
		?>
		<tr>
			<td><?=$row['code']; ?></td>
			<td><?=$row['name']; ?></td>
			<td><?=$typeAccount; ?></td>
			<td><?php echo $tool->moneyFormat((float)$totalDR); ?></td>
			<td></td>
			
		</tr>
	<?php else: ?>
		<?php 
		$totalCR = $row['credit']-$row['debit'];
		$totalDebit += $totalCR;
		?>
		<tr>
			<td><?=$row['code']; ?></td>
			<td><?=$row['name']; ?></td>
			<td><?=$typeAccount; ?></td>
			<td></td>
			<td><?php echo $tool->moneyFormat((float)$totalCR); ?></td>
			
		</tr>
	<?php endif; ?>
		<?php endforeach; ?>
		<tr>
			<td colspan=3>Total</td>
			<td><?php echo $tool->moneyFormat((float)$totalDebit); ?></td>
			<td><?php echo $tool->moneyFormat((float)$totalCredit); ?></td>
		</tr>
	</tbody>
</table>
