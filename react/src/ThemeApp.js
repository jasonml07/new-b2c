import React, { useContext } from 'react';
import { createTheme, ThemeProvider, styled, useTheme } from '@mui/material/styles';
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
// Components
import Content from './Pages/Content'
// Themes
import EETTheme from './Layout/EET/theme'
import EHTheme from './Layout/EH/theme'
import IFLYTheme from './Layout/IFLY/theme'
// Pages
import Review from './Pages/Review'
import TravelRez from './Pages/TravelRez'

const themes = {
  'EET': EETTheme,
  'EH': EHTheme,
  'IFLY': IFLYTheme
}
// Machine
import { GlobalStateProvider, GlobalState } from './global.state'
import { useActor } from '@xstate/react'

function ThemeApp () {
  const Services = useContext(GlobalState)
  const AppState = useActor(Services.AppService)
  const theme = themes[AppState[0].context.agency] || themes.EH
  return (
    <Router>
      <ThemeProvider theme={theme.style}>
        <Switch>
          <Route path="/product/:id/agency/:agency/token/:token">
            <Content appTheme={theme} />
          </Route>
          <Route path="/product/:id/agency/:agency">
            <Content appTheme={theme} />
          </Route>
          <Route path="/product/:id">
            <Content appTheme={theme} />
          </Route>
          <Route path="/review/:bookingId">
            <Review />
          </Route>
          <Route path="/travelrez">
            <TravelRez />
          </Route>
          <Route path="*">
            <span>Invalid path</span>
          </Route>
        </Switch>
      </ThemeProvider>
    </Router>
  )
}

export default ThemeApp;