import axios from 'axios'
import { createMachine, assign } from 'xstate'

export default createMachine({
  id: 'Review',
  initial: 'idle',
  context: {
    information: null
  },
  states: {
    idle: {
      on: {
        FETCH: 'fetch.process'
      }
    },
    fetch: {
      states: {
        process: {
          invoke: {
            src: 'fetching',
            onDone: {
              // target: '#Review.idle',
              target: 'success',
              actions: ['setData']
            },
            onError: 'failure'
          }
        },
        success: {},
        failure: {
          on: {
            RETRY: 'process'
          }
        },
      }
    }
  }
}, {
  actions: {
    setData: assign({
      information: (ctx, event) => event.data.data
    })
  },
  services: {
    fetching(ctx, event) {
      return axios.get('/api/v2/tour/booking-detail', {
        params: { ref: event.data }
      })
    }
  }
})