import axios from 'axios'
import { createMachine, assign } from 'xstate'
import PRODUCT from '../../assets/product.json'
import TOURS from '../../assets/tours.json'
// import TOURS from '../../assets/tours1.json'
import DEPARTURE_MONTHS from '../../assets/departureMonths.json'


const guests = {
  pax: 1,
  rooms: '',
  selections: []
}
export default createMachine({
  id: 'App',
  // initial: 'detail',
  initial: 'idle',
  on: {
    
  },
  context: {
    agency: 'EH',
    bookingRef: '',
    errorMessage: '',
    currentTour: null,
    product: null,
    tours: [],
    departureMonths: null,
    guests: {...guests},
    form: {
      beds: [],
      packages: [],
      extraBills: [],
      totalPrice: 0,
      discount: null,
    },
    bookingData: {},
    failure: null,
    discount: null
  },
  states: {
    idle: {
      on: {
        URL_PARAM_NOT_DEFINE: 'errors.paramNotDefine',
        FETCH: 'fetch.process'
      }
    },
    errors: {
      on: {
        // RETRY: '#App.fetch'
      },
      states: {
        paramNotDefine: {},
      }
    },
    fetch: {
      states: {
        process: {
          invoke: {
            src: 'fetching',
            onDone: {
              target: '#App.detail',
              actions: ['setData']
            },
            onError: [
              { target: 'failure.withMessage', cond: 'isFailure', actions: ['setErrorMessage']},
              { target: 'failure.idle'},
            ]
          }
        },
        failure: {
          on: {
            RETRY: '#App.fetch.process'
          },
          states: {
            idle: {},
            withMessage: {}
          }
        }
      }
    },
    detail: {
      on: {
        // ROOM_SELECTION: 'roomSelection'
        ROOM_SELECTION: {
          target: 'roomSelection',
          actions: ['setCurrentTour']
        }
      },
      initial: 'calendar',
      states: {
        calendar: {},
        list: {},
        compact: {},
      }
    },
    roomSelection: {
      entry: {
        actions: 'clearSelection'
      },
      on: {
        SET_PACKAGE_QTY: {
          target: '.settingPackageQuantity',
          actions: ['setPackageQty']
        },
        DETAIL: 'detail',
        CHECKOUT: {
          target: 'checkout',
          actions: ['setForm']
        },
        SET_PAX: {
          actions: 'setPax'
        },
        SET_ROOMS: {
          actions: 'setRooms'
        },
        SET_ROOM_CHOICE: {
          actions: 'setRoomChoice'
        }
      },
      initial: 'idle',
      type: 'parallel',
      states: {
        idle: {},
        discount: {
          id: 'Discount',
          initial: 'close',
          states: {
            open: {
              on: {
                DISCOUNT_TOGGLE: 'close',
              },
              initial: 'idle',
              states: {
                idle: {
                  on: {
                    DISCOUNT_SUBMIT: 'process'
                  }
                },
                process: {
                  invoke: {
                    src: 'checkDiscount',
                    onDone: {
                      target: '#Discount.close',
                      actions: ['setDiscount']
                    },
                    onError: {
                      target: 'failure',
                      actions: ['setFailure']
                    }
                  }
                },
                failure: {
                  on: {
                    DISCOUNT_SUBMIT: 'process'
                  }
                },
              }
            },
            close: {
              on: {
                DISCOUNT_TOGGLE: 'open'
              }
            }
          }
        },
        settingPackageQuantity: {
          after: {
            100: {
              target: 'idle',
            }
          }
        }
      }
    },
    checkout: {
      initial: 'show',
      states: {
        show: {
          initial: 'idle',
          states: {
            idle: {
              on: {
                ROOM_SELECTION: '#App.roomSelection',
                DETAIL: '#App.detail',
                // BOOK: 'process',
                CONFIRM: {
                  target: 'confirm',
                  actions: ['setBookingData']
                },
              }
            },
            confirm: {
              on: {
                BOOK: 'process',
                CANCEL: 'idle'
              }
            },
            process: {
              invoke: {
                src: 'booking',
                onDone: {
                  target: 'success',
                  actions: ['setBookingRef']
                },
                onError: 'failure'
              }
            },
            success: {
              on: {
                CLOSE: '#App.checkout.show'
              }
            },
            failure: {
              on: {
                CLOSE: '#App.checkout.show'
              }
            },
          }
        }
      }
    }
  }
}, {
  guards: {
    isFailure: (ctx, event) => {
      return event.data.response.status == 400
    }
  },
  actions: {
    // actions
    setDiscount: assign({
      discount(ctx, event) {
        return event.data
      }
    }),
    setFailure: assign({
      failure(ctx, event) {
        return event.data.response
      }
    }),
    setPackageQty: assign({
      currentTour: (ctx, event) => {
        const packages = [...ctx.currentTour?.product_supplements || []]
        packages[event.data.index].quantity = event.data.value
        return {...ctx.currentTour, product_supplements: packages}
      }
    }),
    setBookingData: assign({
      bookingData: (ctx, event) => event.data
    }),
    setBookingRef: assign({
      bookingRef: (ctx, event) => event.data.data.reference
    }),
    setErrorMessage: assign({
      errorMessage: (ctx, event) => event.data.response.data.message
    }),
    setForm: assign({
      form: (ctx, event) => event.data
    }),
    setCurrentTour: assign({
      currentTour: (ctx, event) => event.data
    }),
    setRoomChoice: assign({
      guests: (ctx, event) => {
        const selections = [...ctx.guests.selections]
        const {choice, index} = event.data
        for(var i=0; i<selections.length; i++) {
          if(i == index) {
            selections[i] = choice
            break;
          }
        }
        return {
          ...ctx.guests,
          selections
        }
      }
    }),
    setRooms: assign({
      guests: (ctx, event) => {
        const numberOfRooms = Number(event.data)
        const selections = []
        if(Boolean(numberOfRooms)) {
          for(let i=0; i<numberOfRooms; i++) {
            selections.push('')
          }
        }
        return {
          ...ctx.guests,
          rooms: numberOfRooms,
          selections
        }
      }
    }),
    setPax: assign({
      guests: (ctx, event) => {
        return {
          ...ctx.guests,
          pax: event.data
        }
      }
    }),
    clearSelection: assign((ctx, event) => {
      return {
        ...ctx,
        guests: {...guests},
      }
    }),
    setData: assign((ctx, event) => {
      const data = event.data.data
      return {
        ...ctx,
        agency: data.agency.toUpperCase(),
        product: data.product,
        tours: data.tours.map((item, index) => ({...item, id: index})),
        // tours: TOURS.map((item, index) => ({...item, id: index})),
        departureMonths: data.departures
      }
    })
  },
  services: {
    // services
    checkDiscount(ctx, event) {
      return axios.post('/api/v2/tour/discount/check', {code: event.data})
        .then(response => response.data)
    },
    booking(ctx, event) {
      return axios.post('/api/v2/tour/book', event.data)
    },
    fetching(ctx, event) {
      return axios.get('/api/v2/tour/detail', {
        params: event.data
      })
    }
  }
})