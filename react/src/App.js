import React, {  } from 'react';
import PropTypes from 'prop-types';
import { GlobalStateProvider } from './global.state'
// Component
import ThemeApp from './ThemeApp'

// console.log(process)
export default function App({

}) {
  global.process = {
    env: (typeof process != 'undefined') ?(process.env || {}): {}
  }
  // global.process = window.process
  return (
    <GlobalStateProvider>
      <ThemeApp />
    </GlobalStateProvider>
  )
}