import React, { useContext } from 'react'
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
// MUI
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import MUIButton from '@mui/material/Button';
import { styled } from '@mui/material/styles';
// Icons
import MenuIcon from '@mui/icons-material/Menu';
// Machine
import { TravelRezState } from './machines/state'
import { useActor } from '@xstate/react'
// Components
import Discount from './Discount'

const Button = styled(MUIButton)(({theme, ...props}) => ({
  color: theme.palette.common.white
}))

function Dashboard() {
  const Services = useContext(TravelRezState)
  const AppState = useActor(Services.AppState)

  const handleLogout = () => {
    AppState[1]('LOGOUT')
  }
  return (
    <Box>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
          >
            TravelRez
          </Typography>
          <Box>
            <Button>Discount</Button>
            <Button onClick={handleLogout}>Logout</Button>
          </Box>
        </Toolbar>
      </AppBar>
      <Switch>
        <Route path="/travelrez/users">
          <Typography>Users</Typography>
        </Route>
        <Route exact path="">
          <Discount />
        </Route>
      </Switch>
    </Box>
  )
}

export default Dashboard