import React, { useContext, useEffect } from 'react'
import { TravelRezState } from './machines/state'
import { useActor } from '@xstate/react'
import moment from 'moment'
// MUI
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
// Icons
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import ErrorIcon from '@mui/icons-material/Error';
// Components
import DiscountCreate from './Discount.Create'
import DiscountDeleteConfirm from './Discount.DeleteConfirm'

function Discount () {
  const Services = useContext(TravelRezState)
  const DiscountState = useActor(Services.DiscountState)
  const search = DiscountState[0].context.search
  const discounts = DiscountState[0].context.list
  const failure = DiscountState[0].context.failure
  let errors = {}
  if(failure?.status == 422) {
    errors = failure?.data?.errors || {code: 'Failed to create.'}
  }

  const handleSearch = (e) => {
    DiscountState[1]({type: 'SEARCH', data: e.target.value})
  }
  const handleClearSearch = () => {
    DiscountState[1]({type: 'SEARCH', data: ''})
  }
  useEffect(() => {
    DiscountState[1]('FETCH')
  }, [])
  return (
    <Box sx={{p: 5}}>
      <DiscountCreate
        errors={DiscountState[0].matches('create.failure') ?errors : null}
        submitting={DiscountState[0].matches('create.process')}
        open={['create.show', 'create.process', 'create.failure'].some(DiscountState[0].matches)}
        onClose={() => DiscountState[1]('CANCEL')}
        onSubmit={(values) => DiscountState[1]({type: 'SUBMIT', data: values})}
      />
      <DiscountDeleteConfirm
        open={DiscountState[0].matches('delete.confirm')}
        onConfirm={() => DiscountState[1]('CONFIRM')}
        onCancel={() => DiscountState[1]('CANCEL')}
      />
      <Grid container spacing={2}>
        <Grid item xs={12} container spacing={2}>
          <Grid item xs={10}>
            <TextField
              fullWidth
              label="Search"
              size="small"
              InputProps={{
                endAdornment: <InputAdornment position="end">{search.length > 0 ?<IconButton onClick={handleClearSearch}><HighlightOffIcon /></IconButton> :''}</InputAdornment>
              }}
              value={search}
              onChange={handleSearch}
            />
          </Grid>
          <Grid item xs={2}>
            <Button fullWidth variant="contained" onClick={() => DiscountState[1]('CREATE')}>Create discount</Button>
          </Grid>
        </Grid>
        {
          (() => {
            if(discounts.length == 0) {
              return (
                <Grid item xs={6} sm={4}>
                  <Paper elevation={5} sx={{p: 3}}>
                    <Typography variant="h2" align="center" color="error"><ErrorIcon style={{fontSize: '2em'}}/></Typography>
                    <Typography variant="h5" align="center" color="error">No discounts yet.</Typography>
                  </Paper>
                </Grid>
              )
            }

            return discounts.map((discount, index) => (
              <Grid item xs={6} sm={4} key={'discount-'+index}>
                <Paper elevation={5} sx={{p: 3}}>
                  <Grid container spacing={1}>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Code</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.code}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Availability Date</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{moment(discount.startDate.sec).format('MMM DD, YYYY')} until {moment(discount.endDate.sec).format('MMM DD, YYYY')}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Type</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.type}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Category</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.category}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Value/Amount</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.value}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Number of Pax</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.pax || 'none'}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Products</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.products || 'none'}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Message</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.message}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="h6"><b>Agency</b></Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>{discount.agencies.join(', ')}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Box sx={{display: 'flex', justifyContent: 'end'}}>
                        <IconButton color="error" onClick={() => DiscountState[1]({type: 'DELETE', data: discount})}>
                          <DeleteIcon />
                        </IconButton>
                        <IconButton color="info">
                          <ModeEditIcon />
                        </IconButton>
                      </Box>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            ))
          })()
        }
      </Grid>
    </Box>
  )
}

export default Discount