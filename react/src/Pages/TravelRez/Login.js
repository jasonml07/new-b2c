import React, { useContext } from 'react'
// MUI
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
// Components
import LoginForm from './Login.Form'
import LoginPin from './Login.Pin'
import LoginVerified from './Login.Verified'
// Machine
import { TravelRezState } from './machines/state'
import { useActor } from '@xstate/react'

function Login () {
  const Services = useContext(TravelRezState)
  const LoginState = useActor(Services.LoginState)
  const failure = LoginState[0].context.failure
  const otpHash = LoginState[0].context.otpHash
  // console.log(failure?.data?.errors)
  const handleOtpSubmit = (values) => {
    console.log(values)
    LoginState[1]({type: 'SUBMIT', data: values})
  }
  const handleResend = () => {
    LoginState[1]('RESEND')
  }
  return (
    <Grid container>
      <Grid item xs={1} sm={4}>
      </Grid>
      <Grid item xs={10} sm={4}>
        {
          LoginState[0].matches('form') && (
            <LoginForm submitting={LoginState[0].matches('form.process')}
              onSubmit={handleOtpSubmit}
              errors={LoginState[0].matches('form.failure') ?failure?.data?.errors || {email: 'Something went wrong!'} : null}
            />
          ) 
        }
        {
          LoginState[0].matches('pin') && (
            <LoginPin submitting={LoginState[0].matches('pin.process')}
              errors={LoginState[0].matches('pin.failure') ?failure?.data?.errors || {pin: 'Failed to validate.'} :null}
              resending={LoginState[0].matches('pin.resend.process')}
              onSubmit={handleOtpSubmit}
              onResend={handleResend}
            />
          )
        }
        {
          LoginState[0].matches('validated') && (
            <LoginVerified/>
          )
        }
      </Grid>
      <Grid item xs={1} sm={4}>
      </Grid>
    </Grid>
  )
}

export default Login