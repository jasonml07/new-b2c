import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
// MUI
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import InputAdornment from '@mui/material/InputAdornment';
// Machine
import { TravelRezState } from './machines/state'
import { useActor } from '@xstate/react'
// Form
import { useFormik } from 'formik'

function LoginPin ({
  errors,
  resending,
  submitting,
  onResend,
  onSubmit
}) {
  const form = useFormik({
    initialValues: {
      pin: ''
    },
    onSubmit(values) {
      onSubmit(values)
    }
  })
  useEffect(() => {
    if(errors) {
      const fields = Object.keys(errors)
      for(let i=0; i < fields.length; i++) {
        const field = fields[i]
        form.setFieldError(field, errors[field])
      }
    }
  }, [errors])
  return (
    <Paper elevation={5} sx={{padding: 5, mt: 10}}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h3" align="center" style={{color: 'primary.main'}}>TravelRez</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            size="small"
            label="Enter PIN code"
            variant="outlined"
            name="pin"
            value={form.values.pin}
            onChange={form.handleChange}
            error={Boolean(form.errors.pin)}
            helperText={Boolean(form.errors.pin) && form.errors.pin}
            InputProps={{
              endAdornment: <InputAdornment position="end"><Button onClick={onResend}>Resend</Button></InputAdornment>
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <Button fullWidth size="large" variant="contained" onClick={form.handleSubmit}>
            { submitting ?'Validating. . .' : (resending ?'Resending. . .' :'Submit')}
          </Button>
        </Grid>
      </Grid>
    </Paper>
  )
}
LoginPin.propTypes = {
  errors: PropTypes.object,
  resending: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  onResend: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default LoginPin