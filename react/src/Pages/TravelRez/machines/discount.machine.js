import { createMachine, assign } from 'xstate'
import axios from 'axios'

export default createMachine({
  id: 'Discount',
  initial: 'idle',
  on: {
    SEARCH: {
      actions: ['setSearch']
    }
  },
  context: {
    search: '',
    list: [],
    failure: null,
    current: null
  },
  states: {
    idle: {
      on: {
        CREATE: 'create.show',
        FETCH: 'list.process',
        DELETE: {
          target: 'delete.confirm',
          actions: ['setCurrent']
        }
      }
    },
    list: {
      states: {
        process: {
          invoke: {
            src: 'fetching',
            onDone: {
              target: '#Discount.idle',
              actions: ['setList']
            },
            onError: {
              target: 'failure'
            }
          }
        },
        failure: {

        },
      }
    },
    create: {
      id: 'Create',
      states: {
        show: {
          on: {
            SUBMIT: 'process',
            CANCEL: '#Discount.idle'
          }
        },
        process: {
          invoke: {
            src: 'creating',
            onDone: {
              target: '#Discount.idle',
              actions: ['addList']
            },
            onError: {
              target: 'failure',
              actions: ['setFailure']
            }
          }
        },
        // success: {},
        failure: {
          on: {
            SUBMIT: 'process'
          }
        }
      }
    },
    delete: {
      states: {
        confirm: {
          on: {
            CANCEL: '#Discount.idle',
            CONFIRM: 'process'
          }
        },
        process: {
          invoke: {
            src: 'deleting',
            onDone: {
              target: '#Discount.idle',
              actions: ['removeDiscount']
            },
            onError: {
              target: 'failure'
            }
          }
        },
        failure: {}
      }
    }
  }
}, {
  actions: {
    removeDiscount: assign({
      list(ctx, event) {
        const list = [...ctx.list]
        for(let i=0; i < list.length; i++) {
          const item = list[i]
          if(ctx.current.id == item.id) {
            list.splice(i, 1)
            break;
          }
        }
        return list
      }
    }),
    setCurrent: assign({
      current(ctx, event) {
        return event.data
      }
    }),
    addList: assign({
      list(ctx, event) {
        return [event.data, ...ctx.list]
      }
    }),
    setList: assign({
      list(ctx, event) {
        return event.data
      }
    }),
    setFailure: assign({
      failure(ctx, event) {
        return event.data.response
      }
    }),
    setSearch: assign({
      search(ctx, event) {
        return event.data
      }
    })
  },
  services: {
    deleting(ctx, event) {
      console.log(ctx.current)
      return axios.get('/api/v2/travelrez/discount/delete?id='+ctx.current._id['$id'])
        .then(response => response.data)
    },
    fetching(ctx, event) {
      return axios.get('/api/v2/travelrez/discounts')
        .then(response => response.data)
    },
    creating(ctx, event) {
      return axios.post('/api/v2/travelrez/discount/create', event.data)
        .then(response => response.data)
    }
  }
})