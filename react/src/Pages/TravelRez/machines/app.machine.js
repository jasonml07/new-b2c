import { createMachine } from 'xstate'
import Cookies from 'js-cookie'
import axios from 'axios'

export default createMachine({
  id: 'App',
  initial: 'idle',
  context: {

  },
  states: {
    idle: {
      on: {
        CHECK: 'check.authenticate'
      }
    },
    check: {
      states: {
        authenticate: {
          invoke: {
            src: 'authenticating',
            onDone: 'success',
            onError: 'failure'
          }
        },
        success: {
          on: {
            LOGOUT: {
              actions: ['logout']
            }
          }
        },
        failure: {},
      }
    }
  }
}, {
  actions: {
    logout(ctx, event) {
      axios.defaults.headers.common['Authorization'] = ''
      Cookies.remove('tztokn')
      window.location.reload()
    }
  },
  services: {
    authenticating(ctx, event) {
      // return new Promise((resolve, reject) => {
      //   setTimeout(() => {
      //     resolve(true)
      //   }, 2000)
      // })
      return axios.get('/api/v2/travelrez/check')
    }
  }
})