import React, { createContext } from 'react';
import { useInterpret } from '@xstate/react';
import LoginMachine from './login.machine'
import AppMachine from './app.machine'
import DiscountMachine from './discount.machine'

export const TravelRezState = createContext({});

export const TravelRezStateProvider = (props) => {
  const AppState = useInterpret(AppMachine)
  const LoginState = useInterpret(LoginMachine)
  const DiscountState = useInterpret(DiscountMachine)
  return (
    <TravelRezState.Provider value={{ LoginState, AppState, DiscountState }}>
      {props.children}
    </TravelRezState.Provider>
  )
}

