import axios from 'axios'
import { createMachine, assign } from 'xstate'
import Cookies from 'js-cookie'

export default createMachine({
  id: 'Login',
  initial: 'form',
  context: {
    email: '',
    failure: null,
    otpHash: ''
  },
  states: {
    form: {
      initial: 'show',
      states: {
        show: {
          on: {
            SUBMIT: {
              target: 'process',
              actions: ['setEmail']
            }
          }
        },
        process: {
          invoke: {
            src: 'sendingOtp',
            onDone: {
              target: '#Login.pin',
              actions: ['setOtpHash']
            },
            onError: {
              target: 'failure',
              actions: ['setFailure']
            }
          }
        },
        failure: {
          on: {
            SUBMIT: 'process'
          }
        }
      }
    },
    pin: {
      id: 'Pin',
      initial: 'show',
      states: {
        show: {
          on: {
            RESEND: 'resend.process',
            SUBMIT: 'process'
          }
        },
        resend: {
          states: {
            process: {
              invoke: {
                src: 'sendingOtp',
                onDone: {
                  target: '#Pin.show',
                  actions: ['setOtpHash']
                },
                onError: {
                  target: 'failure',
                  actions: ['setFailure']
                }
              }
            },
            failure: {
              on: {
                RESEND: 'process'
              }
            }
          }
        },
        process: {
          invoke: {
            src: 'sendingPin',
            onDone: {
              target: '#Login.validated',
              actions: ['setCookie', 'reload']
            },
            onError: {
              target: 'failure',
              actions: ['setFailure']
            }
          }
        },
        success: {},
        failure: {
          on: {
            SUBMIT: 'process'
          }
        },
      }
    },
    validated: {
      id: 'Validated',
      entry: {

      }
    }

  }
}, {
  actions: {
    setCookie(ctx, event) {
      Cookies.set('tztokn', event.data.token)
    },
    setOtpHash: assign({
      otpHash(ctx, event) {
        return event.data.hash
      }
    }),
    setEmail: assign({
      email(ctx, event) {
        return event.data.email
      }
    }),
    setFailure: assign({
      failure(ctx, event) {
        // console.log(event.data)
        return event.data.response
      }
    }),
    reload(ctx, event) {
      setTimeout(() => {
        window.location.reload()
      }, 1000)
    }
  },
  services: {
    sendingOtp(ctx, event) {
      // return new Promise((resolve, reject) => {
      //   setTimeout(() => {
      //     resolve(true)
      //   }, 2000)
      // })
      return axios.post('/api/v2/travelrez/login', {email: ctx.email})
        .then(response => response.data)
    },
    sendingPin(ctx, event) {
      // return new Promise((resolve, reject) => {
      //   setTimeout(() => {
      //     resolve(true)
      //   }, 2000)
      // })
      return axios.post('/api/v2/travelrez/otp', {hash: ctx.otpHash, pin: event.data.pin})
        .then(response => response.data)
    }
  }
})