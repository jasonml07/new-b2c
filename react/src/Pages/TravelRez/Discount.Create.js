import React, { useContext, useEffect } from 'react'
import PropTypes from 'prop-types'
// Validator
import { useFormik } from 'formik'
import * as yup from 'yup'
// MUI
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
// Icons
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import ErrorIcon from '@mui/icons-material/Error';


const DEFAULT_VALUES = {
  code: '',
  availabilityDate: {
    start: '',
    end: '',
  },
  category: '',
  type: '',
  value: '',
  message: '',
  numberOfPax: '',
  products: [],
  agencies: [],
}

function DiscountCreate ({
  errors,
  submitting,
  open,
  onClose,
  onSubmit
}) {
  const form = useFormik({
    validateOnChange: false,
    initialValues: {...DEFAULT_VALUES},
    validationSchema: yup.object({
      code: yup.string()
        .required('The code field is required.'),
      availabilityDate: yup.object({
        start: yup.string()
          .required('The availability start field is required.'),
        end: yup.string()
          .required('The availability end field is required.'),
      }),
      type: yup.string()
        .required('The type field is required.'),
      category: yup.string()
        .required('The category field is required.'),
      value: yup.string()
        .required('The value field is required.')
        .test(
          'decimal',
          'The value field is not valid amount.',
          function(value, context) {
            return /^\d+\.?\d+$/.test(value)
          }
        ),
      numberOfPax: yup.string()
        .test(
          'number-only',
          'The pax field only allows number.',
          function(value, context) {
            return !/\d+/.test(value)
          }
        ),
      // products: 
      message: yup.string()
        .required('The message field is required.'),
      // agencies: yup.array()
    }),
    onSubmit(values) {
      onSubmit(values)
    }
  })
  const handleCancel = () => {
    onClose()
  }
  const handleSubmit = () => {

  }
  const handleDateChange = (name, value) => {
    form.setFieldValue(name, value.format('YYYY-MM-DD'))
  }
  const handleAgencyChange = (e) => {
    const value = e.target.value
    const agencies = [...form.values.agencies]
    if(agencies.includes(value)) {
      for(let i=0; i < agencies.length; i++) {
        const agency = agencies[i]
        if(agency === value) {
          agencies.splice(i, 1)
          break
        }
      }
    } else {
      agencies.push(value)
    }
    form.setFieldValue('agencies', agencies)
  }
  useEffect(() => {
    if(errors) {
      const fields = Object.keys(errors)

      for(let i=0; i < fields.length; i++) {
        const field = fields[i]
        form.setFieldError(field, errors[field])
      }
    }
  }, [errors])
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Create Discount</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Create new discount for the tour.
        </DialogContentText>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              autoFocus
              margin="dense"
              id="code"
              name="code"
              label="Code"
              fullWidth
              variant="outlined"
              value={form.values.code}
              onChange={form.handleChange}
              error={Boolean(form.errors.code)}
              helperText={form.errors.code}
            />
          </Grid>
          <Grid item xs={12}>
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <DatePicker
                minDate={new Date()}
                label="Availability start"
                value={form.values.availabilityDate.start}
                onChange={(newValue) => handleDateChange('availabilityDate.start', newValue)}
                renderInput={(params) => <TextField fullWidth {...params} error={Boolean(form.errors.availabilityDate?.start)} helperText={form.errors.availabilityDate?.start}/>}
              />
            </LocalizationProvider>
          </Grid>
          <Grid item xs={12}>
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <DatePicker
                minDate={Boolean(form.values.availabilityDate.start) ?form.values.availabilityDate.start :new Date()}
                label="Availability end"
                value={form.values.availabilityDate.end}
                onChange={(newValue) => handleDateChange('availabilityDate.end', newValue)}
                renderInput={(params) => <TextField fullWidth {...params} error={Boolean(form.errors.availabilityDate?.end)} helperText={form.errors.availabilityDate?.end}/>}
              />
            </LocalizationProvider>
          </Grid>
          <Grid item xs={12}>
            <FormControl fullWidth error={Boolean(form.errors.category)}>
              <InputLabel id="category-select-label">Category</InputLabel>
              <Select
                labelId="category-select-label"
                id="category-select"
                name="category"
                value={form.values.category}
                label="Category"
                onChange={form.handleChange}
              >
                <MenuItem value={''}><i>None</i></MenuItem>
                <MenuItem value={'PRODUCT'}>Product</MenuItem>
                <MenuItem disabled={true} value={'FLIGHT'}>Flight</MenuItem>
              </Select>
            </FormControl>
            {
              Boolean(form.errors.category) && (
                <FormHelperText error>{form.errors.category}</FormHelperText>
              )
            }
          </Grid>
          <Grid item xs={12}>
            <FormControl fullWidth error={Boolean(form.errors.type)}>
              <InputLabel id="type-select-label">Value type</InputLabel>
              <Select
                labelId="type-select-label"
                id="type-select"
                name="type"
                value={form.values.type}
                label="Value type"
                onChange={form.handleChange}
              >
                <MenuItem value={''}><i>None</i></MenuItem>
                <MenuItem value={'PERCENTAGE'}>Percentage</MenuItem>
                <MenuItem value={'AMOUNT'}>Amount</MenuItem>
                <MenuItem disabled={true} value={'AMOUNT_PERSON'}>Amount per person/guest</MenuItem>
                <MenuItem disabled={true} value={'LESS_AMOUNT_PERSON'}>Less amount per person/guest</MenuItem>
              </Select>
            </FormControl>
            {
              Boolean(form.errors.type) && (
                <FormHelperText error>{form.errors.type}</FormHelperText>
              )
            }
          </Grid>
          <Grid item xs={12}>
            <TextField
              margin="dense"
              id="value"
              name="value"
              label="Value/Amount"
              fullWidth
              variant="outlined"
              value={form.values.value}
              onChange={form.handleChange}
              error={Boolean(form.errors.value)}
              helperText={form.errors.value}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              disabled={true}
              margin="dense"
              id="numberOfPax"
              name="numberOfPax"
              label="Pax/Guests"
              fullWidth
              variant="outlined"
              value={form.values.numberOfPax}
              onChange={form.handleChange}
              error={Boolean(form.errors.numberOfPax)}
              helperText={form.errors.numberOfPax}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              disabled={true}
              margin="dense"
              id="products"
              name="products"
              label="Products"
              fullWidth
              variant="outlined"
              value={form.values.products}
              onChange={form.handleChange}
              error={Boolean(form.errors.products)}
              helperText={form.errors.products}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              margin="dense"
              id="message"
              name="message"
              label="Message"
              fullWidth
              variant="outlined"
              multiline={true}
              rows={4}
              value={form.values.message}
              onChange={form.handleChange}
              error={Boolean(form.errors.message)}
              helperText={form.errors.message}
            />
          </Grid>
          <Grid item xs={12}>
            <FormGroup>
              <FormControlLabel control={<Checkbox checked={form.values.agencies.includes('EuropeTravelDeals')} onChange={handleAgencyChange} value='EuropeTravelDeals'/>} label="EuropeTravelDeals" />
              <FormControlLabel disabled={true} control={<Checkbox checked={form.values.agencies.includes('iFlyGo')} onChange={handleAgencyChange} value='iFlyGo' />} label="iFlyGo" />
            </FormGroup>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button disabled={submitting} onClick={handleCancel} color="error">Cancel</Button>
        <Button disabled={submitting} onClick={form.handleSubmit}>{submitting ?'Creating. . .' :'Submit'}</Button>
      </DialogActions>
    </Dialog>
  )
}
DiscountCreate.propTypes = {
  errors: PropTypes.object,
  submitting: PropTypes.bool,
  open: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default DiscountCreate