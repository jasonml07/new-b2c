import React, { useContext, useEffect } from 'react'
import Cookies from 'js-cookie'
import axios from 'axios'
// MUI
import Typography from '@mui/material/Typography';
// Components
import Login from './Login'
import Checking from './Checking'
import Dashboard from './Dashboard'
// Machine
import { TravelRezStateProvider, TravelRezState } from './machines/state'
import { useActor } from '@xstate/react'

function App() {
  const Services = useContext(TravelRezState)
  const AppState = useActor(Services.AppState)
  useEffect(() => {
    const token = Cookies.get('tztokn')
    if(token) {
      axios.defaults.headers.common['Authorization'] = token
    }
    AppState[1]('CHECK')
  }, [])
  return (() => {
    // return (
    //   <Checking />
    // )
    if(AppState[0].matches('check.authenticate')) {
      // return (
      //   <Checking />
      // )
      return ''
    }
    if(AppState[0].matches('check.success')) {
      return (
        <Dashboard />
      )
    }

    return (
      <Login />
    )
  })()
}

function TravelRez () {

  return (
    <TravelRezStateProvider>
      <App />
    </TravelRezStateProvider>
  )
}

export default TravelRez