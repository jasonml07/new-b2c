import React, { useContext } from 'react'
import PropTypes from 'prop-types'
// MUI
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import InputAdornment from '@mui/material/InputAdornment';
// Icons
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
// Machine
import { TravelRezState } from './machines/state'
import { useActor } from '@xstate/react'
// Form
import { useFormik } from 'formik'

function LoginVerified ({
  
}) {
  return (
    <Paper elevation={5} sx={{padding: 5, mt: 10}}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h1" align="center" sx={{color: 'success.main'}}>
            <CheckCircleIcon style={{ fontSize: '2em' }}/>
          </Typography>
          <Typography align="center" variant="h4">You are now login. You are now redirected</Typography>
        </Grid>
      </Grid>
    </Paper>
  )
}
LoginVerified.propTypes = {

}

export default LoginVerified