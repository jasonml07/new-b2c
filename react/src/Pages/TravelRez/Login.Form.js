import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
// MUI
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
// Machine
import { TravelRezState } from './machines/state'
import { useActor } from '@xstate/react'
// Form
import { useFormik } from 'formik'

function LoginForm ({
  submitting,
  onSubmit,
  errors
}) {
  const form = useFormik({
    initialValues: {
      email: ''
    },
    onSubmit(values) {
      onSubmit(values)
    }
  })
  useEffect(() => {
    // console.log('errors', errors)
    if(errors) {
      const fields = Object.keys(errors)
      for(let i=0; i < fields.length; i++) {
        const field = fields[i]
        form.setFieldError(field, errors[field])
      }
    }
  }, [errors])
  return (
    <Paper elevation={5} sx={{padding: 5, mt: 10}}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h3" align="center" style={{color: 'primary.main'}}>TravelRez</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            size="small"
            label="Email address"
            variant="outlined"
            name="email"
            value={form.values.email}
            onChange={form.handleChange}
            error={Boolean(form.errors.email)}
            helperText={Boolean(form.errors.email) && form.errors.email}
          />
        </Grid>
        <Grid item xs={12}>
          <Button fullWidth size="large" variant="contained" onClick={form.handleSubmit}>
            { submitting ?'Sending OTP. . .' :'SEND OTP'}
          </Button>
        </Grid>
      </Grid>
    </Paper>
  )
}
LoginForm.propTypes = {
  submitting: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  errors: PropTypes.object
}

export default LoginForm