import React, { useContext, useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import PropTypes from 'prop-types';
// Material
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
// Icons
import ErrorIcon from '@mui/icons-material/Error';
// Components
import DetailsAndSelection from './Views/DetailsAndSelection'
import PageNotFound from '../components/PageNotFound'
// Context
import { GlobalState } from '../global.state'
import { useActor } from '@xstate/react'

function Content({
  appTheme
}) {
  // const history = useHistory()
  const params = useParams()
  const theme = useTheme()
  const Services = useContext(GlobalState)
  const AppService = useActor(Services.AppService)
  // const params = useState({})

  useEffect(() => {
    AppService[1]({type: 'FETCH', data: params})
  }, []);
  // const handleClick = () => {
  //   console.log('handle click')
  //   history.push('/review/123')
  // }
  const display = () => {
    if(['fetch.process', 'idle'].some(AppService[0].matches)) {
      return <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={true}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    }
    if(AppService[0].matches('errors.paramNotDefine')) {
      return <Paper elavation={6} sx={{m:20, p:2}}>
        <Typography variant="h2" align="center"><ErrorIcon color="error" style={{fontSize: '1.5em'}}/></Typography>
        <Typography variant="h4" align="center">Required parameter is not defined!</Typography>
        <br />
      </Paper>
    }
    if(AppService[0].matches('fetch.failure.withMessage')) {
      return <Paper elavation={6} sx={{m:20, p:2}}>
        <Typography variant="h2" align="center"><ErrorIcon color="error" style={{fontSize: '1.5em'}}/></Typography>
        <Typography variant="h4" align="center">{AppService[0].context.errorMessage}</Typography>
        <br />
        <Typography align="center">
          <Button onClick={() => AppService[1]({type: 'RETRY', data: params})}>
            Click to try again!
          </Button>
        </Typography>
      </Paper>
    }
    if(AppService[0].matches('fetch.failure')) {
      return <PageNotFound onRetry={() => AppService[1]({type: 'RETRY', data: params})}/>
    }

    return (
      <Grid container spacing={1} sx={{width: '100%', m: 0}}>
        <appTheme.Header />
        <DetailsAndSelection />
        <appTheme.Footer />
      </Grid>
    )
  }
  return (
    <>
      { display ()}
    </>
  )
}

Content.propTypes = {
  appTheme: PropTypes.object
}
Content.defaultValues = {
  appTheme: {}
}

export default Content