import React, { useState, useContext, useEffect, Fragment } from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import { useParams } from 'react-router-dom'
// Material
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { useTheme } from '@mui/material/styles';
// Icons
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import ErrorIcon from '@mui/icons-material/Error';
import PersonIcon from '@mui/icons-material/Person';
import GroupIcon from '@mui/icons-material/Group';
import AirplanemodeActiveIcon from '@mui/icons-material/AirplanemodeActive';
import HotelIcon from '@mui/icons-material/Hotel';
// Context
import { GlobalState } from '../global.state'
import { useActor } from '@xstate/react'

function Review({

}) {
  const theme = useTheme();
  const Services = useContext(GlobalState)
  const ReviewState = useActor(Services.ReviewService)
  const information = ReviewState[0].context.information
  const flight = information?.flight
  const beds = (information?.beds || [])
  const extrabills = (information?.extrabills || [])
  const { bookingId } = useParams()
  const [totalPrice, setTotalPrice] = useState()

  const getTotalPrice = () => {
    let _price = 0
    beds.forEach((item) => {
      _price+=item.totalPrice*item.quantity
    })
    if(Boolean(flight)) {
      _price+=flight.totalPrice*flight?.passengers
    }
    extrabills.forEach((o) => {
      _price+=o.totalPrice*o.quantity
    })
    setTotalPrice(_price)
  }
  useEffect(() => {
    ReviewState[1]({type: 'FETCH', data: bookingId || ''})
  }, [])
  useEffect(() => {
    if(ReviewState[0].matches('fetch.success')) {
      getTotalPrice()
    }
  }, [ReviewState[0]])
  
  return (() => {
    if(!Boolean(bookingId)) {
      return (
        <Paper elavation={6} sx={{m:20, p:2}}>
          <Typography variant="h2" align="center"><ErrorIcon color="error" style={{fontSize: '1.5em'}}/></Typography>
          <Typography variant="h4" align="center">Invalid booking reference!</Typography>
          <br />
        </Paper>
      )
    }

    if(ReviewState[0].matches('fetch.process')) {
      return (
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={true}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      )
    }
    if(ReviewState[0].matches('fetch.failure')) {
      return (
        <Paper elavation={6} sx={{m:20, p:2}}>
          <Typography variant="h2" align="center"><ErrorIcon color="error" style={{fontSize: '1.5em'}}/></Typography>
          <Typography variant="h4" align="center">Oops! Failed to get booking detail!</Typography>
          <br />
          <Typography align="center">
            <Button onClick={() => ReviewState[1]('RETRY')}>
              Click to try again!
            </Button>
          </Typography>
        </Paper>
      )
    }

    return (
      <Grid container spacing={2} sx={{p: 10}}>
        <Grid item xs={12}>
          <Paper elevation={1} sx={{p: 2}}>
            <Typography variant='h4'>{information?.product?.name}</Typography>
            <hr />
            <Typography dangerouslySetInnerHTML={{ __html: information?.product?.inclusionDescription}} />
              {/*{{information?.product?.inclusionDescription}}*/}
            {/*</Typography>*/}
          </Paper>
        </Grid>
        <Grid container item xs={8} spacing={2}>
          <Grid item xs={12}>
            <Paper elavation={1} sx={{p: 2}}>
              <Typography variant='h5'><PersonIcon />&nbsp;Voucher Information</Typography>
              <hr />
              <Grid container spacing={1}>
                <Grid item xs={4}>
                  <Typography>Name</Typography>
                </Grid>
                <Grid item xs={8}>
                  <Typography>{information?.voucherFirstname +' '+ information?.voucherLastname}</Typography>
                </Grid>
                
                <Grid item xs={4}>
                  <Typography>Email Address</Typography>
                </Grid>
                <Grid item xs={8}>
                  <Typography>{information?.voucherEmailAddress}</Typography>
                </Grid>
                
                <Grid item xs={4}>
                  <Typography>Phone Number</Typography>
                </Grid>
                <Grid item xs={8}>
                  <Typography>{information?.voucherPhoneNumber}</Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper elavation={1} sx={{p:2}}>
              <Typography variant='h5'><GroupIcon />&nbsp;Passengers</Typography>
              <hr />
              {
                (() => {
                  const guests = information?.guests || []
                  if(guests.length > 10) {
                    const group2 = guests.splice(Math.ceil(guests.length/2))
                    return (
                      <Grid container spacing={1}>
                        <Grid xs={6}>
                          {
                            guests.map((item, index) => (
                              <Typography key={'guest-'+index}>{item.title+' '+item.firstName+' '+item.lastName}</Typography>
                            ))
                          }
                        </Grid>
                        <Grid xs={6}>
                          {
                            group2.map((item, index) => (
                              <Typography key={'guest-'+index}>{item.title+' '+item.firstName+' '+item.lastName}</Typography>
                            ))
                          }
                        </Grid>
                      </Grid>
                    )
                  }

                  return (
                    <Grid xs={6}>
                      {
                        guests.map((item, index) => (
                          <Typography key={'guest-'+index}>{item.title+' '+item.firstName+' '+item.lastName}</Typography>
                        ))
                      }
                    </Grid>
                  )
                })()
              }
            </Paper>
          </Grid>
          {
            (() => {
              if(!Boolean(flight)) {
                return;
              }

              return (
                <Grid item xs={12}>
                  <Paper elavation={1} sx={{p: 2}}>
                    <Typography variant='h5'><AirplanemodeActiveIcon />&nbsp;Fligth Information</Typography>
                    <hr />
                    <Grid container spacing={1}>
                      <Grid item xs={4}>City</Grid>
                      <Grid item xs={8}>{flight?.departureCity}</Grid>

                      <Grid item xs={4}>Class</Grid>
                      <Grid item xs={8}>{flight?.preferredClass}</Grid>
                      
                      <Grid item xs={4}>Departure Date</Grid>
                      <Grid item xs={8}>{moment(flight?.departureDate, 'DD/MM/YYYY').format('MMMM D, YYYY')}</Grid>
                      
                      <Grid item xs={4}>Return Date</Grid>
                      <Grid item xs={8}>{moment(flight?.returnDate, 'DD/MM/YYYY').format('MMMM D, YYYY')}</Grid>
                      
                      <Grid item xs={4}>Price</Grid>
                      <Grid item xs={8}>${Number(flight?.price).toLocaleString()}</Grid>
                    </Grid>
                  </Paper>
                </Grid>
              )
            })()
          }
          {/*// Discount*/}
          {/*// Extra bills*/}
          {/*// Packages*/}
          {/*// Agent Information*/}
        </Grid>
        <Grid item xs={4}>
          <Paper elavation={1} sx={{p: 2}}>
            <Typography variant='h5' align="center">PRODUCTS SUMMARY</Typography>
            <hr />
            <Grid container spacing={1}>
              <Grid item xs={4}>
                <Typography>Tour Start</Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography>{moment(information?.tour?.fromDayText, 'DD MMM YYYY').format('MMMM D, YYYY')}</Typography>
              </Grid>

              <Grid item xs={4}>
                <Typography>Tour End</Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography>{moment(information?.tour?.toDayText, 'DD MMM YYYY').format('MMMM D, YYYY')}</Typography>
              </Grid>
              
              <Grid item xs={4}>
                <Typography>Days</Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography>{information?.tour?.duration}</Typography>
              </Grid>
            </Grid>
            <br />
            <Typography variant='h6' sx={{display: 'flex', alignItems: 'center'}}><HotelIcon />&nbsp;Room/Cabin Occupancy</Typography>
            <hr />
            <Grid container spacing={1}>
              {
                beds.map((item, index) => (
                  <Fragment key={'cabin-room-'+index}>
                    <Grid item xs={7}>
                      <Typography>{item.name+' X'+item.quantity}</Typography>
                    </Grid>
                    <Grid item xs={5}>
                      <Typography>${Number(item.price*item.quantity).toLocaleString()}</Typography>
                    </Grid>
                  </Fragment>
                ))
              }
            </Grid>
            {
              (() => {
                if(extrabills.length > 0) {
                  return (
                    <Fragment>
                      <br />
                      <Typography variant='h6' sx={{display: 'flex', alignItems: 'center'}}><TurnedInIcon />&nbsp;Extra Bills</Typography>
                      <hr />
                      <Grid container spacing={1}>
                        {
                          extrabills.map((eb, index) => (
                            <Fragment key={'extrabill-'+index}>
                              <Grid item xs={7}>
                                <Typography>{eb.name+' X'+eb.quantity}</Typography>
                              </Grid>
                              <Grid item xs={5}>
                                <Typography>${Number(eb.totalPrice*eb.quantity).toLocaleString()}</Typography>
                              </Grid>
                            </Fragment>
                          ))
                        }
                      </Grid>
                    </Fragment>
                  )
                }
              })()
            }
            {
              (() => {
                if(!Boolean(flight)) {
                  return ''
                }
                return (
                  <Grid container spacing={1}>
                    <Grid item xs={7}>
                      <Typography>Flight X{flight?.passengers}</Typography>
                    </Grid>
                    <Grid item xs={5}>
                      <Typography>${Number(flight?.price * flight?.passengers).toLocaleString()}</Typography>
                    </Grid>
                  </Grid>
                )
              })()
            }
            <hr />
            <Typography variant='h3' align="right" color="error">${Number(totalPrice).toLocaleString()}</Typography>
            <Typography align="right">TOTAL PRICE</Typography>
          </Paper>
        </Grid>
      </Grid>
    )
  })()
}

export default Review