import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// Material
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
// Component
import Inputs from '../../components/Inputs'

function RoomSelectionPackages({
  packages,
  maxPax,
  onChange
}) {

  if(packages.length == 0) {
    return ''
  }

  return (
    <Grid item xs={6} container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h5">Add Ons/Packages</Typography>
        <Typography>&nbsp;</Typography>
        <hr />
      </Grid>
      {
        packages.map((item, index) => (
          <Grid key={'package-'+index} item xs={12} style={{display: 'flex', justifyContent: 'end'}}>
            <Typography style={{flexGrow: 1}}>
              <span>{item.name} (${item.price})</span>
            </Typography>
            <div style={{width: 200}}>
              <Inputs.AddMinus
                label={""}
                value={item.quantity}
                size="small"
                maxValue={maxPax}
                onChange={(e) => onChange(e, item.index)}
              />
            </div>
          </Grid>
        ))
      }
    </Grid>
  )
}
RoomSelectionPackages.propTypes = {
  maxPax: PropTypes.number,
  packages: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
}
RoomSelectionPackages.defaultProps = {
  maxPax: 0
}


export default RoomSelectionPackages