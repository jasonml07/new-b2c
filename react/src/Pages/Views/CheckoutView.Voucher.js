import React, {} from 'react';
import PropTypes from 'prop-types';
// Material
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';

function CheckoutViewVoucher({
  formik
}) {

  return (
    <Grid container spacing={2}>
      <Grid item xs={2}>
        <FormControl size="small" fullWidth error={formik.touched.voucher?.title && Boolean(formik.errors.voucher?.title)}>
          <InputLabel id="demo-simple-select-label">Title</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="title"
            value={formik.values.voucher.title}
            onChange={(event) => formik.setFieldValue('voucher.title', event.target.value, true)}
          >
            <MenuItem value="">Title</MenuItem>
            <MenuItem value="Mr">Mr.</MenuItem>
            <MenuItem value="Ms">Ms.</MenuItem>
            <MenuItem value="Mrs">Mrs.</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={4}>
        <TextField
          size="small"
          fullWidth
          variant="outlined"
          label="First name"
          value={formik.values.voucher.firstName}
          onChange={(event) => formik.setFieldValue('voucher.firstName', event.target.value, true)}
          error={formik.touched.voucher?.firstName && Boolean(formik.errors.voucher?.firstName)}
          helperText={formik.touched.voucher?.firstName && Boolean(formik.errors.voucher?.firstName) && formik.errors.voucher?.firstName}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          size="small"
          fullWidth
          variant="outlined"
          label="Last name"
          value={formik.values.voucher.lastName}
          onChange={(event) => formik.setFieldValue('voucher.lastName', event.target.value, true)}
          error={formik.touched.voucher?.lastName && Boolean(formik.errors.voucher?.lastName)}
          helperText={formik.touched.voucher?.lastName && Boolean(formik.errors.voucher?.lastName) && formik.errors.voucher?.lastName}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          size="small"
          fullWidth
          variant="outlined"
          label="Email address"
          value={formik.values.voucher.emailAddress}
          onChange={(event) => formik.setFieldValue('voucher.emailAddress', event.target.value, true)}
          error={formik.touched.voucher?.emailAddress && Boolean(formik.errors.voucher?.emailAddress)}
          helperText={formik.touched.voucher?.emailAddress && Boolean(formik.errors.voucher?.emailAddress) && formik.errors.voucher?.emailAddress}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          size="small"
          fullWidth
          variant="outlined"
          label="Phone number"
          value={formik.values.voucher.phoneNumber}
          onChange={(event) => formik.setFieldValue('voucher.phoneNumber', event.target.value, true)}
          error={formik.touched.voucher?.phoneNumber && Boolean(formik.errors.voucher?.phoneNumber)}
          helperText={formik.touched.voucher?.phoneNumber && Boolean(formik.errors.voucher?.phoneNumber) && formik.errors.voucher?.phoneNumber}
        />
      </Grid>
    </Grid>
  )
}
CheckoutViewVoucher.propTypes = {
  formik: PropTypes.object.isRequired
}

export default CheckoutViewVoucher
