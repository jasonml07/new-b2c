import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
// Material
import Alert from '@mui/material/Alert';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import { styled } from '@mui/material/styles';
import Fade from '@mui/material/Fade';
// Icons
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
// Components
import TourSelection from './TourSelection'
import RoomSelection from './RoomSelection'
import CheckoutView from './CheckoutView'
// Machine
import { GlobalState } from '../../global.state'
import { useActor } from '@xstate/react'

const Tour = (
    <div>
      <TourSelection />
    </div>
  )
const Room = (
    <div>
      <RoomSelection />
    </div>
  )

function DetailsAndSelection({

}) {
  const Services = useContext(GlobalState)
  const AppService = useActor(Services.AppService)

  // const display = (
  //   <div>
  //    {
  //     AppService[0].matches('detail') && <TourSelection />
  //    }
  //    {
  //     AppService[0].matches('roomSelection') && <RoomSelection />
  //    }
  //   </div>
  // )
  return (
    <Grid container item xs={12} spacing={2} sx={{mx: 5}}>
      <Grid item xs={12} style={{paddingLeft: 0}}>
        <Paper elevation={0} sx={{p: 2}}>
          <Typography variant='h4'>{AppService[0].context.product?.name}</Typography>
          <hr />
          <Typography variant='h6' dangerouslySetInnerHTML={{__html: AppService[0].context.product?.summary}} />
        </Paper>
      </Grid>
      <Grid item xs={12} style={{paddingLeft: 0}}>
        <Fade in={AppService[0].matches('detail')} style={{display: AppService[0].matches('detail')?'block': 'none'}}>
          <div><TourSelection service={AppService} /></div>
        </Fade>
        <Fade in={AppService[0].matches('roomSelection')} style={{display: AppService[0].matches('roomSelection')?'block': 'none'}}>
          <div><RoomSelection service={AppService} /></div>
        </Fade>
        <Fade in={AppService[0].matches('checkout')} style={{display: AppService[0].matches('checkout')?'block': 'none'}}>
          <div><CheckoutView service={AppService} /></div>
        </Fade>
      </Grid>
      <Grid item xs={12} style={{paddingLeft: 0}}>
        <Paper elevation={0}>
          <Accordion defaultExpanded={true}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography variant="h6">ITINERARY</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography dangerouslySetInnerHTML={{__html: AppService[0].context.product?.detailed}} />
            </AccordionDetails>
          </Accordion>
          <Accordion defaultExpanded={true}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography variant="h6">INCLUSIONS</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography dangerouslySetInnerHTML={{__html: AppService[0].context.product?.inclusion}} />
            </AccordionDetails>
          </Accordion>
          <Accordion defaultExpanded={true}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography variant="h6">EXCLUSIONS</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography dangerouslySetInnerHTML={{__html: AppService[0].context.product?.exclusion}} />
            </AccordionDetails>
          </Accordion>
        </Paper>
      </Grid>
    </Grid>
  )
}
DetailsAndSelection.propTypes = {
  // Props here
}

export default DetailsAndSelection


