import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment'
// Material
import Alert from '@mui/material/Alert';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
// Calendar
import { makeStyles } from '@mui/styles';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
// import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { StaticDatePicker } from '@mui/x-date-pickers/StaticDatePicker';
import { PickersDay } from '@mui/x-date-pickers/PickersDay';
import endOfWeek from 'date-fns/endOfWeek';
import isSameDay from 'date-fns/isSameDay';
import isWithinInterval from 'date-fns/isWithinInterval';
import startOfWeek from 'date-fns/startOfWeek';
// Machine
import { GlobalState } from '../../../global.state'
import { useActor } from '@xstate/react'


const useStyles = makeStyles({
  staticCalendar: {
    ['& .MuiPickerStaticWrapper-content > div > div']: {
      width: '100%',
    },
    ['& .MuiCalendarPicker-root']: {
      width: '100%'
    },
    ['& .MuiTypography-root']: {
      width: '100%',
      fontWeight: 'bolder',
      fontSize: '1.1em'
    },
  }
})
const CalendarTour = styled(StaticDatePicker)(({theme, ...props}) => ({
  // ['& .MuiCalendarPicker-root']: {
  //   width: '100%'
  // }
}))

const CustomPickersDay = styled(PickersDay, {
  // shouldForwardProp: (prop) =>
  //   prop !== 'dayIsBetween' && prop !== 'isFirstDay' && prop !== 'isLastDay',
})(({ theme, istour, tour, isSelected, ...props }) => ({
  height: 50,
  width: 100,
  borderRadius: 0,
  ['&.Mui-selected']: {
    color: theme.palette.common.black,
    backgroundColor: theme.palette.common.white,
    // borderColor: theme.palette.success.main,
    // ['&:before']: {
    //   borderColor: theme.palette.success.main+' transparent transparent',
    // },
    // ['&:after']: {
    //   color: theme.palette.success.main,
    // },
    '&:hover, &:focus': {
      // color: theme.palette.common.white,
      backgroundColor: theme.palette.grey[200],
    },
  },
  ...(isSelected && {
    borderColor: theme.palette.success.main+' !important',
    ['&:before']: {
      borderColor: theme.palette.success.main+' transparent transparent',
    },
    ['&:after']: {
      color: theme.palette.success.main,
    },
  }),
  ...(istour && {
    ['&:before']: {
      content: '""',
      borderStyle: 'solid',
      borderWidth: '12px 12px 0 0',
      borderColor: (isSelected ?theme.palette.success.main :theme.palette.error.main)+' transparent transparent',
      position: 'absolute',
      top: 0,
      left: 0
    },
    ['&:after']: {
      content: '"$'+tour.displayPrice+'"',
      color: isSelected ?theme.palette.success.main :theme.palette.error.main,
      position: 'absolute',
      bottom: 0,
      left: 5
    },
    borderRadius: 0,
    border: '2px solid '+theme.palette.error.main,
    // backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.black,
    '&:hover, &:focus': {
      // color: theme.palette.common.white,
      // backgroundColor: theme.palette.error.light,
    },
  }),
  // ...(isFirstDay && {
  //   // borderRight: 'none',
  // }),
  // ...(isLastDay && {
  //   // borderLeft: 'none',
  // }),
}));


function Calendar({

}) {
  const classes = useStyles()
  const Services = useContext(GlobalState)
  const AppService = useActor(Services.AppService)
  const tours = AppService[0].context.tours
    .reduce((acc, curr) => {
      // const dateKey = `${(curr.fromMonth)}-${curr.fromDay}-${curr.fromYear}`
      const dateKey = moment({
        year: curr.fromYear,
        month: curr.fromMonth-1,
        day: curr.fromDay
      }).format('MM-DD-YYYY')
      if(!Boolean(acc[dateKey])) {
        acc[dateKey] = curr
      }
      return acc
    }, {})
  const tourKeys = Object.keys(tours);
  // console.log('tourKeys', tourKeys)
  // const lastDate = new Date(tourKeys[tourKeys.length-1])
  const startDate = moment(tourKeys[0],'MM-DD-YYYY').subtract(1, 'days')._d
  const lastDate = moment(new Date(tourKeys[tourKeys.length-1])).add(1,'months').set('date', 1).subtract(1, 'days')._d
  const [value, setValue] = useState(startDate);
  const [previousMonth, setPreviousMonth] = useState(startDate);
  const [currentMonth, setCurrentMonth] = useState(moment(previousMonth).add(1, 'months')._d);
  
  const handleOnChange = (newValue) => {
    if(Boolean(newValue)) {
      const dateKey = moment(newValue).format('MM-DD-YYYY')
      setValue(newValue);
      setCurrentMonth(newValue)
      AppService[1]({type: 'ROOM_SELECTION', data: tours[dateKey]})
    }
  }
  const handlePreviousMonthChange = (date) => {
    setCurrentMonth(moment(date).add(1, 'months'))
  }
  const handleMonthChange = (date) => {
    setPreviousMonth(moment(date).subtract(1, 'months'))
  }
  // const tours = AppService[0].context.tours.map((item) => ({...item, supplement_avail_name_data: null}))
  
  useEffect(() => {
    setValue(startDate)
  }, [])

  // console.log('tourKeys', tourKeys, 'lastDate', lastDate)
  const renderWeekPickerDay = (date, selectedDates, pickersDayProps) => {
    // console.log('tours', tours)
    // if (!value) {
    //   return <CustomPickersDay {...pickersDayProps} />;
    // }
    const _value = moment(value).format('MM-DD-YYYY')
    const dateKey = moment(date).format('MM-DD-YYYY')
    const tour = tours[dateKey]
    const isTour = Boolean(tour)
    const isSelected = dateKey==_value
    // console.log(dateKey, _value, isSelected)
    // console.log('renderWeekPickerDay', dateKey)
    // console.log('dateKey', dateKey, 'isTour', isTour)

    // const start = startOfWeek(value);
    // const end = endOfWeek(value);

    // const dayIsBetween = isWithinInterval(date, { start, end });
    // const isFirstDay = isSameDay(date, start);
    // const isLastDay = isSameDay(date, end);

    return (
      <CustomPickersDay
        {...pickersDayProps}
        disableMargin
        isSelected={isSelected}
        istour={isTour}
        tour={tour}
      />
    );
  };
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Alert severity="info">This is an info alert — check it out!</Alert>
      </Grid>
      <Grid item xs={12}>
        <Paper elevation={3} sx={{m: 1}}>
          <Grid container sx={{display: 'flex', justifyContent: 'center'}}>
            <Grid item xs={6} className={classes.staticCalendar}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <CalendarTour
                  minDate={startDate}
                  maxDate={lastDate}
                  displayStaticWrapperAs="desktop"
                  label="Week picker"
                  value={previousMonth}
                  onChange={handleOnChange}
                  renderDay={renderWeekPickerDay}
                  renderInput={(params) => <TextField {...params} />}
                  inputFormat="'Week of' MMM d"
                  onMonthChange={handlePreviousMonthChange}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item xs={6} className={classes.staticCalendar}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <StaticDatePicker
                  minDate={moment(startDate).add(1,'months').date(1)._d}
                  maxDate={lastDate}
                  displayStaticWrapperAs="desktop"
                  label="Week picker"
                  value={currentMonth}
                  onChange={handleOnChange}
                  renderDay={renderWeekPickerDay}
                  renderInput={(params) => <TextField {...params} />}
                  inputFormat="'Week of' MMM d"
                  onMonthChange={handleMonthChange}
                />
              </LocalizationProvider>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  )
}

Calendar.propTypes = {
  // Types here
}

export default Calendar