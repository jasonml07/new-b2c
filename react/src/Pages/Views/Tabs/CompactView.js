import React from 'react';
import PropTypes from 'prop-types';
// Material
import { useTheme, styled } from '@mui/material/styles'
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
// Icons
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

const TourBoxWrapper = styled(Grid)(({theme, ...props}) => ({
  ['&.MuiGrid-root']: {
    position: 'relative',
    zIndex: props.selected ? 9999 : '',
    cursor: 'pointer',
    ['&:hover']: {
      zIndex: 9999
    }
  }
}))
const TourBox = styled(Paper)(({theme, ...props}) => {
  const boxShadow = 'rgb(0 0 0 / 20%) 0px 5px 5px -3px, rgb(0 0 0 / 14%) 0px 8px 10px 1px, rgb(0 0 0 / 12%) 0px 3px 14px 2px'
  return {
    ['&.MuiPaper-root']: {
      boxShadow: props.selected ?boxShadow :'',
      ['&:hover']: {
        boxShadow
      }
    }
  }
})

const DollarSign = styled('span')(({theme, ...props}) => ({
  fontSize: 20,
  position: 'relative',
  bottom: 20
}))

const CheckMark = styled('div')(({theme, ...props}) => ({
  ['& > svg']: {
    fontSize: '5em',
  },
  top: 0,
  position: 'absolute',
  width: '100%',
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: theme.palette.divider
}))

function CompactView({
  service
}) {
  const theme = useTheme()
  const handleOnSelect = (tour) => {
    service[1]({type: 'ROOM_SELECTION', data: tour})
  }
  return (
    <Grid container spacing={0} sx={{p: theme.spacing(1)}}>
      {
        service[0].context.tours.map((tour, index) => {
          const selected = service[0].context.currentTour?.id == tour.id
          return (
            <TourBoxWrapper item xs={3} onClick={() => handleOnSelect({...tour})} selected={selected}>
              <TourBox elevation={2} selected={selected}>
                <Typography variant='h2' align='center' color='primary'><DollarSign>$</DollarSign>{Number(parseFloat(tour.displayPrice).toFixed(0)).toLocaleString()}</Typography>
                <Typography variant='caption' align='center' color='primary' sx={{display: 'block'}}>Good for <b>{tour.duration}</b> days</Typography>
                <hr style={{borderColor: theme.palette.primary.main}}/>
                <Typography variant='subtitle' align='center' sx={{display: 'block'}}><b>TOUR START</b></Typography>
                <Typography variant='subtitle' align='center' sx={{display: 'block'}}>{tour.fromDayText}</Typography>
                <Typography variant='subtitle' align='center' sx={{display: 'block'}}><b>TOUR END</b></Typography>
                <Typography variant='subtitle' align='center' sx={{display: 'block'}}>{tour.toDayText}</Typography>
                <br />
              </TourBox>
              {
                selected &&
                <CheckMark><CheckCircleIcon color="success"/></CheckMark>
              }
            </TourBoxWrapper>
          )
        })
      }
    </Grid>
  )
}

CompactView.propTypes = {
  service: PropTypes.array
}

export default CompactView
