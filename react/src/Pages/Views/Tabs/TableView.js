import React from 'react';
import PropTypes from 'prop-types';
// Material
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';


function TableView({
  service
}) {
  const handleSelectTour = (row) => {
    service[1]({type: 'ROOM_SELECTION', data: row})
  }
  return (
    <Table sx={{ minWidth: 650 }} aria-label="simple table">
      <TableHead>
        <TableRow>
          <TableCell>TOUR START</TableCell>
          <TableCell>TOUR END</TableCell>
          <TableCell>DURATION</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {service[0].context.tours.map((row, index) => (
          <TableRow key={'table-view-'+index}>
            <TableCell component="th" scope="row">
              {row.fromDayText}
            </TableCell>
            <TableCell component="th" scope="row">{row.toDayText}</TableCell>
            <TableCell>{row.duration} days</TableCell>
            <TableCell>
              <Button variant="contained" color={service[0].context.currentTour?.id === row.id ?"success" :"primary"} fullWidth onClick={() => handleSelectTour({...row, index})}>SELECT</Button>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}

TableView.propTypes = {
  service: PropTypes.array
}

export default TableView