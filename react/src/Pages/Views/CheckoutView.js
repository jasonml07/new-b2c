import React, { useEffect, useState, Fragment } from 'react'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import moment from 'moment'
import faker from '@faker-js/faker'
// Material
import SwipeableViews from 'react-swipeable-views';
import { useTheme, styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Alert from '@mui/material/Alert';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import FormHelperText from '@mui/material/FormHelperText';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
// Icons
import DataObjectIcon from '@mui/icons-material/DataObject';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
// Validation
import { useFormik } from 'formik'
import * as yup from 'yup'
// Components
import Alerts from '../../components/Alerts'
import Dialogs from '../../components/Dialogs'
import ConfirmBooking from './ConfirmBooking'
import CheckoutViewItem from './CheckoutView.Item'
import CheckoutViewCreditCard from './CheckoutView.CreditCard'
import CheckoutViewBankDeposit from './CheckoutView.BankDeposit'
import CheckoutViewVoucher from './CheckoutView.Voucher'
import CheckoutViewGuests from './CheckoutView.Guests'



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const tabIndex= ['credit-card', 'bank-deposit']
const paymentMethodCodes = ['CC', 'BD', 'PP', 'EN', 'PL']
const defaultValues = {
  guests: [],
  voucher: {
    title: '',
    firstName: '',
    lastName: '',
    emailAddress: '',
    phoneNumber: ''
  },
  // credit-card
  // bank-deposit
  paymentMethod: 1,
  creditCard: {
    referenceNumber: '',
    cardHolderName: '',
    expiryYear: '',
    expiryMonth: '',
    cardVerificationNumber: '',
    paymentAmount: ''
  }
}


function CheckoutView({
  service
}) {
  const history = useHistory()
  const theme = useTheme()
  // const [activeTab, setActiveTab] = useState(0)
  const product = service[0].context.product
  const tour = service[0].context.currentTour || {}
  const form = service[0].context.form
  const bookingRef = service[0].context.bookingRef
  const bookingData = service[0].context.bookingData
  const beds = (service[0].context.form?.beds || [])
    .reduce((acc, curr) => {
      const index = acc.findIndex(item => item.index == curr.index)
      if(index > -1) {
        acc[index].quantity+=1;
      } else {
        acc.push({...curr, quantity: 1})             
      }
      return acc
    }, [])
  const packages = (form?.packages || [])
  const extrabills = (form?.extraBills || [])
  const discount = form.discount
  const totalPrice = form?.totalPrice || 0
  const discountedPrice = form?.discountedPrice || 0
  const formik = useFormik({
    initialValues: {...defaultValues},
    validationSchema: yup.object({
      guests: yup.array()
        .of(yup.object({
          title: yup.string()
            .required('The title field is required.')
            .oneOf(['Mr', 'Ms', 'Mrs'], 'The title field is not valid.'),
          firstName: yup.string()
            .required('The first name field is required.'),
          lastName: yup.string()
            .required('The last name field is required.')
        })),
      voucher: yup.object({
        title: yup.string()
          .required('The title field is required.')
          .oneOf(['Mr', 'Ms', 'Mrs'], 'The title field is not valid.'),
        firstName: yup.string()
          .required('The first name field is required.'),
        lastName: yup.string()
          .required('The last name field is required.'),
        emailAddress: yup.string()
          .email('The email address field is invalid')
          .required('The email address field is required.'),
        phoneNumber: yup.string().nullable()
      }),
      creditCard: yup.object({
        referenceNumber: yup.string()
          .when('paymentMethod', {
            is: () => formik.values.paymentMethod === 0,
            then: (schema) => schema.required('The reference number field is required.')
          }),
        cardHolderName: yup.string()
          .when('paymentMethod', {
            is: () => formik.values.paymentMethod === 0,
            then: (schema) => schema.required('The card holder name field is required.')
          }),
        expiryYear: yup.string()
          .when('paymentMethod', {
            is: () => formik.values.paymentMethod === 0,
            then: (schema) => schema.required('The year field is required.')
          }),
        expiryMonth: yup.string()
          .when('paymentMethod', {
            is: () => formik.values.paymentMethod === 0,
            then: (schema) => schema.required('The month field is required.')
          }),
        cardVerificationNumber: yup.string()
          .when('paymentMethod', {
            is: () => formik.values.paymentMethod === 0,
            then: (schema) => schema.required('The card verification number field is required.')
          }),
        paymentAmount: yup.string()
          .when('paymentMethod', {
            is: () => formik.values.paymentMethod === 0,
            then: (schema) => schema.required('The payment amount field is required.')
          }),
      })
    }),
    onSubmit: (values) => {
      formik.setSubmitting(false)
      const hasFlight = form.hasFlight == 'yes'
      // console.log('onSubmit')
      const {
        product_supplements,
        options,
        sub_supplement_avail_name_data,
        supplement_avail_name_data,
        analyzation,
        ...tourInfo
      } = tour
      const data = {
        ...formik.values,
        productId: product?.id,
        paymentMethod: paymentMethodCodes[values.paymentMethod],
        beds,
        tour: tourInfo,
        hasFlight,
        flight: hasFlight ?form.flight :null,
        packages,
        extrabills,
        totalPrice,
        discountedPrice,
        discountCode: discount?.code || null
      }
      service[1]({type: 'CONFIRM', data})
    }
  })
  const handleTabChange = (event, newValue) => {
    formik.setFieldValue('paymentMethod', newValue, false)
    // setActiveTab(newValue)
  }
  const handleChangeIndex = (index) => {
    // setActiveTab(index)
    formik.setFieldValue('paymentMethod', newValue, false)
  }
  const handleGuestTitleChange = (event, index) => {
    const value = event.target.value
    const voucher = formik.values.voucher
    const guests = formik.values.guests
    if(Boolean(guests[index])) {
      guests[index].title = value
      formik.setFieldValue('guests', guests, true)
      if(index == 0 && !Boolean(voucher.title)) {
        formik.setFieldValue('voucher.title', value, true)
      }
    }
  }
  const handleGuestFirstNameChange = (event, index) => {
    const value = event.target.value
    // const voucher = formik.values.voucher
    const guests = formik.values.guests
    if(Boolean(guests[index])) {
      guests[index].firstName = value
      formik.setFieldValue('guests', guests, true)
      // if(index == 0 && !Boolean(voucher.firstName)) {
      //   formik.setFieldValue('voucher.firstName', value, true)
      // }
    }
  }
  const handleGuestLastNameChange = (event, index) => {
    const value = event.target.value
    const guests = formik.values.guests
    if(Boolean(guests[index])) {
      guests[index].lastName = value
      formik.setFieldValue('guests', guests, true)
    }
  }
  const handleBlurOnVoucherFirstname = (event, index) => {
    const value = event.target.value
    const guests = formik.values.guests
    const voucher = formik.values.voucher
    if(!Boolean(voucher.firstName) && index == 0) {
      formik.setFieldValue('voucher.firstName', value, true)
    }
  }
  const handleGenerateData = () => {
    const form = {...formik.values}
    const guests = form.guests.map((guests, index) => {
      return {
        title: faker.random.arrayElement(['Mr', 'Ms', 'Mrs']),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      }
    })
    const voucher = {
      title: faker.random.arrayElement(['Mr', 'Ms', 'Mrs']),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      emailAddress: faker.internet.email(),
      phoneNumber: ''
    }

    formik.setFieldValue('guests', guests)
    formik.setFieldValue('voucher', voucher)
  }
  const handleBlurOnVoucherLastName = (event, index) => {
    const value = event.target.value
    const guests = formik.values.guests
    const voucher = formik.values.voucher
    if(!Boolean(voucher.lastName) && index == 0) {
      formik.setFieldValue('voucher.lastName', value, true)
    }
  }
  const guests = formik.values.guests
  const handleReviewPage = () => {
    history.push('/review/'+bookingRef)
  }
  useEffect(() => {
    if(service[0].matches('checkout') && service[0].history.matches('roomSelection')) {
      console.log('checkout state')
      for(let i=0; i < service[0].context.form?.pax || 0; i++) {
        guests.push({
          title: '',
          firstName: '',
          lastName: ''
        })      
      }
      formik.setFieldValue('guests', guests, false)
      formik.setFieldValue('creditCard.referenceNumber', 'N123456', false);
      formik.setFieldValue('creditCard.paymentAmount', service[0].context.form?.totalPrice, false)
    }
  }, [service[0]])
  return (
    <Grid container spacing={2}>
      {
        (() => {
          if(global.process?.env?.NODE_ENV == 'development') {
            return (
              <Tooltip title="Feed form">
                <IconButton
                  size="large"
                  onClick={handleGenerateData}
                  sx={{
                    position: 'fixed',
                    zIndex: 1,
                    right: 0,
                    backgroundColor: (theme) => theme.palette.primary.main,
                    color: (theme) => theme.palette.common.white,
                    '&:hover': {
                      backgroundColor: (theme) => theme.palette.primary.main,
                    }
                  }}
                >
                  <DataObjectIcon />
                </IconButton>
              </Tooltip>
            )
          }
        })()
      }
      <ConfirmBooking
        data={bookingData}
        open={service[0].matches('checkout.show.confirm')}
        onConfirm={() => service[1]({type: 'BOOK', data: bookingData})}
        onCancel={() => service[1]('CANCEL')}
      />
      {
        Object.keys(formik.touched).length > 0 && !formik.isValid && (
          <Alerts.Error
            open={true}
            message={"Please check form fields."}
          />
        )
      }
      <Dialogs.Loading
        open={service[0].matches('checkout.show.process')}
        message={
          <>
            <Typography align="center">Your request has been processed</Typography>
            <Typography align="center">Please wait. . .</Typography>
            <Typography align="center">You will be redirected one the request is done.</Typography>
          </>
        }
      />
      <Dialogs.Success
        open={service[0].matches('checkout.show.success')}
        title="Booking Success!"
        message="Your booking was successfully processed!"
        onClose={() => handleReviewPage()}
      />
      <Dialogs.Failure
        open={service[0].matches('checkout.show.failure')}
        title="Failed to book!"
        message="Please try again!"
        onClose={() => service[1]('CLOSE')}
      />
      <Grid item xs={8}>
        <Paper elevation={2} sx={{p: 1}}>
          <Typography variant="h4" component="div">
            Passengers
          </Typography>
          <hr />
          <Grid container spacing={0}>
            {/*{ paxField() }*/}
            <CheckoutViewGuests
              guests={guests}
              formik={formik}
              handleGuestTitleChange={handleGuestTitleChange}
              handleGuestFirstNameChange={handleGuestFirstNameChange}
              handleGuestLastNameChange={handleGuestLastNameChange}
              handleBlurOnVoucherFirstname={handleBlurOnVoucherFirstname}
              handleBlurOnVoucherLastName={handleBlurOnVoucherLastName}
            />
          </Grid>
          
          <Typography variant="h4" component="div" style={{marginTop: theme.spacing(3)}}>
            Voucher
          </Typography>
          <hr />
          <CheckoutViewVoucher
            formik={formik}
          />
          <Typography variant="h4" component="div" style={{marginTop: theme.spacing(3)}}>
            Payment Methods
          </Typography>
          <hr />
          <Grid container spacing={0}>
            <Grid item xs={12}>
              <Tabs
                value={formik.values.paymentMethod}
                onChange={handleTabChange}
                indicatorColor="secondary"
                textColor="inherit"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                {/*<Tab label={<span style={{display: 'flex', alignItems: 'center'}}><CreditCardIcon /> &nbsp; Credit Card</span>} {...a11yProps(0)} />*/}
                <Tab value={1} label=<span style={{display: 'flex', alignItems: 'center'}}><CreditCardIcon /> &nbsp; Bank Deposit</span> {...a11yProps(1)} />
              </Tabs>
              <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={formik.values.paymentMethod}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={formik.values.paymentMethod} index={0} dir={theme.direction}>
                  <CheckoutViewCreditCard
                    formik={formik}
                  />
                </TabPanel>
                <TabPanel value={formik.values.paymentMethod} index={1} dir={theme.direction}>
                  <CheckoutViewBankDeposit
                    formik={formik}
                  />
                </TabPanel>
              </SwipeableViews>
            </Grid>
          </Grid>

          <hr />
          <Grid container spacing={0}>
            <Grid item xs={6} sx={{display: 'block'}}>
              <Typography><Link style={{display: 'block', cursor: 'pointer'}} onClick={() => service[1]('DETAIL')}>Change Tour Date schedule</Link></Typography>
              <Typography><Link style={{cursor: 'pointer'}} onClick={() => service[1]('ROOM_SELECTION')}>Change Tour Classification</Link></Typography>
            </Grid>
            <Grid item xs={6} align="right">
              <Button variant="contained" size="large" disabled={formik.isSubmitting} onClick={formik.handleSubmit}>Book Now &nbsp; <ThumbUpIcon /></Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={4}>
        <Card>
          <CardContent>
            <Typography variant="h5" component="div" align="center">
              PRODUCT INFORMATION
            </Typography>
            
            <Grid container spacing={2} sx={{mt: 2}}>
              <Grid item xs={7}>
                <Typography variant="subtitle2" component="div">
                  TOUR START
                </Typography>
              </Grid>
              <Grid item xs={5}>
                <Typography variant="subtitle2" component="div">
                  {moment(service[0].context.currentTour?.fromDayText, 'DD MMM YYYY, dddd').format('MMM DD, YYYY')}
                </Typography>
              </Grid>
              <Grid item xs={7}>
                <Typography variant="subtitle2" component="div">
                  TOUR END
                </Typography>
              </Grid>
              <Grid item xs={5}>
                <Typography variant="subtitle2" component="div">
                  {moment(service[0].context.currentTour?.toDayText, 'DD MMM YYYY, dddd').format('MMMM DD, YYYY')}
                </Typography>
              </Grid>
              <Grid item xs={7}>
                <Typography variant="subtitle2" component="div">
                  DURATION
                </Typography>
              </Grid>
              <Grid item xs={5}>
                <Typography variant="subtitle2" component="div">
                  {service[0].context.currentTour?.duration} days
                </Typography>
              </Grid>
              <CheckoutViewItem
                discount={discount}
                title="ROOM/CABIN OCCUPANCY"
                items={beds}
              />
              {
                packages.length > 0 && (
                  <CheckoutViewItem
                    discount={discount}
                    title="PACKAGES"
                    items={packages}
                  />
                )
              }
              {
                extrabills.length > 0 && (
                  <CheckoutViewItem
                    discount={discount}
                    title="EXTRA BILLS"
                    items={extrabills}
                  />
                )
              }
              {
                service[0].context.form?.hasFlight == 'yes' && (
                  <>
                    <Grid item xs={7}>
                      <Typography variant="subtitle2" component="div">
                        <b>Flight return price ({(service[0].context.form?.flight?.passengers || 0)}X${Number(service[0].context.form?.flight?.price || 0).toLocaleString()})</b>
                      </Typography>
                    </Grid>
                    <Grid item xs={5}>
                      <Typography variant="subtitle2" component="div">
                        ${Number((service[0].context.form?.flight?.passengers || 0) * Number(service[0].context.form?.flight?.price || 0)).toLocaleString()}.00
                      </Typography>
                    </Grid>
                  </>
                )
              }
            </Grid>
          </CardContent>
          <CardActions sx={{display: 'block'}}>
            <hr />
            <Typography variant="h4" component="div" align="right" color="error">
              ${Number(totalPrice).toLocaleString()}.00
            </Typography>
            {
              (() => {
                if(discountedPrice) {
                  return (
                    <Typography variant="h5" align="right" style={{textDecoration: 'line-through'}} color="error">
                      ${Number(discountedPrice).toLocaleString()}.00
                    </Typography>
                  )
                }
              })()
            }
            <Typography variant="subtitle2" component="div" align="right" color="default">
              Total price
            </Typography>
          </CardActions>
        </Card>
      </Grid>
    </Grid>
  )
}

CheckoutView.propTypes = {
  service: PropTypes.array.isRequired
}

export default CheckoutView