import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// Material
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';


function RoomSelectionRoomCabins({
  rooms
}) {

  return (
    <Fragment>
      <Grid item xs={12}>
        <Typography variant="h5">Rooms/Cabins Occupancy</Typography>
        <hr />
      </Grid>
      {
        rooms.map((room, index) => (
          <Grid item container xs={12} key={'room-and-cabin-name-'+index}>
            <Grid item xs={7}>
              <Typography variant="h6">{room.supplement_avail_name}</Typography>
            </Grid>
            <Grid item xs={5}>
              <Typography variant="h6" align="right">${Number(parseFloat(room.grossPrice).toFixed(0)).toLocaleString()}.00</Typography>
            </Grid>
          </Grid>
        ))
      }
    </Fragment>
  )
}

RoomSelectionRoomCabins.propTypes = {
  rooms: PropTypes.array.isRequired
}


export default RoomSelectionRoomCabins