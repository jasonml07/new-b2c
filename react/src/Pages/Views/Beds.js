import React, {} from 'react';
import PropTypes from 'prop-types';
// Material
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import { useTheme, styled, makeStyles } from '@mui/material/styles';
// Icons
import ManIcon from '@mui/icons-material/Man';
// Components
import Alerts from '../../components/Alerts'

const BedWrapper = styled(Stack)(({theme}) => ({
  justifyContent: 'center',
}))

const Man = styled(ManIcon)(({theme}) => ({
  color: theme.palette.grey[600],
  fontSize: 100
}));

const Bed = styled(Paper)(({theme}) => ({
  ['&.MuiPaper-root']: {
    backgroundColor: theme.palette.grey[200]
  }
}))

const Wrap = styled(Paper)(({theme, ...props}) => {
  // console.log(props)
  return {
    minWidth: 222,
    width: 222,
    display: 'inline-block',
    margin: '10px',
    cursor: 'pointer',
    ['&.MuiPaper-root']: {
      padding: theme.spacing(1),
      border: props.selected ? '3px solid '+theme.palette.primary.light : (props.error ?'3px solid '+theme.palette.error.main :'3px solid transparent') 
    }
  }
})

function Single({
  error = false,
  title,
  onClick = () => {},
  selected = false
}) {
  
  return (
    <Wrap error={error} selected={selected} elevation={3} onClick={() => onClick('single')}>
      <Typography align="center" sx={{mb:1}}><b>{title}</b></Typography>
      <BedWrapper direction="row" spacing={2}>
        <Bed elevation={1}>
          <Man/>
        </Bed>
      </BedWrapper>
    </Wrap>
  )
}
Single.propTypes = {
  error: PropTypes.bool,
  title: PropTypes.string,
  onClick: PropTypes.func,
  selected: PropTypes.bool
}

function Twin({
  error = false,
  title,
  onClick = () => {},
  selected = false
}) {
  
  return (
    <Wrap error={error} selected={selected} elevation={3} onClick={() => onClick('twin')}>
      <Typography align="center" sx={{mb:1}}><b>{title}</b></Typography>
      <BedWrapper direction="row" spacing={1}>
        <Bed elevation={1}>
          <Man/>
        </Bed>
        <Bed elevation={1}>
          <Man/>
        </Bed>
      </BedWrapper>
    </Wrap>
  )
}
Twin.propTypes = {
  error: PropTypes.bool,
  title: PropTypes.string,
  onClick: PropTypes.func,
  selected: PropTypes.bool
}


function Double({
  error = false,
  title,
  onClick = () => {},
  selected = false
}) {
  return (
    <Wrap error={error} selected={selected} elevation={3} onClick={() => onClick('double')}>
      <Typography align="center" sx={{mb:1}}><b>{title}</b></Typography>
      <BedWrapper direction="row" spacing={2}>
        <Bed elevation={1}>
          <Man/>
          <Man/>
        </Bed>
      </BedWrapper>
    </Wrap>
  )
}
Double.propTypes = {
  error: PropTypes.bool,
  title: PropTypes.string,
  onClick: PropTypes.func,
  selected: PropTypes.bool
}

export default {
  Single,
  Double,
  Twin
}