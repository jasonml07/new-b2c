import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// Material
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
// Icons
import CheckIcon from '@mui/icons-material/Check';


function RoomSelectionExtraBills({
  extraBills
}) {
  if(extraBills.length == 0) {
    return ''
  }

  return (
    <Grid item xs={6} container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h5">Extra Bills</Typography>
        <Typography>The list of items will automatically be included on your payments.</Typography>
        <hr />
      </Grid>
      {
        extraBills.map((item, index) => (
          <Grid item xs={6} key={'extra-bill-'+index}>
            <Typography style={{display: 'flex'}}><CheckIcon color="success"/> <span>&nbsp;{item.name} (${item.price})</span></Typography>
          </Grid>
        ))
      }
    </Grid>
  )
}

RoomSelectionExtraBills.propTypes = {
  extraBills: PropTypes.array.isRequired
}


export default RoomSelectionExtraBills