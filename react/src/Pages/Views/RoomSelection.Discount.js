import React, { useState } from 'react'
import PropTypes from 'prop-types'
// MUI
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import Alert from '@mui/material/Alert';



function Discount ({
  error,
  submitting,
  open,
  onClose,
  onSubmit
}) {
  const [code, setCode] = useState('')
  return (
    <Dialog
      open={open}
      aria-labelledby="discount-dialog-title"
      aria-describedby="discount-dialog-description"
    >
      <DialogTitle id="discount-dialog-title">
        Discount
      </DialogTitle>
      <DialogContent>
        <Alert variant="outlined" severity="info">
          Note: Promo Code and associated discounts apply to either the land or the flight product only.
        </Alert>
        <br />
        <TextField
          label={"Enter code"}
          autoFocus
          variant="outlined"
          fullWidth
          value={code}
          onChange={(e) => setCode(e.target.value)}
          error={Boolean(error)}
          helperText={error}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Close</Button>
        {
          (() => {
            if(submitting) {
              return (
                <Button variant="contained">
                  Checking. . .
                </Button>
              )
            }
            return (
              <Button variant="contained" onClick={() => onSubmit(code)} autoFocus>
                Submit
              </Button>
            )
          })()
        }
      </DialogActions>
    </Dialog>
  )
}
Discount.propTypes = {
  error: PropTypes.string,
  submitting: PropTypes.bool,
  open: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default Discount