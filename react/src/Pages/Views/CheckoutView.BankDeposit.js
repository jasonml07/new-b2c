import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// Material
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Alert from '@mui/material/Alert';

function CheckoutViewBankDeposit({
  formik
}) {

  return (
    <Fragment>
      <Alert variant="outlined" severity="info">
        Payments can be made by bank transfer into the account below. Please fax our office (07 55262944) or e-mail us a copy of your payment. Pricing is subject to change until full payment has been made.
      </Alert>
      <Grid container spacing={2} sx={{mt:2}}>
        <Grid item xs={6}>
          <Typography align="right"><b>Name</b></Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>Westpac BSB 034-215</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography align="right"><b>ACCN</b></Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>462080</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography align="right"><b>SWIFT</b></Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>WPACAU2S</Typography>
        </Grid>
      </Grid>
    </Fragment>
  )
}
CheckoutViewBankDeposit.propTypes = {
  formik: PropTypes.object.isRequired,
}

export default CheckoutViewBankDeposit
