import React, {} from 'react';
import PropTypes from 'prop-types';
// Material
import { useTheme, styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

const GuestGrid = styled(Grid)(({theme, ...props}) => ({

  border: '2px dashed '+(props.error ?theme.palette.error.main :theme.palette.primary.main),
  padding: theme.spacing(1)
}))


function CheckoutViewGuests({
  guests,
  formik,
  handleGuestTitleChange,
  handleGuestFirstNameChange,
  handleGuestLastNameChange,
  handleBlurOnVoucherFirstname,
  handleBlurOnVoucherLastName
}) {

  return guests.map((guest, index) => {
    const isErrorTitle = (formik.touched?.guests || [{}])[index]?.title && Boolean((formik.errors?.guests || [{}])[index]?.title)
    const isErrorFirstName = (formik.touched?.guests || [{}])[index]?.firstName && Boolean((formik.errors?.guests || [{}])[index]?.firstName)
    const isErrorLastName = (formik.touched?.guests || [{}])[index]?.lastName && Boolean((formik.errors?.guests || [{}])[index]?.lastName)
    return (
    <GuestGrid item xs={6} key={'guest-form-'+index} error={isErrorTitle || isErrorFirstName || isErrorLastName}>
      <Typography variant="h6" component="div" align="center">
        Guest {index+1}
      </Typography>
      <hr />
      <Grid container spacing={1}>
        <Grid item xs={3}>
          <FormControl size="small" fullWidth error={isErrorTitle}>
            <InputLabel id="demo-simple-select-label">Title</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="title"
              value={guest.title}
              onChange={(event) => handleGuestTitleChange(event, index)}
            >
              <MenuItem value="">Title</MenuItem>
              <MenuItem value="Mr">Mr.</MenuItem>
              <MenuItem value="Ms">Ms.</MenuItem>
              <MenuItem value="Mrs">Mrs.</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={4.5}>
          <TextField
            size="small"
            fullWidth
            variant="outlined"
            label="First name"
            value={guest.firstName}
            onChange={(event) => handleGuestFirstNameChange(event, index)}
            onBlur={(event) => handleBlurOnVoucherFirstname(event, index)}
            error={isErrorFirstName}
          />
        </Grid>
        <Grid item xs={4.5}>
          <TextField
            size="small"
            fullWidth
            variant="outlined"
            label="Last name"
            value={guest.lastName}
            onChange={(event) => handleGuestLastNameChange(event, index)}
            onBlur={(event) => handleBlurOnVoucherLastName(event, index)}
            error={isErrorLastName}
          />
        </Grid>
      </Grid>
    </GuestGrid>
  )})
}

CheckoutViewGuests.propTypes = {
  guests: PropTypes.array.isRequired,
  formik: PropTypes.object.isRequired,
  handleGuestTitleChange: PropTypes.func,
  handleGuestFirstNameChange: PropTypes.func,
  handleGuestLastNameChange: PropTypes.func,
  handleBlurOnVoucherFirstname: PropTypes.func,
  handleBlurOnVoucherLastName: PropTypes.func,
}

export default CheckoutViewGuests