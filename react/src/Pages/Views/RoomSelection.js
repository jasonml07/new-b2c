import React, { Fragment, useContext, useState, useEffect } from 'react';
import Util from '../../util'
import PropTypes from 'prop-types';
import moment from 'moment'
import { makeStyles } from '@mui/styles';
import { useTheme, styled } from '@mui/material/styles';
// Material
import Grid from '@mui/material/Grid';
import Alert from '@mui/material/Alert';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import FormHelperText from '@mui/material/FormHelperText';
import Stack from '@mui/material/Stack';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import Collapse from '@mui/material/Collapse';
import TextField from '@mui/material/TextField';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
// import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
// import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
// import { DatePicker } from '@mui/x-date-pickers/DatePicker';
// Icons
import CheckIcon from '@mui/icons-material/Check';
import FlightIcon from '@mui/icons-material/Flight';
import ManIcon from '@mui/icons-material/Man';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import AttachMoneyRoundedIcon from '@mui/icons-material/AttachMoneyRounded';
// Validation
import { useFormik } from 'formik'
import * as yup from 'yup'
// Machine
// import { GlobalState } from '../global.state'
// import { useActor } from '@xstate/react'
import { createMachine, assign } from 'xstate'
import { useMachine } from '@xstate/react'
// Components
import Inputs from '../../components/Inputs'
import BootstrapDialog from '../../components/BootstrapDialog'
import Alerts from '../../components/Alerts'
import Beds from './Beds'
import RoomSelectionPackages from './RoomSelection.Packages'
import RoomSelectionRoomCabins from './RoomSelection.RoomCabins'
import RoomSelectionExtraBills from './RoomSelection.ExtraBills'
import RoomSelectionFlight from './RoomSelection.Flight'
import Discount from './RoomSelection.Discount'


const useStyles = makeStyles({
  // datepicker: {
  //   ['& > div']: {
  //     width: '100%'
  //   }
  // }
})
const BedValue = {
  single: 1,
  twin: 2,
  'double': 2,
}

const Machine = createMachine({
  id: 'Errors',
  initial: 'idle',
  on: {
    IDLE: 'idle',
    FLIGHT_NO_ROOM: 'flight.noRoomsSelection',
    MORE_THAN_PAX: 'pax.moreThan'
  },
  states: {
    idle: {},
    flight: {
      states: {
        noRoomsSelection: {}
      }
    },
    pax: {
      states: {
        moreThan: {}
      }
    }
  }
})

const MyLink = styled(Link)(({theme}) => ({
  ['&.MuiLink-root']: {
    color: theme.palette.info.main,
    cursor: 'pointer'
  }
}))

const defaultValue = {
  totalPax: 0,
  totalPrice: 0,
  discountedPrice: 0,
  pax: 1,
  rooms: '',
  beds: [],
  hasFlight: 'no',
  flight: {
    departureCity: 'Sydney',
    preferredClass: 'Economy',
    departureDate: moment(new Date()).format('DD/MM/YYYY'),
    returnDate: '',
    passengers: 0,
    price: 0,
    month: ''
  }
}

function formCosting (item) {
  return {
    basePrice: item.OriginalBasePrice,
    convertedPrice: item.convertedBasePrice,
    rate: item.convertedRate,
    fromCurrency: item.fromCurrency,
    toCurrency: item.toCurrency,
    commissionPercentAmnt: item.comm_pct_amount||0,
    markupPercentAmnt: item.markup_pct_amount||0,
    netAmnt: item.netAmount,
    grossPrice: item.grossPrice
  }
}

function RoomSelection ({
  service
}) {
  const classes = useStyles()
  const [returnDate, setReturnDate] = useState(new Date())
  const [returnDateMinDate, setReturnDateMinDate] = useState(new Date())
  const [showAirLines, setShowAirLines] = useState(false)
  const machine = useMachine(Machine)
  const discount = service[0].context.discount
  const supplements = (service[0].context?.currentTour?.product_supplements || []).map((item, index) => {
    // console.log('supplements')
    const costing = formCosting(item)
    const price = Math.ceil(item.grossPrice)
    const discountAmount = Util.discount(price, discount)
    return {
      index,
      name: item.supplement_name,
      quantity: item.quantity || 0,
      price,
      discount: discountAmount,
      totalPrice: Math.ceil(price - discountAmount),
      costing,
      minguest: item.minguest
    }
  })
  const failure = service[0].context.failure
  const packages = []
  // const packages = supplements.filter(item => item.minguest == 0)
  const extraBills = supplements.filter(item => item.minguest == 1)
  // console.log('machine', machine)
  const theme = useTheme()
  // const Services = useContext(GlobalState)
  const AppService = service
  // const [totalPax, setTotalPax] = useState(0)

  const validator = useFormik({
    initialValues: {...defaultValue},
    validationSchema: yup.object({
      totalPax: yup.number()
        .test(
          'is-enough-guest',
          'Please reselect any room to accomudate total guest!',
          function(value, context) {
            return value == context.parent.pax
          }),
      pax: yup.number().required('The pax field is required.'),
      hasFlight: yup.string().required('Please select flight option.'),
      rooms: yup.number().required('The room field is required.'),
      beds: yup.array()
        .test(
          'is-no-selection',
          'Please select room.',
          function(value, context) {
            return value.filter(item => Boolean(item)).length > 0
          })
    }),
    onSubmit: (values) => {
      validator.setSubmitting(false)
      const data = {
        ...values,
        hasFlight: 'false',
        packages: packages.filter(i => Boolean(i.quantity)),
        extraBills: extraBills.map((item) => ({...item, quantity: values.pax})),
        discount
      }
      AppService[1]({
        type: 'CHECKOUT',
        data
      })
    }
  })
  const handleShowDiscount = () => {
    AppService[1]('DISCOUNT_TOGGLE')
  }
  const handleCloseAirlines = () => {
    setShowAirLines(false)
  }
  const handleOnChangeTour = () => {
    AppService[1]('DETAIL')
  }
  const handleOnChangeNumGuest = (event) => {
    const value = event.target.value
    let flightPrice = (service[0].context.departureMonths[validator.values.flight.month] || 0);
    // let totalPrice = 0
    // // AppService[1]({type: 'SET_PAX', data: value})
    // if(validator.values.hasFlight == 'yes') {
    //   totalPrice+=flightPrice*value
    // }
    // validator.setFieldValue('totalPrice', totalPrice, false)
    validator.setFieldValue('pax', value, false)
    validator.setFieldValue('rooms', '', false)
    validator.setFieldValue('beds', [], false)
    validator.setFieldValue('flight.passengers', value, false)
    validator.setFieldValue('flight.price', flightPrice, false)
    // calcTotalPrice()
  }
  const handleOnRoomSelection = (event) => {
    const value = event.target.value
    const numberOfRooms = Number(value)
    const beds = []
    // AppService[1]({type: 'SET_ROOMS', data: value})
    validator.setFieldValue('rooms', numberOfRooms, false)
    for(let i=0; i < numberOfRooms; i++) {
      beds.push('')
    }
    
    validator.setFieldValue('beds', beds, false)
    // calcTotalPrice()
  }
  const handleBedOfRoses = (value, index) => {
    let totalGuest = 0
    // let totalPrice = 0
    // console.log('handleBedOfRoses', value)
    const beds = [...validator.values.beds]
    const discountAmount = Util.discount(value.price, discount)
    const totalPrice = Math.ceil(value.price - discountAmount)
    beds[index] = {...value,
      discount: discountAmount,
      totalPrice
    }
    for(let i=0; i < beds.length; i++) {
      if(Boolean(beds[i])) {
        // totalPrice += Number(Number(beds[i].price).toFixed(0))
        totalGuest += beds[i].bed
      }
    }
    // if(validator.values.hasFlight == 'yes') {
    //   totalPrice+=validator.values.flight.price*validator.values.flight.passengers
    // }
    validator.setFieldValue('beds', beds, false)
    validator.setFieldValue('totalPax', totalGuest, false)
    // validator.setFieldValue('totalPrice', totalPrice, false)
    // calcTotalPrice()
  }
  const handleFlightOption = (value) => {
    if(Boolean(validator.values.rooms)) {
      // let totalPrice = validator.values.beds.reduce((acc, curr) => {
      //   if(Boolean(curr)) {
      //     acc += Number(Number(curr.price).toFixed(0))
      //   }
      //   return acc
      // }, 0)
      // if(value == 'yes') {
      //   totalPrice+=Number(validator.values.flight.price * validator.values.flight.passengers)
      // }
      // validator.setFieldValue('totalPrice', totalPrice, false)
      validator.setFieldValue('hasFlight', value, false)
      // calcTotalPrice()
      if(value == 'yes') {
        window.open('https://secl-au-ifly.azurewebsites.net/', '_blank').focus();
      }
    } else {
      machine[1]('FLIGHT_NO_ROOM')
    }
  }
  const handlePackageChange = (event, packageIndex) => {
    service[1]({type: 'SET_PACKAGE_QTY', data: {value: event.target.value, index: packageIndex}})
    // calcTotalPrice()
  }
  const calcTotalPrice = () => {
    // console.log('calcTotalPrice')
    let totalPrice = 0
    let discountedPrice = 0
    // const hasDiscount = Boolean(discount)
    // console.log(discount)

    if(Boolean(validator.values.rooms)) {
      const beds = [...validator.values.beds]
      const hasFlight = validator.values.hasFlight == 'yes'
      const totalBedsPrice = beds
        .filter(curr => Boolean(curr))
        .reduce((acc, curr) => {
          acc.price = acc.price + curr.totalPrice
          acc.discount = acc.discount + curr.discount
          return acc
        }, {
          price: 0,
          discount: 0
        })
      let flightTotalPrice = 0
      // if(hasFlight) {
      //   const passengers = validator.values.flight.passengers
      //   let flightPrice = (service[0].context.departureMonths[validator.values.flight.month] || 0);
      //   flightTotalPrice = flightPrice*passengers
      // }
      let packagesTotalPrice = 0
      let extrabillsTotalPrice = 0
      if(beds.length > 0) {
        packagesTotalPrice = packages.reduce((acc, curr) => {
          // return acc + (Number(curr.totalPrice)*curr.quantity)
          acc.price = acc.price + (curr.totalPrice * Number(curr.quantity))
          acc.discount = acc.discount + (curr.discount * Number(curr.quantity))
          return acc
        }, {
          price: 0,
          discount: 0
        })
        // console.log('extraBills', extraBills)
        extrabillsTotalPrice = extraBills.reduce((acc, curr) => {
          // return acc + (Number(curr.totalPrice)*Number(validator.values.pax))
          acc.price = acc.price + (curr.totalPrice * Number(validator.values.pax))
          acc.discount = acc.discount + (curr.discount * Number(validator.values.pax))
          return acc
        }, {
          price: 0,
          discount: 0
        })
      }
      // console.log(totalBedsPrice, flightTotalPrice, packagesTotalPrice, extrabillsTotalPrice)
      totalPrice = totalBedsPrice.price + flightTotalPrice + packagesTotalPrice.price + extrabillsTotalPrice.price
      discountedPrice = totalPrice + totalBedsPrice.discount + extrabillsTotalPrice.discount
      if(discountedPrice == totalPrice) {
        discountedPrice = 0
      }
    }
    // if(hasDiscount) {
    //   // if(discount.type === 'PERCENTAGE') {
    //   //   discountedPrice = totalPrice
    //   //   totalPrice = totalPrice - (totalPrice*(parseFloat(discount.value)/100))
    //   // }
    //   // discountedPrice = totalPrice
    //   // totalPrice = Util.discount(totalPrice, discount)
    // }
    validator.setFieldValue('totalPrice', totalPrice, false)
    validator.setFieldValue('discountedPrice', discountedPrice, false)
    // return ''
  }
  useEffect(() => {
    // console.log(service[0])
    if(service[0].history.matches('detail')) {
      const departureDate = moment(service[0].context.currentTour.fromDayText, 'DD MMM YYYY, dddd').subtract(3, 'day').format('DD/MM/YYYY')
      const returnDate = moment(service[0].context.currentTour.toDayText, 'DD MMM YYYY, dddd').add(1, 'day')

      setReturnDateMinDate(returnDate._d)
      setReturnDate(returnDate._d)
      const month = returnDate.format('MMMM').toUpperCase()
      // const returnDate = moment(service[0].context.currentTour.toDayText, 'DD MMM YYYY, dddd').add(1, 'day').format('DD/MM/YYYY')
      // console.log('Came from tour detail must clear information')
      
      validator.resetForm({
        values: {...defaultValue,
          flight: {
            ...defaultValue.flight,
            month,
            departureDate,
            passengers: validator.values.pax,
            price: (service[0].context.departureMonths[month] || 0),
            returnDate: returnDate.format('DD/MM/YYYY')
          }
        }
      })
    }
    if(service[0].matches('roomSelection.settingPackageQuantity')) {
      calcTotalPrice()
    }
  }, [service[0]])
  useEffect(() => {
    calcTotalPrice()
  }, [
    validator.values.totalPax,
    validator.values.pax,
    validator.values.rooms,
    validator.values.beds,
    validator.values.hasFlight,
    validator.values.flight,
  ])
  useEffect(() => {
    // console.log('discount effect')
    const beds = [...validator.values.beds].map((o, i) => {
      const discountAmount = Util.discount(o.price, discount)
      return {
        ...o,
        discount: discountAmount,
        totalPrice: Math.ceil(o.price - discountAmount)
      }
    })
    validator.setFieldValue('beds', beds)
  }, [discount])
  const displayRoomSelection = () => {
    if(!Boolean(validator.values.rooms)) {
      return (
        <Grid item xs={12}>
          <Alert severity="error">Complete your selection for your guests and rooms.</Alert>
        </Grid>
      )
    }

    let listOfRooms = AppService[0].context.currentTour?.supplement_avail_name_data.reduce((acc, curr) => {
      const rooms = []
      const roomCosting = formCosting(curr)
      if(Boolean(curr.hasSupplement)) {
        rooms.push({
          name: 'Single Room',
          bed: 1,
          type: 'single',
          price: curr.supplement.grossPrice,
          costing: {
            // basePrice: curr
          }
        })
      }
      if(/twin/i.test(curr.supplement_avail_name)) {
        rooms.push({
          name: curr.supplement_avail_name.replace(/(double\/|\/double|double)/i, ''),
          bed: 2,
          type: 'twin',
          price: curr.grossPrice,
          costing: roomCosting
        })
      }
      if(/double/i.test(curr.supplement_avail_name)) {
        rooms.push({
          name: curr.supplement_avail_name.replace(/twin\/|\/twin|twin/i, ''),
          bed: 2,
          type: 'double',
          price: curr.grossPrice,
          costing: roomCosting
        })
      }
      if(/single/i.test(curr.supplement_avail_name) || !/double|twin/i.test(curr.supplement_avail_name)) {
        rooms.push({
          name: curr.supplement_avail_name,
          bed: 1,
          type: 'single',
          price: curr.grossPrice,
          costing: roomCosting
        })
      }

      return acc.concat(rooms)
    }, [])
    .reduce((acc, curr) => {
      if(Boolean(acc[curr.type])) {
        acc[curr.type].push(curr)
      }

      return acc
    }, {
      twin: [],
      'double': [],
      single: []
    })
    listOfRooms = listOfRooms.twin.concat(listOfRooms['double']).concat(listOfRooms.single)
    return validator.values.beds.map((n, i) =>(
      <Grid key={'room-'+i} item xs={12} wrap="nowrap">
        <Typography variant="h6" display="block">Room no. {i+1}</Typography>
        {
          Boolean(validator.errors.beds) && !Boolean(n) && <FormHelperText error>{validator.errors.beds}</FormHelperText>
        }
        <hr />
        {
          listOfRooms.map((room, index) => {
            const isError = Boolean(validator.errors.beds) && !Boolean(n)
            const selected = n?.index == index

            if(room.type=='single') {
              return <Beds.Single key={'single-bed-'+i+index} error={isError} selected={selected} title={room.name} onClick={(value) => handleBedOfRoses({...room, index}, i)} />
            }
            if(room.type == 'twin') {
              return <Beds.Twin key={'twin-bed-'+i+index} error={isError} selected={selected} title={room.name} onClick={(value) => handleBedOfRoses({...room, index}, i)}/>
            }
            if(room.type == 'double') {
              return <Beds.Double key={'double-bed-'+i+index} error={isError} selected={selected} title={room.name} onClick={(value) => handleBedOfRoses({...room, index}, i)}/>
            }
          })
        }
      </Grid>
    ))
  }
  return (
    <Grid container spacing={2}>
      {
        !validator.isValid &&
        <Alerts.Error
          open={true}
          onClose={() => {}}
          message="Please check your room selection."
        />
      }
      <Discount
        error={AppService[0].matches('roomSelection.discount.open.failure') ?'Invalid discount code.' :''}
        submitting={AppService[0].matches('roomSelection.discount.open.process')}
        open={AppService[0].matches('roomSelection.discount.open')}
        onSubmit={(value) => AppService[1]({type: 'DISCOUNT_SUBMIT', data: value})}
        onClose={() => AppService[1]('DISCOUNT_TOGGLE')}
      />

      
      <Alerts.Error open={machine[0].matches('flight.noRoomsSelection')} onClose={() => machine[1]('IDLE')} message="No rooms has been selected yet."/>
      {/*<Alerts.Error open={machine[0].matches('roomSelection.maximumPax')} onClose={() => machine[1]('IDLE')} message="Cannot select room, number of guest is more than to pax"/>*/}
      <Grid item xs={12}>
        <Typography variant='h5'>Your tour will start at <span style={{color: theme.palette.error.main}}>{AppService[0].context.currentTour?.fromDayText}</span> and will end at <span style={{color: theme.palette.error.main}}>{AppService[0].context.currentTour?.toDayText}</span></Typography>
        <Typography variant="caption" display='block'>This product has a {AppService[0].context.currentTour?.duration} days tour. <MyLink onClick={handleOnChangeTour}>Click here</MyLink> to change your tour date.</Typography>
        <hr />
      </Grid>
      <Grid item xs={12} sx={{mt: 2}}>
        <Alert severity="info">NOTE : Please review your selection upon booking.</Alert>
      </Grid>
      <Grid item xs={12}>
        <Paper elevation={3} sx={{p: 3}}>
          <Grid container spacing={2}>
            <RoomSelectionRoomCabins
              rooms={AppService[0].context.currentTour?.supplement_avail_name_data || []}
            />
            <RoomSelectionPackages
              maxPax={validator.values.pax}
              packages={packages}
              onChange={handlePackageChange}
            />
            <RoomSelectionExtraBills
              extraBills={extraBills}
            />
            <Grid item xs={12}>
              <br />
            </Grid>  
            <Grid item xs={12} md={6}>
              <FormControl fullWidth error={Boolean(validator.errors.pax)}>
                <InputLabel id="number-guest-select-label">Number of Guests</InputLabel>
                <Select
                  labelId="number-guest-select-label"
                  id="number-guest-select"
                  label="Number of Guests"
                  value={validator.values.pax}
                  onChange={handleOnChangeNumGuest}
                >
                  <MenuItem value=""><b>Select number of guest for your TOUR.</b></MenuItem>
                  {
                    [1,2,3,4,5,6,7,8,9,10].map((n, i) => (
                      <MenuItem key={'pax-option-'+n} value={n}>{n} Guest(s)</MenuItem>
                    ))
                  }
                </Select>
                {Boolean(validator.errors.pax) && <FormHelperText>{validator.errors.pax}</FormHelperText>}
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth error={Boolean(validator.errors.rooms)}>
                <InputLabel id="number-rooms-select-label">Number of Rooms</InputLabel>
                <Select
                  labelId="number-rooms-select-label"
                  id="number-rooms-select"
                  label="Number of Guests"
                  value={validator.values.rooms}
                  onChange={handleOnRoomSelection}
                >
                  <MenuItem value=""><b>Select number of rooms for your GUESTS.</b></MenuItem>
                  {
                    [1,2,3,4,5,6,7,8,9,10].map((n, i) => (
                      <MenuItem disabled={n > validator.values.pax} key={'room-option-'+n} value={n}>{n} Room(s)</MenuItem>
                    ))
                  }
                </Select>
                {Boolean(validator.errors.rooms) && <FormHelperText>{validator.errors.rooms}</FormHelperText>}
              </FormControl>
            </Grid>
            {
              Boolean(validator.errors.totalPax) &&
              <Grid item xs={12}>
                <Alert
                  variant="filled"
                  severity="error"
                >{validator.errors.totalPax}</Alert>
              </Grid>
            }
            { displayRoomSelection() }
            <Grid item xs={12}>
              <Stack direction="row" spacing={2}>
                <Typography color={Boolean(validator.errors.hasFlight) ?'error' :'default'} variant="h5">Add flights to your holiday. </Typography>
                <ButtonGroup>
                  <Button variant={validator.values.hasFlight == 'yes' ?'contained' :'outlined'} onClick={() => handleFlightOption('yes')}>Yes</Button>
                  <Button variant={validator.values.hasFlight == 'no' ?'contained' :'outlined'} onClick={() => handleFlightOption('no')}>No</Button>
                </ButtonGroup>
              </Stack>
              { Boolean(validator.errors.hasFlight) && <FormHelperText error>{validator.errors.hasFlight}</FormHelperText> }
              {/*<RoomSelectionFlight
                returnDate={returnDate}
                setReturnDate={setReturnDate}
                returnDateMinDate={returnDateMinDate}
                show={validator.values.hasFlight=='yes'}
                validator={validator}
              />*/}
            </Grid>
            <Grid item xs={12} sx={{display: 'flex', justifyContent: 'center', borderBottom: '1px solid #eee'}}>
              <div style={{textAlign: 'center'}}>
                <Typography variant="h2" color="error" align="center">
                  <AttachMoneyRoundedIcon style={{fontSize: 40}} />{Number(validator.values.totalPrice).toLocaleString()}.00
                </Typography>
                {
                  validator.values.discountedPrice > 0 && (
                    <Typography variant="h4" color="error" align="center" sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', textDecoration: 'line-through'}}>
                      <AttachMoneyRoundedIcon style={{fontSize: 40}} />{Number(validator.values.discountedPrice).toLocaleString()}.00
                    </Typography>
                  )
                }
                <Typography variant="caption" align="center">Over all total of your Tour</Typography>
              </div>
            </Grid>
            {
              (() => {
                if(Boolean(discount)) {
                  return (
                    <Grid item xs={12}>
                      <Typography align="center">
                        {discount?.message}
                      </Typography>
                    </Grid>
                  )
                }

                return (
                  <Grid item xs={12}>
                    <Typography align="center">Got a promo code? <MyLink onClick={handleShowDiscount}>Click here</MyLink> to get a DISCOUNTß</Typography>
                  </Grid>
                )
              })()
            }
            <Grid item xs={12} sx={{display: 'flex', justifyContent: 'center'}}>
              <div style={{textAlign: 'center'}}>
                <Button variant="contained" disabled={validator.isSubmitting} onClick={validator.handleSubmit}>CHECKOUT <ShoppingCartIcon/></Button>
                <Typography><MyLink display="block" sx={{mt: 2}} onClick={handleOnChangeTour}>Change tour date schedule</MyLink></Typography>
              </div>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  )
}
RoomSelection.propTypes = {
  // types here
  service: PropTypes.array.isRequired,
}

export default RoomSelection