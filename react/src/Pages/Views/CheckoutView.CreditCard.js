import React, { Fragment } from 'react';
import PropTypes from 'prop-types'
// Material
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import FormHelperText from '@mui/material/FormHelperText';
import Link from '@mui/material/Link';
// Images
import VisaImg from '../../../assets/img/credit-card/visa2.gif'
import MasterCardImg from '../../../assets/img/credit-card/mastercard.gif'
import AmericanExpressImg from '../../../assets/img/credit-card/amex.gif'
import JCBImg from '../../../assets/img/credit-card/jcb.gif'
import DinersClubImg from '../../../assets/img/credit-card/diners.gif'
import VeriSignTrustedImg from '../../../assets/img/credit-card/VeriSignTrusted.gif'

function CheckoutViewCreditCard({
  formik
}) {

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h5">Enter Payment Details</Typography>
      </Grid>
      <Grid container item xs={8} spacing={1}>
        <Grid item xs={6}>
          <Typography sx={{display: 'inline'}} color="error">*</Typography>
          <Typography sx={{display: 'inline'}}>Booking Reference Number:</Typography>
        </Grid>
        <Grid item xs={6}>
          <TextField
            size="small"
            fullWidth
            disabled={true}
            value={formik.values.creditCard?.referenceNumber}
            error={formik.touched.creditCard?.referenceNumber && Boolean(formik.errors.creditCard?.referenceNumber)}
            helperText={formik.touched.creditCard?.referenceNumber && Boolean(formik.errors.creditCard?.referenceNumber) && formik.errors.creditCard?.referenceNumber}
          />
        </Grid>
        <Grid item xs={6}>
          <Typography sx={{display: 'inline'}} color="error">*</Typography>
          <Typography sx={{display: 'inline'}}>Card Holder Name:</Typography>
        </Grid>
        <Grid item xs={6}>
          <TextField
            size="small"
            fullWidth
            value={formik.values.creditCard?.cardHolderName}
            error={formik.touched.creditCard?.cardHolderName && Boolean(formik.errors.creditCard?.cardHolderName)}
            helperText={formik.touched.creditCard?.cardHolderName && Boolean(formik.errors.creditCard?.cardHolderName) && formik.errors.creditCard?.cardHolderName}
          />
        </Grid>
        <Grid item xs={6}>
          <Typography sx={{display: 'inline'}} color="error">*</Typography>
          <Typography sx={{display: 'inline'}}>Expiry Date:</Typography>
        </Grid>
        <Grid item xs={2}>
          <TextField
            label="Month"
            size="small"
            fullWidth
            value={formik.values.creditCard?.expiryMonth}
            error={formik.touched.creditCard?.expiryMonth && Boolean(formik.errors.creditCard?.expiryMonth)}
          />
        </Grid>
        <Grid item xs={2}>
          <TextField
            label="Year"
            size="small"
            fullWidth
            value={formik.values.creditCard?.expiryYear}
            error={formik.touched.creditCard?.expiryYear && Boolean(formik.errors.creditCard?.expiryYear)}
          />
        </Grid>
        <Grid item xs={2}>
          <Typography>e.g. 02/20</Typography>
        </Grid>
        <Grid item xs={6}>
        </Grid>
        <Grid item xs={6}>
          {
            formik.touched.creditCard?.expiryMonth && Boolean(formik.errors.creditCard?.expiryMonth) &&
            <FormHelperText error>{formik.errors.creditCard?.expiryMonth}</FormHelperText>
          }
          {
            formik.touched.creditCard?.expiryYear && Boolean(formik.errors.creditCard?.expiryYear) &&
            <FormHelperText error>{formik.errors.creditCard?.expiryYear}</FormHelperText>
          }
        </Grid>
        <Grid item xs={6}>
          <Typography sx={{display: 'inline'}} color="error">*</Typography>
          <Typography sx={{display: 'inline'}}>Card verification number:</Typography>
        </Grid>
        <Grid item xs={2}>
          <TextField
            size="small"
            fullWidth
            value={formik.values.creditCard?.cardVerificationNumber}
            error={formik.touched.creditCard?.cardVerificationNumber && Boolean(formik.errors.creditCard?.cardVerificationNumber)}
          />
        </Grid>
        <Grid item xs={4}>
          <Link href="#">More Information</Link>
        </Grid>
        <Grid item xs={6}>
        </Grid>
        <Grid item xs={6}>
          {
            formik.touched.creditCard?.cardVerificationNumber && Boolean(formik.errors.creditCard?.cardVerificationNumber) &&
            <FormHelperText error>{formik.errors.creditCard?.cardVerificationNumber}</FormHelperText>
          }
        </Grid>
        <Grid item xs={6}>
          <Typography sx={{display: 'inline'}} color="error">*</Typography>
          <Typography sx={{display: 'inline'}}>Payment Amount (AUD):</Typography>
        </Grid>
        <Grid item xs={6}>
          <TextField
            size="small"
            fullWidth
            value={Number(formik.values.creditCard?.paymentAmount || 0).toFixed(2)}
            disabled={true}
            error={formik.touched.creditCard?.paymentAmount && Boolean(formik.errors.creditCard?.paymentAmount)}
            helperText={formik.touched.creditCard?.paymentAmount && Boolean(formik.errors.creditCard?.paymentAmount) && formik.errors.creditCard?.paymentAmount}
          />
        </Grid>
      </Grid>
      <Grid item xs={4} container spacing={0}>
        <Grid item xs={12}>
          <Typography>
            <img src={VisaImg} alt="visa" />&nbsp;
            <img src={MasterCardImg} alt="master card" />&nbsp;
            <img src={AmericanExpressImg} alt="american express" />&nbsp;
            <img src={JCBImg} alt="JCB" />&nbsp;
            <img src={DinersClubImg} alt="diners club" />&nbsp;
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography>Surcharges apply</Typography>
            </Grid>
            <Grid item xs={10}>
              <Typography sx={{ml: 2}}>Visa / Mastercard</Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography>1.5%</Typography>
            </Grid>
            <Grid item xs={10}>
              <Typography sx={{ml: 2}}>American Express / JCB</Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography>1.5%</Typography>
            </Grid>
            <Grid item xs={10}>
              <Typography sx={{ml: 2}}>Diners Club</Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography>3.5%</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h5">Merchant Details</Typography>
      </Grid>
      <Grid container item xs={8} spacing={1}>
        <Grid item xs={6}>
          <Typography>Merchant Name</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>EASTERN EUROTOURS</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>ABN</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>74071920360</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>Address</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>1/66 Appel Street Surfers Paradise QLD 4217</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>Email Address</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>0755262855</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>Fax</Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>0755262944</Typography>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Typography align="center">
          <img src={VeriSignTrustedImg} alt="verisign trusted" />
        </Typography>
      </Grid>
    </Grid>
  )
}

CheckoutViewCreditCard.propTypes = {
  formik: PropTypes.object.isRequired
}

export default CheckoutViewCreditCard