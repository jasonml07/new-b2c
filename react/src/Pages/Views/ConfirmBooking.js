import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
// Material
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
// Icons
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import AccessTimeIcon from '@mui/icons-material/AccessTime';


function Items({
  items
}) {

  return items.map((item, index) => (
    <Fragment key={'item-'+index}>
      <Grid item xs={6}>
        <Typography>{item.name} X{item.quantity}</Typography>
      </Grid>
      <Grid item xs={6}>
        <Typography>${Number(item.totalPrice * item.quantity).toLocaleString()}</Typography>
      </Grid>
    </Fragment>
  ))
}
Items.propTypes = {
  items: PropTypes.array.isRequired
}

function ConfirmBooking({
  data,
  open,
  onConfirm,
  onCancel
}) {
  const flight = data?.flight
  return (
    <Dialog
      open={open}
      onClose={onCancel}
      aria-labelledby="success-dialog-title"
      aria-describedby="success-dialog-description"
    >
      <DialogTitle id="success-dialog-title">
        <Typography variant="h4">Review of your Tour</Typography>
      </DialogTitle>
      <DialogContent>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h5">Tour Details</Typography>
            <hr />
          </Grid>
          <Grid item xs={6}>
            <Typography><CalendarMonthIcon />&nbsp;Start Date</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography>{moment(data?.tour?.fromDayText, 'DD MMM YYYY').format('MMMM DD, YYYY')}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography><CalendarMonthIcon />&nbsp;End Date</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography>{moment(data?.tour?.toDayText, 'DD MMM YYYY').format('MMMM DD, YYYY')}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography><AccessTimeIcon />&nbsp;Duration</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography>{data?.tour?.duration} days</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h5">Room/Cabin Occupancy</Typography>
            <hr />
          </Grid>
          {
            (data?.beds || []).map((item, index) => (
              <Fragment key={'room-'+index}>
                <Grid item xs={6}>
                  <Typography>{item.name} X{item.quantity}</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography>${Number((item.totalPrice || item.price) * item.quantity).toLocaleString()}</Typography>
                </Grid>
              </Fragment>
            ))
          }
          {
            (() => {
              if((data?.packages||[]).length > 0) {
                return (
                  <Fragment>
                    <Grid item xs={12}>
                      <Typography variant="h5">Add Ons/Packages</Typography>
                      <hr />
                    </Grid>
                    <Items items={data.packages}/>
                  </Fragment>
                )
              }
            })()
          }
          {
            (() => {
              if((data?.extrabills||[]).length > 0) {
                return (
                  <Fragment>
                    <Grid item xs={12}>
                      <Typography variant="h5">Extra Bills</Typography>
                      <hr />
                    </Grid>
                    <Items items={data.extrabills}/>
                  </Fragment>
                )
              }
            })()
          }
          {
            Boolean(flight) && (
              <>
                <Grid item xs={6}>
                  <Typography>Flight return price ({flight?.passengers}X${Number(flight?.price).toLocaleString()})</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography>${Number(flight?.price * flight?.passengers).toLocaleString()}</Typography>
                </Grid>
              </>
            )
          }
          <Grid item xs={12}>
            <hr />
            <Typography variant="h3" align="center" color="error">${Number(data?.totalPrice || 0).toLocaleString()}</Typography>
            {
              (() => {
                if(data?.discountedPrice > 0) {
                  return (
                    <Typography variant="h4" align="center" color="error" style={{ textDecoration: 'line-through'}}>${Number(data?.discountedPrice || 0).toLocaleString()}</Typography>
                  )
                }
              })()
            }
            <Typography align="center">Tour Details</Typography>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color="error">Cancel</Button>
        <Button onClick={onConfirm} variant="contained">Confirm</Button>
      </DialogActions>
    </Dialog>
  )
}
ConfirmBooking.propTypes = {
  data: PropTypes.object.isRequired,
  open: PropTypes.bool,
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func
}

export default ConfirmBooking;