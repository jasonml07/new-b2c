import React, { Fragment } from 'react';
import PropTypes from 'prop-types'
import Util from '../../util'
// Material
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';


function CheckoutViewItem({
  discount,
  title,
  items
}) {

  return (
    <Fragment>
      <Grid item xs={12}>
        <Typography variant="h6" component="div" align="center">
          {title}
        </Typography>
        <hr/>
      </Grid>
      {
        items.map((pack, index) => (
          <Fragment>
            <Grid item xs={7}>
              <Typography variant="subtitle2" component="div">
                <b>{pack.name} X{pack.quantity}</b>
              </Typography>
            </Grid>
            <Grid item xs={5}>
              <Typography variant="subtitle2" component="div">
                ${Number((pack.totalPrice || pack.price)*pack.quantity).toLocaleString()}.00
              </Typography>
            </Grid>
          </Fragment>
        ))
      }
    </Fragment>
  )
}

CheckoutViewItem.propTypes = {
  discount: PropTypes.object,
  title: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired
}

export default CheckoutViewItem