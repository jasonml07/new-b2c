import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTheme, styled } from '@mui/material/styles';
// Material
import Paper from '@mui/material/Paper';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
// Components
import Calendar from './Tabs/Calendar'
import TableView from './Tabs/TableView'
import CompactView from './Tabs/CompactView'
// Icons
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import WindowIcon from '@mui/icons-material/Window';


const MyPaper = styled(Paper)(({theme}) => ({
  ['&.MuiPaper-root']: {
    margin: '10px 100px'
  }
}))
function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ mt: 2 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};


function TourSelection ({
  service
}) {
  const theme = useTheme();
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <Paper elevation={0}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="secondary"
        textColor="inherit"
        aria-label="full width tabs example"
        variant="fullWidth"
      >
        <Tab label={<span style={{display: 'flex', alignItems: 'center'}}><CalendarMonthIcon /> Calendar</span>} {...a11yProps(0)} />
        <Tab label={<span style={{display: 'flex', alignItems: 'center'}}><FormatListBulletedIcon /> List</span>} {...a11yProps(1)} />
        <Tab label={<span style={{display: 'flex', alignItems: 'center'}}><WindowIcon /> Compact</span>} {...a11yProps(2)} />
      </Tabs>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <Calendar />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <TableView service={service}/>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <CompactView service={service}/>
        </TabPanel>
      </SwipeableViews>
    </Paper>
  )  
}
TourSelection.propTypes = {
  // types here
  service: PropTypes.array
}

export default TourSelection