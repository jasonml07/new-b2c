import React, { useState, Fragment } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

import { useTheme, styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Collapse from '@mui/material/Collapse';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

import FlightIcon from '@mui/icons-material/Flight';

import BootstrapDialog from '../../components/BootstrapDialog'


const MyLink = styled(Link)(({theme}) => ({
  ['&.MuiLink-root']: {
    color: theme.palette.info.main,
    cursor: 'pointer'
  }
}))

const useStyles = makeStyles({
  datepicker: {
    ['& > div']: {
      width: '100%'
    }
  }
})

const RoomSelectionFlight = ({
  returnDate,
  returnDateMinDate,
  show,
  validator
}) => {
  const classes = useStyles()
  const [showAirlines, setShowAirLines] = useState(false)
  const handleCloseAirlines = () => {
    setShowAirLines(false)
  }
  return (
    <Fragment>
      <BootstrapDialog.BootstrapDialog
        open={showAirlines}
        onClose={handleCloseAirlines}
      >
        <BootstrapDialog.BootstrapDialogTitle id="customized-dialog-title" onClose={handleCloseAirlines}>
          <b>Airline used - subject to availability</b>
        </BootstrapDialog.BootstrapDialogTitle>
        <DialogContent dividers>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <ul>
                {
                  ['Etihad Airways', 'China Southern', 'Emirates, Qantas Airways', 'Qatar Airways', 'Singapore Airlines', 'British Airways', 'Cathay Pacific', 'Lufthansa'].map(a => (
                    <li key={a}>
                      <Typography>{a}</Typography>
                    </li>
                  ))
                }
              </ul>
            </Grid>
            <Grid item xs={6}>
              <ul>
                {
                  ['Swiss Airways', 'Austrian Airlines', 'Air France', 'KLM', 'Finnair', 'Virgin Australia', 'Korean Airlines', 'Malaysian Airlines'].map(a => (
                    <li key={a}>
                      <Typography>{a}</Typography>
                    </li>
                  ))
                }
              </ul>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleCloseAirlines}>
            Close
          </Button>
        </DialogActions>
      </BootstrapDialog.BootstrapDialog>
      <Collapse in={show}>
        <Grid container spacing={2} sx={{paddingTop: 2}}>
          <Grid item xs={6}>
            <Typography><FlightIcon style={{transform: 'rotate(45deg)'}} /> Flight Details</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography align="right"><MyLink onClick={() => setShowAirLines(true)}>Airline used - subject to availability</MyLink></Typography>
          </Grid>
          <Grid item xs={12}>
            <hr />
          </Grid>
          <Grid item xs={6}>
            {/*<TextField fullWidth label="Departure City"/>*/}
            <FormControl fullWidth>
              <InputLabel id="departure-city-select-label">Departure City</InputLabel>
              <Select
                labelId="departure-city-select-label"
                id="departure-city-select"
                label="Departure City"
                value={validator.values.flight.departureCity}
              >
                <MenuItem value={"Sydney"}>Sydney</MenuItem>
                <MenuItem value={"Melbourne"}>Melbourne</MenuItem>
                <MenuItem value={"Brisbane"}>Brisbane</MenuItem>
                <MenuItem value={"Adelaide"}>Adelaide</MenuItem>
                <MenuItem value={"Perth"}>Perth</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <TextField
              fullWidth
              label="Preferred Class"
              disabled={true}
              value={validator.values.flight.preferredClass}/>
          </Grid>
          <Grid item xs={6}>
            <TextField
              fullWidth
              label="Departure Date"
              disabled={true}
              value={validator.values.flight.departureDate}/>
          </Grid>
          <Grid item xs={6} className={classes.datepicker}>
            {/*<TextField fullWidth label="Return Date"/>*/}
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                fullWidth
                minDate={returnDateMinDate}
                label="Return Date"
                value={returnDate}
                onChange={(newValue) => {
                  setReturnDate(newValue)
                  validator.setFieldValue('flight.returnDate', moment(newValue).format('DD/MM/YYYY'))
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </Grid>
          <Grid item xs={6}>
            <TextField
              fullWidth
              label="Passengers"
              disabled={true}
              value={validator.values.flight.passengers}/>
          </Grid>
          <Grid item xs={6}>
            <TextField
              fullWidth
              label="Price"
              disabled={true}
              value={(validator.values.flight.price * validator.values.flight.passengers)}/>
          </Grid>
          <Grid item xs={12}>
            <FormGroup>
              <FormControlLabel control={<Checkbox />} label={<Typography>I  have read the <MyLink href="https://www.europeholidays.com.au/air-terms-and-conditions" target="_blank">terms and conditions</MyLink> of the flight.</Typography>} />
            </FormGroup>
          </Grid>
        </Grid>
      </Collapse>
    </Fragment>
  )
}
RoomSelectionFlight.propTypes = {
  returnDateMinDate: PropTypes.object,
  validator: PropTypes.object.isRequired,
  show: PropTypes.bool
}
RoomSelectionFlight.defaultProps = {
  show: false
}

export default RoomSelectionFlight