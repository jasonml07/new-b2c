import React, { Fragment, useEffect } from 'react';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Fade from '@mui/material/Fade';
import { styled } from '@mui/material/styles';
// Icons
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import Logo from '../../../assets/img/ifly/logo.png'
// Machine
import { createMachine } from 'xstate'
import { useMachine } from '@xstate/react'
// Menu
import MenuList from './Header.menu'

const Link = styled('div')(({theme, ...props}) => {
  
  const borders = {
    backgroundColor: theme.palette.primary.main,
    content: "''",
    position: 'absolute',
    transition: '.3s'
  }
  return {
    position: 'relative',
    margin: 2,
    '&:before': {
      width: 2,
      left: 0,
      top: '100%',
      bottom: 0,
      ...borders
    },
    '&:after': {
      width: 2,
      bottom: '100%',
      right: 0,
      top: 0,
      ...borders
    },
    '&:hover': {
      '&:before': {
        top: 0
      },
      '&:after': {
        bottom: 0
      }
    },
    '& > p': {
      cursor: 'pointer',
      letterSpacing: '4px',
      fontSize: 14,
      padding: 15,
      color: theme.palette.primary.main,
      '&:before': {
        height: 2,
        right: '100%',
        top: 0,
        left: 0,
        ...borders,
        width: ''
      },
      '&:after': {
        height: 2,
        bottom: 0,
        left: '100%',
        right: 0,
        ...borders,
        width: ''
      },
      '&:hover': {
        color: theme.palette.info.main,
        '&:before': {
          right: 0
        },
        '&:after': {
          left: 0
        }
      }
    },
  }
})

const DestinationHeaderLink = styled(Typography)(({theme, ...props}) => {
  const fontColor = '#1b1b1d'
  return {
    fontFamily: 'Roboto Condensed',
    cursor: props.hasLink ?'pointer' :'',
    fontSize: '14px',
    color: fontColor,
    lineHeight: '42px',
    letterSpacing: '3px',
    fontWeight: 700,
    '&:hover > a': {
      color: theme.palette.info.main
    },
    '& > a': {
      color: fontColor,
      textDecoration: 'none'
    }
  }
})
const DestinationLink = styled(Typography)(({theme, ...props}) => {
  const fontColor = '#1b1b1d'
  return {
    cursor: 'pointer',
    fontSize: 18,
    color: fontColor,
    lineHeight: '36px',
    fontWeight: 300,
    '&:hover > a': {
      color: theme.palette.primary.main
    },
    '& > a': {
      textDecoration: 'none',
      color: fontColor
    }
  }
})

const TripStyleLink = styled(Typography)(({theme, ...props}) => {
  const fontColor = '#1b1b1d'
  return {
    cursor: 'pointer',
    fontSize: 18,
    color: fontColor,
    margin: '10px 0',
    '&:hover': {
      color: theme.palette.primary.main
    },
    '& > a': {
      color: fontColor,
      textDecoration: 'none'
    }
  }
})

const BASE_URL = 'https://iflygo.com.au'

const MegaWapper = styled(Fade)(({theme, ...props}) => {
  return {
    zIndex: 1,
    width: 'calc(100% - 50px)',
    position: 'absolute',
    top: 82,
    left: 0,
    backgroundColor: '#f6f8f8',
    padding: '25px'
  }
})

const megaMenuMachine = createMachine({
  id: 'MegaMenu',
  initial: 'idle',
  on: {
    INACTIVE: 'inactive',
    DESTINATIONS: 'destinations',
    TRIP_STYLE: 'tripStyle'
  },
  context: {

  },
  states: {
    idle: {},
    destinations: {},
    tripStyle: {},
    inactive: {}
  }
})


export default function Header({

}) {
  const MegaState = useMachine(megaMenuMachine)
  const handleClick = (path, isUrl) => {
    window.location.href = isUrl ?path :'https://iflygo.com.au/'+path
  }
  const handleNavigate = (item) => {
    if(Boolean(item.link)) {
      MenuList.navigate(item.link);
    }
  }

  const handleHover = (type) => {
    // destinations
    // trip_style
    MegaState[1](type)
  }


  useEffect(() => {
    const handleScroll = (event) => {
      if(window.scrollY > 400) {
        handleHover('INACTIVE')
      }
    }
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    }
  }, [])
  return (
    <Fragment>
      <Paper key="header" sx={{zIndex: 9999, position: 'fixed', width: '100%', boxShadow: '0 0 25px rgb(0 0 0 / 25%)'}}>
        <Grid container>
          <Grid item xs={2}>
            <Typography align="center">
              <img src={Logo} alt="ifly logo" style={{height: 85}}/>
            </Typography>
          </Grid>
          <Grid container item xs={7} sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <Link onClick={() => handleClick('')} onMouseEnter={() => handleHover('INACTIVE')}>
              <Typography>HOME</Typography>
            </Link>
            <Link onClick={() => handleClick('destinations')} onMouseEnter={() => handleHover('DESTINATIONS')}>
              <Typography align="center" sx={{display: 'flex', justifyContent: 'center'}}>DESTINATIONS <ArrowDropDownIcon/></Typography>
            </Link>
            <Link onClick={() => handleClick('pricing')} onMouseEnter={() => handleHover('TRIP_STYLE')}>
              <Typography align="center" sx={{display: 'flex', justifyContent: 'center'}}>TRIP STYLE <ArrowDropDownIcon/></Typography>
            </Link>
            <Link onClick={() => handleClick('https://secl-au-ifly.azurewebsites.net/', true)} onMouseEnter={() => handleHover('INACTIVE')}>
              <Typography>FLIGHTS</Typography>
            </Link>
            <Link onClick={() => handleClick('contact-iflygo')} onMouseEnter={() => handleHover('INACTIVE')}>
              <Typography>CONTACT US</Typography>
            </Link>
          </Grid>
          <Grid item xs={3} sx={{display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
            <Typography sx={{fontFamily: 'Arial', letterSpacing: 5}} color="primary" align="center" variant="h6">1800 242 373</Typography>
            <Typography sx={{fontFamily: 'Arial'}} align="center" variant="caption">24/7 Support, Based in Australia</Typography>
          </Grid>
        </Grid>
      </Paper>
      <MegaWapper in={MegaState[0].matches('destinations')} sx={{width: 'calc(100% - 100px)', padding: '25px 50px'}}>
        <Box sx={{width: '100%'}}>
          <Grid container spacing={5}>
            {
              MenuList.DESTINATIONS.map((group, index) => {
                return (
                  <Grid item key={'group-'+index} sm={MenuList.DESTINATION_GROUP_SIZE[index] ?? ''}>
                    {
                      group.map((item, i) => (
                        <Fragment>
                          <DestinationHeaderLink>
                            {
                              (() => {
                                if(Boolean(item.link)) {
                                  return (
                                    <a href={item.link}>{item.text}</a>
                                  )
                                }

                                return item.text
                              })()
                            }
                          </DestinationHeaderLink>
                          {
                            item.links.map((sub, i) => (
                              <DestinationLink key={'sub-link-'+index+''+i}>
                                <a href={sub.link}>{sub.text}</a>
                              </DestinationLink>
                            ))
                          }
                        </Fragment>
                      ))
                    }
                  </Grid>
                )
              })
            }
          </Grid>
        </Box>
      </MegaWapper>
      <MegaWapper in={MegaState[0].matches('tripStyle')}>
        <Box sx={{width: '100%'}}>
          <Grid container spacing={2}>
            <Grid item sm={6}></Grid>
            <Grid item sm={2}>
              <TripStyleLink>
                <a href="https://iflygo.com.au/adventure-travel">Go Adventure</a>
              </TripStyleLink>
              <TripStyleLink>
                <a href="https://iflygo.com.au/backpacking-travel">Go Backpacking</a>
              </TripStyleLink>
              <TripStyleLink>
                <a href="https://iflygo.com.au/independent-travel">Go Independent</a>
              </TripStyleLink>
              <TripStyleLink>
                <a href="https://iflygo.com.au/group-travel">Go Group</a>
              </TripStyleLink>
              <TripStyleLink>
                <a href="https://iflygo.com.au/cruise-travel">Go Cruise</a>
              </TripStyleLink>
              <TripStyleLink>
                <a href="https://iflygo.com.au/island-hopping-travel">Go Island Hopping</a>
              </TripStyleLink>
              <TripStyleLink>
                <a href="https://iflygo.com.au/festival-travel">Go Festivals</a>
              </TripStyleLink>
            </Grid>
            <Grid item sm={4}>
              <iframe style={{width: '100%', height: '100%'}} src="https://www.youtube.com/embed/ISdydcliclQ?start=0&showinfo=1&controls=1&autoplay=0" frameBorder="0"></iframe>
            </Grid>
          </Grid>
        </Box>
      </MegaWapper>
      <Box style={{height: 100}}></Box>
    </Fragment>
  )
}