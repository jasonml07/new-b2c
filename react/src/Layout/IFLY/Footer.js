import React, {} from 'react';
import { useTheme, styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Link from '@mui/material/Link';
// Icons
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import AccessTimeIcon from '@mui/icons-material/AccessTime';

const SocialButton = styled(Button)(({theme, ...props}) => {
  return {
    color: theme.palette.common.white,
    border: '1px solid '+theme.palette.grey['A700'],
    '&:hover': {
      color: theme.palette.primary.main
    }
  }
})

const FooterLink = styled(Link)(({theme, ...props}) => ({
  cursor: 'pointer',
  color: theme.palette.grey['A700'],
  textDecoration: 'none',
  '&:hover': {
    color: theme.palette.primary.main
  }
}))

const useStyles = makeStyles({
  link: {
    padding: '10px 0'
  }
})

export default function Footer({

}) {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <Box sx={{backgroundColor: '#000', color: theme.palette.common.white, p: 8}}>
      <Grid container spacing={6}>
        <Grid item xs={12} md={4}>
          <Typography variant="h6" sx={{lineHeight: 2, color: theme.palette.grey['A700']}}>Need to talk to one our our travel experts? We're here to help you anyway we can to ensure your next travel adventure is epic.</Typography>
          <br />
          <Stack direction="row" spacing={2}>
            <SocialButton sx={{p: 3}} variant="text"><FacebookIcon/></SocialButton>
            <SocialButton sx={{p: 3}} variant="text"><TwitterIcon/></SocialButton>
            <SocialButton sx={{p: 3}} variant="text"><InstagramIcon/></SocialButton>
          </Stack>
        </Grid>
        <Grid item xs={12} md={4}>
          <Typography variant="h5" sx={{lineHeight: 2}}>2/66 Appel Street, Surfers Paradise, QLD Australia 4217</Typography>
          <br />
          <br />
          <Stack direction="row" spacing={1} sx={{alignItems: 'center'}}>
            <LocalPhoneIcon style={{width: 15, color: theme.palette.grey['A700']}} />
            <Typography style={{color: theme.palette.grey['A700']}}>1800 242 373</Typography>
          </Stack>
          <br />
          <Stack direction="row" spacing={1} sx={{alignItems: 'center'}}>
            <AccessTimeIcon style={{width: 15, color: theme.palette.grey['A700']}} />
            <Typography style={{color: theme.palette.grey['A700']}}>8:00 - 17:00 MON TO FRI</Typography>
          </Stack>
        </Grid>
        <Grid item xs={12} md={4}>
          <Typography display="block" gutterBottom>BEFORE YOU GO</Typography>
          <Typography className={classes.link}><FooterLink>Booking Terms & Conditions</FooterLink></Typography>
          <br />
          <br />
          <Typography display="block" gutterBottom>ABOUT & SUPPORT</Typography>
          <Typography className={classes.link}><FooterLink>About iFlyGo</FooterLink></Typography>
          <Typography className={classes.link}><FooterLink>FAQ's</FooterLink></Typography>
          <Typography className={classes.link}><FooterLink>Testimonials</FooterLink></Typography>
          <Typography className={classes.link}><FooterLink>Privacy Policy</FooterLink></Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography style={{marginLeft: 75, color: theme.palette.grey['A700']}} variant="overline" display="block" gutterBottom><span style={{display: 'inline-block', transform: 'scale(1.3, 1)'}}>© COPYRIGHT {(new Date()).getFullYear()} IFLYGO, ALL RIGHTS RESERVED. IATA NO: 0236 1424 ABN: 74 071 920 360 ATAS NO: A11315</span></Typography>
          <Typography style={{marginLeft: 19, color: theme.palette.grey['A700']}} variant="overline" display="block" gutterBottom><span style={{display: 'inline-block', transform: 'scale(1.3, 1)'}}>ALL TRIP PRICES ARE IN AUD $</span></Typography>
        </Grid>
      </Grid>
    </Box>
  )
}