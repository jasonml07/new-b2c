const BASE_URL = 'https://iflygo.com.au'

const DESTINATIONS = [
  [
    {
      text: 'GO ASIA',
      link: BASE_URL+'/all-asia-trips',

      links: [
        {text: 'Thailand', link: BASE_URL+'/thailand'},
        {text: 'Vietnam', link: BASE_URL+'/vietnam'},
        {text: 'Singapore', link: BASE_URL+'/singapore'},
        {text: 'Indonesia', link: BASE_URL+'/indonesia'},
        {text: 'Maldives', link: BASE_URL+'/maldives'},
      ]
    }
  ],
  [
    {
      text: 'GO OCEANIA',
      link: BASE_URL+'/all-oceania-trips',

      links: [
        {text: 'Australia', link: BASE_URL+'/australia'},
        {text: 'New Zealand', link: BASE_URL+'/new-zealand'},
        {text: 'Fiji', link: BASE_URL+'/fiji'}
      ]
    }
  ],
  [
    {
      text: 'GO EUROPE',
      link: BASE_URL+'/all-europe-trips',

      links: [
        {text: 'Croatia', link: BASE_URL+'/croatia'},
        {text: 'Greece', link: BASE_URL+'/greece'},
        {text: 'Iceland', link: BASE_URL+'/iceland'},
        {text: 'Malta', link: BASE_URL+'/malta'},
        {text: 'Spain', link: BASE_URL+'/spain'},
        {text: 'Turkey', link: BASE_URL+'/turkey'},
      ]
    }
  ],
  [
    {
      text: 'GO MIDDLE EAST',
      // link: BASE_URL+'/all-europe-trips',

      links: [
        {text: 'Jordan', link: BASE_URL+'/jordan'},
        {text: 'Israel', link: BASE_URL+'/israel'},
      ]
    }
  ],
  [
    {
      text: 'GO NORTH AMERICA',
      link: BASE_URL+'/all-north-america-trips',

      links: [
        {text: 'Canada', link: BASE_URL+'/canada'},
        {text: 'United States', link: BASE_URL+'/united-states'},
      ]
    }
  ],
  [
    {
      text: 'GO SOUTH AMERICA',
      link: BASE_URL+'/all-south-america-trips',

      links: [
        {text: 'Brazil', link: BASE_URL+'/brazil'},
        {text: 'Peru', link: BASE_URL+'/peru'},
      ]
    },
    {
      text: 'GO CENTRAL AMERICA & CARIBBEAN',
      // link: BASE_URL+'/all-south-america-trips',

      links: [
        {text: 'Mexico', link: BASE_URL+'/mexico'},
        {text: 'Costa Rica', link: BASE_URL+'/costa-rica'},
      ]
    }
  ],
  [
    {
      text: 'GO AFRICA',
      link: BASE_URL+'/all-africa-trips',

      links: [
        {text: 'South Africa', link: BASE_URL+'/south-africa'}
      ]
    }
  ]
];

const DESTINATION_GROUP_SIZE={
  5: 3
}

const navigate = (link) => {
  window.location.href=link
}

export default {
  DESTINATIONS,
  DESTINATION_GROUP_SIZE,
  navigate,
}