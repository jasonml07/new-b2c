import React from 'react'
import { createTheme } from '@mui/material/styles';
import Header from './Header'
import Footer from './Footer'

const style = createTheme({
  typography: {
    fontFamily: [
      'Rajdhani', 'sans-serif'
    ].join(','),
  },
  palette: {
    primary: {
      main: '#ea1d29',
    }
  }
})

export default {
  style,
  Header,
  Footer
}