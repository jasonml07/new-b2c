import React, {} from 'react';
import { useTheme, styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Link from '@mui/material/Link';
// Icons
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import AccessTimeIcon from '@mui/icons-material/AccessTime';

const SocialButton = styled(Button)(({theme, ...props}) => {
  return {
    color: theme.palette.common.white,
    border: '1px solid '+theme.palette.grey['A700'],
    '&:hover': {
      color: theme.palette.primary.main
    }
  }
})

const FooterLink = styled(Link)(({theme, ...props}) => ({
  cursor: 'pointer',
  color: theme.palette.grey['A700'],
  textDecoration: 'none',
  '&:hover': {
    color: theme.palette.primary.main
  }
}))

const useStyles = makeStyles({
  link: {
    padding: '10px 0'
  }
})

const BASE_URL = 'https://europeholidays.com.au'

export default function Footer({

}) {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <Box sx={{backgroundColor: '#fafafa', color: theme.palette.grey['A700'], width: '100%'}}>
      <Grid container spacing={2} sx={{p: 5}}>
        <Grid item sx={12} md={3}>
          <Typography variant="h6">CONTACT US</Typography>
          <br />
          <Typography>Europe Holidays is a division of Magic Tours International Pty Ltd.</Typography>
          <br />
          <Typography>Suite 1&2 / 66 Appel Street, Surfers Paradise, QLD. 4217 Australia.</Typography>
          <br />
          <Typography>Phone: (07) 5526 2855</Typography>
          <Typography>Toll Free: 1800 242 353</Typography>
        </Grid>
        <Grid item sx={12} md={2}>
          <Typography variant="h6">MOST POPULAR</Typography>
          <br />
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/tours/europe/#austria"}>Austria</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/tours/europe/#czechia"}>Czechia</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/tours/the-mediterranean/#croatia"}>Croatia</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/tours/the-mediterranean/#italy"}>Italy</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/tours/the-mediterranean/#turkey"}>Turkey</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/tours/the-mediterranean/#spain"}>Spain</FooterLink></Typography>
        </Grid>
        <Grid item sx={12} md={3}>
          <Typography variant="h6">ONLINE BROCHURE</Typography>
          <br />
          <Typography>Our Comprehensive <FooterLink href={BASE_URL+"/brochures/"}>Online Brochures</FooterLink> for 2022</Typography>
        </Grid>
        <Grid item sx={12} md={2}>
          <Typography variant="h6">SUPPORT</Typography>
          <br />
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/terms-and-conditions/"}>Terms and Conditions</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/health-safety-procedures/"}>Health & Safety Procedures</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/privacy-policy/"}>Privacy Policy</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/testimonials/"}>Testimonials</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/contact-us/"}>Contact Us</FooterLink></Typography>
          <Typography sx={{p:0.5}}><FooterLink href={BASE_URL+"/about-europe-holidays/"}>About Us</FooterLink></Typography>
        </Grid>
        <Grid item sx={12} md={2}>
          <Typography variant="h6">SOCIAL</Typography>
          <br />
          <Stack direction="row" spacing={1}>
            <Link href="https://www.facebook.com/europeholidaydeals/"><FacebookIcon style={{ color: theme.palette.info.main, width: '2em', height: '2em' }}/></Link>
            <Link href="https://twitter.com/europe_holidays" style={{backgroundColor: '#05aced', padding: 5, height: 25, width: 25, display: 'flex', justifyContent: 'center', marginTop: 7}}><TwitterIcon style={{ color: theme.palette.common.white, width: '1em', height: '1em' }}/></Link>
            <Link href="https://www.instagram.com/Europe_Holidays" style={{backgroundColor: theme.palette.primary.main, padding: 5, padding: 5, height: 25, width: 25, display: 'flex', justifyContent: 'center', marginTop: 7}}><InstagramIcon style={{ color: theme.palette.common.white, width: '1em', height: '1em' }}/></Link>
          </Stack>
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{p: 4, backgroundColor: theme.palette.primary.main, color: theme.palette.common.white}}>
        <Grid item xs={12} style={{paddingTop: 0}}>
          <Typography>COPYRIGHT © 2022. EUROPE HOLIDAYS.</Typography>
        </Grid>
      </Grid>
    </Box>
  )
}