const BASE_URL = 'https://europeholidays.com.au'

const TOURS = [
  {
    text: 'EUROPE & UK',
    links: [
      {text: 'Albania', link: BASE_URL+'/tours/europe/#albania'},
      {text: 'Austria', link: BASE_URL+'/tours/europe/#austria'},
      {text: 'Baltics', link: BASE_URL+'/tours/europe/#baltics'},
      {text: 'Benelux', link: BASE_URL+'/tours/europe/#benelux'},
      {text: 'Bosnia & Herzegovina', link: BASE_URL+'/tours/europe/#bosnia-herzegovina'},
      {text: 'Czechia', link: BASE_URL+'/tours/europe/#czechia'},
      {text: 'France', link: BASE_URL+'/tours/europe/#france'},
      {text: 'Germany', link: BASE_URL+'/tours/europe/#germany'},
      {text: 'Iceland', link: BASE_URL+'/tours/europe/#iceland'},
      {text: 'Poland', link: BASE_URL+'/tours/europe/#poland'},
      {text: 'Rep. of Ireland', link: BASE_URL+'/tours/europe/#republic-of-ireland'},
      {text: 'Romania', link: BASE_URL+'/tours/europe/#romania'},
      {text: 'Scandinavia', link: BASE_URL+'/tours/europe/#scandinavia'},
      {text: 'Serbia', link: BASE_URL+'/tours/europe/#serbia'},
      {text: 'Slovenia', link: BASE_URL+'/tours/europe/#slovenia'},
      {text: 'Switzerland', link: BASE_URL+'/tours/europe/#switzerland'},
      {text: 'Ukraine', link: BASE_URL+'/tours/europe/#ukraine'},
      {text: 'United Kingdom', link: BASE_URL+'/tours/europe/#united-kingdom'},
      {text: 'Christmas & New Year', link: BASE_URL+'/tours/europe/#christmas-new-year'},
    ]
  },{
    text: 'MEDITERRANEAN',
    links: [
      {text: 'Croatia', link: BASE_URL+'/tours/the-mediterranean/#croatia'},
      {text: 'Greece', link: BASE_URL+'/tours/the-mediterranean/#greece'},
      {text: 'Italy', link: BASE_URL+'/tours/the-mediterranean/#italy'},
      {text: 'Malta', link: BASE_URL+'/tours/the-mediterranean/#malta'},
      {text: 'Montenegro', link: BASE_URL+'/tours/the-mediterranean/#montenegro'},
      {text: 'Portugal', link: BASE_URL+'/tours/the-mediterranean/#portugal'},
      {text: 'Spain', link: BASE_URL+'/tours/the-mediterranean/#spain'},
      {text: 'Türkiye', link: BASE_URL+'/tours/the-mediterranean/#turkey'}
    ]
  },{
    text: 'MIDDLE EAST & AFRICA',
    links: [
      {text: 'Egypt', link: BASE_URL+'/tours/the-middle-east/#egypt'},
      {text: 'Israel', link: BASE_URL+'/tours/the-middle-east/#israel'},
      {text: 'Jordan', link: BASE_URL+'/tours/the-middle-east/#jordan'},
      {text: 'Morocco', link: BASE_URL+'/tours/africa/#morocco'},
      {text: 'South Africa', link: BASE_URL+'/tours/africa/#south-africa'}
    ]
  },{
    text: 'WORLDWIDE DESTINATIONS',
    links: [
      {text: 'Cambodia', link: BASE_URL+'/tours/worldwide-destinations/#cambodia'},
      {text: 'Indonesia', link: BASE_URL+'/tours/worldwide-destinations/#indonesia'},
      {text: 'Japan', link: BASE_URL+'/tours/worldwide-destinations/#japan'},
      {text: 'Laos', link: BASE_URL+'/tours/worldwide-destinations/#laos'},
      {text: 'Malaysia', link: BASE_URL+'/tours/worldwide-destinations/#malaysia'},
      {text: 'Philippines', link: BASE_URL+'/tours/worldwide-destinations/#philippines'},
      {text: 'Singapore', link: BASE_URL+'/tours/worldwide-destinations/#singapore'},
      {text: 'South Korea', link: BASE_URL+'/tours/worldwide-destinations/#south-korea'},
      {text: 'Taiwan', link: BASE_URL+'/tours/worldwide-destinations/#taiwan'},
      {text: 'Thailand', link: BASE_URL+'/tours/worldwide-destinations/#thailand'},
      {text: 'Vietnam', link: BASE_URL+'/tours/worldwide-destinations/#vietnam'}
    ]
  }
]

const CITY_BREAKS = [
  {
    text: 'EUROPE & UK',
    links: [
      {text: 'Albania', link: BASE_URL+'/city-breaks/#austria'},
      {text: 'Baltics', link: BASE_URL+'/city-breaks/#baltics'},
      {text: 'Benelux', link: BASE_URL+'/city-breaks/#benelux'},
      {text: 'Czechia', link: BASE_URL+'/city-breaks/#czechia'},
      {text: 'France', link: BASE_URL+'/city-breaks/#france'},
      {text: 'Germany', link: BASE_URL+'/city-breaks/#germany'},
      {text: 'Hungary', link: BASE_URL+'/city-breaks/#hungary'},
      {text: 'Iceland', link: BASE_URL+'/city-breaks/#iceland'},
      {text: 'Poland', link: BASE_URL+'/city-breaks/#poland'},
      {text: 'Rep. of Ireland', link: BASE_URL+'/city-breaks/#republic-of-ireland'},
      {text: 'Romania', link: BASE_URL+'/city-breaks/#romania'},
      {text: 'Scandinavia', link: BASE_URL+'/city-breaks/#scandinavia'},
      {text: 'Slovakia', link: BASE_URL+'/city-breaks/#slovakia'},
      {text: 'Ukraine', link: BASE_URL+'/city-breaks/#ukraine'},
      {text: 'United Kingdom', link: BASE_URL+'/city-breaks/#united-kingdom'},
      {text: 'Christmas', link: BASE_URL+'/city-breaks/#christmas'}
    ]
  },
  {
    text: 'MEDITERRANEAN',
    links: [
      {text: 'Croatia', link: BASE_URL+'/city-breaks/#croatia'},
      {text: 'Greece', link: BASE_URL+'/city-breaks/#greece'},
      {text: 'Italy', link: BASE_URL+'/city-breaks/#italy'},
      {text: 'Malta', link: BASE_URL+'/city-breaks/#malta'},
      {text: 'Portugal', link: BASE_URL+'/city-breaks/#portugal'},
      {text: 'Spain', link: BASE_URL+'/city-breaks/#spain'},
      {text: 'Türkiye', link: BASE_URL+'/city-breaks/#turkey'}
    ]
  },
  {
    text: 'MIDDLE EAST & AFRICA',
    links: [
      {text: 'Egypt', link: BASE_URL+'/city-breaks/#egypt'},
      {text: 'Israel', link: BASE_URL+'/city-breaks/#israel'},
      {text: 'Jordan', link: BASE_URL+'/city-breaks/#jordan'},
      {text: 'Morocco', link: BASE_URL+'/city-breaks/#morocco'},
      {text: 'United Arab Emirates', link: BASE_URL+'/city-breaks/#united-arab-emirates'}
    ]
  },
  {
    text: 'WORLDWIDE DESTINATIONS',
    links: [
      {text: 'Bali, Indonesia', link: BASE_URL+'/city-breaks/#bali'},
      {text: 'Cambodia', link: BASE_URL+'/city-breaks/#cambodia'},
      {text: 'Laos', link: BASE_URL+'/city-breaks/#laos'},
      {text: 'Singapore', link: BASE_URL+'/city-breaks/#singapore'},
      {text: 'South Korea', link: BASE_URL+'/city-breaks/#south-korea'},
      {text: 'Taiwan', link: BASE_URL+'/city-breaks/#taiwan'},
      {text: 'Thailand', link: BASE_URL+'/city-breaks/#thailand'},
      {text: 'Vietnam', link: BASE_URL+'/city-breaks/#vietnam'},
      {text: 'USA', link: BASE_URL+'/city-breaks/#usa'}
    ]
  }
]

const ISLAN_HOPPING = [
  {
    text: 'ISLAND HOPPING',
    links: [
      { text: 'Croatia', link: BASE_URL+'/island-hopping-cruises/island-hopping/#croatia'},
      { text: 'Greece', link: BASE_URL+'/island-hopping-cruises/island-hopping/#greece'},
      { text: 'Spain', link: BASE_URL+'/island-hopping-cruises/island-hopping/#spain'}
    ]
  },
  {
    text: 'CRUISES',
    links: [
      { text: 'Croatia', link: BASE_URL+'/island-hopping-cruises/cruises/#croatia'},
      { text: 'Greece', link: BASE_URL+'/island-hopping-cruises/cruises/#greece'},
      { text: 'Mediterranean', link: BASE_URL+'/island-hopping-cruises/cruises/#the-mediterranean'},
      { text: 'Worldwide', link: BASE_URL+'/island-hopping-cruises/cruises/#worldwide'}
    ]
  },
  {
    text: 'RIVER CRUISING',
    links: [
      { text: 'Austria & Germany', link: BASE_URL+'/island-hopping-cruises/river-cruises/#austria-germany'},
      { text: 'Benelux', link: BASE_URL+'/island-hopping-cruises/river-cruises/#benelux'},
      { text: 'France', link: BASE_URL+'/island-hopping-cruises/river-cruises/#france'},
      { text: 'Mediterranean', link: BASE_URL+'/island-hopping-cruises/river-cruises/#the-mediterranean'},
      { text: 'UK & Ireland', link: BASE_URL+'/island-hopping-cruises/river-cruises/#uk-ireland'},
      { text: 'Christmas & New Year', link: BASE_URL+'/island-hopping-cruises/river-cruises/#christmas-new-year'}
    ]
  }
]

const navigate = (url) => {
  window.location.href=url
}

export default {
  TOURS,
  CITY_BREAKS,
  ISLAN_HOPPING,
  navigate
}