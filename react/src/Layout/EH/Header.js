import React, { Fragment, useEffect } from 'react';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Collapse from '@mui/material/Collapse';
import { styled } from '@mui/material/styles';
// Images
import Logo from '../../../assets/img/eh/logo.jpeg'
// Icons
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
// Machine
import { createMachine } from 'xstate'
import { useMachine } from '@xstate/react'
// Components
import Collapsable from './Header.collapse'

import Menus from './Header.menu'

const DropdownLink = styled(Typography)(({theme, ...props}) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const HeaderLink = styled('div')(({theme, ...props}) => {

  return {
    cursor: 'pointer',
    padding: '0 20px',
    display: 'inline-block',
    '& > p': {
      fontSize: '16px',
      fontWeight: 400,
      lineHeight: '24px',

      '& > svg': {
        transitionDuration: '.5s',
        transitionProperty: 'transform'
      },

      '&:hover': {
        color: theme.palette.primary.main,

        '& > svg': {
          transform: 'rotate(180deg)'
        }
      }
    }
  }
})


const MegaMenu = createMachine({
  id: 'MegaMenu',
  initial: 'inactive',
  on: {
    INACTIVE: 'inactive',
    TOURS: 'tours',
    CITY_BREAKS: 'cityBreaks',
    ISLAND_HOPING: 'islandHoping'
  },
  states: {
    inactive: {},
    tours: {},
    cityBreaks: {},
    islandHoping: {},
  }
})

export default function Header({

}) {
  const MegaMenuState = useMachine(MegaMenu)

  const handleHover = (state) => {
    MegaMenuState[1](state)
  }

  useEffect(() => {
    const handleScroll = (event) => {
      if(window.scrollY > 400) {
        handleHover('INACTIVE')
      }
    }
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    }
  }, [])
  return (
    <Fragment>
      <Paper key="header" sx={{width: '100%', position: 'fixed', zIndex: 9999, borderRadius: 0, boxShadow: 'none', border: '1px solid #e8e8e8'}}>
        <Grid container spacing={2} sx={{width: '100%'}}>
          <Grid item xs={2}>
            <Typography align="center">
              <img src={Logo} alt="Europe holidays logo" style={{height: 70}}/>
            </Typography>
          </Grid>
          <Grid item xs={8} sx={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <HeaderLink onMouseEnter={() => handleHover('TOURS')}>
              <DropdownLink>Tours <KeyboardArrowDownIcon /></DropdownLink>
            </HeaderLink>
            <HeaderLink onMouseEnter={() => handleHover('CITY_BREAKS')}>
              <DropdownLink>City Breaks <KeyboardArrowDownIcon /></DropdownLink>
            </HeaderLink>
            <HeaderLink onMouseEnter={() => handleHover('ISLAND_HOPING')}>
              <DropdownLink>Island Hopping & Cruises <KeyboardArrowDownIcon /></DropdownLink>
            </HeaderLink>
            <HeaderLink onMouseEnter={() => handleHover('INACTIVE')}>
              <Typography>Flights</Typography>
            </HeaderLink>
            <HeaderLink onMouseEnter={() => handleHover('INACTIVE')}>
              <Typography>Inspiration</Typography>
            </HeaderLink>
            <HeaderLink onMouseEnter={() => handleHover('INACTIVE')}>
              <Typography>Brochures</Typography>
            </HeaderLink>
          </Grid>
          <Grid item xs={2}>
          </Grid>
        </Grid>
      </Paper>
      <Box sx={{height: '100px'}}>
      </Box>
      <Collapsable navigate={Menus.navigate} menu={Menus.TOURS} collapse={MegaMenuState[0].matches('tours')}/>
      <Collapsable navigate={Menus.navigate} menu={Menus.CITY_BREAKS} collapse={MegaMenuState[0].matches('cityBreaks')}/>
      <Collapsable navigate={Menus.navigate} column={4} menu={Menus.ISLAN_HOPPING} collapse={MegaMenuState[0].matches('islandHoping')}/>
    </Fragment>
  )
}