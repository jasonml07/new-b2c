import React, { Fragment } from 'react';
import PropTypes from 'prop-types'
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Collapse from '@mui/material/Collapse';
import { styled } from '@mui/material/styles';

const CollapseHeader = styled(Typography)(({theme, ...props}) => ({
  width: '100%',
  fontSize: '21px',
  lineHeight: '26px',
  fontWeight: '700',
  borderBottom: '1px solid #EC1C29'
}))
const CollapseLink = (styled)(Typography)(({theme, ...props}) => ({
  cursor: 'pointer',
  '&:hover': {
    textDecoration: 'underline'
  },
  '& > a': {
    textDecoration: 'none',
    color: 'rgba(0, 0, 0, 0.87)'
  }
}))

function Collapsable({
  column,
  menu,
  collapse,
  navigate
}) {
  return (
    <Collapse timeout={200} in={collapse} sx={{width: '100%', position: 'fixed', top: '77px', zIndex: 2}} easing={{enter: 'ease-in', exit: ''}}>
      <Paper sx={{backgroundColor: '#fbfafa', position: 'relative', top: 40}}>
        <Grid container spacing={5} sx={{px: 8, paddingBottom: '40px'}}>
          {
            menu.map((group, index) => (
              <Grid item xs={column} key={'group-'+index}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sx={{display: 'flex', alignItems: 'end', justifyContent: 'center'}}>
                      <CollapseHeader align="center" >{group.text}</CollapseHeader>
                    </Grid>
                    {
                      group.links.map((item, i) => (
                        <Grid item xs={4} key={'item-'+i}>
                          <CollapseLink>
                            <a href={item.link}>{item.text}</a>
                          </CollapseLink>
                        </Grid>
                      ))
                    }
                  </Grid>
              </Grid>
            ))
          }
        </Grid>
      </Paper>
    </Collapse>
  )
}
Collapsable.propTypes = {
  column: PropTypes.number,
  menu: PropTypes.array,
  collapse: PropTypes.bool,
  navigate: PropTypes.func
}
Collapsable.defaultProps = {
  column: 3,
  menu: [],
  collapse: false
}

export default Collapsable