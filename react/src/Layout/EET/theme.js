import React from 'react'
import { createTheme } from '@mui/material/styles';
import Header from './Header'
import Footer from './Footer'

const style = createTheme({
  typography: {
    fontFamily: [
      'Cabin', 'Open Sans', 'sans-serif'
    ].join(','),
  },
  palette: {
    primary: {
      main: '#fe0000',
    }
  }
})

export default {
  style,
  Header,
  Footer
}