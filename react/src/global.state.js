import React, { createContext } from 'react';
import { assign } from 'xstate'
import { useInterpret } from '@xstate/react';
import AppMachine from './machines/app.machine'
import ReviewMachine from './machines/review.machine'

export const GlobalState = createContext({});

export const GlobalStateProvider = (props) => {
  const AppService = useInterpret(AppMachine)
  const ReviewService = useInterpret(ReviewMachine)
  return (
    <GlobalState.Provider value={{ AppService, ReviewService }}>
      {props.children}
    </GlobalState.Provider>
  )
}

