const discount = (price, discount) => {
  if(discount) {
    const newPrice = parseFloat(price)
    if(discount.type==='PERCENTAGE') {
      const lessPrice = newPrice * (parseFloat(discount.value) / 100)
      return Math.ceil(lessPrice)
    }
  }
  return 0
}

export default {
  discount
}