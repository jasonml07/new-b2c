import React, {} from 'react'
import PropTypes from 'prop-types'
// Material
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
// Icons
import ErrorIcon from '@mui/icons-material/Error';


function PageNotFound({
  onRetry = () => {}
}) {

  return (
    <Paper elavation={6} sx={{m:20, p:2}}>
      <Typography variant="h2" align="center"><ErrorIcon color="error" style={{fontSize: '1.5em'}}/></Typography>
      <Typography variant="h4" align="center">Oops! Something wen't wrong.</Typography>
      <br />
      <Typography align="center">
        <Button variant="contained" onClick={() => onRetry()}>
          Click to try again!
        </Button>
      </Typography>
    </Paper>
  )
}

PageNotFound.propTypes = {
  onRetry: PropTypes.func
}

export default PageNotFound