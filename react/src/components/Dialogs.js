import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
// Material
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
// Icons
import CancelIcon from '@mui/icons-material/Cancel';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';


function Loading({
  open,
  message,
}) {
  const theme = useTheme();
  return (
    <Backdrop
      sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={open}
    >
      <div style={{display: 'inline-block'}}>
        <Typography align="center">
          <CircularProgress color="inherit" />
        </Typography>
        {message}
      </div>
    </Backdrop>
  )
}

Loading.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.oneOfType([PropTypes.String, PropTypes.object])
}
Loading.defaultValue = {
  open: false,
  message: ''
}

function Success({
  open,
  title,
  message,
  onClose
}) {
  const theme = useTheme()
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="success-dialog-title"
      aria-describedby="success-dialog-description"
    >
      <DialogTitle id="success-dialog-title">
        <Typography variant="h5" align="center">{title}</Typography>
      </DialogTitle>
      <DialogContent>
        <Typography align="center"><CheckCircleIcon style={{fontSize: '6em', color: theme.palette.success.main}} /></Typography>
        <DialogContentText id="success-dialog-description">
          <Typography align="center">{message}</Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Close</Button>
      </DialogActions>
    </Dialog>
  )
}

Success.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func
}
function Failure({
  open,
  title,
  message,
  onClose
}) {
  const theme = useTheme()
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="failure-dialog-title"
      aria-describedby="failure-dialog-description"
    >
      <DialogTitle id="failure-dialog-title">
        <Typography variant="h5" align="center">{title}</Typography>
      </DialogTitle>
      <DialogContent>
        <Typography align="center"><CancelIcon style={{fontSize: '6em', color: theme.palette.error.main}} /></Typography>
        <DialogContentText id="failure-dialog-description">
          <Typography align="center">{message}</Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Close</Button>
      </DialogActions>
    </Dialog>
  )
}

Failure.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func
}

export default {
  Loading,
  Success,
  Failure
}