import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
// Material
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

const Alert = forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function Error({
  open,
  onClose = () => {},
  message
}) {

  return (
    <Snackbar open={open} autoHideDuration={6000} onClose={onClose} anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
      <Alert onClose={onClose} severity="error" sx={{ width: '100%' }}>
        {message}
      </Alert>
    </Snackbar>
  )
}
Error.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  message: PropTypes.string
}

function Loading({
  open,
  message
}) {

  return (
    <Snackbar open={open} onClose={onClose} anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
      <Alert onClose={onClose} sx={{ width: '100%' }}>
        {message}
      </Alert>
    </Snackbar>
  )
}
Loading.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string
}

export default {
  Error,
  Loading
}