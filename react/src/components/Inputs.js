import React, {} from 'react';
import PropTypes from 'prop-types';
// Material
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import {default as MUISelect} from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
// Icons
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';


function Select({
  error,
  label,
  value,
  onChange,
  options,
  id
}) {
  return (
    <FormControl fullWidth error={Boolean(error)}>
      <InputLabel id={id}>{label}</InputLabel>
      <MUISelect
        labelId={id}
        id={id}
        label={label}
        value={value}
        onChange={onChange}
      >
        {
          (options||[]).map(item => {
            if(typeof item == 'object') {
              return (
                <MenuItem value={item.value}>{item.label}</MenuItem>
              )
            }
            return (
              <MenuItem value={item}>{item}</MenuItem>
            )
          })
        }
      </MUISelect>
      {Boolean(error) && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  )
}

Select.propTypes = {
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func,
  options: PropTypes.array,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}
Select.defaultValues = {
  id: 'select-'+(new Date()).getTime(),
  options: [],
  onChange: () => {}
}


function AddMinus({
  onChange,
  ...props
}) {
  const handleSubButton = () => {
    let value = Number(props.value) - 1
    if(value < 0) {
      value = 0
    }
    onChange({
      target: {
        name: props.name,
        value: value
      }
    })
  }
  const handleAddButton = () => {
    let value = Number(props.value) + 1
    if(props.maxValue > 0 && value > Number(props.maxValue)) {
      value = Number(props.value)
    }
    onChange({
      target: {
        name: props.name,
        value: value
      }
    })
  }
  const handleTextChange = (event) => {
    let value = Number(event.target.value)
    if(value < 0) {
      value = 0
    }
    if(props.maxValue > 0 && value > Number(props.maxValue)) {
      value = Number(props.value)
    }

    onChange({
      target: {
        name: props.name,
        value: value
      }
    })
  }
  return (
    <Box sx={{w: props.width, display: 'flex'}}>
      <Button variant="contained" color="warning" onClick={handleSubButton}>
        <RemoveIcon />
      </Button>
      <TextField
        sx={{'& > input': {textAlign: 'center'}}}
        label={props.label}
        size={props.size}
        variant={props.variant}
        name={props.name}
        value={props.value}
        onChange={handleTextChange}
      />
      <Button variant="contained" color="primary" onClick={handleAddButton}>
        <AddIcon />
      </Button>
    </Box>
  )
}
AddMinus.propTypes = {
  width: PropTypes.number,
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  variant: PropTypes.string,
  size: PropTypes.string,
  maxValue: PropTypes.number,
  onChange: PropTypes.func
}
AddMinus.defaultProps = {
  width: 200,
  label: '',
  size: 'small',
  value: 0,
  maxValue: -1,
  variant: 'outlined',
  onChange: () => {}
}


export default {
  Select,
  AddMinus
}