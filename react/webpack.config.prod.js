const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const port = process.env.PORT || 3000;

module.exports = {
  mode: 'development',
  // Webpack configuration goes here
  entry: './src/index.js',
  output: {
    filename: 'static/[name].[fullhash].js',
    path: path.resolve(__dirname, '../static/v2/tour'),
    publicPath: '/static/v2/tour/'
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
      // First Rule
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },

      // Second Rule
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localsConvention: 'camelCase',
              sourceMap: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: '../../../application/views/v2/tour/index.html',
      template: 'public/index.html',
      // favicon: 'public/favicon.ico'
    })
  ],
  devServer: {
    host: 'localhost',
    allowedHosts: [
      'new-b2c.test'
    ],
    port: port,
    historyApiFallback: true,
    open: true
  }
};