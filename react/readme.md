# React Project
> New UI for B2B

### Development
- Run `yarn install`
- Start web by `yarn start`


## Creating Tour Data

- Tour that having double or twin must be name as (double/twin)
- When tour is double/twin, can be named as double or twin
- Double/Twin room or cabin will be generated 2 selection namely Double and Twin cabin/room
- Where double or twin room/cabin type will be as ease.

![rooms-and-cabins.png](./rooms-and-cabins.png)