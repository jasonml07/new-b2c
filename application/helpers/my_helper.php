<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('_AGENCY_SOCIALS') )
{
	# Code here
	function _AGENCY_SOCIALS ( $code )
	{
		# Code here
		$socials = json_decode(AGENCY_SOCIALS, TRUE);

		if ( isset( $socials[ $code ] ) )
		{
			# Code here
			return $socials[ $code ];
		} // end of if statement
		return FALSE;
	} // end of _AGENCY_SOCIALS
} // end of if statement

if ( ! function_exists('is_in_range_date') )
{
	# Code here
	function is_in_range_date ( $start, $end, $check )
	{
		# Code here
		return (( strtotime( $check ) >= strtotime( $start ) ) && ( strtotime( $check ) <= strtotime( $end ) ));
	} // end of is_in_range_date
} // end of if statement

if ( ! function_exists('_array_key_exists') )
{
	# Code here
	function _array_key_exists ( $array, $keys )
	{
		# Code here
		$exists = TRUE;
		foreach ( $keys as $key )
		{
			# code...
			if ( ! isset( $array[ $key ] ) )
			{
				# Code here
				$exists = FALSE;
				break;
			} // end of if statement
		}

		return $exists;
	} // end of _array_key_exists
} // end of if statement

if ( ! function_exists('_APPCODE') )
{
	# Code here
	function _APPCODE ( $type )
	{
		# Code here
		$codes = json_decode(APPLICATION_CODES, TRUE);

		if ( !@$codes[ $type ] )
		{
			# Code here
			return FALSE;
		} // end of if statement

		return $codes[ $type ];
	} // end of _APPCODE
} // end of if statement

if ( ! function_exists('date_difference') )
{
	# Code here
	function date_difference ( $date, $now = null )
	{
		# Code here
		$now       = is_null($now) ? time() : strtotime($now); // or your date as well
		$your_date = strtotime( $date );
		$datediff  = $your_date - $now;

		return floor($datediff / (60 * 60 * 24));
	} // end of date_d
} // end of if statement

if ( ! function_exists('is_item_exists') )
{
	# Code here
	function is_item_exists ( $collectionName, $property, $id )
	{
		# Code here
		$connection = new MongoClient();
		$db = $connection->db_system;

		$collection = $db->selectCollection( $collectionName );

		$count = $collection->find( array( $property => $id ) )->count();

		$connection->close();

		return ( $count > 0 );
	} // end of is_item_exists
} // end of if statement

if ( ! function_exists('item_costing') )
{
	# Code here
	function item_costing ( $converted, $commisionPCT, $markupPTC, $discountPCT=0 )
	{
		# Code here
		
		$net_amount_agency = (float)$converted;
		$markup_amount     = ($net_amount_agency * convert_decimal( (float)$markupPTC ) );
		$gross_amount      = round($markup_amount + $net_amount_agency);
		$commission        = $gross_amount * convert_decimal( (float)$commisionPCT );
		$discount          = $gross_amount * convert_decimal( (float)$discountPCT );
		$total_less_amount = $commission+$discount;
		$due_amount        = $gross_amount - $total_less_amount;
		$profit_amount     = $markup_amount - $total_less_amount;

		return array(
			'net_amount_agency' => $net_amount_agency,
			'gross_amount'      => $gross_amount,
			'markup_pct'        => $markupPTC,
			'markup_amount'     => $markup_amount,
			'discount_pct'      => $discountPCT,
			'discount_amount'   => $discount,
			'commission_pct'    => $commisionPCT,
			'commission_amount' => $commission,
			'total_less_amount' => $total_less_amount,
			'due_amount'        => $due_amount,
			'profit_amount'     => $profit_amount,
		);
	} // end of item_costing
} // end of if statement

if ( ! function_exists('convert_decimal') )
{
	# Code here
	function convert_decimal ( $number )
	{
		# Code here
		return ( (float) $number / 100 );
	} // end of convert_decimal
} // end of if statement

if ( ! function_exists('generate_token') )
{
	# Code here
	function generate_token ( $name )
	{
		# Code here
		$CI =& get_instance();

		$token = random_string('unique');

		$CI->nativesession->set( $name, $token );
		return $token;
	} // end of generate_token
} // end of if statement

if ( ! function_exists('_AGENCY') )
{
	# Code here
	function _AGENCY ( $code )
	{
		# Code here
		$connection = new MongoClient();
		$db         = $connection->db_system;

		$agency = $db->agency->findOne(
			array('agency_code' => $code),
			array('_id' => 0)
		);

		$connection->close();

		return $agency;
	} // end of _AGENCY
} // end of if statement

if ( ! function_exists('_AGENT_INFO') )
{
	# Code here
	/**
	 * Returns the agency information
	 * @param  [type]  $code     [description]
	 * @param  boolean $toBase64 [description]
	 * @return [type]            [description]
	 */
	function _AGENT_INFO ( $code )
	{
		# Code here
		$connection = new MongoClient();
		$db = $connection->db_system;
		
		$information = $db->agency->findOne(
			array('agency_code' => $code),
			array('_id' => 0)
		);

		$db = $connection->db_agency;
		$agency = $db->agency_b2c_setup->findOne(array('agency_code' => base64_encode( $code )), array('_id' => 0));

		$connection->close();
		
		if ( ! is_null( $agency ) )
		{
			# Code here
			$information = array_merge($information, $agency);
		} // end of if statement
		return $information;
	} // end of _AGENT_INFO
} // end of if statement

if ( ! function_exists('_POST') )
{
	# Code here
	function _POST (  )
	{
		# Code here
		$_POST = ( empty($_POST) )? json_decode(file_get_contents('php://input'), TRUE): $_POST;
	} // end of _POST
} // end of if statement


if ( ! function_exists('response') )
{

	function response(  $data, $statusCode = 200 ,$xAjaxFunction=false)
	{
		header('Content-type: application/json');
		header('Response', TRUE, $statusCode);
		ob_start();
		if($xAjaxFunction){
			echo $xAjaxFunction."('".json_encode( $data )."');";
		}else{
			echo json_encode( $data );
		}
		

		ob_end_flush();
		exit();
	}
}



if ( ! function_exists('get_next_id') )
{
	# Code here
	function get_next_id ( $collection )
	{
		# Code here
		$connection = new MongoClient();
		$db = $connection->db_system;
		$id = 0;
		//echo $collection;
		
        //$res = $db->counters->findAndModify(array("query"=>array("_id"=>1),"update"=>array("$inc"=>array("seq"=>1)),"new"=>true));
		$res = $db->counters->findAndModify(
				array( '_id'    => $collection ),
				array( '$inc'   => array('seq'=>1) ),
				array( 'seq'    => 1, '_id' => 0 ),
				array( 'upsert' => true, 'new' => true )
			);
		//print_r($res);
		
		$connection->close();
    return $res['seq'];
	} // end of get_next_id
} // end of if statement

if ( ! function_exists('cURL') )
{
	# Code here
	function cURL ( $url, $params, $customURL = '' )
	{
		# Code here
		$mainURL = SYS_ADMIN_IP . '_ajax/' . $url . '.php';
		if ( ! empty( $customURL ) )
		{
			# Code here
			$mainURL = $customURL;
		} // end of if statement
		$ch = curl_init(); 
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
        curl_setopt($ch, CURLOPT_URL, $mainURL); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $result = curl_exec($ch);

	$result = json_decode($result, TRUE);
		
		#$CI =& get_instance();
		// $CI->load->model('log_model');
		// $CI->log_model->debug( 'CURL :: ' . json_encode(curl_getinfo($ch)) );
		curl_close($ch);

		return $result;
	} // end of cURL
} // end of if statement

if ( ! function_exists('d_redirect') )
{
	# Code here
	function d_redirect ( $link, $delay = 3 )
	{
		# Code here
		header( 'refresh:' . $delay . '; url=' . base_url() . $link );
		exit();
	} // end of d_redirect
} // end of if statement

if ( ! function_exists('xml_to_array') )
{
	# Code here
	function xml_to_array ( $xml )
	{
		# Code here
		$CI =& get_instance();
		$CI->load->library('SimpleXml2Array', array('xml' => $xml));
        $xml = new SimpleXml2Array(array('xml' => $xml));

        return $xml->arr;
	} // end of xml_to_array
} // end of if statement

if ( ! function_exists('wrapp_xml') )
{
	# Code here
	function wrapp_xml ( $curl_response )
	{
		# Code here
		return '<main>' . str_replace('<?xml version="1.0"?>', '', $curl_response) . '</main>';
	} // end of wrapp_xml
} // end of if statement

if ( ! function_exists('readable_xml') )
{
	# Code here
	function readable_xml ( $xml )
	{
		# Code here
		if( empty( $xml ) )
        {
            return FALSE;
        }

        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->formatOutput = TRUE;
        // echo $dom->saveXml();
        $dom->loadXML($xml);
        return $dom->saveXml();
	} // end of readable_xml	
} // end of if statement

if ( ! function_exists('check_login') )
{
	# Code here
	function check_login ( $doReturn = FALSE )
	{
		# Code here
		$CI =& get_instance();

		if( $doReturn === TRUE )
		{
			return ( $CI->session->userdata('logged_in') != 1 )?FALSE:TRUE;
		}
		if($CI->session->userdata('logged_in') != 1)
		{
			header("Location: ".base_url()."");
			die();
		}
	} // end of check_login
} // end of if statement

if ( ! function_exists('unique_transaction_id') )
{
	# Code here
	function unique_transaction_id (  )
	{
		# Code here
		$connection = new MongoClient();
		$db = $connection->db_system;

		$transactionId = 'E' . random_string('numeric', 10);

		$count = $db->transaction_id->find( array('reference' => $transactionId) )->count();

		if ( $count > 0 )
		{
			# Code here
			$connection->close();
			unique_transaction_id();
		} // end of if statement
		$db->transaction_id->insert( array('reference' => $transactionId) );
		$connection->close();
		return $transactionId;
	} // end of unique_transaction_id
} // end of if statement

if ( ! function_exists('generate_DKIM') )
{
	# Code here
	function generate_DKIM ( $to, $from, $subject, $body )
	{
		# Code here
		$domain_d    = DOMAIN_D;
		$domain_s    = DOMAIN_S;
		$domain_priv = DOMAIN_PRIV;

		include_once 'application/libraries/class.mailDomainSigner.php';
		$headers            = array();
		$headers['from']    = "From: {$from}";
		$headers['to']      = "To: {$to}";
		$headers['subject'] = "Subject: {$subject}";
		$headers['mimever'] = "MIME-Version: 1.0";
		$headers['date']    = "Date: ".date('r');
		$headers['mid']     = "Message-ID: <".sha1(microtime(true))."@{$domain_d}>";
		$headers['ctype']   = "Content-Type: text/html; charset=windows-1252";
		$headers['cencod']  = "Content-Transfer-Encoding: quoted-printable";

		// Create mailDomainSigner Object
		$mds = new mailDomainSigner($domain_priv,$domain_d,$domain_s);

		$dkim_sign = $mds->getDKIM(
			"from:to:subject:mime-version:date:message-id:content-type:content-transfer-encoding",
			array(
				$headers['from'],
				$headers['to'],			
				$headers['subject'],
				$headers['mimever'],
				$headers['date'],
				$headers['mid'],
				$headers['ctype'],
				$headers['cencod']
			),
			$body
		);

		// Create DomainKey-Signature Header
		$domainkey_sign = $mds->getDomainKey(
				"from:to:subject:mime-version:date:message-id:content-type:content-transfer-encoding",
				array(
					$headers['from'],
					$headers['to'],			
					$headers['subject'],
					$headers['mimever'],
					$headers['date'],
					$headers['mid'],
					$headers['ctype'],
					$headers['cencod']
				),
				$body
			);

		// Create Email Data, First Headers was DKIM and DomainKey
		$email_data = "{$dkim_sign}\r\n";
		$email_data.= "{$domainkey_sign}\r\n";

		// Include Other Headers
		foreach($headers as $val){
			$email_data.= "{$val}\r\n";
		}
		
		/// OK, Append the body now
		$email_data.= "\r\n{$body}";

		return $email_data;
	} // end of generate_DKIM
} // end of if statement


if(!function_exists('save_file')) {
	/**
	 * [save_file description]
	 * @param  [type] $path       [description]
	 * @param  [type] $content    [description]
	 * @param  string $permission [description]
	 * @return [type]             [description]
	 */
	function save_file($path, $content, $permission = 'r+')
	{
		$exists = read_file($path);
		if(!$exists) {
			log_message('info', 'File not exist, creating. . .');
			fopen($path, "w");
		}
		$isSaved = write_file($path, $content, $permission);
		if($isSaved) {
			log_message('info', 'File saved '.$path);
		} else {
			log_message('error', 'Failed to save '.$path);
		}
	}
}

if(!function_exists('save_url_file')) {
	/**
	 * [save_url_file description]
	 * @param  [type]  $path          [description]
	 * @param  string  $location      [description]
	 * @param  string  $permission    [description]
	 * @param  boolean $forRedownload [description]
	 * @return [type]                 [description]
	 */
	function save_url_file($path, $location = '', $permission = 'r+', $forRedownload = false)
	{
		$exists = read_file($location);
		if(!$exists) {
			log_message('info', 'File not exist, creating. . .');
			fopen($location, "w");
		}
		$content = '';
		if(!$exists || $forRedownload) {
			log_message('info', 'Fetching file for '. ($forRedownload ?'redownloading' :'not exist'));
			$content =  file_get_contents($path);
		}
		save_file($location, $content, $permission);
	}
}
