<?php
if (! function_exists('TZ_agency_code_map')) {
	function TZ_agency_code_map($code){
		$codes=json_decode(AGENCY_CODE_MAP,TRUE);

		return isset($codes[$code])?$codes[$code]:$code;
	}
}
if ( ! function_exists('TZ_custom_theme') )
{
	/**
	 * [TZ_custom_theme description]
	 * @param [type] $code [description]
	 */
	function TZ_custom_theme ( $code )
	{
		$CI =& get_instance();
		$type = $CI->nativesession->get('SYSTEM');
		
		$excludedTheme = json_decode(CUSTOM_THEME, TRUE);

		if ( isset( $excludedTheme[ $code ] ) && $type )
		{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$config = $db->agency_b2b_b2c->findOne(array('agency_code' => $code), array('_id' => 0));

			if ( ! empty( $config ) && $config[ strtolower( $type ) ] == 1 )
			{
				# Code here
				return empty( $excludedTheme[ $code ] ) ? FALSE : $excludedTheme[ $code ];
			} // end of if statement
		} // end of if statement
		return FALSE;
	} // end of TZ_custom_theme
} // end of if statement

if ( ! function_exists('TZ_custom_theme_style') )
{
	/**
	 * [TZ_custom_theme_style description]
	 * @param [type] $code [description]
	 */
	function TZ_custom_theme_style ( $code )
	{
		# Code here
		$excludedTheme = TZ_custom_theme( $code );

		if ( ! $excludedTheme )
		{
			return FALSE;
		} // end of if statement
		# Code here
		$CI =& get_instance();
		$styles = $excludedTheme['STYLE'];
		if ( is_array( $styles ) )
		{
			# Code here
			foreach ( $styles as $key => $style )
			{
				?>
				<style type="text/css">
					<?=$CI->load->view( 'themes/' . trim( $code ) . '/' . $style, TRUE )?>
				</style>
				<?php
			}
			return TRUE;
		} // end of if statement

		?>
		<style type="text/css">
			<?=$CI->load->view( 'themes/' . trim( $code ) . '/' . $styles, TRUE )?>
		</style>
		<?php
		return TRUE;
	} // end of TZ_custom_theme_style
} // end of if statement

if ( ! function_exists('TZ_custom_theme_script') )
{
	/**
	 * [TZ_custom_theme_script description]
	 * @param [type] $code [description]
	 */
	function TZ_custom_theme_script ( $code )
	{
		# Code here
		$excludedTheme = TZ_custom_theme( $code );

		if ( ! $excludedTheme )
		{
			return FALSE;
		} // end of if statement
		# Code here
		$CI =& get_instance();
		$scripts = $excludedTheme['SCRIPT'];

		if ( is_array( $scripts ) )
		{
			# Code here
			foreach ( $scripts as $key => $script )
			{
				# code...
				?>
				<script type="text/javascript">
					<?=$CI->load->view( 'themes/' . trim( $code ) . '/' . $script, TRUE ) ?>
				</script>
				<?php
			}

			return TRUE;
		} // end of if statement
		?>
		<script type="text/javascript">
			<?=$CI->load->view( 'themes/' . trim( $code ) . '/' . $scripts, TRUE ) ?>
		</script>
		<?php
		return TRUE;
	} // end of TZ_custom_theme_script
} // end of if statement

if ( ! function_exists('TZ_custom_theme_header') )
{
	/**
	 * [TZ_custom_theme_header description]
	 * @param [type] $code [description]
	 */
	function TZ_custom_theme_header ( $code )
	{
		# Code here
		$excludedTheme = TZ_custom_theme( $code );

		if ( ! $excludedTheme )
		{
			# Code here
			return FALSE;
		} // end of if statement

		$CI =& get_instance();
		?>
			<?=$CI->load->view( 'themes/' . trim( $code ) . '/' . $excludedTheme['HEADER'], TRUE) ?>
		<?php

		return TRUE;
	} // end of TZ_custom_theme_header
} // end of if statement

if ( ! function_exists('TZ_custom_theme_footer') )
{
	/**
	 * [TZ_custom_theme_footer description]
	 * @param [type] $code [description]
	 */
	function TZ_custom_theme_footer ( $code )
	{
		# Code here
		$excludedTheme = TZ_custom_theme( $code );

		if ( ! $excludedTheme )
		{
			# Code here
			return FALSE;
		} // end of if statement

		$CI =& get_instance();
		?>
			<?=$CI->load->view( 'themes/' . trim( $code ) . '/' . $excludedTheme['FOOTER'], TRUE) ?>
		<?php

		return TRUE;
	} // end of TZ_custom_theme_footer
} // end of if statement

if (! function_exists('TZ_custom_agent_func')) {
	/**
	 * [TZ_custom_agent_func description]
	 */
	function TZ_custom_agent_func($code){
		$data=json_decode(AGENCY_CUSTOM_FUNC, TRUE);
		// $this->nativesession->get('agencyRealCode')
		return isset($data[$code])?$data[$code]:false;
	}
}

if (! function_exists('TZ_includes')) {
	function TZ_includes($code,$type){
		$excludedTheme = TZ_custom_theme( $code );
		if (!$excludedTheme||!isset($excludedTheme['LIBRARY'][$type])) {
			return FALSE;
		}
		$libs=$excludedTheme['LIBRARY'][$type];
		foreach ($libs as $key => $value) {
			switch ($type) {
				case 'styles':
					?><link rel="stylesheet" href="<?=base_url()?><?=$value?>"><?php
					break;
				case 'scripts':
					?><script type="text/javascript" src="<?=base_url()?><?=$value?>"></script><?php
					break;
				
				default:
					# code...
					break;
			}
		}
	}
}