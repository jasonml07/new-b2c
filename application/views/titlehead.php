<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php $title = "Travelrez - Designed for the Travel Agent"; ?>
    <title><?php echo $title; ?></title>

     <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:700,600,400,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oswald:400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/awe-booking-font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/jquery-ui.css">
    <!-- REVOLUTION DEMO -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/revslider-demo/css/settings.css">

    <!-- Form Validation CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/formValidation.min.css">

    <!-- MAIN STYLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/dashboard-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/demo.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/animate.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/animate.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/dataTables.bootstrap.min.css">
    <style type="text/css" class="init"></style>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/jquery.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/adjustments.css">
    <!-- CSS COLOR -->
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/blue.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/adjustments.css">


</head>