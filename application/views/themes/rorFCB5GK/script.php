(function($){
	$(document).ready(function(){
		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			event.preventDefault(); 
			event.stopPropagation(); 
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	});
})(jQuery);
(function ( $ ) {
	var $racqMenu     = $( '#RACQ-menus-collapse' );
	var $hoverContent = $( '#hover-content' );
	var $section      = $( '#page-wrap > section' );

/*	$racqMenu.find('[data-id]').hover(function () {
		var $this = $( this );

		$hoverContent.find( '[data-menu-id]' ).removeClass('active');
		$hoverContent.find( '[data-menu-id="' + $this.data('id') + '"]' ).addClass('active');
	});*/

	/*===================================
	=            Search Form            =
	===================================*/
	
	var SEARCH = {
		eleContainer: $( '#RACQ-contact-collapse .search-form' ),

		init: function ( ) {
			this.eleContainer.find('.search-input').autocomplete({
				source: function ( request, response ) {
					$.get('https://racq-search.clients.funnelback.com/s/suggest.json?collection=racq-web&show=5&sort=0&alpha=0.5&fmt=json', {partial_query: request.term}, function ( data ) {
						response( data );
					});
				},
				select: function( event, ui ) {
					SEARCH.eleContainer.attr('action', 'https://www.racq.com.au/search?query=' + ui.item.value)
				}
			});
		}
	};
	
	/*=====  End of Search Form  ======*/
	

	/*======================================
	=            Mobile Buttons            =
	======================================*/
	
	var MOBILE_BUTTONS = {
		eleContainer: $( '.contact-header .navbar-header' ),

		init: function () {
			this.eleContainer.find('button[data-function]').click(function () {
				MOBILE_BUTTONS.clear();
				var $this    = $( this );
				var type     = $this.data('function');
				var showMenu = (MENU.eleMobileMenu.data('type') != type);
				var isShown  = MENU.eleMobileMenu.data('show');

				$this.addClass('active');
				MENU.mobileMenuHide( type );
				if ( showMenu || (! showMenu && isShown) )
				{
					MENU.mobileMenuShow( type );
					setTimeout(function ( ) {
						MENU.showType( type, false );
					}, 50);
				} else {
					MENU.showType( type );
				}
			});
		},
		/**
		 * [clear description]
		 * @return {[type]} [description]
		 */
		clear: function () {
			this.eleContainer.find('button[data-function]').removeClass('active');
		},

	};
	
	/*=====  End of Mobile Buttons  ======*/
	

	/*===============================
	=            Section            =
	===============================*/
	
	var SECTION = {
		eleContainer: $section,

		init: function () {

		}
	};
	
	/*=====  End of Section  ======*/
	

	/*==============================
	=            Conver            =
	==============================*/
	
	var COVER = {
		eleContainer: $hoverContent,

		init: function () {
			this.hide();
		},
		/**
		 * [equalizeItemSize description]
		 * @return {[type]} [description]
		 */
		equalizeItemSize: function () {
			var $contents = this.eleContainer.find('[data-menu-id]');

			$contents.each(function ( key, content ) {
				var $items = $( content ).find('.item');
				var size   = $items.length;
				var ceil  = Math.ceil(size / 3);

				for ( var i = 0; i < ceil; i++ )
				{
					// console.log( $items.slice((i * 3), ((i + 1) * 3)) );
					var $sliced = $items.slice((i * 3), ((i + 1) * 3));
					var maxHeight = COVER.getMaxHeight( $sliced );
					$sliced.css('min-height', maxHeight + 'px').data('maxHeight', maxHeight);
				}

				var $itemsContainer   = $( content ).find('.items');
				var $popularContainer = $( content ).find('.popular-items');

				if ( $popularContainer.height() > $itemsContainer.height() )
				{
					$itemsContainer.css('height', $( content ).height());
				}
			});
		},
		/**
		 * [getHeighestHeight description]
		 * @param  {[type]} $items [description]
		 * @return {[type]}        [description]
		 */
		getMaxHeight: function ( $items ) {
			var height = 0;

			$items.each(function ( key, ele ) {
				var eleHeight = $( ele ).height();

				if ( eleHeight > height )
				{
					height = eleHeight;
				}
			});

			return height;
		},
		/**
		 * [clear description]
		 * @return {[type]} [description]
		 */
		clear: function () {
			this.eleContainer.find('[data-menu-id]').removeClass('active');
		},
		/**
		 * [active description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		active: function ( id ) {
			this.eleContainer.find('[data-menu-id="' + id + '"]').addClass('active');
		},
		/**
		 * [hide description]
		 * @return {[type]} [description]
		 */
		hide: function () {
			this.eleContainer.fadeOut();
		},
		/**
		 * [show description]
		 * @return {[type]} [description]
		 */
		show: function () {
			this.eleContainer.fadeIn();
		},
	};
	
	/*=====  End of Conver  ======*/
	

	/*============================
	=            Menu            =
	============================*/
	
	var $mobileMenu = $( '#mobile-menu' );
	var MENU = {
		eleContainer     : $racqMenu,
		eleMobileMenu    : $mobileMenu,
		eleMenuWrapper   : $mobileMenu.find('.menu-wrapper'),
		eleContactWrapper: $mobileMenu.find('.contact-wrapper'),

		init: function () {
			this.mobileMenuHide();
			this.eleMobileMenu.click(function ( e ) {
				if( e.target == this )
				{
					MENU.showType( MENU.eleMobileMenu.data('type') );
					MENU.mobileMenuHide( MENU.eleMobileMenu.data('type') );
				}
			});
			this.eleContainer.find('[data-id]').hover(function () {
				var $this = $( this );

				MENU.clear();
				MENU.active( $this.data('id') );
				COVER.clear();
				COVER.active( $this.data('id') );
				COVER.show();
				COVER.equalizeItemSize();
			});
			this.eleContainer.find('li.discount, li.account').hover(function () {
				var $this = $( this );

				MENU.clear();
				COVER.hide();
			});
		},
		/**
		 * [showType description]
		 * @param  {[type]} type [description]
		 * @return {[type]}      [description]
		 */
		showType: function ( type, isHide ) {
			var isHide = isHide || true;
			switch ( type )
			{
				case 'menu':
					this.eleMenuWrapper.stop();
					if ( isHide )
					{
						this.eleMenuWrapper.animate({'left': '-300px'}, 250);
					} else {
						MENU.eleContactWrapper.data('show', false);
						this.eleContactWrapper.hide("scale", {}, 150, function () {
							MENU.eleMenuWrapper.animate({'left': 0}, 250);
							MENU.eleMenuWrapper.data('show', true);
						});
					}
					break;

				case 'contact':
					this.eleContactWrapper.stop();
					if ( isHide )
					{
						this.eleContactWrapper.hide("scale", {}, 150);
					} else {
						MENU.eleMenuWrapper.data('show', false);
						this.eleMenuWrapper.animate({'left': '-300px'}, 250, function () {
							MENU.eleContactWrapper.show("scale", {}, 150, function () { });
							MENU.eleContactWrapper.data('show', true);
						});
					}
					break;
			}
		},
		/**
		 * [mobileMenuShow description]
		 * @return {[type]} [description]
		 */
		mobileMenuShow: function ( type ) {
			this.eleMobileMenu.stop();
			this.eleMobileMenu.data('show', false);
			this.eleMobileMenu.data('type', type);
			this.eleMobileMenu.fadeIn();
		},
		/**
		 * [mobileMenuShow description]
		 * @return {[type]} [description]
		 */
		mobileMenuHide: function ( type ) {
			this.eleMobileMenu.stop();
			this.eleMobileMenu.data('show', true);
			this.eleMobileMenu.data('type', type);
			this.eleMobileMenu.fadeOut('slow', function () {
				$( this ).removeAttr('style');
				$( this ).attr('style', 'display: none !important;');
			});
		},

		/**
		 * [clear description]
		 * @return {[type]} [description]
		 */
		clear: function () {
			this.eleContainer.find('[data-id]').removeClass('active');
		},
		/**
		 * [active description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		active: function ( id ) {
			this.eleContainer.find('[data-id="' + id + '"]').addClass('active');
		},
	};
	
	/*=====  End of Menu  ======*/

	function init () {
		COVER.init();
		MENU.init();
		MOBILE_BUTTONS.init();
		SEARCH.init();

		// console.log( 'COVER.eleContainer.offset()', COVER.eleContainer.find('> .container').offset() );
		// console.log( 'MENU.eleContainer.offset()', MENU.eleContainer.offset() );

		$( window ).mousemove(function ( event ) {
			var $element = COVER.eleContainer.find('> .container');
			
			var width  = $element.width()
			var height = $element.height()
			var offset = $element.offset();

			if ( (event.pageX < ( offset.left + 15)) || (event.pageX > ( offset.left + width )) || ( event.pageY > (height + offset.top) ) || ( event.pageY < (offset.top - MENU.eleContainer.height()) ) )
			{
				COVER.hide();
				MENU.clear();
			}
		});
	}
	init();

})(jQuery);