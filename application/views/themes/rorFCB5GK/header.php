<?php $racq = 'https://www.racq.com.au'; ?>
<!--================================
=            Top Header            =
=================================-->
<nav class="contact-header navbar navbar-default">
    <div class="container-fluid">
        <div class="container">
            <div class="navbar-header">
                <button type="button" data-function="menu" data-toggle="collapse" aria-expanded="false" class="pull-left navbar-toggle collapsed">
                    <span class="sr-only">Toggle Main Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" data-function="search" data-toggle="collapse" aria-expanded="false" class="navbar-toggle collapsed">
                    <span class="sr-only">Toggle Search Menu</span>
                    <span class="fa fa-search"></span>
                </button>
                <button type="button" data-function="contact" data-toggle="collapse" aria-expanded="false" class="navbar-toggle collapsed">
                    <span class="sr-only">Toggle Contact Menu</span>
                    <span class="fa fa-phone"></span>
                </button>
                <a class="navbar-brand logo-brand" href="#"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASgAAABOCAMAAACkA3vYAAABXFBMVEUAAAAAQX4AP38AP38AP38AQn0AQn0AQ38AQX0AQX0AQnwAQn0AQn8AQn0AQX0AQX7/1QP/yAn/1AX/1AP/1AP7rRb/0gP8nx/3lh3+ww7/xgv/0wP9uBL/0wP/ywn/ygj/0QP/ww3/0gP/0wP4mxv5nhv/0AT/zgb3mR36pRn8rxb7rxP/0wP5mxz/1QP4nRv7rhb/wQ3/xwv8sxT7sBb6qRn/1gI9V2WZdUH3mR3/zAf/wg7/vg79vBH4nxv4mBz7sBX6qhgzXmVfeU8AQn75nhwQSnb4mB2/sCL/1QP/0AWAjEFgdlH/0gT/0wT+uRL8sBf/ygr/xwteaFifmzPPqSNAZWD/1AP6pRr7qhlQcFjPuBv/zAcwXGivpyn/wQ7/zgf/xA3+vBH9sxX+vhD/zwb9tRT/xQz/vw+PkzkgVG/vxw2/qiXvvRM+WWbKji6NeUa6gzVvdU+fjjpwgefaAAAARHRSTlMAnyAwEO/fQIC/YM9QcK+PgBAw758wYBCAnzAg779g78/PQI/v36+fQM9QIN+/UGDv30Bwn79wgIDPz4+vz3Cf36+fgNo/4zMAAAfJSURBVHhe7Zxlc+RIEoarpEKh2zhre3hndweW6RjL9jAuM9Mh/P+Ik8M7Y7eVqkq96piIu/DzuUWvspIq1eL/hxMy+zNiFMrGj8PJbRSFnNJR8CWqnDfhCF7WuUhjXVOG1HE4PqQwvnFVNvmUlidyawKFlpWIoIomkJh2UVrpQINf0QYSx7AlZ8Iwus0ETdWECKZQYgEEPt5OsdEmKZMMKSQhlapNSKBrMRkbxtAokaQKNAaQqU+rxDyFDgyMFRMpwih0JVKYMEDUzTkN3YE1gUkrpuHCSBysfETjvAwEyUdWbeBTKjGFJoxFiigGkLgOBOlHzk0Yg86nZQejkeBS9oJGjX5ZHvAaU5UKAJEYomKuRpBkyWVHL2IZwlNUSgWEHHJ59GG5DoibVDIAGIVnBwglZFChRnSiEzJVBggvQOoAUUAxVCI60a68CSBucdmBf4yJmLAgyfRIQ8w15mdkgAHdlI+7XFs37Msx7l+JOaAFZJHkb/riK5MnUo5twVlIYPlJnPbSdUhPLPoqKoR3rvGa6zbw7KBhrQ+PGFRwTAdpjvaRbKsP5SuiEaM8PC53Bqs5afK0rdBvUDMMKvUScnaToih/Pj6j7ZBuLdSaUUrh2UHFWqABSvI14/fGDnZ4Vaw74bP+QT7tpfCgZ3k/w1KyLFnsSCXiKDOiWpDxu+BDVN9MPbGqsUpVzzXwbmO1iYQuwnk4gwplRzaFCiwiKR3XiZOAlItomHue4RmGH9PRsxssbXZpndIRQy0gO2h52VbDaK+4JmKIBehlzdgwJqMH4EGv5v3MpR/AREOFJwwDbMi3IkaWtlzospYX923aoIrYe8jAlLkZr2+TNF1owWesDoPmGJRQww2EGsuYFaBvAXhzhtg89ykJxYn7LwfV8JhBVYC+lPEC2UHKKK1nVAFqIL7JoYCjUkbK98w14v8XXRJndRkoDDPBL4acWwXuuxkk1Pu4g8FWvHdPaId7FUXaoOI1t6M7XMAqaqDywwLZAUDJvpOhgOOxlWchz+YmC1UvppuakXJEBTG0ZeBRGhcKuC66rSfJ248uMTAHlJAhThfKBwSZdhwyEs4LRhsMj9JASc+gxHTiGhQtYksLZbGgJ8ENlHEsZF/MRrU0ZCQogCgPr9jpQuVhNMYybkOraO4PLgZcKLTWw7MD7RSrs5BwpAsVCk+++DhQJr5B0Y2cpyxUMbnN0gY+uqm4VapLVUlPWSgJhFg8O/CZIDGJyRq6geAW78yVO8rbvzjV8cIGdYt0lrq8vtTjJXGABmawGP26pNlmkRQQd8t2/nF2H/1rb2/vly9viEInOzMXVldu9FmdDV/WOTlUCuuMV837eQxl+DW4GkKPMrLh8vd/7OzsPPs7ocp48rV1+gbFerJhnreaOT1WBARHhkq0yxLzRv/8oOPyVSFjL+UtUqaVZU7DXDnWvqHSAcFTQnnQq1ZRv/3ow45X1oQc9oerCZ0SHjUvGV18FzDImkBhcVqKuFL3O44p1czp9D7FyjK7Ya4IpSRhUAg5tYYKrC+kVUKprzteWTv6OBVDJ3YpTiuVEQYFUFC5TQkWXUViv+Tf9zq2Z0pTMe/iXZI5nQQwvuSJgU0AKQpw+1aTE9Hx9/ufrzp+O8t1P2ys0zqtpxrmblTDSwaUUmTQDDh9zTY1cPvuDx2/EUVvrS7fIbk4doZMmZhJZQFHka0wCX4LViTW5+67n3f8WtTHrGFrhdTpuZ5jRPbJbc+gECzdrpeRD6qzSDVRJNT89G/fd5wRcs6gZuduUpybAQ1Ssu023aCCE4o9bpe5A+szkQ9g6kQW8d7DfTZFeVTU50idVrYY/ZExpZwPE2iGnthUYh4r5y+dpeZj6dB3+0HH2U1lDn3H819QnF0GJsxjE3k2TEEPW6Spn+QgqpKmt+7l4OR0dnhYf4G+80nHr2b5k5Of+YTkDDhDRteDtD3aIejdBxmfxfdeE3ExvuT9PjqQ/HVfhtPiMa89IFkCJsyjDUx+VUsXlRWW17eEA2Ky+/G3Hc+LAzbPPqR4htcwt4KtZ0nqx29at+A+dY4XTj/95WbHQS45O/8lxfkZr2GeMzdnVbQNzR+p9djUQxUwPr25zwXRcf5zildnzEKcbQ5UAxiYV0Nsw+GLL9y+07Ff7z7zDcX2JrO1U7KrBhfN+fjWm0OLD09NfrzTcW12dfsexRV8wpzfTDHY8HGB7eirMmC8c7fjnFjb/q7P65N3fhyrcRJlsFopEJ1wpXY/vtuxKtb+cP84bwAT5qRJ4QYVHXotsIkjXKn3O1bFxh8/nOdNYMIcMCk7voWrgK+KTU78qQSi1OmZuHLpgyNc3hAkI7cgDfCZLrsNkfnAxKkF9Fd3P7vRce2CEC9f2nnMixtjOmBJZ4z/P4XSMY/osL3qHFx+tw52N7eEWDv14rN7e5d+/8LCZtcc35PTFPTl+EbVWOivlLRTsp95Xj+Q6iV0whxTylhockaTY/80ps0EiYpLVRaKdpDvdetvn9MXl5b+/CdBwGiY0+RSD3Q3lGBhpY4OSuStDiRlG1vZqi6TfRdrCFf10a3r169/duuj3VZFhbI9MpEgs30EAfMEfYFz5828Ro2rVPq0xfFPB3zjrOIZnrfif5TcTvyjUsU3PC37Mp2QFdKHQ4x3EZVOiPkZnBP+C748DI6djFzXAAAAAElFTkSuQmCC" alt="RACQ"></a>
                <a class="navbar-brand contact-info visible-md visible-lg" href="#">
                    <em><strong>13 1905</strong></em>
                    <br>
                    <small>24 hours every day</small>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="RACQ-contact-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="<?=$racq . '/about' ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="glyphicon glyphicon-chevron-down"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=$racq . '/about/racq' ?>">About RACQ                      <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/about/news-and-community' ?>">News and Community<span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/about/blog' ?>">Blog                            <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/about/careers' ?>">RACQ Careers                 <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/about/faqs' ?>">FAQs                            <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/about/club-news' ?>">Club News                  <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?=$racq . '/links/business/business' ?>">Business</a>
                    </li>
                    <li class="dropdown">
                        <a href="<?=$racq . '/our-sites' ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Sites <span class="glyphicon glyphicon-chevron-down"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=$racq . '/our-sites/racq-living' ?>">RACQ Living<span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/our-sites/free2go' ?>">free2go        <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/our-sites/learn2go' ?>">Learn2go      <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?=$racq . '/support' ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="glyphicon glyphicon-chevron-down"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=$racq . '/support/website-help' ?>">Website help                <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/support/your-account' ?>">Your Account                <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/support/forgot-your-password' ?>">Forgot your password<span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?=$racq . '/contact-us' ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact <span class="glyphicon glyphicon-chevron-down"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=$racq . '/contact-us/contact-us-online' ?>">Contact us online<span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                            <li><a href="<?=$racq . '/contact-us/find-a-branch' ?>">Find a branch        <span class="glyphicon glyphicon-chevron-right pull-right"></span></a></li>
                        </ul>
                    </li>
                </ul>

                <form action="" class="navbar-form navbar-right search-form" method="POST">
                    <input type="hidden" name="ucHeaderControl$SearchControl1$SearchBtn" value="Button">
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <input type="text" class="form-control search-input" name="ucHeaderControl$SearchControl1$txtSearch" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</nav>
<?php $items = array(
    'membership' => array(
        'title' => 'Membership',
        'link' => '/membership',
        'popular-items' => array(
            array('title' => 'Movie Tickets',      'link' => '/externallinkmanager/movie-tickets'),
            array('title' => 'Theme Park Tickets', 'link' => '/externallinkmanager/theme-park-tickets'),
            array('title' => 'Competitions',       'link' => '/links/membership/popular-items/competitions'),
            array('title' => 'Member Discounts',   'link' => '/links/membership/popular-items/member-discounts'),
            array('title' => 'Dining Rewards',     'link' => '/links/membership/popular-items/dining-rewards'),
            array('title' => 'Joining RACQ',       'link' => '/links/membership/popular-items/joining-racq'),
            array('title' => 'Lifestyle',          'link' => '/membership/lifestyle'),
        ),
        'items' => array(
            array(
                'header' => array('title' => '<span class="fa cms-icon-shopping left-icon"></span> <span class="text">Lifestyle</span>', 'link' => '/membership/lifestyle'),
                'items' => array(
                    array('title' => 'RACQ Discounts App',     'link' => '/membership/lifestyle/discounts-app'),
                    array('title' => 'How it works',           'link' => '/membership/lifestyle/how-it-works'),
                    array('title' => 'Terms &amp; Conditions', 'link' => '/membership/lifestyle/terms-and-conditions'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-what-you-get left-icon"></span> <span class="text">Member benefits</span>', 'link' => '/membership/what-you-get'),
                'items' => array(
                    array('title' => 'Aussie Assist',         'link' => '/membership/what-you-get/aussie-assist'),
                    array('title' => 'Car brokerage service', 'link' => '/membership/what-you-get/car-brokerage-service'),
                    array('title' => 'Competitions',          'link' => '/membership/what-you-get/competitions'),
                    array('title' => 'Dining rewards',        'link' => '/membership/what-you-get/dining-rewards'),
                    array('title' => 'More...',               'link' => '/membership/what-you-get'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-credit-card left-icon"></span> <span class="text">Manage your membership</span>', 'link' => '/your-account/account-overview'),
                'items' => array(
                    array('title' => 'Update personal details',   'link' => '/your-account/personal-details'),
                    array('title' => 'View your member benefits', 'link' => '/your-account/membership-benefits'),
                    array('title' => 'Report fraud',              'link' => '/membership/manage-your-membership/report-fraud'),
                    array('title' => 'Make a payment',            'link' => '/membership/manage-your-membership/make-a-payment'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-person left-icon"></span> <span class="text">Joining RACQ</span>', 'link' => '/membership/joining-racq'),
                'items' => array(
                    array('title' => 'Membership savings calculator', 'link' => '/membership/joining-racq/membership-savings-calculator'),
                    array('title' => 'Why RACQ membership?',          'link' => '/membership/joining-racq/why-racq-membership'),
                    array('title' => 'Membership FAQs',               'link' => '/membership/joining-racq/membership-faqs'),
                    array('title' => 'Ways to join',                  'link' => '/membership/joining-racq'),
                    array('title' => 'More...',                       'link' => '/membership/joining-racq'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-calculator left-icon"></span> <span class="text">Business membership benefits</span>', 'link' => '/business/member-benefits-for-business'),
                'items' => array(
                    array('title' => '<p>We support our business members by providing benefits to save you money.</p>')
                )
            ),
        )
    ),
    'roadside-assistance' => array(
        'title' => 'Roadside Assistance',
        'link'  => '/roadside-assistance',
        'popular-items' => array(
            array('title' => 'Roadside Assistance Options',   'link' => '/links/roadside-assistance/popular-items/roadside-assistance-options'),
            array('title' => 'Compare Roadside Options',      'link' => '/links/roadside-assistance/popular-items/compare-roadside-options'),
            array('title' => 'Make A Payment',                'link' => '/links/roadside-assistance/popular-items/make-a-payment'),
            array('title' => 'Member Benefits',               'link' => '/links/roadside-assistance/popular-items/member-benefits'),
            array('title' => 'Roadside Assistance Explained', 'link' => '/links/roadside-assistance/popular-items/roadside-assistance-explained')
        ),
        'items' => array(
            array(
                'header' => array('title' => '<span class="fa cms-icon-roadside left-icon"></span> <span class="text">Get roadside assistance</span>', 'link' => '/roadside-assistance/compare-options'),
                'items' => array(
                    array('title' => 'Request roadside assistance', 'link' => '/externallinkmanager/contact-us/roadside-assistance-accordion'),
                    array('title' => 'Gift roadside assistance',    'link' => '/roadside-assistance/get-roadside-assistance/gift-roadside-assistance'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-info left-icon"></span> <span class="text">How it works</span>', 'link' => '/roadside-assistance/how-it-works'),
                'items' => array(
                    array('title' => 'Roadside assistance explained', 'link' => '/roadside-assistance/how-it-works/roadside-assistance-explained'),
                    array('title' => 'Terms &amp; conditions',        'link' => '/roadside-assistance/how-it-works/terms-and-conditions')
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-credit-card left-icon"></span> <span class="text">Manage your roadside assistance</span>', 'link' => '/your-account/roadside-assistance'),
                'items' => array(
                    array('title' => 'Make a payment',              'link' => '/roadside-assistance/manage-your-roadside-assistance/make-a-payment'),
                    array('title' => 'Update personal details',     'link' => '/your-account/personal-details'),
                    array('title' => 'Update car details',          'link' => '/your-account/personal-details'),
                    array('title' => 'Upgrade roadside assistance', 'link' => '/your-account/personal-details')
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-safety left-icon"></span> <span class="text">Breakdown safety</span>', 'link' => '/roadside-assistance/breakdown-safety'),
                'items' => array(
                    array('title' => '<p>We want you to stay safe. Help yourself and other road users by keeping up to date with RACQ\'s Breakdown Safety Guide.</p>')
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-chart left-icon"></span> <span class="text">Roadside assistance options</span>', 'link' => '/roadside-assistance/roadside-assistance-options'),
                'items' => array(
                    array('title' => 'RACQ Ultimate', 'link' => '/roadside-assistance/roadside-assistance-options/racq-ultimate'),
                    array('title' => 'Ultra Care',    'link' => '/roadside-assistance/roadside-assistance-options/ultra-care'),
                    array('title' => 'Plus Care',     'link' => '/roadside-assistance/roadside-assistance-options/plus-care'),
                    array('title' => 'Roadside',      'link' => '/roadside-assistance/roadside-assistance-options/roadside'),
                    array('title' => 'More...',       'link' => '/roadside-assistance/roadside-assistance-options'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-compare left-icon"></span> <span class="text">Compare options</span>', 'link' => '/roadside-assistance/compare-options'),
                'items' => array(
                    array('title' => 'All options',    'link' => '/roadside-assistance/compare-options#tab-all-options'),
                    array('title' => 'Help me choose', 'link' => '/roadside-assistance/compare-options#tab-help-me-choose'),
                )
            ),
        )
    ),
    'cars-driving' => array(
        'title' => 'Cars &amp; Driving',
        'link'  => '/cars-and-driving',
        'popular-items' => array(
            array('title' => 'Road Conditions',        'link' => '/links/cars-and-driving/popular-items/road-conditions'),
            array('title' => 'Car Buying and Reviews', 'link' => '/links/cars-and-driving/popular-items/car-buying-and-reviews'),
            array('title' => 'Fair fuel prices',       'link' => '/links/cars-and-driving/popular-items/fair-fuel-prices'),
            array('title' => 'Battery Finder',         'link' => '/links/cars-and-driving/popular-items/battery-finder'),
            array('title' => 'Vehicle inspections',    'link' => '/links/cars-and-driving/popular-items/vehicle-inspections'),
            array('title' => 'Driving courses',        'link' => '/links/cars-and-driving/popular-items/driving-courses'),
            array('title' => 'Approved Repairers',     'link' => '/links/cars-and-driving/popular-items/approved-repairers'),
        ),
        'items' => array(
            array(
                'header' => array('title' => '<span class="fa cms-icon-battery left-icon"></span> <span class="text">Products & services</span>', 'link' => '/cars-and-driving/products-and-services'),
                'items' => array(
                    array('title' => 'Batteries',                        'link' => '/cars-and-driving/products-and-services/batteries'),
                    array('title' => 'Windscreens',                      'link' => '/cars-and-driving/products-and-services/windscreens'),
                    array('title' => 'Window tinting',                   'link' => '/cars-and-driving/products-and-services/window-tinting'),
                    array('title' => 'Starter motors &amp; alternators', 'link' => '/cars-and-driving/products-and-services/starter-motors-and-alternators'),
                    array('title' => 'More...',                          'link' => '/cars-and-driving/products-and-services'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-car left-icon"></span> <span class="text">Cars</span>', 'link' => '/cars-and-driving/cars'),
                'items' => array(
                    array('title' => 'Car reviews',                    'link' => '/cars-and-driving/cars/car-reviews'),
                    array('title' => 'Buying a car',                   'link' => '/cars-and-driving/cars/buying-a-car'),
                    array('title' => 'Owning &amp; maintaining a car', 'link' => '/cars-and-driving/cars/owning-and-maintaining-a-car'),
                    array('title' => 'Selling a car',                  'link' => '/cars-and-driving/cars/selling-a-car'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-steering-wheel left-icon"></span> <span class="text">Driving</span>', 'link' => '/cars-and-driving/driving'),
                'items' => array(
                    array('title' => 'Fair Fuel Prices',  'link' => '/cars-and-driving/driving/fair-fuel-prices'),
                    array('title' => 'Learning to drive', 'link' => '/cars-and-driving/driving/learning-to-drive'),
                    array('title' => 'Towing',            'link' => '/cars-and-driving/driving/towing'),
                    array('title' => 'Caravans',          'link' => '/cars-and-driving/driving/caravans'),
                    array('title' => 'More...',           'link' => '/cars-and-driving/driving'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-safety left-icon"></span> <span class="text">Safety on the road</span>', 'link' => '/cars-and-driving/safety-on-the-road'),
                'items' => array(
                    array('title' => 'Mount Cotton Driver Training Centre', 'link' => '/cars-and-driving/safety-on-the-road/mount-cotton-driver-training-centre'),
                    array('title' => 'Road Conditions',                     'link' => 'https://qldtraffic.qld.gov.au/', 'custom' => TRUE),
                    array('title' => 'Car safety features',                 'link' => '/cars-and-driving/safety-on-the-road/car-safety-features'),
                    array('title' => 'Driving safely',                      'link' => '/cars-and-driving/safety-on-the-road/driving-safely'),
                    array('title' => 'More...',                             'link' => '/cars-and-driving/safety-on-the-road'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-road left-icon"></span> <span class="text">Representing Queensland drivers</span>', 'link' => '/cars-and-driving/representing-queensland-drivers'),
                'items' => array(
                    array('title' => 'Ways we advocate',             'link' => '/cars-and-driving/representing-queensland-drivers/ways-we-advocate'),
                    array('title' => 'Road safety priorities',       'link' => '/cars-and-driving/representing-queensland-drivers/road-safety-priorities'),
                    array('title' => 'Road surveys and assessments', 'link' => '/cars-and-driving/representing-queensland-drivers/road-surveys-and-assessments'),
                    array('title' => 'Queensland roads',             'link' => '/cars-and-driving/representing-queensland-drivers/queensland-roads'),
                )
            ),
        )
    ),
    'insurance' => array(
        'title' => 'Insurance',
        'link'  => '/insurance',
        'popular-items' => array(
            array('title' => 'Get An Insurance Quote',      'link' => '/links/insurance/popular-items/get-an-insurance-quote'),
            array('title' => 'Make An Insurance Claim',     'link' => '/links/insurance/popular-items/make-an-insurance-claim'),
            array('title' => 'Car Insurance',               'link' => '/links/insurance/popular-items/car-insurance'),
            array('title' => 'Home And Contents Insurance', 'link' => '/links/insurance/popular-items/home-and-contents-insurance'),
            array('title' => 'Pet Insurance',               'link' => '/links/insurance/popular-items/pet-insurance'),
            array('title' => 'Travel Insurance',            'link' => '/links/insurance/popular-items/travel-insurance'),
            array('title' => 'Life and Income Insurance',   'link' => '/links/insurance/popular-items/life-and-income-insurance'),
        ),
        'items' => array(
            array(
                'header' => array('title' => '<span class="fa cms-icon-quote left-icon"></span> <span class="text">Get insurance quote</span>', 'link' => '/insurance/get-a-quote'),
                'items' => array(
                    array('title' => 'Retrieve a quote', 'link' => '/insurance/quote/retrieve'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-umbrella left-icon"></span> <span class="text">Get insurance</span>', 'link' => '/insurance/get-insurance'),
                'items' => array(
                    array('title' => 'Car insurance',               'link' => '/insurance/get-insurance/car-insurance'),
                    array('title' => 'Home and Contents Insurance', 'link' => '/insurance/get-insurance/home-and-contents-insurance'),
                    array('title' => 'Life and Income Protection',  'link' => '/insurance/get-insurance/life-and-income-protection'),
                    array('title' => 'Pet Insurance',               'link' => '/insurance/get-insurance/pet-insurance'),
                    array('title' => 'More...',                     'link' => '/insurance/get-insurance'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-credit-card left-icon"></span> <span class="text">Manage your Insurance</span>', 'link' => '/your-account/insurance'),
                'items' => array(
                    array('title' => 'Make a payment',                     'link' => '/insurance/manage-your-insurance/make-a-payment'),
                    array('title' => 'Update personal details',            'link' => '/your-account/personal-details'),
                    array('title' => 'Download my Insurance documents',    'link' => '/your-account/insurance'),
                    array('title' => 'New to Your Account? Register here', 'link' => '/register'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-form left-icon"></span> <span class="text">Claims</span>', 'link' => '/insurance/claims'),
                'items' => array(
                    array('title' => 'How to make a claim', 'link' => '/insurance/claims/how-to-make-a-claim'),
                    array('title' => 'Claim notification',  'link' => '/insurance/claims/claim-notification-landing'),
                    array('title' => 'Motor assessing',     'link' => '/insurance/claims/motor-assessing'),
                    array('title' => 'Selected repairers',  'link' => '/insurance/claims/selected-repairers'),
                    array('title' => 'More...',             'link' => '/insurance/claims'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-envelope left-icon"></span> <span class="text">Learn more about insurance</span>', 'link' => '/insurance/learn-more-about-insurance'),
                'items' => array(
                    array('title' => 'Insurance discounts',                     'link' => '/insurance/learn-more-about-insurance/insurance-discounts'),
                    array('title' => 'Be prepared',                             'link' => '/insurance/learn-more-about-insurance/be-prepared'),
                    array('title' => 'Find emergency contacts &amp; resources', 'link' => '/insurance/learn-more-about-insurance/find-emergency-contacts-and-resources'),
                    array('title' => 'Product Disclosure Statements',           'link' => '/insurance/learn-more-about-insurance/product-disclosure-statements'),
                    array('title' => 'More...',                                 'link' => '/insurance/learn-more-about-insurance'),
                )
            ),
        )
    ),
    'travel' => array(
        'title' => 'Travel',
        'link'  => '/travel',
        'popular-items' => array(
            array('title' => 'Trip Planner',                 'link' => '/links/travel/popular-items/trip-planner'),
            array('title' => 'Holidays',                     'link' => '/links/travel/popular-items/holidays'),
            array('title' => 'Accommodation',                'link' => '/links/travel/popular-items/accommodation'),
            array('title' => 'Discount Airport Parking',     'link' => '/links/travel/popular-items/discount-airport-parking'),
            array('title' => 'International Drivers Permit', 'link' => '/links/travel/popular-items/international-drivers-permit'),
            array('title' => '150 Must Dos in Queensland',   'link' => '/links/travel/popular-items/150-must-dos-in-queensland'),
        ),
        'items' => array(
            array(
                'header' => array('title' => '<span class="fa cms-icon-suitcase left-icon"></span> <span class="text">Holidays</span>', 'link' => '/travel/holidays'),
                'items' => array(
                    array('title' => 'Cruises',             'link' => '/travel/holidays#tab-cruises'),
                    array('title' => 'Overseas holidays',   'link' => '/travel/holidays#tab-overseasholidays'),
                    array('title' => 'Australian holidays', 'link' => '/travel/holidays#tab-australianholidays'),
                    array('title' => 'Browse holidays',     'link' => '/travel/holidays/browse-holidays'),
                    array('title' => 'More...',             'link' => '/travel/holidays'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-travel-book left-icon"></span> <span class="text">Holiday preparation</span>', 'link' => '/travel/holiday-preparation'),
                'items' => array(
                    array('title' => 'Travel checklist',         'link' => '/travel/holiday-preparation/travel-checklist'),
                    array('title' => 'Airport & cruise parking', 'link' => '/travel/holiday-preparation/airport-and-cruise-parking'),
                    array('title' => 'Travelshoot',              'link' => '/travel/holiday-preparation/travelshoot'),
                    array('title' => 'Medical Traveller',        'link' => '/travel/holiday-preparation/medical-travel-companions'),
                    array('title' => 'More...',                  'link' => '/travel/holiday-preparation'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-trip-planner left-icon"></span> <span class="text">Trip planner</span>', 'link' => '/travel/trip-planner'),
                'items' => array(
                    array('title' => 'Find a place, get directions or plan your next road trip with RACQ\'s Trip Planner. '),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-map left-icon"></span> <span class="text">Destinations</span>', 'link' => '/travel/destinations'),
                'items' => array(
                    array('title' => 'Europe',      'link' => '/travel/destinations/europe'),
                    array('title' => 'USA',         'link' => '/travel/destinations/usa'),
                    array('title' => 'Canada',      'link' => '/travel/destinations/canada'),
                    array('title' => 'New Zealand', 'link' => '/travel/destinations/new-zealand'),
                    array('title' => 'More...',     'link' => '/travel/destinations'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-road left-icon"></span> <span class="text">Road Trips</span>', 'link' => '/travel/road-trips/road-trips'),
                'items' => array(
                    array('title' => 'Road trips',                   'link' => '/travel/road-trips/road-trips'),
                    array('title' => 'Road trip checklist',          'link' => '/travel/road-trips/road-trip-checklist'),
                    array('title' => 'Longer tourist drives',        'link' => '/links/travel/tourist-information-centre/tourist-drives'),
                    array('title' => '150 Must Do\'s in Queensland', 'link' => '/travel/road-trips/150-must-dos-in-queensland'),
                    array('title' => 'More...',                      'link' => '/travel/road-trips/road-trips'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-road left-icon"></span> <span class="text">Travel Events</span>', 'link' => '/travel/travel-events'),
                'items' => array(
                    array('title' => 'New Zealand Comes to Town', 'link' => '/travel/travel-events/new-zealand-comes-to-town'),
                )
            ),
        )
    ),
    'loans' => array(
        'title' => 'Loans',
        'link' => '#',
        'popular-items' => array(
            array('title' => 'Loan Rates and Calculator', 'link' => '/links/loans/popular-items/loan-rates-and-calculator'),
            array('title' => 'Car Loans',                 'link' => '/links/loans/popular-items/car-loans'),
            array('title' => 'Personal Loans',            'link' => '/links/loans/popular-items/personal-loans'),
            array('title' => 'Apply for a Loan',          'link' => '/links/loans/popular-items/apply-for-a-loan'),
        ),
        'items' => array(
            array(
                'header' => array('title' => '<span class="fa cms-icon-dollar left-icon"></span> <span class="text">Apply now</span>', 'link' => '/loans/apply-now'),
                'items' => array(
                    array('title' => 'Apply for your RACQ loan online, over the phone or have one of our consultants call you back at a time that best suits you.'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-calculator left-icon"></span> <span class="text">Loan rates &amp; calculator</span>', 'link' => '/loans/loan-rates-and-calculator'),
                'items' => array(
                    array('title' => 'We can help you work out your monthly or fortnightly RACQ loan repayments, for the amount you would like to borrow.'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-people left-icon"></span> <span class="text">Loan Faqs</span>', 'link' => '/about/faqs?cat=Loans'),
                'items' => array(
                    array('title' => 'We’ve got everything you need to know about RACQ loans - from our eligibility criteria, right down to our flexible repayment options.  '),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-car-loan left-icon"></span> <span class="text">Car loans</span>', 'link' => '/loans/car-loans'),
                'items' => array(
                    array('title' => 'Compare RACQ\'s car loans. With rates from 6.99%*p.a Comparison Rate 7.52%^p.a.'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-personal-loan left-icon"></span> <span class="text">Personal loans</span>', 'link' => '/loans/personal-loans'),
                'items' => array(
                    array('title' => 'With low rates and no ongoing account keeping fees.'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-business-loan left-icon"></span> <span class="text">Business car loans</span>', 'link' => '/loans/business-car-loans'),
                'items' => array(
                    array('title' => 'Our business car loans are simple and convenient, with a quick approval process.'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-tick left-icon"></span> <span class="text">Satisfaction Guarantee</span>', 'link' => '/loans/satisfaction-guarantee'),
                'items' => array(
                    array('title' => 'We are so confident you are getting a great deal on your car and personal loan we are offering a 21 day ‘Loan Satisfaction Guarantee’. '),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-tick left-icon"></span> <span class="text">Cost of living</span>', 'link' => '/loans/cost-of-living'),
                'items' => array(
                    array('title' => 'Cost of health',    'link' => '/-/media/racq/pdf/loans/racq%20-%20cost%20of%20living%20-%20health.ashx'),
                    array('title' => 'Cost of holidays',  'link' => '/-/media/racq/pdf/loans/racq%20-%20cost%20of%20living%20-%20holidays.ashx'),
                    array('title' => 'Cost of housing',   'link' => '/-/media/racq/pdf/loans/racq%20-%20cost%20of%20living.ashx'),
                    array('title' => 'Cost of education', 'link' => '/-/media/racq/pdf/loans/racq%20-%20cost%20of%20living%20-%20education.ashx'),
                    array('title' => 'More...',           'link' => '/loans/cost-of-living'),
                )
            ),
        )
    ),
    'home-assistance' => array(
        'title' => 'Home Assistance',
        'link' => '#',
        'popular-items' => array(
            array('title' => 'Home Rescue',    'link' => '/links/home-assistance/popular-items/home-rescue'),
            array('title' => 'Home Quick Fix', 'link' => '/links/home-assistance/popular-items/home-quick-fix'),
            array('title' => 'Testimonials',   'link' => '/home-assistance/how-it-works/home-assistance-testimonials'),
        ),
        'items' => array(
            array(
                'header' => array('title' => '<span class="fa cms-icon-home-rescue-small left-icon"></span> <span class="text">Home Rescue</span>', 'link' => '/home-assistance/home-rescue'),
                'items' => array(
                    array('title' => 'Get Home Rescue',    'link' => '/links/home-assistance/subscribe-to-home-assistance'),
                    array('title' => 'Excess service fee', 'link' => '/home-assistance/home-rescue/excess-service-fee'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-home-quickfix-small left-icon"></span> <span class="text">Home Quick Fix</span>', 'link' => '/home-assistance/home-quick-fix'),
                'items' => array(
                    array('title' => 'Pricing', 'link' => '/home-assistance/home-quick-fix/service-fees'),
                )
            ),
            array(
                'header' => array('title' => '<span class="fa cms-icon-info left-icon"></span> <span class="text">How it works</span>', 'link' => '/home-assistance/how-it-works'),
                'items' => array(
                    array('title' => 'Testimonials', 'link' => '/home-assistance/how-it-works/home-assistance-testimonials'),
                    array('title' => 'FAQs', 'link' => '/externallinkmanager/contact-us-online/home-assistance/home-assistance-faqs'),
                )
            ),
        )
    ),
); ?>
<nav class="menus-header navbar navbar-default visible-md visible-lg">
    <div class="container-fluid">
        <div class="container">

            <div class="collapse navbar-collapse" id="RACQ-menus-collapse">
                <ul class="nav navbar-nav">
                    <?php foreach ($items as $key => $value): ?>
                        <li><a href="<?=( @$value['custom'] ? $value['link']: $racq . $value['link'] ) ?>" data-id="<?=$key ?>"><?=$value['title'] ?></a></li>
                    <?php endforeach ?>
                    <li class="discount"><a href="#"><span class="fa fa-tag"></span> <span>Discounts</span></a></li>
                    <li class="account"><a href="#"><span class="fa fa-lock"></span> <span>Your account</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div id="mobile-menu" class="visible-sm visible-xs">
    <div class="menu-wrapper">
        <ul class="nav navbar">
            <?php foreach ( $items as $item ): ?>
                <li class="has-child dropdown">
                    <a href="<?=( @$item['custom'] ? $item['link'] : $racq . $item['link'] ) ?>"><?=$item['title'] ?></a>
                    <span class="fa fa-plus pull-right dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></span>
                    
                    <?php $itemItems = $item['items'] ?>
                    <ul class="dropdown-menu">
                        <?php foreach ( $itemItems as $itemItem_key => $itemItem ): ?>
                            <?php $_items   = $itemItem['items'] ?>
                            <?php $hasChild =  isset($_items[0]['link'])?>
                            <li class="<?=$hasChild ? 'has-child dropdown dropdown-submenu': '' ?>">
                                <a href="<?=$itemItem['header']['link'] ?>"><?=$itemItem['header']['title'] ?></a>

                                <?php if ( $hasChild ): ?>
                                    <span class="fa fa-plus pull-right dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></span>

                                    <ul class="dropdown-menu">
                                        <?php foreach ( $_items as $_item_key => $_item ): ?>
                                            <?php if ( isset( $_item['link'] ) ): ?>
                                                <li><a href="<?=(@$_item['custom'] ? $_item['link'] : $racq . $_item['link']) ?>"><?=$_item['title'] ?></a></li>
                                            <?php else: ?>
                                                <!-- <li><p><?=$_item['title'] ?></p></li> -->
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </ul>
                                <?php endif ?>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </li>
            <?php endforeach ?>
            <li><a href="<?=$racq . '/membership/what-you-get/discounts' ?>">Discounts</a></li>
            <li class="theme-bg-blue"><a href="<?=$racq . '/login?returnUrl=%2f&LoginTo=%2fyour-account' ?>"><span class="fa nav-icon-membership icon pull-left"></span> Your account</a></li>
            <li class="theme-bg-yellow"><a href="https://www.racq.com.au:443/roadside-assistance/compare-options"><span class="fa cms-icon-car icon pull-left"></span> Get Roadside</a></li>
            <li class="theme-bg-yellow"><a href="https://www.racq.com.au:443/insurance/get-a-quote"><span class="fa cms-icon-quote icon pull-left"></span> Get insurance quote</a></li>
            <li class="theme-bg-yellow"><a href="https://www.racq.com.au:443/membership/manage-your-membership/make-a-payment"><span class="fa cms-icon-credit-card icon pull-left"></span> Make a payment</a></li>
            <li><a href="https://www.racq.com.au:443/travel/trip-planner"><span class="fa cms-icon-trip-planner icon pull-left"></span> Trip planner</a></li>
            <li><a href="<?=$racq . '/membership/what-you-get/discounts' ?>"><span class="fa cms-icon-alert icon pull-left"></span> Road conditions</a></li>
            <li><a href="https://www.racq.com.au:443/cars-and-driving/driving/fair-fuel-prices"><span class="fa cms-icon-form icon pull-left"></span> Fair Fuel Price</a></li>
            
            <li class="normal"><a href="<?=$racq . '/about' ?>">ABOUT</a></li>
            <li class="normal"><a href="<?=$racq . '/links/business/business' ?>">BUSINESS</a></li>
            <li class="normal"><a href="<?=$racq . '/our-sites' ?>">OUR SITES</a></li>
            <li class="normal"><a href="<?=$racq . '/support' ?>">SUPPORT</a></li>
            <li class="normal"><a href="<?=$racq . '/contact-us' ?>">CONTACT</a></li>
        </ul>
    </div>
    <div class="contact-wrapper">
        <div class="col-xs-12 details">
            <div class="page-header">
                <h4><i>Contact us</i></h4>
            </div>
            <a href="tel://131905" class="btn btn-yellow btn-block"><strong><i>General enquiries</i> <span class="fa fa-chevron-right pull-right"></span></strong></a>
            <a href="tel://131111" class="btn btn-yellow btn-block"><strong><i>Roadside assistance</i> <span class="fa fa-chevron-right pull-right"></span></strong></a>
        </div>
        <div class="clearfix"></div>
        <div class="list-menu">
            <ul class="nav navbar">
                <li><a href="<?=$racq . '/contact-us/contact-us-online' ?>"><span class="fa cms-icon-envelope icon pull-left"></span> Email us</a></li>
                <li><a href="<?=$racq . '/contact-us/find-a-branch' ?>"><span class="fa cms-icon-phone icon pull-left"></span> Find a branch</a></li>
                <li><a href="<?=$racq . '/contact-us' ?>"><span class="fa cms-icon-person icon pull-left"></span> View all contacts</a></li>
                <li class="socila-links">
                    <div class="col-xs-12">
                        <p>FOLLOW US</p>
                        <a href="http://www.facebook.com/racqofficial"  class="facebook"    title="Facebook"    target="_blank" rel="publisher"></a>
                        <a href="http://www.twitter.com/racqofficial"   class="twitter"     title="Twitter"     target="_blank" rel="publisher"></a>
                        <a href="https://plus.google.com/+RacqAu/posts" class="google-plus" title="Google Plus" target="_blank" rel="publisher"></a>
                        <a href="http://www.racq.com.au/about/blog"     class="blog"        title="RACQ Blog"   target="_blank" rel="publisher"></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="hover-content" class="hidden-xs hidden-sm">
    <div class="container">
        <?php $isFirst = TRUE; ?>
        <?php foreach ( $items as $menuKey => $menu ): ?>
            <?php if ( empty($menu) ): ?>
                <?php continue; ?>
            <?php endif ?>
            <div class="hidden-xs hidden-sm col-md-12 <?=$isFirst? 'active': '' ?>" data-menu-id="<?=$menuKey ?>">
            <?php $isFirst = FALSE; ?>
                <div>
                    <div class="col-md-3 popular-items">
                        <div class="page-header">
                            <h4><i>Popular items</i></h4>
                        </div>
                        <ul class="popular-content">
                            <?php foreach ( $menu['popular-items'] as $popularItem ): ?>
                                <li><a href="<?=( @$popularItem['custom'] ? $popularItem['link'] : $racq . $popularItem['link'] ) ?>"><?=$popularItem['title'] ?><span class="pull-right fa fa-chevron-right"></span></a></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <div class="col-md-9 items">
                        <?php foreach ( $menu['items'] as $contentKey => $content ): ?>
                            <div class="col-md-4 item">
                                <h6><a href="<?=$content['header']['link'] ?>"></span> <?=$content['header']['title'] ?></span></a><span class="pull-right fa fa-chevron-right"></h6>

                                <?php if (isset($content['items'])): ?>
                                    <ul>
                                        <?php foreach ( $content['items'] as $contentLink ): ?>
                                            <li>
                                            <?php if (isset( $contentLink['link'] )): ?>
                                                <a href="<?=( @$contentLink['custom'] ? $contentLink['link'] : $racq . $contentLink['link'] ) ?>"><?=(strtolower($contentLink['title']) == 'more...') ? '<i class="more">' . $contentLink['title'] . '</i>': $contentLink['title'] ?><span class="pull-right fa fa-chevron-right"></span></a>
                                            <?php else: ?>
                                                <p><?=$contentLink['title'] ?></p>
                                            <?php endif ?>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                <?php endif ?>
                                <div class="col-xs-12 hr"></div>
                            </div>
                            <?php if ( ($contentKey + 1) % 3 == 0 ): ?>
                                <div class="clearfix"></div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
<!--====  End of Top Header  ====-->
