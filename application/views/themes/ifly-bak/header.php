<?php
$base_url='https://www.europeholidays.com.au/';
$header_items = array(
	'home' => array('title' => '<i class="fa fa-home"></i>', 'link' => $base_url),
	'escorted-tours' => array(
		'title' => 'Tours',
		'links' => array(
			array(
				'title' => 'The Mediterranean',
				'links' => array(
					array('title' => 'Bosnia &amp; Herzegovina', 'link' => $base_url.'index.php/destinations/the-mediterranean/bosnia-herzegovina','style'=>'font-size:.9em; line-height: 1.1;'),
					array('title' => 'Greece',                   'link' => $base_url.'index.php/destinations/the-mediterranean/greece','style'=>'font-size: .9em;height: 40px;'),
					array('title' => 'Croatia',                  'link' => $base_url.'index.php/destinations/the-mediterranean/croatia','style'=>'height:40px; font-size:.9em;'),
					array('title' => 'Spain, Portugal & Morocco','link' => $base_url.'index.php/destinations/the-mediterranean/spain-portugal-morocco','style'=>'font-size:.9em; padding-top: 2px; line-height: 1.1'),
					array('title' => 'Italy',                    'link' => $base_url.'index.php/destinations/the-mediterranean/italy','style'=>'height:40px;margin-top:-5px;font-size:.9em;'),
					array('title' => 'Turkey',                   'link' => $base_url.'index.php/destinations/the-mediterranean/turkey','style'=>'height:40px; font-size:.9em;'),
				)
			),
			array(
				'title' => 'Europe',
				'links' => array(
					array('title' => 'Austria',                      'link' => $base_url.'index.php/destinations/europe/austria','style'=>'font-size: .9em;height: 30px;'),
					array('title' => 'Poland',                       'link' => $base_url.'index.php/destinations/europe/poland'),
					array('title' => 'Scandinavia',                  'link' => $base_url.'index.php/destinations/europe/scandinavia'),
					array('title' => 'The Baltics',                  'link' => $base_url.'index.php/destinations/europe/the-baltics'),
					array('title' => 'Iceland',                      'link' => $base_url.'index.php/destinations/europe/iceland'),
					array('title' => 'Christmas & New Year',               'link' => $base_url.'index.php/destinations/europe/christmas-new-year','style'=>'font-size:.9em; line-height: 1.1;'),
					
					array('title' => 'Czech Republic',               'link' => $base_url.'index.php/destinations/europe/czech-republic','style'=>'font-size: .9em; line-height: 1.1; padding-top: 0;'),
					array('title' => 'Romania',                      'link' => $base_url.'index.php/destinations/europe/romania'),
					array('title' => 'Serbia',                       'link' => $base_url.'index.php/destinations/europe/serbia'),
					array('title' => 'Ireland',                      'link' => $base_url.'index.php/destinations/europe/ireland'),
					array('title' => 'United Kingdom',               'link' => $base_url.'index.php/destinations/europe/united-kingdom','style'=>'font-size:.9em; line-height: 1.1;'),

					array('title' => 'France',                       'link' => $base_url.'index.php/destinations/europe/france','style'=>'font-size: .9em;height: 30px;'),
					array('title' => 'Russia',                       'link' => $base_url.'index.php/destinations/europe/russia'),
					array('title' => 'Slovenia',                     'link' => $base_url.'index.php/destinations/europe/slovenia'),
					array('title' => 'Ukraine',                      'link' => $base_url.'index.php/destinations/europe/ukraine'),
					array('title' => 'Switzerland',               'link' => $base_url.'index.php/destinations/europe/switzerland'),
					// array('title' => 'Germany',                      'link' => $base_url.'index.php/destinations/europe/germany'),
					
					
					// array('title' => 'Spain, Portugal & Morocco', 'link' => $base_url.'index.php/destinations/the-mediterranean/spain-portugal-morocco','style'=>'font-size:.7em;'),
					// array('title' => 'Croatia',                   'link' => $base_url.'index.php/destinations/the-mediterranean/croatia','style'=>'height: 33px;font-size:.9em;'),
					// array('title' => 'Italy',                     'link' => $base_url.'index.php/destinations/the-mediterranean/italy','style'=>'margin-top: -8px;font-size:.9em;'),
					
				)
			),
			array(
				'title' => 'The Middle East',
				'links' => array(
					array('title' => 'Israel, Jordan & Egypt','link' => $base_url.'index.php/destinations/the-middle-east/egypt','style'=>'font-size: .9em;line-height: 1.1;'),
					// array('title' => 'Morocco',              'link' => $base_url.''),
					array('title' => 'United Arab Emirates',    'link' => $base_url.'destinations/the-middle-east/united-arab-emirates','style'=>'font-size: .9em;line-height: 1.1;'),
				)
			),
			array(
				'title' => 'City Breaks',
				'links' => array(
					array('title' => 'Austria',         'link' => $base_url.'destinations/packages/austria'),
					array('title' => 'Croatia',         'link' => $base_url.'destinations/packages/croatia','style'=>'font-size: .9em; height: 36px;'),
					array('title' => 'Finland',         'link' => $base_url.'destinations/packages/finland','style'=>'font-size: .9em; height: 36px;'),
					array('title' => 'Greece',          'link' => $base_url.'destinations/packages/greece'),
					array('title' => 'Italy',           'link' => $base_url.'italy','style'=>'font-size: .9em; height: 36px;'),
					array('title' => 'Sweden',          'link' => $base_url.'sweden'),
					
					array('title' => 'Baltics',         'link' => $base_url.'destinations/packages/baltics','style'=>'font-size: .9em;'),
					array('title' => 'Czech Republic',  'link' => $base_url.'destinations/packages/czech-republic','style'=>'font-size: .9em;line-height: 1.1;'),
					array('title' => 'France',          'link' => $base_url.'destinations/packages/france','style'=>'font-size: .9em;'),
					array('title' => 'Hungary',         'link' => $base_url.'hungary'),
					array('title' => 'Rep. Of Ireland', 'link' => $base_url.'destinations/packages/republic-of-ireland','style'=>'font-size: .9em;line-height: 1.1;'),
					array('title' => 'United Kingdom',  'link' => $base_url.'destinations/packages/united-kingdom','style'=>'font-size: .9em;line-height: 1.1;'),
					
					array('title' => 'Benelux',         'link' => $base_url.'destinations/packages/benelux'),
					array('title' => 'Iceland',         'link' => $base_url.'destinations/packages/iceland','style'=>'font-size: .9em; height: 36px;'),
					array('title' => 'Denmark',         'link' => $base_url.'index.php/destinations/packages/denmark','style'=>'font-size: .9em; height: 36px;'),
					array('title' => 'Germany',         'link' => $base_url.'index.php/destinations/packages/germany','style'=>'font-size: .9em; height: 36px;'),
					array('title' => 'Iceland',           'link' => $base_url.'destinations/packages/iceland'),
					array('title' => 'Spain',           'link' => $base_url.'spain'),
					
					
					// array('title' => 'Belgium',      'link' => $base_url.'destinations/packages/belgium'),
					// array('title' => 'Estonia',      'link' => $base_url.'estonia'),
					// array('title' => 'Great Britain','link' => $base_url.'great-britain'),
					// array('title' => 'Latvia',       'link' => $base_url.'latvia'),
					// array('title' => 'Lithuania',    'link' => $base_url.'index.php/destinations/packages/lithuania'),
				)
			),
		),
	),
	'island-hopping-cruises' => array(
		'title' => 'Island Hopping &amp; Cruises',
		'links' => array(
			array(
				'title' => 'Island Hopping',
				'links' => array(
					array('title' => 'Croatia', 'link' => $base_url.'index.php/island-hopping-cruises/island-hopping/croatia'),
					array('title' => 'Greece',  'link' => $base_url.'index.php/island-hopping-cruises/island-hopping/greece'),
					array('title' => 'Spain',   'link' => $base_url.'index.php/island-hopping-cruises/island-hopping/spain'),
				)
			),
			array(
				'title' => 'Cruises',
				'links' => array(
					array('title' => 'Croatia',                    'link' => $base_url.'index.php/island-hopping-cruises/cruises/croatia'),
					// array('title' => 'The Adriatic',            'link' => $base_url.'index.php/island-hopping-cruises/cruises/the-adriatic'),
					array('title' => 'Italy',                      'link' => $base_url.'index.php/island-hopping-cruises/cruises/cruises-italy'),
					array('title' => 'Iceland Cruises',            'link' => $base_url.'index.php/island-hopping-cruises/cruises/iceland-cruises'),
					array('title' => 'Greece',                     'link' => $base_url.'index.php/island-hopping-cruises/cruises/greek-islands'),
					array('title' => 'Spain',                      'link' => $base_url.'index.php/island-hopping-cruises/cruises/spain'),
					array('title' => 'European River Cruising',    'link' => $base_url.'index.php/island-hopping-cruises/cruises/european-river-cruising'),
					array('title' => 'Waterways of Russia',        'link' => $base_url.'index.php/island-hopping-cruises/cruises/waterways-of-russia'),
				)
			)
		)
	),
	'flights' => array('title' => 'Flights', 'link' => 'https://secl-au-reservations.azurewebsites.net'),
	'contact-us' => array('title' => 'Inspiration', 'link' => $base_url.'index.php/inspiration')
);

/**
 * [submenu description]
 * @param  [type] $items [description]
 * @return [type]        [description]
 */
function submenu ( $items, $title, $id, $link = '#' )
{
	# Code here
	?>
	<a href="<?=$link ?>" data-id="<?=$id ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$title ?> <span class="header-item-icon fa fa-chevron-up"></span></a>
	<ul class="dropdown-menu">
		<?php foreach ( $items as $key => $item ): ?>
			<li class="dropdown dropdown-submenu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$item['title'] ?> <span class="caret pull-right"></span></a>

				<ul class="dropdown-menu">
					<?php foreach ( $item['links'] as $key => $itemLink ): ?>
						<li><a href="<?=$itemLink['link'] ?>"><?=$itemLink['title'] ?></a></li>
					<?php endforeach ?>
				</ul>
			</li>
		<?php endforeach ?>
	</ul>
	<?php
} // end of submenu
/**
 * [subitem description]
 * @param  [type] $items [description]
 * @param  [type] $tile  [description]
 * @param  string $link  [description]
 * @return [type]        [description]
 */
function subitem ( $items, $tile, $id, $link = '#' )
{
	# Code here
	$size = 12 / count($items);

	foreach ( $items as $key => $item )
	{
		?>
			<div class="col-sm-<?=$size ?> col-md-<?=$size ?> template-subitem" data-id="<?=$id ?>">
				<div class="page-header">
					<h4 class="text-center"><?=$item['title'] ?></h4>
				</div>
				<ul>
					<?php foreach ( $item['links'] as $subKey => $subItem ): ?>
						<li style="<?=(isset($subItem['style'])?$subItem['style']:"font-size: .9em;")?>"><a href="<?=$subItem['link'] ?>"><?=$subItem['title'] ?></a></li>
					<?php endforeach ?>
				</ul>
			</div>
		<?php
	}
} // end of subitem
?>

<nav class="navbar navbar-default navbar-static-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" data-toggle="collapse" data-target="#europeholidays-menu-collapse" aria-expanded="false" class="navbar-toggle collapsed">
				<span class="sr-only">Toggle Mobile Menu</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="https://www.europeholidays.com.au" class="navbar-brand">
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAD6CAYAAAAbbXrzAAAABHNCSVQICAgIfAhkiAAAIABJREFUeJztnV1u20bU988h1fepUxeWYBtBLQNWL1LJV1FWUHkFtVcQeQWNVxB7BXZWYHcFcVdgdQVRr2IlF5GBykVgCZLxuE0eVJzzXoiUh9RQ1seQHMrnBwSxKIkcieKfZ86cDwCGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYRjmUYBJD4Bh0kQ7X3qLCFlLOAe564/1pMfz2MgkPQCGSRWIuwQAfbR2AYAFK2aspAfAMAwzKSxYDMOkBhYshmFSAwsWwzCpgQWLYZjUwILFMExqYMFiGCY1sGAxDJMaWLAYhkkNLFgMw6QGFiyGYVIDCxbDTEgnv/3K+xsBf0lyLI8VFiyGYVIDCxbDTIgAMazOQEC/JzmWxwoLFsMwqYHrYSVId2UrK5afVAWIbKYPZ7nPjWbSY2IYk2ELK0HE90tvCeEY0XrtZPBd92mxkPSYGMZk2MJKiO7TYsEBrAw3IGZFBncB4CSxQUVMd2Ur6ywvnSJClgh69t2X/dztVS/pcTHpgQUrIRyLysFtBPQ8ibHEhbO8dOqVGAYEcJaXAG5hL+lxTYoFVpncv92whsMEh/Mo4SlhUqD9s2JrJe5hxAkiZMc9ZpiHYMFKCFRYWIBY6K5s8UVsKkhysGiBfY7xw4KVECT7ryT6y9+OChmTODf5YoVGfI7WbnIjepywYCUFUTPpIcRJd2UrSwQ+MSaCcoosygoADM6bgBMg6hHCMVtZ8cKCZRgWQSHpMURBf/nbMiD6xQkxmzaLkoB+W7t+f0BAbwAA+pnFPF+mwoKVFIgF1WaBfAEwTBgsWEkRMiVEQQvZTRgFpWXqpwTJWkl6DAwLVmIQ0G+q7Wt/fziPeyxR013ZyoJlHaueQ8CXcY9nWrorW1lAqqieS7sQpw0WLMNIkRN6YtyA0YLyScRqe2NbKWYm4EbnXwCiz9dmETQBAMC233Y2itUEhvYoYcFKCARURrWnzQn9EJ389itAHL/8b8Grm3zxMJ4RTYezvHQ8FCsBJ5m7rycAAKvXjTNwnD0g6pFlnd7ki5Ukx/lYYMFKjoJqowXWQglWINgSgKhHJHaAyJdDaCGqIv8TZTAVxCoAAJE4Wrt+fyDnPq79/eGcgPYAAJAsrkAaAyxYSRGYYngQkHEXrlaE2F9vNWogxH7SQ3kI2dr1LKsg661GDSAkc4HRDgtWAjwwfVjoHz5Z2JP/N5kMieEYw6bqXuAoERj/eRYBFqxkqIQ+g8g5aoaQu/5Y98JPEPBt+4effL647spW1sngWwAAJC6ZHAcsWAnwkL9mkXPUvDCGtPjqbBo41gExC7b9trNZuug+LRY6G8Wqs7z0DhDLQHS2et04S3qsjwEWrJjprmxlwxKfPRbaj4VY7W48KxPQr/JmU6dUueuPdZucHaBBQC8BVsQ3eEqWdQoAWSQ4WGtdGu+PWxRYsGJGfPftqPUUWDEDxN3OZuminS+97W48S4UlEgY54k1wWx+tLIC/FpZNzlFsg5qS3PXHOhINPwcJ9LIRmqut9wtbIdZEWLBiop0vnbY3t8m9M98zuHOPRLcTYAUQdx2038Y1xihY+/vDuSoNCRF8KUi5649GpySRNI1HhCt3mlhmf2O8sGDFwE2+WPHieYIg4G8gRLjDFrGQdisrNMpdwvTAS/vuy4H3twCRBcQskTjiTkfxwoKVMFZfnLtWSKgPx51CLQyZPjSlaVUq4GYZZsCClSAIVBveoQnPEh1MRKhyI3OfG01CcStvW9Q6YIxeWLCSRNxXbLAVzulFICzgcpg87MJ1wJhJYMFKCqKeHLuT+9xoIlAtuQHFi0C/YDHMJLBgJYRXYldGECmX9tNccyk49jBRNjH5mTEPFqwYGClSR9RTJdOutxo15QUdUvwuDZClTvL2koYZZhpYsOIgGNJAeBa26iSI/lC8vxDBqIyC2OnOTAALVhwEQhaCK2QyCLj10PvTRLAWui+cgaT69Y9AlJn5YcGKAQScOO0EVatlKagdFUawTpQs1ohm5g8y5sKCFQMCxFxBkmmoHTULFAhtMD3anUkeFiwmUoj8Sc5y/BUBXcU+ICbVsGClgFTXCw+Ugh4Xf5WmaHcEqnGvwvhhwYqY7tNiAQHnq7gw6CpT0TMio6jJD9IW7c513OOHBSti+hmoAuLEgZ9jCtlV9IwoPlSlVzJ9jnBnZocFK3pqwQ0ZEqHdncNqgyPg83a+9Km9uU3tfOlTGkrO9DOjFpNcjiVz99W3GMHR7sxDsGBppruylW1vbB+386XT7saz8khaDdH5uGJ1q9eNM1XBO0DcHcYqIRbSXtgPgEu2MNOTSXoAi8agUzBUARAcsMtoQY+k5xFwNJI9AAK+IYDx6TgpCLS0wCrLn10pxF6DB+Bod+Zh2MLSjC/wE7EcbDhh3f179tA+JnlNGhAg/InPihVCX6nkFIgwkywsWHFCFJpDKJO7veoB0VkMI4oUXvZndMOCFRdEPbuvLh+jYprXmspIWg6NWljBZO8FDd9gNMGCFRME9GaahgW5z43mIlhZMosS2Y6C5kq1YmYnlU737spWViw/qRLQzwT0J8DgR0QW9jJ9aBrXyYSoqap/9RB2n46cDOxOE8dlEkRQBhz/GhRUB/v+sanR7t2nxYLj/k0W9h74WExEpE6wuitbWWd56QIQygAICDhoTGoDIAA43wC0N7cBiHqeQ5cE1gnFrQVWz0tEztx9rce1rE5A+7McK/e50ezkt4/CVgxv8sWK0YXwAkKrskyCF7+p0e79DBRYpJIndYIllp9UB2L1AIhZ8qLDLaggWEAAgO4s2Pn+CbS/vxc2Eli3//n3aB4R6z4tFpygVSHgZP16dlFZbb0/aedLPwPiSMdoBDwGgBez7jtKuitbWSewTVV1IkOi5yB7JpjJSN0vJbhUPjeIWQKsgAWvxPdLcwVjiozln74RNdeu3x+MectE2Hdf1PWwEMs3+eLhvPuPgrBuOUGCQbQc7c6MI3WCZYGltoA0VOUkwMo8rccJ/VM3VdzRLIyz+tJ0gRs9fZ0S1YonEz2pmxJad/+eOctLL4NlSwjozfpfl4cAg+lI4A5f8f5AwC1EKAwcwgpntq3ZgnukjES5jwGBal6AbVqi3QnoCgELCQ/j0ZE6wcrdXvXgFl60N7dDrwf3NTVpUy34ms5m6YKCFRCI6uPy/KZF2VBiVoiaaYoEFyCyKBvwqrQcFSn6jEz8pG5KCKBufx46VQzBdycn6gHRmX33ZWf+0Q33eb7eahzq2h2G9CwkgoLq+0iaYDONcdNjnl4xk5JKwVI5dKeumy7dyRGhvta6nCn0wCMoGqGO8hlZvW6cgYDRWC7EgrO8dGGaaCmbaYQQDCjlaHcmjFQKlu5OyPNO3bpPiwVneemTvC2KGC9EUEeKI5bF8pOq7uPNQ7CW+7jveFrrmHm8pFKwVN2EMyQm/tHPsxKoYqSqaER9BK1+eOE/7eEe84Lqjs8qgtaxqdHuTPKkwunupeJ4F6WF+HPQ4z6NszwYtTxvbpgFVi8wnkgEa1y0tWW4H2ia8Zka7c4kj/GC5abifAKE4arTpMvlkzJv37/V1vuTTn77Pg4r7pUuorr1z9dQ6ytuVD6ocd1y1luNWntzO8ohMQuC8VNC57snrx9M/p1yCmaB5Zuu6GiMsNp6P3Vysy7WWpcvTC83PM2UPU3BsEy8GG9hoUVleiDl31e1cgKCMULiGzxt50tNArrK9OFs7moPk8YcLS6V4IYHp+wpizNjksF4wSKC3kMlSuZd5SPACuCg9oOToV+7K1s/zmOx6ErJSStI1orvnE1gASNCk2Dgu0pLtDsTP8ZPCUGI35TbiXpE4giF2J82QHPsMjpiMK2HmZJgpdFJLGBfP0a2tJgQjLew1v7+cN7Jbx8oEovra3/NFkm+2np/0tko9gRCAdF6rWWgzBAiyMoW1pjmsPevAfpzWNuMSQRvNR5gkLNrol/UeMECGMTpoGZjcPW6cQYA0N7c9gsWUT3Y4JOZktHE9D+n3YXxxQk14ibrv0LAXwAACOh3nWldkyK+X3rr5dc6y0svu7C1Y5pomT8lHFAJbpjkrj01bj6haScpTag6Uk8Sg5W2Ouk6x+ssL50iWq8BsQyIZUTrdWejWNW1/4formxl2/nSW19LOsSys7z0Ls5xTEJaBGuEWe7aD2HffTlgsZqPPlojISjjYrA8grFwpke7k4U9ogkq347hJl+sdDZLF6pqsoQYi2jdlxx3x0B0BuSKMWKBLOtUdRNKilQIFgI+j2K/wQDHtIqVYcnCleCGSabYwVi4VES7z9EcpL2xfYxoXQQb7Ur7HoiF5jQyGUmsBoJEdLbWuty3+7Qnr+w6aP8a1RimJR2ChTDywzAtYTbJuxACGvODGrm5EPUmah5rWqejCOluPCuDBa8mea2TwUimZd2NZ+VBBolfrAAG58ImZ2coWojV9sa2shFK3BgtWN2VrWx7Y/tYZXpPXU7mARCoNs/75alQ3HFEKkFPkIL8YKqgXumuvqjR7t2NZ2UH7Qvlk0R1IrHji1tDzJJlneosH9T+4addB+2LoYUo4MQTK4/c9ce6T7QseNX+4afEV3GNFiznuyevwYJXUfXl01mmZr3VqMlzf137HTmGAlMK4HVXtrIjK4QCJxasaTMW4iSYzjUrDtqnyt+zu+Cz3mrUXKHwfRfO8tKFDiu+s1Gsgm2/9caAQuyHNUoZipY3REWVlLgxWrDGmc3T5KaFIZ8AHauOiNFUaXgIAlIH18aM8+R/KsFtCGKmxRHTot21le9Rld1xp2Pe1Dl3/bFu333xixZi2UF7rkKNnY1ilSzrdLhLIfa98J4wZP+VCW4YswVrDDprrwMMplVROjijRHdBw2npPi0WOpulC7DtkTZpljNaTz8MnzWGWGhvbB+n9ZyEQqSqqlEOClHu9qqnEK2ss7z0bhZLq72xfTytWLXzpVNArLrjPksywd/DbMGKqBCe8lCAFSeD7+a5g4Wu+Ggi7OJN2lR3MvhW+dmJmtM40wnFrW+DBa+cDL5bJNGy777sj4jWIOZpxHrK3V711lqXL4DoTHptwcHRG0MYw5uJN1sh6oHj7E0pVudBH1dSGC5YeBbl7pGsFf+GOfMIpbthFCs7ImOFOT1ruo81KTf5YiWsuugMU9XayBbEbD8D1elHZiauCO2NLPKEiBYAwFrrcj8oWu18qftQOEt3ZSvru5kQ9Wxydtb+/hBaO80NIvVZVrr7E8yD0YK1dv3+QNl4QRPBJF2A+Wpj2X3a8/4mHKRZ6ISARuPRiOomprAgUC1z93Wqc7featTAcfaiPOemYP3vlz0i4e+E9IBo+V6PmEXA0FADN2zhnRS20LPJ2RnnSpHisqqDt4ijeZuz6MZowQJQTBMAtNWbCoZLEImjeeKBcp8bTe/OGUWogaoTTVKOfg+lwAs4Wf3rcqYUp7W/P5zb0PdZZiY4e3WTu73qrbcah5NODwEA1luNQxRiX35tO1/qBsMN3NCJ0+Fq9XRiVQYAQIKDJPIZH8J4wVKhrd6Ut7xMVLf/Ez+aeIJMJ/e50RzxNSJV5tlnML1Hd8ydSay1Lvd8IgQwVrRWrxtnAdHKgnVfcWQY53VvWdXtPr2YRqyIxJEJDnYVqRQs3SBC7zFFWutmJH5qio45KqIoYW0yq9eNMyJxFAgYHStatui/kCLRy+18qStZVvc34rsvO+N+2z6xIuqZall5GC9YqohnHYGSssNymuBGZhRVxdd58huDMU8m3kx0xAHKrLcahwT3PlAAGCtaI5HoiFkH7dcgZRrYfdobNy13+2neW1ZAe6ZaVh7GC1awISfAaKfguY+h8pMxE6NKTp/ngvbtz9D6+LrjAAGkRYcJLa3c9cc6Evmj1KWVdSeDodHx3Y1nZSeD7zzLikjsmLh4E8RYwepuPCu386VP804vwpCDLU3v6ecRSQ2wOXHTcfzhFkTNeS5oX+kgxILOPDrTWfv7w7mcDgMAoaJ1ky8eysGggLgLSLvS44Kw7JGVRMnPlQUAsMlJhVgBGCxYvlWOADoERg62nKRekwlEUQNsXsR3347GhhGGxvlMQrA4nirlZ5HJXX+suz6q5nCjK1rew3a+dKos7x24ZgiwIscE+sSKqG6L/liHvGkYWSJ58KWGW1a6BUa3P+JRYeHL4KZgWMK02P/+X835/ol0DOsXAJhLBNNG7vpjvf3DTwe+dKeBc/0Uif4gL7BzAsiyTjsbRbBA1CWxaqaxuq6RFpaqauUQop6OmusIuOX9LTBTeUzTDl10nxYLIyk5c04HAdxCiv74pMTLmiTB2t8fzu3/xI8BS8uXwDyEqIdANQSqqVLayLJOfZbV3Rfjm++qMFKw3FItoV+mji9aDsIkhGNneWn0RzAHj2HlUZkqNOd00AOJfr9/gFkTajElgVtMb2/c9eBGpOdW/7rcWf3rcmetdZnzpfJ4DMTqLA2dwsMwUrAAAEZWP7ztmmomjSzFK+pqz8NjWHkkIO3TQQ/rn69+4RtMCx8lueuPdQLa8/pwytYnkThSxU25+YdN+XW26L8wJYl5VowVrGEwXQBdK2Uj8V3qsh9MCN2nxcLICq6G6aCHalr4WKftnc3SBaJ1gWi9dqeDFQAAIGqOy9ckoKE4IeAvaXKuh2GsYIWha6VsmEdI1CQSR7oy0k2p/hk1ygoKmqaDHsFpoXJFcoHpPi0W2hvbxyN+wmEkO56Pm9oFquCO1NxKI6kTLG14JWKJjtZbjUNdc3rdQa2mgqBYHXTEG53HWL1unPl8N4oVyUVlGNg5puouofg97DnpRTXvT2d5KdIuPHFgtGCFNCKo6TxGWmKwTMKtgVXwbSSqR5RCc++vAayk/YKbhJt8sRJa+11ikmBPn6gh7s5bpDJpjBasqJDTFRY9sTYKVNYVAkZSV94mx2e1ObZlTEuzKOhsFKuI1oUyw2PQ5HRocU4iPOutRi1Y3SHNlpbRgqXKI9SRQiDHeZmYWJsCRnxJVl9EsmiRu/5Y99c1p4X1Y3U2ilVCHI1eh0F9qrXW5b5sdU1aHXf1unHmy1F0LS0tg44ZIwUr6jzCITHWjF8UOhvF6shUheg8SuFHonsrC7GwiDFZwyYRinQ0FGJ/3ioKwxxFqbpDO196a1Ib+kkwUrCEZR8r8wgDvdpmxau3ZHIfPGNROL59q3lRHPKfr+d+57sVu/NdVZFCVyWJTn77ldK5HmgYERSXaWcbipI0u/O2DosbIwUrrPuMrnLA2nrMPTJCUnF6I0GemnFXcO+Pgbgbtw9GVfJaR+XbzmbpghBGa7MHGkYMuzVLz89yPGUdreWl47SIlpGCFYaqUNy8LOL0Qhfu1PxtZ7N00dksXTgZVLVYHxsLpAtTne+zFir0utOEtEeryzXYO/ntV3K3ZoD5ZgcK0arKlSBMxkzBUuVBRXEYwArY9tu03F3ixkH7LSDuEmCFAEdDGSC+rtO56491X2ssC161N7epvblN7XzpUxRt1aLCLUv8DlQVF7yyxq5YtfOlU5UFNm+AskK0yu18yfjVQyMFy777cjDSt22Aatv8fPf/CpHsN+2E1CMbEneLMREijogFwvCWVybR/uGnXVesCsHnEKjmlXzprmxlO5ulC6WogZ4AZaWlpbaijcFIwcrdXvUE0UgeoS58AalE54uQY6WbSaY6vtW7GHAj35vqwYwPsjSBm3yx4k7tCiNPEp15rdHcnoIXI9NFqV9jsMjhQ3Ty269U1pNCtAqd/PYrU2cdRgpWGLrv5m72+t7Dr2R8EPVAwMlD7c6jwO7TDpE4crvM+I5v6nSmu7KVvckXDxFQ3WJewIlXRWGkTZcHURPIGfpwbZxuhZIQjp0MvlOFMQRFixCOxfdL6rEmjMmCVYlqx96diy2rcIKttgDcukt/vce11mVu7fq9svxP1OQ+N5rrrcbheqtxaN998Y2hnxltNGsCzvLSMaL1WmUFohD73nfZ2ShWHSvzThXnZvdpB6XtM/12B511lI0pRkQrUFrZFEwWLB8hPi1j9jey/ylNdtNQhX6Y1qwjRUXoFIUOqYdC7HtW6khDCel19t2X/dznRlN4RSdnif/y3vOQaN19+dGLdyTLOm3nS0ZFxKdGsNIGWZiWi0mJqj28QLMsGFP9LApGSxYD7a1eN8688AZVQwkEqtnkDOuue77XWeK/hu8hqo8Vrdurnn33xbd6aNL3bKRgdVe2snLNdQB9JYc9P8djqVs1K6r28MFzkjTBXDpTE9lVoR8IeCw1Mq2Ovol6q39d7shTPy+3dpZrwfu9r7UuXwDR+TjRcpaXjofTUgEnJlmyxgmWu0LyKXgSdZUc9vwcUdWtSvtU0EPlw0LDLCyL/OMxNZF9vdU4HIktRCw731jqfFmi5khvQvc9ALNdC97v/SZfrNjkHAFRcyha0mJFO186HV57ROdJ+SrDME6wHLRDnJN6hUA15dFB2qeCAF4KDo1MUUxr5BqcopqcteDWWD978IUDn9VIr8CAJVSb9vjy9eP6ql4Mp4fu6qFPrAAAhIglKHgajBMsVc4WAICtuQuNasrDDHAy+FZ10yCIN+7qITJ3X08CSdGnJvlbgth3Xw7G5gAS9QhoTzUFcwgL3t+zTH29G6lnOedur3ogxL2lNVidrLrjOLNF/4WXx2gSxgmWCgSqaTT3K4N94ttOfju0/Oxj5SZfPFROUwScmNbO3L3ogsXpjI14dx3aPyotrYFl9WPYdyx3Kp/lWvB6ecqrv2t/fzh3La37/bkxYaaG/BgnWKSoC65zKpIhce7+YJrKLPmUoXPx4CZfrCjbnxPVTfNleKz9/eHcF6KCWJ01IXlaZknGz91e9VTTQyQ6GOfcHi54zFhiydt3cOEkd3vVI6DfgKhnos8qiHGt6tf+/nDe3Xj2wrEyw/gPXZ1yAAAEWGXJ9DXKJzMLuhYPuitbWQdQ2UzWJmfmjkLdla2s+O7bXQLrOVpUJoLyfdcX6iFCnQh6BPQnCqrb//5fbdpVKes/2ncyMAy4RMDT7sqW0c1C11qX+92NZ2/6aGUzJHoPWTSIUCCYs8QSUV21cOL2NTyceb8xYpxgAQycgu3N7eFjnQ5ygVBAGERtQ1TJ1CnEWV5SV7skOJhletD+4addsKyXjtSglgABUN45ZgmgMtiMu2ADON8/gfZy6RyE+G1SH0ruc6PZyW8fEbgWM2LB+e7Ja7gFo62Fab5XLztjnhJLuurJJYlxU0KA0cqKuh3kCFRz0ztqOvebVtyyxyMrbAhUm7Y0b2ejWG3nS5/cJN/ZVu0Qd8G237bzpU+Trvyttt6fBMvPxDU1jBo57GCemzcJrIcVx0wLRgqW3CQCACBDQtudAcla0bWvRaD7tKguzULUs/6jiaeC7R9+2m3nS5/C6pLPBGLBFa6JapZZ/9G+PM13p4bGrhpOipwjOc/NW1csY5IYKVhBdK5YuH6Upq79pR3xDar73wmxP8lqVPdpsdDZLF2ElU1BoBqROALH2SMSO94/JDgItKIPB3HXWV560NrKfW40EfC+LBFiob/8bepXguUgXm+1b0ZqAKMzmDRhpA/LRwSO8cfSnfkhbvLFw5ASveeT+I86+e1XDtBrAEnwBufrHIT4/YF91ADgZOiUR3w91jJDzIJtv+3ktw/GTVNXW+9POpulX7zPhWi97m48S3XNMyLY8nx/OhYSgjOYNGG8hcWdbaKhu/GsHBLC0LPvvoydCnrVMAnh+H5ljmooxP5a6zK31rrcn9hhfnvVW71unK21Ln8kEjsPRYMTwnE7X1KuZnoEp4YO2mNfbzpoURlg/gojns82mNKUJowULBQ0vAPoTgdxE0hrOveZNrorW1kHbWWBtrBIa4+bfLHiLC99GlowgynfzupflzvzFvRbbzVqa63Lffs/8aNboK+pfCFidZxoKaaG5Zt88XCWMXmNOIggsWmUd2xd14JpVTemwcgpIVlY9la/dcZgAcAggZRI6y6TBAG3pl0NcwBfKqdfAk7Wr8NXTgedid2aTURNJDqKouqo6zs7BIDDzkaxCha+HJm6DgJEr9wYohF0TQ3d3NZQ3xkCvnS//1pkq86uFavjWkCgWpp//UYKlkwURePiKEMSW8gEYhVB3ahgKojq9j9fQuvodzaKVbKsUzff7U2YUOjGFcSz7tNiwcngawDYHU5D0Xp9ky+GCkUwoNRB+/QmX5w2Nqvge0TU8xzfmbuvJ/3lb8sWWGUBogIRWO7yzUjHtUAEPV9Pg5Qxl2B1V7aywZpEQdyTqXTyIVkr3vzcwxcJDQBCQ7PKIP0MFG7y97EtD0UadzeelSd1VFpgldN4B7PJ2Q+bCnY3npWdgVid2XdfxqaQRIVrde13V7YOXCf9r4BYHhfVrggoLSPM3hUGCQ6sPg37MOZur3pwC7WbfBEAoKKydC2CQtgUDAGfhyX7E0EhaAXruBYI6E8g/CXOGLVJIvkn3tesb2xvbB8Li8qyEywoNgAABAAY5ipDN/o5sM03QM3WEALVENHnbBZoQ2ezFPoeAZM7+4goC4aVYXkIInE09gflWD0b+iMlT5LAFYkzcK2ufgaq416/2np/0s6XlCWLpkWAqK8qQj0yfWg6GXyOCD7LhQgKZGEBg2+QXxP2hOJNOqx2FFQH23rtE243RWrefYcRvL6s/2iikBkV477LSBhjlVXkBwi4hYB/ThtpvcgMLJ2M1hrbCFRb/etytFjcAuH5webZBxEMaqtrsi7HWTiqWQkKqptY7iVuYhcsZj7aP/y0i5ratBNBM6kpHsMwDMMwDMMwDMMwDMMwDMMwDJMgvErIGEt3ZSvrPPmfCllYthB/JoKsskEGwCBVCKFJAuuIcGVRv2ZC3Bijl5kEq7NRrCaYQDlRzlYwqDCuVJKHkJNwZxnTrEm8IdR0RiHroLuylRXLT6oE9DJUnCaFqAmE5zb0f4vzM+q6PiyCpkBoRpHmlYYiS/kxAAATLUlEQVRrWMVsgrVZukiq1CqROJrkQnc7wAyjedf+em+ENdne3B4GN88yJvn9WiE6R6LfrX++nicRlzXMFVS1bdcAAtUE0VEcOZ5RXB8IVAPC362+ONfR8i4N17AK45OfmZhA3KVBZc/jm+Xim8zd15M4hKu7spV1lpeOnTChIqoT0O8oqG4jNcMsJSmDooKAz4MVFgiwgoiVzmapZglnpsYaSUKAFUCoON9Yx+186czu05HGXp2pYW7BQoKDOLsox1FpIS3M8t2joCxZWPYSzxVlW7II+NpZXnrZ/e7ZXpQXdvuHn3adQQ14f54fURMB30xjTXiJyCBVTFCVpiHAimNl3rU3tk/i6ME36/XhJU0j4HMAqPi+I8Sqk4HdTn77SEvqGtEZAcXWln6ea3huwRIg6tx9Jhnm+O59OWluS65ffNMxxIID9kV349lOFKLVzpdOR6Z/mmtsyaVpxDd46hNnC16186WK3ae9KC0VXdeH1zZtaDkOWqQdt/Ol52uty5n7RgIMSoan5Ro2suIoEy9rf3849yp9+hpDIGYdtC90Ni3ormxl2/nSO59YEfWQ4GCtdfljVAUBV/+63HFLMDeHTyCWnQy+S0NTBvcc7bmf4f4Gglhtb2ynvoP5pLBgMUNynxvNtdblHgpxf8ceiJaWmuiuv+rCt/pHVLf79CKOqhzrrUbNvvvywlc3PgJRjhL3M+z4biwWvJq0f2PaYcFiRli9bpy5nbEHIJY7G8XqPPsMEasz++7LTpzO49ztVW+tdbmvEOXUiFbu9mrQKES2tCzrUVhZLFiMkvVW41Du0kKIc5W0cZaXToNitda61FZfalpWrxtnStFKSePV3O1VzyZHHn9h3ptKGmDBYkIRRD4rS26ZPg03+eKhL8zAFau5BzgnKtES3y8puwmZSO76Y12e3hLiLwkOJxZYsJhQ1luNmuykdgL19ydhpP8hUd0EsfIITn8JsKI5myBSfOEIY7r7LAosWMxDDP0kZE2fKiMs+963QtSz+7SnaVzaCE5/EfDXWa3JuAmGI6Rl3LPCgsWMZZ5eeJ2NYtUX+yTEzM0HosbXLRox67YUSweSFdzPpLdJ6iSwYDGRQVJ3IgSqmdxEIfe50SSgN8MNiNW0WCsYQSs8U2HBYiKhs1Gsyn31fA58Q1lvNQ59Prs0WVmPBBYsZiwIuDXTG6W2WggUXRt3zaB/ZbSahjCHpKouJAELFvMQQ0f7pK3Su0+LBV/CsSPejHm5UVj/fD0f+rIAQHz3rdErb8Fpa1puDLPCgsWE0n1aLMjBntaEVQdExpJjrpom+66CuIGsw/GaHtskbKkBsZwnuaCwYDGh+NrAU3gtqhGQ7i9ywtSI1RAhfh/+bXpsk9zROo3f9ZSwYDFKggGfCDjxtM7nUyHnD70ji56gRTiurXyStH/4aVf+rm3ox1bTKilYsJgRuitbWQft+xQVouak1RSCCcRpmg7KyIGkANK0yxC6K1tZsKz7KhpE52mrojoLLFiMj+7Gs7KzvPRODkmwyZk4Ol2A5Ssdo3VwMSKIhpahW/XTGKTKF4MVzEEGQeTVU02Aa7ozAHDfBCJYWx2F2J/mzj0o6zukqWt8cWMRNMn9IIhgTGjDTb5YcQBP5RsKEh2YmkGgGxasR8xNvlixwCoT0M9O0LlM1AMh9lennNIhWSteL6Z50nqSRiA0PeElgqlzKHXT3XhWdtD+NVhWGoXYj6JKq6nM34QC8LizWYq8phEJrMfRNCBNzPLdB5uRkrsn/36pZvVpP/f5Q3PqMQ0aW0z7NuPIkOg56HpMgk0ypsACq3yTL872XoICgfUckHYdyaICgJlvKCoQ8GVns/TzvPt5CB3X8PwWFmI5mkZ5gcNYcRwlZczy3Y/TEqJzAnqzpin40AIrkeJ8Oshdf6y3N7fn3g8hHOOMrmIanqvASSNy23xNf0NRglggiD5pWsc1zFPCx4rb2l0Q/YGC6va//1fTXf0zzvZvC4/XxdoRbx6Lv0rF3IJFJHYWPR3AVKb57tv50ifPUYtAtdXW5U6EQwOAwZQm6mMYD1EdEaa+ERBBj4D+tAiaFoh6lCEL83Rijhu2sB4JBLSPgBeDv7HS2ShWo3bWCkyvYHU3npUdDfshoIO1v/iGrguOw3okuOWO5Ry54ygqEZDAhZgG9tG6/26kZGgmWViwHhF2nw58VTW/e6K93hOhuPX+Ni3gchpQ0FCwEGEhRHgRYMF6RIxU1bTgle5efHIJGpMCLqdFrl9PNL0PiokGFqxHRrCqpq9JhAbkEjRpLixnIQ7jktIcALtoLKxgZe6+shkfAgHt3/89cMDr2ndwNcvUSgcPEYhuryU1DsbPwgpWUh2F00DUDnhfyyyyjC6Ap6K78awsR7fzzc8cFlawgpjQAcWEMXhE6oAnlArgkdkF8BQ4kPHVo+ebnzk8GsEyoV+bCWPwiNIBb/XFfX4bYiF100Kk6vBvWXyZxFlowZKnJiZEXctjCBSIS4SoHPC5z41moJPyyzEvNwq3PdlwOmjd/XuW4HCYAAstWPJytAlR1wTWMC6JJuxAEzWROeAF3ZfrTVFTUrn5KxCd8XTQLBZbsKTlaHmZOinQIqmsC10lORaPqBzwq9eNs7Q1JQ02f7XJSU17ssfCQgsWSMvRBFhJsilmd2UrG4hLqiU0lBGicsAHm5Ka7MvqrmxlCfF+SvxIaqSnjYUWLNd6MKIppu/YRD2TKlxE5YB3razhRY+Ap6Z2UnaWl45l39VjqZGeNhZasFzuV6ys5Jy/hPir9NC4TjJROeBtcoY+MkAsOMtLWiPrdeBOBaveYyJx9JhrTpnMwgsWwb3zlwArSUxLbvLFir8sseSQNogoHPC56491IhGcGh7Ou19ddDeelcnfLqueltpQj5GFFyx3Wtj0HiPg6ZiXa6e7spWVj4lANZOmgzJROeDXW41DX5gDWq91pgPNitvY4WK4YdAua+KWZkz8LLxgAfgtB0AstDe2Y5uWuL6RgvdYyI5oA4nKAW/975c92Z9FlnWapKV1ky9WHLT9vf3I2eGpoNk8CsEKWg5gwas47vBB3wgQnZtqXXlE5YDP3V717LsvOz4nPFqv2/nS27gd8Z389itE60J2siPRAa8Kms+jECwAAPvuy748NYz6Dn+TLx4GfCNN++7L/pi3GENkEfAK0QLEXWd56V37h58iX8HtPi0WOpulC0KQwxd6tui/eEy9/dLMoxGs3O1VzyZnTw5zGN7hNUZhexcFoiVHTPdscvbSFDUdVQS8JFq+fEOw7bedzdJFFIsi3ZWtbHtj+9j5xvrki4Ujqtvk7LBllR7mbkIxT6PIWcn0oTmLryF3/bHe3Xi244Dku0DcdTJQuckX32Tuvp7MKirdla1sf/nbVw7grwBS403PN5Kyi2K91ai186VzcDtCuw74cx2im7u96sEt7HXy268I6LV3LgiwgoiVzmapBoJ+m9fq8bolO4FuyQAAIODE/ufLUZpuIlGBgFtxr57Peg3P1KK3s1m6SLKa5LxtiborW1nx/dJb5WcgOkfAPwSI+kP+JrnVOwRbvYPbQfl/v2i1rNqb28NulFG3WOs+LRacDL4biruAE93dt91jvA62YAcAr/lDzTsfmbuv9XHf5U2+WLEICjRIw6rIix3SPusEdBDl9yZfH6a2wUvrNfwo23y5d/idzkaxSoi+CGdA3CWAXQQLhp1/pd5yk7R6B6IeEh2k3S+S+9xo3uSLbxDcPMCBA/43ndaie5fd7z4tHo0I1+C8DM+H8/0TaH+/PWwCC6A4H2G3YKI6Er1J+zl57MwkWCSwnmTreNRU6WD1unHWXdk6F999u0uIv8o/fP8BpZbw41u915HojfXPVy1TJ+VQpHgmm0Tk05n1VuOwnS899xpK9NHaBdDfRUYWrn4Gqgj4y5jzcd9affz56AHAuU3Omzin5PL1Ecc5moW0XsMzTQkXle7TYkFkrF0Ceo4IhYdMZgSqEUETAf+0+uKcY3j00n1aLAgbKgTWc7SoTAS+0sVB3PPRQ8A/LOrX0uY3ZBiGYRiGYRiGYRiGYRiGYRiGYRiGYRiGYVLLo43DIqIsAIwEJiJiLf7RMMziQESVwKYmIjZ17HsmwSKiY7i/2OuIOFV+GRFdSA8PEDGWAD8iKgBAFQB+AYVYSdQB4DcAOEPEWCKViagMcF/2BBF34jiuYhyRnpvg53yAOgD8CQA1XT94HQR+/78j4kmExzoF8PXUPJr2pur+7uVKu01E1FrqyBWpXwEgrExQEwbX1Elc19QQIrqgey4efsfI+2UqEQwxeLwsER3T9HSJ6DDq8bljrMgHjuOYIeOI9NwEP+cUfCKSWsgnBBEVFL+RyAoQKr6vT9Mej/zXq/bzStNdW10iird7FaVIsIio7J5kFe/cz+L9ezfmdZFWxSQWrEn5FMW4phj/qWJMryI+ZlAQJi6oSESvZn3vjGMjGlwvx0R0SERvaSBSQeITLUqJYNFArIJf1if3JCoFiAZ30MOQ98V2J43qOBOMI9JzE/yc7rmoKP7tuuch7CZyqHtsE4w9GzKWTzEc99O058b9Lcu/43eaxxU8l+9U46LRGc4nIhrnktELpUCw3C8pKDqnNKHouO+/CLw/MksrePKjOMaE44j03Ch+5A8egwY3nuC5IIpZtGggoPJFJ1ON+NjlwPEeFEnFd6ZVJMhvbT44NaZ7iyveZrqUDsF6GzjOTO29aHQKEEnHHWLBmuS9VRoltqkF+W+ArwK/samvgxmOf0h+Dse8Nvhdhb52jvFcRH1daIEMFyzFRTHzj4kGllZwWlLQOFzvOCxYk70/eCF+ohju2IHjdt3fxVyfZcZxBH+LI1YTjc4utE4FpePIxziM4hhBFrUJxa+BxzMv4bpLsMGwjeD+mZhAxDMAkHs7FmAQqhI1cn/GM0TsueEFTWn7yxjGEfwtq2YOpwDgiXhP8R5dyCEvWxEdY37IYAuLRh2jWjo9B+5s2p2swbu17v1PMY5ILQYdVklcFoR0vN3AmAvSc0GLrxC+J23jCZ0aKsYa2QomjbpL4g1XmBQyW7CCF4SWL5FGl4cLOvYr7Z8Fa7r9BC+WKFdw5d/728BzQfHUcoOcckxdGqwIBscSqV9NcS6JBmJaiOqYOppQlKP+YqakEnhc07TfYMR3AUBPbXlmJn4H/1SwDPrO9RAa+Igq0qY38vOI2COiMwDwLJldIjqIIZp7HwDewWDql4XBNLAH8UwFAWCQxkZEJ3D/2QEGU+fXNGiW+xsAnCeepUDqJeZZqWgem89c1rjf4FSzomvf7v7ZwkpgPxMc51Q6htIVQKPR74dRjEVx3KDVLxNpMKtiHKrgUI93pCnsY1Gd7tqJPf+JSRy6zz31OFK9zrUgzqVNcTjfwc1hrCmeOo8yvzFkHC8A4ATUs44yAJyShiwFHVPCOoyuoj2ESVPIiYjqDs4Yjbwa3AO/KAV5A/eJvwUiqrormlGzBwCfIMapoApXtA8A4IAG0+iXMPg+CtLLCgBwQUR7iDjuu9QLme10D66SaInuVey3omO/0v55SjjdfoLTId1R3EEH9oOBkeSPfo905TJw3Lmuxyhxz7c8PqI5EsYXcUoYdI5XNO33F/kB181KHHna1YugRFEV7q0WAIBx/iLvJlOQXl+OQvDTBiLW3FJJZ9LmLMwYO7dwguWapk1p09xBnu7dQA6PiM+cZUZwhUC2qKI4HzqCgznA2MWtv9WUNv08y34WTrBcfpP+LtD8KyavwX+3/S3shUy0uDePYKyT0hk+xzGq4LeWZmWXYggkTRHyjYWnhBInMHA+erye1cdBg8BTWfDqsToMmSGuWF2AX0xOIojzkS2jJk4BAPwY2NdrWHCm8EcV5j3WQgqWG4Igr5RkYbA6MZVouWIVvJvHvgLDDKeB78A/FayDfuuqEjjGVPt3xfNM2lSd1cFsOjSIP3sH6nzG4Guz4Pcnx1IW3Tv4XKsSAV9lJYIhesdRVUM8fOgHRIMVolPFe6sRjpVXCUdfm6VBrt4FjRJJbTLyl4zpzriP4Gc81DzM4PHk7yeWVUIarJr7UpLCzgepK57MNOPREYdlLIh44H6JVWnzawD4lYjOAeAP8DsCyzBwBqryD/djiqsBgMGPcIqXx9bIIwKOiUgVlFuGcD/HOQzOh9ZgXhr4m+Rz/ybkpWNxU1bqcG+p/QoAh3MNzjyagcdVAKgQ0RvwW0+/wOiK63msv9d5FX3SO6wuFHeDafgU0xhViaSTom18UZ+bOT/nJ4qwIgCNWtWFOfYVrOJQ1TfSkWNdSMeJLQ6L1CXIH2Iuy3ghfVhBXCf5jzDwR0x6V27CwHL5kWOuEqUOg0WUHfdcRLLgQaOW+Nk8znzXGpffv3DOd9dKegF+n10YPRi0KHsxj2U8a19C2VyfOmiP/Hfuetx5eu7xKwDwHPymahMAriBukxWGF8ys0dravsOoz80Un1Nb881JUIxr7uMHrpPIgo3nvR41jcGLVXwOowsjf8LgmuJ8XIZhGIZhGIZhGIZhGIZhGIZhGIZhGIZhGIZhGIZhGGYy/j95IKC0B+DgTwAAAABJRU5ErkJggg==" alt="Europe Holidays">
			</a>
		</div>
		
		<div class="collapse navbar-collapse" id="europeholidays-menu-collapse">
			<ul class="nav navbar-nav navbar-center">
				<?php foreach ( $header_items as $key => $header_item ): ?>
					<?php $has_child =  isset($header_item['links']) ?>
					<li <?=$has_child ? 'class="has-child dropdown"' : '' ?>>
						<?php if ( $has_child ): ?>
							<?php submenu( $header_item['links'], $header_item['title'], $key ) ?>
						<?php else: ?>
							<a href="<?=$header_item['link'] ?>" data-id="<?=$key ?>"><?=$header_item['title'] ?></a>
						<?php endif ?>
					</li>
				<?php endforeach ?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="contact-item visible-md visible-lg">
					<div>
						<div class="col-md-8 contact-social">
							<div class="col-md-12 number text-center">
								<p><a href="tel:1800242373" style="color: red;"><span class="AVANSERnumber"><?=@$agency['number'];?></span></a></p>
							</div>
							<div class="col-md-12 social text-center">
								<a href="https://www.facebook.com/europeholidaydeals/" target="_blank"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAB2lBMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAABRFkUVAAAAnnRSTlP//vz7+vf18O7t7Ovq6ejn5uXk4+Lh4N/b2tnY1tTT0s7NzMvKycfGxcTCwcC/vr28u7q5uLe1tLOysbCvrq2sqqmop6alpKOhn56bioB+e3p5eHVzcW9ta2llXVtZWFdUUlFQT01MSUhHRkVEQ0JBQDw6ODY0MzEwLy4rKiknJiUkIyEgHx4cGxoZFxYUERAPDgwKCQgHBgUEAwIBAB6UleMAAAIOSURBVDjLjdTpWxJRFAbwN9CimQmpUAJBoSwrLSWj0NTKTNozSQnbS83MotU2jGwxMS2X1DTO/9ogWHMu48D7aZ73/mZ4mHPvgLSZj/rLZEmSJdnR0LvIlqC5fhvcWtERiSWSyQ9Peto925rjunDci8BL7TOe12PPZC68hIafJGS6BmER7sYz0skwahlckUtTpJslxa6F1p20btyO/3Cvkwyy3b8GLxYvGcFZUzgDx0xDZJh7RROr8EAL5UkgkIYvLN94PX/BV1V7OqlpEpvfqfBUq3D/PqTzWVsdOUeY8cS4e6AqE1ysG6hYRLRKeGBEhU9TX3jpu47gGQFeBWy/xb/T1gZnD2uWqRNQvqdm+EiveGAbZk0MZmDDpn97IZv7NsijrHmITOo5HFGgTLDm/VkvUBwKXuPwowRZeN3UDZTkzD4hQYnnwi0LInylwDakA3+J8G4JXN2FwMuVaAoVAk+0o3dXIdB7E3OeR/lhX+UyKHSMl2F1hCI8fF7djyMWtvXozg5fq/Ae45bR9FE4FKQ8qWtcPTNfzf3G7lbRVOa4dpnnjNw0IpT9ANTZjaDSSGuQHO71XVlmLfs1K5UW9NnsxnLSQjqIQT3Xh6PEoTqQ6kmRjfsQJRHS1H7UPNaywWr4f1AuVLfxSbv7eNfAm09jr/s7W8rtHdqRgf3Wyo0mp1WSZcnqar79hy39BWNZS3UtJ+UZAAAAAElFTkSuQmCC" alt="Facebook"></a>
								<a href="https://twitter.com/europe_holidays" target="_blank"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAACDVBMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAACvpYe0AAAAr3RSTlP//v38+/r5+Pf29fTz8vHw7+7s6+rp6Ofm5eTj4uDf3dzb2tnX1tXU09HQzs3My8rJx8bEw8LBv769vLm4t7a1s7Kwr66trKqppqWko6Kgn52cm5qUk5GQj46LiYd/fXx6eXh2dXJwbWxpaGVfXl1bWFZVUU5NS0pJSEdFRENCQUA+Ozo2NTQzMjEwLy0rKignJiUkIyIfHh0cGhkXFBIREA8ODQwLCgkHBgUEAwEA90zpMQAAAkRJREFUOMuF1Otf0mAUB/BDVFpZMZaQYgpKMyuxtKxBloBWVpRd7J7dzbK0sruZVlKIty6iBKlZYJly/saQwXa2YPzesPOc7+fZM7bnAaT50+EoZvV6Vs9u4u9EZC0g1yPuwvKmtt5PodDn/nZPRUGdNy0M7QD7ezqHzwncl//hDdgzhYpEXHBJCWugD9PkI2yWQ+PGv5g+5TkRAgsrMGPsqxdFWFuCKrFxKdi68rcaRKZFgAHtC1WHXo03Afe6MEtObluCA8xkNvhrw/M4bHJnc4inDiD85HrTdCbefEdcmE+Vw9YA3LOJ3UVv6umPA8D1scvfxNbuK1DvEau5FeeFi2ZIxOwTW2d3QWmbWE2vgsa5+G9QK0DriNjqMoGxR1qZHSD3gh+HBAcDUsfHADsqla8TU5m2ahJu7YzUCRuBlVYcrtYBiZn8Cz/MFOJB6uCwAkorxlk3hV0Ehg1gfEXqB8SVxkjjAwNlt0n9mMCH9E11FoHrKB0Ybl6fdC2yV3qmFjoqZSN9awR3Wv7ua65ClBMXGTxnTU6X2yl3fusEoMcp1t11hjyNNp+/q9waJ5zx73FQN06GopPhBVRmhu1Z2gqO/Zglx6oSeya4vFvdvYMhYbveWjar5mJ5FzF5ADgK1OCW7ZiCaLFkdjsZlCCWrItmuG+xbp5C3AdP07m3YEteiQdpO1QGlWyKh1ZUQpzmofqlbDYeqqSvmh72gSNF1oZrTwa/jvuf3TzEmRpHSRNk94o9aigz6FlWn2+pvy8/g/8BU7B6WnkbJNAAAAAASUVORK5CYII=" alt="Twitter"></a>
								<a href="https://www.instagram.com/Europe_Holidays/" target="_blank"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAACH1BMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAAD9hUwBAAAAtXRSTlP//v38+/n49vXz8vHw7+7t7Ovq6ejn5uXk4+Lg393c29rZ2NfW1dTT0dDPzs3My8rJx8bFxMPCwb++vby7ube2tbOysK+uraqpqKalpKOin52cm5aTkY6Ni4qGgH99fHp4dnVycG5tbGhlZGNfXVtXVlRRUE5NS0pJSEdGRUNBQD49PDs6OTg2NTMyMTAvLSsqJyYlJCMiIB8eHRwbGhkYFhUUEhEQDw4NDAsJCAcGBQQDAgEAuKOWLwAAAnZJREFUOMuF1PtfS2EcB/BPGSWaHbZWU63Jad1XimoauriFJCO3XCORW4jo4hZdxlwqiYVCakrx/QPtdp49z6z1/eG8ns/5vs/Oa+e5gPiabbElSxqNpJHWWy9NCy1wY2d1YkZNU9cbl+ttT3Ntpn5bX1joKkD5M/43Biohv/sfnkXpBIXUdBWOh8JidFOY6sdGEeoSflP4Mq+Y5mBiJi1aW2MXGCxJoQhlkRXYuNwdCZLa7ocjy+5HdNQX1eeDZVW0RB3K8cIX6g/Kjc/1uYXF/irKN+99z77n2nseWFOt5C9aCBXD5qV+B2FK7lLiUQ6l9Q41o0bpvEofweX84BfjYAHRc8isteUUKmpZKuVgHlEvTKx1ZDPSmlgqCyC9AYgr2mRCcL6uJ0HXKUAVVDeIOuJ8T2Sx1oAa0muWsvHEvQet3mE7yicd0LPWuA7SR5YysEAW+BeLKpEoRmKt70Yemj0wGz/9UCfCVEhODv6iE7jiHd5GnQDHtdA9FOCnKFwlugU4BdivhumCAKk7yveH20iA1wyo2s+SjFnP9WtDavLhMW+OXcNa9hK05LGUgylhdUUnsGHxaczID5TUgJM073a75xc8lzm6CDa7jvRRUG2lEr8ZxGW2elTpHKz0rMfB+GEm7RsysgJlNtWNs/tSp3cr2LbTEnXA4tszY6q2yO4pHP7tej76RyT3d+UxChwANn0kaM4lBZLRuLgrVFMQUsqqmUXemxw/x0Oy4W449xjK3mMHaTPyx0LZhBWNFApp0oqiDp49ssISXNX8YT+yz5C+88ydwaHhl+3ndstJu5xcE8K7/tysNmk1kqRZZ6xonRNa/wBugHm9IqZ1lAAAAABJRU5ErkJggg==" alt="Instagram"></a>
								<a trigger-message ><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAABJlBMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAADjrCtWAAAAYnRSTlP//fr5+Pf08vHw7+7t7Ovp5+Xg3NLJyMXBvri3tbSysbCjn56cmpmYl5aRj46NjImDfnx5eHd0bm1ramJfVVRNR0RDQkFAOTcyMTAtKykoJCMhHx4XFREQDg0LCgkIBwMCADm1arYAAAFaSURBVDjLtdRZW4JAFAbgE1qGLbZnq62mhlnZZmmp0aapmGVmmPH//0QswzDAzORFfTfMgfcBZp4zA9qAgf+HfUUuFmWlz4dqPhYBM5FYXmXCz4wARISMSoeFIHgyfE2De0BJwg/XgZoNL9wGRnbc8AKYyZGwKbBh8IWAW8DJrgMVnoOhJoZZLoQTDKN8uGTD9igfhjsIVtCNaNZ8tZhKiUSpp4pgySqT+jAJEKhrWj2ASyO3btjQhw2AuPFHcVwaKSFY/g3eIVh1Pp2wPl0TcGmkhmB3Ak3m1JqMJIlEqXd7z17HZf7yrOIFP+fDHIavIzwXenO6R+LBA6LN2pNsN9UhO7zAhlfuPXPIhGXPLtxnwWPvvj5jwBXfSfE4S4XjH/5DKj9DirnNafMqU04z7T49HzYeji2kHzTt+2ZNHx/RoJ5eq/LU+rKr98vQIgP68twdEPpm/UfwB60NOSf8SAssAAAAAElFTkSuQmCC" alt=""></a>
							</div>
						</div>
						<div class="col-md-4 others">
							<div class="col-md-12">
								<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAAAoCAYAAADjT+RAAAAQg0lEQVR42u1aabQdVZX+TtWt4Q7vvoHMc0IGiBhJAkGIyCAGg0y23VEaBBslaRsQBMTl1DQtHXUpiNCroSG9kNYGDUQSJMEYJAzJIiHvYUhI4s38hryp3r013qq6derU7h/v3fhIgLxgNMH1vrXqR5199q599q69a5+zCxjEIAZxAsGz7eu3bt16AQC0t7Zewjn/zPHW6YMGdqwFBkHwRV3XHwvDcGvRMN5kjI3I1dSYmq7PURRlWBJFlyvp9AvHe+EfBEjHUlhXV9c8zvkdURR1M8Y2NgwZcgUxlsrX1l5YqVReiONYLVcq32xpaZlzvBf+QcAxc45lFT83dOjQ1bquZze99trnHcdp5ZxvDsPwt0EQdHue91ph27b52Wz2nJEjR27cs2fPoIP+WgiC4I9ERFEUtRjd3T8TQrSUenr+r7Bjx+3lcnkN53yLYRiPCCEqRESu6/72eOt8ouOYRI5lWfNUVZ0GAGEYbldSqawkSWO1dPpSTdc/nslkPpFKpT5MRFoQBJsBQNf1i/fs2TP1eBvgRMYxcQ4J8ZEoihDHMVzHMWIhsgkROOc2iEqcc5NzDlmWJc75Xs45OOfI5XIzjrcBTmSkjoWQomnakm3/QZIkq7Oz881cLjesHPha4AdtYRi+gfb2vBBiuKYom7ngiWmaYwDoSZKUj7cBTmQcE+cMGVJfn8/Xz2SMIZPJOGoqVVtbX39+GIZO0TByo8eO/QwA5th2hyTLmVwu9zEhBEzTVI+3AU5kHHSOXSp9Op3LfShJkgIA5UiMjCVyFEVyTU3dE5blrslkan6gKAoymUyGc94BAESEOEnkKk8QhnY+n58AACKO+YoVK15cu3atfuaZsz6vKEqZSKIB6MyTJPmwaZovjR49et2hRCKaBWAcY2x5VVXPdR+KOP9FQ0PDugHIf18gItkqFr+6bPnyJddcc80/tbTsfXPKlFNfff3112edddZZjUfi7+joOD+fzZ6azecfOozY09Pz2TAI6GgQc06WVbwKAFzX3URExDnvLJVK90ZR9EZPT89DLfv3f8l13ad93/9dd3f3j6u8tm0/CQC2bf+HiOMBPzNJEgrDIGlra5v5Lkb6BBHdWL1vbW290DAM6uzs/M9D5y5dulQmordtxNeuXZvqP0ZE0tKlS+X+dLzD5p2IVKO7e8uyZctmEdEpABDH8bf27Nq18t1k9Y0xANi7d++dlmWt7U87GDlDhgxZ5jjOdMd1HqhUogYhRLMkSYelvSRJwDl3R48ePSedTk/WtMwiAE9GkX+LEOn1iRDh7t27n504fnx3Opu9zvf9A5qmndG6f//3Esba6urqFgHI7dq1658BQNf1ayVZhuM4Wzs7O7dqmlYjSYfXKUQUy5I0WdP17UOGDLl5zJgxxru8hDEAXr3JZrM3RGH4oKwoI9ra2qaNGTOmAACWZT2aTqfnhWHYVigUFkybNu2A4zjP6ro+IwyDzQAWtLW1XRVF0VcuueSSfQCuMU3zQU3T5pXL5Z2ZTOYLjLGiaZq3pXX9dgCvyrKstbW1dQOYunLlylGVSuXS8RMnnl0oFCZNnTrVjKLoN5dddtnQnu7uXw8ZNuybABCG4dNBEIx2HGe0kkq9d2SvWLFico9hhEEQkOd5sed5ov/luq5wHCfmnCdERHEck2VZiwBgS2PjKZZlbRBCkO/7S13X7Whva3uViJpt2/61EIKKxeJTa9euHQEAjuPcW42ISqWSOI5z2POql+/7ZHR1FQDo76U/EZ1HRAv7omaGYRjr+yL0YdM0HwAAq1S6y3GcFiJSXNdd1tzcfGtPT89dnutuJCLJdd2XN2zYcG3z/v0royha3ZdZ/rvseesBwPf9x0zTfPLVV1+dZJpmR6VSOZ2IFhV7egiAHgTBEy+++OLVRHSP49h/AADbNDd6nncnESm2Ze3bvXv3fMMwbg98v2np0qWy53nboih68R0jp4orrrhit+u6X1BVdbEkSW0AjHeYJwHoFkLMlmV5Vj6ff9i27dba2tpVURTdmCTJcsbYDMsqPS0EDTWMrr35fN2FADY2NDTcdsEFF3QahvH1bDZzGwDEUbROVdU/qqo6FEByyLM4gLFJkmRTqdQNAEIcGQQANTWZ+ZlMzTlEdB+A+X65bANAnCRn5FT1PsYYB/DZPuO/ns3lvs0YSwCcBwAdHR03K4ry474In6Kn0zIR3Q9guCRJbXPmzLk+iqLfaJq2GcDmlpaWz61evXokA2RVVQ0Amm3ZG86ZNq2GSdLYbDY7BcC9uZqawLLts8qeN1FR5HsXLFggXNddLIT4+/d0Tu+iap7auXPnS5MmTXpeluWxAFrxpz0R61u8JstyAwAwxpDL5Vb6vv9dVVXvATDW9/25gORNmDD2zfb29jPjOP6prutv9IXyEkVRvlRNX0yWhwHwAGQOeYYAcEoQ+FszmezcqtEHgEpvFEmfZox9D0AjgGcJyYOFQuHjQNLKK5UGACgWi6c5pjkVgBsEXgMAmKZ5/v79+1VG1Fl9WYQQteVy+ZV8Pv+1SqUyI4qiKJVKXQngI30Rq3V0dJxk23bUd08AZCKSPU1LEiIAeIAxttW27Uu7urq2jxs37r/iOBkOAEEQDM/lctkBrg8Iw/DWIAjIsqywVCp1mqZpVC/LskzDMLaXy+VC/w92GIaWZVkL30me73nfjePY7T/fcZy3isXirlKpZPaXXyqVOmzbpiiqkGmanx2ozn1p7cJiV9enisXinv4027a/b9v26qampjPMUqlYqVT+3XGcYseBA19taWm52LYsKwzD7zi27TU1NX2hrbX1CSK6GgC6uzu/bFlWt+/71zmOs+lAW9t9AOB53lYh+C89113a3NxMAJQgCJ5+5ZVXPsU5n+95Hq1Zs6a2q6PjUc9111cqwZcty9rX1dU1ef/u3XNt2/Z83/9X27bLlUplxREjpwpd1+9fvHjxz6+5+urf6+m0EkdRKxg7WG1EYehrqjqhP4+qqrUijs8H8Mih8pgsXy7Lcq7/GK9Uyp7n7VNUVQcAShIwxkRKVU81S6XfnTx58sUDfpv+hDrB2FsycHP/wSAI7k+n05fMnj270XXdBQD+jTF298jRox/oM/QtjLGFTJKunz179tJisdgNoAAAw4aNWOI4jiVJ0o2MaM1zK1feBQBxHH8mjpX7/CDY2NHa8RD1noz8dMyYMS2KojT7vv/w2WeffVYul7uh7LrfB6TP67p+S11d3W4Au61i8QZZ065Vs9lFsiwfseQ+DJzzu4UQ71neCiEojmNyXffRd5OzatUqLQiCVZxzEkJQkiTvKTOO4388Wl37FwQfdAy42bZkyZKaSy+9dGgqlapomtYgy3KeiGIhRIox5rqua7/wwgs911577RGPZFavXp2dN29eFgDzfX9cHMeKoijgnBucc59zrmzatKn78ssv9492QUR0HoBpjLFHjpb3RMOAnUN0l8TY3QkRSU1NTfIZZ5zBD59DWdd1M/l8vroHYbu2b5+palpWkiQuhJCZolRKYanDarcqmqZpqqp2HyqrujFjjA20AOjP+zfjnAEj9P3f9Z0AUHt7+9/1pwVB8INqOgqCYEt1fMeOHRPCMKQkSYhzTlEUEeec4r4TAdd1WgHIALBw4UKFc95cTWnFYvEb70fPv6W0NqCWwezZsxUmy6cBQCIE2tratlVpGzduHCFJ0k0HBUrSqevWrRsFALquz+ytKIFUKgVFUZBKpcAYQ5IkkCS2Gb3lMn70wx/eKMvyOKC3NFdSqZkD0W2AYJ7nvWZZ1lXH0ni2bU8tl8sfPZYyjxp79uz5MOeciIjK5fLbylPLsn7Yd1KQEBEJEVNXV9c8ANi2bdv4ffv2nb9p06Y5lTBsqUbe3r27/mHHjh2zC4XCJACYDqhBEBykExH5vr/9/ej6TpFjGMYVPYZBhmH88tD5jY2NGSJSDpEhdXV15Q49d3vrrbfUzZs3H9yLdHV1PRqG4WEy/6owDOPqg6nItqunvVi2bNnIIAjKRERFw3i6Uqns7Nu73Nmf/5lnnqmrVCoOEVEYBB3Tp09/W6ugvb39a0REURQ1d3V0LO/bL4mXX3557LFwjmmaz7W3t3+nu7v7mc7Ozk9Wxy3L+mm5XDZcx3nO87yHAeDAgQMZ3/d/Xy6X9/iet7xP5klhGP7MNM0noygyLMv6NgDEcbxDxDGZpnn+X8LuA0prmqadLstytQWwuTp+7ty5t+q6ngFgv97YeAeAIgDIsnx6f/5Zs2ZNliSpBgASopbt27dH/cipfD5/KwAIIf63vbNzMQCkUilp0qRJp/25C2xpaZnDOW8YNWrUPaqqFlRV/XKvY4qLJEm62vO8iUySXlZV9UsAkM2mfyGE2JvNZk8WRNwwjCUAQsbYdWEYLlcU5dyUJN3c2Ng4pFKpvJIQNdXX17903JxTNbYQAr7vbwaAtWvX1tXW198AAJ7nbZg/f/7+OI4VAJAk6W3tZ03TpjDWmyHiON7Xn+Z53hez2ew4IkJra+vrtm2bSZJAlmXU1NScPgD13hO1tbWX1NTUzCai23O53DmapnwcAHQ9O8fzvJ8PHz7cu+228+4Hkl+XSqVzAWliLpcbQkQ/yeVyQxljIxzH+UgixIaRI0f+ijH2xzCKWlpbW8dwzpuFEIW/hGOAAXRCp0+frsqy/CGgt11QLB7YAgBnnnnmzaqq1gNALpf7pBAiYX0ekCT2ofXr14+fO3duc68h9EnVczTO+du+Waqi3AH0FgEnn3zy8ilTphx8YVKp1J/tHCHERalUaimAvCzLTzIm39HS0nJxEAQHJEkaBQBXXfXjLIjVaprGK5VQC8PwWV3XnwNwShAEzkknnVRvm+bB75IsyyRJUpL0ayQeF+esWbXqNFmWR/YtdM+MGXP2dnR0ZDVN+0afscMwCAKJsbScSkFPp3VJknHyhAkzATQDgKIoUxljEEJACHHQOeVy+Val76+dcrnsUJIwIkrp6bSiKEpKSaVmvY81SX0XSqXSvxDRREVRPlYlWpZVm8/n7/3Dhg1XzvzoR19wXfd7KVk+lUnSOdlsdoNpmi+TEIsArCyXy9+VZfkVAL8g4OB3MkmSVMV1JSLqFELcSkQ1jDH3WDvnyGlNUaZFUQTOOSqVykYAkCTpK3EcZ6Mogm3bP8rX1jbk8vn0vr17rw/DEHEcg0nSKVURcRSN6ONHFEU7quOc8+s4jxGGobd169Zza/L5fL62NuM4zvNCCPA4HlooFEYf5ZoS9DbcwBjjMmOL+xO7u7v/B8DzI8aP98MwvEpRlDqvXH7RL5ffIKJR9fX1X5EVpTEIgnVJkhijRo26B0BZkuXHqjJkWf7Z6IkTzYaGhseTJGl2HOfKY+0YYACR4zjOc01NTUNd1xU7d+4MAKBUKj3R1NT0eKFQ4Dt27Dh4XPOrp258qq7uyufHjx8vjxs3LgZ6d/tNTU0LCoWC5Hle0t7e7vaNZ7Zs2XLZSy+95MiynNx0001eVc7jjz/+uXQ6rU+ZMkW66KKLjvaN3IDeFgHq6+sPO+ebOnWqAeDrnZ2dC7PZ7C2aps1jjN0ZRVGKMdYOAJqm3dSfhzFmAPhJ9b6uru7BfuT3E92DOBIcx/mW7/tvhGH4JBENO976DGIQgxjEIAYxiEEMYhCDOCb4f1OOkKdST6XpAAAAAElFTkSuQmCC" alt="">
							</div>
							<div class="col-md-12">
								<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAoCAMAAACVZWnNAAACi1BMVEX////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////9/f0AAAD8C4heAAAA2XRSTlP//v38+/n49fTz8vHw7+3s6+nm5eTj4uHg3t3c29rZ19bV1NPS0dDPzcvKycjGw8LBwL++vby7urm4t7azsbCvramopqWko6KhnpybmpmYl5aVlJOSkI+OjYyLiYiHhoWDgoGAf359e3p5eHd2dHNycXBubWtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUU9OTUxKSUhHRkVDQUA/Pjw6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCIhIB8eHRwbGhgXFhUUExIREA8ODQwLCgkIBwYFBAMCAZ0AJPAdLwAAA35JREFUSMfNlYlXTFEcx38xtlSyE4lkiyQlYy2DFJMKFTXKEsqMVJZMUVMjprSptCChJmkZQ9JipCa0jPYp989x33t31uaUdJzje86b+f3u+33Oefcz950BNIXA/wOnz9/kqMk6+/eTg6+Dfl5PDo6ZCvwxUZJe5ou5ayXpD+7/+AthyRj+MBlh7UmpiQpSCzFcR2p5fMqDn9qp0ZLLJ7gnLibXGsIRGDhpAt6D61TNUNE8jQ1XA3glXrHuGQPLqcmdZKZAp3KZPpxNLyUyTYIODqPXq5nGCZf2foG+HmtgkT68H2AagLMxPLwUzKYDnKEbBa58mImGR306uB5gBs8aoNwIlgCsDwFY0E81dXg5yYTtCwB2yBnglNGedwP4qWcApFDNFxbAmqetvcbwCoqL0CjTwjJcSCjhO+gpN1qAuc3209V6MKXrOarBn3cNHpsHYNGJ7uG2imqbFmlth+pgrMsRf7GJsttE8NBiAH+EeucDBNNz6gwfp+VzaLpQA2NdwO2t6Ash74KEtXBxPaML4lRvvrM1yuj0K6izH6SBz+OGRa4APZ14s2Ys5mKUkYwsAfhF4FEbvbfQqks7I9N/O7GyweYR5kYL9n+cwJn4nqdIwBckeeEqQQtjXRAoFAgEyS6MMtu5vjFpOZlXsQpIJvA+XNczr9Ys7SljdK1iykKgldnpHsSB2H6Ja3cCHMB1PqnjcB2mOwczO5GDlt3YQeCsLYdd8sjQM9dDm0WUBnzxnTh75WT97rZDzmXos9B/14YVds6+xQYnzCjZqxVo4piGy/Bmu/8MfrJ2J1s/u/da4l0t92SPE8eDQwx8E8Zkti1MECsVA6fCXEuDmEM08gQLy3ECdoMMPNTVY5hu1Sga7u4ZL90q9C//Jfsr1UYryspv5HhXK03AssjB1uraxgHUr5S3XBhAVQ3o7RtU2qZ811aLRqKCy2NzUZUKybzrTMB5jgXHihK9eY3xHLdzwmEk9hYFZt70y7l0OcT9tpgTXup6Lm1rREbQkSYTcJWoNLY9ITbU68zRLH4kanwY5fAWcUcbRBKvl25iFU8c8Tgghc9R36owAXf484peuYXXuL4Q8vLvIKlH6BXeI0HQjYyvJ4VSvs/ZkujgmNO1qYHcZlPCZJUISWtQB+rMaabacvT6xVAB3uGnHLUy9wfqk34vrkfFNf/0p5oovwHsiKq5WuY9gAAAAABJRU5ErkJggg==" alt="">
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>
<div id="hover-template" class="hidden-xs">
	<div class="container hidden-xs">
		<?php foreach ( $header_items as $key => $header_item ): ?>
			<?php $has_child =  isset($header_item['links']) ?>
			<?php if ( $has_child ): ?>
				<div class="row" data-id="<?=$key ?>">
					<?php subitem( $header_item['links'], $header_item['title'], $key ) ?>
				</div>
			<?php endif ?>
		<?php endforeach ?>
	</div>
</div>