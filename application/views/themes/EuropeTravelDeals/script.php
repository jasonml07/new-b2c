/*=================================
=            Bootstrap            =
=================================*/

(function($){
	$(document).ready(function(){
		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			event.preventDefault(); 
			event.stopPropagation(); 
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	});
})(jQuery);

/*=====  End of Bootstrap  ======*/


(function ($) {
	var $header    = $( 'header.custom-theme-header' );
	var $headerNav = $header.find( '#europeholidays-menu-collapse' );
	var $menuTemplate = $header.find('#hover-template');

	var MOUSE = {
		init: function () {
			$( 'body' ).mousemove(function ( event ) {
				if ( event.pageY > 400 && HEADER.eleContainer.hasClass('hovered') )
				{
					HEADER.hide();
					TOP_HEADER.clearHovered();
					MENU_TEMPLATE.clear();
				} else if ( event.pageY > 130 && ! HEADER.eleContainer.hasClass('hovered') ) {
					TOP_HEADER.clearHovered();
					MENU_TEMPLATE.clear();
				}
			});
		},
	};
	var HEADER = {
		eleContainer: $header,

		init: function () {
			this.eleContainer.after('<header class="header-clone"></header>');
		},
		/**
		 * [show description]
		 * @return {[type]} [description]
		 */
		show: function () {
			this.eleContainer.addClass('hovered');
			setTimeout(function () {
				HEADER.eleContainer.next('.header-clone').hide();
			}, 100);
		},
		/**
		 * [show description]
		 * @return {[type]} [description]
		 */
		hide: function () {
			this.eleContainer.removeClass('hovered');
			setTimeout(function () {
				HEADER.eleContainer.next('.header-clone').show();
			}, 100);
		}
	};
	var TOP_HEADER = {
		eleContainer: $headerNav,

		init: function () {
			this.eleContainer.find('a[data-id]').click(function () {
				var $this = $( this );
				var id    = $this.data('id');
				var hasChild = $this.parent('li').hasClass('has-child');
				TOP_HEADER.clearHovered();
				MENU_TEMPLATE.clear();

				$this.addClass('hovered');
				HEADER.hide();
				if ( hasChild )
				{
					HEADER.show();
					MENU_TEMPLATE.show( id );
				}
			});
		},
		/**
		 * [clearHovered description]
		 * @return {[type]} [description]
		 */
		clearHovered: function () {
			this.eleContainer.find('a[data-id]').removeClass('hovered');
			// this.eleContainer.find('li.open').removeClass('open');
		},
	};
	var MENU_TEMPLATE = {
		eleContainer: $menuTemplate,

		init: function () {
			this.clear();
		},
		/**
		 * [show description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		show: function ( id ) {
			this.stop( id );
			this.eleContainer.find('div[data-id="' + id + '"]').fadeIn();
		},
		/**
		 * [hide description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		hide: function ( id ) {
			this.stop( id );
			this.eleContainer.find('div[data-id="' + id + '"]').fadeOut();
		},
		/**
		 * [stop description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		stop: function ( id ) {
			this.eleContainer.find('div[data-id="' + id + '"]').stop();
		},
		/**
		 * [clear description]
		 * @return {[type]} [description]
		 */
		clear: function () {
			this.eleContainer.find('div[data-id]').stop();
			this.eleContainer.find('div[data-id]').fadeOut();
		}
	};

	var ZSIQ = {
		interval: null,
		eleContainer: null,
		eleMessageBtn: $( '[trigger-message]' ),

        init: function () {
        	this.interval = setInterval(ZSIQ.find, 3000);
        },

        find: function () {
        	let $ele = $( '#zsiqbtn' );
        	if ( $ele.length > 0 )
        	{
        		ZSIQ.eleContainer = $ele;
        	}
        	if ( ZSIQ.eleContainer != null ) {
        		clearInterval( ZSIQ.interval );
        		ZSIQ.event();
        	}
        },

        event: function () {
        	ZSIQ.eleMessageBtn.click(function () {
        		console.info('CLicked');
        		ZSIQ.eleContainer.click();
        	});
        }
    };

	init();
	function init () {
		TOP_HEADER.init();
		MENU_TEMPLATE.init();
		HEADER.init();
		MOUSE.init();
		ZSIQ.init();
	};
})(jQuery);