<?php
$items = array(
	'about-us' => array(
		'title' => 'about us',
		'links' => array(
			array('title' => 'Europe Holidays is a division of Magic Tours International Pty Ltd.'),
			array('title' => '<span class="fa fa-map-marker"></span>&nbsp;&#09;&nbsp;&#09;Suite 2, 66 Appel Street, Surfers Paradise, QLD Australia'),
			array('title' => '<span class="fa fa-phone"></span>&nbsp;&#09;&nbsp;&#09;<span class="AVANSERnumber">(07) 5526 7467</span>'),
		)
	),
	'europe' => array(
		'title' => 'europe',
		'links' => array(
			array('title' => 'Austria',        'link' => 'https://www.europeholidays.com.au/index.php/destinations/europe/austria'),
			array('title' => 'The Baltics',    'link' => 'https://www.europeholidays.com.au/index.php/destinations/europe/the-baltics'),
			array('title' => 'Czech Republic', 'link' => 'https://www.europeholidays.com.au/index.php/destinations/europe/czech-republic'),
			array('title' => 'Russia',         'link' => 'https://www.europeholidays.com.au/index.php/destinations/europe/russia'),
			array('title' => 'Slovenia',       'link' => 'https://www.europeholidays.com.au/index.php/destinations/europe/slovenia'),
			array('title' => 'Poland',         'link' => 'https://www.europeholidays.com.au/index.php/destinations/europe/poland'),
		)
	),
	'mediterranean' => array(
		'title' => 'Mediterranean',
		'links' => array(
			array('title' => 'Bosnia &amp; Herzegovina',      'link' => 'https://www.europeholidays.com.au/index.php/destinations/the-mediterranean/bosnia-herzegovina'),
			array('title' => 'Croatia',                       'link' => 'https://www.europeholidays.com.au/index.php/destinations/the-mediterranean/croatia'),
			array('title' => 'Italy',                         'link' => 'https://www.europeholidays.com.au/index.php/destinations/the-mediterranean/italy'),
			array('title' => 'Turkey',                        'link' => 'https://www.europeholidays.com.au/index.php/destinations/the-mediterranean/turkey'),
			array('title' => 'Spain, Portugal &amp; Morocco', 'link' => 'https://www.europeholidays.com.au/index.php/destinations/the-mediterranean/spain-portugal-morocco'),			
		)
	),
	'support' => array(
		'title' => 'Support',
		'links' => array(
			array('title' => 'Terms and Conditions', 'link' => 'https://www.europeholidays.com.au/index.php/terms-and-conditions'),
			array('title' => 'Privacy Policy',       'link' => 'https://www.europeholidays.com.au/index.php/privacy-policy'),
			array('title' => 'Testimonials',         'link' => '#'),
			array('title' => 'Contact Us',           'link' => 'https://www.europeholidays.com.au/index.php/contact-us'),
			array('title' => 'About Us',             'link' => 'https://www.europeholidays.com.au/index.php/about-europe-holidays?id=22'),
		)
	),
);
?>

<div class="container">
	<?php $count = 1; ?>
	<?php foreach ( $items as $key => $item ): ?>
		<div class="col-sm-6 col-md-3">
			<div class="page-header">
				<h6><?=$item['title'] ?></h6>
			</div>
			<ul>
				<?php $subItems = $item['links'] ?>
				<?php foreach ( $subItems as $subKey => $subItem ): ?>
					<li><?=( isset( $subItem['link'] ) ? '<a href="' . $subItem['link'] . '" title="' . $subItem['title'] . '">' . $subItem['title'] . '</a>' : '<p>' . $subItem['title'] . '</p>' ) ?></li>
				<?php endforeach ?>
			</ul>
		</div>
		<?php if ( ($count % 2) == 0 ): ?>
			<div class="visible-sm clearfix"></div>
		<?php endif ?>
		<?php $count++; ?>
	<?php endforeach ?>
</div>
<div class="copy-right">
	<div class="container text-uppercase">copyright &copy; <?=date('Y',strtotime('now'))?>.  Europe Holidays. A Client of Select Marketing &amp; Web Solutions.  </div>
</div>