#zsiqbtn{
	position: fixed;
  bottom: 10px;
  left: 10px;
}

/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 300;
  src: local('Raleway Light'), local('Raleway-Light'), url(https://fonts.gstatic.com/s/raleway/v11/-_Ctzj9b56b8RgXW8FArifk_vArhqVIZ0nv9q090hN8.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
}

/*=================================
=            Bootstrap            =
=================================*/

.marginBottom-0 {margin-bottom:0;}

.dropdown-submenu{position:relative;}
.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
.dropdown-submenu:hover>a:after{border-left-color:#555;}
.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
.dropdown-toggle > .caret { display: none; }

/*=====  End of Bootstrap  ======*/


footer, header { font-family: 'Raleway'; }

/*==============================
=            Header            =
==============================*/

.custom-theme-header
{
	height: 0px;
	-webkit-transition: 300ms ease height;
	-o-transition     : 300ms ease height;
	transition        : 300ms ease height;
}
.custom-theme-header.hovered
{
	height: 430px;
	-webkit-transition: 300ms ease height;
	-o-transition     : 300ms ease height;
	transition        : 300ms ease height;
}
header.header-clone { height: 130px; }

.custom-theme-header { background-image: radial-gradient(circle farthest-side at center bottom,#333333,#051125 125%); }
.custom-theme-header .navbar-nav > li { position: relative; top: 0px; }
.custom-theme-header .navbar-nav > li.open > a, .custom-theme-header .navbar-nav > li.open > a:focus, .custom-theme-header .navbar-nav > li.active > a { background-color: transparent !important; }
.custom-theme-header .navbar-nav > li > a.hovered, .custom-theme-header .navbar-nav > li.open > a, .custom-theme-header .navbar-nav > li > a:hover, .custom-theme-header .navbar-nav > li.active > a { color: #fe0000 !important; border-bottom: 4px solid #fe0000; }
.custom-theme-header .navbar-nav > li > a:focus, .custom-theme-header .navbar-nav > li > a { font-size: 20px; padding: 53px 20px; color: #FFF; }
.custom-theme-header .navbar-nav > li.contact-item { margin-top: 15px; }
.custom-theme-header .navbar-nav > li.contact-item .contact-social { width: 183.17px; padding-left: 0px; padding-right: 10px; border-right: 1px solid #BDBDBD; margin-right: 10px; }
.custom-theme-header .navbar-nav > li.contact-item .contact-social > div { padding: 0px; }
.custom-theme-header .navbar-nav > li.contact-item .contact-social .number > p { font-size: 28px; display: inline; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; color: #fe0000; }
.custom-theme-header .navbar-nav > li.contact-item .others { padding: 0px; height: 87px; width: 105px; margin-top: 15px; }
.custom-theme-header .navbar-nav > li.contact-item .others > div { padding: 0px; }
.custom-theme-header.hovered > nav { border-bottom: 1px solid #696969; background: transparent; }
.custom-theme-header > nav { background-color: #333333; border-radius: 0px; border: none; }
.custom-theme-header .navbar-brand { width: 156px; height: 130px; padding: 0px; margin-left: 50px !important;- }

.custom-theme-header .navbar-nav > li > a.hovered > .header-item-icon
{
	-webkit-transform: rotate(-180deg);
	-ms-transform    : rotate(-180deg);
	-o-transform     : rotate(-180deg);
	transform        : rotate(-180deg);

	-webkit-transition: 300ms ease -webkit-transform;
	-o-transition     : 300ms ease -o-transform;
	transition        : 300ms ease transform;
}
.custom-theme-header .navbar-nav > li > a > .header-item-icon
{
	-webkit-transform: rotate(-360deg);
	-ms-transform    : rotate(-360deg);
	-o-transform     : rotate(-360deg);
	transform        : rotate(-360deg);

	-webkit-transition: 300ms ease -webkit-transform;
	-o-transition     : 300ms ease -o-transform;
	transition        : 300ms ease transform;
}

#hover-template .page-header { border-bottom-color: #FFF; margin-top: 10px; }
#hover-template .page-header > h4 { color: #FFF; font-family: 'Raleway'; text-transform: uppercase; margin: 0px; font-size: 1.3em; }
#hover-template .page-header ~ ul { width: 100%; list-style: none; padding-left: 0; padding-right: 0;columns: 3; display: inline-block;}
#hover-template .page-header ~ ul li { padding: 5px 0px; }
#hover-template .page-header ~ ul a { color: #FFF; font-size: 1em; }
#hover-template .page-header ~ ul a:hover { text-decoration: underline; }

/*=====  End of Header  ======*/

/*==============================
=            Footer            =
==============================*/

.custom-theme-footer { border-top: 1px solid #E0E0E0; }
.custom-theme-footer .page-header { border: none; margin-bottom: 10px; }
.custom-theme-footer .page-header > h6 { font-family: 'Raleway'; text-transform: uppercase; letter-spacing: 2px; }
.custom-theme-footer .page-header ~ ul { list-style: none; padding: 0px; }
.custom-theme-footer .page-header ~ ul li { padding: 5px 0px; }
.custom-theme-footer .page-header ~ ul li > a { color: #262626; }
.custom-theme-footer .page-header ~ ul li > a:hover { color: #fe0000; }
.custom-theme-footer .copy-right { background: rgb(254, 0, 0) none repeat scroll 0 0; margin-top: 40px; }
.custom-theme-footer .copy-right .container { padding: 40px 0px; color: #FFF; font-size: 12px; }

/*=====  End of Footer  ======*/


/*=============================
=            Media            =
=============================*/

@media ( min-width: 768px )
{
	.dropdown-menu { visibility: hidden; };
}

@media ( max-width: 1287px )
{
	.custom-theme-header .navbar-nav > li.contact-item .others { display: none; }
	.custom-theme-header .navbar-nav > li.contact-item .contact-social { border-right: none; }
}
@media ( max-width: 1182px )
{
	.custom-theme-header .navbar-nav > li.contact-item .contact-social { display: none; }
}
@media ( min-width: 768px ) and ( max-width: 991px )
{
	.custom-theme-header .navbar-nav > li > a:focus, .custom-theme-header .navbar-nav > li > a { font-size: 17px; padding: 56px 9px; margin-bottom: -6px; margin-top: 1px; }
	.custom-theme-header .navbar-brand { margin: 0px; margin-left: 0px !important; }
}
@media ( max-width: 767px )
{
	.dropdown-submenu>a:after { display: none; }
	.dropdown-toggle > .caret:before { content: "\f077"; }
	.dropdown-toggle > .caret
	{
		font          : normal normal normal 14px/1 FontAwesome;
		border        : none;
		display       : inline-block;
		font-size     : inherit;
		text-rendering: auto;
		width: 17px;
		height: 17px;
		position: relative;
		left: 3px;
	}
	.custom-theme-header .navbar-nav > li { top: 0px; }
	.custom-theme-header .navbar-nav > li > a:focus, .custom-theme-header .navbar-nav > li > a { padding: 10px; }
	.custom-theme-header .navbar-header { height: 130px; }
	.custom-theme-header .navbar-header, #europeholidays-menu-collapse { background-color: #212121; }
	.custom-theme-header .navbar-nav > li > a > .header-item-icon { float: right; }
	.custom-theme-header .navbar-nav > li > a.hovered, .custom-theme-header .navbar-nav > li.open > a { padding-bottom: 10px; }
	.custom-theme-header .navbar-nav .dropdown-menu a { color: #FFF !important; font-size: 17px; }
	.custom-theme-header .navbar-nav .dropdown-submenu .dropdown-menu { text-indent: 15px; }

	.custom-theme-header .dropdown-submenu.open > a > .caret,
	.custom-theme-header .navbar-nav > li.has-child.open > a > .header-item-icon
	{
		-webkit-transform: rotate(-180deg);
		-ms-transform    : rotate(-180deg);
		-o-transform     : rotate(-180deg);
		transform        : rotate(-180deg);

		-webkit-transition: 300ms ease -webkit-transform;
		-o-transition     : 300ms ease -o-transform;
		transition        : 300ms ease transform;
	}
	.custom-theme-header .dropdown-submenu.open > a { border-bottom: 2px solid; }

	.custom-theme-header .dropdown-submenu > a > .caret,
	.custom-theme-header .navbar-nav > li.has-child > a > .header-item-icon
	{
		-webkit-transform: rotate(-360deg);
		-ms-transform    : rotate(-360deg);
		-o-transform     : rotate(-360deg);
		transform        : rotate(-360deg);

		-webkit-transition: 300ms ease -webkit-transform;
		-o-transition     : 300ms ease -o-transform;
		transition        : 300ms ease transform;
	}
	.custom-theme-header .navbar-nav > li.has-child:not(.open) > a
	{
		color: #FFF !important;
		border-bottom: none;
	}
	.custom-theme-header.hovered { height: 0px; }
	button[data-target="#europeholidays-menu-collapse"]
	{
		position: relative;
		top: 50%;
		border: none;
		margin-top: 0px;
		margin-bottom: 0px;
		-webkit-transform: -webkit-translateY(-50%);
		-ms-transform    : -ms-translateY(-50%);
		-o-transform     : -o-translateY(-50%);
		transform        : translateY(-50%);
	}
	.custom-theme-header button[data-target="#europeholidays-menu-collapse"] > span.icon-bar { background-color: #FFF !important; width: 25px; }
	.custom-theme-header .navbar-toggle .icon-bar+.icon-bar { margin-top: 5px; }
	.custom-theme-header .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover { background: transparent; }
	.custom-theme-header button[data-target="#europeholidays-menu-collapse"][aria-expanded="true"] span.icon-bar:nth-child(2)
	{
	    transform: rotate(45deg);

	    -webkit-transition: 300ms ease -webkit-transform;
		-o-transition     : 300ms ease -o-transform;
		transition        : 300ms ease transform;
	}
	.custom-theme-header button[data-target="#europeholidays-menu-collapse"][aria-expanded="true"] span.icon-bar:nth-child(3)
	{
	    transform: rotate(-45deg);
	    top: -6px;
    	position: relative;

    	-webkit-transition: 300ms ease -webkit-transform;
		-o-transition     : 300ms ease -o-transform;
		transition        : 300ms ease transform;
	}
	.custom-theme-header button[data-target="#europeholidays-menu-collapse"][aria-expanded="false"] span.icon-bar:nth-child(2)
	{
	    transform: rotate(0deg);

	    -webkit-transition: 300ms ease -webkit-transform;
		-o-transition     : 300ms ease -o-transform;
		transition        : 300ms ease transform;
	}
	.custom-theme-header button[data-target="#europeholidays-menu-collapse"][aria-expanded="false"] span.icon-bar:nth-child(3)
	{
	    transform: rotate(0deg);
	    top: 0px;
    	position: relative;

    	-webkit-transition: 300ms ease all;
		-o-transition     : 300ms ease all;
		transition        : 300ms ease all;
	}
}
/*=====  End of Media  ======*/
[data-id="home"]{
	font-size: 2em !important;
	padding-bottom: 45px !important;
}

/*===================================
=            Adjustments            =
===================================*/

.btn-danger { background-color: #fe0000; }
.btn-success, .btn-success:hover { background-color: #2c2c2c; border-color: #2c2c2c; }
.ui-datepicker .calendar-product.ui-datepicker-current-day .calendar-price, .ui-datepicker .calendar-product.ui-datepicker-current-day .fa-usd { color: #2c2c2c !important; }
/*.ui-datepicker .calendar-product.ui-datepicker-current-day .calendar-indicator, .ui-datepicker .calendar-product.ui-datepicker-current-day { border-color: #2c2c2c transparent transparent transparent; }*/
.ui-datepicker .calendar-product.ui-datepicker-current-day { border-color: #2c2c2c; }
.ui-datepicker .calendar-on-range { border: 1px solid #2c2c2c; }
.ui-datepicker .calendar-product { border-color: #fe0000; }
.ui-datepicker .calendar-product .calendar-price { color: #fe0000; }
.panel-primary>.panel-heading { background-image: radial-gradient(circle farthest-side at center bottom,#333333,#051125 125%); }
.text-danger { color: #fe0000; }

/*=====  End of Adjustments  ======*/


div#packages-container .form-group > label{
	padding-left: 0;
	padding-right: 0;
}