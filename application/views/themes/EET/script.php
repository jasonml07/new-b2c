(function($){
	// $('[data-submenu]').submenupicker();
	 $('.dropdown').on('show.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(100);
  });

  // Add slideUp animation to Bootstrap dropdown when collapsing.
  $('.dropdown').on('hide.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100);
  });

  var $container=$('div.container');
  var $links=$('li.has-child');
  dropdown_position();
  /*$(window).resize(function(){

  });*/
  
  function dropdown_position(){
  	$links.click(function(){
	  	var $this=$(this);
	  	var $anchor=$this.find('> a');
	  	var $menu=$this.find('> .dropdown-menu');
	  	var anchorWidth=$anchor.outerWidth();
	  	var anchorPosition=$anchor.offset().left;
	  	var menuWidth=$menu.outerWidth();

	  	var left=($container.outerWidth()-anchorPosition)+56;
	  	var right=left-(anchorWidth/2)-(menuWidth/2);
	  	$menu.css({right:right});
	  })
  }
})(jQuery);