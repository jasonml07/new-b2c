#zsiqbtn{
	position: fixed;
  bottom: 10px;
  left: 10px;
}

.navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover{
  background-color: transparent;
}
nav #easterneurotours-menu-wrapper{
	background: #027fba;
}
@media (max-width: 767px) {
	.navbar-brand {
		padding: 0px;
	}
	nav #easterneurotours-menu-wrapper .yamm-content{
		padding: 0px 30px;
	}
	nav #easterneurotours-menu-wrapper .header-item-icon{
		float: right;
	}
	nav #easterneurotours-menu-wrapper .navbar-nav > li>a{
		padding-top: 5px;
		padding-bottom: 5px;
	}
	nav #easterneurotours-menu-wrapper h6,nav #easterneurotours-menu-wrapper a{
		color: #FFF;
	}
}
@media (min-width: 768px){
	nav .navbar-brand {
		overflow: hidden;
		height: 100%;
		padding: 8px;
	}
	.nav.navbar-nav>li>a,
	.nav.navbar-nav>li>a:focus,
	.nav.navbar-nav>li>a:hover{
		/*font-weight: bold;*/
    /*text-transform: uppercase;*/
		padding-top: 30px;
		padding-bottom: 30px;
		padding-left: 15px;
		padding-right: 15px;
		font-size: 1.1em;
		color: #FFF !important;
	}
	.dropdown-submenu>a:after{content: none;}
	.dropdown-submenu>a.dropdown-toggle:after {
			content: '';
	    float: right;
	    margin-top: 6px;
	    margin-right: -10px;
	    border-left: 4px dashed;
	    border-top: 4px solid transparent;
	    border-bottom: 4px solid transparent;
	}
}
.nav.navbar-nav >li>a{
	font-weight: bold;
	text-transform: uppercase;
}
.yamm-content h6{
	font-size: .8em;
}
.yamm-content ul{
	list-style: none;
	padding-left: 10px;
}
.yamm-content ul>li>a:hover{
	color: #5999e8;
}
.yamm-content ul>li>a{
	color: #777;
	font-size: .9em;
}
.yamm-content ul>li{
	padding-bottom: 7px;
}
.yamm-content ul>li:last-child{
	padding-bottom: 0px;
}

#page-wrap{
	padding-bottom: 100px;
}

footer .container-fluid, footer a{
	background: #f1f1f1;
	color: #999;
}
footer h6{
	color: #888;
}
footer ul{
	list-style: none;
	padding-left: 19px;
}
footer li{
	padding-bottom: 7px;
}
footer a,footer p{
	cursor: pointer;
}
footer a:hover,footer p:hover {
	webkit-transition: color 0.2s linear;
  -moz-transition: color 0.2s linear;
  -o-transition: color 0.2s linear;
  transition: color 0.2s linear;
	color: #111;
}
footer .copy-right{
	padding: 50px;
}

/* .dropdown .dropdown-menu{
  display: block;
  opacity: 0;
	-webkit-transition-delay: 1s;
	-o-transition-delay: 1s;
	transition-delay: 1s;
  -moz-transition:    all 300ms ease;
  -webkit-transition: all 300ms ease;
  -o-transition:      all 300ms ease;
  -ms-transition:     all 300ms ease;
  transition:         all 300ms ease;
}
.dropdown.open .dropdown-menu {
  display: block;
  opacity: 1;
} */
.social-links {
	height: 40px;
}
.top-header{
	padding: 10px;
}
.top-header .contact-information .wrapper{
	display: inline-block;
	font-size: 1.2em;
	padding-right: 10px;
}