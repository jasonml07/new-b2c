<?php
$base_url='https://www.easterneurotours.com.au/';
$header_items = array(
	'home' => array('title' => 'Home', 'link' => $base_url),
	'destinations' => array('title' => 'Destinations', 'links' => array(
		array(
			'title' => 'The Mediterranean',
			'links' => array(
				array('title' => 'Croatia',                 'link' => $base_url.'index.php/destinations/the-mediterranean/croatia'),
				array('title' => 'Greece',                  'link' => $base_url.'index.php/destinations/the-mediterranean/greece'),
				array('title' => 'Israel, Jordan and Egypt','link' => $base_url.'index.php/destinations/the-mediterranean/israel-jordan-and-egypt'),
				array('title' => 'Italy',                   'link' => $base_url.'index.php/destinations/the-mediterranean/italy'),
				array('title' => 'Morocco',                 'link' => $base_url.'index.php/destinations/the-mediterranean/morocco'),
				array('title' => 'Portugal',                'link' => $base_url.'index.php/destinations/the-mediterranean/portugal'),
				array('title' => 'Spain',                   'link' => $base_url.'index.php/destinations/the-mediterranean/spain'),
				array('title' => 'Turkey',                  'link' => $base_url.'index.php/destinations/the-mediterranean/turkey'),
			)
		),
		array(
			'title' => 'Europe',
			'links' => array(
				array('title' => 'Austria',             'link' => $base_url.'index.php/destinations/europe/austria'),
				array('title' => 'Baltics',             'link' => $base_url.'index.php/destinations/europe/baltics'),
				array('title' => 'Benelux',             'link' => $base_url.'index.php/destinations/europe/benelux'),
				array('title' => 'Bosnia & Herzegovina','link' => $base_url.'index.php/destinations/europe/bosnia-herzegovina'),
				array('title' => 'Bulgaria',            'link' => $base_url.'index.php/destinations/europe/bulgaria'),
				array('title' => 'Czech Republic',      'link' => $base_url.'index.php/destinations/europe/czech-republic'),
				array('title' => 'France',              'link' => $base_url.'index.php/destinations/europe/france'),
				array('title' => 'Germany',             'link' => $base_url.'index.php/destinations/europe/germany'),
				array('title' => 'Hungary',             'link' => $base_url.'index.php/destinations/europe/hungary'),
				array('title' => 'Iceland',             'link' => $base_url.'index.php/destinations/europe/iceland'),
				array('title' => 'Ireland',             'link' => $base_url.'index.php/destinations/europe/ireland-britain'),
				array('title' => 'Poland',              'link' => $base_url.'index.php/destinations/europe/poland'),
				array('title' => 'Romania',             'link' => $base_url.'index.php/destinations/europe/romania'),
				array('title' => 'Russia',              'link' => $base_url.'index.php/destinations/europe/russia'),
				array('title' => 'Scandinavia',         'link' => $base_url.'index.php/destinations/europe/scandinavia'),
				array('title' => 'Serbia',              'link' => $base_url.'index.php/destinations/europe/serbia'),
				array('title' => 'Slovenia',            'link' => $base_url.'index.php/destinations/europe/slovenia'),
				array('title' => 'Scotland',            'link' => $base_url.'index.php/destinations/europe/scotland'),
				array('title' => 'Switzerland',         'link' => $base_url.'index.php/destinations/europe/switzerland'),
				array('title' => 'Ukraine',             'link' => $base_url.'index.php/destinations/europe/ukraine'),
				array('title' => 'United Kingdom',      'link' => $base_url.'index.php/destinations/europe/britain'),
				array('title' => 'Bulgaria',            'link' => $base_url.'index.php/destinations/europe/united-kingdom'),
			)
		),
		array(
			'title' => 'The Middle East',
			'links' => array(
				array('title' => 'United Arab Emirates','link' => $base_url.'index.php/destinations/the-middle-east/united-arab-emirates'),
			)
		),
		array(
			'title' => 'Mongolia',
			'links' => array(
			)
		),
	)),
	'accommodation' => array('title' => 'Accommodation', 'links' => array(
		array(
			'title' => 'Apartments',
			'links' => array(
				array('title' => 'Austria',            'link' => $base_url.'index.php/accommodation/apartments/austria'),
				array('title' => 'Czech Republic',     'link' => $base_url.'index.php/accommodation/apartments/czech-republic'),
				array('title' => 'Denmark',            'link' => $base_url.'index.php/accommodation/apartments/denmark'),
				array('title' => 'France',             'link' => $base_url.'index.php/accommodation/apartments/france'),
				array('title' => 'Germany',            'link' => $base_url.'index.php/accommodation/apartments/germany'),
				array('title' => 'Great Britain',      'link' => $base_url.'index.php/accommodation/apartments/great-britain'),
				array('title' => 'Hungary',            'link' => $base_url.'index.php/accommodation/apartments/hungary'),
				array('title' => 'Italy',              'link' => $base_url.'index.php/accommodation/apartments/italy'),
				array('title' => 'Poland',             'link' => $base_url.'index.php/accommodation/apartments/poland'),
				array('title' => 'Republic of Ireland','link' => $base_url.'index.php/accommodation/apartments/republic-of-ireland'),
				array('title' => 'Slovakia',           'link' => $base_url.'index.php/accommodation/apartments/slovakia'),
				array('title' => 'Spain',              'link' => $base_url.'index.php/accommodation/apartments/spain'),
				array('title' => 'Switzerland',        'link' => $base_url.'index.php/accommodation/apartments/switzerland'),
			)
		),
		array(
			'title' => 'Hotels',
			'links' => array(
				array('title' => 'Book Hotels Online',       'link' => 'http://www.travelrez.com.au/'),
			)
		),
	)),
	'cc-payments' => array('title' => 'CC Payments',    'link' => 'https://www.payway.com.au/MakePayment?BillerCode=172817'),
	'agent-flights' => array('title' => 'Agent Flights','link' => 'https://eet.agentflights.com.au/searchAir.do'),
	'contact-us' => array('title' => 'Contact Us',      'link' => $base_url.'index.php/contact-us')
);
/**
 * [submenu description]
 * @param  [type] $items [description]
 * @return [type]        [description]
 */
function YammMenu ( $items, $title, $id, $link = '#' )
{
	# Code here
	?>
	<!-- <div class="row"></div> -->
	<a href="<?=$id ?>" data-id="<?=$id ?>" class="dropdown-toggle" data-submenu data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$title ?> <span class="header-item-icon fa fa-chevron-down"></span></a>
	<ul class="dropdown-menu">
		<li>
			<div class="yamm-content" id="<?=$id?>">
				<div class="row">
					<?php $size=count($items); ?>
					<?php foreach ( $items as $key => $item ): ?>
						<div class="col-md-<?=(12/$size)?>">
							<h6 class="text-uppercase"><?=$item['title'] ?></h6>
							<ul>
								<?php foreach ( $item['links'] as $key => $itemLink ): ?>
									<li><a href="<?=$itemLink['link'] ?>"><?=$itemLink['title'] ?></a></li>
								<?php endforeach ?>
							</ul>
						</div>
					<?php endforeach ?>
				</div>
			</div>
		</li>
	</ul>
	<?php
} // end of submenu/**
/*
 * [submenu description]
 * @param  [type] $items [description]
 * @return [type]        [description]
 */
function submenu ( $items, $title, $id, $link = '#' )
{
	# Code here
	?>
	<a href="<?=$link ?>" data-id="<?=$id ?>" class="dropdown-toggle" data-submenu data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$title ?> <span class="header-item-icon fa fa-chevron-down"></span></a>
	<ul class="dropdown-menu">
		<?php foreach ( $items as $key => $item ): ?>
			<li class="dropdown-submenu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-submenu><?=$item['title'] ?></a>

				<ul class="dropdown-menu">
					<?php foreach ( $item['links'] as $key => $itemLink ): ?>
						<li class="dropdown-submenu"><a href="<?=$itemLink['link'] ?>"><?=$itemLink['title'] ?></a></li>
					<?php endforeach ?>
				</ul>
			</li>
		<?php endforeach ?>
	</ul>
	<?php
} // end of submenu
/**
 * [subitem description]
 * @param  [type] $items [description]
 * @param  [type] $tile  [description]
 * @param  string $link  [description]
 * @return [type]        [description]
 */
function subitem ( $items, $tile, $id, $link = '#' )
{
	# Code here
	$size = 12 / count($items);

	foreach ( $items as $key => $item )
	{
		?>
			<div class="col-sm-<?=$size ?> col-md-<?=$size ?> template-subitem" data-id="<?=$id ?>">
				<div class="page-header">
					<h4 class="text-center"><?=$item['title'] ?></h4>
				</div>
				<ul>
					<?php foreach ( $item['links'] as $subKey => $subItem ): ?>
						<li style="font-size: .9em;"><a href="<?=$subItem['link'] ?>"><?=$subItem['title'] ?></a></li>
					<?php endforeach ?>
				</ul>
			</div>
		<?php
	}
} // end of subitem
?>

<nav class="navbar yamm navbar-default navbar-static-top">
	<div class="container-fluid">
		<div class="container">
			<div class="social-links pull-left">
			  <iframe frameborder="0" allowtransparency="true" scrolling="no" style="position: absolute; top: 10px; left: 10px;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FEasternEurotours&amp;width=97&amp;layout=button&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=65&amp;appId=341368003149"></iframe>
			  <a href="https://www.instagram.com/eastern_eurotours/?ref=badge" style="position: absolute;top: 8px;left: 110px;"><img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" class="image1"> </a>
			</div>
			<div class="wrapper pull-right tz-plugin top-header">
	      <div class="pull-left contact-information">
	        <div class="wrapper">
	          <i class="fa fa-phone"></i>
	           1800 242 353
	        </div>
	        <div class="wrapper">
	          <i class="fa fa-envelope"></i>
	           <a href="mailto:info@easterneurotours.com.au?subject=Website%20enquiry">EMAIL US</a>
	        </div>
	      </div>
	      <!-- <strong class="pull-left"></strong>
	      <button type="button" tz-login="" class="btn btn-mini btn-primary" title="Login your TravelRez Account">Login</button> -->
	    </div>
		</div>
	</div>
	<div class="container-fluid" id="easterneurotours-menu-wrapper">
		<div class="container">
			<div class="navbar-header">
				<button type="button" data-toggle="collapse" data-target="#easterneurotours-menu" aria-expanded="false" class="navbar-toggle collapsed">
					<span class="sr-only">Toggle Mobile Menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<img class="navbar-brand" src="<?=base_url()?>/assets/images/EET/logo.png" alt="Easterneurotours">
			</div>
			
			<div class="collapse navbar-collapse" id="easterneurotours-menu">
				<ul class="nav navbar-nav navbar-right">
					<?php foreach ( $header_items as $key => $header_item ): ?>
						<?php $has_child =  isset($header_item['links']) ?>
						<li <?=$has_child ? 'class="has-child dropdown"' : '' ?>>
							<?php if ( $has_child ): ?>
								<?php YammMenu( $header_item['links'], $header_item['title'], $key ) ?>
							<?php else: ?>
								<a href="<?=$header_item['link'] ?>" data-id="<?=$key ?>"><?=$header_item['title'] ?></a>
							<?php endif ?>
						</li>
					<?php endforeach ?>
				</ul>
			</div>
		</div>
	</div>
</nav>