<?php
$items = array(
	'who-we-Are' => array(
		'title' => 'Who We Are',
		'links' => array(
			array('title' => 'About Us'),
			array('title' => 'Testimonials'),
			array('title' => 'Contact'),
			array('title' => 'Partner Programs'),
			array('title' => 'The Real Dollar Club'),
		)
	),
	'popular-destinations' => array(
		'title' => 'Popular Destinations',
		'links' => array(
			array('title' => 'Croatia'),
			array('title' => 'France'),
			array('title' => 'Italy'),
			array('title' => 'Germany'),
			array('title' => 'Greece'),
			array('title' => 'Russia'),
		)
	),
	'resources' => array(
		'title' => 'Resources',
		'links' => array(
			array('title' => 'Terms and Conditions',      'link' => 'http://selectmarketingprojects.net.au/easterneurotours/index.php/terms-and-conditions'),
			array('title' => 'Order our Brochure',),
			array('title' => 'Privacy Policy',                         'link' => 'http://selectmarketingprojects.net.au/easterneurotours/index.php/privacy-policy'),
			array('title' => 'TravelRez Clue Card',                        'link' => 'http://selectmarketingprojects.net.au/easterneurotours/images/EET/pdf/Clue-Card-flyer.pdf', 'attribute'=>'target="_blank" rel="noopener noreferrer"'),
			array('title' => 'Agent Flights T&C\'s', 'link' => 'http://selectmarketingprojects.net.au/easterneurotours/index.php/agent-flights-terms-and-conditions'),			
		)
	),
	'contact-us' => array(
		'title' => 'Contact Us',
		'links' => array(
			array('title' => 'Phone: (07) 5526 2855'),
			array('title' => 'Toll Free: 1800 242 353'),
			array('title' => 'Fax: (07) 5526 2944'),
			array('title' => 'Email: info@easterneurotours.com.au'),
		)
	),
);
?>

<div class="container-fluid">
	<div class="container"><?php $count = 1; ?>
		<?php foreach ( $items as $key => $item ): ?>
			<div class="col-sm-6 col-md-3">
				<div class="page-header">
					<h6><?=$item['title'] ?></h6>
				</div>
				<ul>
					<?php $subItems = $item['links'] ?>
					<?php foreach ( $subItems as $subKey => $subItem ): ?>
						<li><a href="<?=(!isset($subItem['link']))?'#':$subItem['link']?>" <?=(isset($subItem['link']))?'target="_blank"':''?> title="<?=$subItem['title']?>"><?=$subItem['title']?></a></li>
					<?php endforeach ?>
				</ul>
			</div>
			<?php if ( ($count % 2) == 0 ): ?>
				<div class="visible-sm clearfix"></div>
			<?php endif ?>
			<?php $count++; ?>
		<?php endforeach ?></div>
</div>
<div class="copy-right">
	<div class="container text-center">Eastern Eurotours © 1992 - 2017. A Client of Select Marketing & Web Solutions</div>
</div>