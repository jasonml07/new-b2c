$($('.container').get(0)).css({paddingTop: '100px'})

// function deselect(e) {
//   $('.trip-style.pop').slideFadeToggle(function() {
//     e.removeClass('selected');
//   });    
// }



$(function() {
	/**
	 * [toggleDestination description]
	 * @param  {Function} callback [description]
	 * @return {[type]}            [description]
	 */
	function toggleDestination(callback) {
		var $btn = $('#destination')
		var $dialog = $('.destination.pop')
		if($btn.hasClass('selected')) {
			$dialog.slideFadeToggle('swing', function(e) {
		    $btn.removeClass('selected');
		    if(callback) {
		    	callback()
		    }
		  });
		} else {
			$btn.addClass('selected');
			$dialog.slideFadeToggle('swing', function() {
		 		if(callback) {
		 			callback()
		 		}
		 	})
		} 
	}
	/**
	 * [toggleTripStyle description]
	 * @param  {Function} callback [description]
	 * @return {[type]}            [description]
	 */
	function toggleTripStyle(callback) {
		var $btn = $('#trip-style')
		var $dialog = $('.trip-style.pop')

		if($btn.hasClass('selected')) {
		  $dialog.slideFadeToggle('swing', function(e) {
		    $btn.removeClass('selected');
		    if(callback) {
		    	callback()
		    }
		  });
		} else {
			$btn.addClass('selected');
		 	$dialog.slideFadeToggle('swing', function() {
		 		if(callback) {
		 			callback()
		 		}
		 	})
		}
		 
	}
	/**
	 * [closeMenu description]
	 * @return {[type]} [description]
	 */
	function closeMenu() {
  	var $destination = $('#destination')
  	var $trip = $('#trip-style')

  	$destination.removeClass('selected')
  	$trip.removeClass('selected')
  	$('.messagepop').css({display: 'none'})
  }

  $('#destination').mouseenter(function() {
  	var $trip = $('#trip-style')
  	if($trip.hasClass('selected')) {
  		toggleTripStyle(function() {
  			toggleDestination()
  		})
  	} else {
  		toggleDestination()
  	}
    return false;
  });

  // $('.close').on('click', function() {
  //   deselect($('#destination'));
  //   return false;
  // });
  $('#trip-style').mouseenter(function() {
  	var $destination = $('#destination')
  	if($destination.hasClass('selected')) {
  		toggleDestination(function() {
  			toggleTripStyle()
  		})
  	} else {
  		toggleTripStyle()
  	}
    return false;
  });


  
  $('.remove-hover').mouseenter(closeMenu)
  // $('.remove-hover, #destination, #trip-style').mouseout(function(e) {
  $('section > .container').mouseenter(function(e) {
  	// console.log('Mouse entered', e.target)
  	closeMenu()
  	// if(e.target.tagName=='A'||e.target.tagName=='SPAN'||e.target.tagName=='LI') {

  	// } else {
  	// 	console.log('Close the menu', e.target)
  	// }
  })

  // $('.close').on('click', function() {
  //   $('.trip-style.pop').slideFadeToggle(function(e) {
	 //    $(e).removeClass('selected');
	 //  }); 
  //   $('.destination.pop').slideFadeToggle(function(e) {
	 //    $(e).removeClass('selected');
	 //  }); 
  //   return false;
  // });
});

$.fn.slideFadeToggle = function(easing, callback) {
  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
};

/*=================================
=            Bootstrap            =
=================================*/

(function($){
	$(document).ready(function(){
		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			event.preventDefault(); 
			event.stopPropagation(); 
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	});
})(jQuery);

/*=====  End of Bootstrap  ======*/


(function ($) {
	var $header    = $( 'header.custom-theme-header' );
	var $headerNav = $header.find( '#europeholidays-menu-collapse' );
	var $menuTemplate = $header.find('#hover-template');

	var MOUSE = {
		init: function () {
			$( 'body' ).mousemove(function ( event ) {
				if ( event.pageY > 400 && HEADER.eleContainer.hasClass('hovered') )
				{
					HEADER.hide();
					TOP_HEADER.clearHovered();
					MENU_TEMPLATE.clear();
				} else if ( event.pageY > 130 && ! HEADER.eleContainer.hasClass('hovered') ) {
					TOP_HEADER.clearHovered();
					MENU_TEMPLATE.clear();
				}
			});
		},
	};
	var HEADER = {
		eleContainer: $header,

		init: function () {
			this.eleContainer.after('<header class="header-clone"></header>');
		},
		/**
		 * [show description]
		 * @return {[type]} [description]
		 */
		show: function () {
			this.eleContainer.addClass('hovered');
			setTimeout(function () {
				HEADER.eleContainer.next('.header-clone').hide();
			}, 100);
		},
		/**
		 * [show description]
		 * @return {[type]} [description]
		 */
		hide: function () {
			this.eleContainer.removeClass('hovered');
			setTimeout(function () {
				HEADER.eleContainer.next('.header-clone').show();
			}, 100);
		}
	};
	var TOP_HEADER = {
		eleContainer: $headerNav,

		init: function () {
			this.eleContainer.find('a[data-id]').click(function () {
				var $this = $( this );
				var id    = $this.data('id');
				var hasChild = $this.parent('li').hasClass('has-child');
				TOP_HEADER.clearHovered();
				MENU_TEMPLATE.clear();

				$this.addClass('hovered');
				HEADER.hide();
				if ( hasChild )
				{
					HEADER.show();
					MENU_TEMPLATE.show( id );
				}
			});
		},
		/**
		 * [clearHovered description]
		 * @return {[type]} [description]
		 */
		clearHovered: function () {
			this.eleContainer.find('a[data-id]').removeClass('hovered');
			// this.eleContainer.find('li.open').removeClass('open');
		},
	};
	var MENU_TEMPLATE = {
		eleContainer: $menuTemplate,

		init: function () {
			this.clear();
		},
		/**
		 * [show description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		show: function ( id ) {
			this.stop( id );
			this.eleContainer.find('div[data-id="' + id + '"]').fadeIn();
		},
		/**
		 * [hide description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		hide: function ( id ) {
			this.stop( id );
			this.eleContainer.find('div[data-id="' + id + '"]').fadeOut();
		},
		/**
		 * [stop description]
		 * @param  {[type]} id [description]
		 * @return {[type]}    [description]
		 */
		stop: function ( id ) {
			this.eleContainer.find('div[data-id="' + id + '"]').stop();
		},
		/**
		 * [clear description]
		 * @return {[type]} [description]
		 */
		clear: function () {
			this.eleContainer.find('div[data-id]').stop();
			this.eleContainer.find('div[data-id]').fadeOut();
		}
	};

	var ZSIQ = {
		interval: null,
		eleContainer: null,
		eleMessageBtn: $( '[trigger-message]' ),

        init: function () {
        	this.interval = setInterval(ZSIQ.find, 3000);
        },

        find: function () {
        	let $ele = $( '#zsiqbtn' );
        	if ( $ele.length > 0 )
        	{
        		ZSIQ.eleContainer = $ele;
        	}
        	if ( ZSIQ.eleContainer != null ) {
        		clearInterval( ZSIQ.interval );
        		ZSIQ.event();
        	}
        },

        event: function () {
        	ZSIQ.eleMessageBtn.click(function () {
        		console.info('CLicked');
        		ZSIQ.eleContainer.click();
        	});
        }
    };

	init();
	function init () {
		// TOP_HEADER.init();
		// MENU_TEMPLATE.init();
		// HEADER.init();
		// MOUSE.init();
		ZSIQ.init();
	};
})(jQuery);