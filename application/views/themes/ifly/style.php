#zsiqbtn{
	position: fixed;
  bottom: 10px;
  left: 10px;
}

/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 300;
  src: local('Raleway Light'), local('Raleway-Light'), url(https://fonts.gstatic.com/s/raleway/v11/-_Ctzj9b56b8RgXW8FArifk_vArhqVIZ0nv9q090hN8.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
}

/* devanagari */
@font-face {
  font-family: 'Rajdhani';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://fonts.gstatic.com/s/rajdhani/v10/LDI2apCSOBg7S-QT7pb0EPOqeef2kg.woff2) format('woff2');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Rajdhani';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://fonts.gstatic.com/s/rajdhani/v10/LDI2apCSOBg7S-QT7pb0EPOleef2kg.woff2) format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Rajdhani';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://fonts.gstatic.com/s/rajdhani/v10/LDI2apCSOBg7S-QT7pb0EPOreec.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Rajdhani';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://fonts.gstatic.com/s/rajdhani/v10/LDI2apCSOBg7S-QT7pa8FvOqeef2kg.woff2) format('woff2');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Rajdhani';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://fonts.gstatic.com/s/rajdhani/v10/LDI2apCSOBg7S-QT7pa8FvOleef2kg.woff2) format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Rajdhani';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://fonts.gstatic.com/s/rajdhani/v10/LDI2apCSOBg7S-QT7pa8FvOreec.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

/*=================================
=            Bootstrap            =
=================================*/

/*.marginBottom-0 {margin-bottom:0;}

.dropdown-submenu{position:relative;}
.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
.dropdown-submenu:hover>a:after{border-left-color:#555;}
.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
.dropdown-toggle > .caret { display: none; }*/

/*=====  End of Bootstrap  ======*/


footer, header,
footer h3, header h3,
footer h4, header h4,
footer h6, header h6 { font-family: 'Rajdhani'; }
footer p {
	line-height: 36px;
	font-size: 18px;
}
footer .hover-danger:hover {
	color: #ea1e2a;
}
.social {
	font-size: 18px;
	padding: 20px;
	border: 1px solid;
}
h6 { font-weight: 500; }
.font-size-14 { font-size: 14px; }
.font-size-15 { font-size: 15px; }
.letter-spacing-2 { letter-spacing: 2px; }
.letter-spacing-4 { letter-spacing: 4px; }
.line-height-18 { line-height: 18px; }
.border-top { border-top: 1px solid #66656745; }
.text-dim { color: #666567; }
.border-dim { border-color: #66656745; }
.text-white { color: #fff; }
.m-0 {margin: 0;}

/*==============================
=            Header            =
==============================*/

.custom-theme-header
{
	height: 0px;
	-webkit-transition: 300ms ease height;
	-o-transition     : 300ms ease height;
	transition        : 300ms ease height;
}
.custom-theme-header.hovered
{
	height: 430px;
	-webkit-transition: 300ms ease height;
	-o-transition     : 300ms ease height;
	transition        : 300ms ease height;
}
header.header-clone { height: 130px; }
header .text-primary { color: #ea1e2a !important; }
header .text-primary-hover:hover { color: #1c75bc !important; }

header .menu-hover > a {
	margin: 0 5px;
	transition: .3s;
	overflow: hidden;
	font-size: 16px;
}
header .menu-hover > a:before,
header .menu-hover > a:after
,header .menu-hover > a > span:before,
header .menu-hover > a > span:after {
	background: #ea1e2a;
	content: "";
	position: absolute;
	transition: .3s;
}
header .menu-hover > a:before { left: 0; }
header .menu-hover > a:after { left: calc(100% - 2px); }
header .menu-hover:not(:hover) > a:before { transform: translateY(101%); }
header .menu-hover:not(:hover) > a:after { transform: translateY(-101%); }
header .menu-hover > a:before,
header .menu-hover > a:after {
	top: 0;
	bottom: 0;
	width: 2px;
	transition: .3s;
}
header .menu-hover > a > span:before { top: 0;  }
header .menu-hover > a > span:after { bottom: 0;  }
header .menu-hover:not(:hover) > a > span:before { transform: translateX(-101%); }
header .menu-hover:not(:hover) > a > span:after { transform: translateX(101%); }
header .menu-hover > a > span:before,
header .menu-hover > a > span:after {
  left: 0;
  right: 0;
  height: 2px;
  transition: .3s;
}

header .padding-0 {
	padding: 0;
}

header .menu-hover:hover > a > span {  }
.custom-theme-header { background-image: radial-gradient(circle farthest-side at center bottom,#333333,#051125 125%); }
.custom-theme-header .navbar-nav > li { position: relative; top: 0px; }
.custom-theme-header .navbar-nav > li.open > a, .custom-theme-header .navbar-nav > li.open > a:focus, .custom-theme-header .navbar-nav > li.active > a { background-color: transparent !important; }
/*.custom-theme-header .navbar-nav > li > a.hovered, .custom-theme-header .navbar-nav > li.open > a, .custom-theme-header .navbar-nav > li > a:hover, .custom-theme-header .navbar-nav > li.active > a { color: #fe0000 !important; border-bottom: 4px solid #fe0000; }*/
/*.custom-theme-header .navbar-nav > li > a:focus, .custom-theme-header .navbar-nav > li > a { font-size: 20px; padding: 53px 20px; color: #FFF; }*/
/*.custom-theme-header .navbar-nav > li.contact-item { margin-top: 15px; }*/
.custom-theme-header .navbar-nav > li.contact-item .contact-social { width: 183.17px; padding-left: 0px; padding-right: 10px; border-right: 1px solid #BDBDBD; margin-right: 10px; }
.custom-theme-header .navbar-nav > li.contact-item .contact-social > div { padding: 0px; }
.custom-theme-header .navbar-nav > li.contact-item .contact-social .number > p { font-size: 28px; display: inline; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; color: #fe0000; }
.custom-theme-header .navbar-nav > li.contact-item .others { padding: 0px; height: 87px; width: 105px; margin-top: 15px; }
.custom-theme-header .navbar-nav > li.contact-item .others > div { padding: 0px; }
.custom-theme-header.hovered > nav { border-bottom: 1px solid #696969; background: transparent; }

.custom-theme-header > nav {
	background: #fff;
}
.custom-theme-header > nav { border-radius: 0px; border: none; }
.custom-theme-header > nav:after {
  box-shadow: 0 0 25px rgba(0, 0, 0, 0.25);
  content: '';
  display: block;
  height: 110px;
  left: 0;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: -1;
}
.custom-theme-header .navbar-brand { height: 100px; }

div#packages-container .form-group > label{
	padding-left: 0;
	padding-right: 0;
}

/*a.selected {
  background-color:#1F75CC;
  color:white;
  z-index:100;
}
*/
.messagepop {
	position: absolute;
	width: 100% !important;
	top: 77px;
	background: #f5f8f8;
	z-index: 9999 !important;
  border: none;
  cursor:default;
  display:none;
  margin-top: 15px;
  position:absolute;
  text-align:left;
  z-index:50;
  padding: 25px 25px 20px;
}

.messagepop h6 {
	font-weight: 600;
}
.messagepop ul > li {
	margin: 10px 0;
}
.messagepop ul a {
	color: #1b1b1d;
}
.messagepop ul a:hover {
	color: #ea1e2a;
}
.messagepop ul {
	margin: 0;
	padding: 0;
	list-style: none;
	font-size: 1.4em;
}


#page-wrap > section {
	background: #ffff;
}
/*.custom-theme-header .navbar-nav > li > a.hovered > .header-item-icon
{
	-webkit-transform: rotate(-180deg);
	-ms-transform    : rotate(-180deg);
	-o-transform     : rotate(-180deg);
	transform        : rotate(-180deg);

	-webkit-transition: 300ms ease -webkit-transform;
	-o-transition     : 300ms ease -o-transform;
	transition        : 300ms ease transform;
}
.custom-theme-header .navbar-nav > li > a > .header-item-icon
{
	-webkit-transform: rotate(-360deg);
	-ms-transform    : rotate(-360deg);
	-o-transform     : rotate(-360deg);
	transform        : rotate(-360deg);

	-webkit-transition: 300ms ease -webkit-transform;
	-o-transition     : 300ms ease -o-transform;
	transition        : 300ms ease transform;
}

#hover-template .page-header { border-bottom-color: #FFF; margin-top: 10px; }
#hover-template .page-header > h4 { color: #FFF; font-family: 'Raleway'; text-transform: uppercase; margin: 0px; font-size: 1.3em; }
#hover-template .page-header ~ ul { width: 100%; list-style: none; padding-left: 0; padding-right: 0;columns: 3; display: inline-block;}
#hover-template .page-header ~ ul li { padding: 5px 0px; }
#hover-template .page-header ~ ul a { color: #FFF; font-size: 1em; }
#hover-template .page-header ~ ul a:hover { text-decoration: underline; }*/

/*=====  End of Header  ======*/

/*==============================
=            Footer            =
==============================*/

footer {
	background: #1a181b;
	color: #666567;
}
.custom-theme-footer { border-top: 1px solid #E0E0E0; }
.custom-theme-footer .page-header { border: none; margin-bottom: 10px; }
.custom-theme-footer .page-header > h6 { font-family: 'Raleway'; text-transform: uppercase; letter-spacing: 2px; }
.custom-theme-footer .page-header ~ ul { list-style: none; padding: 0px; }
.custom-theme-footer .page-header ~ ul li { padding: 5px 0px; }
.custom-theme-footer .page-header ~ ul li > a { color: #262626; }
.custom-theme-footer .page-header ~ ul li > a:hover { color: #fe0000; }
/*.custom-theme-footer .copy-right { background: rgb(254, 0, 0) none repeat scroll 0 0; margin-top: 40px; }*/
.custom-theme-footer .copy-right .container { padding: 40px 0px; color: #FFF; font-size: 12px; }

/*=====  End of Footer  ======*/


/*=============================
=            Media            =
=============================*/

@media ( min-width: 768px )
{
	/*.dropdown-menu { visibility: hidden; };*/
	.navbar-nav {
		padding-top: 40px;
		display: inline-block;
		float: none;
	}
}

@media ( max-width: 1287px )
{
	/*.custom-theme-header .navbar-nav > li.contact-item .others { display: none; }*/
	/*.custom-theme-header .navbar-nav > li.contact-item .contact-social { border-right: none; }*/
}
@media ( max-width: 1182px )
{
	/*.custom-theme-header .navbar-nav > li.contact-item .contact-social { display: none; }*/
}
@media ( min-width: 768px ) and ( max-width: 991px )
{
	/*.custom-theme-header .navbar-nav > li > a:focus, .custom-theme-header .navbar-nav > li > a { font-size: 17px; padding: 56px 9px; margin-bottom: -6px; margin-top: 1px; }*/
	/*.custom-theme-header .navbar-brand { margin: 0px; margin-left: 0px !important; }*/
}
@media ( max-width: 767px )
{

/*===================================
=            Adjustments            =
===================================*/

.btn-danger { background-color: #fe0000; }
.btn-success, .btn-success:hover { background-color: #2c2c2c; border-color: #2c2c2c; }
.ui-datepicker .calendar-product.ui-datepicker-current-day .calendar-price, .ui-datepicker .calendar-product.ui-datepicker-current-day .fa-usd { color: #2c2c2c !important; }
/*.ui-datepicker .calendar-product.ui-datepicker-current-day .calendar-indicator, .ui-datepicker .calendar-product.ui-datepicker-current-day { border-color: #2c2c2c transparent transparent transparent; }*/
.ui-datepicker .calendar-product.ui-datepicker-current-day { border-color: #2c2c2c; }
.ui-datepicker .calendar-on-range { border: 1px solid #2c2c2c; }
.ui-datepicker .calendar-product { border-color: #fe0000; }
.ui-datepicker .calendar-product .calendar-price { color: #fe0000; }
.panel-primary>.panel-heading { background-image: radial-gradient(circle farthest-side at center bottom,#333333,#051125 125%); }
.text-danger { color: #fe0000; }



/*=====  End of Adjustments  ======*/

}
section > .container,
section > .container .page-header > h4,
section > .container .page-header > h6 {
	font-family: 'Rajdhani' !important;
}