<div class="container">
	<div class="row" style='padding: 75px 0; margin: 0 5px;'>
		<div class="col-md-4">
			<div class="row">
				<div class="col-sm-12">
					<p style='line-height: 36px; font-size: 18px;'>Need to talk to one our our travel experts? We're here to help you anyway we can to ensure your next travel adventure is epic.</p>
				</div>
				<div class="col-sm-12"><br><br></div>
				<div class="col-sm-12">
					<a class='text-white hover-danger border-dim social' href="http://iflygo.com.au/iFlyGo/">
						<i class="fa fa-facebook-f" style='width: 21px; height: 21px; text-align: center;'></i>
					</a>
					<a class='text-white hover-danger border-dim social' style='margin: 0 5px;' href="http://iflygo.com.au/iFlyGo/">
						<i class="fa fa-twitter" style='width: 21px; height: 21px; text-align: center; background: none;'></i>
					</a>
					<a class='text-white hover-danger border-dim social' href="https://www.instagram.com/iflygo/">
						<i class="fa fa-instagram" style='width: 21px; height: 21px; text-align: center; background: none;'></i>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<h4 class='m-0 text-white' style='font-weight: 700; font-size: 24px; line-height: 42px;'>2/66 Appel Street, Surfers Paradise, QLD Australia 4217</h4>
			<br>
			<br>
			<h6 class='text-dim m-0 letter-spacing-2 line-height-18 font-size-14'><span class="fa fa-phone"></span> 1800 242 373</h6>
			<br>
			<h6 class='text-dim m-0 letter-spacing-2 line-height-18 font-size-14'><span class="fa fa-clock-o"></span> 8:00 - 17:00 MON TO FRI</h6>
		</div>
		<div class="col-md-4">
			<h6 class='text-white'>BEFORE YOU GO</h6>
			<p><a class='text-dim hover-danger' href="http://iflygo.com.au/booking-terms-and-conditions">Booking Terms & Conditions</a></p>
			
			<br>
			<h6 class='text-white'>ABOUT & SUPPORT</h6>
			<p><a class='text-dim hover-danger' href="http://iflygo.com.au/about-iflygo">About iFlyGo</a></p>
			<p><a class='text-dim hover-danger' href="http://iflygo.com.au/frequently-asked-questions">FAQ's</a></p>
			<p><a class='text-dim hover-danger' href="http://iflygo.com.au/testimonials">Testimonials</a></p>
			<p><a class='text-dim hover-danger' href="http://iflygo.com.au/privacy-policy">Privacy Policy</a></p>
		</div>
	</div>
</div>
<div class="copy-right border-top">
	<div class="container text-uppercase" style='padding: 20px 15px;'>
		<div class="row">
			<div class="col-sm-12">
				<h6 style='font-weight: 700;' class='text-dim m-0 letter-spacing-2 line-height-18 font-size-14'>&copy; COPYRIGHT 2021 IFLYGO, ALL RIGHTS RESERVED.  IATA NO: 0236 1424   ABN: 74 071 920 360   ATAS NO: A11315</h6>
			</div>
			<div class="col-sm-12"><br></div>
			<div class="col-sm-12">
				<h6 style='font-weight: 700;' class='text-dim m-0 letter-spacing-2 line-height-18 font-size-14'>ALL TRIP PRICES ARE IN AUD $</h6>
			</div>
		</div>
	</div>
</div>