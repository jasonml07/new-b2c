<?php
$base_url='https://www.europeholidays.com.au/';
?>

<div class="messagepop destination pop" id="menu-destination">
   <div style='padding-right: 15px; padding-left: 15px; margin-left: auto; margin-right: auto;'>
   	<div class="row">
   		<div class="col-md-3">
   			<h6>GO ASIA</h6>
   			<ul>
   				<li><a href="http://iflygo.com.au/thailand">Thailand</a></li>
   				<li><a href="http://iflygo.com.au/vietnam">Vietnam</a></li>
   			</ul>
   		</div>
   		<div class="col-md-3">
   			<h6>GO EUROPE</h6>
   			<ul>
   				<li><a href="http://iflygo.com.au/iceland">Iceland</a></li>
   			</ul>
   		</div>
   		<div class="col-md-3">
   			<h6>GO MEDITERRANEAN</h6>
   			<ul>
   				<li><a href="http://iflygo.com.au/croatia">Croatia</a></li>
   				<li><a href="http://iflygo.com.au/greece">Greece</a></li>
   				<li><a href="http://iflygo.com.au/spain">Spain</a></li>
   			</ul>
   		</div>
   		<div class="col-md-3">
   			<h6>GO OCEANIA</h6>
   			<ul>
   				<li><a href="http://iflygo.com.au/australia">Australia</a></li>
   				<li><a href="http://iflygo.com.au/new-zealand">New Zealand</a></li>
   			</ul>
   		</div>
   	</div>
   </div>
</div>

<div class="messagepop trip-style pop" id="menu-trip-style">
   <div style='padding-right: 15px; padding-left: 15px; margin-left: auto; margin-right: auto;'>
   	<div class="row">
   		<div class="col-md-3">
   			&nbsp;
   			<!-- <h6>GO ASIA</h6>
   			<ul>
   				<li><a href="http://iflygo.com.au/thailand">Thailand</a></li>
   				<li><a href="http://iflygo.com.au/vietnam">Vietnam</a></li>
   			</ul> -->
   		</div>
   		<div class="col-md-3">
   			&nbsp;
   			<!-- <h6>GO EUROPE</h6>
   			<ul>
   				<li><a href="http://iflygo.com.au/iceland">Iceland</a></li>
   			</ul> -->
   		</div>
   		<div class="col-md-2">
   			<ul>
   				<li><a href="javascript:void(0);">Go Adventure</a></li>
   				<li><a href="javascript:void(0);">Go Independent</a></li>
   				<li><a href="javascript:void(0);">Go Group</a></li>
   				<li><a href="javascript:void(0);">Go Cruises</a></li>
   				<li><a href="javascript:void(0);">Go Festivals</a></li>
   				<li><a href="javascript:void(0);">Go Skiing</a></li>
   			</ul>
   		</div>
   		<div class="col-md-4" style="padding:0;">
   			<iframe style='width: 100%;' src="https://www.youtube.com/embed/ISdydcliclQ?start=0&showinfo=1&controls=1&autoplay=0" frameborder="0"></iframe>
   		</div>
   	</div>
   </div>
</div>

<nav class="navbar navbar-default navbar-static-top border-dim">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" data-toggle="collapse" data-target="#iflygo-menu-collapse" aria-expanded="false" class="navbar-toggle collapsed">
				<span class="sr-only">Toggle Mobile Menu</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="http://iflygo.com.au" class="navbar-brand padding-0" style="margin-left: 10px;">
				<img style='height: 100%;' src="<?=base_url('assets/images/iflygo-logo.png')?>" alt="iflygo">
			</a>
		</div>
		
		<div class="collapse navbar-collapse" id="iflygo-menu-collapse" style='text-align: center;'>
			<ul class="nav navbar-nav navbar-center">
				<li class='text-primary-hover menu-hover'>
					<a href="http://iflygo.com.au/" class='remove-hover font-size-15 text-primary letter-spacing-4 text-primary-hover'>
						<span>HOME</span>
					</a>
				</li>
				<li id='destination' class='text-primary-hover menu-hover'>
					<a href="http://iflygo.com.au/destinations" class='font-size-15 text-primary letter-spacing-4 text-primary-hover'>
						<span>DESTINATIONS</span>
					</a>
				</li>
				<li id='trip-style' class='text-primary-hover menu-hover'>
					<a href="http://iflygo.com.au/pricing" class='font-size-14 text-primary letter-spacing-4 text-primary-hover'>
						<span>TRIP STYLE</span>
					</a>
				</li>
				<li class='text-primary-hover menu-hover'>
					<a href="http://iflygo.com.au/contact-us-2" class='remove-hover font-size-14 text-primary letter-spacing-4 text-primary-hover'>
						<span>CONTACT US</span>
					</a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right" style='background: #ea1e2a;'>
				<li class="contact-item visible-md visible-lg" style='padding: 5px 65px; top: -10px;'>
					<div>
						<h5 class='text-white text-center m-0' style='letter-spacing: 6px; font-size: 18px;'>1800 242 373</h5>
						<p class='text-white text-center'>24/7 Support.  Based in Australia.</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>