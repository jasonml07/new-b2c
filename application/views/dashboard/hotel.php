<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_id() == '') {
	session_start();
}

/**
 * 
 */
class Hotel extends CI_Controller {

	
	public function __construct()
    {
        parent::__construct();
    }  


    ########## PAGES VIEWS ###############
	public function index()
	{
		
		
		$this->load_view('dashboard/body'); 
		
		//$this->load->view('dashboard/index2');  
	}

	public function theme () {
		$this->load->view('dashboard/titlehead', array("theme" => $this->uri->segment(2)));
		$this->load->view('dashboard/header'); 
		$this->load->view('dashboard/body');
		$this->load->view('dashboard/footer');
	}
	public function bookings(){
		//die('asd');
		 
		//print_r($res);
		if($this->session->userdata('logged_in') != 1){
			header("Location: ".base_url()."");
		}
		$this->load->model('booking_model');
		 $agencyCode = $this->session->userdata('agencycode');
		 $res = $this->booking_model->getBookingDetails($agencyCode);
		// print_r($res);
		$data = array("bookings"=>$res);
		$this->load_view('bookings/body',$data ); 
	}
	public function load_view($viewFilename, $data = array()){


		$this->load->view('dashboard/titlehead'); 
		$this->load->view('dashboard/header'); 
		$this->load->view($viewFilename, $data);
		$this->load->view('dashboard/footer');
    
	}
	public function bookingDetails($bookingID){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		//print_r($result);
		$data = array();

		$data['BookingData'] = $result['bookingData'];
		$data['ItemDatas'] = $result['itemDatas'];
		$data['ReceiptDatas'] = $result['receiptDatas'];
		$data['CostingsDatas'] = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/details',$data); 
	}
	public function itemDetails($bookingID,$itemId){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleItemDetails($bookingID,$itemId);
		$data = array("item"=>$result);
		$this->load_view('bookings/item_details',$data); 
	
		
	}
	public function bookingPayments($bookingID){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');


		$data = array();

		$data['enetSuccess'] = false;
		if(isset($_POST['submit'])){
			if($_POST['type'] == 'enet'){
				

				$checkEnet = $this->booking_model->submitEnetReceipt($agencyCode,(int)$bookingID,$_POST);
				if($checkEnet){

					$data['enetSuccess'] = true;

				}
			}
			if($_POST['type'] == 'bank'){
				$_SESSION['paymentData'] = $_POST;
				redirect('dashboard/paymentVerification/'.$bookingID);
			}
			
		}

		
		

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		//print_r($result);
		



		
		$data['BookingData']    = $result['bookingData'];
		$data['ItemDatas']      = $result['itemDatas'];
		$data['ReceiptDatas']   = $result['receiptDatas'];
		$data['CostingsDatas']  = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/payments',$data); 
	}
	public function paymentVerification($bookingID){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');


		$data = array();

		

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		//print_r($result);
		

		$data['pay_data'] = $_SESSION['paymentData'];


		$data['BookingData']    = $result['bookingData'];
		$data['ItemDatas']      = $result['itemDatas'];
		$data['ReceiptDatas']   = $result['receiptDatas'];
		$data['CostingsDatas']  = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/paymentCCverify',$data); 
	}
	public function hotel(){
		$data = array();
		$this->load_view('hotel/searchhotel',$data); 
	}
	public function hotelDetails(){
		$data = array();
		$this->load_view('hotel/searchdetails',$data); 
	}
	public function flight(){
		$data = array();
		$this->load_view('flight/searchflight',$data); 
	}
	public function flightDetails(){
		$data = array();
		$this->load_view('flight/searchdetails',$data); 
	}



	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
