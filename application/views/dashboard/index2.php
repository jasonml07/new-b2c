<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Travelrez Agency Dash</title>
    <link href="<?php echo base_url();?>bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>libs/jquery1.11.4/jquery-ui.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
   
    
</head>

<body>
    <section >
            <div class="row">
                    <div class="col-md-6">
                         <form onsubmit="return false;"  id="form_search" >
                            Destination: <input id="destination_area" type="text" required>
                            <input id="destination_code" type="hidden" name="destination_code" value="" required><br>
                            Hotel Name: <input id="hotel_area" type="text">
                            <input id="hotel_code" type="hidden" name="hotel_code"><br>

                            Date <input type="text"  id="start" value="" name="checkin_date" required />
                            <span >to</span>
                            <input type="text"  id="end" value="" name="checkout_date" required />
                            <br>
                            Number of Rooms
                            <select onChange="travelrez.room(this.value)"  id="rooms" name="rooms" >
                                <option value="1"  selected="selected" >1</option> 
                                <option value="2" >2</option> 
                                <option value="3" >3</option> 
                                <option value="4" >4</option> 
                                <option value="5" >5</option> 
                                        
                            </select>
                            <br>
                            <div id="roomPax1"> Room 1
                            Adult <select id="adult1"   name="rooms[1][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                            Child  <select id='child1'   name="rooms[1][child]" onChange='travelrez.child_fnc(this.value,1)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                              </select>
                               <div id="age1"   ></div> 
                            </div>
                            <div id="roomPax2" style="display:none;">Room 2
                            Adult <select id="adult2"   name="rooms[2][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                            Child  <select id='child2'   name="rooms[1][child]" onChange='travelrez.child_fnc(this.value,2)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                              </select>
                               <div id="age2"   ></div> 
                               </div>
                               <div id="roomPax3" style="display:none;">Room 3
                            Adult <select id="adult3"   name="rooms[3][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                            Child  <select id='child3'   name="rooms[3][child]" onChange='travelrez.child_fnc(this.value,3)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                              </select>
                               <div id="age3"   ></div> 
                               </div>
                               <div id="roomPax4" style="display:none;">Room 4
                            Adult <select id="adult4"   name="rooms[4][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                            Child  <select id='child4'   name="rooms[4][child]" onChange='travelrez.child_fnc(this.value,4)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                              </select>
                               <div id="age4"   ></div> 
                               <div id="roomPax5" style="display:none;"> Room 5
                            Adult <select id="adult5"   name="rooms[5][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                            Child  <select id='child5'   name="rooms[5][child]" onChange='travelrez.child_fnc(this.value,5)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                              </select>
                                 <div id="age5"   ></div> 
                                </div>
                             </div>

                                 <button type="submit" id="submit_search" > Search for Hotels</button>
                        </form>

                    </div>

                    <div class="col-md-6">
                        
                    </div>
            </div>
           
        </section>
   
    

    <!-- jQuery -->
    <script src="<?php echo base_url();?>libs/jquery1.11.4/external/jquery/jquery.js"></script>
    <script src="<?php echo base_url();?>libs/jquery1.11.4/jquery-ui.js"></script>
    <script>


    $( document ).ready(function() {
        $( "#destination_area" ).autocomplete({
            source: "<?php echo base_url(); ?>_ajax/execute_destination.php?action=getDestinationHB",
            minLength: 3,
            select: function( event, ui ) { 
                event.preventDefault()
                $(this).val(ui.item.label);
                $("#destination_code").val(ui.item.value);
                $( "#hotel_area" ).autocomplete('option', 'source', "<?php echo base_url(); ?>_ajax/execute_products.php?action=getHotelProductsHB&destination_code="+ui.item.value)
                  //document.getElementById("country_code").value = ui.item.code;
            }
        });
        $( "#hotel_area" ).autocomplete({
            minLength: 1,
            select: function( event, ui ) { 
                event.preventDefault()
                $(this).val(ui.item.label);
                $("#hotel_code").val(ui.item.value);
                  //document.getElementById("country_code").value = ui.item.code;
            }
        });

        var forDate = new Date();
        forDate.setDate(forDate.getDate()+2);
        var forDate2 = new Date();
        forDate2.setDate(forDate2.getDate()+3);

        $( "#start" ).datepicker({
            dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            minDate: forDate,
            onClose: function( selectedDate ) {
               var nDate = selectedDate.split('/')
                          var ffdate = new Date(nDate[1]+"/"+nDate[0]+"/"+nDate[2]);
                           ffdate.setDate(ffdate.getDate()+1);

                          var fthisdate = ffdate.getDate() + '/' + (ffdate.getMonth() +1 ) + '/' +  ffdate.getFullYear(); 
                           
                          $('#end').val(fthisdate); 
            $( "#end" ).datepicker( "option", "minDate", selectedDate );
               
            }
          });
        $( "#end" ).datepicker({
            dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            minDate: forDate,
            onClose: function( selectedDate ) {
            //$( "#start" ).datepicker( "option", "maxDate", selectedDate );
            }
          });

    });

    $('#submit_search').click(function(){

        var forms = $('#form_search').serialize(); 
        
                if($('#destination_code').val() != "" && $('#start').val() !="" && $('#end').val() !="") {
                    $.ajax({
                            type:"GET",
                            url:"<?php echo base_url(); ?>_ajax/execute_search_hb_manager.php?action=doSearchHB",
                            data:forms, 
                            dataType:'json',
                            success:function(data){ 
                                if(data.status=="ok"){
                                   alert(error);
                                }else{
                                  alert(error);
                                 

                                }
                                  
                          
                            },error:function(data){
                             alert("error");
                            }
                    }); 
              
            } 
    });
     

    var travelrez = function () {
        return {
            room: function(roomVal){
                var roomVal = parseFloat(roomVal);
                for(var x=1;x<=roomVal;x++){
                    document.getElementById("roomPax"+x).style.display = "block";
                }
                var xRoom = parseFloat(roomVal+1);
                for(var y=xRoom;y<=5;y++){
                    
                    document.getElementById("roomPax"+y).style.display = "none";
                }
                        
            },
            child_fnc: function(numCHild,roomNum){
                var row_min= $('#age'+roomNum).html('');

                 if(numCHild != '0'){ 
                      for(var x = 0; x<numCHild; x++){
                         row_min.append('Age <input type="number" name="rooms['+roomNum+'][childAge][]"  value="8" required>');
                    }
                   
                 } 
                        
            }

        
        }
    }();   
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>
