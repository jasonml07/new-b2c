<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php if(!empty($this->session->userdata['result']['name'])):?>
        <title><?=$this->nativesession->get('agency_name'); ?> Hotel</title>
    <?php endif;?>
     <!-- GOOGLE FONT -->
   <!--  <link href='https://fonts.googleapis.com/css?family=Open+Sans:700,600,400,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oswald:400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'> -->

    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/awe-booking-font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/jquery-ui.css">
    <!-- REVOLUTION DEMO -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/revslider-demo/css/settings.css">

    <!-- Form Validation CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/formValidation.min.css">

    <!-- MAIN STYLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/dashboard-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/demo.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/uploadify.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>bower_components/Yamm3/yamm/yamm.css">
    <style type="text/css" class="init">
    
    

    </style>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/jquery.dataTables.min.css">

    <!-- CSS COLOR -->


    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/blue.css">

    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/ui-bootstrap-csp.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/adjustments.css">
    
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/blue.css"> -->
    

    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/adjustments.css"> -->
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/waitMe.min.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/theme/theme-<?php echo @$themes; ?>.css">
    <!-- <link id="colorreplace" rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/theme/theme-10.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/bootstrap-dialog.min.css">
    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <?php TZ_custom_theme_style( $this->nativesession->get('agencyRealCode') ) ?>
    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/hotel.css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TPN294J');</script>
    <!-- End Google Tag Manager -->
    <style type="text/css">
        span.amount{
            font-size: 22px !important;
        }
        #zsiqbtn{
            position: fixed;
            bottom: 10px;
            left: 10px;
        }
    </style>
</head>