    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <footer class="custom-theme-footer">
            <?php TZ_custom_theme_footer( $this->nativesession->get('agencyRealCode') ) ?>
        </footer>
    <?php else: ?>
<!-- FOOTER PAGE -->
        <footer id="footer-page" class="divs">
            <a name="contact"></a>
            <div class="container">
                <div class="row">

                    <!-- WIDGET -->
                    <div class="col-md-4">
                        <div class="widget widget_about_us">
                            <h3 style="color: #EDEDED!important; margin-left: 28px; margin-top: 20px;">Contact Us</h3>
                            <div class="widget_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4 col-xs-4">
                                            <form>
                                                <h5 style="color:#EDEDED; margin-top: -10px;">
                                                    <span class="fa fa-globe"></span>
                                                    RESERVATION Office
                                                </h5>
                                                <address>
                                                 
                                                    <br>
                                                    <?=@$address;?>
                                                    <br>
                                                    <label> Phone:</label>
                                                    <?=@$number;?>
                                                    <br>
                                                    <label> Toll free:</label>
                                                    <?=@$fax;?>
                                                    <br>
                                                </address>
                                                <address>
                                                    <strong>Email</strong>
                                                    <br>
                                                    <a href="mailto:<?=@$email;?>"><?=@$email;?> </a>
                                                    </address>
                                                    <address>
                                                    <strong>Hours of Operation </strong>
                                                    <br>
                                                    Mon-Fri: 08.00 am – 17.30 pm
                                                    <br>
                                                    Sat: 24 hour Contact 0433 161 250
                                                    <br>
                                                    Sun: 24 hour Contact 0433 161 250
                                                    <br>
                                                </address>
                                            </form>
                                        </div>

                                        <div class="col-md-8 col-xs-8">
                                            <div class="well well-sm" style="background-color: #333;border:0px solid #FFF">
                                                <form accept-charset="utf-8" method="post" action="<?php echo base_url();?>php_mailer/sendMail ">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> Name</label>
                                                                <input id="name" class="form-control" type="text" required="required" placeholder="Enter name" style="border:1px solid #ddd" name="name" value="<?php echo set_value('name'); ?>">
                                                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="email"> Email Address</label>
                                                                <input id="email" class="form-control" type="email" required="required" placeholder="Enter email" style="border:1px solid #ddd" name="email" value="<?php echo set_value('email'); ?>">
                                                                <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="subject"> Subject</label>
                                                                <input id="subject" class="form-control" type="text" required="required" placeholder="Enter subject" name="subject" style="border:1px solid #ddd" value="<?php echo set_value('subject'); ?>">
                                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="name"> Message</label>
                                                                <textarea id="message" class="form-control" placeholder="Message" required="required" cols="25" rows="9" style="border:1px solid #ddd;border-radius: 0px" name="message" value="<?php echo set_value('message'); ?>"></textarea>
                                                                <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="qna">What is 3 + 2?</label>
                                                                <input id="qna" class="form-control" type="text" required="required" placeholder="Answer" name="captcha" style="border:1px solid #ddd">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <button id="btnContactUs" class="btn btn-primary" type="submit"> Send Message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / WIDGET -->
                </div>
                        <div class="widget widget_follow_us" style="text-align: center;">
                            <div class="widget_content">
                                <div class="awe-social">
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-youtube-play"></i></a>
                                </div>
                            </div>
                        </div>
                <div class="copyright">
                    <p>©2019 TravelRez™ All rights reserved.</p>
                </div>
            </div>
        </footer>
        <!-- END / FOOTER PAGE -->
        <?php endif ?>

    </div>
    <!-- END / PAGE WRAP -->

     <!-- LOAD JQUERY -->
  
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.magnific-popup.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url();?>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/wow.min.js"></script>
    <!--load bootstrap.js-->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/moment.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>static/js/adjustments.js"></script>
    <!-- load form validation -->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap-dialog.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.validate.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.dataTables.min.js"></script>
    <!-- REVOLUTION DEMO -->
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/jquery.uploadify.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/waitMe.min.js"></script>

        
    <!-- ANGULARJS -->
    <script type="text/javascript" src="<?=base_url();?>static/js/angular.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/angular-resource.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/faker.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/dirPagination.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ui-bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.serialize-object.min.js"></script>

    <script type="text/javascript" src="<?=base_url();?>static/js/angular-filter.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/bindonce.min.js"></script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCVcfc7DEojrpPwTY0dzAhR-sW-cbEHiiA&libraries=geometry" type="text/javascript"></script>
    <script src="https://rawgit.com/allenhwkim/angularjs-google-maps/master/testapp/scripts/markerclusterer.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ng-map.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ng-infinite-scroll.min.js"></script>



    <script type="text/javascript">
        (function ($) {
            $('input#hotel-startdate').datepicker({
                onSelect: function ( dateText, inst ) {
                    // console.log( dateText, inst );
                    var split = dateText.split('/');
                    var date  = new Date(split[1] + '/' + split[0] + '/' + split[2]);
                    date.setDate( date.getDate() + 1 );
                    $('input#hotel-enddate').datepicker('option', 'minDate', new Date(split[1] + '/' + split[0] + '/' + split[2]));
                    $('input#hotel-enddate').datepicker('setDate', $.datepicker.formatDate('dd/mm/yy', date));
                }
            });
        }(jQuery));
    </script>

    <script type="text/javascript">
        angular.module('custom.utility', [])
            .constant('config', {
                'URL': '<?=base_url()?>',
                'DATETODAY': new Date(),
                'TEST_INPUTS': <?=(TEST_INPUTS)?'true':'false'?>
            })
            .directive('myCurrency', [function () {
                return {
                    restrict: 'E',
                    replace : true,
                    scope   : {
                        currency: '=',
                        amount  : '=',
                        async   : '='
                    },
                    template: '<div><span class="fa fa-{{ (currency != \'AUD\')?(currency|lowercase):\'usd\' }}"></span> {{ amount|number:2 }}</div>'
                };
            }])
            .filter('filterBoard', [function () {
                return function (input) {
                    var boards = [
                      {code: 'AI', text: 'All Inclusive'},
                      {code: 'FB', text: 'Full Board'},
                      {code: 'HB', text: 'Half Board'},
                      {code: 'BB', text: 'Bed & Breakfast'},
                      {code: 'RO', text: 'Room Only'}
                    ];

                    angular.forEach(boards, function (value, key) {
                        input = input.replace(value.code, value.text);
                    });
                    return input.replace(',', ', ');
                };
            }])
            ;
    </script>

    <?php if( isset($hasAngular) && $hasAngular == TRUE ) :?>
        <?php foreach ($angularJSs as $key => $value) :?>
            <script type="text/javascript" src="<?=base_url()?>static/<?=$value?>"></script>
        <?php endforeach; ?>

    <?php endif; ?>

    <!-- DATE CONFIGURATION -->
    <script type="text/javascript">
        (function ($) {
            var dateToday = new Date();
            dateToday.setDate(dateToday.getDate() + 3);
            $.datepicker.setDefaults({
                minDate: dateToday,
                dateFormat: 'dd/mm/yy',
            });
            
            $('#paymentexpirationTo').datepicker({
                dateFormat: 'dd MM'
            });
            $('#paymentexpirationFrom').datepicker({
                dateFormat: 'dd MM',
                onSelect: function (dateText, inst) {
                    var date = new Date(inst.selectedYear + "-" + (inst.selectedMonth+1) + "-" + inst.selectedDay);
                    $('#paymentexpirationTo').datepicker( "option", "minDate", date);
                }
            });

            // Configuration on the payment date
            $('#ePaymentdate').datepicker({
                minDate: dateToday
            });
        }(jQuery));
    </script>

    <script type="text/javascript">
        jQuery.noConflict();
        (function($){
            $(document).ready(function() {
                $('.slider-main').bxSlider({
                  mode: 'horizontal',
                  captions: true,
                  autoplay: true

              });

                if($('#slider-revolution').length) {
                $('#slider-revolution').show().revolution({
                    ottedOverlay:"none",
                    delay:10000,
                    startwidth:1600,
                    startheight:650,
                    hideThumbs:200,

                    thumbWidth:100,
                    thumbHeight:50,
                    thumbAmount:5,
                    
                                            
                    simplifyAll:"off",

                    navigationType:"none",
                    navigationArrows:"solo",
                    navigationStyle:"preview4",

                    touchenabled:"on",
                    onHoverStop:"on",
                    nextSlideOnWindowFocus:"off",

                    swipe_threshold: 0.7,
                    swipe_min_touches: 1,
                    drag_block_vertical: false,
                    
                    parallax:"mouse",
                    parallaxBgFreeze:"on",
                    parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                                            
                                            
                    keyboardNavigation:"off",

                    navigationHAlign:"center",
                    navigationVAlign:"bottom",
                    navigationHOffset:0,
                    navigationVOffset:20,

                    soloArrowLeftHalign:"left",
                    soloArrowLeftValign:"center",
                    soloArrowLeftHOffset:20,
                    soloArrowLeftVOffset:0,

                    soloArrowRightHalign:"right",
                    soloArrowRightValign:"center",
                    soloArrowRightHOffset:20,
                    soloArrowRightVOffset:0,

                    shadow:0,
                    fullWidth:"on",
                    fullScreen:"off",

                    spinner:"spinner2",
                                            
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,

                    shuffle:"off",

                    autoHeight:"off",
                    forceFullWidth:"off",
                    
                    
                    
                    hideThumbsOnMobile:"off",
                    hideNavDelayOnMobile:1500,
                    hideBulletsOnMobile:"off",
                    hideArrowsOnMobile:"off",
                    hideThumbsUnderResolution:0,

                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    startWithSlide:0
                });
            }
                $('#bookinglist').DataTable({
                    "order": [[ 0, "desc" ]]
                });
            $("#checkAll").click(function () {
                $(".check").prop('checked', 
                    $(this).prop('checked')
                );

                var totalAmount = 0;
                $('input.check[type=checkbox]').each(function () {
                    if(this.checked){
                        var dataT = $(this).val().split("|");
                       totalAmount += parseFloat(dataT[2]);

                    }
                   // var sThisVal = (this.checked ? $(this).val() : ""); 
                });
                $('#card_amount').val(totalAmount);
            });
            $(".check").click(function () {
                
                var totalAmount = 0;
                $('input.check[type=checkbox]').each(function () {
                    if(this.checked){
                        var dataT = $(this).val().split("|");
                       totalAmount += parseFloat(dataT[2]);
                       
                    }
                   // var sThisVal = (this.checked ? $(this).val() : ""); 
                });
                $('#card_amount').val(totalAmount);
            });

            });

           
    
            
        }(jQuery));
    </script>
    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <?php TZ_custom_theme_script( $this->nativesession->get('agencyRealCode') ) ?>
    <?php endif; ?>
    <script type="text/javascript">
var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"3b7f5b3d2364120336dd891c7706fe1110fa720c9fd129af1795ba7dfee5b4938155ad889dc9bdc4a5d9a79cd083266b", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);
</script>
</body>
</html>