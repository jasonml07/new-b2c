
        <!-- HERO SECTION -->
        <section class="hero-section">
            <div id="slider-revolution">
                <ul>
                    <?php if($this->session->userdata('result')):?>
                    <?php //print_r($this->session->userdata['result']['slides']);?>
                    <?php if(empty($this->session->userdata['result']['slides'])):?>
                    <li data-slotamount="7" data-masterspeed="300" data-title="Paris">
                        <img src="<?php echo base_url();?>static/images/12.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">

                        <!-- <div class="tp-caption sfb fadeout slider-caption-sub slider-caption-sub-1" data-x="500" data-y="230" data-speed="700" data-start="1500" data-easing="easeOutBack">
                          Last minute deal
                        </div>

                        <div class="tp-caption sfb fadeout slider-caaption slider-caption-1" data-x="center" data-y="280" data-speed="700" data-easing="easeOutBack"  data-start="2000">Top discount Paris Hotels</div> -->
                        
                       
                    </li> 

                    <li data-slotamount="7" data-masterspeed="300" data-title="Europe">
                        <img src="<?php echo base_url();?>static/images/13.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">

                        <!-- <div class="tp-caption  sft fadeout slider-caption-sub slider-caption-sub-2" data-x="center" data-y="220" data-speed="700" data-start="1500" data-easing="easeOutBack">
                          Check out the top weekly destination
                        </div>

                        <div class="tp-caption sft fadeout slider-caption slider-caption-2" data-x="center" data-y="260" data-speed="700" data-easing="easeOutBack"  data-start="2000">
                            Travel with us
                        </div> -->
                        
                        
                    </li>

                    <li data-slotamount="7" data-masterspeed="300" data-title="Moscow">
                        <img src="<?php echo base_url();?>static/images/14.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">

                        <!-- <div class="tp-caption lfl fadeout slider-caption slider-caption-3" data-x="center" data-y="260" data-speed="700" data-easing="easeOutBack"  data-start="1500">
                            Gofar
                        </div>
                        
                        <div href="#" class="tp-caption lfr fadeout slider-caption-sub slider-caption-sub-3" data-x="center" data-y="365" data-easing="easeOutBack" data-speed="700" data-start="2000">Take you to every corner of the world</div> -->
                    </li>    
                    <?php else:?>    
                    <?php foreach($this->session->userdata['result']['slides'] as $value):?>
                    
                    <li data-slotamount="7" data-masterspeed="500" data-title="Moscow">
                        <img src="<?php echo B2B_IP."slides/".$value['image_name']?>" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">

                        <div class="tp-caption lfl fadeout slider-caption slider-caption-3" data-x="center" data-y="260" data-speed="700" data-easing="easeOutBack"  data-start="1500">
                        <?=$value['label']?>    
                        </div>
                        
                        <!-- <div href="#" class="tp-caption lfr fadeout slider-caption-sub slider-caption-sub-3" data-x="center" data-y="365" data-easing="easeOutBack" data-speed="700" data-start="2000">Take you to every corner of the world</div> -->
                    </li>
                   
                    <?php endforeach;?>    
                    <?php endif;?> 
                    <?php endif;?>

                </ul>
            </div>
        </section>
        <!-- END / HERO SECTION -->


        <!-- SEARCH TABS -->
        <section>
            <div class="container" style="padding-bottom: 80px; margin-top: -350px;   ">
            

                <div class="awe-search-tabs tabs">
                    <ul class="adjust-tabs">
                        <!-- <li>
                            <a href="#awe-search-tabs-1">
                                <i class="awe-icon awe-icon-briefcase"></i>
                            </a>
                        </li> -->
                        <li class="adjust-anchor">
                            <a href="#awe-search-tabs-2">
                                <i class="awe-icon awe-icon-hotel"></i>
                            </a>
                        </li>
                        <!-- <li class="adjust-anchor">
                            <a href="#awe-search-tabs-3">
                                <i class="awe-icon awe-icon-plane"></i>
                             </a>
                        </li> -->
                        <!-- <li>
                            <a href="#awe-search-tabs-4">
                                <i class="awe-icon awe-icon-train"></i>
                            </a>
                        </li> -->
                        <!-- <li>
                            <a href="#awe-search-tabs-5">
                                <i class="awe-icon awe-icon-car"></i>
                            </a>
                        </li> -->
                        <!-- <li class="adjust-anchor">
                            <a href="#awe-search-tabs-6">
                                <i class="awe-icon awe-icon-bus"></i>
                            </a>
                        </li> -->

<!--
                         <li class="adjust-anchor">
                            <a href="#awe-search-tabs-7">
                                <i class="awe-icon awe-icon-briefcase"></i>
                            </a>
                        </li>
-->

                    </ul>
                    <div class="awe-search-tabs__content tabs__content" style="margin-top: -20px; padding-top: 15px; opacity: 0.90; background-color: #fff; ">
                        <!-- <div id="awe-search-tabs-1" class="search-flight-hotel">
                            <h2>Search Flight + Hotel</h2>
                            <form>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>From</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="Ho Chi Minh, Hanoi, Vietnam">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>To</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="Ankara, Turkey">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Depart on</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Check in">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Return on</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Check out">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Adult</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                        <span>12 yo and above</span>
                                    </div>
                                    <div class="form-elements">
                                        <label>Kids</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                        <span>0-11 yo</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Budget</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>All types</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Hotel Rate</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>All types</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" value="Find My Tour">
                                </div>
                            </form>
                        </div> -->
                        <div id="awe-search-tabs-2" class="search-hotel" ng-app="HotelApplication" ng-controller="HotelController" ng-init="" style="background-color: #fff;" >
                            <?php if ( isset($searchErrors['hotel']) ) : ?>
                                <?php $errorHotel = $searchErrors['hotel']; ?>
                                <div class="alert alert-danger" role="alert">
                                    <?php if( gettype($errorHotel) == 'array' ) : ?>
                                        <?php foreach ($errorHotel as $key => $error) : ?>
                                            <?=$error?> <br>
                                        <?php endforeach; ?>
                                    <?php elseif ( gettype($errorHotel) == 'string' ) : ?>
                                        <?=$errorHotel?>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <form action="<?=base_url()?>hotel/search" method="POST">
                                <div class="form-group">
                       
                                        <div class="form-elements">
                                        <label>Destinations</label>
                                        <div class="form-item scrollable-typehead">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <!-- <pre>Model: {{hotelCitySelected | json}}</pre> -->
                                            <input type="text"
                                                autocomplete="off"
                                                class="form-control"
                                                id="location[name]"
                                                name="location[name]"
                                                ng-model="hotelCitySelected"
                                                ng-model-options="data.modelOptions"
                                                placeholder="Search for city"
                                                uib-typeahead="address as address.name for address in getHotelLocation($viewValue)"
                                                typeahead-loading="loadingLocations"
                                                typeahead-no-results="noResults"
                                                ng-value="'<?=(isset($hotel_post)?$hotel_post['location']['name']:'')?>'">
                                            <i ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></i>
                                            <div ng-show="noResults"1>
                                                <i  class="glyphicon glyphicon-remove"></i> No Results Found
                                            </div>
                                            <input type="hidden" id="location[id]" name="location[id]" value="<?=(isset($hotel_post)?$hotel_post['location']['id']:'')?>">
                                            <input type="hidden" id="location[city]" name="location[city]" value="<?=(isset($hotel_post)?$hotel_post['location']['city']:'')?>">
                                            <input type="hidden" id="location[country_code]" name="location[country_code]" value="<?=(isset($hotel_post)?$hotel_post['location']['country_code']:'')?>">
                                        </div>
                                    </div>
                           
                                    <div class="form-elements">
                                        <label>Rooms</label>
                                        <div class="form-item">
                                            <select class="awe-select"
                                                id="rooms"
                                                name="rooms"
                                                ng-model="data.hotel.numberOfRooms"
                                                ng-init="data.hotel.numberOfRooms = '<?=(isset($hotel_post)?count($hotel_post['rooms']):1 )?>'"
                                                ng-change="showLog()">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                 </div>
                                <div class="adjust-padding-64 form-group">

                                    <!-- CHECK IN -->
                                    <div class="form-elements"
                                        ng-init="data.hotel.rooms = <?php if (isset($hotel_post)) { print_r(str_replace("\"", "'", json_encode($hotel_post['rooms']))); } else { echo '[]'; } ?>">
                                        <label>Check in</label>
                                        <div class="form-item" ng-init="data.hotel.search.checkIn = ('<?=(isset($hotel_post)?$hotel_post['start_date']:'Date')?>')">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input id="hotel-startdate"
                                                name="start_date"
                                                type="text"
                                                class="awe-calendar"
                                                autocomplete="off"
                                                ng-model="data.hotel.search.checkIn">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Check out</label>
                                        <div class="form-item" ng-init="data.hotel.search.checkOut = ('<?=(isset($hotel_post)?$hotel_post['end_date']:'Date')?>')">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input id="hotel-enddate"
                                                name="end_date"
                                                type="text"
                                                class="awe-calendar"
                                                autocomplete="off"
                                                ng-value="data.hotel.search.checkOut">
                                        </div>
                                    </div>
                                    <!-- ADULTS -->
                                    <div ng-repeat="room in range(data.hotel.numberOfRooms)">
                                        <div class="form-elements">
                                            <label>Adult/s</label>
                                            <div class="form-item">
                                                <select class="awe-select"
                                                    id="rooms[{{ $index }}][adult]"
                                                    name="rooms[{{ $index }}][adult]"
                                                    ng-model="data.hotel.search.rooms[$index]['adult']"
                                                    ng-init="data.hotel.search.rooms[$index]['adult'] = (data.hotel.rooms.length > 0)?data.hotel.rooms[$index].adult:'2'">
                                                    <option value="1" >1</option>
                                                    <option value="2" >2</option>
                                                    <option value="3" >3</option>
                                                    <option value="4" >4</option>
                                                    <option value="5" >5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-elements">
                                            <label>Children</label>
                                            <div class="form-item"
                                                ng-init="data.hotel.search.rooms[$index]['child']['count'] = (data.hotel.rooms.length > 0)?data.hotel.rooms[$index].child:'0'">
                                                <select class="awe-select"
                                                    id="rooms[{{ $index }}][child]"
                                                    name="rooms[{{ $index }}][child]"
                                                    ng-model="data.hotel.search.rooms[$index]['child']['count']">
                                                    <option value="0" >0</option>
                                                    <option value="1" >1</option>
                                                    <option value="2" >2</option>
                                                    <option value="3" >3</option>
                                                    <option value="4" >4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-elements" style="width: 100%">
                                            <div class="row">
                                                <div class="col-md-2"
                                                    style="width: 20%;"
                                                    ng-repeat="age in range(data.hotel.search.rooms[$index]['child']['count'])"
                                                    ng-init="data.hotel.search.rooms[$parent.$index]['child']['age'][$index] = (data.hotel.rooms.length > 0 && data.hotel.rooms[$parent.$index].childAges[$index].length > 0)?(data.hotel.rooms[$parent.$index].childAges[$index]|convertToNumber):8">
                                                    <label>Age</label>
                                                    <div class="form-item">
                                                        <input type="number"
                                                            id="rooms[{{ $parent.$index }}][childAges][]"
                                                            name="rooms[{{ $parent.$index }}][childAges][]"
                                                            ng-model="data.hotel.search.rooms[$parent.$index]['child']['age'][$index]">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="hidden" name="fromTheMainPage" value="1">
                                    <input type="submit" value="Find My Hotel">
                                </div>
                                <!-- <div class="row">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" name="showSpecialDeals" checked="" value="1" aria-label="Pay At the Hotel" style="border: none;font-weight: initial;color: initial;line-height: initial;height: initial;width: initial;">
                                            </span>
                                            <span class="form-control">Show special deals</span>
                                        </div>
                                    </div>
                                </div> -->

                            </form>
                        </div>
                        <!-- <div id="awe-search-tabs-3" class="search-flight">
                            <h2>Search Flight</h2>
                            <form action="<?=base_url();?>dashboard/flight">
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>From</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="Ho Chi Minh, Hanoi, Vietnam">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>To</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="Ankara, Turkey">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Depart on</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Check in">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Return on</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Check out">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Adult</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                        <span>12 yo and above</span>
                                    </div>
                                    <div class="form-elements">
                                        <label>Kids</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                        <span>0-11 yo</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Budget</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>All types</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" value="Find My Flight">
                                </div>
                            </form>
                        </div> -->

                        <!-- <div id="awe-search-tabs-4" class="search-flight">
                            <h2>Search Train</h2>
                            <form>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>From</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="Ho Chi Minh, Hanoi, Vietnam">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>To</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="Ankara, Turkey">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Depart on</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Check in">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Return on</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Check out">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Adult</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                        <span>12 yo and above</span>
                                    </div>
                                    <div class="form-elements">
                                        <label>Kids</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                        <span>0-11 yo</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Budget</label>
                                        <div class="form-item">
                                            <select class="awe-select">
                                                <option>All types</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" value="Find My Flight">
                                </div>
                            </form>
                        </div>
 -->
                        <!-- <div id="awe-search-tabs-5" class="search-car">
                            <h2>Search Car</h2>
                            <form>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Picking up</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="City, airport...">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Droping off</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="City, airport...">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Pink off</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Drop off</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" value="Find My Car">
                                </div>

                            </form>
                        </div> -->

                        <!-- <div id="awe-search-tabs-6" class="search-bus">
                            <h2>Search Transfer</h2>
                            <form>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Picking up</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="City, airport...">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Droping off</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="City, airport...">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Pink off</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Drop off</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-calendar"></i>
                                            <input type="text" class="awe-calendar" value="Date">
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="form-actions">
                                    <input type="button" value="Find My Car">
                                </div>
                        </div> -->


        
        </section>
        <!-- END / SEARCH TABS -->



       