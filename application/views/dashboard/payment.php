<!-- HERO SECTION -->
        <section class="hero-section">
            <div id="slider-revolution">
                <ul>
                    <li data-slotamount="7" data-masterspeed="500" data-title="Paris">
                        <img src="<?php echo base_url();?>static/images/12.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">

                        <!-- <div class="tp-caption sfb fadeout slider-caption-sub slider-caption-sub-1" data-x="500" data-y="230" data-speed="700" data-start="1500" data-easing="easeOutBack">
                          Last minute deal
                        </div>

                        <div class="tp-caption sfb fadeout slider-caption slider-caption-1" data-x="center" data-y="280" data-speed="700" data-easing="easeOutBack"  data-start="2000">Top discount Paris Hotels</div>
                         -->
                       
                    </li> 

                    <li data-slotamount="7" data-masterspeed="500" data-title="Europe">
                        <img src="<?php echo base_url();?>static/images/13.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">

                        <!-- <div class="tp-caption  sft fadeout slider-caption-sub slider-caption-sub-2" data-x="center" data-y="220" data-speed="700" data-start="1500" data-easing="easeOutBack">
                          Check out the top weekly destination
                        </div>

                        <div class="tp-caption sft fadeout slider-caption slider-caption-2" data-x="center" data-y="260" data-speed="700" data-easing="easeOutBack"  data-start="2000">
                            Travel with us
                        </div>
                        
                         -->
                    </li>

                    <li data-slotamount="7" data-masterspeed="500" data-title="Moscow">
                        <img src="<?php echo base_url();?>static/images/14.jpg" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">

                        <!-- <div class="tp-caption lfl fadeout slider-caption slider-caption-3" data-x="center" data-y="260" data-speed="700" data-easing="easeOutBack"  data-start="1500">
                            Gofar
                        </div>
                        
                        <div href="#" class="tp-caption lfr fadeout slider-caption-sub slider-caption-sub-3" data-x="center" data-y="365" data-easing="easeOutBack" data-speed="700" data-start="2000">Take you to every corner of the world</div> -->
                    </li> 
                   

                </ul>
            </div>
        </section>

          <div id="awe-search-tabs-7" class="payment">
                            <h2>Secure Payment</h2>
                                <div class="form-group">
                                    <div class="form-elements">
                                        <div class="form-item"> 
                                        <label>We accept:</label>                   
                                        </div>
                                    </div>
                                    <img src="http://localhost:3333/static/images/paypal.png" height="10px;">
                                  
                        </div>        
                
                                    <div class="form-group">
                                    <div class="form-elements">
                                         <label>Full Name</label>
                                         <div class="form-item">
                                            <input type="text" value="Enter Fullname">
                                         </div>
                                    </div>
                                    <div class="form-elements">
                                        <label>Card Number</label>
                                        <div class="form-item">
                                            <input type="text" value="Enter Number">
                                        </div>
                                    </div>  
                                     <div class="form-elements">
                                        <label>Address</label>
                                        <div class="form-item">
                                         <i class="awe-icon awe-icon-marker-1"></i>
                                            <input type="text" value="Address">
                                        </div>
                                     </div>    
                                </div>

                                <div class="form-group">
                                    <div class="form-elements">
                                        <label>Country</label>
                                            <div class="form-item">
                                                <select class="awe-select">
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                    <option>Country</option>
                                                </select>                          
                                            </div>
                                    </div>

                                        <div class="form-elements">
                                            <label>Expiration Date</label>
                                                <div class="form-item">
                                                    <i class="awe-icon awe-icon-calendar"></i>
                                                    <input type="text" class="awe-calendar" value="Date">
                                                </div>
                                        </div>  
                                </div>
                                <div class="form-actions">
                                    <input type="button" value="Pay">
                                </div>