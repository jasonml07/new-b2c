<?php

/*function treeview($array, $id = 0)
    {
     //   echo "<ul  class=\"sub-menu\" >";
       for ($i = 0; $i < count($array); $i++)
       {

          if($array[$i]['parent_id']==$id) {
            if($array[$i]['parent_id'] == 0){
                echo "<li class=\"current-menu-parent\"><a href=\"#\">".$array[$i]['label'].'</a></li>';
            }else{
                 echo "<ul class=\"sub-menu\"><li ><a href=\"#\">".$array[$i]['label'].'</a></li></ul>';
            }
             
            treeview($array, $array[$i]['id']);
          }
       }
     //   echo "</ul>";
    }*/
    function printTree($tree,$isSub = false) {
      if(!is_null($tree) && count($tree) > 0) {
            if(!$isSub){
                 echo '<ul class="menu-list">';
            }else{
                 echo '<ul class="sub-menu">';
            }
         
          foreach($tree as $key => $node) {
          //  print_r($node);
            //  break;
             if($node['val']['parent_id']==0){
              echo '<li class="current-menu-parent"><a class="bx-nav-fcolor" href="'.$node['val']['url'].'">'.$node['val']['label']."</a>";
             }else{
               echo '<li><a href="'.$node['val']['url'].'">'.$node['val']['label']."</a>";
             }

              if(!empty($node['sub_arr'])){
                  if(!empty($node['sub_arr'])){
                     printTree($node['sub_arr'],true);
                  }
                
              }
              echo '</li>';
          }
          echo '</ul>';
           
         
      }
  }
?>

<?php if($this->session->userdata('agencycode')):?>
<body style="font-family :<?=(isset($fonts) && !empty($fonts)?$fonts:'Arial')?> !important">
<?php else:?>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TPN294J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif;?>
<!-- PAGE WRAP -->
    <div id="page-wrap" >
        <!-- PRELOADER -->
        <div class="preloader"></div>
        <!-- END / PRELOADER -->

        <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
          <header class="custom-theme-header">
            <?php TZ_custom_theme_header( $this->nativesession->get('agencyRealCode') ) ?>
          </header>
        <?php else: ?>
        <!-- HEADER PAGE -->
        <header id="header-page" class = "">
            <div class="header-page__inner panel-themes">
                <div class="container"  >
                    <!-- LOGO -->
                    <div class="logo">
                      <!-- <?=B2B_IP."uploads/".$img_url?> -->
                        <?php $image = @getimagesize(B2B_IP."uploads/".$img_url) ?>
                        <?php if ( is_array($image) ) :?>
                          <img src="<?php echo B2B_IP."uploads/".$img_url?>">
                        <?php endif; ?>
                    </div>
                    <!-- END / LOGO -->


                    <nav class="adjust-navigation navigation awe-navigation" data-responsive="1200" style="color: #fff!important;">

                      <?php if( isset($memu) && !empty($menu) ) :?>
                        <?php printTree($menu); ?> 
                      <?php else : ?>
                        <?php printTree($this->nativesession->get('headermenu')); ?>
                      <?php endif; ?>
                      
                    </nav>
                    <!-- END / NAVIGATION -->
                    
                    <!-- SEARCH BOX 
                    <div class="search-box">
                        <span class="searchtoggle"><i class="glyphicon glyphicon-search"></i></span>
                        <form class="form-search">
                            <div class="form-item">
                                <input type="text" value="Search &amp; hit enter">
                            </div>
                        </form>
                    </div>-->
                    <!-- END / SEARCH BOX -->


                    <!-- TOGGLE MENU RESPONSIVE -->
                    <a class="toggle-menu-responsive" href="#">
                        <div class="hamburger">
                            <span class="item item-1"></span>
                            <span class="item item-2"></span>
                            <span class="item item-3"></span>
                        </div>
                    </a>
                    <!-- END / TOGGLE MENU RESPONSIVE -->

                </div>
            </div>
        </header>
    <?php endif ?>
        
        <!-- END / HEADER PAGE -->
 