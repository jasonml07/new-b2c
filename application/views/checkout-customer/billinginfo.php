        <section class="checkout-section-demo" style="padding-left:0 !important;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="checkout-page__top">
                            <div class="title">
                                <h1 class="text-uppercase" style="color: #1e1e1f; margin-left: 40px;">Billing Info</h1>
                            </div>
                            <span class="phone" style="color: #0091ea; padding-left: 10px !important;">Support Call: +1-888-8765-1234</span>
                        </div>
                    </div>
                   
                    <div class="col-lg-9">
                        <div class="checkout-page__content">
                            <div class="customer-content" style="background-color: #fff !important; border: 3px solid #f2f2f2; border-radius: 3px; ">

                                <div class="woocommerce-billing-fields">
                                    <h3 style="border-bottom: 4px solid #0091ea !important; background-color: #e6f3ff; color:#1e1e1f; padding-left: 13px;">Room 1</h3>

                                    <div class="col-lg-6" style="padding:1 !important;">
                                         <h4 style="color: #0091ea !important; margin-top: 10px; padding-left: 10px; margin-top: 30px; padding-top: 9px; padding-bottom: 9px; ">Adult 1</h4>
                                        <div class="col-md-3"  style="padding:0 !important;">
                                            <label style="padding-top: 10px !important;color: #1e1e1f;">Title</label>
                                            <!--
                                                <select  style="width:98% !important;background-color:#f2f2f2; color: #1e1e1f;">
                                                    <option>Mr</option>
                                                    <option>Ms</option>
                                                    <option>Mrs</option>
                                                </select>
                                                -->
                                        </div>
                                       <div class="col-md-5" style="padding:10 !important;">
                                            <label style="padding-top: 10px !important;color: #1e1e1f;">First name</label>
                                          <!--  <input type="text" style="width:98% !important;background-color:#fff;"> -->
                                        </div>
                                        <div class="col-md-4" style="padding:0 !important;">
                                            <label style="padding-top: 10px !important;color: #1e1e1f;">Last name</label>
                                           <!-- <input type="text" style="width:98% !important;background-color:#fff; border-radius: 2px;"> -->
                                        </div>
                                        <h4 style=" margin-top: 110px!important; color: #0091ea; padding-left: 10px; padding-top: 9px; padding-bottom: 9px;">Adult 2</h4>
                                        <div class="col-md-3"  style="padding:0 !important;">
                                            <label style="padding-top: 10px !important;color: #1e1e1f;">Title</label>
                                           <!-- <select  style="width:98% !important;background-color:#EEE;color: #1e1e1f;">
                                            
                                                <option>Mr</option>
                                                <option>Ms</option>
                                                <option>Mrs</option>
                                            </select>
                                            -->
                                        </div>
                                       <div class="col-md-5" style="padding:10 !important;">
                                            <label style="padding-top: 10px !important;color: #1e1e1f;">First name</label>
                                           <!-- <input type="text" style="width:98% !important; color: #1e1e1f; background-color:#fff;">
                                           -->
                                        </div>
                                        <div class="col-md-4" style="padding:0 !important;">
                                           <!-- <label style="padding-top: 10px !important;color: #1e1e1f;">Last name</label>
                                            <input type="text" style="width:98% !important;background-color:#fff; color: #1e1e1f;">
                                            -->
                                        </div>
                                    </div>
                                    <div class="col-lg-6" style="padding: 4px !important;">
                                       <h4 style="color: #0091ea !important; margin-top: 15px; padding-left: 10px; margin-top: 25px; margin-left: 0px; padding-top: 9px; padding-bottom: 9px;">Child 1</h4>
                                        <div class="col-md-3"  style="padding:0 !important;">
                                         <label  style="padding-top: 10px !important;color: #1e1e1f; padding-left: 20px;">Title</label>
                                            <!--
                                            <select  style="width:98% !important;background-color:#EEE;color: #1e1e1f;">
                                            
                                                <option>Mr</option>
                                                <option>Ms</option>
                                                <option>Mrs</option>
                                            </select>
                                            -->
                                        </div>
                                       <div class="col-md-5" style="padding:10 !important;">
                                            <label style="padding-top: 10px !important;color: #1e1e1f;">First name</label>
                                              <!-- <input type="text" style="width:98% !important;background-color:#fff;">
                                              -->
                                        </div>
                                        <div class="col-md-4" style="padding:0 !important;">
                                            <label style="padding-top:10px !important; color: #1e1e1f;">Last name</label>
                                               <!--
                                            <input type="text" style="width:98% !important;background-color:#fff;">
                                            -->
                                        </div>

                                    </div>
                                   
                                </div>

                                 <div class="woocommerce-billing-fields">
                                    <div class="col-lg-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;">
                                        <h3 style="border-bottom: 4px solid #0091ea !important; background-color: #e6f3ff; color:#1e1e1f; padding-left: 13px; padding-top: 10px;padding-bottom: 10px">Special Requirements (Optional)</h3>
                                             <label style="margin-top: 30px; color: #1e1e1f; margin-left: 30px;">Please write your requests in English</label>
                                                <!--
                                          <textarea style="margin-top: 0px !important; border-color:#0091ea; width: 90%; margin-left: 30px;">No-smoking</textarea> -->

                                   </div>
                                </div>

                                <div class="woocommerce-billing-fields">
                                    <div class="col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;">
                                    <h3 style="border-bottom: 4px solid #0091ea!important; background-color: #e6f3ff; color:#1e1e1f;padding-top: 10px;padding-bottom: 10px;  padding-left: 13px;">Voucher Information</h3>
                                      <div class="col-md-2"  style="padding:0 !important;">
                                            <label style="padding-top: 30px !important;color: #1e1e1f;font-size: 14px; padding-left: 5px; margin-left: 20px;">Title</label>
                                              <!-- <select style="width:70% !important;background-color:#EEE;color: #1e1e1f; padding-left: 10px; margin-left: 20px;">
                                            
                                                <option>Mr</option>
                                                <option>Ms</option>
                                                <option>Mrs</option>
                                            </select>
                                            -->
                                        </div>
                                        <div class="col-md-5" style="padding: 8px !important;">
                                            <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >First name</label>
                                               <!--
                                            <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;">
                                            -->
                                        </div>
                                         <div class="col-md-5" style="padding:8px !important;">
                                            <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Last name</label>
                                              <!-- <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;">
                                              -->
                                        </div>
                                        <div class="col-md-2" style="padding:0 !important;">
                                        </div>
                                         <div class="col-md-5" style="padding:0 !important;">
                                            <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 48px;font-size: 14px;">Email Address</label>
                                               <!--
                                            <input type="text" style="width:74% !important;background-color:#fff;margin-left: 50px;">
                                            -->
                                        </div>
                                        <div class="col-md-5" style="padding:10 !important;">
                                            <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 33px;font-size: 14px;">Phone Number</label>
                                              <!-- <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;">
                                              -->
                                        </div>
                                   </div>
                                </div>

                                 <div class="woocommerce-billing-fields">
                                    <div class="col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;  ">
                                        <h3 style="border-bottom: 4px solid #0091ea!important; background-color: #e6f3ff; color:#1e1e1f;padding-top: 10px;padding-bottom: 10px;  padding-left: 13px;">Payment Form</h3> 
                                            <div class="col-md-6" style="padding: 8px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >Cardholder Name</label>
                                                      <!-- <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;">
                                                      -->
                                                 <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Email Address</label>
                                                     <!--  <input type="text" style="width:78% !important;background-color:#fff;margin-left: 40px;">
                                                     -->
                                                <label style="padding-top: 25px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Card Verification Number</label>
                                                      <!-- <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;">
                                                      -->
                                               <br> <a href="#" style="color: #0091e1 !important;padding-left: 40px; padding-top: 20px;">More Information</a>

                                            </div>

                                         <div class="col-md-6" style="padding:8px !important;">
                                            <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Card Number</label>
                                                 <!--  <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"> -->
                                            <label style="color: #1e1e1f;padding-left: 40px;font-size: 14px;padding-top: 20px;">Payment Amount</label>
                                                  <!-- <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px; ;"> -->
                                             <label style="padding-top: 25px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Expiry Date</label>
                                                <div>
                                                    <!-- 
                                                     <input type="text"style="width:15% !important;background-color:#fff;margin-left: 43px;"> -->
                                                 <!--     <input type="text"style="width:15% !important;background-color:#fff;margin-left: 3px;"> e.g  02/18 -->
                                                </div>
                                        </div>
                                        
                                   </div>
                                </div>

                                <div id="payment">
                                    <h3 style="border-bottom: 4px solid #0091ea !important; background-color: #e6f3ff; color:#1e1e1f; padding-left: 13px; padding-top: 10px;padding-bottom: 10px; margin-top: 10px;">Cancellation Policy</h3>
                                     <div>
                                           <div class="col-md-12" >
                                            <input id="payment_method_bacs" type="radio" name="payment_method">
                                            <label  style="padding: 8px !important; margin-top: 20px; padding-bottom: 20px;" for="payment_method_bacs" >Refundable </label>
                                            <div class="payment_box payment_method_bacs"><p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                                            </div>
                                       
                                            <input id="payment_method_cheque" type="radio" name="payment_method">
                                            <label style="padding: 8px !important; margin-top: 20px; padding-bottom: 20px;" for="payment_method_cheque">Non-refundable</label>
                                            <div class="payment_box payment_method_cheque"><p>Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
                                            </div>
                                            </div>
                                    </div>
                                     <div>
                                        <h3 id="ship-to-different-address">
                                            <input id="ship-to-different-address-checkbox" type="checkbox" checked="checked">
                                            <label style="color: #0091ea !important; margin-top: 40px;font-size: 14px;" for="ship-to-different-address-checkbox">I have read the cancellation policy</label>
                                            
                                        </h3>
                                    </div>
                                    <div class="form-row place-order">
                                        <form action="<?=base_url();?>billinginfo">
                                      <!--  <input type="submit" class="button alt" id="place_order" value="Billing Info"> -->
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                     <div class="col-lg-3">
                       <div class="booking-info" style="border: 4px solid #0091ea !important; background-color: #cce6ff; margin-left: 30px; width: 100%;">
                                <h3 style="color: #fff !important; font-size: 12px;">Thank You for </h3>
                                <div class="form-group">
                               
                            <!--    <div class="form-submit" style="left: -4px !important;">
                                    <div class="add-to-cart">
                                        <button type="submit" style="background-color: #0091ea !important;left: -9;">
                                            <i class="awe-icon awe-icon-cart"></i>Add this to Cart
                                        </button>
                                    </div>
                                --> 
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </section>
