<script type="text/javascript">
	// console.log(<?=json_encode($booker_info)?>);
</script>

		<div class="container">

            <?php if ($booker_info['response'][0]['success'] == 0) : ?>
                <div class="clearfix">&nbsp;</div>
                <div class="alert alert-danger">
                    Sorry, there was an error while proccessing your request<br>
                    <a href="<?=base_url()?>/dashboard/hotel" class="btn btn-primary">Click me!</a> to redirect to the search result.
                </div>
            <?php else : ?>
			<div class="page-header">
				<h1>A preview on your booking request
                <?php if ( isset($_GET['booking_id']) ) : ?>
                        <a href="<?=base_url()?>dashboard/bookingDetails/<?=$_GET['booking_id']?>" class="btn btn-success">Return to your booking <span class="fa fa-arrow-circle-right"></span></a>
                <?php endif; ?>
			</div>
			<div class="row">
                <!-- response[0]['content']['reservation'][0]['content']['item'] -->
                <?php foreach ($booker_info['response'][0]['content']['reservation'][0]['content']['item'] as $infokey => $info) : ?>
                    <?php if ( isset($info['content']['quantity'][0]) && intval($info['content']['quantity'][0]) > 1 ) : ?>
                        <?php $quantity = intval($info['content']['quantity'][0]); ?>
                        <!-- item quantity is more than 1 -->
                        <?php foreach (range(0, ($quantity - 1)) as $byQuantity => $quantityValue) : ?>
                            <div class="col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <?=$info['content']['name'][0]?> <small>(<?=$info['content']['board'][0]['content']?>)</small>
                                        <span class="label label-danger pull-right"><?=$info['status']?></span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row" style="color: #666;">
                                            <div class="col-md-12" style="padding: 0px;">
                                                <!-- Loop throug the pax-groups -->
                                                <ul class="list-group">
                                                    <?php foreach ($info['content']['pax-groups'][0]['pax-group'][$byQuantity]['content']['pax'] as $key => $pax) : ?>
                                                        <li class="list-group-item"><?=($pax['content']['title'][0] == 'C')?'':$pax['content']['title'][0] . '.'?> <?=$pax['content']['firstName'][0]?> <?=$pax['content']['lastName'][0]?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                            <div class="col-md-12">
                                                <!-- Loop from the cancellation policy -->
                                                <?php foreach ($info['content']['cancellation'][0]['frame'] as $key => $policy) : ?>
                                                    <?php if ( date('Y', strtotime($policy['endTime'])) == 1970 ) : ?>
                                                        <?php $date = strtotime("now"); ?>
                                                    <?php else : ?>
                                                        <?php $date = strtotime($policy['endTime'] . ' -1 day'); ?>
                                                    <?php endif; ?>
                                                    <?=ucfirst($policy['type'])?> <small>(<?=date('F d, Y', $date)?>)</small><br>
                                                <?php endforeach; ?>
                                                <?php if ($info['content']['booking'][0]['billable'] == 1) : ?><span class="label label-primary"><span class="fa fa-check"></span> Billable</span><?php endif; ?>
                                                <?php if ($info['content']['booking'][0]['payAtTheHotel'] == 1) : ?><span class="label label-primary"><span class="fa fa-check"></span> Pay at the hotel</span><?php endif; ?>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                            <div class="col-md-12 text-center" style="font-size: 2em;">
                                                Amount: <span class="fa fa-<?=($info['content']['booking'][0]['content']['price'][0]['currency'] != 'AUD')?strtolower($info['content']['booking'][0]['content']['price'][0]['currency']):'usd'?>"></span> <?=number_format(($info['content']['booking'][0]['content']['price'][0]['new_amount'] / $quantity), 2, '.', '')?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <!-- item quantity is more than 1 -->
                    <?php else : ?>
        				<div class="col-md-4">
        					<div class="panel panel-primary">
        						<div class="panel-heading">
                                    <?=$info['content']['name'][0]?> <small>(<?=$info['content']['board'][0]['content']?>)</small>
                                    <span class="label label-danger pull-right"><?=$info['status']?></span>
        						</div>
        						<div class="panel-body">
                                    <div class="row" style="color: #666;">
                                        <div class="col-md-12" style="padding: 0px;">
                                            <!-- Loop throug the pax-groups -->
                                            <ul class="list-group">
                                                <?php foreach ($info['content']['pax-groups'][0]['pax-group'][0]['content']['pax'] as $key => $pax) : ?>
                                                    <li class="list-group-item"><?=($pax['content']['title'][0] == 'C')?'':$pax['content']['title'][0] . '.'?> <?=$pax['content']['firstName'][0]?> <?=$pax['content']['lastName'][0]?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-12">
                                            <!-- Loop from the cancellation policy -->
                                            <?php foreach ($info['content']['cancellation'][0]['frame'] as $key => $policy) : ?>
                                                <?php if ( date('Y', strtotime($policy['endTime'])) == 1970 ) : ?>
                                                    <?php $date = strtotime("now"); ?>
                                                <?php else : ?>
                                                    <?php $date = strtotime($policy['endTime'] . ' -1 day'); ?>
                                                <?php endif; ?>
                                                <?=ucfirst($policy['type'])?> <small>(<?=date('F d, Y', $date)?>)</small><br>
                                            <?php endforeach; ?>
                                            <?php if ($info['content']['booking'][0]['billable'] == 1) : ?><span class="label label-primary"><span class="fa fa-check"></span> Billable</span><?php endif; ?>
                                            <?php if ($info['content']['booking'][0]['payAtTheHotel'] == 1) : ?><span class="label label-primary"><span class="fa fa-check"></span> Pay at the hotel</span><?php endif; ?>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        <div class="col-md-12 text-center" style="font-size: 2em;">
                                            Amount: <span class="fa fa-<?=($info['content']['booking'][0]['content']['price'][0]['currency'] != 'AUD')?strtolower($info['content']['booking'][0]['content']['price'][0]['currency']):'usd'?>"></span> <?=number_format($info['content']['booking'][0]['content']['price'][0]['new_amount'], 2, '.', '')?>
                                        </div>
                                    </div>
                                </div>
        					</div>
        				</div>
                    <?php endif; ?>
                <?php if ( ($infokey+1) %3 == 0 ) : ?> <div class="clearfix"></div> <?php endif; ?>
                <?php endforeach; ?>
			</div>
            <?php endif; ?>
		</div>