
<section class="container" style="font:'OpenSans';background: #fff;">
<h2 class="text-left xxh-Bold">Booking List: </h2>
<br>
<table class="table table-striped table-bordered" cellspacing="0" id="bookinglist">
    <thead>
      <tr>
        <th>Booking Id</th>
        <!-- <th>Booking Consultant</th> -->
        <th>Consultant</th>
        <th>Booking Created Date</th>
        <th>Status</th>
        <th>Total Due Amount</th>
        <th>Total Deposited Amount</th>
        <th>Total Balance Amount</th>
        <th>Items</th>
        <th>Views</th>
        <th>Payments</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($bookings as $key=>$book): ?>
      <tr>
        <td><?= $book['booking_id']; ?></td>
       <!--  <td><?= $book['booking_consultant_name']; ?></td> -->
        <td><?= $book['booking_agency_consultant_name']; ?></td>
        <td><?= $book['booking_creation_date']; ?></td>
        <td>
          <?php
          if($book['booking_status'] == 'Confirmed'){
            $color ="green";
          }elseif($book['booking_status'] == 'Quote'){
             $color ="orange";
          }else{
            $color ="red";
          }
          ?>
          <span style="color:<?= $color; ?>;">
            <?= $book['booking_status']; ?>
          </span>
        </td>
        <td><?= number_format($book['booking_dueAmount'],2,'.',''); ?> AUD</td>
        <td><?= number_format($book['booking_deposited_Amount'],2,'.',''); ?> AUD</td>
         <td><span style="color:red;font-size: 15px;"><?= number_format($book['booking_dueAmount']-$book['booking_deposited_Amount'],2,'.',''); ?> AUD</span></td>
        <td><?= $book['itemNumber']; ?></td>
        <td><a href="<?= base_url();?>dashboard/bookingDetails/<?= $book['booking_id']; ?>"><button id="btnContactUs" class="btn btn-warning" style="font-size:10px;">View</button></a> </td>
        <td><a href="<?= base_url();?>dashboard/bookingPayments/<?= $book['booking_id']; ?>"><button id="btnContactUs" style="font-size:10px;" class="btn btn-danger" >Pay Now</button></a></td>
      </tr>
     <?php endforeach; ?>
    </tbody>
  </table>
</section>
<br></br>