<!-- BREADCRUMB -->
        <section>
            <div class="container">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">&nbsp;</a></li>
                        <li><a href="<?=base_url();?>dashboard/bookingDetails/<?=$item['bookingId'];?>"> Click Here To Booking Details &amp; Payments</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- BREADCRUMB -->

        
        <section class="product-detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="product-detail__info">
                            <div class="product-title">
                                <h2><?=$item['itemName'];?></h2>
                                <div class="hotel-star">
                                    <?php for($i=0;$i<(int)$item['itemRatings'];$i++):?>
                                    <i class="fa fa-star"></i>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="product-address">
                                <?php if($item['itemFromCountry'] != ""):?>
                                <span><?=$item['itemFromAddress']?> <?=$item['itemFromCity']?> <?=$item['itemFromCountry']?>
                                <?php endif; ?>

                                 <?php if($item['itemToCountry'] != ""):?>
                                <span> to <?=$item['itemToAddress']?> <?=$item['itemToCity']?> <?=$item['itemToCountry']?>
                                <?php endif; ?>
                                </span>
                            </div>
                           

                            <div class="trips">
                                <div class="item">
                                    <h6>Policy Description</h6>
                                    <?php if(empty($item['itemCancellation'])):?>
                                    <p>No Cancellation Policy</p>
                                    <?php else: ?>
                                     <p>Has Policy</p>
                                    <?php endif; ?>
                                </div>
                                <div class="item">
                                    <h6>Date</h6>
                                    <p><i class="fa fa-clock-o"></i>
                                        <?php

                                        if(is_object($item['itemStartDate']) ){
                                            if((int)$item['itemStartDate']->sec != 39600){
                                                echo date('d/m/Y', $item['itemStartDate']->sec)." ".$item['itemStartTime'];
                                            }
                                            
                                        }
                                       
                                        if(is_object($item['itemEndDate'])){
                                            if((int)$item['itemEndDate']->sec != 39600){
                                                echo " to ".date('d/m/Y', $item['itemEndDate']->sec)." ".$item['itemEndTime'];
                                            }
                                           
                                        }
                                        
                                        ?>
                                    </p>
                                </div>
                                <div class="item">
                                    <h6>Service Type</h6>
                                    <p><?=$item['itemServiceName']?></p>
                                </div>
                               
                            </div>

                            <table class="ticket-price col-md-12" >
                                <thead>
                                    <tr>
                                        <th class="ticket-price"><span>Adult</span></th>
                                        <th class="adult"><span>Child</span></th>
                                        <th class="kid"><span>Infant</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="ticket-price">
                                             <span class="amount"><?=$item['itemAdultMin'];?>-<?=$item['itemAdultMax'];?></span>
                                        </td>
                                        <td class="adult">
                                            
                                                <span class="amount"><?=$item['itemChildMin'];?>-<?=$item['itemChildMax'];?></span>
                                          
                                            
                                        </td>
                                        <td class="kid">
                                           
                                                <span class="amount"><?=$item['itemInfantMin'];?>-<?=$item['itemInfantMax'];?></span>
                                           
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="product-descriptions">
                                <p>&nbsp;</p>
                            </div>
                            <div class="product-descriptions">
                                <p><?=$item['itemSumarryDescription'];?></p>
                            </div>
                            
                            <div class="product-tabs tabs">
                                <?php if(!empty($item['itemCancellation']) || !empty($item['itemSupplements'])): ?>
                                <ul>
                                    <?php if(!empty($item['itemCancellation'])): ?>
                                    <li>
                                        <a href="#tabs-1">Cancellation Policies</a>
                                    </li>
                                    <?php endif; ?>
                                     <?php if(!empty($item['itemCancellation'])): ?>
                                    <li>
                                        <a href="#tabs-2">Supplements</a>
                                    </li>
                                     <?php endif; ?>
                                   
                                </ul>
                                <div class="product-tabs__content">
                                    <?php if(!empty($item['itemCancellation'])): ?>
                                    <div id="tabs-1">
                                        <table class="facilities-freebies-table">
                                        <tbody>
                                            <?php foreach($item['itemCancellation'] as $key=>$policy):?>
                                            <tr>
                                                <th>
                                                    <p>Cancellation between 

                                                        <span style="color:red;"><?=date('d, M Y', $policy['dateFrom']->sec); ?></span> to <span style="color:red;"><?=date('d, M Y', $policy['dateTo']->sec); ?> </span>
                                                        will charge 
                                                        <?php if($policy['typeCancellation'] == "Percentage"):?>
                                                        <span style="color:red;"><?=$policy['percent'];?>%</span> from the total amount due.
                                                        <?php else:?>
                                                        <span style="color:red;"><?=number_format($policy['amount'],2,'.','')?></span>.
                                                        <?php endif; ?>
                                                    </p>
                                                </th>
                                               
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                        
                                    </div>
                                     <?php endif; ?>
                                     <?php if(!empty($item['itemCancellation'])): ?>
                                    <div id="tabs-2">
                                        <table class="room-type-table">
                                            <thead>
                                                <tr>
                                                    <th class="room-type">Supplement Name</th>
                                                    <th class="room-people">Qty</th>
                                                    <th class="room-price">Amount</th>
                                                 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($item['itemSupplements'] as $key=>$supple):?>
                                                <tr>
                                                    <td class="room-type">
                                                       
                                                       <span style="font-size:14px"> <?=$supple['supplement_name']?></span>
                                                    </td>
                                                    <td class="room-condition">
                                                       <span style="font-size:12px"> <?=$supple['supplement_qty']?></span>
                                                       
                                                    </td>
                                                    
                                                    <td class="room-price">
                                                        <div class="price">
                                                            <span class="amount" style="color:red;">$<?=number_format($supple['supplement_costings']['totalDueAmt'],2,'.','');?></span>
                                                           
                                                           
                                                        </div>
                                                        
                                                    </td>
                                                </tr>
                                               <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                     <?php endif; ?>
                                </div>
                                
                                <?php endif; ?>
                            </div>
                            <div class="trip-schedule-accordion accordion">
                                <?php if($item['itemDetailedDescription'] != ""): ?>
                                <h3>Detail Description</h3>
                                <div>
                                    <p><?=$item['itemDetailedDescription'];?></p>
                                </div>
                                 <?php endif; ?>
                                  <?php if($item['itemCostingsDescription'] != ""): ?>
                                <h3>Costing Description</h3>
                                <div>
                                    <p><?=$item['itemCostingsDescription'];?></p>
                                </div>
                                <?php endif; ?>
                                 <?php if($item['itemInclusionDescription'] != ""): ?>
                                <h3>Inclussion Description</h3>
                                <div>
                                    <p><?=$item['itemInclusionDescription'];?></p>
                                </div>
                                <?php endif; ?>
                                 <?php if($item['itemExclusionDescription'] != ""): ?>
                                <h3>Exclusion Description</h3>
                                <div>
                                    <p><?=$item['itemExclusionDescription'];?></p>
                                </div>
                                <?php endif; ?>
                                 <?php if($item['itemConditionsDescription'] != ""): ?>
                                <h3>Condition Description</h3>
                                <div>
                                    <p><?=$item['itemConditionsDescription'];?></p>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        
                    </div>
                    <div class="col-md-3">
                        <div class="detail-sidebar">
                            <a href="<?=base_url();?>dashboard/bookingPayments/<?=$item['bookingId'];?>"><button id="btnContactUs" class="btn btn-warning" style="font-size:14px;width:100%;margin-bottom:5px;" ><b>Make Payments</b></button>
                            <div class="booking-info">
                                <h3>Item Costing Info</h3>
                                
                                
                                <div class="price">
                                    <em><b>Total  Amount</b></em>
                                    $<?=number_format($item['itemCostings']['grossAmt'],2,'.','');?>
                                     <em><b>Total  Less Amount</b></em>
                                   $<?=number_format($item['itemCostings']['totalLessAmt'],2,'.','');?>
                                     <em><b>Total Due Amount</b></em>
                                    <span class="amount">$<?=number_format($item['itemCostings']['totalDueAmt'],2,'.','');?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                    
               
                
               
            </div>
        </section>