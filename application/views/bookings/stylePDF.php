/*
	 CSS-Tricks Example
	 by Chris Coyier
	 http://css-tricks.com
*/

* { margin: 0; padding: 0; }
body { font-size: 12px 'Open Sans', serif; color: #262626;}

textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
table { border-collapse: collapse; }
table td, table th { border: 1px solid black; padding: 5px; }


.header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }
.logo img{ text-align: right; float: left; position: relative; margin-top: 10px; border: 1px solid #fff;  overflow: hidden; }
.address { text-transform: uppercase; font: 13px Helvetica, Sans-Serif;font-size: 12px;}
.invoice-detail { float: left;overflow: hidden;font-size: 12px;}
.invoice-details table{margin-top: -135px; margin-left: 500px; position: relative;font-size: 12px;}
.invoice-date { font-size: 12px; font-weight: bold; float: left;font-size: 12px; }
.data-guests{ font-size: 10px; font-weight: normal; }

#meta { margin-top: 1px; width: 300px; float: right; font-size: 12px;}
#meta  td { text-align: right;  font-size: 12px;}
#meta  tr .meta-head { text-align: left; background: #eee;  font-size: 12px;}
#meta  td div { width: 100%; height: 20px; text-align: right; font-size: 12px;}

.items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black;font: 14px Georgia, Serif; }
.items th { background: #eee; font-size: 12px;}
.items textarea { width: 80px; height: 50px; }
.items td {  border: 0; vertical-align: top; font-size: 12px; }
.items .item-row{  border-style: solid;border-bottom-width: 1px;font-size: 12px;}
.items .item-row td{  font-size: 12px;}
.items .item-row2{  border-style: solid;border-top-width: 1px;}
.items .item-row2 td{  font-size: 12px;}
.items td .description { width: 300px; } 

.items td .item-name { width: 175px; font-size: 12px;}
.items td .receipt-name { width: 175px;font-size: 12px;}
.items td .description textarea, #items td.item-name textarea { width: 100%; font-size: 12px;}

#terms { text-align: left; margin: 20px 0 0 0; font: 14px Georgia, Serif;}
#terms h5 {text-align: center; text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }

.table-head th{ background: #222;  color: white; font-size: 12px;}

/** Voucher **/

.voucher-row {
	border: 1px;
	font-size: 12px;
}

.voucher-header{
	background: #222;
	color: white;
	font-size: 12px;
}
.voucher-note td{
	border: 0;
	border-collapse:collapse;
	font-size: 12px;
}


.pull-left{
  float: left;
  width: 50%;

}
.pull-right{
  float: right;
  width: 300px;
  margin-right: 50px;
}
/**/





