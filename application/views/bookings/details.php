<!-- BREADCRUMB -->
       
        <!-- BREADCRUMB -->

        
        <section >
            <div class="container">
                   
                    
                    
                <div class="row">
                    <div class="col-md-9">
                        <div class="product-tabs tabs">
                            <ul>
                                <li>
                                    <a href="#tabs-1">Item Details</a>
                                </li>
                                <li>
                                    <a href="#tabs-2">Guests</a>
                                </li>
                                <li>
                                    <a href="#tabs-3">Receipts</a>
                                </li>
                                <li>
                                    <a href="#tabs-4">Costings</a>
                                </li>
                                <li>
                                    <a href="#tabs-5">Itinerary</a>
                                </li>
                                <li>
                                    <a href="#tabs-6">Vouchers</a>
                                </li>

                               
                            </ul>
                            <div class="product-tabs__content">
                                <div id="tabs-1">
                                    <div >
                                        
                                    </div>
                                    <table class="room-type-table">
                                        <thead>
                                            <tr>
                                                <th class="room-type">Item Name</th>
                                                <th class="room-people">Service</th>
                                                <th class="room-condition">Start Date</th>
                                                <th class="room-condition">End Date</th>
                                                 <th class="room-people">Status</th>
                                                <th class="room-price">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($ItemDatas as $key=>$item):?>
                                            <tr>
                                                <td class="room-type">
                                                   
                                                    <div class="room-title">

                                                        <?php if((int)$item['itemIsLive'] ==1):?>
                                                       <h4><a href="<?=base_url();?>dashboard/itemDetails/<?=$item['bookingId']?>/<?=$item['itemId']?>"><?=$item['itemName']?></a> <span style="background-color:red;color:#fff !important; font-size: 12px;padding: 0 5px;">LIVE</span></h4>
                                                       <?php else:?>
                                                       <h4><a href="<?=base_url();?>dashboard/itemDetails/<?=$item['bookingId']?>/<?=$item['itemId']?>"><?=$item['itemName']?></a> <span style="background-color:blue;color:#fff !important; font-size: 12px;padding: 0 5px;">SYS</span></h4>
                                                       
                                                        <?php endif;?>
                                                       
                                                    </div>
                                                    <div class="room-descriptions">
                                                        <p style="font-size:9px"><?=$item['itemFromAddress']?> <?=$item['itemFromCity']?> <?=$item['itemFromCountry']?></p>
                                                       
                                                    </div>
                                                   <!--  <div class="room-type-footer">
                                                        <i class="awe-icon awe-icon-gym"></i>
                                                        <i class="awe-icon awe-icon-car"></i>
                                                        <i class="awe-icon awe-icon-food"></i>
                                                        <i class="awe-icon awe-icon-level"></i>
                                                        <i class="awe-icon awe-icon-wifi"></i>
                                                    </div> -->
                                                    <?php if(empty($item['itemCancellation'])):?>
                                                    <span style="font-size:12px;color:orange;">No Cancellation Policy</span>
                                                    <?php else: ?>
                                                    <span style="font-size:12px;color:blue;"><a href="<?=base_url();?>dashboard/itemDetails/<?=$item['bookingId']?>/<?=$item['itemId']?>" style="font-size:12px;color:blue;">View Cancellation Policy</a></span>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="room-condition">
                                                   <span style="font-size:12px"> <?=$item['itemServiceName']?></span><br>
                                                   <a href="<?=base_url();?>dashboard/itemDetails/<?=$item['bookingId']?>/<?=$item['itemId']?>" style="color:blue;font-size:12px">View Item Details</a>
                                                </td>
                                                <td class="room-condition">
                                                   <span style="font-size:12px"> <?php
                                                   if(is_object($item['itemStartDate'])){
                                                    echo date('d/m/Y', $item['itemStartDate']->sec)." ".$item['itemStartTime'];
                                                   }
                                                   
                                                   ?></span>
                                                </td>
                                                 <td class="room-condition">
                                                   <span style="font-size:12px"><?php
                                                  if(is_object($item['itemEndDate'])){
                                                   echo date('d/m/Y', $item['itemEndDate']->sec)." ".$item['itemEndTime'];
                                               }
                                                   ?></span>
                                                </td>
                                                 <td class="room-condition">
                                                    <?php
                                                    $str = "Quote";
                                                    if(1 == (int)$item['itemIsCancelled'] ) {
                                                        $str = "<span style=\"font-size:12px;color:red;\">Cancelled</span>";

                                                    }else if((int)$item['itemIsPaxAllocated'] == 1 && (int)$item['itemIsConfirmed'] == 0 && (int)$item['itemIsCancelled']== 0){
                                                        $str = "<span style=\"font-size:12px;color:orange;\">Quote/Pax Allocated</span>";
                                                    }
                                                    else if((int)$item['itemIsPaxAllocated'] == 1 && (int)$item['itemIsConfirmed'] == 1 &&(int)$item['itemIsCancelled'] == 0){
                                                        $str = "<span style=\"font-size:12px;color:green;\">Confirmed</span>";
                                                    }
                                                    
                                                    echo $str;
                                                    ?>


                                                </td>
                                                <td class="room-price">
                                                    <div class="price">
                                                        <span class="amount" style="color:red;">$<?=number_format($item['itemCostings']['totalDueAmt'],2,'.','');?></span>
                                                       
                                                       
                                                    </div>
                                                    <?php if((int)$item['itemIsLive'] ==1):?>
                                                    <button id="btnContactUs" class="btn btn-warning" style="font-size:12px;" >Cancel Item</button>
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                           <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="tabs-2">
                                    <table class="good-to-know-table">
                                        <tbody>
                                            <?php foreach($ItemDatas as $key=>$item):?>
                                            <?php
                                            if($item['itemServiceCode'] == 'SFEE' || $item['itemServiceCode'] == 'SSFEE'){
                                                continue;
                                            }
                                            ?>
                                            <tr>
                                                <th>
                                                    <p><?=$item['itemName']?></p>
                                                </th>
                                                <td style="vertical-align:top;">
                                                    
                                                    <?php foreach($item['itemGuests'] as $key=>$guest):?>
                                                    <?=$guest['guest_title'];?> <?=$guest['guest_first_name'];?> <?=$guest['guest_last_name'];?><br>
                                                    <?php endforeach;?>
                                                    
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                           
                                        </tbody>
                                    </table>
                                </div>
                                <div id="tabs-3">
                                     <table class="room-type-table">
                                        <thead>
                                            <tr>
                                                <th class="room-condition">Receipt ID</th>
                                                <th class="room-people">Created Date</th>
                                                <th class="room-condition">Receipt Date</th>
                                                <th class="room-condition">Type</th>
                                                 <th class="room-type">Description</th>
                                                <th class="room-price">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($ReceiptDatas as $key=>$receipt):?>
                                            <tr>
                                                <td class="room-condition">
                                                   
                                                     <span style="font-size:12px"> <?=$receipt['receiptId']?></span>
                                                </td>
                                                <td class="room-condition">
                                                   <span style="font-size:12px"> <?php
                                                   if(is_object($receipt['created_datetime'])){
                                                    echo date('d/m/Y', $receipt['created_datetime']->sec);
                                                   }
                                                   
                                                   ?></span>
                                                   
                                                </td>
                                                <td class="room-condition">
                                                    <span style="font-size:12px"> <?php
                                                   if(is_object($receipt['receipt_datetime'])){
                                                    echo date('d/m/Y', $receipt['receipt_datetime']->sec);
                                                   }
                                                   
                                                   ?></span>
                                                </td>
                                                 <td class="room-condition">
                                                    <span style="font-size:12px"> <?=$receipt['receiptTypeName']?></span>
                                                </td>
                                                 <td class="room-type">
                                                     <span style="font-size:12px"> <?=$receipt['receiptDescription']?></span>
                                                </td>
                                                <td class="room-price">
                                                    <div class="price">
                                                        <span class="amount">$<?=number_format($receipt['receiptDepositedAmout'],2,'.','');?></span>
                                                       
                                                    </div>
                                                </td>
                                            </tr>
                                           <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div id="tabs-4">
                                     <div >
                                        <span style="color:red;"><b>Note:</b> Costing is available upon request. Please email us for the request.</span>
                                    </div>
                                    <table class="room-type-table">
                                        <thead>
                                            <tr>
                                                <th class="room-condition">Costing Id</th>
                                               <th class="room-condition">Costing Reference</th>
                                                <th class="room-condition">Created Date</th>
                                                 <th class="room-type">Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($CostingsDatas as $key=>$costing):?>
                                           
                                            <tr>
                                                <td class="room-condition">
                                                    <span style="font-size:12px"> <?=$costing['costingsId']?></span>
                                                   
                                                </td>
                                                <td class="room-condition">
                                                    <span style="font-size:12px"> <?=$costing['costings_reference']?></span>
                                                   
                                                </td>
                                               
                                                 <td class="room-condition">
                                                   <span style="font-size:12px"><?php
                                                  if(is_object($costing['created_datetime'])){
                                                   echo date('d/m/Y', $costing['created_datetime']->sec);
                                               }
                                                   ?></span>
                                                </td>
                                                 
                                                <td class="room-type">
                                                        
                                                       <a id="btnContactUs" class="btn btn-warning" style="font-size:12px;" data-toggle="modal" data-target="#myModalCosting" onclick='document.getElementById("srcModalCosting").src="<?php echo fileDocsPath;?>costings-<?=$costing['costings_reference'];?>.pdf"'>View</a>
                                                       

                                                       <button id="btnContactUs" class="btn btn-danger" style="font-size:12px;" >Email</button>
                                                      
                                                   
                                                </td>
                                            </tr>
                                           <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                     <!-- Modal -->
                                        <div class="modal fade" id="myModalCosting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                          <div class="modal-dialog" style="width:90%;">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Costing</h4>
                                              </div>
                                              <div class="modal-body">
                                                  <iframe  width="100%" height="300" frameborder="0" allowtransparency="true" id="srcModalCosting" type="application/pdf"></iframe>  
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save</button>
                                              </div>
                                            </div>
                                            <!-- /.modal-content -->
                                          </div>
                                          <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                </div>
                                <div id="tabs-5">
                                    <div >
                                        <span style="color:red;"><b>Note:</b> Itinerary is available upon request. Please email us for the request.</span>
                                    </div>
                                     <table class="room-type-table">
                                        <thead>
                                            <tr>
                                                <th class="room-condition">Itinerary Id</th>
                                               <th class="room-condition">Itinerary Reference</th>
                                                <th class="room-condition">Created Date</th>
                                                 <th class="room-type">Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($ItineraryDatas as $key=>$itinerary):?>
                                           
                                            <tr>
                                                <td class="room-condition">
                                                    <span style="font-size:12px"> <?=$itinerary['itineraryId']?></span>
                                                   
                                                </td>
                                                <td class="room-condition">
                                                    <span style="font-size:12px"> <?=$itinerary['itinerary_reference']?></span>
                                                   
                                                </td>
                                               
                                                 <td class="room-condition">
                                                   <span style="font-size:12px"><?php
                                                  if(is_object($itinerary['created_datetime'])){
                                                   echo date('d/m/Y', $itinerary['created_datetime']->sec);
                                               }
                                                   ?></span>
                                                </td>
                                                 
                                                <td class="room-type">
                                                        
                                                       <a id="btnContactUs" class="btn btn-warning" style="font-size:12px;" data-toggle="modal" data-target="#myModalItinerary" onclick='document.getElementById("srcModalItinerary").src="<?php echo fileDocsPath;?>itinerary-<?=$itinerary['itinerary_reference'];?>.pdf"'>View</a>
                                                       

                                                       <button id="btnContactUs" class="btn btn-danger" style="font-size:12px;" >Email</button>
                                                      
                                                   
                                                </td>
                                            </tr>
                                           <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                     <!-- Modal -->
                                        <div class="modal fade" id="myModalItinerary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                          <div class="modal-dialog" style="width:90%;">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Itinerary</h4>
                                              </div>
                                              <div class="modal-body">
                                                  <iframe  width="100%" height="300" frameborder="0" allowtransparency="true" id="srcModalItinerary" type="application/pdf"></iframe>  
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save</button>
                                              </div>
                                            </div>
                                            <!-- /.modal-content -->
                                          </div>
                                          <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                </div>
                                <div id="tabs-6">
                                     <table class="room-type-table">
                                        <thead>
                                            <tr>
                                                <th class="room-condition">Item Name</th>
                                                <th class="room-people">Voucer ID</th>
                                                <th class="room-condition">Voucher Reference</th>
                                                <th class="room-condition">Created Date</th>
                                                 <th class="room-type">Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($ItemDatas as $key=>$item):?>
                                            <?php if(empty($item['itemVoucher'])){continue;}?>
                                            <tr>
                                                <td class="room-condition">
                                                    <span style="font-size:12px"> <?=$item['itemName']?></span>
                                                   
                                                </td>
                                                <td class="room-condition">
                                                   <span style="font-size:12px"> <?=$item['itemVoucher']['voucherId']?></span>
                                                   
                                                </td>
                                                <td class="room-condition">
                                                    <span style="font-size:12px"> <?=$item['itemVoucher']['voucher_reference']?></span>
                                                </td>
                                                 <td class="room-condition">
                                                   <span style="font-size:12px"><?php
                                                  if(is_object($item['itemVoucher']['created_datetime'])){
                                                   echo date('d/m/Y', $item['itemVoucher']['created_datetime']->sec);
                                               }
                                                   ?></span>
                                                </td>
                                                 
                                                <td class="room-type">
                                                        
                                                       <a id="btnContactUs" class="btn btn-warning" style="font-size:12px;" data-toggle="modal" data-target="#myModalVoucher" onclick='document.getElementById("srcModalVoucher").src="<?php echo fileDocsPath;?>voucher-<?=$item['itemVoucher']['voucher_reference'];?>.pdf"'>View</a>
                                                       

                                                       <button id="btnContactUs" class="btn btn-danger" style="font-size:12px;" >Email</button>
                                                      
                                                   
                                                </td>
                                            </tr>
                                           <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <!-- Modal -->
                                        <div class="modal fade" id="myModalVoucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                          <div class="modal-dialog" style="width:90%;">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Voucher</h4>
                                              </div>
                                              <div class="modal-body">
                                                  <iframe  width="100%" height="300" frameborder="0" allowtransparency="true" id="srcModalVoucher" type="application/pdf"></iframe>  
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save</button>
                                              </div>
                                            </div>
                                            <!-- /.modal-content -->
                                          </div>
                                          <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                </div>
                            </div>
                            

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="detail-sidebar">
                            <a href="<?=base_url();?>dashboard/bookingPayments/<?=$BookingData['booking_id'];?>"><button id="btnContactUs" class="btn btn-warning" style="font-size:14px;width:100%;margin-bottom:5px;" ><b>Make Payments</b></button>
                            <div class="booking-info">
                                <h3>Booking info</h3>
                                <div class="form-elements form-room">
                                     <label>Agency: </label> <?=$BookingData['booking_agency_name'];?><br>
                                    <label>Booking #: </label> <?=$BookingData['booking_id'];?><br>
                                     <?php if($BookingData['startDate'] != "" && $BookingData['endDate'] != ""): ?>
                                        <label>Service Start Date: </label> <?=$BookingData['startDate'];?><br>
                                        <label>Service End Date: </label> <?=$BookingData['endDate'];?><br>
                                    <?php endif; ?>
                                    <label>Booking Consultant: </label> <?=$BookingData['booking_consultant_name'];?><br>
                                    <label>Agency Consultant: </label> <?=$BookingData['booking_agency_consultant_name'];?><br>
                                    
                                </div>
                                
                                <div class="price">
                                    <em><b>Total Amount</b></em>
                                    $<?=number_format($BookingData['totalPrice'],2,'.','');?>
                                     <em><b>Total Paid Amount</b></em>
                                   $<?=number_format($BookingData['totalDeposited'],2,'.','');?>
                                     <em><b>Total Due Amount</b></em>
                                    <span class="amount">$<?=number_format($BookingData['totalPrice']-$BookingData['totalDeposited'],2,'.','');?></span>
                                </div>
                              
                            </div>
                            
                    </div>
                </div>
            </div>
        </section>