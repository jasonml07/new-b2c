<!-- BREADCRUMB -->
       <section>
            <div class="container">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">&nbsp;</a></li>
                        <li><a href="<?=base_url();?>dashboard/bookingDetails/<?=$BookingData['booking_id'];?>"> Click Here To View Booking Info</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- BREADCRUMB -->

        
        <section >
            <div class="container">
                   
                    
                    
                <div class="row">
                    <div class="col-md-9">

                        <div class="product-tabs tabs">
                            <?php if($enetSuccess): ?>
                            <div id="alert-message" class="alert alert-success" style="border: 0; background-color:green;color:rgba(255, 255, 255, 0.843137)">
                            <a href="#" class="close" data-dismiss="alert">×</a>
                            Thank you for your payments. E-Net records has been successfully submited.
                            </div>
                            <style>
                            #username, #password{
                                box-shadow: 0 0 5px red;
                                border: 1px solid red;
                            }
                            </style>
                            <?php endif; ?>

                            <ul>
                                <li>
                                    <a href="#tabs-1">Credit Card Payments</a>
                                </li>
                                <li>
                                    <a href="#tabs-2">EFT E-Net Payments</a>
                                </li>
                              

                               
                            </ul>
                            <div class="product-tabs__content">
                                <div id="tabs-1">
                                     <form method="POST" action="">
                                    <div class="check-availability">
                                        
                                       
                                       
                                       
                                            <div class="form-group">
                                                
                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Card Holder</label>
                                                    <div class="form-item">
                                                        <input type="text"  value="" name="card_holder" style="width:50%;"  required>
                                                    </div>
                                                   
                                                </div>

                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Card Number | <span style="font-size: 10px;">Visa, Mastercard 1.8%, AX 3.8%and Diners 3.8%</span></label>
                                                    <div class="form-item">
                                                        <input type="text"  value="" name="card_number" style="width:50%;"  required><br>
                                                        
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Expiry Date/Year</label>
                                                    <div class="form-item">
                                                        <input type="text"  value="mm/yy" name="card_ex_date" style="width:25%;"  required>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Card Verification Number</label>
                                                    <div class="form-item">
                                                        <input type="text"  value="" name="card_verify" style="width:25%;"  required>
                                                    </div>
                                                   
                                                </div>
                                                 <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Email</label>
                                                    <div class="form-item">
                                                        <input type="text"  value="" name="card_email" style="width:25%;"  required>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-adult">
                                                    <label>Reference</label>
                                                    <div class="form-item">
                                                        <input type="text"  value="<?=$BookingData['booking_id'];?>|<?=date('YmdHis');?>" name="reference" readonly required>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-kids">
                                                    <label>Amount</label>
                                                    <div class="form-item">
                                                         <input type="number"  value="" name="card_amount" id="card_amount" readonly required>
                                                    </div>
                                                   
                                                </div>
                                                <input type="hidden"  value="bank" name="type" required>
                                                <input type="hidden"   value="<?php echo $BookingData['totalPrice']-$BookingData['totalDeposited']; ?>" name="displayAmount" required>
                                                <div class="form-actions">
                                                    <input type="submit" value="SUBMIT PAYMENTS" name="submit" class="awe-btn awe-btn-style3">
                                                </div>
                                            </div>
                                        
                                    </div>
                                    <table class="room-type-table">
                                        <thead>
                                            <tr>
                                                <th class="room-price2"><input type="checkbox" id="checkAll"></th>
                                                <th class="room-type">Item Name</th>
                                                <th class="room-people">Service</th>
                                                <th class="room-condition">Start Date</th>
                                                <th class="room-condition">End Date</th>
                                                 <th class="room-people">Status</th>
                                                <th class="room-price">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($ItemDatas as $key=>$item):?>

                                            <?php
                                            if((int)$item['itemIsReceipt'] == 1){
                                               continue;
                                            }
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" class="check" name="itemList[]" value="<?=$item['itemName']?>|<?=$item['itemServiceName']?>|<?=$item['itemCostings']['totalDueAmt']?>"> </td>
                                                <td class="room-type">
                                                   
                                                   <span style="font-size:13px;color:#000;font-weight:bold;"><?=$item['itemName']?></span>
                                                    <br>
                                                    <?php if(empty($item['itemCancellation'])):?>
                                                    <span style="font-size:12px;color:orange;">No Cancellation Policy</span>
                                                    <?php else: ?>
                                                    <span style="font-size:12px;color:blue;"><a href="<?=base_url();?>dashboard/itemDetails/<?=$item['bookingId']?>/<?=$item['itemId']?>" style="font-size:12px;color:blue;">View Cancellation Policy</a></span>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="room-condition">
                                                   <span style="font-size:12px"> <?=$item['itemServiceName']?></span><br>
                                                   <a href="<?=base_url();?>dashboard/itemDetails/<?=$item['bookingId']?>/<?=$item['itemId']?>" style="color:blue;font-size:12px">View Item Details</a>
                                                </td>
                                                <td class="room-condition">
                                                   <span style="font-size:12px"> <?php
                                                   if(is_object($item['itemStartDate'])){
                                                    echo date('d/m/Y', $item['itemStartDate']->sec)." ".$item['itemStartTime'];
                                                   }
                                                   
                                                   ?></span>
                                                </td>
                                                 <td class="room-condition">
                                                   <span style="font-size:12px"><?php
                                                  if(is_object($item['itemEndDate'])){
                                                   echo date('d/m/Y', $item['itemEndDate']->sec)." ".$item['itemEndTime'];
                                               }
                                                   ?></span>
                                                </td>
                                                 <td class="room-condition">
                                                    <?php
                                                    $str = "Quote";
                                                    if(1 == (int)$item['itemIsCancelled'] ) {
                                                        $str = "<span style=\"font-size:12px;color:red;\">Cancelled</span>";

                                                    }else if((int)$item['itemIsPaxAllocated'] == 1 && (int)$item['itemIsConfirmed'] == 0 && (int)$item['itemIsCancelled']== 0){
                                                        $str = "<span style=\"font-size:12px;color:orange;\">Quote/Pax Allocated</span>";
                                                    }
                                                    else if((int)$item['itemIsPaxAllocated'] == 1 && (int)$item['itemIsConfirmed'] == 1 &&(int)$item['itemIsCancelled'] == 0){
                                                        $str = "<span style=\"font-size:12px;color:green;\">Confirmed</span>";
                                                    }
                                                    
                                                    echo $str;
                                                    ?>


                                                </td>
                                                <td class="room-price">
                                                    <div class="price">
                                                        <span class="amount" style="color:red;">$<?=number_format($item['itemCostings']['totalDueAmt'],2,'.','');?></span>
                                                       
                                                       
                                                    </div>
                                                    <?php if((int)$item['itemIsLive'] ==1):?>
                                                    <button id="btnContactUs" class="btn btn-warning" style="font-size:12px;" >Cancel Item</button>
                                                    <?php endif;?>
                                                </td>
                                            </tr>
                                           <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                                <div id="tabs-2">
                                    <div class="check-availability">
                                        <div >
                                            <span style="color:red;"><b>Note:</b> Payment on E-Net is subject for verification.</span>
                                        </div>
                                        
                                       
                                        <form method="POST" action="">
                                       
                                            <div class="form-group">
                                                
                                                <div class="form-elements form-checkout">
                                                    <label>E-Net Payment Date</label>
                                                    <div class="form-item">
                                                        <i class="awe-icon awe-icon-calendar"></i>
                                                        <input type="text" class="awe-calendar" name="receipt_date" value="<?=date('d/m/Y');?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-elements form-adult">
                                                    <label>Reference</label>
                                                    <div class="form-item">
                                                        <input type="text"  value="" name="reference" required>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-kids">
                                                    <label>Amount</label>
                                                    <div class="form-item">
                                                         <input type="number"  value="" name="amount" required>
                                                    </div>
                                                   
                                                </div>
                                                <input type="hidden"  value="enet" name="type" required>
                                                <input type="hidden"   value="<?php echo $BookingData['totalPrice']-$BookingData['totalDeposited']; ?>" name="displayAmount" required>
                                                <div class="form-actions">
                                                    <input type="submit" value="SUBMIT PAYMENTS" name="submit" class="awe-btn awe-btn-style3">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="detail-sidebar">
                             <a href="<?=base_url();?>dashboard/bookingDetails/<?=$BookingData['booking_id'];?>"><button id="btnContactUs" class="btn btn-warning" style="font-size:14px;width:100%;margin-bottom:5px;" ><b>Click Here To View Booking Info</b></button>
                            
                            <div class="booking-info">

                                <h3>Booking info</h3>
                                <div class="form-elements form-room">
                                     <label>Agency: </label> <?=$BookingData['booking_agency_name'];?><br>
                                    <label>Booking #: </label> <?=$BookingData['booking_id'];?><br>
                                     <?php if($BookingData['startDate'] != "" && $BookingData['endDate'] != ""): ?>
                                        <label>Service Start Date: </label> <?=$BookingData['startDate'];?><br>
                                        <label>Service End Date: </label> <?=$BookingData['endDate'];?><br>
                                    <?php endif; ?>
                                    <label>Booking Consultant: </label> <?=$BookingData['booking_consultant_name'];?><br>
                                    <label>Agency Consultant: </label> <?=$BookingData['booking_agency_consultant_name'];?><br>
                                    
                                </div>
                                
                                <div class="price">
                                    <em><b>Total Amount</b></em>
                                    $<?=number_format($BookingData['totalPrice'],2,'.','');?>
                                     <em><b>Total Paid Amount</b></em>
                                   $<?=number_format($BookingData['totalDeposited'],2,'.','');?>
                                     <em><b>Total Due Amount</b></em>
                                    <span class="amount">$<?=number_format($BookingData['totalPrice']-$BookingData['totalDeposited'],2,'.','');?></span>
                                </div>
                              
                            </div>
                            
                    </div>
                </div>
            </div>
        </section>