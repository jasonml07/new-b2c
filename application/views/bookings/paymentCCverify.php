<!-- BREADCRUMB -->
       <section>
            <div class="container">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="#">&nbsp;</a></li>
                        <li><a href="<?=base_url();?>dashboard/bookingPayments/<?=$BookingData['booking_id'];?>"> Click Here To Booking Payments</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- BREADCRUMB -->

        
        <section >
            <div class="container">
                   
                    
                    
                <div class="row">
                    <div class="col-md-9">
                        <div class="product-tabs tabs">
                            

                            <ul>
                                <li>
                                    <a href="#tabs-1">Please verify payment details</a>
                                </li>
                               

                               
                            </ul>
                            <div class="product-tabs__content">
                                <div id="tabs-1">
                                     
                                     <form method="POST" action="">
                                    <div class="check-availability">
                                        
                                       
                                       
                                       
                                            <div class="form-group">
                                                <div class="form-elements form-adult">
                                                    <label>Reference</label>
                                                    <div class="form-item" style="width:100%;">
                                                         <?= $pay_data['reference'];?>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Card Holder</label>
                                                    <div class="form-item">
                                                       <?= $pay_data['card_holder'];?>
                                                    </div>
                                                   
                                                </div>

                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Card Number </label>
                                                    <div class="form-item">
                                                       <?= $pay_data['card_number'];?>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Expiry Date/Year</label>
                                                    <div class="form-item">
                                                        <?=$pay_data['card_ex_date'];?>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Email</label>
                                                    <div class="form-item">
                                                        <?=$pay_data['card_email'];?>
                                                    </div>
                                                   
                                                </div>
                                                <div class="form-elements form-adult" style="width:100%;">
                                                    <label>Card Verification Number</label>
                                                    <div class="form-item">
                                                        <?= $pay_data['card_verify'];?>
                                                    </div>
                                                   
                                                </div>
                                                
                                                <div class="form-elements form-kids">
                                                    <label>Amount</label>
                                                    <div class="form-item">
                                                          <?= $pay_data['card_amount'];?>
                                                    </div>
                                                   
                                                </div>
                                                <input type="hidden"  value="bank" name="type" required>
                                                <input type="hidden"   value="<?php echo $BookingData['totalPrice']-$BookingData['totalDeposited']; ?>" name="displayAmount" required>
                                                <div class="form-actions">
                                                    <input type="submit" value="VERIFY" name="submit" class="awe-btn awe-btn-style3">
                                                </div>
                                            </div>
                                        
                                    </div>
                                    
                                    </form>
                                </div>
                                
                                
                            </div>
                            

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="detail-sidebar">
                             <a href="<?=base_url();?>dashboard/bookingPayments/<?=$BookingData['booking_id'];?>"><button id="btnContactUs" class="btn btn-warning" style="font-size:14px;width:100%;margin-bottom:5px;" ><b>Click Here To Booking Payments</b></button>
                            
                            <div class="booking-info">

                                <h3>Booking info</h3>
                                <div class="form-elements form-room">
                                     <label>Agency: </label> <?=$BookingData['booking_agency_name'];?><br>
                                    <label>Booking #: </label> <?=$BookingData['booking_id'];?><br>
                                     <?php if($BookingData['startDate'] != "" && $BookingData['endDate'] != ""): ?>
                                        <label>Service Start Date: </label> <?=$BookingData['startDate'];?><br>
                                        <label>Service End Date: </label> <?=$BookingData['endDate'];?><br>
                                    <?php endif; ?>
                                    <label>Booking Consultant: </label> <?=$BookingData['booking_consultant_name'];?><br>
                                    <label>Agency Consultant: </label> <?=$BookingData['booking_agency_consultant_name'];?><br>
                                    
                                </div>
                                
                                <div class="price">
                                    
                                     <em><b>Total Amount Paid</b></em>
                                    <span class="amount">$<?=number_format((float)$pay_data['displayAmount'],2,'.','');?></span>
                                </div>
                              
                            </div>
                            
                    </div>
                </div>
            </div>
        </section>