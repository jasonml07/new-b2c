<?php $background = '#2c3e50'; ?>
	<table border="0" 
		cellspacing="0"
		cellpadding="0"
		width="100%"
		style="width:100.0%;background:#fafafa;border-collapse:collapse">
		<tbody>
			<tr>
				<td valign="top" style="padding:0cm 0cm 0cm 0cm">
					<div align="center">
						<table border="1" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:white;border-collapse:collapse;border:none">
							<tbody>
								<tr>
									<td valign="top" style="border:solid #0c1e30 1.0pt;background:#0c1e30;padding:0cm 0cm 0cm 0cm">
										<div align="center">
										</div>
									</td>
								</tr>
							<tr>
				<?php switch ($this->nativesession->get('agencyRealCode')) {
					case 'EuropeTravelDeals':
						$background = '#262727';
						?>
						<td valign="top" style="border-top:none;border-left:solid #dddddd 1.0pt;border-bottom:solid #e84a52 2.25pt;border-right:solid #dddddd 1.0pt;background:#d8e2ea;padding:0cm 0cm 0cm 0cm">
							<div align="center">
								<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:white;border-collapse:collapse">
									<tbody>
										<tr>
											<td style="padding:0cm 0cm 0cm 0cm; background: <?=$background ?>">
												<div style='font-family: "Avenir Next" !important; line-height:20px; padding:10px;'>
												  <span style="color:red;font-size:30px;letter-spacing:2px;" >EUROPE</span><br>
												  <span style="color:#fff;letter-spacing:9px;margin-left:2px;">HOLIDAYS</span>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
						<?php
						break;
					case 'EET':
						?>
						<td valign="top" style="border-top:none;border-left:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;background:#d8e2ea;padding:0cm 0cm 0cm 0cm">
							<div align="center">
								<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:white;border-collapse:collapse">
									<tbody>
										<tr>
											<td style="padding:0cm 0cm 0cm 0cm">
												<p class="MsoNormal" style="background: #0082b9">
													<b><span style="font-size:25.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020">
																<img src="<?=base_url()?>assets/images/EET/logo.png" class="CToWUd"><u></u><u></u>
														</span></b></p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
						<?php
						break;
					
					default:
						?>
						<td valign="top" style="border-top:none;border-left:solid #dddddd 1.0pt;border-bottom:solid #e84a52 2.25pt;border-right:solid #dddddd 1.0pt;background:#d8e2ea;padding:0cm 0cm 0cm 0cm">
							<div align="center">
								<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:white;border-collapse:collapse">
									<tbody>
										<tr>
											<td style="padding:0cm 0cm 0cm 0cm">
												<p class="MsoNormal" style="background: #000;">
													<b><span style="font-size:25.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020">
																<img src="<?=base_url()?>assets/images/header-logo.png" style="height: 50px;" class="CToWUd"><u></u><u></u>
														</span></b></p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
						<?php
						break;
				} ?>
			</tr>
			<tr>
				<td valign="top" style="border:solid #dddddd 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm">
					<div align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="top" style="background:white;padding:0cm 0cm 0cm 0cm">
										<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
											<tbody>
												<tr>
													<td valign="top" style="padding:11.25pt 11.25pt 11.25pt 11.25pt">
														<p style="line-height:150%;">
															<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">
																<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Hi <?=$receiver?>!</span>
																<br><br>
																	<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">A customer booked under reference number <?=$booking_id?> and a payment reference ID of <?=$card['REF']?> made on <?=date('F d, Y', strtotime('now'))?>.</span>
																<br><br>
																<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Below are the customer information:</span>
																		<br>
																		<br>
																		
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><strong>Voucher Information</strong></span><br>
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><strong>Name:</strong> <?=$card['VOUCHER']['firstname']?> <?=$card['VOUCHER']['lastname']?></span><br>
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><strong>Email Address:</strong> <?=$card['VOUCHER']['email']?></span><br>
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><strong>Phone Number:</strong> <?=$card['VOUCHER']['phonenum']?></span><br>
																		<br>
																		<!-- <span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><b>Holder Name:</b> <?=$card['nm_card_holder']?></span><br>
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><b>Card Number:</b> <?=$card['no_credit_card']?></span><br>
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><b>Card Verification Number:</b> <?=$card['no_cvn']?></span><br>
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><b>Expiry Date:</b> <?=$card['dt_expiry_month']?>/<?=$card['dt_expiry_year']?></span><br>
																		<br> -->

																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Below are the items booked:</span>
																		<br>
																	<table>
																		<thead>
																			<tr>
																				<th style="width: 100px; text-align: left;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Price</span></th>
																				<th><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Item&#39;s</span></th>
																			</tr>
																		</thead>
																		<tbody>
																			<?php foreach ($product['items'] as $key => $item) : ?>
																				<tr>
																					<td style="width: 100px;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">$<?=number_format($item['price'], 2, '.', ',')?></span></td>
																					<td><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><?=$item['name']?> 
																					<?php if ($item['type']=='product'||$item['type']=='flight'): ?>
																						<br>(From: <?=date('F d, Y', $item['startdate']->sec)?> - To: <?=date('F d, Y', $item['enddate']->sec)?>)
																					<?php endif ?>
																					</span></td>
																				</tr>
																			<?php endforeach; ?>
																		</tbody>
																		<tfoot>
																			<tr>
																				<td style="border-top: 1px solid;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">$<?=number_format($product['total'], 2, '.', ',')?></span></td>
																				<td style="border-top: 1px solid;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><strong>TOTAL</strong></span></td>
																			</tr>
																		</tfoot>
																	</table>
																</span>
																<br>
																<!-- <span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Please don&#39;t </span> -->
															</span>
															<?php if ( isset( $AGENCY_INFO ) && (! empty( $AGENCY_INFO ) || ! is_null( $AGENCY_INFO )) ): ?>
																<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Please contact us on <?=$AGENCY_INFO['phone'] ?> if you require further assistance. 
																	<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Thank you for your continued support.</span>
																	<!-- <br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Many Thanks Again</span> -->
																	<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Kind Regards,</span>
																	<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Online Reservations</span>
																	<br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><?=$AGENCY_INFO['agency_name'] ?>, a division of Magic Tours International</span> <u></u><u></u>
																</span>
															<?php else: ?>
																<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Please contact us on 1800 242 353 if you require further assistance. 
																	<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Thank you for your continued support.</span>
																	<!-- <br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Many Thanks Again</span> -->
																	<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Kind Regards,</span>
																	<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Online Reservations</span>
																	<br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">TravelRez, a division of Magic Tours International</span> <u></u><u></u>
																</span>
															<?php endif; ?>
														</p>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border:solid <?=$background ?> 1.0pt;border-top:none;background:<?=$background ?>;padding:0cm 0cm 0cm 0cm">
					<div align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:<?=$background ?>;border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="top" style="padding:7.5pt 7.5pt 7.5pt 7.5pt">
										<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;background:<?=$background ?>;border-collapse:collapse">
											<tbody>
												<tr>
													<td width="300" style="width:225.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt">
														<h5 style="line-height:125%">
															<span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white">RESERVATIONS CALL CENTRE<u></u><u></u></span>
														</h5>
														<p style="line-height:125%">
															<?php if ( isset( $AGENCY_INFO ) && (! empty( $AGENCY_INFO ) || ! is_null( $AGENCY_INFO )) ): ?>
																<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white"><?=$AGENCY_INFO['address'] ?>, <?=$AGENCY_INFO['city'] ?>, <?=$AGENCY_INFO['country'] ?><br>
																<strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Phone</span></strong>: <?=isset($AGENCY_INFO['number'])?$AGENCY_INFO['number']:'&nbsp;' ?><br>
																<?php if (! empty($AGENCY_INFO['fax'])): ?>
																	<strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Toll free</span></strong>: <?=$AGENCY_INFO['fax'] ?><br>
																<?php endif ?>
																<strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Email</span></strong>: <a href="<?=$AGENCY_INFO['email'] ?>" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://info@travelrez.net.au&amp;source=gmail&amp;ust=1466657759385000&amp;usg=AFQjCNGkGsRIssXX5GK26C3I8GoNUpT0Ww"><span style="color:white"><?=$AGENCY_INFO['email'] ?></span></a><br>
																<u></u><u></u></span>
															<?php else: ?>
																<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white">1/66 Appel St, Surfers Paradise, QLD 4217, Australia <br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Phone</span></strong>: (07) 5526 2855<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Toll Free</span></strong>: 1800 242 353<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Fax</span></strong>: (07) 5526 2944<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Email</span></strong>: <a href="http://info@travelrez.net.au" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://info@travelrez.net.au&amp;source=gmail&amp;ust=1466657759385000&amp;usg=AFQjCNGkGsRIssXX5GK26C3I8GoNUpT0Ww"><span style="color:white">info@travelrez.net.au</span></a><br>Hours of Operation<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Mon-Fri</span></strong>: 08.00 am - 17.30 pm<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Sat</span></strong>: 24 hour Contact 0433 161 250<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Sun</span></strong>: 24 hour Contact 0433 161 250 <u></u><u></u></span>
															<?php endif ?>
														</p>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
							</table>
								</div>
									</td>
										</tr><tr>
									<td valign="top" style="border:solid #252525 1.0pt;border-top:none;background:#252525;padding:0cm 0cm 0cm 0cm">
										<div align="center">
											<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:#252525;border-collapse:collapse">
												<tbody>
													<tr>
														<td valign="top" style="padding:.75pt .75pt .75pt .75pt">
															<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;background:#252525;border-collapse:collapse">
																<tbody>
																	<tr>
																		<td style="padding:.75pt .75pt .75pt .75pt">
																			<p class="MsoNormal" align="center" style="text-align:center;line-height:125%"><em>
																				<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white">Copyright &copy; <?=date('Y',strtotime('now'))?> <span style="color:white;text-decoration:none"><?=@$AGENCY_INFO['agency_name'] ?></span>, All rights reserved.</span>
																				</em>
																				<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white"> <u></u><u></u></span>
																			</p>
																		</td>
																		<td style="padding:.75pt .75pt .75pt .75pt"><p class="MsoNormal" align="right" style="text-align:right;line-height:125%">
																			<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#707070">&nbsp;
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
