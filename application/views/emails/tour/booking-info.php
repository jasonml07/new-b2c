
	<table border="0" 
		cellspacing="0"
		cellpadding="0"
		width="100%"
		style="width:100.0%;background:#fafafa;border-collapse:collapse">
		<tbody>
			<tr>
				<td valign="top" style="padding:0cm 0cm 0cm 0cm">
					<div align="center">
						<table border="1" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:white;border-collapse:collapse;border:none">
							<tbody>
								<tr>
									<td valign="top" style="border:solid #0c1e30 1.0pt;background:#0c1e30;padding:0cm 0cm 0cm 0cm">
										<div align="center">
											<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:#0c1e30;border-collapse:collapse">
												<tbody>
													<tr>
														<td style="padding:3.75pt 3.75pt 3.75pt 3.75pt">
															<p class="MsoNormal" align="right" style="text-align:right">
																<span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white"><?=date('F d Y', strtotime('now'))?> <u></u><u></u></span>
															</p>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							<tr>
				<td valign="top" style="border-top:none;border-left:solid #dddddd 1.0pt;border-bottom:solid #e84a52 2.25pt;border-right:solid #dddddd 1.0pt;background:#d8e2ea;padding:0cm 0cm 0cm 0cm">
					<div align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:white;border-collapse:collapse">
							<tbody>
								<tr>
									<td style="padding:0cm 0cm 0cm 0cm">
										<p class="MsoNormal" style="background: #000;">
											<b><span style="font-size:25.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020">
														<img src="<?=base_url()?>assets/images/header-logo.png" style="height: 50px;" class="CToWUd"><u></u><u></u>
												</span></b></p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border:solid #dddddd 1.0pt;border-top:none;padding:0cm 0cm 0cm 0cm">
					<div align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="top" style="background:white;padding:0cm 0cm 0cm 0cm">
										<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse">
											<tbody>
												<tr>
													<td valign="top" style="padding:11.25pt 11.25pt 11.25pt 11.25pt">
														<p style="line-height:150%;">
															<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">
																<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Dear <?=$agencyName?>,</span>
																<br><br>
																<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">A customer booked under reference <?=$bookingId?> made on <?=$created_at?>.</span>
																<br><br>
																<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">
																		<?php if( isset( $payment ) && ! empty( $payment ) && _array_key_exists( $payment, array('type', 'paid', 'receiptId') ) ) :?>
																			<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">With a payment via <?=$payment['type']?> is being processed with the amount of $<?=number_format($payment['paid'], 2, '.', ',')?>.</span>
																			<br>
																		<?php endif; ?>
																		<?php if ( ! isset($data['isFullyPaid']) || $data['isFullyPaid'] == FALSE ): ?>
																			<?php if ( isset( $data ) ): ?>
																				<?php if ( @$data['isPayable'] == FALSE ): ?>
																					<?php if ( @$data['PAYMENT_TYPE'] && $data['PAYMENT_TYPE'] == 'BANKDEPOSIT' ): ?>
																						<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">
																							A Customer must pay a full or deposit a payment before the end of <?=date('l h:i A, d M Y', strtotime($data['TODAYS_DATE'] . ' +1 day'))?>. Customer must deposit their payment through the credit account given below.
																						</span>
																						<br>
																						<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">
																							<strong>Name</strong>: Select World Travel Westpac BSB 034-216
																							<br>
																							<strong>ACCN:</strong> 261 424
																							<br>
																							<strong>SWIFT:</strong> WPACAU2S
																						</span>
																						<br>
																					<?php else: ?>
																						<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">
																							Customer must pay 
																							<?php if ( @$data['isFullPayment'] ): ?>
																								a full payment
																							<?php else: ?>
																								a deposit of 30 percent of the total booking price
																							<?php endif ?>
																							 before the end of <?=$data['startDate']?>.
																						</span>
																						<br>
																					<?php endif ?>
																				<?php endif ?>
																			<?php endif ?>
																		<?php endif; ?>
																		<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">There are <?=$pax['adults']?> adults<?=((isset($pax['childs']) && $pax['childs'] > 0)?' and ' . $pax['childs'] . ' childs':'')?><?=((isset($pax['rooms']) && $pax['rooms'] > 0)? ' for ' . $pax['rooms'] . ' room/s':'' )?> booked on the item.</span>
																		<br>
																	<?php if( isset( $items ) ) :?> 
																	<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Below are the list of items they booked along with the prices:</span>
																	<table>
																		<thead>
																			<tr>
																				<th style="width: 100px; text-align: left;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Price</span></th>
																				<th><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Item&#39;s</span></th>
																			</tr>
																		</thead>
																		<tbody>
																			<?php foreach ($items['items'] as $key => $item) : ?>
																				<tr>
																					<td style="width: 100px;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">$<?=number_format($item['price'], 2, '.', ',')?></span></td>
																					<td><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><?=$item['name']?> 
																					<?php if ($item['type']=='product'||$item['type']=='flight'): ?>
																						<br>(From: <?=date('F d, Y', $item['startdate']->sec)?> - To: <?=date('F d, Y', $item['enddate']->sec)?>)
																					<?php endif ?>
																					</span></td>
																				</tr>
																			<?php endforeach; ?>
																		</tbody>
																		<tfoot>
																			<tr>
																				<td style="border-top: 1px solid;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">$<?=number_format($items['total'], 2, '.', ',')?></span></td>
																				<td style="border-top: 1px solid;"><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050"><strong>TOTAL</strong></span></td>
																			</tr>
																		</tfoot>
																	</table>
																	<?php endif; ?>
																</span>
																<br>
																<!-- <span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Please don&#39;t </span> -->
															</span>
															<span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Please contact us on 1800 242 353 if you require further assistance. 
																<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Thank you for your continued support.</span>
																<!-- <br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Many Thanks Again</span> -->
																<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Kind Regards,</span>
																<br><br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">Online Reservations</span>
																<br><span style="font-size:9.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#505050">TravelRez, a division of Magic Tours International</span> <u></u><u></u>
															</span>
														</p>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border:solid #2c3e50 1.0pt;border-top:none;background:#2c3e50;padding:0cm 0cm 0cm 0cm">
					<div align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:#2c3e50;border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="top" style="padding:7.5pt 7.5pt 7.5pt 7.5pt">
										<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;background:#2c3e50;border-collapse:collapse">
											<tbody>
												<tr>
													<td width="300" style="width:225.0pt;padding:7.5pt 7.5pt 7.5pt 7.5pt">
														<h5 style="line-height:125%">
															<span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white">RESERVATIONS CALL CENTRE<u></u><u></u></span>
														</h5>
														<p style="line-height:125%">
															<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white">1/66 Appel St, Surfers Paradise, QLD 4217, Australia <br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Phone</span></strong>: (07) 5526 2855<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Toll Free</span></strong>: 1800 242 353<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Fax</span></strong>: (07) 5526 2944<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Email</span></strong>: <a href="http://info@travelrez.net.au" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://info@travelrez.net.au&amp;source=gmail&amp;ust=1466657759385000&amp;usg=AFQjCNGkGsRIssXX5GK26C3I8GoNUpT0Ww"><span style="color:white">info@travelrez.net.au</span></a><br>Hours of Operation<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Mon-Fri</span></strong>: 08.00 am - 17.30 pm<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Sat</span></strong>: 24 hour Contact 0433 161 250<br><strong><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Sun</span></strong>: 24 hour Contact 0433 161 250 <u></u><u></u></span>
														</p>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
							</table>
								</div>
									</td>
										</tr><tr>
									<td valign="top" style="border:solid #252525 1.0pt;border-top:none;background:#252525;padding:0cm 0cm 0cm 0cm">
										<div align="center">
											<table border="0" cellspacing="0" cellpadding="0" width="600" style="width:450.0pt;background:#252525;border-collapse:collapse">
												<tbody>
													<tr>
														<td valign="top" style="padding:.75pt .75pt .75pt .75pt">
															<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;background:#252525;border-collapse:collapse">
																<tbody>
																	<tr>
																		<td style="padding:.75pt .75pt .75pt .75pt">
																			<p class="MsoNormal" align="center" style="text-align:center;line-height:125%"><em>
																				<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white">Copyright &copy; <?=date('Y',strtotime('now'))?> <a href="http://travelrez.net.au/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://travelrez.net.au/&amp;source=gmail&amp;ust=1466657759386000&amp;usg=AFQjCNG6T0getGa2CCp6B5SQRWxiDvZwnw">
																				<span style="color:white;text-decoration:none">TravelRez</span></a>, All rights reserved.</span>
																				</em>
																				<span style="font-size:9.0pt;line-height:125%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white"> <u></u><u></u></span>
																			</p>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
