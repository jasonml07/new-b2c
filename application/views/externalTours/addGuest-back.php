    <section class="booking-container">
            <div class="container">
                <div class="row">
                
                    <div class="col-md-12">
                        <div class="checkout-page__top">
                            <div class="title">
                                <h1 class="text-uppercase" style=" margin-left: 40px; margin-top: 20px;">Assign Guest/Customer Information</h1>
                            </div>
                        </div>
                    </div>
                    <div id="main-forms-wrapper" class="col-md-9">
                        <!-- Main Errors -->
                
                        <form novalidate="" id="paxForm" name="paxForm" method="POST">
                            <div class="checkout-page__content">
                                <div class="customer-content" style="background-color: #fff !important; border: 3px solid #f2f2f2; border-radius: 3px; ">

                                        <div class="woocommerce-billing-fields passengers" style="margin-top: 0px;">
                                            <h3 class="header" style="margin-bottom: 10px;">Passengers</h3>
                                            <!-- ERRORS -->
                                            <div class="alert alert-danger passenger-errors"></div>
                                            <!-- START OF ADULT -->
                                            <?php foreach ( range(1, intval($_POST['passengers'])) as $key => $value ) :?>
                                                <div class="col-sm-6 col-md-6" style="border: 1px dashed #CCC;">
                                                    <div class="col-md-12 text-center">
                                                        <h6>Guest <?=$value?></h6>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-6">
                                                            <div class="form-group">
                                                                <label for="">First name</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-btn">
                                                                        <button style="padding-top: 8px; padding-bottom: 8px;" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Title <span class="caret"></span></button>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a href="#">Mr.</a></li>
                                                                            <li><a href="#">Ms</a></li>
                                                                            <li><a href="#">Mrs.</a></li>
                                                                        </ul>
                                                                        <input type="hidden" id="passenger-title-<?=$value?>" name="passenger[][title]" class="sr-only">
                                                                    </div>
                                                                    <input type="text" id="passenger-firstname-[<?=$value?>]" name="passenger[][firstname]" class="form-control" <?=($value == 1)?'data-bind="voucher[firstname]"':''?> <?=(TEST_INPUTS && $value != 1)?'data-value="name"':''?>>
                                                                </div>
                                                                <!-- <label for="title[<?=$value?>]">Title</label>
                                                                <select id="title[<?=$value?>]" style="appearance:none; -moz-appearance:none; -webkit-appearance:none;" name="passenger[][title]" style="width: 100%;" <?=($value == 1)?'data-bind="voucher[title]"':''?> <?=(TEST_INPUTS && $value != 1)?'data-value="array" data-choices="Mr|Ms|Mrs"':''?>>
                                                                    <option value="">---</option>
                                                                    <option value="Mr">Mr</option>
                                                                    <option value="Ms">Ms</option>
                                                                    <option value="Mrs">Mrs</option>
                                                                </select> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6">
                                                            <div class="form-group">
                                                                <label for="passenger-firstname-[<?=$value?>]">Last name</label>
                                                                <input type="text" id="passenger-firstname-[<?=$value?>]" name="passenger[][firstname]" class="form-control" <?=($value == 1)?'data-bind="voucher[firstname]"':''?> <?=(TEST_INPUTS && $value != 1)?'data-value="name"':''?>>
                                                                <!-- <label for="title[<?=$value?>]">Title</label>
                                                                <select id="title[<?=$value?>]" style="appearance:none; -moz-appearance:none; -webkit-appearance:none;" name="passenger[][title]" style="width: 100%;" <?=($value == 1)?'data-bind="voucher[title]"':''?> <?=(TEST_INPUTS && $value != 1)?'data-value="array" data-choices="Mr|Ms|Mrs"':''?>>
                                                                    <option value="">---</option>
                                                                    <option value="Mr">Mr</option>
                                                                    <option value="Ms">Ms</option>
                                                                    <option value="Mrs">Mrs</option>
                                                                </select> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-sm-5 col-md-5" style="padding-right: 0px;">
                                                        <div class="form-group">
                                                            <label for="firstname">First Name</label>
                                                            <input type="text" id="firstname[<?=$value?>]" name="passenger[][firstname]" class="form-control" <?=($value == 1)?'data-bind="voucher[firstname]"':''?> <?=(TEST_INPUTS && $value != 1)?'data-value="name"':''?>>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-5 col-md-5" style="padding-right: 0px;">
                                                        <div class="form-group">
                                                            <label for="lastname">Last Name</label>
                                                            <input type="text" id="lastname[<?=$value?>]" name="passenger[][lastname]" class="form-control" <?=($value == 1)?'data-bind="voucher[lastname]"':''?> <?=(TEST_INPUTS && $value != 1)?'data-value="name"':''?>>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    <!-- <?php if ( ! (@$SESSION && is_array( $SESSION )) ): ?>
                                    <?php endif?> -->

                                    <!--=============================
                                    =            FLIGHTS            =
                                    ==============================-->
                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="form-group flight-details-option sr-only" style="color: #000;">
                                        <label for="" class="control-label col-sm-4" style="padding-left: 0px;">Include Flight Details?</label>
                                        <div class="col-sm-8">
                                            <label for="includeFlight1" class="radio-inline"><input type="radio" id="includeFlight1" name="includeFlight" value="true" <?=filter_var(@$_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN)?'checked':''?>> Yes</label>
                                            <label for="includeFlight2" class="radio-inline"><input type="radio" id="includeFlight2" name="includeFlight" value="false" <?=(! filter_var(@$_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN))?'checked':''?>> No</label>
                                        </div>
                                    </div>
                                    <div class="woocommerce-billing-fields fligh-details sr-only">
                                        <h3 class="header" style="margin-bottom: 10px;">Flight Details</h3>
                                        <div class="alert alert-danger flight-errors"></div>
                                        
                                        <div class="col col-md-12">
                                            <div class="col col-md-6" style="padding-left: 0px;">
                                                <div class="form-group">
                                                    <label for="flight-city">Departure City</label>
                                                    <div class="input-group">
                                                        <!-- <input type="text" class="form-control" id="flight-city" name="flight[city]"> -->
                                                        <select name="flight[city]" id="flight-city" class="form-control">
                                                            <option value="Sydney"    <?=(@$FLIGHT['city'] && @$FLIGHT['city'] == 'Sydney')   ?'selected':''?>>Sydney</option>
                                                            <option value="Melbourne" <?=(@$FLIGHT['city'] && @$FLIGHT['city'] == 'Melbourne')?'selected':''?>>Melbourne</option>
                                                            <option value="Brisbane"  <?=(@$FLIGHT['city'] && @$FLIGHT['city'] == 'Brisbane') ?'selected':''?>>Brisbane</option>
                                                            <option value="Adelaide"  <?=(@$FLIGHT['city'] && @$FLIGHT['city'] == 'Adelaide') ?'selected':''?>>Adelaide</option>
                                                            <option value="Perth"     <?=(@$FLIGHT['city'] && @$FLIGHT['city'] == 'Perth')    ?'selected':''?>>Perth</option>
                                                        </select>
                                                        <div class="input-group-addon"><span class="fa fa-map-marker"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col col-md-6" style="padding-right: 0px;">
                                                <div class="form-group">
                                                    <label for="flight-class">Preferred Class</label>
                                                    <input type="text" class="form-control" id="flight-class" name="flight[class]" value="<?=@$FLIGHT['class']?>" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-md-6">
                                            <div class="form-group">
                                                <label for="FD-departure-date">Departure Date</label>
                                                <div class="input-group">
                                                    <input type="text" class="flight-date form-control departure" id="FD-departure-date" name="flight[departureDate]" value="<?=date('d/m/Y', strtotime(str_replace('/', '-', @$FLIGHT['departureDate'])))?>">
                                                    <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                                </div>                                                    
                                            </div>
                                        </div>
                                        <div class="col col-md-6">
                                            <div class="form-group">
                                                <label for="FD-return-date">Return Date</label>
                                                <div class="input-group">
                                                    <input type="text" class="flight-date form-control return" id="FD-return-date" name="flight[returnDate]" value="<?=date('d/m/Y', strtotime(str_replace('/', '-', @$FLIGHT['returnDate'])))?>">
                                                    <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-md-6">
                                            <div class="form-group">
                                                <label for="flight-pax">Number of Passengers</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="flight[pax]" id="flight-pax" value="<?=@$FLIGHT['pax']?>" readonly="">
                                                    <div class="input-group-addon"><span class="fa fa-users"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="position: relative; top: 50px; left: 15px;">
                                            <div class="form-group">
                                                <label for="" class="sr-only">For Show</label>
                                                <a id="flight-preferred-airlines" style="cursor: pointer;"><strong>Airlines Used - subject to availability <span class="fa fa-plane"></span></strong></a>
                                            </div>
                                        </div>
                                        <div class="col col-md-6 sr-only">
                                            <div class="form-group">
                                                <label for="flight-price">Price</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><span class="fa fa-usd"></span></div>
                                                    <input type="text" class="form-control" name="flight[price]" id="flight-price" value="<?=@$FLIGHT['price']?>" data-per-person="<?=$departurePrice?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-md-12">
                                            <div class="form-group">
                                                <input type="checkbox" name="flight[terms]" id="flight-terms" <?=(@$FLIGHT['terms'] && @$FLIGHT['terms'] == 'on')?'checked':''?>>
                                                <label style="color: #0091ea; cursor: pointer;">I have read the <a id="flight-terms-and-condition">terms and condition</a> of the flight.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--====  End of FLIGHTS  ====-->
                                    

                                    <div class="woocommerce-billing-fields voucher" style="margin-top: 0px;">
                                        <div class="col-sm-12 col-md-12" style="padding-left: 0px !important; padding-right: 0px; font-size: 12px; margin-top: 10px;">
                                        <h3 class="header" >Voucher Information</h3>
                                        <!-- ERRORS -->
                                        <div class="alert alert-danger voucher-errors"></div>
                                        <!-- END ERRORS --> 
                                        <div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">First name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-btn" style="padding: 0px;">
                                                                <button style="padding-top: 8px; padding-bottom: 8px;" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Title <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="#">Mr.</a></li>
                                                                    <li><a href="#">Ms</a></li>
                                                                    <li><a href="#">Mrs.</a></li>
                                                                </ul>
                                                                <input type="hidden" name="voucher[title]" value="" class="sr-only">
                                                                <!-- <select
                                                                    class="form-control adjust-select"
                                                                    name="voucher[title]">
                                                                    <option value="">title</option>
                                                                    <option value="Mr">Mr</option>
                                                                    <option value="Ms">Ms</option>
                                                                    <option value="Mrs">Mrs</option>
                                                                </select> -->
                                                            </div>
                                                            <input readonly="" name="voucher[firstname]" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="voucher-last-name">Last name</label>
                                                        <input readonly="" id="voucher-last-name" name="voucher[lastname]" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="voucher-email">Email Address</label>
                                                        <input type="text" id="voucher-email" name="voucher[email]" class="form-control" data-value="email">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="voucher-phone">Phone number</label>
                                                        <input type="text" id="voucher-phone" name="voucher[phonenum]" class="form-control" data-value="phone">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-sm-2 col-md-2"  style="margin-top: 20px; padding:0 !important; ">
                                                <label style="padding-top: 10px !important;color: #1e1e1f;font-size: 14px; padding-left: 5px; margin-left: 20px;">Title</label>
                                                <select style="width:70% !important;background-color:#EEE; padding-left: 10px; margin-left: 20px;"
                                                    class="adjust-select"
                                                    name="voucher[title]">
                                                    <option value="">title</option>
                                                    <option value="Mr">Mr</option>
                                                    <option value="Ms">Ms</option>
                                                    <option value="Mrs">Mrs</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-5 col-md-5" style="padding: 8px !important; margin-top: 20px;">
                                                <label style="padding-top: 0px !important; padding-left: 40px; font-size: 14px;" >First name</label>
                                                <input readonly="" name="voucher[firstname]" type="text" class="adjust-input-text" style="width:78% !important;background-color:#fff; margin-left: 40px;">
                                            </div>
                                            <div class="col-sm-5 col-md-5" style="padding:8px !important; margin-top: 20px;">
                                                <label style="padding-top: 0px !important; padding-left: 40px;font-size: 14px;">Last name</label>
                                                <input readonly="" name="voucher[lastname]" type="text" class="adjust-input-text" style="width:78% !important;background-color:#fff; margin-left: 40px;">
                                            </div>
                                            <div class="col-sm-offset-2 col-sm-5 col-md-offset-2 col-md-5" style="padding:0 !important;">
                                                <label style="padding-top: 0px !important; font-size: 14px;">Email Address</label>
                                                <input name="voucher[email]" data-value="email" type="email" class="adjust-input-text" style="width:87% !important;background-color:#fff;">
                                            </div>
                                            <!-- value="{{ fakerEmail() }}" -->
                                            <div class="col-sm-5 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 10px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Phone Number</label>
                                                <input name="voucher[phonenum]" data-value="phone" type="text" class="adjust-input-text" style="width:83% !important;background-color:#fff;margin-left: 33px;">
                                            </div>
                                            <!-- <div class="col-sm-offset-2 col-sm-5 col-md-offset-2 col-md-5" style="padding:0px 10px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Home Number</label>
                                                <input name="voucher[homenum]" type="text" class="adjust-input-text" style="width:83% !important;background-color:#fff;margin-left: 40px;">
                                            </div> -->
                                       </div>
                                    </div>
                                    <!-- END OF ROOM CONTAINER -->
                                     <div class="woocommerce-billing-fields">
                                        <div class="col-md-12" style="padding-left: 0px !important; padding-right: 0px; font-size: 12px; margin-top: 10px;">
                                            <h3 class="header" >Special Requirements (Optional)</h3>
                                                 <label style="margin-top: 30px;  margin-left: 30px;">Please write your requests in English</label>
                                              <textarea class="special-offer" id="request" name="request" style="margin-top: 30px; width: 90%;" >No Smoking</textarea>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- If the payment is not required..
                        There will be an optional payment
                        So the logic should be when the user wants to pay the submit the form from handofUrl
                        Else submit the form without payments -->
                            <!-- PAYMENTS -->
                            <!-- This item only shows when the payment is required of the user wants to pay
                            if the user wants to pay, the button will appear
                            but when the payment is indeed requred the payment form will appear -->
                            <!-- Check first for the session if is required -->

                            <div class="checkout-page__content">
                                <div class="customer-content" style="background-color: #fff !important; border: 3px solid #f2f2f2; border-radius: 3px; padding-top: 0px;">
                                    <!-- POLICY -->
                                    <!-- <div class="woocommerce-billing-fields" style="margin-top: 0px;">
                                        <h3 class="header" >Cancellation Policy</h3>
                                        <div>
                                            <div class="col-md-12" >
                                                <!-- <script type="text/javascript">console.log(<?=json_encode($policies)?>)</script> -->
                                                   <!--  <label  style="padding: 8px !important; margin-top: 20px; padding-bottom: 20px;" ></label>
                                                    <div class="payment_box payment_method_bacs">
                                                        <ul>
                                        
                                                        </ul>
                                                            No policy
                                                       
                                                    </div>
                                                
                                            </div>
                                        </div>
                                    </div> --> 
                                    <!-- this button only shows when payment is not neccessary -->
                                   <!--  <div>
                                        <button class="btn btn-primary">Do you want to pay?</button>
                                    </div> -->
                                    <div class="woocommerce-billing-fields" style="margin-top: 0px;">
                                        <?php if ( $IS_AGENT ): ?>
                                            <h3 class="header" >Payment</h3>

                                            <!-- THIS SHOULD BE DATE TODAY SUBTRACT TO THE START OF DEPARTURE -->
                                            <!-- <?php echo $remaining_days; ?> -->
                                            <?php if ( $remaining_days >= 30 ): ?>
                                                <label for="pay-later" class="btn btn-primary btn-sm" title="Pay later">
                                                    <input type="checkbox" class="sr-only" id="pay-later">
                                                    Pay Later ?
                                                    <span class="fa fa-square-o"></span>
                                                </label>

                                                <?php foreach ($policies as $key => $policy): ?>
                                                    <div class="bs-callout bs-callout-danger">
                                                        <p><?=$policy?>.</p>
                                                    </div>
                                                <?php endforeach ?>
                                            <?php endif ?>

                                            <div class="payment-wrapper" style="height: 350px;">
                                                <ul id="payment-nav" class="nav nav-tabs nav-justified" role="tablist">
                                                    <li role="presentation" class="active" data-active="true"><a href="#CC" aria-controls="CC" role="tab" data-toggle="tab"><strong>CREDIT CARD <span style="color: green;" class="fa fa-check-circle"></span></strong></a></li>
                                                    <li role="presentation">
                                                        <a href="#PP" aria-controls="PP" role="tab" data-toggle="tab"><strong>Paypal <span style="color: green;" class="fa fa-check-circle"></span></strong></a>
                                                    </li>
                                                    <li role="presentation"><a href="#EN" aria-controls="EN" role="tab" data-toggle="tab"><strong>E-NETT <span style="color: green;" class="fa fa-check-circle"></span></strong></a></li>
                                                </ul>
                                                
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="CC">
                                                        <form id="paymentForm-CC" novalidate="" method="POST" target="my_iframe">
                                                            <input type="hidden" name="token">
                                                            <?php if( PAYWAY_IS_TEST ) : ?>
                                                                <input type="hidden" name="amount" value="<?=(isset($FLIGHT['price'])? ($costing['gross_amount'] + $FLIGHT['price']) : $costing['gross_amount']) ?>" data-original="<?=(isset($FLIGHT['price'])? ($costing['gross_amount'] + $FLIGHT['price']) : $costing['gross_amount']) ?>">
                                                            <?php endif; ?>
                                                            <input type="hidden" name="biller_code" value="<?=htmlentities(PAYWAY_BILLER_CODE)?>">
                                                            <input type="hidden" name="action" value="MakePayment">
                                                            
                                                            <div class="col-sm-12 col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;  ">
                                                                <!-- ERRORS -->
                                                                <div class="alert alert-danger payment-errors"></div>
                                                
                                                                <div class="col-sm-6 col-md-6" style="padding: 8px !important;">
                                                                    <label style="padding-top: 20px !important; padding-left: 40px; font-size: 14px;" >Cardholder Name</label>
                                                                        <input id="payment-form-holder-name" type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                                            class="adjust-input-text"
                                                                            name="nm_card_holder"
                                                                            value="<?=TEST_INPUTS?'John Doe':''?>">
                                                                    <label style="padding-top: 25px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Card Verification Number</label>
                                                                        <input id="payment-form-ver-number" type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                                            class="adjust-input-text"
                                                                            name="no_cvn"
                                                                            value="<?=TEST_INPUTS?'123':''?>">
                                                                </div>
                                                                 <div class="col-sm-6 col-md-6" style="padding:8px !important;">
                                                                    <label style="padding-top: 20px !important; padding-left: 40px;font-size: 14px;">Card Number</label>
                                                                        <input id="payment-form-card-number" type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                                            class="adjust-input-text"
                                                                            name="no_credit_card"
                                                                            value="<?=TEST_INPUTS?'123456789012345':''?>">
                                                
                                                                    <div style="padding-left: 40px;">
                                                                        <label style="padding-top: 25px ; font-size: 14px;" class="col">Expiry Date</label>
                                                                        <br>
                                                                        <select name="dt_expiry_month">
                                                                            <option value="" >-- Month --</option>
                                                                            <?php for($m=1; $m<=12; ++$m): ?>
                                                                                <option value="<?=($m < 10)?'0' . $m: $m?>"><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                                                            <?php endfor; ?>
                                                                        </select>
                                                                        <?php $startDate = date('y', strtotime('now')); ?>
                                                                        <select name="dt_expiry_year">
                                                                            <option value="">-- Year --</option>
                                                                            <?php foreach (range(intval($startDate), intval($startDate) + 10) as $key => $value) : ?>
                                                                                <option value="<?=$value?>"><?=$value?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                        </form>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane " id="PP">
                                                        <br><br>
                                                        <div class="col-md-12">
                                                            <img class="img-responsive" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/2000px-PayPal.svg.png" alt="PayPal">
                                                        </div>
                                                    </div>
                                                
                                                    <div role="tabpanel" class="tab-pane" id="EN">
                                                        <div class="alert alert-danger e-nett-payment-errors"></div>
                                                        <form id="e-nett-payment" method="POST" class="form-inline" role="form" novalidate>
                                                            <br>
                                                            <div class="form-group col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4">
                                                                <label for="date">Date</label>
                                                                <input type="text" class="form-control text-center" id="date" name="date" placeholder="Receipt date" value="<?=date('d/m/Y', strtotime('now'))?>" readonly>
                                                            </div>
                                                            <div class="form-group col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4">
                                                                <label for="recreipt-ref">Reference</label>
                                                                <input type="text" class="form-control text-center" id="recreipt-ref" name="reference" placeholder="" value="<?=unique_transaction_id()?>" readonly>
                                                                <!-- <input type="text" class="form-control text-center" id="recreipt-ref" name="reference" placeholder="" value="" readonly> -->
                                                            </div>
                                                            <div class="form-group col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4">
                                                                <label for="recreipt-amount" class="text-center">Amount</label>
                                                                <input type="text" class="form-control text-center" id="recreipt-amount" name="amount" placeholder="" data-original="<?=$costing[ $displatPrice ]?>" value="<?=number_format( $costing[ $displatPrice ], 2, '.', '')?>" readonly>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php else: ?>
                                            <h3 class="header" >Payment</h3>
                                            <div role="tabpanel">
                                                <!-- Nav tabs -->
                                                <ul id="customer-payment-nav" class="nav nav-tabs nav-justified" role="tablist">
                                                    <li role="presentation" class="active">
                                                        <a href="#CC" aria-controls="CC" role="tab" data-toggle="tab"><strong>Credit Card <span style="color: green;" class="fa fa-check-circle"></span></strong></a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#PP" aria-controls="PP" role="tab" data-toggle="tab"><strong>Paypal <span style="color: green;" class="fa fa-check-circle"></span></strong></a>
                                                    </li>
                                                    <li role="presentation" >
                                                        <a href="#BD" aria-controls="BD" role="tab" data-toggle="tab"><strong>Bank Deposit <span style="color: green;" class="fa fa-check-circle"></span></strong></a>
                                                    </li>
                                                </ul>
                                            
                                                <!-- Tab panes -->
                                                <div class="tab-content">

                                                    <div role="tabpanel" class="tab-pane active" id="CC">
                                                        <br>
                                                        <div class="col-md-12">
                                                            A 1.8% fee will apply for payments with MasterCard and VISA Credit and Debit cards and 3.8% for American Express and Diners. You can also select the Direct Deposit payment method to benefit from no processing fees.
                                                        </div>
                                                        <form id="paymentForm-CC" novalidate="" method="POST" target="my_iframe">
                                                            <input type="hidden" name="token">
                                                            <?php if( PAYWAY_IS_TEST ) : ?>
                                                                <input type="hidden" name="amount" value="<?=$costing[ $displatPrice ]?>" data-original="<?=$costing[ $displatPrice ]?>">
                                                            <?php endif; ?>
                                                            <input type="hidden" name="biller_code" value="<?=htmlentities(PAYWAY_BILLER_CODE)?>">
                                                            <input type="hidden" name="action" value="MakePayment">

                                                            <div class="col-sm-12 col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;  ">
                                                                <!-- ERRORS -->
                                                                <div class="alert alert-danger payment-errors"></div>

                                                                <div class="col-sm-6 col-md-6" style="padding: 8px !important;">
                                                                    <label style="padding-top: 20px !important; padding-left: 40px; font-size: 14px;" >Cardholder Name</label>
                                                                        <input id="payment-form-holder-name" type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                                            class="adjust-input-text"
                                                                            name="nm_card_holder"
                                                                            value="<?=(TEST_INPUTS)? 'John Doe' :''?>">
                                                                    <label style="padding-top: 25px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Card Verification Number</label>
                                                                        <input id="payment-form-ver-number" type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                                            class="adjust-input-text"
                                                                            name="no_cvn"
                                                                            value="<?=(TEST_INPUTS)? '123' :''?>">
                                                                </div>
                                                                 <div class="col-sm-6 col-md-6" style="padding:8px !important;">
                                                                    <label style="padding-top: 20px !important; padding-left: 40px;font-size: 14px;">Card Number</label>
                                                                        <input id="payment-form-card-number" type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                                            class="adjust-input-text"
                                                                            name="no_credit_card"
                                                                            value="<?=(TEST_INPUTS)? '123456789012345' :''?>">

                                                                    <div style="padding-left: 40px;">
                                                                        <label style="padding-top: 25px ; font-size: 14px;" class="col">Expiry Date</label>
                                                                        <br>
                                                                        <select name="dt_expiry_month">
                                                                            <option value="" >-- Month --</option>
                                                                            <?php for($m=1; $m<=12; ++$m): ?>
                                                                                <option value="<?=($m < 10)?'0' . $m: $m?>"><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                                                            <?php endfor; ?>
                                                                        </select>
                                                                        <?php $startDate = date('y', strtotime('now')); ?>
                                                                        <select name="dt_expiry_year">
                                                                            <option value="">-- Year --</option>
                                                                            <?php foreach (range(intval($startDate), intval($startDate) + 10) as $key => $value) : ?>
                                                                                <option value="<?=$value?>"><?=$value?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                           </div>
                                                        </form>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane " id="PP">
                                                        <br><br>
                                                        <div class="col-md-12">
                                                            <img class="img-responsive" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/2000px-PayPal.svg.png" alt="PayPal">
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="BD">
                                                        <br>
                                                        <div class="col-md-12">&nbsp;&nbsp;&nbsp;Payments can be made by bank transfer into the account below. Please fax our office (07 55262944) or e-mail us a copy of your payment. Pricing is subject to change until full payment has been made.
                                                            <br>
                                                            <br>
                                                            <strong>Name:</strong> Select World Travel Westpac BSB 034-216
                                                            <br>
                                                            <strong>ACCN:</strong> 261 424
                                                            <br>
                                                            <strong>SWIFT:</strong> WPACAU2S</div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                    <div id="payment" style="margin-top: 0px;">
                                        <div>
                                            <hr>
                                            <h3 >
                                                <input  id="cancellation-policy" type="checkbox" class="cancellation-policy-status">
                                                <!-- name="policy_read" -->
                                                <label for="cancellation-policy" style="color: #0091ea !important; /*margin-top: 40px;*/font-size: 14px;">I have read the <a target="_blank" href="http://easterneurotours.com.au/terms-and-conditions">terms and conditions</a></label>
                                            </h3>
                                        </div>
                                        <div class="form-row place-order">
                                            <button id="validate-inputs" type="button" class="btn btn-success"> BOOK NOW</button>
                                            <!-- <button id="validate-inputs" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="submitFunc()"> BOOK NOW</button> -->
                                            <!-- Check first if the booking id session does exist then warned the user that this item will be added to the existing book -->
                                            </div>
                                    </div>
                                </div>
                            </div>
                        <!-- </form> -->
                    </div>


                    <div class="col-md-3 ET-detail-sidebar book-form" >
                        <div >
                            <div class="call-to-book panel panel-themes" style="margin-top: 10px; margin-bottom: 10px;">
                                <i class="awe-icon awe-icon-phone" style="margin-left:10px; margin-top: 20px; " ></i>
                                <em style="font-size: 15px; font-weight: bold; margin-top: 20px; margin-left: 20px;">Call to book</em> <span style="padding-bottom:20px; font-size: 14px;"><?=$agency['number']?></span>
                            </div>
                            <div class="booking-info panel panel-themes" style="border: 2px solid #f2f2f2 !important; background-color: #BA1515;" >
                                <h3 style="font-size: 30px; letter-spacing: 3px; text-align: center; border-bottom: 1px solid #EEE;">Booking Info</h3>

                                <form action="" id="paymentForm-PP" method="POST">
                                    <div class="row">
                                            <div class="col-md-12" style="font-size: 15px; padding-left: 0px; padding-right: 0px;">
                                    
                                                   <div class="col-md-4" style="padding-left: 0px;"><strong> Tours Start: </strong></div> <div class="col-md-8"><?=date('d M Y, l', strtotime($_POST['startdate']))?></div><!--   <label style="color: #cccccc">    </label> <br> -->
                                                   <div class="col-md-4" style="padding-left: 0px;"><strong>Tours End:</strong></div>     <div class="col-md-8"><?=date('d M Y, l', strtotime($_POST['startend']));?></div><!-- <label style="color: #cccccc">     </label> <br> -->
                                                   <div class="col-md-4" style="padding-left: 0px;"><strong>Duration:</strong></div>      <div class="col-md-8"><?=$_POST['dateduration'];?></div><!-- <label style="color: #cccccc"><?php echo $_POST['dateduration'];?> </label> <br><br> -->
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>

                                            <?php foreach ($FORMATTED_ITEMS as $key => $products): ?>
                                                <div style="padding-left: 0px;" class="col-md-12">
                                                    <strong> <?=$key?> </strong>
                                                </div>
                                                <?php foreach( $products as $item => $product ) :?>
                                            
                                                    <input type="hidden" name="<?=str_replace(' ', '', $key)?>[<?=$item?>][name]"     value="<?=$product['name']?>">
                                                    <input type="hidden" name="<?=str_replace(' ', '', $key)?>[<?=$item?>][quantity]" value="<?=$product['quantity']?>">
                                                    <input type="hidden" name="<?=str_replace(' ', '', $key)?>[<?=$item?>][price]"    value="<?=$product['price']?>">
                                                    <div class="col-md-8"><?=$product['name']?></div>
                                                    <div class="col-md-4"><?=$product['displayPrice']?></div>
                                                <?php endforeach; ?>
                                                <div class="clearfix"></div>
                                                <br>
                                            <?php endforeach ?>
                                            <div style="padding-left: 0px;" class="col-md-12"><strong>Number of Pax: <?=$_POST['passengers']?></strong></div>
                                            
                                    </div>
                                </form>
                                    <?php if ( $IS_AGENT ): ?>
                                        <!-- AGENT VIEW -->
                                        <div class="agent-view">
                                            <div class="form-group" style="margin-bottom: 0px;">
                                                <label for="" class="control-label col-sm-8">Gross Amount</label>
                                                <div class="col-sm-4">$<?=number_format($costing['gross_amount'], 2, '.', ',')?></div>
                                            </div>
                                            <div class="form-group" style="margin-bottom: 0px;">
                                                <label for="" class="control-label col-sm-8">Commission Amount</label>
                                                <div class="col-sm-4">$<?=number_format($costing['commission'], 2, '.', ',')?></div>
                                            </div>
                                            <div class="form-group" style="margin-bottom: 0px;">
                                                <label for="" class="control-label col-sm-8">Nett Amount</label>
                                                <div class="col-sm-4">$<?=number_format($costing['due_amount'], 2, '.', ',')?></div>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <?php if ( $IS_DISCOUNTED ): ?>
                                        <div for="" style="font-weight: bold; padding-left: 0px;" class="col-sm-12"><?=$discountMessage?></div>
                                        <!-- <div class="col-sm-4">$<?=number_format($costing['discount_amount'], 2, '.', ',')?></div> -->
                                        <br>
                                    <?php endif ?>
    
                                    <!-- <?php if ( ! @$SESSION && ! is_array( $SESSION ) ): ?>
                                        DEPARTURE FEE
                                    <?php endif; ?> -->
                                    <!--====================================
                                    =            RETURN FLIGHTS            =
                                    =====================================-->
                                    
                                    <div style="padding-left: 0px; " class="col-md-12">
                                        <strong> RETURN FLIGHTS PRICE: <br>&nbsp;&nbsp;&nbsp;<span id="departure-fee" data-per-person="<?=(float)@$FLIGHT['price']/intval(@$FLIGHT['pax'])?>" data-price="<?=@$FLIGHT['price']?>"><?=filter_var(@$_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN)?'$' . number_format((float)$FLIGHT['price']/intval($FLIGHT['pax']), 2) . ' X ' . $FLIGHT['pax'] . ' = $' . number_format($FLIGHT['price'], 2):'$0.00'?></span> </strong>
                                    </div>
                                    <br>
                                    <br>
                                    
                                    <!--====  End of RETURN FLIGHTS  ====-->
                                    

                                    <div class="price">
                                        <em>Total for this booking</em>
                                        <span class="amount" id="total-price" data-price="<?=$costing['gross_amount']?>" data-display-price="<?=$costing[ $displatPrice ]?>"> $<?=number_format((isset($FLIGHT['price'])? ($costing['gross_amount'] + $FLIGHT['price']) : $costing['gross_amount']), 2, '.', ',');?></span>
                                        <!-- <div class="cart-added">
                                            <i class="awe-icon awe-icon-check"></i>
                                            Added
                                        </div> -->
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="payment-modal"
                class="modal fade"
                tabindex="-1"
                role="dialog"
                aria-labelledby="paymentModal"
                data-backdrop="static">
                <div class="modal-dialog modal-lg" style="top: 50px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 style="color: #3c763d" class="modal-title text-center" id="paymentModal">Evaluating <span class="fa fa-refresh"></span></h4>
                        </div>
                        <div class="modal-body">
                            <iframe id="my_iframe" style="width: 100%; height: 300px;" name="my_iframe" src="" frameborder="0"></iframe>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="modal fade" id="flightTermsAndCondition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Flight Terms and Condition</h4>
                        </div>
                        <div class="modal-body">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis reiciendis, optio veniam maiores illo et voluptatem alias pariatur. Eveniet tenetur tempora et reprehenderit inventore eum beatae asperiores quas voluptatem quaerat.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-md" style="top: 50px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 style="color: #3c763d" class="modal-title text-center">Success <span class="fa fa-check-circle"></span></h4>
                        </div>
                        <div class="modal-body">
                            Thank you for booking,<br>
                            Your booking details was been sent to your email.
                        </div>
                        <div class="modal-footer"> 
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>