<?php
    function printTree($tree,$isSub = false, $isLogin = FALSE) {
      if(!is_null($tree) && count($tree) > 0) {
            if(!$isSub){
                 echo '<ul class="menu-list">';
            }else{
                 echo '<ul class="sub-menu">';
            }
         
          foreach($tree as $key =>$node) {
          //  print_r($node);
            //  break;
             if($node['val']['parent_id']==0){
              echo '<li class="current-menu-parent"><a class="bx-nav-fcolor" href="'.$node['val']['url'].'">'.$node['val']['label']."</a>";
             }else{
               echo '<li><a href="'.$node['val']['url'].'">'.$node['val']['label']."</a>";
             }

              if(!empty($node['sub_arr'])){
                 printTree($node['sub_arr'],true, $isLogin);
              }
              echo '</li>';
          }
          if ( $isLogin )
          {
            # Code here
            echo '<li><a href="#">Welcome Agent</a></li>';
          } // end of if statement
          if ( !$isSub ) {
            ?>
            <li class="dropdown hidden-md hidden-sm hidden-xs sr-only" id="shopping-cart">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <span class="fa fa-shopping-cart"></span>
                  <span class="label label-danger" role="passenger">0</span>
                </a>
                <ul class="dropdown-menu pull-right">
                  <li class="text-center"><a href="#">Empty</a></li>
                  <li class="divider" role="separator"></li>
                  <li class="text-right total">
                      <a href="#">
                          <h6>
                              <span class="fa fa-usd"></span>
                              <span role="price">0.00</span>
                          </h6>
                      </a>
                  </li>

                </ul>
            </li>
            <?php
          }
          echo '</ul>';
           
         
      }
  }
?>
        <!-- PRELOADER -->
        <div class="preloader"></div>
        <!-- END / PRELOADER -->
        
        <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
          <header class="custom-theme-header">
            <?php TZ_custom_theme_header( $this->nativesession->get('agencyRealCode') ) ?>
          </header>
        <?php else: ?>
          <!-- HEADER PAGE -->
          <header id="header-page" class = "">
              <div class="header-page__inner panel-themes">
                  <div class="container">
                      <!-- LOGO -->
                      <div class="logo">
                          <?php $img_url = isset($agency['img_url']) ? $agency['img_url'] : FALSE;?>
                          <?php if ($img_url): ?>
                            <?php $image = @getimagesize(B2B_IP."uploads/".$img_url) ?>
                            <?php if ( is_array($image) ) :?>
                              <img src="<?php echo B2B_IP."uploads/".$img_url?>">
                            <?php endif; ?>
                          <?php endif ?>
                      </div>
                      <!-- END / LOGO -->


                      <nav class="adjust-navigation navigation awe-navigation" data-responsive="1200" style="color: #fff!important;">
                          <?php printTree($menu, FALSE, ( isset( $SESSION ) && is_array( $SESSION ) )); ?> 
                      </nav>
                      <!-- END / NAVIGATION -->
                      
                      <!-- SEARCH BOX 
                      <div class="search-box">
                          <span class="searchtoggle"><i class="glyphicon glyphicon-search"></i></span>
                          <form class="form-search">
                              <div class="form-item">
                                  <input type="text" value="Search &amp; hit enter">
                              </div>
                          </form>
                      </div> -->
                      <!-- END / SEARCH BOX -->


                      <!-- TOGGLE MENU RESPONSIVE -->
                      <button type="button" id="shopping-cart-button" class="btn pull-right sr-only hidden-lg">
                          <span class="fa fa-shopping-cart"></span>
                          <span class="label label-danger" role="passenger">0</span>
                      </button>
                      <a class="toggle-menu-responsive" href="#">
                          <div class="hamburger">
                              <span class="item item-1"></span>
                              <span class="item item-2"></span>
                              <span class="item item-3"></span>
                          </div>
                      </a>
                      <!-- END / TOGGLE MENU RESPONSIVE -->

                  </div>
              </div>
          </header>
        <?php endif ?>