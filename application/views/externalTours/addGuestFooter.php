    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
            <footer class="custom-theme-footer">
                <?php TZ_custom_theme_footer( $this->nativesession->get('agencyRealCode') ) ?>
            </footer>
        <?php else: ?>
 <!-- FOOTER PAGE -->
        <footer id="footer-page" class="divs">
            <a name="contact"></a>
            <div class="container">
                <div class="row">

                    <!-- WIDGET -->
                    <div class="col-md-4">
                        <div class="widget widget_about_us">
                            <h3 style="color: #fff!important;">Contact Us</h3>
                            <div class="widget_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form>
                                                <h5 style="color:#EDEDED;">
                                                    <span class="fa fa-globe"></span>
                                                    RESERVATION Office
                                                </h5>
                                                <address>
                                                 
                                                    <br>
                                                    <?=$agency['address'];?>
                                                    <br>
                                                    <label> Phone:</label>
                                                    <?=$agency['number'];?>
                                                    <br>
                                                    <?php if ( ! empty( $agency['fax'] ) ) : ?>
                                                        <label> Fax:</label>
                                                        <?=$agency['fax'];?>
                                                        <br>
                                                    <?php endif ?>
                                                    <label> Toll Free:</label>
                                                    1800 242 353
                                                    <br>
                                                </address>
                                                <address>
                                                    </address>
                                                    <address>
                                                    <strong>Hours of Operation </strong>
                                                    <br>
                                                    Mon-Fri: 08.00 am – 17.30 pm
                                                    <br>
                                                    Sat: 24 hour Contact 0433 161 250
                                                    <br>
                                                    Sun: 24 hour Contact 0433 161 250
                                                    <br>
                                                </address>
                                            </form>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="well well-sm" style="background-color: #333;border:0px solid #FFF">
                                            
                                                <form accept-charset="utf-8" method="post" action="<?php echo base_url();?>php_mailer/sendMessage">
                                                    <input type="hidden" name="redirectLink" value="<?=$_POST['previousURL']?>">
                                                    <input type="hidden" name="to" value="<?=$agency['email']?>">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> Name</label>
                                                                <input id="name" class="form-control" type="text" required="required" placeholder="Enter name" style="border:1px solid #ddd; height: 45px;" name="name" >
                                                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Email</label>
                                                                <input id="email" class="form-control" type="text" required="required" placeholder="Enter email" style="border:1px solid #ddd; height: 45px;" name="email" >
                                                                <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Subject</label>
                                                                <input id="subject" class="form-control" type="text" required="required" placeholder="Enter subject" style="border:1px solid #ddd; height: 45px;" name="subject" >
                                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                               <label> Message</label>
                                                               <textarea id="message" class="form-control" placeholder="Message" required="required" cols="25" rows="9" style="border:1px solid #ddd;border-radius: 0px" name="message" value="<?php echo set_value('message'); ?>"></textarea>
                                                               <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="qna">What is 3 + 2?</label>
                                                                <input id="qna" class="form-control" type="text" required="required" placeholder="Answer" name="captcha" style="border:1px solid #ddd; height: 45px;">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <button id="btnContactUs" class="btn btn-primary" type="submit"> Send Message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / WIDGET -->
                </div>
                        <div class="widget widget_follow_us" style="text-align: center;">
                            <div class="widget_content">
                                <div class="awe-social">
                                    <?php $socials = _AGENCY_SOCIALS( $this->nativesession->get('agencyRealCode') ) ?>
                                    <?php if ( !!$socials ): ?>
                                        <?php foreach ( $socials as $social ): ?>
                                            <a href="<?=$social['link'] ?>"><i class="fa fa-<?=strtolower(str_replace(' ', '-', $social['title'])) ?>"></i></a>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                <div class="Copyright &copy;">
                    <p>&copy;2015 <?=$agency['agency_name'] ?>&trade; All rights reserved.</p>
                </div>
            </div>
        </footer>
        <!-- END / FOOTER PAGE -->
        <?php endif ?>

    </div>
    <!-- END / PAGE WRAP -->

     <!-- LOAD JQUERY -->
  
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.magnific-popup.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url();?>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/wow.min.js"></script>

    <script type="text/javascript" src="<?=base_url();?>static/js/faker.min.js"></script>
    <!--load bootstrap.js-->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>

    <!-- load form validation -->
    <script type="text/javascript" src="<?=base_url()?>static/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/waitMe.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/jquery.serialize-object.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.number.min.js"></script>

    <!-- REVOLUTION DEMO -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.tools.min.js"></script> -->


    <script type="text/javascript">
        (function ( $ ) {
            var $container      = $( '.booking-container' );
            var $content        = $container.find('.content');
            var $passengers     = $content.find( '.passengers' );
            var $voucher        = $content.find( '.voucher' );
            var $paymentMethods = $content.find( '.pament-methods' );
            var $sideBar        = $container.find('.side-bar');
            var IS_WIN_FROM_API = false;
            var waitMeAnimation = {
                    effect : 'pulse',
                    text   : 'Please wait . . .',
                    bg     : 'rgba(255,255,255,0.7)',
                    color  : '#000',
                    maxSize: '',
                    source : ''
                };

            test_inputs();

            console.log('window', $( window ));
            console.log('window inner height', window.innerHeight);
            console.log('screen', screen);
            console.log('side bar', $( '.side-bar-content' ).offset().top);
            console.log('offset', (window.innerHeight - $( '.side-bar-content' ).offset().top) );

            $( document ).scroll(function () {
                // console.log( 'scroll position', $( this ).scrollTop() );
            });



            /*$('body').scrollspy({
                target: '.side-bar',
                offset: 0
            });
            pageAffix();

            $( window ).resize(function () {
                console.log( $( window ).width() );
                var affix = $('.side-bar-content');
                var width = affix.width();

                if ( $( window ).width() >= 992 )
                {
                    pageAffix();
                    affix.width(width);
                } else {
                    affix.width('');
                    $(window).off('.affix');
                }
                                }
            });*/


            $passengers.find('[data-bind]').change(function () {
                var $this = $( this );
                $voucher.find('[name=\'' + $this.data('bind') + '\']').val( $this.val() );
            });
            $content.find('#pay-later').change(function () {
                var $this = $( this );
                
                if( $this.prop('checked') )
                {
                    $this.next().find('span').removeClass('fa-square-o').addClass('fa-check-square-o');
                    $paymentMethods.find( '.payment-wrapper' ).waitMe({
                        effect : 'none',
                        text   : '',
                        bg     : 'rgba(255,255,255,0.7)',
                        color  : '#000',
                        maxSize: '',
                        source : ''
                    });
                } else {
                    $paymentMethods.find( '.payment-wrapper' ).waitMe('hide');
                    $this.next().find('span').removeClass('fa-check-square-o').addClass('fa-square-o');
                }
            });

            $('button[data-action="submit-button"]').click(function () {

                var data = $( '#book-information' ).serializeObject();
                var code = $paymentMethods.find('.nav-tabs li.active > a').data('category');

                data.payment_type = paymentCodeToString( code );
                data.payment      = baseCodeData( code );
                data.isPayable    = ($paymentMethods.find('#pay-later').length > 0) ? (! $paymentMethods.find('#pay-later').prop('checked')) : true;
                data.policy_read  = $content.find('#policy').prop('checked');

                data.includeFlight = <?=$_POST['hasFlight'] ?>;
                data.isDiscounted  = <?=$IS_DISCOUNTED?'true': 'false' ?>;
                // console.log( 'data', data );
                validateInputs( data );
            });

            /**
             * [validateInputs description]
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            function validateInputs ( data )
            {
                $content.waitMe( waitMeAnimation );
                $.post('<?=base_url() ?>externalToursAddGuest/validate', data)
                .done(function ( response ) {
                    // console.log( response );
                    $content.waitMe('hide');

                    var details = data;
                    details.products = getProduts();
                    BootstrapDialog.show({
                        type    : BootstrapDialog.TYPE_PRIMARY,
                        title   : '<strong>Review</strong>',
                        cssClass: 'review-modal',
                        message: $( '<div class="col-xs-12"></div>' ).load('<?=base_url() ?>application/views/externalTours/addGuest-modal-template.php', details),
                        buttons: [
                            {
                                id      : 'history-link',
                                label   : '<span class="fa fa-repeat"></span> <strong>Back to main tour</strong>',
                                cssClass: 'pull-left dialog-remove-top-margin',
                                action  : function ( dialog ) {
                                    window.location.href = '<?=base_url() . $_POST['previousURL'] ?>'
                                }
                            },
                            {
                                label   : '<strong>Book and Finalize <span class="fa fa-thumbs-up"></span></strong>',
                                cssClass: 'btn-danger btn-lg pull-right',
                                action  : function ( dialog ) {
                                    console.log('Book now');
                                    dialog.close();
                                    $content.waitMe( waitMeAnimation );
                                    // bookItem( data );
                                    bookByPayment( data, response );
                                }
                            }
                        ]
                    });
                })
                .fail(function ( error ) {
                    // console.log( error );
                    MOBILE.hide();
                    $content.waitMe('hide');
                    $content.find( '.error-container' ).addClass('sr-only');
                    $content.find('.payment-wrapper [role="tablist"] span.icon').removeClass('fa-exclamation-circle').addClass('fa-check-circle');

                    BootstrapDialog.show({
                        title  : '<strong>Oops, something wen\'t wrong!!</strong>',
                        type   : BootstrapDialog.TYPE_DANGER,
                        message: function ( dialog ) {
                            var template = '<div class="row">';
                            
                            if ( ('responseJSON' in error) )
                            {
                                var errors = error.responseJSON;

                                if ( ( 'errors' in errors ) )
                                {
                                    var errors   = errors.errors;
                                    var message = ( 'main' in errors )? errors.main : '';

                                    $.each(errors, function ( key, items ) {
                                        if ( key != 'payment' )
                                        {
                                            var $container = $content.find( '.error-container[data-type="' + key + '"]' );
                                            // $container.addClass('sr-only');

                                            if ( $container.length > 0 )
                                            {
                                                var $body = $container.find('.media-body');
                                                var $ul = $( '<ul></ul>' );

                                                $.each(items, function ( index, item ) {
                                                    $ul.append('<li>' + item + '</li>');
                                                });

                                                $body.empty();
                                                $body.append( $ul );
                                                $container.removeClass('sr-only');
                                            }
                                        } else {
                                            switch ( data.payment_type.toLowerCase().trim() )
                                            {
                                                case 'creditcard':
                                                    var $container = $content.find('.error-container[data-type="credit-card"]');
                                                    var $body = $container.find('.media-body');
                                                    var $ul = $( '<ul></ul>' );

                                                    $.each(items, function ( index, item ) {
                                                        $ul.append('<li>' + item + '</li>');
                                                    });

                                                    $body.empty();
                                                    $body.append( $ul );
                                                    $container.removeClass('sr-only');

                                                    $content.find('.payment-wrapper [role="tablist"] [data-category="CC"] span.icon').removeClass('fa-check-circle').addClass('fa-exclamation-circle');

                                                    break;

                                                case 'e-nett':
                                                    var $container = $content.find('.error-container[data-type="e-nett"]');
                                                    var $body = $container.find('.media-body');
                                                    var $ul = $( '<ul></ul>' );

                                                    $.each(items, function ( index, item ) {
                                                        $ul.append('<li>' + item + '</li>');
                                                    });

                                                    $body.empty();
                                                    $body.append( $ul );
                                                    $container.removeClass('sr-only');

                                                    $content.find('.payment-wrapper [role="tablist"] [data-category="EN"] span.icon').removeClass('fa-check-circle').addClass('fa-exclamation-circle');

                                                    break;
                                            }
                                        }
                                    });
                                } else {
                                    message = 'Something went%27t wrong upon processing your request.';
                                }
                            } else {
                                message = '<div class="col-xs-12"><strong>Something went wrong upon validating your request. Please contact the administrator.</strong></div>';
                            }

                            template += message;
                            template += '</div>';

                            return template;
                        },
                        onshown: function ( dialog ) {
                            var $body = dialog.getModalBody();
                            var $hovers = $body.find('a[data-hover]');
                            
                            if ( $hovers.length )
                            {
                                $body.find('a[data-hover]').click(function () {
                                    var $this = $( this );
                                    var $element  = $content.find( $this.data('hover') );
                                    var offset = 0;

                                    if ( $this.data('offset') )
                                    {
                                        switch ( $this.data('offset') )
                                        {
                                            case 'middle':
                                                offset = $( window ).height() / 2;
                                                break;
                                            default :
                                                offset = parseInt( $this.data('offset') );
                                                break;
                                        }
                                    }
                                    $( 'html, body' ).animate({scrollTop: $element.offset().top - offset}, 250);
                                });
                            }
                        }
                    });
                });
            };

            /**
             * [bookByPayment description]
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            function bookByPayment ( data, response )
            {
                console.log( 'data', data, 'response', response );

                switch ( data.payment_type.toLowerCase().trim() )
                {
                    case 'creditcard':
                        creditCardPayment( data, response );
                        break;
                        
                    case 'paypal':
                        paypalPayment( data, response );
                        break;
                        
                    case 'e-nett':
                        bookItem( data );
                        break;
                        
                    case 'bankdeposit':
                        data.isPayable = false;
                        data.payment = { discount: <?=$costing['discount_amount']?>, amount: <?=$FINAL_PRICE ?> }
                        bookItem( data );
                        break;

                    case 'paylater':

                        break;
                }
            };

            /**
             * [bookItem description]
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            function bookItem ( data )
            {
                data.PROMOCODE     = "<?=@$_POST['PROMOCODE'] ?>";
                data.inclusion     = "<?=$inclusions?>";
                
                data.tour          = <?=json_encode($_POST['tour']) ?>;
                data.item          = <?=json_encode($_POST)?>;

                data.items         = <?=json_encode($_POST['items'])?>;
                data.flight        = <?=json_encode(@$_POST['flight']) ?>;
                data.product       = <?=json_encode($_POST['product']) ?>;
                data.costing       = <?=json_encode($costing)?>;
                data.supplements   = <?=json_encode($SUPPLEMENTS) ?>;
                data.previous_link = '<?=$_POST['previousURL'] ?>';

                delete data.products;
                $.post('<?=base_url() ?>externalToursAddGuest/createBooking', data)
                .done(function ( response ) {
                    console.log(response);
                    $content.waitMe('hide');
                    BootstrapDialog.show({
                        type    : BootstrapDialog.TYPE_SUCCESS,
                        title   : '<strong><span class="fa fa-flag-checkered"></span> Success!!!</strong>',
                        closable: false,
                        message : ''
                            +'<div class="col-xs-12">'
                                +'<p>'
                                    +'<strong>Thank you for your booking, your booking details was been sent to your email.</strong>'
                                +'</p>'
                            +'</div>',
                        buttons: [{
                            label   : '<strong><span class="fa fa-repeat"></span> Book for more</strong>',
                            cssClass: 'btn-default',
                            action  : function ( dialog ) {
                                window.location.href = '<?=base_url() . $_POST['previousURL'] ?>';
                            }
                        }]
                    });
                })
                .fail(function ( error ) {
                    BootstrapDialog.show({
                        type    : BootstrapDialog.TYPE_DANGER,
                        title   : '<strong>Oops, something wen\'t wrong!!</strong>',
                        closable: false,
                        message : ''
                            +'<div class="col-xs-12">'
                                +'<p>'
                                    +'<strong>Something wrong upon processing your request.</strong>'
                                +'</p>'
                            +'</div>',
                        buttons: [{
                            label   : '<strong>Close</strong>',
                            cssClass: 'btn-default',
                            action  : function ( dialog ) {
                                dialog.close();
                                $content.waitMe('hide');
                            }
                        }]
                    });
                });
            };

            /**
             * [creditCardPayment description]
             * @param  {[type]} data     [description]
             * @param  {[type]} response [description]
             * @return {[type]}          [description]
             */
            function creditCardPayment ( data, response )
            {
                var payment = $paymentMethods.find('#payment-credit-card').serializeObject();
                payment.payment_amount = response['CREDITCARD']['AMOUNT'];
                payment.payment_date   = response['CREDITCARD']['DATE'];
                payment.payment_status = response['CREDITCARD']['STATUS'];
                payment.payment_time   = response['CREDITCARD']['TIME'];
                payment.discount       = <?=intval($costing['discount_amount'])?>;

                data.payment = payment;

                bookItem( data );
            };

            /**
             * Save data
             * @param  {[type]} data     [description]
             * @param  {[type]} response [description]
             * @return {[type]}          [description]
             */
            function paypalPayment( data, response )
            {
                console.info('PayPal');

                data.includeFlight = data.products.hasFlight;
                data.isDiscounted  = data.products.discounted;
                data.PROMOCODE     = "<?=@$_POST['PROMOCODE'] ?>";
                data.inclusion     = "<?=$inclusions?>";
                
                data.tour          = <?=json_encode($_POST['tour']) ?>;
                data.item          = <?=json_encode($_POST)?>;
                data.items         = <?=json_encode($_POST['items'])?>;
                data.flight        = <?=json_encode(@$_POST['flight']) ?>;
                data.product       = <?=json_encode($_POST['product']) ?>;
                data.costing       = <?=json_encode($costing)?>;
                data.supplements   = <?=json_encode($SUPPLEMENTS) ?>;
                data.reference     = response.PAYPAL.REFERENCE;
                data.token         = response.PAYPAL.TOKEN;
                data.previous_link = '<?=$_POST['previousURL'] ?>';

                $.post('<?=base_url() ?>externalToursAddGuest/savePayPalData', data)
                .done(function ( response ) {
                    window.location.href = response.REDIRECT;
                })
                .fail(function ( error ) {
                    BootstrapDialog.show({
                        type   : BootstrapDialog.TYPE_DANGER,
                        title  : '<strong>Oops, something wen\'t wrong!!</strong>',
                        message: '<div class="col-xs-12">The process was suddenly stopped.</div>',
                        onhidden: function ( dialog ) {
                            $content.waitMe('hide');
                        }
                    });
                });
            };

            /**
             * [getProduts description]
             * @return {[type]} [description]
             */
            function getProduts ()
            {
                var data = {
                    isAgent   : <?=$IS_AGENT ? 'true' : 'false' ?>,
                    hasFlight : <?=$_POST['hasFlight'] ?>,
                    discounted: <?=$IS_DISCOUNTED?'true': 'false' ?>,
                    prices    : <?=json_encode($_POST['prices']) ?>,
                    
                    passengers: '<?=$_POST['passengers']?>',

                    dates: {
                        start   : '<?=date('d M Y, l', strtotime($_POST['tour']['start']))?>',
                        end     : '<?=date('d M Y, l', strtotime($_POST['tour']['end']))?>',
                        duration: '<?=$_POST['tour']['duration'];?>'
                    },
                    
                };

                if ( data.isAgent )
                {
                    data.agency = {
                        due       : '$<?=number_format($costing['due_amount'], 2, '.', ',')?>',
                        gross     : '$<?=number_format($costing['gross_amount'], 2, '.', ',')?>',
                        commission: '$<?=number_format($costing['commission'], 2, '.', ',')?>',
                    };
                }
                <?php if( filter_var(@$_POST['hasFlight'], FILTER_VALIDATE_BOOLEAN) ) :?>
                data.flight = {
                    name    : 'Return Flight Price X<?=$_POST['passengers']?>',
                    quantity: '<?=$_POST['passengers']?>',
                    price   : '<?=$FLIGHT['price'] ?>'
                };
                <?php endif ?>
                
                if ( data.discounted )
                {
                    data.discount = {
                        amount : '<?=$costing['discount_amount']?>',
                        message: '<?=$discountMessage?>'
                    };
                }

                data.products = <?=json_encode($_POST['items']) ?>;

                return data;
            };

            /**
             * Get the payment data to validate base on the selected payment method
             * @param  {[type]} code [description]
             * @return {[type]}      [description]
             */
            function baseCodeData ( code )
            {
                var data = {};

                switch( code.toLowerCase() )
                {
                    case 'cc':
                        data = $paymentMethods.find('#payment-credit-card').serializeObject();
                        data.amount = <?=$FINAL_PRICE?>;
                        data.payment_amount = <?=$FINAL_PRICE?>;
                        break;
                    case 'pp':
                        data = $( '#paymentForm-PP' ).serializeObject();
                        data.amount = <?=$FINAL_PRICE?>;
                        break;
                    case 'bd':
                        
                        break;
                    case 'en':
                        data = $paymentMethods.find('#e-nett-payment').serializeObject();
                        break;
                }

                return data;
            };

            /**
             * [paymentCodeToString description]
             * @param  {[type]} code [description]
             * @return {[type]}      [description]
             */
            function paymentCodeToString ( code )
            {
                var string = '';
                switch( code.toLowerCase() )
                {
                    case 'cc':
                        string = 'CREDITCARD';
                        break;
                    case 'pp':
                        string = 'PAYPAL';
                        break;
                    case 'bd':
                        string = 'BANKDEPOSIT';
                        break;
                    case 'en':
                        string = 'E-NETT';
                        break;
                }
                return string;
            };

            /**
             * [test_inputs description]
             * @return {[type]} [description]
             */
            function test_inputs ()
            {
                if( <?=TEST_INPUTS? 'true' : 'false' ?> )
                {
                    var $emptyFields = $container.find('input[data-type]:not([data-bind]), select[data-type]:not([data-bind])');

                    $.each( $emptyFields, function (key, ele) {
                        var $this = $( ele );

                        if( $this.get(0).tagName.toLowerCase() == 'select' )
                        {
                            var $options = $this.find('option');
                            var $options = $.grep( $options, function ( n, i ) {
                                return ( $( n ).val() != '' );
                            });

                            var choices = [];
                            $.each( $options, function ( key, option) {
                                var $ele = $( option );
                                choices.push( $ele.val() );
                            });

                            try {
                                var random = Math.floor(Math.random() * choices.length) + 1;
    
                                $this.val( choices[ ( random - 1 ) ] );
                            } catch ( e ) { }
                        } else {
                            var val = '';
                            switch ( $this.data('type') ) {
                                case 'name':
                                    val = faker.name.firstName();
                                    break;
                                case 'email':
                                    val = faker.internet.email();
                                    break;
                                case 'address':
                                    val = faker.address.streetAddress();
                                    break;
                                case 'phone':
                                    val = faker.phone.phoneNumber();
                                    break;
                                case 'city':
                                    val = faker.address.city();
                                    break;
                            }
                            $this.val( val );
                        }
                    });

                    var $creditCard = $paymentMethods.find('#payment-method-credit-card');
                    $creditCard.find('#payment-credit-card-name').val('TEST');
                    $creditCard.find('#payment-credit-card-number').val('4564710000000004');
                    $creditCard.find('#payment-credit-card-verification-number').val('847');
                    $creditCard.find('#payment-credit-card-expiry-month').val('02');
                    $creditCard.find('#payment-credit-card-expiry-year').val('19');
                }
            };

            /*==============================
            =            Mobile            =
            ==============================*/
            
            var MOBILE = {
                eleSidebar     : $( '.side-bar' ),
                eleShoppingCart: $( '.shopping-cart' ),
                /**
                 * [init description]
                 * @return {[type]} [description]
                 */
                init: function () {
                    var screenHeight = window.innerHeight;
                    var $content = this.eleSidebar.find('.side-bar-content');
                    $content.attr('style', 'height: ' + screenHeight + 'px; overflow-y: scroll;');
                    
                    this.hide();
                    $content.stop();
                    this.eleShoppingCart.click(function () {
                        if ( $content.data('show') )
                        {
                            MOBILE.hide();
                            return true;
                        }
                        MOBILE.show();
                    });
                },
                /**
                 * [show description]
                 * @param  {Function} callback [description]
                 * @return {[type]}            [description]
                 */
                show: function ( callback ) {
                    if ( ! this.isMobile() ) { return true; }
                    var $content = this.eleSidebar.find('.side-bar-content');

                    $content.stop();
                    $content.animate({top: 0},250, callback || false);
                    $content.data('show', true);
                },
                /**
                 * [show description]
                 * @param  {Function} callback [description]
                 * @return {[type]}            [description]
                 */
                hide: function ( callback ) {
                    if ( ! this.isMobile() ) { return true; }
                    var $content = this.eleSidebar.find('.side-bar-content');
                    var screenHeight = window.innerHeight;

                    $content.stop();
                    $content.animate({top: screenHeight},250, callback || false);
                    $content.data('show', false);
                },
                /**
                 * [isMobile description]
                 * @return {Boolean} [description]
                 */
                isMobile: function () {
                    return ($( window ).width() < 992);
                },

                /**
                 * [reset description]
                 * @return {[type]} [description]
                 */
                reset: function () {
                    var $content = this.eleSidebar.find('.side-bar-content');
                    $content.removeAttr('style');
                    
                    this.hide();
                    $content.stop();
                }
            };
            
            /*=====  End of Mobile  ======*/
            

            /**
             * [affixInit description]
             * @return {[type]} [description]
             */
            function affixInit ()
            {
                var $element      = $( '.side-bar' ).find('.side-bar-content');
                var scrollIn      = 0;
                var currentWidth  = $( '.side-bar' ).width();
                var screenHeight  = window.innerHeight;
                var elementHeight = $element.outerHeight();

                $element.width( currentWidth );

                if ( elementHeight > screenHeight )
                {
                    scrollIn = (elementHeight - screenHeight) + 5;
                }

                // console.log( '$element.offset().top', $element.offset().top, 'screenHeight', screenHeight, 'elementHeight', elementHeight, 'scrollIn', scrollIn );

                $element.affix({
                    offset: {
                        top   : $element.offset().top + scrollIn,
                        bottom: function () {
                            return (this.bottom = $( 'footer' ).outerHeight(true) + 20)
                        }
                    }
                });
            }
            setTimeout(function () {
                
                affixInit();
                $( window ).resize(function () {
                    if ( window.innerWidth < 992 )
                    {
                        $( window ).off('.affix');
                        $( '.side-bar' ).find('.side-bar-content').css('width', '');
                        $( '.side-bar' ).find('.side-bar-content').removeData('bs.affix').removeClass('affix affix-top affix-bottom');
                        MOBILE.init();
                    } else {
                        MOBILE.reset();
                        if ( ! $('.side-bar-content[class*="affix"]').length )
                        {
                            affixInit();
                        } else {
                            $( '.side-bar' ).find('.side-bar-content').width( $( '.side-bar' ).width() );
                        }
                    }
                });
                $( window ).resize();
            }, 1);
        })(jQuery);
    </script>
    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <?php TZ_custom_theme_script( $this->nativesession->get('agencyRealCode') ) ?>
    <?php endif; ?>
</body>
</html>