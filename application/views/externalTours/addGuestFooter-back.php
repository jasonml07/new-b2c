 <!-- FOOTER PAGE -->
        <footer id="footer-page" class="divs">
            <a name="contact"></a>
            <div class="container">
                <div class="row">

                    <!-- WIDGET -->
                    <div class="col-md-4">
                        <div class="widget widget_about_us">
                            <h3 style="color: #fff!important;">Contact Us</h3>
                            <div class="widget_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form>
                                                <h5 style="color:#EDEDED;">
                                                    <span class="fa fa-globe"></span>
                                                    RESERVATION Office
                                                </h5>
                                                <address>
                                                 
                                                    <br>
                                                    <?=$agency['address'];?>
                                                    <br>
                                                    <label> Phone:</label>
                                                    <?=$agency['number'];?>
                                                    <br>
                                                    <?php if ( ! empty( $agency['fax'] ) ) : ?>
                                                        <label> Fax:</label>
                                                        <?=$agency['fax'];?>
                                                        <br>
                                                    <?php endif ?>
                                                    <label> Toll Free:</label>
                                                    1800 242 353
                                                    <br>
                                                </address>
                                                <address>
                                                    </address>
                                                    <address>
                                                    <strong>Hours of Operation </strong>
                                                    <br>
                                                    Mon-Fri: 08.00 am – 17.30 pm
                                                    <br>
                                                    Sat: 24 hour Contact 0433 161 250
                                                    <br>
                                                    Sun: 24 hour Contact 0433 161 250
                                                    <br>
                                                </address>
                                            </form>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="well well-sm" style="background-color: #333;border:0px solid #FFF">
                                            
                                                <form accept-charset="utf-8" method="post" action="<?php echo base_url();?>php_mailer/sendMessage">
                                                    <input type="hidden" name="redirectLink" value="<?=$_POST['previousURL']?>">
                                                    <input type="hidden" name="to" value="<?=$agency['email']?>">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> Name</label>
                                                                <input id="name" class="form-control" type="text" required="required" placeholder="Enter name" style="border:1px solid #ddd; height: 45px;" name="name" >
                                                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Email</label>
                                                                <input id="email" class="form-control" type="text" required="required" placeholder="Enter email" style="border:1px solid #ddd; height: 45px;" name="email" >
                                                                <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Subject</label>
                                                                <input id="subject" class="form-control" type="text" required="required" placeholder="Enter subject" style="border:1px solid #ddd; height: 45px;" name="subject" >
                                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                               <label> Message</label>
                                                               <textarea id="message" class="form-control" placeholder="Message" required="required" cols="25" rows="9" style="border:1px solid #ddd;border-radius: 0px" name="message" value="<?php echo set_value('message'); ?>"></textarea>
                                                               <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="qna">What is 3 + 2?</label>
                                                                <input id="qna" class="form-control" type="text" required="required" placeholder="Answer" name="captcha" style="border:1px solid #ddd; height: 45px;">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <button id="btnContactUs" class="btn btn-primary" type="submit"> Send Message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / WIDGET -->
                </div>
                        <div class="widget widget_follow_us" style="text-align: center;">
                            <div class="widget_content">
                                <div class="awe-social">
                                    <?php $socials = _AGENCY_SOCIALS( $this->nativesession->get('agencyRealCode') ) ?>
                                    <?php if ( !!$socials ): ?>
                                        <?php foreach ( $socials as $social ): ?>
                                            <a href="<?=$social['link'] ?>"><i class="fa fa-<?=strtolower(str_replace(' ', '-', $social['title'])) ?>"></i></a>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                <div class="Copyright &copy;">
                    <p>&copy;2015 <?=$agency['agency_name'] ?>&trade; All rights reserved.</p>
                </div>
            </div>
        </footer>
        <!-- END / FOOTER PAGE -->

    </div>
    <!-- END / PAGE WRAP -->

     <!-- LOAD JQUERY -->
  
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.magnific-popup.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url();?>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/wow.min.js"></script>

    <script type="text/javascript" src="<?=base_url();?>static/js/faker.min.js"></script>
    <!--load bootstrap.js-->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>

    <!-- load form validation -->
    <script type="text/javascript" src="<?=base_url()?>static/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/waitMe.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/jquery.serialize-object.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.number.min.js"></script>

    <!-- REVOLUTION DEMO -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.tools.min.js"></script> -->

    <script type="text/javascript">
    
    
    
        var data = <?php echo json_encode($_POST); ?>;
    
        jQuery(document).ready(function() {
            jQuery( '.passengers input[data-bind], .passengers select[data-bind]' ).each(function( key, ele ) {
                var $_ele = $( this );
    
                if( $_ele.get(0).tagName == 'SELECT' )
                {
                    $_ele.change(function () {
                        var $this = $( this );
                        $( '.voucher select[name="' + $this.data('bind') + '"]' ).val( $this.val() );
                    });
                } else {
                    $_ele.keyup(function () {
                        var $this = $( this );
                        $( '.voucher input[name="' + $this.data('bind') + '"]' ).val( $this.val() );
                    });
                }
    
            });
    
            <?php if ( TEST_INPUTS ): ?>
                // GENERATOR
                $.each( $paxForm.find('input, select'), function ( key, ele ) {
                    var val = '';
                    var type = $( ele ).attr('type');
                    
                    if( typeof $( ele ).data('value') != 'undefined' )
                    {
                        if( type == 'text' || type == 'email' && $( ele ).get(0).tagName === 'INPUT' ) {
                            switch ( $( ele ).data('value') ) {
                                case 'name':
                                    val = faker.name.firstName();
                                    break;
                                case 'email':
                                    val = faker.internet.email();
                                    break;
                                case 'address':
                                    val = faker.address.streetAddress();
                                    break;
                                case 'phone':
                                    val = faker.phone.phoneNumber();
                                    break;
                                case 'city':
                                    val = faker.address.city();
                                    break;
                            }
                            $( ele ).val( val );
                        }
                        if( $( ele ).get(0).tagName === 'SELECT' )
                        {
                            var _choices = $( ele ).data('choices');
    
                            try {
                                var _choices = _choices.split('|');
                                var random   = Math.floor(Math.random() * _choices.length) + 1;
    
                                $( ele ).val( _choices[ ( random - 1 ) ] );
                            } catch ( e ) {
    
                            }
                        }
                    }
                } );
                $( 'form #paymentForm input#payment-form-card-number' ).val('123456789012345');
                $( 'form #paymentForm input#payment-form-ver-number' ).val('1234');
            <?php endif ?>
    
        });
    
     function submitFunc(){
    
        var bookingData = {}
    
        var adultfname = [];
        var fields = document.getElementsByName("adultfname[]");
        for(var i = 0; i < fields.length; i++) {
            adultfname.push(fields[i].value);
        }
    
        var adultlname = [];
        var fields1 = document.getElementsByName("adultlname[]");
        for(var i1 = 0; i1 < fields1.length; i1++) {
            adultlname.push(fields1[i1].value);
        }
    
        bookingData.adultnames = [];
        for(var i2 = 0; i2 < data.adult; i2++) {
            bookingData.adultnames.push({first_name: adultfname[i2] + ' ' + adultlname[i2]});
        }
    
        ///////////////////////////////////////////////////////////////////////////////
    
        var childfname = [];
        var fields = document.getElementsByName("childfname[]");
        for(var i3 = 0; i3 < fields.length; i3++) {
            childfname.push(fields[i3].value);
        }
    
        var childlname = [];
        var fields = document.getElementsByName("childlname[]");
        for(var i4 = 0; i4 < fields.length; i4++) {
            childlname.push(fields[i4].value);
        }
    
        bookingData.childnames = [];
        for(var i5 = 0; i5 < data.getchild; i5++) {
            bookingData.childnames.push({first_name: childfname[i5] + ' ' + childlname[i5]});
        }
    
        ///////////////////////////////////////////////////////////////////////////////
    
        //console.log(bookingData);
    
    
            // jQuery.ajax({
            //     type:"POST",
            //     url: "<?php base_url();?>example",
            //     data: data,
            //     success:function(response)
            //     {
            //       //console.log(data);
            //     }
            // });
    
        }
    
        var $errorsWrapper          = $('div.voucher-errors, div.payment-errors, div.passenger-errors, div.e-nett-payment-errors, div.flight-errors');
        var $mainFormWrapper        = $( 'div#main-forms-wrapper' );
        var $paymentModal           = $( 'div#payment-modal' );
        var $paxForm                = $( 'form#paxForm' );
        var $paymentFormCC          = $( 'form#paymentForm-CC' );
        var $paymentFormBD          = $( 'form#paymentForm-BD' );
        var $paymentFormPP          = $( 'form#paymentForm-PP' );
        var $enettForm              = $( 'form#e-nett-payment' );
        var $payLater               = $( 'input#pay-later' );
        var $totalPrice             = $( 'span#total-price' );
        var $departureFee           = $( 'span#departure-fee' );
        var $flightDetail           = $( 'div.fligh-details' );
        var $flightOption           = $( 'div.flight-details-option' );
        var $flightPreferredAirline = $( 'a#flight-preferred-airlines' );
        var $flightTermCondition    = $( 'a#flight-terms-and-condition' );
        var $isFlightIncluded       = $( 'input[name="includeFlight"]' );
        var $customerPaymentNav     = $( 'ul#customer-payment-nav' );
        var monthPrices             = <?=DEPARTURE_MONTH?>;
        var AVAILABLE_AIRLINES      = <?=AVAILABLE_AIRLINES?>;
        var PAYMENT_TYPES           = <?=PAYMENT_TYPES?>;
        var IS_WIN_FROM_API         = false;
        var waitMeAnimation         = {
                    effect : 'pulse',
                    text   : 'Please wait . . .',
                    bg     : 'rgba(255,255,255,0.7)',
                    color  : '#000',
                    maxSize: '',
                    source : ''
                };
        
        (function ( $ ) {
    
            $( document ).ready( function () {
    
                $customerPaymentNav.find('span.fa-check-circle').hide();
                $customerPaymentNav.find('li').click( function () {
                    $customerPaymentNav.find('span.fa-check-circle').hide();
                    $( this ).find('span.fa-check-circle').show();
                });
                $customerPaymentNav.find('li.active').trigger('click');
    
    
                /*=====================================
                =            FLIGHT SCRIPT            =
                =====================================*/
                
                /*$flightTermCondition.click(function () {
                    BootstrapDialog.show({
                        title  : '<strong>Flight Terms and Conditions</strong>',
                        message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum quia porro nisi odio doloribus veritatis veniam ducimus commodi rerum blanditiis tempore atque, dolores rem aliquid, beatae, inventore! Enim, suscipit, quo?',
                        cssClass: 'term-and-condition',
                        buttons: [
                            {
                                label : 'Disagree',
                                action: function ( dialog ) {
                                    $( 'input[name="flight[terms]"]' ).prop('checked', false);
                                    dialog.close();
                                }
                            }, {
                                label   : '<strong>Agree</strong>',
                                cssClass: 'btn-primary',
                                action  : function ( dialog ) {
                                    $( 'input[name="flight[terms]"]' ).prop('checked', true);
                                    dialog.close();
                                }
                            }
                        ]
                    });
                });
                $flightPreferredAirline.click(function () {
                    BootstrapDialog.show({
                        title  : '<strong>Airlines Used - subject to availability <span class="fa fa-plane"></span></strong>',
                        message: function ( dialog ) {
                            var size     = Math.round(AVAILABLE_AIRLINES.length / 2);
                            var $message = $( '<div class="dialog-content col-md-12"></div>' );
    
                            size++;
                            var split = [AVAILABLE_AIRLINES.slice(0, (size - 1)), AVAILABLE_AIRLINES.slice(size)];
    
                            split.forEach(function ( item ) {
                                var $div = $( '<div class="col-md-6 col-sm-6"></div>' )
                                var $ul  = $( '<ul></ul>' );
    
                                item.forEach(function ( airline ) {
                                    $ul.append('<li>' + airline + '</li>');
                                });
    
                                $div.append( $ul );
                                $message.append( $div );
                            });
    
                            dialog.getModalBody().css('height', '190px');
    
                            return $message;
                        },
                        // onshown: function ( dialog ) {
                        //     var $message = dialog.getModalBody().find('div.dialog-content');
                        //     dialog.getModalBody().css('height', ($message.height() + 30));
                        // }
                    });
                });
    
                disableFlight(  );
    
                $( 'input[name="includeFlight"]' ).change(function () {
                    disableFlight( ( $( this ).val() == 'false' ) );
                });
                
    
                // GENERATOR
                $( 'input.flight-date' ).datepicker({
                    dateFormat: "dd/mm/yy",
                    beforeShow: function ( input, inst ) {
                        $('#ui-datepicker-div').removeClass('booking-calendar').addClass('booking-calendar');
                    }
                });
    
                $( 'input.return.flight-date' ).datepicker('option', 'minDate', new Date('<?=date('Y-m-d', strtotime($_POST['startdate'] . ' +1 day'))?>'));
    
                $( 'input.departure.flight-date' ).datepicker('option', 'minDate', new Date('<?=date('Y-m-d', strtotime('now +1 day'))?>'));
                $( 'input.departure.flight-date' ).datepicker('option', 'onSelect', function ( dateText, inst ) {
                    // console.log( 'On Select', dateText, inst );
                    var month = $.datepicker.formatDate('MM', $( this ).datepicker('getDate'));
                    month = month.toUpperCase();
    // console.log( $.datepicker.formatDate('d M, y', $( this ).datepicker('getDate')) )
                    var departureDate = new Date( $.datepicker.formatDate('yy-m-d', $( this ).datepicker('getDate')) );
                    departureDate.setDate(departureDate.getDate() + <?=intval($_POST['dateduration'])?>);
    
                    $( 'input.return.flight-date' ).datepicker('option', 'minDate', departureDate);
                    $( 'input.return.flight-date' ).datepicker('setDate', departureDate);
    
                    var departurePrice =  0;
                    var departureText = '$0.00';
                    if( typeof monthPrices[ month ] != 'undefined' )
                    {
                        departurePrice = monthPrices[ month ] * <?=intval($_POST['passengers'])?>;
                        var subTotal   = $totalPrice.data('price') + departurePrice;
    
                        $departureFee.data('perPerson', monthPrices[ month ]);
                        $totalPrice.text('$' + $.number(subTotal, 2));
                        departureText = '$' + $.number(monthPrices[ month ], 2) + ' X <?=$_POST['passengers']?> = $' + $.number(departurePrice, 2);
                    } else {
                        $totalPrice.text('$' + $.number($totalPrice.data('price'), 2));
                        departurePrice = 0;
                    }
                    $departureFee.text( departureText );
                    $departureFee.data('price', departurePrice);
                    $( 'input[name="flight[price]"]' ).val( departurePrice );
                    updatePaidAmount();
                });*/
    
                /*=====  End of FLIGHT SCRIPT  ======*/
    
                <?php if ( $IS_AGENT ): ?>
                    var $navs = $( 'ul#payment-nav' );
    
                    $navs.find('span.fa-check-circle').hide();
    
                    $navs.find( 'li' ).click(function (  ) {
                        var $this = $( this );
    
                        $navs.find('span.fa-check-circle').hide();
                        $this.find('span.fa-check-circle').show();
                        $navs.find('li').data('active', false);
    
                        $this.data('active', true);
                    });
                    $navs.find('li.active').click();
    
                    $payLater.change(function () {
                        if( $( this ).prop('checked') )
                        {
                            $( 'div.payment-wrapper' ).waitMe({
                                effect : 'none',
                                text   : '',
                                bg     : 'rgba(255,255,255,0.7)',
                                color  : '#000',
                                maxSize: '',
                                source : ''
                            });
    
                            $( this ).next('span.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
                        } else {
                            $( 'div.payment-wrapper' ).waitMe('hide');
                            $( this ).next('span.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
                        }
                    });
                <?php endif ?>
    
                $.each( $errorsWrapper, function ( key, val ) {
                    $( this ).hide();
                });
    
    
    
                /*BootstrapDialog.confirm({
                    title         : '<span class="fa fa-info-circle"></span> ALERT!!!',
                    message       : 'Are you sure you want to add this item to your existing booking?',
                    draggable     : true,
                    closable      : true,
                    cssClass      : 'adjust-bootstrap-dialog',
                    btnCancelLabel: 'No',
                    btnOKClass    : 'btn-success',
                    btnOKLabel    : 'Yes'
                });*/
    
                /*================================================
                =            CODE FOR SUBMITTING FORM            =
                ================================================*/
                
                $( 'button#validate-inputs' ).on('click', function (  ) {
                    console.log('validate-inputs');
    
                    $.each( $errorsWrapper, function ( key, val ) {
                        $( this ).find('ul').remove();
                        $( this ).hide();
                    });
                    
                    // $mainFormWrapper.waitMe( waitMeAnimation );
                    // TODO
                    // Get all the form data of the pax and verify inputs
                    var post        = $paxForm.serializeObject();
                    var payment     = {};
                    var paymentType = PAYMENT_TYPES['CC'];
    
                    post.isPayable    = ( $payLater.length )? (! $payLater.prop('checked')):true;
                    // post.isCreditCard = true;
    
                    <?php if ( isset( $SESSION ) && is_array( $SESSION ) ): ?>
    
                        if( post.isPayable )
                        {
                            $navs.find('li').each(function ( key, ele ) {
                                if( $( this ).data('active') )
                                {
                                    var type = $( this ).find('a').attr('aria-controls');
                                    switch ( type )
                                    {
                                        case 'PP':
                                            console.info('PayPal Payment');
                                            payment     = $paymentFormPP.serializeObject();
    
                                            if( $isFlightIncluded.prop('checked') )
                                            {
                                                payment.departure = [{
                                                    name    : 'Return Flight Price X' + <?=$_POST['passengers']?>,
                                                    quantity: <?=$_POST['passengers']?>,
                                                    price   : $departureFee.data('perPerson')
                                                }];
                                            }
                                            break;
                                        case 'CC':
                                            payment = $paymentFormCC.serializeObject();
                                            break;
                                        case 'EN':
                                            payment                = $enettForm.serializeObject();
                                            payment.payment_amount = payment.amount;
                                            break;
                                    }
    
                                    paymentType = PAYMENT_TYPES[ type ];
                                }
                            });
                        } else {
                            if( $payLater.prop('checked') )
                            {
                                paymentType = PAYMENT_TYPES['PL'];
                            }
                        }
                    <?php endif ?>
    
                    if ( $customerPaymentNav.length )
                    {
                        console.log( 'Customer Active' );
                        var $anchor = $customerPaymentNav.find('li.active a');
                        var type    = $anchor.attr('aria-controls');
    
                        switch( type )
                        {
                            case 'PP':
                                console.info('PayPal Payment');
                                payment     = $paymentFormPP.serializeObject();
    
                                if( $isFlightIncluded.prop('checked') )
                                {
                                    payment.departure = [{
                                        name    : 'Return Flight Price X' + <?=$_POST['passengers']?>,
                                        quantity: <?=$_POST['passengers']?>,
                                        price   : $departureFee.data('perPerson')
                                    }];
                                }
                                break;
                            case 'BD':
                                console.info('Bank Deposit Payment');
                                // payment        = $paymentFormBD.serializeObject();
                                post.isPayable = false;
                                break;
                            case 'CC':
                                console.info('Credit Card Payment');
                                payment     = $paymentFormCC.serializeObject();
                                break;
                        }
                        paymentType = PAYMENT_TYPES[ type ];
                    }
    
                    post.policy_read    = $( 'input#cancellation-policy' ).prop('checked');
                    post.payment        = payment;
                    post.payment_type   = paymentType;
                    post.payment.amount = <?=(isset($FLIGHT['price'])? ($costing['gross_amount'] + $FLIGHT['price']) : $costing['gross_amount']) ?>
    
                    /*if( $isFlightIncluded.prop('checked') )
                    {
                        post.payment.amount += $departureFee.data('price');
                    }*/
    
    
                    <?php if ( @$_POST['PROMOCODE'] ): ?>
                        post.payment.discount = <?=$costing['discount_amount']?>;
                    <?php endif ?>
    
                    $.post( '<?=base_url()?>externalToursAddGuest/validate', post )
                        .done( function ( response ) {
                            // console.log( response );
                            
                            /*BootstrapDialog.show({
                                type    : BootstrapDialog.TYPE_INFO,
                                title   : 'LOGS',
                                message : 'See console log',
                                cssClass: 'adjust-bootstrap-dialog'
                            });*/
                            /**
                            
                                TODO:
                                - Show review on dialog
                                - continue process
                            
                             */
                            /*console.log(post);
                            BootstrapDialog.show({
                                type   : BootstrapDialog.TYPE_INFO,
                                title  : '<strong>Summary on your booking</strong>',
                                message: function ( dialog ) {
                                    var template = ''
                                        +'<div class="row">'
                                            +'<h5>Passengers</h5>'
                                            +'<div class="form-horizontal">'
                                                +'<div class="form-group">'
                                                    +'<label for="" class="control-label col-sm-3">Name:</label>'
                                                    +'<div class="col-sm-9">'
                                                        +'<p class="form-control-static"></p>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group"><label for="" class="control-label col-sm-3"></label></div>'
                                            +'</div>'
                                        +'</div>'
                                        +'<div class="row">'
                                            +'<h5>Voucher</h5>'
                                            +'<div class="form-horizontal">'
                                                +'<div class="form-group">'
                                                    +'<label for="" class="control-label col-sm-3">Name:</label>'
                                                    +'<div class="col-sm-9">'
                                                        +'<p class="form-control-static">Mr. Blood Sucker</p>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group">'
                                                    +'<label for="" class="control-label col-sm-3">Email:</label>'
                                                    +'<div class="col-sm-9">'
                                                        +'<p class="form-control-static">dongskay@gmail.com</p>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group">'
                                                    +'<label for="" class="control-label col-sm-3">Contact number:</label>'
                                                    +'<div class="col-sm-9">'
                                                        +'<p class="form-control-static">9128748712691623</p>'
                                                    +'</div>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>'
                                        +'<div class="row">'
                                            +'<h5>Payment</h5>'
                                            +'<div class="form-horizontal">'
                                                +'<div class="form-group">'
                                                    +'<label for="" class="control-label col-sm-3">Type of Payment:</label>'
                                                    +'<div class="col-sm-9">'
                                                        +'<p class="form-control-static"></p>'
                                                    +'</div>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>'
                                        ;
    
                                        return template;
                                },
                                buttons: [
                                    {
                                        label: '<span class="fa fa-chevron-left"></span> Back to main tour',
                                        cssClass: 'btn-primary pull-left dialog-remove-top-margin',
                                        action: function ( dialog ) {
                                            console.log('Back to tour');
                                        }
                                    },
                                    {
                                        label: '<strong>Book Now!</strong>',
                                        cssClass: 'btn-success pull-right',
                                        action: function ( dialog ) {
                                            console.log('Book now');
                                            $mainFormWrapper.waitMe( waitMeAnimation );
                                        }
                                    }
                                ]
                            });
                            return false;*/
                            
                            <?php if ( $IS_AGENT ): ?>
                                // ENEET / CREDIT CARD
                                if( post.isPayable )
                                {
                                    switch ( post.payment_type )
                                    {
                                        case 'PAYPAL':
                                            payPalPayment( response );
                                            break;
                                        case 'CREDITCARD':
                                            confirmPayment( response );
                                            break;
                                        case 'E-NETT':
                                            createBooking( post );
                                            break;
                                    }
                                    /*if( post.payment_type = 'CREDITCARD' )
                                    {
                                        creaditPayment( response );
                                    } else {
                                        // ENETT CREATE BOOKING
                                        createBooking( post );
                                    }*/
                                } else {
                                    // CREATE BOOKING WITHOUT PAYMENT
                                    createBooking( post );
                                }
                            <?php else: ?>
                                // CREDIT CARD
                                switch ( post.payment_type )
                                {
                                    case 'PAYPAL':
                                        payPalPayment( response );
                                        break;
                                    case 'CREDITCARD':
                                        confirmPayment( response );
                                        break;
                                    case 'BANKDEPOSIT':
                                        createBooking( post );
                                        break;
                                }
                            <?php endif ?>
                        })
                        .fail( function ( response ) {
                            $mainFormWrapper.waitMe('hide');
                            $payLater.change();
                            // console.log( response.responseJSON );
                            $.each( response.responseJSON.errors, function ( key, val ) {
                                if( key != 'main' )
                                {
                                    var $errorWrap = $( 'div.' + key + '-errors' );
    
                                    // console.log( $errorWrap );
    
                                    if( $errorWrap.length )
                                    {
                                        var $ul = $( '<ul></ul>' );
    
                                        $.each( val, function ( keyError, error ) {
                                            $ul.append( '<li>' + error + '</li>' );
                                        });
    
                                        $errorWrap.append( $ul );
                                        $errorWrap.show();
                                    }
                                } else if ( key == 'main' ) {
                                    BootstrapDialog.show({
                                        type    : BootstrapDialog.TYPE_DANGER,
                                        cssClass: 'adjust-bootstrap-dialog',
                                        title   : '<span class="fa fa-times-circle"></span> <strong>Opps! Something went wrong</strong>',
                                        message : val,
                                        onshown : function ( dialog ) {
                                            var $scrollTo = dialog.getModalBody().find('a[data-scroll-to]');
    
                                            if( $scrollTo.length )
                                            {
                                                $scrollTo.click(function () {
                                                    var $element = $( $( this ).data('scrollTo') );
                                                    console.log( $element );
                                                    if ( $element.length ) {
                                                        $( 'html, body' ).animate({scrollTop: $element.offset().top - ($( window ).height() / 2)}, 250);
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        });
                });
                /*=====  End of CODE FOR SUBMITTING FORM  ======*/
                
            });
    
            /**
             * [creaditPayment description]
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            function creaditPayment ( response )
            {
                console.log( response );
                var $iFrame = $( 'iframe#my_iframe' );
    
                $paymentModal.modal('show');
                $iFrame.parent().waitMe( waitMeAnimation );
    
                $paymentFormCC.attr('action', response.payway.handOffURL);
                $paymentFormCC.find('input[name="token"]').val( response.payway.token );
                $paymentFormCC.submit();
    
                $iFrame.on('load', function () {
                    $( this ).parent().waitMe('hide');
                });
            };
    
            var openWin = function () {
                window.open('', '_blank');
                win.focus();
            };
    
            /**
             * [payPalPayment description]
             * @param  {[type]} response [description]
             * @return {[type]}          [description]
             */
            var payPalPayment = function payPalPayment ( response )
            {
                console.log( response );
                // var $iFrame = $( 'iframe#my_iframe' );
    
                // $iFrame.attr('src', response.PAYPAL.REDIRECT_URL);
                // $iFrame.parent().waitMe( waitMeAnimation );
    
                var x = screen.width/2 - 700/2;
                var y = screen.height/2 - 450/2;
    
                BootstrapDialog.show({
                    type    : BootstrapDialog.TYPE_SUCCESS,
                    cssClass: 'adjust-bootstrap-dialog',
                    title   : '<strong>Evaluating Payment</strong>',
                    size    : BootstrapDialog.SIZE_SMALL,
                    closable: false,
                    message : ''
                        + '<div class="text-center">'
                        +   '<button id="login-paypal" type="button" class="btn btn-primary"><strong>Login your PayPal account</strong></button>'
                        +'</div>',
                    buttons: [{
                        label: '<strong>Close</strong>',
                        action: function ( dialog ) {
                            dialog.close();
                            $mainFormWrapper.waitMe('hide');
                        }
                    }],
                    onshow: function ( dialog ) {
                        var $button = dialog.getModalBody().find('button#login-paypal');
    
                        $button.click(function () {
                            dialog.close();
                            var paypalWindow = window.open( response.PAYPAL.REDIRECT_URL, "Payment Gateway", "width=780,height=410,toolbar=0,scrollbars=1,status=0,resizable=1,location=0,menuBar=0,left=" + x + ",top=" + y, true);
                            var interval     = setInterval(function () {
                                if (paypalWindow.closed) {
                                    console.log('Window is closed');
                                    if( ! IS_WIN_FROM_API )
                                    {
                                        IS_WIN_FROM_API = false;
                                        $mainFormWrapper.waitMe('hide');
                                    }
                                    clearInterval( interval );
                                }
                            }, 1);
                        });
                    }
                });
            }
    
            /**
             * [disableFlight description]
             * @param  {Boolean} isDisabled [description]
             * @return {[type]}             [description]
             */
            function disableFlight ( isDisabled = true )
            {
                var $flightInputs = $( 'input[name^="flight["], select[name^="flight["]' );
    
                $flightInputs.each(function () {
                    if( ! $( this ).prop('readonly') )
                    {
                        $( this ).prop('disabled', isDisabled);
                    }
                });
    
                $departureFee.text( '$' + $.number(0, 2) );
                $totalPrice.text('$' + $.number($totalPrice.data('price'), 2));
                
                $flightDetail.hide();
                // FLIGHT IS INCLUDED
                if( ! isDisabled )
                {
                    $flightDetail.show();
                    var subTotal       = $totalPrice.data('price') + $departureFee.data('price');
                    var departurePrice = $departureFee.data('price');
                    var perPerson      = $departureFee.data('perPerson');
    
                    $totalPrice.text('$' + $.number(subTotal, 2));
    // console.log( $departureFee.data() );
                    $departureFee.text( '$' + $.number(perPerson, 2) + ' X <?=$_POST['passengers']?> = $' + $.number(departurePrice, 2) );
                }
                updatePaidAmount();
            };
    
            /**
             * [updatePaidAmount description]
             * @return {[type]} [description]
             */
            function updatePaidAmount (  )
            {
                var f_price = parseInt( $( 'input[name="flight[price]"]' ).val() );
                var p_amt   = parseInt( $( 'input[name="amount"]' ).data('original') );
                p_amt += f_price;
                $( 'input[name="amount"]' ).val( $.number(p_amt, 2, '.', '') );
            };
        })(jQuery)
    
        /**
         * [paypalPaymenConfirmation description]
         * @param  {[type]} response [description]
         * @return {[type]}          [description]
         */
        var paypalPaymentConfirmation = function paypalPaymentConfirmation ( response )
        {
            console.info( 'paypalPaymenConfirmation' );
            // $mainFormWrapper.waitMe('hide');
            // console.log( response );
    
            if( response['Ack'] == 'Success' )
            {
                var post     = $paxForm.serializeObject();
                var details  = response['GetExpressCheckoutDetailsResponseDetails'];
                var dateTime = new Date( response['Timestamp'] );
                var payment  = {
                    reference     : details['PayerInfo']['PayerID'],
                    card_type     : 'PAYPAL-Express checkout',
                    bank_reference: details['Token'],
                    payment_amount: details['PaymentDetails']['OrderTotal']['value'],
                    payment_date  : jQuery.datepicker.formatDate( "yy-mm-dd", new Date( dateTime ) ),
                    payment_number: dateTime.getTime(),
                    payment_status: 'approved',
                    payment_time  : dateTime.getHours() + ':' + dateTime.getMinutes(),
                    discount      : <?=intval($costing['discount_amount'])?>,
                };
    
                post.policy_read  = jQuery( 'input#cancellation-policy' ).prop('checked');
                post.payment      = payment;
                post.isPayable    = true;
                post.payment_type = PAYMENT_TYPES['PP'];
                createBooking( post );
            } else {
                $mainFormWrapper.waitMe('hide');
                BootstrapDialog.show({
                    type    : BootstrapDialog.TYPE_DANGER,
                    title   : '<span class="fa fa-times-circle"></span> <strong>Something went wrong</strong>',
                    message : 'There was an error while processing your request',
                    cssClass: 'adjust-bootstrap-dialog'
                });
            }
        }
    
        /**
         * [confirmPayment description]
         * @param  {[type]} response [description]
         * @return {[type]}          [description]
         */
        function confirmPayment ( response ) {
            console.info( 'confirmPayment was triggered' );
            // console.log( response );
    
            $paymentModal.modal('hide');
    
    
            // TODO
            // * Check first the response status if the payment is approved
            // * Create the booking
            
            if ( response['CREDITCARD']['STATUS'] == "approved" )
            {
                // book the customer
                var post    = $paxForm.serializeObject();
                var payment = $paymentFormCC.serializeObject();
    
                payment.payment_amount = response['CREDITCARD']['AMOUNT'];
                payment.payment_date   = response['CREDITCARD']['DATE'];
                payment.payment_status = response['CREDITCARD']['STATUS']
                payment.payment_time   = response['CREDITCARD']['TIME'];
                payment.discount       = <?=intval($costing['discount_amount'])?>;
    
                post.policy_read  = jQuery( 'input#cancellation-policy' ).prop('checked');
                post.payment      = payment;
                post.isPayable    = true;
                post.payment_type = PAYMENT_TYPES['CC'];
    
                createBooking( post );
            } else {
                $mainFormWrapper.waitMe('hide');
                BootstrapDialog.show({
                    type    : BootstrapDialog.TYPE_DANGER,
                    title   : '<span class="fa fa-times-circle"></span> <strong>Something went wrong</strong>',
                    message : 'There was an error while processing your request',
                    cssClass: 'adjust-bootstrap-dialog'
                });
            }
        };
    
        /**
         * [createBooking description]
         * @param  {[type]} data [description]
         * @return {[type]}      [description]
         */
        function createBooking ( data ) {
    
            data.costing   = <?=json_encode($costing)?>;
            data.inclusion = "<?=$inclusions?>";
            data.item      = <?=json_encode($_POST)?>;
            data.items     = <?=json_encode($FORMATTED_ITEMS)?>
    
            $.post('<?=base_url()?>externalToursAddGuest/createBooking', data)
                .done( function ( response ) {
    
                    $mainFormWrapper.waitMe('hide');
                    $( 'button#validate-inputs' ).remove();
                    $payLater.change();
                    BootstrapDialog.show({
                        type    : BootstrapDialog.TYPE_SUCCESS,
                        title   : '<span class="fa fa-check"></span> <strong>You are now successfully booked.</strong>',
                        message : 'Your tour was successfully processed',
                        cssClass: 'term-and-condition',
                        closable: false,
                        buttons : [{
                            label   : '<strong><span class="fa fa-home"></span> Home</strong>',
                            cssClass: 'btn btn-primary',
                            action  : function ( dialogRef ) {
                                window.location = '<?=base_url() . $_POST['previousURL']?>';
                            }
                        }, {
                            label   : '<strong><span class="fa fa-eye"></span> Review</strong>',
                            //cssClass: 'btn btn-primary',
                            action  : function ( dialogRef ) {
                                window.location = '<?=base_url()?>externaltours/review?book=' + response.bookingId + '&item=' + response.itemId;
                            }
                        }]
                    });
                })
                .fail( function ( error ) {
                    // console.error( error );
                    $mainFormWrapper.waitMe('hide');
                    $payLater.change();
                    try {
                        BootstrapDialog.show({
                            type    : BootstrapDialog.TYPE_DANGER,
                            title   : '<span class="fa fa-times-circle"></span> <strong>Something went wrong</strong>',
                            message : error.responseJSON.message,
                            cssClass: 'adjust-bootstrap-dialog'
                        });
                    } catch ( e ) {
                        BootstrapDialog.show({
                            type    : BootstrapDialog.TYPE_DANGER,
                            title   : '<span class="fa fa-times-circle"></span> <strong>Something went wrong</strong>',
                            message : 'There was some error occured while processing your request',
                            cssClass: 'adjust-bootstrap-dialog'
                        });
                    }
                });
        };
    
    </script>

</body>
</html>