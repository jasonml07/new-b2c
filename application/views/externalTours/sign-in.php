<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Consultant</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/bootstrap.min.css">
	<style type="text/css">
	.row-centered {
		text-align:center;
		top       : 30%;
		position  : absolute;
		width     : 100%;
	}
	.col-centered {
		display     :inline-block !important;
		float       :none;
		/* reset the text-align */
		text-align  :left;
		/* inline-block space fix */
		margin-right:-4px;
	}

	</style>
</head>
<body>
	<section>
		<div class="row row-centered">
			<div class="col-md-3 col-sm-6 col-centered" style="display: flex; justify-content: center; align-items: center;">
				<div class="panel panel-primary">
					<div class="panel-heading text-nceter"><strong>LOGIN</strong></div>
					<div class="panel-body">
						<?php if ( $this->session->flashdata('logError') ): ?>
							<div class="alert alert-danger"><?=$this->session->flashdata('logError')?></div>
						<?php endif ?>
						<form action="<?=base_url()?>externaltours/verify?product=<?=$product?>" novalidate="" method="POST">
							<?=(@$login?'<input type="hidden" name="login" value="true">':'')?>
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<input type="text" class="form-control" name="username" placeholder="Username">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
									<input type="password" class="form-control" name="password" placeholder="Password">
								</div>
							</div>
							<div class="form-group"><button type="submit" class="btn btn-block btn-primary">LOGIN</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>
</body>
</html>