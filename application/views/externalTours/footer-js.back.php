<script type="text/javascript">
        (function ( $ ) {

            var IS_AGENT = <?=$IS_AGENT ? 'true' : 'false' ?>;
            var PRODUCTS = <?=json_encode($product['productAvailabilityFinal']) ?>;

            var $flowNavigation = $( '#flow-navigation' );
            var $listNavigation = $( '#list-navigation' );
            /**
             * [FLOW_NAVIGATION description]
             * @type {Object}
             */
            var FLOW_NAVIGATION = {
                PRODUCTS                : $flowNavigation.find('[aria-controls="product-list"]'),
                ITEM_SELECTION          : $flowNavigation.find('[aria-controls="product-item-selection"]'),
                PRODUCT_FILL_INFORMATION: $flowNavigation.find('[aria-controls="product-fill-information"]'),
            };
            /**
             * [FLOW_NAVIGATION description]
             * @type {Object}
             */
            var LIST_NAVIGATION = {
                CALENDAR: $listNavigation.find('[aria-controls="product-calendar-view"]'),
                LIST    : $listNavigation.find('[aria-controls="product-list-view"]'),
                COMPACT : $listNavigation.find('[aria-controls="product-compact-view"]'),
            };

            var $eleShoppingCart = $( '#shopping-cart' );
            var $eleShoppingCartBtn = $( '#shopping-cart-button' );
            var SHOPPING_CART = {
                eleMenuContainer: $eleShoppingCart,
                eleButtonContainer: $eleShoppingCartBtn,

                init: function () {
                    this.eleButtonContainer.click(function () {
                        var data = ITEM_SELECTION.overAllTotal(true);

                        var isItOkay = HELPER.isItOkayToLoad(data);
                        if ( ! isItOkay ) {
                            MODALS.NO_PASSENGERS.open();
                            return false;
                        }

                        var template = $('<div></div>').load('<?=base_url()?>application/views/externalTours/template/body-preview-selection.php', data, function ( responseText, textStatus ) {
                            MODALS.PREVIEW_SELECTIONS.setMessage(template.html());
                            MODALS.PREVIEW_SELECTIONS.open();
                        });
                    });
                },
                /*
                 * [load description]
                 * @return {[type]} [description]
                */
                load: function ( data ) {
                    var isItOkay = HELPER.isItOkayToLoad( data );
                    if ( !isItOkay ) {
                        SHOPPING_CART.empty();
                        return false;
                    }

                    this.eleButtonContainer.find('.label-danger').text(data.items.passengers);
                    this.eleMenuContainer.load('<?=base_url() ?>application/views/externalTours/template/header-shopping-cart.php?empty=false', data, function () {

                    });
                },
                /**
                 * [empty description]
                 * @return {[type]} [description]
                */
                empty: function () {
                    this.eleButtonContainer.find('.label-danger').text('0');
                    this.eleMenuContainer.load('<?=base_url() ?>application/views/externalTours/template/header-shopping-cart.php?empty=true');
                },
                /**
                 * [show description]
                 * @return {[type]} [description]
                 */
                show: function () {
                    this.eleMenuContainer.removeClass('sr-only');
                    this.eleButtonContainer.removeClass('sr-only');
                },
                /**
                 * [show description]
                 * @return {[type]} [description]
                 */
                hide: function () {
                    this.eleMenuContainer.addClass('sr-only');
                    this.eleButtonContainer.addClass('sr-only');
                }
            };

            /**
             * [FLIGHT_OPTION description]
             * @type {Object}
             */
            var FLIGHT_OPTION = {
                eleContainer: null,
                INCLUDED: null,

                init: function ( ) {
                    this.eleContainer = $( '#fligth-option' );
                    this.eleContainer.find('input[name="hasFlight"]').change(function () {
                        var passengers = ITEM_CONTAINER.getSubtotal().passengers;
                        var $this = $( this );

                        if ( passengers == 0 )
                        {
                            MODALS.NO_PASSENGERS.open();
                            FLIGHT_OPTION.eleContainer.find('label').removeClass('active');
                            FLIGHT_OPTION.INCLUDED = null;
                            $this.prop('checked', false);
                            return false;
                        }

                        switch( $this.val().toLowerCase().trim() )
                        {
                            case 'true':
                                FLIGHT_OPTION.INCLUDED = true;
                                break;
                            case 'false':
                            default :
                                FLIGHT_OPTION.INCLUDED = false;
                        }
                        if ( FLIGHT_OPTION.INCLUDED )
                        {
                            FLIGHT_DETAILS.show();
                        } else { FLIGHT_DETAILS.hide(); }
                        HELPER.updateDisplay();
                    });
                }
            };

            /**
             * [FLIGHT_DETAILS description]
             * @type {Object}
             */
            var FLIGHT_DETAILS = {
                DEPARTURE_MONTH: <?=DEPARTURE_MONTH ?>,
                eleContainer: null,

                init: function () {
                    var tourData = HELPER.getGlobalTourInfo();
                    this.eleContainer = $( '#flight-details' );

                    var today = new Date();
                    today.setDate(today.getDate() + 2);
                    var minDate = new Date( tourData.SELECTED_DATE );
                    minDate.setDate(minDate.getDate() - 1);
                    var maxDate = new Date( tourData.END_DATE );
                    maxDate.setDate(maxDate.getDate() + 1);

                    this.eleContainer.find('#flight-departure-date').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: today,
                        maxDate: minDate,
                        defaultDate: minDate,
                        onSelect: this.updateData()
                    });
                    this.eleContainer.find('#flight-return').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: maxDate,
                        defaultDate: maxDate,
                    });
                },
                /**
                 * [getMonthPrice description]
                 * @param  {[type]} month [description]
                 * @return {[type]}       [description]
                 */
                getMonthPrice: function ( month ) {
                    var price = 0;
                    if ( month.toUpperCase() in this.DEPARTURE_MONTH )
                    {
                        price = this.DEPARTURE_MONTH[ month.toUpperCase() ];
                    }
                    return price;
                },
                show: function () { this.eleContainer.collapse('show'); },
                hide: function () { this.eleContainer.collapse('hide'); },

                /**
                 * [updateData description]
                 * @return {[type]} [description]
                 */
                updateData: function () {
                    var passengers = ITEM_CONTAINER.getSubtotal().passengers;
                    var date = this.eleContainer.find('#flight-departure-date').val().split('/');
                    var month = $.datepicker.formatDate('MM', new Date(date[2],(parseInt(date[1]) - 1),date[0]));
                    var price = this.getMonthPrice( month );
                    var totalPrice = price * passengers;
                    
                    this.eleContainer.find('#flight-passengers').val( passengers );
                    this.eleContainer.find('[name="perPerson"]').val( price );
                    this.eleContainer.find('#flight-price').val( $.number(totalPrice, 2) );
                    this.eleContainer.find('[name="price"]').val( totalPrice );
                },
                /**
                 * [getSubtotal description]
                 * @return {[type]} [description]
                 */
                getSubtotal: function () {
                    var data = this.eleContainer.find('form').serializeObject();

                    var subtotal = parseInt(data.price);
                    var passengers = parseInt(data.pax);
                    var price = parseInt(data.perPerson);

                    return {
                        city: data.city,
                        class: data.class,
                        departure: data.departure,
                        return: data.return,
                        subtotal: subtotal,
                        price: price,
                        quantity: passengers,
                        inclusions: 'Return flight price ($' + $.number(price, 2) + ' X' + passengers + ' = $' + $.number(subtotal, 2) + ')'
                    };
                }
            };

            var $gridList = $( LIST_NAVIGATION.COMPACT.attr('href') );
            var GRID_LIST = {
                eleContainer: $gridList,

                init: function ( ) {
                    this.eleContainer.find('[data-product-button]').click(function ( e ) {
                        e.preventDefault();

                        var $this = $( this );
                        var split = $this.data('start').split('-');
                        var start = new Date( split[2], (parseInt(split[0]) - 1), split[1] );
                        split = $this.data('end').split('-');
                        var end = new Date( split[2], (parseInt(split[0]) - 1), split[1] );

                        HELPER.setGlobalTourInfo( start, end, parseInt($this.data('key')));

                        HELPER.hookItemSelected();
                        GRID_LIST.clearSelection();
                        GRID_LIST.setActive( $this.data('key') );
                    });
                },
                /**
                 * [clearSelection description]
                 * @return {[type]} [description]
                 */
                clearSelection: function () {
                    this.eleContainer.find('[data-product-button]').removeClass('active');
                },
                /**
                 * [setActive description]
                 * @param {[type]} key [description]
                 */
                setActive: function ( key ) {
                    this.eleContainer.find('[data-product-button][data-key="' + key + '"]').addClass('active')
                }
            };
            var MODALS = {
                PACKAGE: null,
                NO_PASSENGERS: null,
                UNDEFINED_NUMBER: null,
                PREVIEW_SELECTIONS: null,
                CHECKOUT_PROBLEMS: null,

                closeButton: {
                    label: 'Close',
                    action: function ( dialog ) {
                        dialog.close();
                    }
                },

                init: function () {
                    this.CHECKOUT_PROBLEMS = new BootstrapDialog({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: '<strong>Oops, Something wen\'t wrong.</strong>',
                        closable: false,
                        onshown: function ( dialog ) {
                            
                        },
                        buttons: [{
                            label: 'Close',
                            cssClass: 'pull-left',
                            action: function ( dialog ) { dialog.close(); }
                        }]
                    });
                    this.PREVIEW_SELECTIONS = new BootstrapDialog({
                        type: BootstrapDialog.TYPE_INFO,
                        title: '<strong class="text-center">List of selected items</strong>',
                        closable: false,
                        cssClass: 'review-modal',
                        buttons: [{
                                label: 'Close',
                                cssClass: 'pull-left',
                                action: function ( dialog ) { dialog.close(); }
                            },
                            {
                                label: 'Proceed Checkout <i class="fa fa-thumbs-up"></i>',
                                cssClass: 'btn-danger btn-lg',
                                action: function ( dialog ) {
                                    HELPER.proceedToCheckout( dialog );
                                }
                            }
                        ]
                    });
                    this.UNDEFINED_NUMBER = new BootstrapDialog({
                        type: BootstrapDialog.TYPE_WARNING,
                        title: '<strong>Unknown selection</strong>',
                        message: 'Please fill the right input value.',
                        closable: false,
                        buttons: [MODALS.closeButton]
                    });
                    this.NO_PASSENGERS = new BootstrapDialog({
                        type: BootstrapDialog.TYPE_WARNING,
                        title: '<strong>Lack of Passengers</strong>',
                        message: 'Please select some items before selecting packages and flight. <a data-locate="">Click here</a>',
                        closable: false,
                        onshown: function ( dialog ) {
                            dialog.getModalBody().find('[data-locate]').click(function () {
                                dialog.close();
                                $('html, body').animate({ scrollTop: $('#item-container').offset().top - 40}, 500);
                            });
                        },
                        buttons: [MODALS.closeButton]
                    });
                    this.PACKAGE = new BootstrapDialog({
                        type: BootstrapDialog.TYPE_DANGER,
                        title: '<strong>Oops, Something wen\'t wrong.</strong>',
                        closable: false,
                        buttons: [MODALS.closeButton]
                    });
                },
            };
            /**
             * [ITEM_CONTAINER description]
             * @type {Object}
             */
            var ITEM_CONTAINER = {
                eleContainer: null,
                eleBody: null,
                eleFooter: null,

                init: function () {

                    this.eleFooter = this.eleContainer.find('tfoot');
                    this.eleBody = this.eleContainer.find('tbody');

                    this.eleBody.find('[data-selection]').change(function () {
                        var $this = $( this );
                        var pax = parseInt($this.val());
                        var subtotal = pax * $this.data('price');
                        var minguest = parseInt($this.data('minguest'));
                        var $eleTableRow = $this.closest('tr');
                        var previous = {
                            pax: 0,
                            price: 0,
                            supplement: 0
                        };
                        $eleTableRow.removeData('supplement');

                        if ( minguest > 1 )
                        {
                            var $eleSupplement = $eleTableRow.find('[data-item-supplement]');
                            var nearestPax = (pax / minguest);
                            var fixNearestPax = Math.floor(nearestPax);
                            var isDouble = ((nearestPax % 1 ) != 0);
                            
                            if( isDouble )
                            {
                                var supplement = {
                                    key: $eleSupplement.val(),
                                    name: $eleSupplement.data('name'),
                                    price: $eleSupplement.data('price'),
                                    quantity: (pax - (fixNearestPax * minguest)),
                                    subtotal: 0,
                                    costings: HELPER.parseJSON($eleSupplement.data('costings'))
                                };
                                supplement.subtotal = (Math.round(supplement.price) * supplement.quantity)

                                subtotal += supplement.subtotal;
                                previous.supplement = supplement;

                                $eleTableRow.data('supplement', supplement);
                                $eleTableRow.find('.supplement-label').addClass('active');
                            } else {
                                $eleTableRow.find('.supplement-label').removeClass('active');
                            }
                        }
                        previous.pax = pax;
                        previous.price = subtotal;
                        $this.data('previous', previous);
                        $eleTableRow.data('subtotal', subtotal);
                        $eleTableRow.data('quantity', pax);

                        // ITEM_CONTAINER.footer( pax, subtotal );
                        
                        ITEM_CONTAINER.footer( ITEM_CONTAINER.getSubtotal( ) );
                        FLIGHT_DETAILS.updateData();

                        HELPER.updateDisplay();
                    });
                },
                /**
                 * [getSubtotal description]
                 * @return {[type]} [description]
                 */
                getSubtotal: function (  ) {
                    var subtotal = 0;
                    var passengers = 0;
                    var items = [];
                    var inclusions = [];

                    this.eleBody.find('tr').each(function ( key, item ) {
                        var $this = $( item );
                        var data = $this.data();
                        $this.data('costings', HELPER.parseJSON(data.costings));

                        if ( 'subtotal' in data && data.subtotal > 0 )
                        {
                            items.push( data );
                            inclusions.push(data.name + ' X' + data.quantity + ' ($' + $.number(data.price, 2) + ')');

                            subtotal += data.subtotal;
                            passengers += data.quantity;
                        }
                    });
                    return {
                        items: items,
                        subtotal: subtotal,
                        passengers: passengers,
                        inclusions: inclusions.join(', ')
                    };
                },
                /**
                 * [footer description]
                 * @return {[type]} [description]
                 */
                footer: function ( data ) {
                    var data = data || false;
                    var $eleSubtotal = this.eleFooter.find('[data-subtotal]');
                    var $eleSubPax = this.eleFooter.find('[data-passengers]');

                    if( ! data )
                    {
                        return {
                            subtotal: $eleSubtotal.data('subtotal'),
                            passengers: $eleSubPax.data('passengers'),
                        };
                    }

                    $eleSubtotal.data( 'subtotal', data.subtotal ).text( $.number(data.subtotal, 2) );
                    $eleSubPax.data( 'passengers', data.passengers ).text( data.passengers );
                },
                /**
                 * [reloadFooter description]
                 * @return {[type]} [description]
                 */
                reloadFooter: function () {
                    this.eleFooter.load('<?=base_url()?>application/views/externalTours/template/body-products-footer.php');
                }
            };

            /**
             * [PACKAGE_CONTAINER description]
             * @type {Object}
             */
            var PACKAGE_CONTAINER = {
                eleContainer: null,

                init: function () {
                    this.eleContainer.find('[data-selection-button]').click(function () {
                        var $this = $( this );
                        var $parent = $this.closest('div.input-group');
                        var $selection = $parent.find('[data-package]');
                        
                        var quantity = parseInt($selection.val());

                        switch( $this.data('selectionButton') )
                        {
                            case 'subtract':
                                if ( quantity > 0 ) { quantity--; }
                                break;
                            case 'add':
                                quantity++;
                                break;
                        }
                        $selection.val(quantity);
                        $selection.change();
                    });
                    this.eleContainer.find('[data-package]').change(function () {
                        var $this = $( this );
                        var quantity = parseInt($this.val());
                        var passengers = HELPER.getPassengers();
                        var data = $this.data();

                        data.costings = HELPER.parseJSON( data.costings );

                        if ( passengers == 0 )
                        {
                            MODALS.NO_PASSENGERS.open();
                            $this.val( 0 );
                        } else if ( isNaN(quantity) ) {
                            MODALS.UNDEFINED_NUMBER.open();
                            $this.val( 0 );
                        } else if ( quantity > passengers ) {
                            MODALS.PACKAGE.close();
                            MODALS.PACKAGE.setMessage('The number of selection exceed from the number of passengers.');
                            MODALS.PACKAGE.open();

                            $this.val( passengers );
                        }
                        quantity = parseInt( $this.val() );

                        var subtotal = quantity * data.price;
                        data.subtotal = subtotal;
                        data.quantity = quantity;

                        HELPER.updateDisplay();
                    });
                },
                /**
                 * [getSubtotal description]
                 * @return {[type]} [description]
                 */
                getSubtotal: function () {
                    var items = [];
                    var subtotal = 0;
                    var inclusions = [];

                    this.eleContainer.find('[data-package]').each(function () {
                        var $this = $( this );
                        var quantity = parseInt( $this.val() );

                        if ( quantity > 0 )
                        {
                            items.push( $this.data() );
                            inclusions.push($this.data('name') + ' X' + $this.data('quantity') + ' ($' + $.number($this.data('price'), 2) + ')');
                            subtotal += $this.data('subtotal');
                        }
                    });
                    return {
                        items: items,
                        inclusions: inclusions.join(', '),
                        subtotal: subtotal
                    };
                },
                /**
                 * When the total passenger is not equal to the number of selection
                 * Then it will equalize the selection
                 * @return {[type]} [description]
                 */
                resetSelections: function () {

                }
            };
            /**
             * [EXTRABILL_CONTAINER description]
             * @type {Object}
             */
            var EXTRABILL_CONTAINER = {
                eleContainer: null,

                init: function () {

                },
                /**
                 * [getSubtotal description]
                 * @return {[type]} [description]
                 */
                getSubtotal: function () {
                    var passengers = ITEM_CONTAINER.getSubtotal().passengers;
                    var items = [];
                    var subtotal = 0;
                    var inclusions = [];

                    this.eleContainer.find('[data-extrabill]').each(function () {
                        var $this = $( this );
                        var data = $this.data();
                        data.costings = HELPER.parseJSON(data.costings);
                        data.subtotal = data.price * passengers;
                        data.quantity = passengers;

                        subtotal += data.subtotal;

                        inclusions.push( data.name + ' X' + data.quantity + ' ($' + $.number(data.price, 2) + ')' );
                        items.push( data );
                    });

                    return {
                        items: items,
                        inclusions: inclusions.join(', '),
                        subtotal: subtotal
                    };
                }
            };
            /**
             * [ITEM_SELECTION description]
             * @type {Object}
             */
            var $productItemSelection = $( '#product-item-selection' );
            var ITEM_SELECTION = {
                eleContainer: $productItemSelection,
                ITEM_CONTAINER: null,
                PACKAGE_CONTAINER: null,
                EXTRABILL_CONTAINER: null,
                OVERALL_SUBTOTAL: null,

                init: function () {

                },
                /**
                 * [initializeContainers description]
                 * @return {[type]} [description]
                 */
                initializeContainers: function () {
                    ITEM_CONTAINER.eleContainer = this.eleContainer.find('#item-container');
                    ITEM_CONTAINER.init();

                    PACKAGE_CONTAINER.eleContainer = this.eleContainer.find('#packages-container');
                    PACKAGE_CONTAINER.init();

                    EXTRABILL_CONTAINER.eleContainer = this.eleContainer.find('#extrabills-container');
                    EXTRABILL_CONTAINER.init();

                    this.OVERALL_SUBTOTAL = this.eleContainer.find('#overall-total');
                },
                /**
                 * [load description]
                 * @param  {[type]}   data     [description]
                 * @param  {Function} callback [description]
                 * @return {[type]}            [description]
                 */
                load: function ( data, callback ) {
                    this.eleContainer.load('<?=base_url() ?>application/views/externalTours/template/body-item-selections.php', data, callback);
                },
                /**
                 * [initializeButton description]
                 * @return {[type]} [description]
                 */
                initializeButtons: function () {
                    this.eleContainer.find('#back-to-list').click(function () { HELPER.productList(); });
                    this.eleContainer.find('#checkout-items').click(function () {
                        HELPER.proceedToCheckout();
                    });
                    this.eleContainer.find('#preview-items').click(function () {
                        var data = ITEM_SELECTION.overAllTotal(true);

                        if ( ! ('items' in data) || data.items.passengers == 0 )
                        {
                            MODALS.NO_PASSENGERS.open();
                            return false;
                        }

                        var template = $('<div></div>').load('<?=base_url()?>application/views/externalTours/template/body-preview-selection.php', data, function ( responseText, textStatus ) {
                            MODALS.PREVIEW_SELECTIONS.setMessage(template.html());
                            MODALS.PREVIEW_SELECTIONS.open();
                        });
                    });
                },
                /**
                 * [overAllTotal description]
                 * @return {[type]} [description]
                 */
                overAllTotal: function ( dataOnly ) {
                    var dataOnly = dataOnly || false;
                    var overAllTotal = 0;
                    var items = ITEM_CONTAINER.getSubtotal();
                    var extrabill = EXTRABILL_CONTAINER.getSubtotal();
                    var packages = PACKAGE_CONTAINER.getSubtotal();
                    var flight = FLIGHT_DETAILS.getSubtotal();
                    var prices = {
                        product: 0,
                        flight: 0
                    };

                    overAllTotal += items.subtotal;
                    overAllTotal += extrabill.subtotal;
                    overAllTotal += packages.subtotal;

                    prices.product = overAllTotal;

                    if ( FLIGHT_OPTION.INCLUDED )
                    {
                        prices.flight = flight.subtotal;
                        overAllTotal += flight.subtotal;
                    }

                    this.OVERALL_SUBTOTAL.text( $.number(overAllTotal, 2) );

                    if ( dataOnly )
                    {
                        var TOUR = HELPER.getGlobalTourInfo();
                        var tour = {};
                        tour.duration = TOUR.DURATION,
                        tour.start = $.datepicker.formatDate('dd-mm-yy', TOUR.SELECTED_DATE);
                        tour.end = $.datepicker.formatDate('dd-mm-yy', TOUR.END_DATE);

                        return {
                            tour: tour,
                            items: items,
                            extrabill: extrabill,
                            packages: packages,
                            flight: (FLIGHT_OPTION.INCLUDED ? flight : {}),
                            hasFlight: FLIGHT_OPTION.INCLUDED,
                            isDiscounted: false,
                            prices: prices,
                            total: overAllTotal,
                            IS_AGENT: false,
                        }
                    }
                }
            };
            /**
             * [$checkoutItemInformation description]
             * @type {[type]}
             */
            var $checkoutItemInformation = $( '#product-fill-information' );
            var CHECKOUT_ITEMS = {
                eleContainer: $checkoutItemInformation,
                eleVoucher: null,
                elePaymentMethods: null,
                elePassengers: null,
                eleCancelBtn: null,
                eleSelectBtn: null,
                eleBookBtn: null,

                init: function () {
                    this.elePassengers.find('[data-imitate]:not([data-imitate=""])').change(function () {
                        var $this = $( this );
                        CHECKOUT_ITEMS.eleVoucher.find( '[name="' + $this.data('imitate') + '"]' ).val( $this.val() );
                    });
                    this.eleCancelBtn.click(function () {
                        SHOPPING_CART.show();
                        HELPER.productList();
                    });
                    this.eleSelectBtn.click(function () {
                        SHOPPING_CART.show();
                        HELPER.itemSelection();
                    });
                    this.eleBookBtn.click(function () {
                        console.log('Book Now');
                        var data = ITEM_SELECTION.overAllTotal();

                        console.log( 'Booking Data', data );
                    });
                },
                /**
                 * [load description]
                 * @param  {[type]} data [description]
                 * @return {[type]}      [description]
                 */
                load: function ( callback ) {
                    var callback = callback || false;
                    var data = ITEM_SELECTION.overAllTotal(true);
                    console.log('data', data);

                    this.eleContainer.load('<?=base_url()?>application/views/externalTours/template/body-checkout-items.php', data, function () {
                        CHECKOUT_ITEMS.elePassengers = CHECKOUT_ITEMS.eleContainer.find('#checkout-passengers');
                        CHECKOUT_ITEMS.eleVoucher = CHECKOUT_ITEMS.eleContainer.find('#checkout-voucher');
                        CHECKOUT_ITEMS.elePaymentMethods = CHECKOUT_ITEMS.eleContainer.find('#payment-methods');
                        CHECKOUT_ITEMS.eleCancelBtn = CHECKOUT_ITEMS.eleContainer.find('#checkout-cancel');
                        CHECKOUT_ITEMS.eleSelectBtn = CHECKOUT_ITEMS.eleContainer.find('#checkout-selections');
                        CHECKOUT_ITEMS.eleBookBtn = CHECKOUT_ITEMS.eleContainer.find('#proceed-booking');
                        CHECKOUT_ITEMS.init();
                        
                        if( !!callback ) {
                            callback();
                        }
                        SHOPPING_CART.hide();
                        HELPER.formInformation();
                    });
                }
            };

            /**
             * [HELPER description]
             * @type {Object}
             */
            var HELPER = {
                SELECTED_DATE: null,
                END_DATE     : null,
                DURATION     : 0,
                PRODUCT_KEY  : 0,
                /**
                 * [getDateDuration description]
                 * @param  {[type]} start [description]
                 * @param  {[type]} end   [description]
                 * @return {[type]}       [description]
                 */
                setGlobalTourInfo: function ( start, end, key ) {
                    this.PRODUCT_KEY   = key;
                    this.SELECTED_DATE = new Date( start );
                    this.END_DATE      = new Date( end );

                    var timeDiff  = Math.abs( start.getTime() - end.getTime() );
                    this.DURATION = Math.ceil( timeDiff / (1000 * 3600 * 24) );
                },
                /**
                 * [getGlobalTourInfo description]
                 * @return {[type]} [description]
                 */
                getGlobalTourInfo: function ( format_date ) {
                    var format_date = format_date || false;
                    var SELECTED_DATE = this.SELECTED_DATE;
                    var END_DATE      = this.END_DATE;
                    if ( format_date )
                    {
                        SELECTED_DATE = $.datepicker.formatDate('dd-mm-yy', new Date(this.SELECTED_DATE));
                        END_DATE      = $.datepicker.formatDate('dd-mm-yy', new Date(this.END_DATE));
                    }
                    return {
                        SELECTED_DATE: SELECTED_DATE,
                        END_DATE: END_DATE,
                        DURATION: this.DURATION
                    };
                },
                /**
                 * Trigger an event or status in all list view
                 * @return {[type]} [description]
                 */
                hookItemSelected: function () {
                    console.info('Hooking things');

                    var packages    = _PRODUCT.getPackages( this.PRODUCT_KEY );
                    var extraBills  = _PRODUCT.getExtrabills( this.PRODUCT_KEY );
                    var items       = _PRODUCT.getItems( this.PRODUCT_KEY );
                    var supplements = _PRODUCT.getSupplements( this.PRODUCT_KEY );
                    var tour        = this.getGlobalTourInfo( true );
                    ITEM_SELECTION.load( {packages: packages, extraBills: extraBills, items: items, supplements: supplements, tour: tour}, function (  ) {
                        HELPER.itemSelection();
                        HELPER.initialize();
                    });

                },
                /**
                 * [initialize description]
                 * @return {[type]} [description]
                 */
                initialize: function () {
                    ITEM_SELECTION.initializeButtons();
                    ITEM_SELECTION.initializeContainers();
                    FLIGHT_OPTION.init();
                    FLIGHT_DETAILS.init();
                    SHOPPING_CART.empty();
                    SHOPPING_CART.show();
                },
                /**
                 * [returnToProductList description]
                 * @return {[type]} [description]
                 */
                productList: function () { FLOW_NAVIGATION.PRODUCTS.click(); },
                /**
                 * [itemSelection description]
                 * @param  {[type]} ) {            FLOW_NAVIGATION.ITEM_SELECTION.click( [description]
                 * @return {[type]}   [description]
                 */
                itemSelection: function () { FLOW_NAVIGATION.ITEM_SELECTION.click(); },
                /**
                 * [formInformation description]
                 * @param  {[type]} ) {            FLOW_NAVIGATION.PRODUCT_FILL_INFORMATION.click( [description]
                 * @return {[type]}   [description]
                 */
                formInformation: function () { FLOW_NAVIGATION.PRODUCT_FILL_INFORMATION.click(); },
                /**
                 * [parseJSON description]
                 * @param  {[type]} jsonString [description]
                 * @return {[type]}            [description]
                 */
                parseJSON: function ( jsonString ) {
                    var costing = jsonString;
                    try {
                        costing = jsonString.replace(/(\{|,)\s*(.+?)\s*:/g, '$1 "$2":');
                        costing = $.parseJSON( costing );
                    } catch ( e ) { }

                    return costing;
                },
                /**
                 * [getPassengers description]
                 * @return {[type]} [description]
                 */
                getPassengers: function () {
                    return ITEM_CONTAINER.getSubtotal().passengers;
                },
                /**
                 * [isInRange description]
                 * @param  {[type]}  start   [description]
                 * @param  {[type]}  end     [description]
                 * @param  {[type]}  current [description]
                 * @return {Boolean}         [description]
                 */
                isInRange: function ( start, end, current ) {
                    var current = current || new Date();

                    current = $.datepicker.formatDate('yy-mm-dd', current);

                    start   = $.datepicker.parseDate('yy-mm-dd', start);
                    end     = $.datepicker.parseDate('yy-mm-dd', end);
                    current = $.datepicker.parseDate("yy-mm-dd", current);

                    if( (current <= end && current >= start) )
                    {
                        return true;
                    }
                    return false;
                },
                /**
                 * [updateDisplay description]
                 * @return {[type]} [description]
                 */
                updateDisplay: function () {
                    var data = ITEM_SELECTION.overAllTotal(true);
                    
                    SHOPPING_CART.load( data );
                },
                /**
                 * [isItOkayToLoad description]
                 * @param  {[type]}  data [description]
                 * @return {Boolean}      [description]
                 */
                isItOkayToLoad: function ( data ) {
                    var data = data || false;
                    if ( ! data || ! ('items' in data) || data.items.passengers == 0 ) { return false; }
                    return true;
                },
                /**
                 * [proceedToCheckout description]
                 * @return {[type]} [description]
                 */
                proceedToCheckout: function ( dialog ) {
                    var dialog = dialog || false;
                    var isItOkay = HELPER.isItOkayToLoad( ITEM_SELECTION.overAllTotal(true) );
                    if ( !isItOkay ) {
                        MODALS.NO_PASSENGERS.open();
                        return false;
                    }
                    if ( !!dialog ) { dialog.close(); }
                    CHECKOUT_ITEMS.load();
                }
            };

            /*=====================================
            =            Product Class            =
            =====================================*/
            
            var _PRODUCT = {
                PRODUCTS   : PRODUCTS,

                /**
                 * [getPackages description]
                 * @return {[type]} [description]
                 */
                getPackages: function ( key ) {
                    return this.packageOrBills(key, 0);
                },
                /**
                 * [getExtrabills description]
                 * @param  {[type]} key [description]
                 * @return {[type]}     [description]
                 */
                getExtrabills: function ( key ) {
                    return this.packageOrBills(key, 1);
                },
                /**
                 * [packageOrBills description]
                 * @param  {[type]} key  [description]
                 * @param  {[type]} type [description]
                 * @return {[type]}      [description]
                 */
                packageOrBills: function ( key, type ) {
                    var data = [];

                    $.each(this.getProduct( key ).product_supplements, function ( key, item ) {
                        if ( item.minguest == type )
                        {
                            data.push( item );
                        }
                    });

                    return data;
                },
                /**
                 * [getProduct description]
                 * @param  {[type]} key [description]
                 * @return {[type]}     [description]
                 */
                getProduct: function ( key ) {
                    return this.PRODUCTS[ key ];
                },
                /**
                 * [getItems description]
                 * @param  {[type]} key [description]
                 * @return {[type]}     [description]
                 */
                getItems: function ( key ) {
                    return this.getProduct( key ).supplement_avail_name_data;
                },
                /**
                 * [getSupplements description]
                 * @param  {[type]} key [description]
                 * @return {[type]}     [description]
                 */
                getSupplements: function ( key ) {
                    return this.getProduct( key ).sub_supplement_avail_name_data;
                },
                /**
                 * [getSupplement description]
                 * @param  {[type]} productKey [description]
                 * @param  {[type]} key        [description]
                 * @return {[type]}            [description]
                 */
                getSupplement: function ( productKey, key ) {
                    return this.getSupplements( productKey )[ key ];
                },
            }
            
            /*=====  End of Product Class  ======*/
            
            function init() {
                GRID_LIST.init();
                ITEM_SELECTION.init();
                MODALS.init();
                SHOPPING_CART.init();
            };
            init();
        })(jQuery);
    </script>