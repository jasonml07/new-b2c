<div class="fab-buttons-fixed visible-xs visible-sm">
    <button type="button" class="shopping-cart btn4 btn4-circle btn4-raised ripple-effect btn4-primary"><span class="fa fa-shopping-cart"></span></button>
</div>

<section class="booking-container">
        <div class="container">
            <div class="row">
                <!--=================================
                =            Page Header            =
                ==================================-->
                
                <div class="col-sm-12">
                    <div class="page-header">
                        <h1>ASSIGN GUEST/CUSTOMER INFORMATION</h1>
                    </div>
                </div>
                
                <!--====  End of Page Header  ====-->
                
                <!--==================================
                =            Page Content            =
                ===================================-->
                
                <div class="col-sm-12">
                    <div class="row">
                        <!--==========================
                        =            Form            =
                        ===========================-->
                        
                        <div class="col-md-8 content">

                            <form novalidate id="book-information">
                                <!--================================
                                =            Passengers            =
                                =================================-->
                                <div class="row passengers">
                                    <div class="col-xs-12">
                                        <div class="page-header heading">
                                            <h5>Passengers</h5>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 error-container sr-only" data-type="passenger">
                                        <div class="alert alert-danger">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <!-- <span class="fa fa-times-circle"></span> -->
                                                </div>
                                                <div class="media-body"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                    <?php foreach ( range(1, intval($_POST['passengers'])) as $key => $value ) :?>
                                        <div class="col-sm-6 passenger">
                                            <h6 class="text-center">Guest <?=$value ?></h6>
                                            <div class="row">
                                                <div class="col-sm-7 col-lg-6">
                                                    <div class="form-group">
                                                        <label for="passenger-first-name-[<?=$value?>]">First name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon select">
                                                                <select name="passengers[][title]" id="voucher-title" class="form-control" <?=($value == 1)?'data-bind="voucher[title]"':''?> data-type="">
                                                                    <option value=""><i>Title</i></option>
                                                                    <option value="Mr">Mr.</option>
                                                                    <option value="Ms">Ms.</option>
                                                                    <option value="Mrs">Mrs.</option>
                                                                </select>
                                                            </div>
                                                            <input type="text" id="passenger-first-name-[<?=$value?>]" name="passengers[][firstname]" class="form-control" data-type="name" <?=($value == 1)?'data-bind="voucher[firstname]"':''?>>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-lg-6">
                                                    <div class="form-group">
                                                        <label for="passenger-last-name-[<?=$value?>]">Last name</label>
                                                        <input type="text" id="passenger-last-name-[<?=$value?>]" name="passengers[][lastname]" class="form-control" data-type="name" <?=($value == 1)?'data-bind="voucher[lastname]"':''?>>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                                <!--====  End of Passengers  ====-->
                                
                                <!--=============================
                                =            Voucher            =
                                ==============================-->
                                
                                <div class="row voucher">
                                    <div class="col-xs-12">
                                        <div class="page-header heading">
                                            <h5>Voucher</h5>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 error-container sr-only" data-type="voucher">
                                        <div class="alert alert-danger">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <!-- <span class="fa fa-times-circle"></span> -->
                                                </div>
                                                <div class="media-body"></div>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="voucher-first-name">First name</label>
                                            <div class="input-group">
                                                <div class="input-group-addon select">
                                                    <select name="voucher[title]" id="voucher-title" class="form-control">
                                                        <option value=""><i>Title</i></option>
                                                        <option value="Mr">Mr.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                    </select>
                                                </div>
                                                <input type="text" id="voucher-first-name" class="form-control" name="voucher[firstname]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="voucher-last-name">Last name</label>
                                            <input type="text" id="voucher-last-name" class="form-control" name="voucher[lastname]">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="voucher-email-address">Email address</label>
                                            <input type="text" id="voucher-email-address" class="form-control" name="voucher[email]" data-type="email">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="voucher-phone-number">Mobile number</label>
                                            <input type="text" id="voucher-phone-number" class="form-control" name="voucher[phonenum]" data-type="phone">
                                        </div>
                                    </div>
                                </div>
                                <!--====  End of Voucher  ====-->
                                
                                <!--=========================================
                                =            Specia Requirements            =
                                ==========================================-->
                                
                                <div class="row special-requirements">
                                    <div class="col-xs-12">
                                        <div class="page-header heading">
                                            <h5>Special Requirements (Optional)</h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="special-requirements">Please write your requests in English</label>
                                            <textarea name="request" id="special-requirements" cols="30" rows="10" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <!--====  End of Specia Requirements  ====-->
                            </form>
                            
                            <!--=====================================
                            =            Payment Methods            =
                            ======================================-->
                            
                            <div class="row pament-methods">
                                <div class="col-xs-12">
                                    <div class="page-header heading">
                                        <h5>Payment Method</h5>
                                    </div>
                                </div>

                                <?php if ( $IS_AGENT ): ?>
                                <!--===================================
                                =            Agent Payment            =
                                ====================================-->
                                
                                <div class="col-xs-12 pay-later-container">
                                <?php if ( $remaining_days >= 30 ): ?>
                                    <br>
                                    <label for="pay-later" class="btn btn-primary btn-sm" title="Pay later">
                                        <input type="checkbox" class="sr-only" id="pay-later">
                                        <strong>Pay Later? <span class="fa fa-square-o"></span></strong>
                                    </label>
                                
                                    <?php foreach ($policies as $key => $policy): ?>
                                        <div class="bs-callout bs-callout-danger">
                                            <p><?=$policy?>.</p>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?>
                                </div>
                                
                                <!--====  End of Agent Payment  ====-->
                                <?php endif ?>

                                <div class="col-xs-12 payment-wrapper">
                                    <ul class="nav nav-tabs nav-justified" role="tablist">
                                        <li role="presentation" class="active"><a href="#payment-method-credit-card"  data-category="CC" arial-controls="payment-method-credit-card"  role="tab" data-toggle="tab">Credit Card  <span class="icon fa fa-check-circle"></span></a></li>
                                        <li role="presentation" style="display:none;"><a href="#payment-method-paypal"       data-category="PP" arial-controls="payment-method-paypal"       role="tab" data-toggle="tab">PayPal       <span class="icon fa fa-check-circle"></span></a></li>
                                        <li role="presentation"><a href="#payment-method-bank-deposit" data-category="BD" arial-controls="payment-method-bank-deposit" role="tab" data-toggle="tab">Bank Deposit <span class="icon fa fa-check-circle"></span></a></li>
                                        <?php if ( $IS_AGENT ): ?>
                                            <li role="presentation"><a href="#payment-method-e-nett"   data-category="EN" arial-controls="payment-method-e-nett"       role="tab" data-toggle="tab">E-Nett       <span class="icon fa fa-check-circle"></span></a></li>
                                        <?php endif ?>
                                    </ul>

                                    <div class="tab-content">
                                        <!--=================================
                                        =            Credit Card            =
                                        ==================================-->
                                        
                                        <div role="tabpanel" class="tab-pane active" id="payment-method-credit-card">

                                            <div class="col-xs-12 error-container sr-only" data-type="credit-card">
                                                <div class="alert alert-danger">
                                                    <div class="media">
                                                        <div class="media-left media-middle">
                                                            <!-- <span class="fa fa-times-circle"></span> -->
                                                        </div>
                                                        <div class="media-body"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-primary">
                                                <p class="text-justify">A 1.8% fee will apply for payments with MasterCard and VISA Credit and Debit cards and 3.8% for American Express and Diners. You can also select the Direct Deposit payment method to benefit from no processing fees.</p>
                                            </div>

                                            <form action="" id="payment-credit-card" novalidate="" method="POST">
                                                <input type="hidden" name="token" value="">
                                                <input type="hidden" name="biller_code" value="">
                                                <input type="hidden" name="action" value="">
                                                
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="payment-credit-card-name">Card holder name</label>
                                                        <input type="text" id="payment-credit-card-name" class="form-control" name="nm_card_holder" value="">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="payment-credit-card-number">Card number</label>
                                                        <input type="text" id="payment-credit-card-number" class="form-control" name="no_credit_card" value="">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="payment-credit-card-verification-number">Card verification number</label>
                                                        <input type="text" id="payment-credit-card-verification-number" class="form-control" name="no_cvn" value="">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="payment-credit-card-expiry-month">Expiry date</label>
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <select class="form-control" name="dt_expiry_month" id="payment-credit-card-expiry-month">
                                                                    <option value="">-- Month --</option>
                                                                    <?php for($m=1; $m<=12; ++$m): ?>
                                                                        <option value="<?=($m < 10)?'0' . $m: $m?>"><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                                                    <?php endfor; ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <select class="form-control" name="dt_expiry_year" id="payment-credit-card-expiry-year">
                                                                    <option value="">-- Year --</option>
                                                                    <?php $startDate = date('y', strtotime('now')); ?>
                                                                    <?php foreach (range(intval($startDate), intval($startDate) + 10) as $key => $value) : ?>
                                                                        <option value="<?=$value?>"><?=$value?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        
                                        <!--====  End of Credit Card  ====-->
                                        
                                        <!--============================
                                        =            PayPal            =
                                        =============================-->
                                        
                                        <div role="tabpanel" class="tab-pane" id="payment-method-paypal">
                                            <br><br>
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/2000px-PayPal.svg.png" alt="PayPal" class="img-responsive">
                                        </div>
                                        
                                        <!--====  End of PayPal  ====-->
                                        
                                        <!--==================================
                                        =            Bank Deposit            =
                                        ===================================-->
                                        
                                        <div role="tabpanel" class="tab-pane" id="payment-method-bank-deposit">
                                            <div class="bs-callout bs-callout-primary">
                                                <p class="text-justify">Payments can be made by bank transfer into the account below. Please fax our office (07 55262944) or e-mail us a copy of your payment. Pricing is subject to change until full payment has been made.</p>
                                            </div>

                                            <div class="form-horizontal">
                                                <div class="form-group" style="display:none;">
                                                    <label for="" class="control-label col-sm-3">Name</label>
                                                    <div class="col-sm-9">
                                                        <p class="form-control-static">Select World Travel Westpac BSB 034-216</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="control-label col-sm-3">ACCN</label>
                                                    <div class="col-sm-9">
                                                        <p class="form-control-static">462 080 </p>
                                                    </div>
                                                </div>
						<div class="form-group">
                                                    <label for="" class="control-label col-sm-3">BSB</label>
                                                    <div class="col-sm-9">
                                                        <p class="form-control-static">034 215 </p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="control-label col-sm-3">SWIFT</label>
                                                    <div class="col-sm-9">
                                                        <p class="form-control-static">WPACAU2S</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!--====  End of Bank Deposit  ====-->

                                        <?php if ( $IS_AGENT ): ?>
                                        <!--============================
                                        =            E-Nett            =
                                        =============================-->
                                        
                                        <div role="tabpanel" class="tab-pane" id="payment-method-e-nett">

                                            <div class="col-xs-12 error-container sr-only" data-type="e-nett">
                                                <div class="alert alert-danger">
                                                    <div class="media">
                                                        <div class="media-left media-middle">
                                                            <!-- <span class="fa fa-times-circle"></span> -->
                                                        </div>
                                                        <div class="media-body"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="alert alert-danger e-nett-payment-errors"></div>
                                            <form id="e-nett-payment" method="POST" class="form-inline" role="form" novalidate>
                                                <br>
                                                <!-- <div class="form-group col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4"> -->
                                                    <label for="date">Date</label>
                                                    <input type="text" class="form-control text-center" name="date" placeholder="Receipt date" value="<?=date('d/m/Y', strtotime('now'))?>" readonly>
                                                </div>
                                                <div class="form-group col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4">
                                                    <label for="recreipt-ref">Reference</label>
                                                    <input type="text" class="form-control text-center" name="reference" placeholder="" value="<?=unique_transaction_id()?>" readonly>
                                                </div>
                                                <div class="form-group col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4">
                                                    <label for="recreipt-amount" class="text-center">Amount</label>
                                                    <input type="text" class="form-control text-center" name="amount" placeholder="" data-original="<?=$FINAL_PRICE?>" value="<?=$FINAL_PRICE?>" readonly>
                                                </div>
                                            </form>
                                        </div>
                                        
                                        <!--====  End of E-Nett  ====-->
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            
                            <!--====  End of Payment Methods  ====-->
                            
                            <hr>
                            <div class="checkbox cancellation-container">
                                <label for="">
                                    <input type="checkbox" id="policy"> <strong>I have read the <a target="_blank" href="http://easterneurotours.com.au/terms-and-conditions">terms and conditions</a></strong>
                                </label>
                            </div>
                            <hr>

                            <div class="row hidden-xs hidden-sm">
                                <div class="col-xs-12 text-right">
                                    <button type="button" data-action="submit-button" class="btn btn-primary btn-lg"><strong>Book Now</strong></button>
                                </div>
                            </div>
                        </div>
                        
                        <!--====  End of Form  ====-->
                        









                        <!--=============================
                        =            Sidebar            =
                        ==============================-->
                        
                        <div class="col-md-4 side-bar">
                            <div class="side-bar-content theme-bg">
                                <div class="alert alert-theme contact-info hidden-xs hidden-sm">
                                    <div class="media sidebar-contact">
                                        <div class="media-left media-middle">
                                            <span class="awe-icon circle awe-icon-phone"></span>
                                        </div>
                                        <div class="media-body">
                                            <h6>Need Assistance? Call</h6>
                                            <h5><?=$agency['number']?></h5>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="alert alert-theme booking-info">
                                    <div class="media sidebar-contact visible-xs visible-sm">
                                        <div class="media-left media-middle">
                                            <span class="awe-icon circle awe-icon-phone"></span>
                                        </div>
                                        <div class="media-body">
                                            <h6>Need Assistance? Call</h6>
                                            <h5><?=$agency['number']?></h5>
                                        </div>
                                    </div>

                                    <div class="page-header text-center">
                                        <h4>Booking Info</h4>
                                    </div>
                                    <?php $tours = $_POST['tour'] ?>
                                    <div class="form-horizontal tour-info">
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-4">Tour starts</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static"><strong class="hidden-sm">: </strong><?=date('d M Y, l', strtotime($tours['start']))?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-4">Tour ends</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static"><strong class="hidden-sm">: </strong><?=date('d M Y, l', strtotime($tours['end']))?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-4">Duration</label>
                                            <div class="col-sm-8">
                                                <p class="form-control-static"><strong class="hidden-sm">: </strong><?=$tours['duration']?> days</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <form action="" id="paymentForm-PP" method="POST" class="products">
                                    <input type="hidden" name="discount" value="<?=$costing['discount_amount']?>">
                                    
                                    <?php if (isset( $_POST['items'] )): ?>
                                        <?php $items       = $_POST['items'] ?>
                                        <?php $products    = @$items['products'] ?>
                                        <?php $packages    = @$items['packages'] ?>
                                        <?php $extrabills  = @$items['extrabills'] ?>

                                        <?php if ( ! empty( $SUPPLEMENTS ) ): ?>
                                            <?php foreach ($SUPPLEMENTS as $key => $supplement): ?>
                                                <input type="hidden" name="SUPPLIMENTS[<?=$key?>][name]"     value="Supplement: <?=$supplement['name']?> X<?=$supplement['quantity']?>">
                                                <input type="hidden" name="SUPPLIMENTS[<?=$key?>][quantity]" value="<?=$supplement['quantity']?>">
                                                <input type="hidden" name="SUPPLIMENTS[<?=$key?>][price]"    value="<?=$supplement['price']?>">
                                            <?php endforeach ?>
                                        <?php endif ?>

                                        <?php if ( !!$products ): ?>
                                            <div class="row products">
                                                <div class="col-sm-12">
                                                    <div class="page-header"><h6>PRODUCTS</h6></div>
                                                    <div class="form-horizontal no-margin">
                                                    <?php foreach ($products as $key => $product): ?>
                                                        <input type="hidden" name="PRODUCTS[<?=$key?>][name]"     value="<?=$product['name']?> X<?=$product['quantity']?>">
                                                        <input type="hidden" name="PRODUCTS[<?=$key?>][quantity]" value="<?=$product['quantity']?>">
                                                        <input type="hidden" name="PRODUCTS[<?=$key?>][price]"    value="<?=$product['price']?>">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-sm-7">
                                                                <?=$product['name']?> X<?=$product['quantity']?>
                                                                <?php if ( isset( $product['supplement'] ) ): ?>
                                                                    <?php $supplement = $product['supplement'] ?>
                                                                    <br>
                                                                    Supplement:<br>
                                                                    <?=$supplement['name'] ?> X<?=$supplement['quantity'] ?> ( $<?=number_format($supplement['price'], 2) ?> )
                                                                <?php endif ?>
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <p class="form-control-static"><strong class="hidden-sm">: </strong><span class="fa fa-usd"></span><?=number_format($product['subtotal'], 2)?></p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif ?>

                                        <?php if ( !!$packages ): ?>
                                            <div class="row packages">
                                                <div class="col-sm-12">
                                                    <div class="page-header"><h6>PACKAGES</h6></div>
                                                    <div class="form-horizontal no-margin">
                                                    <?php foreach ($packages as $key => $package): ?>
                                                        <input type="hidden" name="PACKAGES[<?=$key?>][name]"     value="<?=$package['name']?> X<?=$package['quantity']?>">
                                                        <input type="hidden" name="PACKAGES[<?=$key?>][quantity]" value="<?=$package['quantity']?>">
                                                        <input type="hidden" name="PACKAGES[<?=$key?>][price]"    value="<?=$package['price']?>">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-sm-7"><?=$package['name']?> X<?=$package['quantity']?></label>
                                                            <div class="col-sm-5">
                                                                <p class="form-control-static"><strong class="hidden-sm">: </strong><span class="fa fa-usd"></span><?=number_format($package['subtotal'], 2)?></p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif ?>

                                        <?php if ( !!$extrabills ): ?>
                                            <div class="row extrabills">
                                                <div class="col-sm-12">
                                                    <div class="page-header"><h6>EXTRA BILLS</h6></div>
                                                    <div class="form-horizontal no-margin">
                                                    <?php foreach ($extrabills as $key => $extrabill): ?>
                                                        <input type="hidden" name="EXTRABILLS[<?=$key?>][name]"     value="<?=$extrabill['name']?> X<?=$extrabill['quantity']?>">
                                                        <input type="hidden" name="EXTRABILLS[<?=$key?>][quantity]" value="<?=$extrabill['quantity']?>">
                                                        <input type="hidden" name="EXTRABILLS[<?=$key?>][price]"    value="<?=$extrabill['price']?>">
                                                        <div class="form-group">
                                                            <label for="" class="control-label col-sm-7"><?=$extrabill['name']?> X<?=$extrabill['quantity']?></label>
                                                            <div class="col-sm-5">
                                                                <p class="form-control-static"><strong class="hidden-sm">: </strong><span class="fa fa-usd"></span><?=number_format($extrabill['subtotal'], 2)?></p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    <?php endif ?>
                                    <?php if ( filter_var($_POST['hasFlight'], FILTER_VALIDATE_BOOLEAN) ): ?>
                                    <input type="hidden" name="DEPARTURE[0][name]"     value="Return Flight Price X<?=$_POST['passengers']?>">
                                    <input type="hidden" name="DEPARTURE[0][quantity]" value="<?=$_POST['passengers']?>">
                                    <input type="hidden" name="DEPARTURE[0][price]"    value="<?=(float)$FLIGHT['price'] / intval($FLIGHT['pax']) ?>">
                                    <?php endif ?>
                                    </form>
                                
                                    <p><strong>Number of Passengers: <?=$_POST['passengers']?></strong></p>
                                
                                    <?php if ( $IS_AGENT ): ?>
                                    <!--===============================
                                    =            For Agent            =
                                    ================================-->
                                        <div class="row agent-view">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="" class="control-label col-sm-8">Gross Amount</label>
                                                    <div class="col-sm-4">
                                                        <p class="form-control-static">$<?=number_format($costing['gross_amount'], 2)?></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="control-label col-sm-8">Commission Amount</label>
                                                    <div class="col-sm-4">
                                                        <p class="form-control-static">$<?=number_format($costing['commission'], 2)?></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="control-label col-sm-8">Nett Amount</label>
                                                    <div class="col-sm-4">
                                                        <p class="form-control-static">$<?=number_format($costing['due_amount'], 2)?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!--====  End of For Agent  ====-->
                                    <?php endif ?>
                                
                                    <?php if ( $IS_DISCOUNTED ): ?>
                                        <!--==============================
                                        =            Discount            =
                                        ===============================-->
                                        
                                        <p><strong><?=$discountMessage?></strong></p>
                                        
                                        <!--====  End of Discount  ====-->
                                    <?php endif ?>
                                
                                    <!--==================================
                                    =            Flight Price            =
                                    ===================================-->
                                    <?php if ( filter_var($_POST['hasFlight'], FILTER_VALIDATE_BOOLEAN) ): ?>
                                    <p><strong>RETURN FLIGHTS PRICE: <span id="departure-fee">
                                        <span class="fa fa-usd"></span><?=number_format((intval(str_replace(',', '', $_POST['flight']['price'])) / intval($_POST['flight']['pax'])), 2) ?> X <?=$_POST['flight']['pax'] ?> = <span class="fa fa-usd"></span><?=number_format($_POST['flight']['price'], 2) ?>
                                    </span></strong></p>
                                    <?php endif ?>
                                
                                    <!--====  End of Flight Price  ====-->
                                    
                                    <br>
                                </div>
                                
                                <div class="alert alert-theme total-booking">
                                    <small>Total for this booking</small>
                                    <?php if ( $IS_DISCOUNTED ): ?>
                                        <h4><del><span class="fa fa-usd"></span><?=number_format($_POST['prices']['before'], 2) ?></del></h4>
                                    <?php endif ?>
                                    <h2><span class="amount" ><span class="fa fa-usd"></span><?=number_format($_POST['prices'][($IS_DISCOUNTED?'after':'before')], 2);?></span></h5>

                                    <div class="row visible-xs visible-sm">
                                        <div class="col-xs-12 text-right">
                                            <button type="button" data-action="submit-button" class="btn btn-primary btn-lg btn-block"><strong>Book Now</strong></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--====  End of Sidebar  ====-->
                        
                    </div>
                </div>
                
                <!--====  End of Page Content  ====-->
                
            </div>
        </div>
    </section>
