




    </div>
    <!-- END / PAGE WRAP -->
        <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
            <footer class="custom-theme-footer">
                <?php TZ_custom_theme_footer( $this->nativesession->get('agencyRealCode') ) ?>
            </footer>
        <?php else: ?>
         <footer id="footer-page" class="divs" style="background-color: #333 !important;">
            <a name="contact"></a>
            <div class="container">
                <div class="row">

                    <!-- WIDGET -->
                    <div class="col-md-4">
                        <div class="widget widget_about_us">
                            <h3 style="color: #fff!important;">Contact Us</h3>
                            <div class="widget_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form>
                                                <h5 style="color:#EDEDED;">
                                                    <span class="fa fa-globe"></span>
                                                    RESERVATION Office
                                                </h5>
                                                <address>
                                                 
                                                    <br>
                                                    <?=$agency['address'];?>
                                                    <br>
                                                    <label> Phone:</label>
                                                    <?=$agency['number'];?>
                                                    <br>
                                                    <?php if ( ! empty( $agency['fax'] ) ): ?>
                                                        <label> Toll free:</label>
                                                        <?=$agency['fax'];?>
                                                        <br>
                                                    <?php endif ?>
                                                </address>
                                                <address>
                                                    </address>
                                                    <address>
                                                    <strong>Hours of Operation </strong>
                                                    <br>
                                                    Mon-Fri: 08.00 am – 17.30 pm
                                                    <br>
                                                    Sat: 24 hour Contact 0433 161 250
                                                    <br>
                                                    Sun: 24 hour Contact 0433 161 250
                                                    <br>
                                                </address>
                                            </form>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="well well-sm" style="background-color: #333;border:0px solid #FFF">
                                            
                                                <form accept-charset="utf-8" method="post" action="<?php echo base_url();?>php_mailer/sendMessage">
                                                    <input type="hidden" name="redirectLink" value="<?=uri_string() . '?' . $_SERVER['QUERY_STRING']?>">
                                                    <input type="hidden" name="to" value="<?=$agency['email']?>">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> Name</label>
                                                                <input id="name" class="form-control" type="text" required="required" placeholder="Enter name" style="border:1px solid #ddd; height: 45px;" name="name" >
                                                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Email</label>
                                                                <input id="email" class="form-control" type="text" required="required" placeholder="Enter email" style="border:1px solid #ddd; height: 45px;" name="email" >
                                                                <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Subject</label>
                                                                <input id="subject" class="form-control" type="text" required="required" placeholder="Enter subject" style="border:1px solid #ddd; height: 45px;" name="subject" >
                                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                               <label> Message</label>
                                                               <textarea id="message" class="form-control" placeholder="Message" required="required" cols="25" rows="9" style="border:1px solid #ddd;border-radius: 0px" name="message" value="<?php echo set_value('message'); ?>"></textarea>
                                                               <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="qna">What is 3 + 2?</label>
                                                                <input id="qna" class="form-control" type="text" required="required" placeholder="Answer" name="captcha" style="border:1px solid #ddd; height: 45px;">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <button id="btnContactUs" class="btn btn-primary" type="submit"> Send Message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / WIDGET -->
                </div>
                        <div class="widget widget_follow_us" style="text-align: center;">
                            <div class="widget_content">
                                <div class="awe-social">
                                    <?php $socials = _AGENCY_SOCIALS( $this->nativesession->get('agencyRealCode') ) ?>
                                    <?php if ( !!$socials ): ?>
                                        <?php foreach ( $socials as $social ): ?>
                                            <a href="<?=$social['link'] ?>"><i class="fa fa-<?=strtolower(str_replace(' ', '-', $social['title'])) ?>"></i></a>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                <div class="copyright">
                    <p>&copy;2018 <?=$agency['agency_name'] ?>&trade; All rights reserved.</p>
                </div>
            </div>
        </footer>
        <?php endif ?>







    <!-- LOAD JQUERY -->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.magnific-popup.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url();?>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/wow.min.js"></script>
    <!--load bootstrap.js-->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/moment.js"></script>
    <!-- load form validation -->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.validate.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.dataTables.min.js"></script>
    <!-- REVOLUTION DEMO -->
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/jquery.uploadify.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/waitMe.min.js"></script>

        
    <!-- ANGULARJS -->
    <script type="text/javascript" src="<?=base_url();?>static/js/angular.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/angular-resource.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/faker.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/dirPagination.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ui-bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.serialize-object.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.number.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap-dialog.min.js"></script>
    <?php TZ_includes($this->nativesession->get('agencyRealCode'),'scripts') ?>
    <script type="text/javascript">
        (function ($) {
            $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
            $.datepicker._updateDatepicker = function(inst) {
                $.datepicker._updateDatepicker_original(inst);
                var afterShow = this._get(inst, 'afterShow');
                if (afterShow)
                    afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
            }
        }(jQuery));
    </script>

    <script type="text/javascript">
        let DATE=null
        const BASE_URL = '<?=base_url()?>';
        const IS_AGENT = <?=$IS_AGENT ? 'true' : 'false' ?>;
        const PRODUCTS = <?=json_encode($product['productAvailabilityFinal']) ?>;
        const DEPARTURE_MONTH = <?=DEPARTURE_MONTH ?>;
        const PREVIOUS_LINK = '<?=uri_string() . '?' . $_SERVER['QUERY_STRING']?>';
        const CONVERTION_CURRENCY = '<?=CONVERTION_CURRENCY ?>';
        const PRODUCT = {
            id: '<?=$product['productId'];?>',
            name: "<?=$product['productName'];?>",
            phoneDetails: "<?=$product['productPhone'];?>",
            SumarryDescription: '<?=$product['productSumarryDescription'];?>',
            FromCountry: '<?=$product['productFromCountry'];?>',
        };
        const CUSTOM_SETTING=<?=json_encode(TZ_custom_agent_func($this->nativesession->get('agencyRealCode')))?>;
        <?php if(isset($_GET['date'])): ?>
            <?php $match='/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/' ?>
            <?php if(preg_match($match,$_GET['date'])>0) :?>
                DATE='<?=$_GET['date']?>'
            <?php endif; ?>
        <?php endif; ?>
    </script>
    <script type="text/javascript" src="<?=base_url()?>static/js/external-tours.js?<?=time()?>"></script>
    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <?php TZ_custom_theme_script( $this->nativesession->get('agencyRealCode') ) ?>
    <?php endif; ?>
    <script type="text/javascript">
        var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"3b7f5b3d2364120336dd891c7706fe1110fa720c9fd129af1795ba7dfee5b4938155ad889dc9bdc4a5d9a79cd083266b", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);
    </script>
</body>
</html>