<?php $agency = _AGENT_INFO( $this->nativesession->get('agencyRealCode') ) ?>
    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <footer class="custom-theme-footer">
            <?php TZ_custom_theme_footer( $this->nativesession->get('agencyRealCode') ) ?>
        </footer>
    <?php else: ?>
<!-- FOOTER PAGE -->
        <footer id="footer-page" class="divs">
            <a name="contact"></a>
            <div class="container">
                <div class="row">

                    <!-- WIDGET -->
                    <div class="col-md-4">
                        <div class="widget widget_about_us">
                            <h3 style="color: #fff!important;">Contact Us</h3>
                            <div class="widget_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form>
                                                <h5 style="color:#EDEDED;">
                                                    <span class="fa fa-globe"></span>
                                                    RESERVATION Office
                                                </h5>
                                                <address>
                                                 
                                                    <br>
                                                    <?=$agency['address'] ?>
                                                    <br>
                                                    <?=$agency['city'] ?>, <?=$agency['country'] ?>
                                                    <br>
                                                    <label> Phone:</label>
                                                    <?=$agency['phone'] ?>
                                                    <br>
                                                    <label> Toll free:</label>
                                                    <?=$agency['fax'] ?>
                                                    <br>
                                                </address>
                                                <address>
                                                    <strong>Email</strong>
                                                    <br>
                                                    <a href="mailto:#"><?=$agency['email'] ?></a>
                                                    </address>
                                                    <address>
                                                    <strong>Hours of Operation </strong>
                                                    <br>
                                                    Mon-Fri: 08.00 am – 17.30 pm
                                                    <br>
                                                    Sat: 24 hour Contact 0433 161 250
                                                    <br>
                                                    Sun: 24 hour Contact 0433 161 250
                                                    <br>
                                                </address>
                                            </form>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="well well-sm" style="background-color: #333;border:0px solid #FFF">
                                                <form accept-charset="utf-8" method="post" action="<?php echo base_url();?>php_mailer/sendMail ">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> Name</label>
                                                                <input id="name" class="form-control" type="text" required="required" placeholder="Enter name" style="border:1px solid #ddd; height: 45px;" name="name" value="<?php echo set_value('name'); ?>">
                                                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="email"> Email Address</label>
                                                                <input id="email" class="form-control" type="email" required="required" placeholder="Enter email" style="border:1px solid #ddd; height: 45px;" name="email" value="<?php echo set_value('email'); ?>">
                                                                <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="subject"> Subject</label>
                                                                <input id="subject" class="form-control" type="text" required="required" placeholder="Enter subject" name="subject" style="border:1px solid #ddd; height: 45px;" value="<?php echo set_value('subject'); ?>">
                                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="name"> Message</label>
                                                                <textarea id="message" class="form-control" placeholder="Message" required="required" cols="25" rows="9" style="border:1px solid #ddd;border-radius: 0px" name="message" value="<?php echo set_value('message'); ?>"></textarea>
                                                                <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="qna">What is 3 + 2?</label>
                                                                <input id="qna" class="form-control" type="text" required="required" placeholder="Answer" name="captcha" style="border:1px solid #ddd; height: 45px;">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <button id="btnContactUs" class="btn btn-primary" type="submit"> Send Message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / WIDGET -->
                </div>
                        <div class="widget widget_follow_us" style="text-align: center;">
                            <div class="widget_content">
                                <div class="awe-social">
                                    <?php $socials = _AGENCY_SOCIALS( $this->nativesession->get('agencyRealCode') ) ?>
                                    <?php if ( !!$socials ): ?>
                                        <?php foreach ( $socials as $social ): ?>
                                            <a href="<?=$social['link'] ?>"><i class="fa fa-<?=strtolower(str_replace(' ', '-', $social['title'])) ?>"></i></a>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                <div class="copyright">
                    <p>&copy;2015 <?=$agency['agency_name'] ?>&trade; All rights reserved.</p>
                </div>
            </div>
        </footer>
        <!-- END / FOOTER PAGE -->
    <?php endif ?>

    </div>
    <!-- END / PAGE WRAP -->

     <!-- LOAD JQUERY -->
  
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.magnific-popup.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url();?>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/wow.min.js"></script>

    <script type="text/javascript" src="<?=base_url();?>static/js/faker.min.js"></script>
    <!--load bootstrap.js-->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>

    <!-- load form validation -->
    <script type="text/javascript" src="<?=base_url()?>static/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/waitMe.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/jquery.serialize-object.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.number.min.js"></script>

    <script type="text/javascript">
        (function ( $ ) {
            var affix = $('.side-bar-content');
            var width = affix.width();
            affix.width(width);

            $('body').scrollspy({
                target: '.sticky-side-bar',
                offset: 0
            });
            $( '.side-bar-content' ).affix({
                offset: {
                    top: $( '.side-bar-content' ).offset().top,
                    bottom: function ( ) {
                        return (this.bottom = ($( 'footer' ).outerHeight(true) + 20));
                    }
                }
            });
        })(jQuery)
    </script>
    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <?php TZ_custom_theme_script( $this->nativesession->get('agencyRealCode') ) ?>
    <?php endif; ?>
    <?php $CUSTOM_SETTING=TZ_custom_agent_func($this->nativesession->get('agencyRealCode')); ?>
    <?php if (isset($CUSTOM_SETTING['CHECKOUT_SUCCESS_ON_LOAD'])) :?>
        <?php $custom_on_load_data=$CUSTOM_SETTING['CHECKOUT_SUCCESS_ON_LOAD']; ?>
        <script type="text/javascript">
            (function($){
                setTimeout(function(){
                    BootstrapDialog.show({
                        type:BootstrapDialog.TYPE_SUCCESS
                        ,closable:false
                        ,title:'<?=$custom_on_load_data['title']?>'
                        ,message: '<?=$custom_on_load_data['message']?>'
                        ,buttons:[{
                            label:'Close'
                            ,action:function(dialog){
                                dialog.close();
                            }
                        }]
                    });
                },1000);
            })(jQuery);
        </script>
    <?php endif; ?>
    </body>
</html>