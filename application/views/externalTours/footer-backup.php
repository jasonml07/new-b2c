




    </div>
    <!-- END / PAGE WRAP -->
        <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
            <footer class="custom-theme-footer">
                <?php TZ_custom_theme_footer( $this->nativesession->get('agencyRealCode') ) ?>
            </footer>
        <?php else: ?>
         <footer id="footer-page" class="divs" style="background-color: #333 !important;">
            <a name="contact"></a>
            <div class="container">
                <div class="row">

                    <!-- WIDGET -->
                    <div class="col-md-4">
                        <div class="widget widget_about_us">
                            <h3 style="color: #fff!important;">Contact Us</h3>
                            <div class="widget_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form>
                                                <h5 style="color:#EDEDED;">
                                                    <span class="fa fa-globe"></span>
                                                    RESERVATION Office
                                                </h5>
                                                <address>
                                                 
                                                    <br>
                                                    <?=$agency['address'];?>
                                                    <br>
                                                    <label> Phone:</label>
                                                    <?=$agency['number'];?>
                                                    <br>
                                                    <?php if ( ! empty( $agency['fax'] ) ): ?>
                                                        <label> Fax:</label>
                                                        <?=$agency['fax'];?>
                                                        <br>
                                                    <?php endif ?>
                                                    <label> Toll Free:</label>
                                                    1800 242 353
                                                    <br>
                                                </address>
                                                <address>
                                                    </address>
                                                    <address>
                                                    <strong>Hours of Operation </strong>
                                                    <br>
                                                    Mon-Fri: 08.00 am – 17.30 pm
                                                    <br>
                                                    Sat: 24 hour Contact 0433 161 250
                                                    <br>
                                                    Sun: 24 hour Contact 0433 161 250
                                                    <br>
                                                </address>
                                            </form>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="well well-sm" style="background-color: #333;border:0px solid #FFF">
                                            
                                                <form accept-charset="utf-8" method="post" action="<?php echo base_url();?>php_mailer/sendMessage">
                                                    <input type="hidden" name="redirectLink" value="<?=uri_string() . '?' . $_SERVER['QUERY_STRING']?>">
                                                    <input type="hidden" name="to" value="<?=$agency['email']?>">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> Name</label>
                                                                <input id="name" class="form-control" type="text" required="required" placeholder="Enter name" style="border:1px solid #ddd; height: 45px;" name="name" >
                                                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Email</label>
                                                                <input id="email" class="form-control" type="text" required="required" placeholder="Enter email" style="border:1px solid #ddd; height: 45px;" name="email" >
                                                                <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label> Subject</label>
                                                                <input id="subject" class="form-control" type="text" required="required" placeholder="Enter subject" style="border:1px solid #ddd; height: 45px;" name="subject" >
                                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                               <label> Message</label>
                                                               <textarea id="message" class="form-control" placeholder="Message" required="required" cols="25" rows="9" style="border:1px solid #ddd;border-radius: 0px" name="message" value="<?php echo set_value('message'); ?>"></textarea>
                                                               <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="qna">What is 3 + 2?</label>
                                                                <input id="qna" class="form-control" type="text" required="required" placeholder="Answer" name="captcha" style="border:1px solid #ddd; height: 45px;">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <button id="btnContactUs" class="btn btn-primary" type="submit"> Send Message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / WIDGET -->
                </div>
                        <div class="widget widget_follow_us" style="text-align: center;">
                            <div class="widget_content">
                                <div class="awe-social">
                                    <?php $socials = _AGENCY_SOCIALS( $this->nativesession->get('agencyRealCode') ) ?>
                                    <?php if ( !!$socials ): ?>
                                        <?php foreach ( $socials as $social ): ?>
                                            <a href="<?=$social['link'] ?>"><i class="fa fa-<?=strtolower(str_replace(' ', '-', $social['title'])) ?>"></i></a>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                <div class="copyright">
                    <p>&copy;2015 <?=$agency['agency_name'] ?>&trade; All rights reserved.</p>
                </div>
            </div>
        </footer>
        <?php endif ?>







    <!-- LOAD JQUERY -->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.magnific-popup.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url();?>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/wow.min.js"></script>
    <!--load bootstrap.js-->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/moment.js"></script>
    <!-- load form validation -->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.validate.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.dataTables.min.js"></script>
    <!-- REVOLUTION DEMO -->
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/jquery.uploadify.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/waitMe.min.js"></script>

        
    <!-- ANGULARJS -->
    <script type="text/javascript" src="<?=base_url();?>static/js/angular.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/angular-resource.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/faker.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/dirPagination.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ui-bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.serialize-object.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>static/js/jquery.number.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>static/js/bootstrap-dialog.min.js"></script>

    
     <script type="text/javascript">
     var jArray = <?php echo json_encode($product['productAvailabilityFinal']); ?>;
     // console.log(jArray);

         jQuery.noConflict();
            (function($){
                $(".tabs").tabs({
                    active: 0,
                    show: {
                        effect: "fade",
                        duration: 200
                    },
                    hide: {
                        effect: "fade",
                        duration: 200
                    }
                }); 

                var cellContents = [];

                    <?php foreach($product['productAvailabilityFinal'] as $key => $val):?>
                    cellContents.push({
                    fromMonth   : <?=$val['fromMonth']; ?>,
                    fromText    : "<?=$val['fromDayText']; ?>",
                    fromDay     : <?=$val['fromDay']; ?>,
                    fromYear    : <?=$val['fromYear']; ?>,
                    toMonth     : <?=$val['toMonth']; ?>,
                    toText      : "<?=$val['toDayText']; ?>",
                    toDay       : <?=$val['toDay']; ?>,
                    toYear      : <?=$val['toYear']; ?>,
                    displayPrice: <?=$val['displayPrice'] ;?>
                    });

                    $( "a.list-content-<?=$val['fromMonth']; ?><?=$val['fromDay']; ?><?=$val['fromYear']; ?>" ).click(function() {
                        /*$( 'div.pax' ).attr('style', 'margin-top: 30px; visibility: visible');
                        $('div.add-to-cart button').attr('style', 'font-weight: 900; font-size: 1.2em;');
                        $('div.product-content').attr('style', 'visibility: visible; display: block; border-top: 3px solid #e4e4e4; margin-top: 30px; background-color: #e6e6e6; padding-bottom: 30px;  border-bottom-left-radius: 2em; border-bottom-left-radius: 2em;');
                        updateSetDate();
                        updateDatePickerCells();*/
                        var date = '<?=$val['fromMonth']; ?>/<?=$val['fromDay']; ?>/<?=$val['fromYear']; ?>';
                        
                        if( (typeof $('div.product-content').data('date') == 'undefined') || ($('div.product-content').data('date') != date) )
                        {
                            $( 'div.pax' ).attr('style', 'margin-top: 30px; visibility: visible');
                            // $('div.add-to-cart button').attr('style', 'font-weight: 900; font-size: 1.2em; border-radius: 5px; background-color: #009688 !important;');
                            $('div.product-content').attr('style', 'visibility: visible; display: block; border-top: 3px solid #ccc; margin-top: 30px; background-color: #f2f2f2; padding-bottom: 30px; border-bottom-left-radius: 0px;');
                            $('div.product-content').data('date', date);
                            updateDatePickerCells();
                            updateSetDate(date);
                            var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                            updateBookingTotal( data );
                            jQuery( 'div.promo' ).show();
                            init();

                            return;
                        } else {
                            scrollToItem();
                        }
                        // #f5f5f5
                    });
                    <?php endforeach; ?>
                
                var getDatesAvailable = [];
                var datePrice = [];
                for(var x=0;x<cellContents.length;x++){
                    var classTemp = parseInt(cellContents[x].fromDay)+"-"+parseInt(cellContents[x].fromMonth)+"-"+cellContents[x].fromYear;
                    var TempEndDate = parseInt(cellContents[x].toDay)+"-"+parseInt(cellContents[x].toMonth)+"-"+cellContents[x].toYear;
                    getDatesAvailable.push(classTemp);
                    datePrice[classTemp] = {
                        displayPrice:cellContents[x].displayPrice,
                        endDate: TempEndDate,
                        details: cellContents[x]
                        };
                }   
                //console.log(getDatesAvailable);
                function availableDates(date){

                    var dmy = date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear();
                    if($.inArray(dmy,getDatesAvailable) == -1){

                        $('head').append('<style> .content-' + dmy + '>span {border:0 !important;font-size: 8px;width: 46px;height: 36px;line-height:5px;color: #ccc !important;} .content-' + dmy + '{border: 1px solid #ccc !important; }</style>');
                        return [false,'content-'+dmy];

                    }else{
                        //var strText = '<div style="z-index:9999;position:absolute;font-size:10px;font-weight:bold;margin-top:-20px">$'+parseInt(datePrice[dmy].displayPrice)+'</div>';
                        var rule = '.content-' + dmy + '>div:before {content: "$' + parseInt(datePrice[dmy].displayPrice) + '";}.content-' + dmy + '{cursor: pointer; border: 1px solid #ccc !important; }';
                        $('head').append('<style>' + rule + '</style>');
                        
                        return [true,'content-'+dmy];
                    }
                }

                function updateDatePickerCells(dp) {
                    setTimeout(function () {
                        $('#calendarHolder .ui-datepicker tbody tr td > *').each(function (idx, elem) {
                            console.log(  );
                            if ( $( elem ).prop('tagName') == 'A' )
                            {
                                $( elem ).css({position: 'relative', 'paddingRight': '10px', 'lineHeight': '16px', top: '-3px'});
                                $( elem ).attr('style', $( elem ).attr('style') + '; text-align: right !important' );

                                $( "<div style=\"z-index:10; position:absolute;font-size:11px;font-weight:bold;margin:-23px 0 2px 2px;color:#f44336;\"></div>" ).insertAfter(elem);
                                $( "<span style=\"z-index:10;padding: 0;  position:absolute;margin:-35px 0 2px 1px;width: 0;height:0;border-style: solid;border-width: 12px 12px 0 0;border-color: #f44336 transparent transparent transparent;\"></span>" ).insertAfter(elem);
                            }
                            

                            //width: 0;height: 0;border-style: solid;border-width: 12px 12px 0 0;border-color: #007bff transparent transparent transparent;
                            //$(this).insertAfter('<div style="z-index:9999;position:absolute;font-size:10px;font-weight:bold;margin-top:-20px"></div>');
                        });
                    }, 1);
                }

                var globalSelect = "";
                function updateSetDate(date){

                    <?php foreach ($product['productAvailabilityFinal'] as $singleToursPrice) : ?>
                    <?php if ($singleToursPrice['isMultiPrice'] == 0): ?>
                    <?php else: ?>
                        // document.getElementById("side-t-single-price-tours").innerHTML     = '';
                    <?php endif; ?>
                    <?php endforeach; ?>

                    <?php foreach ($product['productAvailabilityFinal'] as $productAvailabilityFinal) : ?>
                    <?php if (empty($productAvailabilityFinal['product_supplements'])): ?>
                    <?php else: ?>
                        // document.getElementById("side-t-supplement").innerHTML             = '';
                        // document.getElementById("side-t-supplement_name3").innerHTML       = '';
                    <?php endif; ?>
                    <?php endforeach; ?>

                   // document.getElementById("side-t-multiple_supplements").innerHTML   = '';
                   // document.getElementById("side-t-supplement_name2").innerHTML       = '';
            
                   // document.getElementById("side-t-sub_product_price_list").innerHTML = '';
                   // document.getElementById("side-t-supplement_name1").innerHTML       = '';

                    var dateArr = date.split('/');

                    var d1     = new Date(date);
                    var dateTO = datePrice[parseInt(dateArr[1])+"-"+parseInt(dateArr[0])+"-"+parseInt(dateArr[2])].endDate;

                    var det       = datePrice[parseInt(dateArr[1])+"-"+parseInt(dateArr[0])+"-"+parseInt(dateArr[2])].details;
                    var tempPrice = datePrice[parseInt(dateArr[1])+"-"+parseInt(dateArr[0])+"-"+parseInt(dateArr[2])].displayPrice;
                    var dateArr2  = dateTO.split('-');
                    
                    var d2 = new Date(parseInt(dateArr2[1])+'/'+(parseInt(dateArr2[0]))+'/'+parseInt(dateArr2[2]));

                    var timeDiff = Math.abs(d2.getTime() - d1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                    if(globalSelect != ""){
                        var dTempArr = globalSelect.split('|');
                        for(var q=0;q<dTempArr.length;q++){
                            //console.log(dTempArr[q]);
                            $('head').append('<style>.content-' + dTempArr[q] + '{background: none !important;}</style>');
                        }
                        
                    }

                    var totalPrice = parseFloat(tempPrice)*diffDays;
                    for(var x=0;x<=diffDays;x++){
                        d1.setDate(d1.getDate()+x);
                        var dmy = d1.getDate()+"-"+(d1.getMonth()+1)+"-"+d1.getFullYear();
                        globalSelect += dmy+"|";
                        d1.setDate(d1.getDate()-x);
                        $('head').append('<style>.content-' + dmy + '{background: rgba(230, 230, 230, 0.76) !important;}</style>');
                    }

                    ///////////////////////////////////////////////////////////////////////////////////

                    var newDate       = new Date(date);
                    var newDateString = newDate.getMonth() + '/' + newDate.getDate() + '/' + newDate.getFullYear();

                    jQuery( 'div.extra-bill' ).hide();

                    for (var iX = 0; iX < jArray.length; iX++) {

                        var newdate1       = new Date(jArray[iX].fromMonth + '/' + jArray[iX].fromDay + '/' + jArray[iX].fromYear);
                        var newdate1String = newdate1.getMonth() + '/' + newdate1.getDate() + '/' + newdate1.getFullYear();

                        if(newDateString == newdate1String){
                            // /console.log(newDateString + ' = ' + newdate1String);
                            if(jArray[iX].isMultiPrice == 1){

                                var product          = jArray[iX].supplement_avail_name_data;
                                var suppData2        = jArray[iX].sub_supplement_avail_name_data;
                                var suppData3        = jArray[iX].product_supplements;
                                var supplementsTotal = 0;
                                var supliments       = jArray[iX].sub_supplement_avail_name_data;
                                var addons           = jArray[iX].product_supplements;


                                //////////////////////////CHILD COMBOBOX///////////////////////////////////////////////////////////////
                                var $summary         = $( 'div.book-summary' );
                                var $packagesWrapper = $( 'div.book-package' );
                                var isAppendedFirst  = false;

                                $summary.show();
                                $packagesWrapper.find('div.non-static').empty();
                                $packagesWrapper.find('div.non-static').empty();
                                $( '#side-t-supplement_name1' ).empty();
                                $( 'div.extra-bill div.group' ).empty();

                                $.each(addons, function ( key, item ) {
                                    //item['supplement_avail_name'] + ': $' + 
                                    var $quantity = $( '<input type="number" class="addon-quantity" id="addon-quantity" value="0"/>' )
                                    var $inpoy1   = $( '<input type="checkbox" id="sub_product_price_list-' + (( item.minguest == 0 )? key: '') + '" name="sub_product_price_list"/>' )

                                    $quantity.data('price'          , item.grossPrice)
                                    $quantity.data('commission'     , item.comPrice);
                                    $quantity.data('netAmount'      , item.netAmount);
                                    $quantity.data('name'           , item.supplement_name);
                                    $quantity.data('included'       , ( item.minguest == 1 ));
                                    $quantity.data('previousValue'  , 0);
                                    $quantity.data('destroyPopOver' , true);
                                    $quantity.prop('disabled'       , ( item.minguest == 1 ));
                                    $quantity.css('width'           , '40px')
                                    $quantity.attr('data-toggle'    , 'popover');
                                    $quantity.attr('data-placement' , 'top');
                                    $quantity.attr('min'            , '0');


                                    $inpoy1.data('price'     , (item.isMarkup?item.grossPrice:item.convertedBasePrice));
                                    $inpoy1.data('commission', item.comPrice);
                                    $inpoy1.data('netAmount' , item.netAmount);
                                    $inpoy1.data('name'      , item.supplement_name);
                                    $inpoy1.data('included'  , ( item.minguest == 1 ));
                                    $inpoy1.prop('disabled'  , ( item.minguest == 1 ));
                                    $inpoy1.attr('value'     , (item.isMarkup?item.grossPrice:item.convertedBasePrice))
                                    // (( item.minguest == 1 )? 'visibility: hidden': '')
                                    if( item.minguest == 1 )
                                    {
                                        $inpoy1.addClass('sr-only');
                                    }

                                    // .appendTo('#side-t-supplement_name1');
                                    if( item.minguest == 0 )
                                    {
                                        $quantity.change(function() {
                                            var $this      = $( this );
                                            var quantity   = parseInt($this.val());
                                            var data       = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                                            var totalGuest = data.selectedGuests;

                                            if( $this.data('destroyPopOver') )
                                            {
                                                $( '.popover' ).remove();
                                            }
                                            if( quantity < 0 )
                                            {
                                                $this.val('0');
                                                $this.change();
                                                return false;
                                            }

                                            if ( getAddonsQuantities() <= totalGuest )
                                            {
                                                $this.data('previousValue', quantity);
                                                
                                                if( quantity > 0 )
                                                {
                                                    // ADD
                                                    // TODO
                                                    // Check the accumodated quantity based on the selected pax
                                                    if ( $packagesWrapper.find('div.static div[data-package-key="' + key + '"]').length )
                                                    {
                                                        $packagesWrapper.find('div.static div[data-package-key="' + key + '"]').find('input[name="packages[' + key + '][quantity]"]').val( quantity )
                                                    } else {
                                                        $packagesWrapper.find('div.static').append( $( '<div data-package-key="' + key + '">'
                                                            +    '<input type="hidden" name="packages[' + key + '][quantity]"           value="' + quantity + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][name]"               value="' + item.supplement_name + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][OriginalBasePrice]"  value="' + item.OriginalBasePrice + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][fromCurrency]"       value="' + item.fromCurrency + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][toCurrency]"         value="' + item.toCurrency + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][convertedRate]"      value="' + item.convertedRate + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][comPrice]"           value="' + item.comPrice + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][comm_pct_amount]"    value="' + item.comm_pct_amount + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][grossPrice]"         value="' + item.grossPrice + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][markup_pct_amount]"  value="' + item.markup_pct_amount + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][netAmount]"          value="' + item.netAmount + '"/>'
                                                            +    '<input type="hidden" name="packages[' + key + '][convertedBasePrice]" value="' + item.convertedBasePrice + '"/>'
                                                            +'</div>' ) );
                                                    }
                                                    
                                                } else {
                                                    // REMOVE

                                                    $packagesWrapper.find('div.static div[data-package-key="' + key + '"]').remove()
                                                }
                                                updateBookingTotal( getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') ) );
                                            } else {
                                                if( getAddonsQuantities() > totalGuest || ! totalGuest )
                                                {
                                                    $this.data('destroyPopOver', true);
                                                    // TODO
                                                    // CHECK THE PREVIOUS SELECTION
                                                    if ( getAddonsQuantities() > totalGuest && ! totalGuest )
                                                    {
                                                        $this.data('previousValue', $this.data('previousValue') - 1);
                                                        $this.data('destroyPopOver', false );
                                                    }

                                                    $this.attr('data-content', 'Please select products before assigning package.');
                                                    $( this ).popover('show').on('shown.bs.popover', function () {
                                                        $( 'div#' + $this.attr('aria-describedby') ).addClass('alert-danger');
                                                    });
                                                    popoverOnClick();
                                                }

                                                $this.val( $this.data('previousValue') + '' );
                                                $this.change();
                                            }
                                        });

                                        $_wrapper = $( '<div class="col-md-12"></div>' );
                                        $_label   = $( '<label />', {
                                            text : item['supplement_name'] + ': $' +   $.number( Math.round( item['grossPrice'] ), 2 ),
                                            style: 'color: #777777;',
                                            for  : 'sub_product_price_list-' + key
                                        } );
                                        // style: 'color: #777777; width: 290px;',

                                        // $_wrapper.append( $inpoy1 );
                                        $_wrapper.append( $quantity )
                                        $_wrapper.append( $( '<span>&nbsp;</span>' ) );
                                        $_wrapper.append( $_label );
                                        $_wrapper.append( $('<br />', {}) );
                                        $_wrapper.appendTo( '#side-t-supplement_name1' );
                                    } else {
                                        $packagesWrapper.find('div.non-static').append( $( '<div data-package-key="' + key + '">'
                                            +    '<input type="hidden" name="extraBills[' + key + '][isMarkedUp]"         value="' + item.isMarkup + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][name]"               value="' + item.supplement_name + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][OriginalBasePrice]"  value="' + item.OriginalBasePrice + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][fromCurrency]"       value="' + item.fromCurrency + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][toCurrency]"         value="' + item.toCurrency + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][convertedRate]"      value="' + item.convertedRate + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][comPrice]"           value="' + item.comPrice + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][comm_pct_amount]"    value="' + item.comm_pct_amount + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][grossPrice]"         value="' + item.grossPrice + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][markup_pct_amount]"  value="' + item.markup_pct_amount + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][netAmount]"          value="' + item.netAmount + '"/>'
                                            +    '<input type="hidden" name="extraBills[' + key + '][convertedBasePrice]" value="' + item.convertedBasePrice + '"/>'
                                            +'</div>' ) );

                                        $_wrapper = $( '<div class="col-md-12"></div>' );
                                        $_label   = $( '<label />', {
                                            text : item['supplement_name'] + ': $' +   $.number( Math.round( $inpoy1.data('price') ), 2 ),
                                            style: 'color: #777777; width: 290px;',
                                            for  : 'sub_product_price_list-' + key
                                        } );
                                        $_wrapper.append( $( '<span>&nbsp;</span>' ) );
                                        $_wrapper.append( $_label );
                                        $_wrapper.append( $('<br />', {}) );
                                        $_wrapper.append( $inpoy1 )

                                        // $inpoy1.appendTo( '#side-t-supplement_name1' );
                                        $( 'div.extra-bill' ).show();
                                        $( 'div.extra-bill div.group' ).append( $_wrapper );

                                        $summary.find('div.summary-extra-bills div.wrapper').append( ''
                                            +'<div class="col-md-8 info">' + item.supplement_name + ' <span class="quantity" data-quantity="1">X1</span></div>'
                                            +'<div class="col-md-4 total" data-price="' + item.grossPrice + '">$' + $.number( truncate( item.grossPrice, 2 ), 2 ) + '</div>'
                                        );
                                    }

                                });
                                if( addons.length )
                                {
                                    $('#side-t-sub_product_price_list').html('<b >Add Ons/Packages:</b>');
                                }
                                generateSuppliments( product, supliments );

                                assingAnalyzation ( jArray[iX].analyzation );
                                //////////////////////////RADIO BUTTON///////////////////////////////////////////////////////////////

                                    var firstradiovalue     = $('input:radio[name=sub_product_price_list]:first').prop('checked', true).val();

                                    var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                                    if( data.guests == 0 ) { data.guests = 1; }
                                    var TotalFirstValue     = data.guests * parseFloat(firstradiovalue);

                                    var supplementsTotalAll = supplementsTotal+TotalFirstValue + data.prices;


                                    document.getElementById("tour-price").innerHTML = "$"+truncate(supplementsTotalAll,2);
                                    $('#tourTotalPrice').val("$"+truncate(supplementsTotalAll, 2));
                                    
                                    $('#startdate').val( det.fromYear + '-' + det.fromMonth + '-' + det.fromDay );
                                    $('#startend').val( det.toYear + '-' + det.toMonth + '-' + det.toDay );
                                    $('#dateduration').val((diffDays+1)+' days');


                                    /*if(jArray[iX].sub_supplement_avail_name_data.length > 0){
                                        $('#side-t-multiple_supplements').html('<b>Add On:</b>');
                                    }*/

                                    /*if(typeof jArray[iX].product_supplements == 'object'){
                                        $('#side-t-supplement').html('<b >Supplements:</b>');
                                    }*/

                                    $('#side-t-num').html((diffDays+1)+' days');
                                    $('#side-t-start').html(det.fromText);
                                    $('#side-t-end').html(det.toText);

                            }else{

                                    var suppData4 = jArray[iX];
                                    //console.log(suppData4);
                                    var suppData5        = jArray[iX].product_supplements;
                                    var db_addons        = jArray[iX].sub_supplement_avail_name_data;
                                    var supplementsTotal = 0;

                                    $('input[name="package[comPrice]"]').val(suppData4['comPrice']);
                                    $('input[name="package[OriginalBasePrice]"]').val(suppData4['displayPrice']);
                                    $('input[name="package[name]"]').val( 'From ' + suppData4['fromDayText'] + " to " + suppData4['toDayText']);
                                    $('input[name="package[grossPrice]"]').val(suppData4['grossPrice']);
                                    $('input[name="package[markupPrice]"]').val(suppData4['markupPrice']);
                                    $('input[name="package[fromCurrency]"]').val(suppData4['fromCurrency']);
                                    $('input[name="package[toCurrency]"]').val(suppData4['toCurrency']);
                                    $('input[name="package[convertedRate]"]').val(suppData4['convertedRate']);
                                    $('input[name="package[comm_pct_amount]"]').val(suppData4['comm_pct_amount']);
                                    $('input[name="package[markup_pct_amount]"]').val(suppData4['markup_pct_amount']);
                                    $('input[name="package[netAmount]"]').val(suppData4['netAmount']);
                                    $('input[name="package[convertedBasePrice]"]').val(suppData4['convertedBasePrice']);
                                    
                                    

                                    $('#side-t-single-price-tours').html('<b>Tours Price: $</b>' + $.number( truncate( jArray[iX].grossPrice, 2 ), 2 ));
                                    /*if(jArray[iX].product_supplements.length > 0){
                                        $('#side-t-supplement').html('<b style="font-size: 1.5; font-weight: 900">Supplements:</b>');
                                    }*/
                                    if(typeof suppData5 == 'object'){
                                        $('#side-t-supplement').html('<b >Products:</b>');
                                    }
                                    var $inpoy1 = $('<input />', { 
                                        type :  'radio', 
                                        value:  suppData4['grossPrice'], 
                                        id   :  'sub_product_price_list',
                                        name :  'sub_product_price_list',
                                        style: 'vertical-align: top; visibility: hidden; display: none;'
                                        // style: 'vertical-align: top; visibility: hidden; display: none;'
                                        // text    :  item['grossPrice'],
                                    });
                                    $inpoy1.prop('checked', true);
                                    $inpoy1.data('price', suppData4['grossPrice']);

                                    var $_wrapper = $( '<div class="col-md-12"></div>' );
                                    $_wrapper.append( $inpoy1 );
                                    $_wrapper.appendTo( '#side-t-supplement_name1' );

                                    // if(db_addons.length){
                                    //     $('#side-t-multiple_supplements').html('<b>Add On:</b>');
                                    // }


                                    generateAddon( db_addons );
                                    generateSuppliments( suppData5 );
                                    assingAnalyzation ( jArray[iX].analyzation );

                                     var singlePriceTotal = supplementsTotal + totalPrice;

                                    $('#startdate').val(det.fromText);
                                    $('#startend').val(det.toText);
                                    $('#dateduration').val((diffDays+1)+' days');

                                    $('#side-t-num').html((diffDays+1)+' days');
                                    $('#side-t-start').html(det.fromText);
                                    $('#side-t-end').html(det.toText);
                                    $('#tour-price').html("$"+singlePriceTotal.toFixed(2));
                                    $('#tourTotalPrice').val("$"+singlePriceTotal.toFixed(2));

                            }
                        }
                    }
                }

                 $(document).ready(function(){

                    /*=====================================
                    =            SUBMIT BUTTON            =
                    =====================================*/
                    
                    $submitButton.click(function () {
                        // console.log('Submit Button');

                        var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                        updateFooter( $productWrapper.find('div.product-footer'), data );

                        // console.log( FORM_ERRORS );

                        if ( FORM_ERRORS.length > 0 )
                        {
                            BootstrapDialog.show({
                                type    : BootstrapDialog.TYPE_DANGER,
                                cssClass: 'term-and-condition',
                                title   : '<strong>Opps! Something went wrong.</strong>',
                                message : function ( dialog ) {
                                    var $message = $( '<ul></ul>' );

                                    $.each( FORM_ERRORS, function ( key, item ) {
                                        $message.append('<li>' + item + '</li>');
                                    });

                                    return $message;
                                },
                                onshown: function ( dialog ) {
                                    $hover = dialog.getModalBody().find('a[data-hover-to]');

                                    $hover.each(function( key, ele ) {
                                        $( ele ).click( function () { eval( $( ele ).data('clickFunction') + '(dialog, ele)') } );
                                    });
                                }
                            });
                        } else {
                            if ( ! $('.price .undescounted').hasClass('sr-only') )
                            {
                                $( '#previousPrice' ).val( $('.price .undescounted > span').text() );
                            } else {
                                $( '#previousPrice' ).val( '' );
                            }
                            $( this ).attr('type', 'submit');
                            $( this ).submit();
                        }
                    });
                    
                    /*=====  End of SUBMIT BUTTON  ======*/
                    
                    
                    /*=====================================
                    =            FLIGHT SCRIPT            =
                    =====================================*/
                    $flightDetail.find('input#flight-terms').change(function () {
                        var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                        updateFooter( $productWrapper.find('div.product-footer'), data );
                    });

                    $( 'input[name="includeFlight"]' ).change(function () {
                        // console.log( $( this ).val() );
                        var passengers = parseInt( $flightDetail.find('input#flight-pax').val() );

                        if( ! ( $( this ).val() == 'false' ) && passengers == 0 )
                        {
                            BootstrapDialog.show({
                                type    : BootstrapDialog.TYPE_WARNING,
                                cssClass: 'term-and-condition',
                                title   : '<strong>Opps! Something a warning came up.</strong>',
                                message : 'Please select a item before selecting flights',
                                onhide  : function ( dialog ) {
                                    jQuery( 'input[name="includeFlight"]' ).prop('checked', false);
                                }
                            });
                        } else {
                            disableFlight( ( $( this ).val() == 'false' ) );
                        }

                        var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                        updateFooter( $productWrapper.find('div.product-footer'), data );
                        
                    });
                    $( 'input.flight-date' ).datepicker({
                        dateFormat: "dd/mm/yy",
                        beforeShow: function ( input, inst ) {
                            $('#ui-datepicker-div').removeClass('booking-calendar').addClass('booking-calendar');
                        }
                    });

                    $( 'input.departure.flight-date' ).datepicker('option', 'onSelect', departureOnSelect);

                    $flightTermCondition.click(function () {
                        BootstrapDialog.show({
                            title   : '<strong>Flight Terms and Conditions</strong>',
                            message : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum quia porro nisi odio doloribus veritatis veniam ducimus commodi rerum blanditiis tempore atque, dolores rem aliquid, beatae, inventore! Enim, suscipit, quo?',
                            cssClass: 'term-and-condition',
                            onhide  : function ( dialog ) {
                                $flightDetail.find('input#flight-terms').change();
                            },
                            buttons : [
                                {
                                    label : 'Disagree',
                                    action: function ( dialog ) {
                                        $( 'input[name="flight[terms]"]' ).prop('checked', false);
                                        dialog.close();
                                    }
                                }, {
                                    label   : '<strong>Agree</strong>',
                                    cssClass: 'btn-primary',
                                    action  : function ( dialog ) {
                                        $( 'input[name="flight[terms]"]' ).prop('checked', true);
                                        dialog.close();
                                    }
                                }
                            ]
                        });
                    });
                    $flightPreferredAirline.click(function () {
                        BootstrapDialog.show({
                            title   : '<strong>Airlines Used - subject to availability <span class="fa fa-plane"></span></strong>',
                            cssClass: 'term-and-condition',
                            message : function ( dialog ) {
                                var size     = Math.round(AVAILABLE_AIRLINES.length / 2);
                                var $message = $( '<div class="dialog-content col-md-12"></div>' );

                                size++;
                                var split = [AVAILABLE_AIRLINES.slice(0, (size - 1)), AVAILABLE_AIRLINES.slice(size)];

                                split.forEach(function ( item ) {
                                    var $div = $( '<div class="col-md-6 col-sm-6"></div>' )
                                    var $ul  = $( '<ul></ul>' );

                                    item.forEach(function ( airline ) {
                                        $ul.append('<li>' + airline + '</li>');
                                    });

                                    $div.append( $ul );
                                    $message.append( $div );
                                });

                                dialog.getModalBody().css('height', '190px');

                                return $message;
                            },
                            // onshown: function ( dialog ) {
                            //     var $message = dialog.getModalBody().find('div.dialog-content');
                            //     dialog.getModalBody().css('height', ($message.height() + 30));
                            // }
                        });
                    });

                    
                    /*=====  End of FLIGHT SCRIPT  ======*/
                    
                    

                    $('[data-toggle="tooltip"]').tooltip();
                    $('[data-toggle="popover"]').popover();

                    $('select#getchild').change(function () {
                        get_child( parseInt( $(this).val() ) );
                    });
                    jQuery( 'div.book-summary' ).hide();

                   /* $('select[name="pax"]').change(function () {
                        var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                        updateBookingTotal( data );
                    });*/
                    
                   $('#calendarHolder').datepicker({
                        inline        : true,
                        prevText      : "Prev",
                        nextText      : "Next",
                        numberOfMonths:2,
                        dayNamesMin   : ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                        beforeShowDay : availableDates,
                        defaultDate   : jArray[0].fromMonth  + '/' + jArray[0].fromDay + '/' + jArray[0].fromYear,
                        beforeShow    : function(elem, dp) { //This is for non-inline datepicker
                            console.log( 'beforeShow' );
                            $('#ui-datepicker-div').removeClass('booking-calendar').addClass('booking-calendar');
                            updateDatePickerCells();
                        },
                        onSelect: function (date, dp) {

                            updateDatePickerCells();
                            if( (typeof $('div.product-content').data('date') == 'undefined') || ($('div.product-content').data('date') != date) )
                            {
                                $( 'div.pax' ).attr('style', 'margin-top: 30px; visibility: visible');
                                // $('div.add-to-cart button').attr('style', 'font-weight: 900; font-size: 1.2em; border-radius: 5px; background-color: #009688 !important;');
                                $('div.product-content').attr('style', 'visibility: visible; display: block; border-top: 3px solid #ccc; margin-top: 30px; background-color: #f2f2f2; padding-bottom: 30px; border-bottom-left-radius: 0px;');
                                $('div.product-content').data('date', date);
                                updateDatePickerCells();
                                updateSetDate(date);
                                var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                                updateBookingTotal( data );
                                jQuery( 'div.promo' ).show();
                                init();

                                return;
                            }

                        },
                        onChangeMonthYear: function(month, year, dp) {
                            // console.log( 'onChangeMonthYear' );
                            updateDatePickerCells();
                        }

                    });
                    updateDatePickerCells();
                });  

            }(jQuery));     
    function get_child(a){
        jQuery('div', '#childAge').empty().remove();
        if(a != 0){
            for(x = 0;x < a;x++){
                var y = 7 + x;
                jQuery('#childAge').append('<div class = "col-md-3"><div class = "form-group"><label>Age</label><select class="form-control" style ="width: 80px" name="childage[]"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option></select></div></div>'
                    );
            }
        }
    }

    var $startdate = jQuery('input#startdate');
    var $startend  = jQuery('input#startend');

    var $selectedPax             = jQuery('select[name="pax"]');
    var $submitButton            = jQuery( 'button.form-button' );
    var $totalSelectedPax        = jQuery('div.product-footer span.total-guests');
    var $productWrapper          = jQuery('div#side-t-supplement_name3');
    var $addonsWrapper           = jQuery( '#side-t-supplement_name2' );
    var $agentView               = jQuery( 'div.agent-view' );
    var $promo                   = jQuery( 'div.promo' );
    var $flightPreferredAirlines = jQuery( 'a#flight-preferred-airlines' );
    var discounts                = {};
    var isDiscounted             = false;
    var data                     = {
        totalSelectedGuest: 0
    };

    var $totalPrice             = jQuery( 'span#tour-price' );

    var $flightDetail           = jQuery( 'div.fligh-details' );
    var $flightOption           = jQuery( 'div.flight-details-option' );
    var $flightPreferredAirline = jQuery( 'a#flight-preferred-airlines' );
    var $flightTermCondition    = jQuery( 'a#flight-terms-and-condition' );
    var $isFlightIncluded       = jQuery( 'input[name="includeFlight"]' );
    var $departureFee           = jQuery( 'span#departure-fee' );

    var AVAILABLE_AIRLINES = <?=AVAILABLE_AIRLINES?>;
    var DEPARTURE_MONTH    = <?=DEPARTURE_MONTH?>;
    
    discounts.codes        = <?=json_encode(@$DISCOUNTS)?>;

    $promo.hide();
    $submitButton.hide();
    var analyzationCandidates = [];
    var FORM_ERRORS           = [];
    var LOG_SEQUENCE          = false;


    /*==================================================
    =            CLICK EVENTS OF THE HOVERS            =
    ==================================================*/
    
    /**
     * [onClickFlightOptions description]
     * @param  {[type]} dialog [description]
     * @return {[type]}        [description]
     */
    var onClickFlightOptions = function onClickFlightOptions ( dialog, target )
    {
        console.info( 'onClickFlightOptions' );
        $elementToHover = jQuery( jQuery( target ).data('hoverTo') );
        elemStyle       = $elementToHover.attr('style');

        jQuery( 'html, body' ).animate({scrollTop: $elementToHover.offset().top - (jQuery( window ).height() / 2)}, {
            duration: 250,
            done    : function (  ) {
                dialog.close();
                $elementToHover.animate({color: 'red'}, {duration: 1000, done: function () { $elementToHover.attr('style', elemStyle); }});
            }
        });
    };

    /**
     * [onClickFlightTerms description]
     * @param  {[type]} dialog [description]
     * @return {[type]}        [description]
     */
    var onClickFlightTerms = function onClickFlightTerms ( dialog, target )
    {
        $elementToHover = jQuery( jQuery( target ).data('hoverTo') );
        elemStyle       = $elementToHover.find('label').attr('style');
        jQuery( 'html, body' ).animate({scrollTop: $elementToHover.offset().top - (jQuery( window ).height() / 2)}, {
            duration: 250,
            done    : function () {
                dialog.close();
                $elementToHover.find('label').animate({color: 'red'}, {duration: 1000, done: function () { $elementToHover.find('label').attr('style', elemStyle); }});
            }
        });
    };
    
    /*=====  End of CLICK EVENTS OF THE HOVERS  ======*/
    

    /**
     * [popoverOnClick description]
     * @return {[type]} [description]
     */
    function popoverOnClick ()
    {
        jQuery('.popover div.popover-content').click(function () {
            jQuery( this ).closest('div.popover').remove();
        });
    };

    /**
     * [departureOnSelect description]
     * @return {[type]} [description]
     */
    function departureOnSelect (  )
    {
        // console.log( 'Departure Date', $totalPrice.data() );
        if( $flightDetail.data('disabled') )
        {
            return false;
        }


        var passengers = parseInt( $flightDetail.find('input#flight-pax').val() );
        
        var $this      = jQuery( 'input.departure.flight-date' );
        var duration   = parseInt( jQuery( 'input#dateduration' ).val() );
        var month      = jQuery.datepicker.formatDate('MM', $this.datepicker('getDate'));

        month = month.toUpperCase();

        // var _minDate = jQuery( 'input.return.flight-date' ).datepicker('option', 'minDate');
        var departureDate = new Date( jQuery.datepicker.formatDate('yy-m-d', $this.datepicker('getDate')) );
        departureDate.setDate(departureDate.getDate() + duration);

        // jQuery( 'input.return.flight-date' ).datepicker('option', 'minDate', departureDate);
        jQuery( 'input.return.flight-date' ).datepicker('setDate', departureDate);

        var departurePrice =  0;
        var departureText  = '$0.00';

        // console.log( DEPARTURE_MONTH, month );

        if( typeof DEPARTURE_MONTH[ month ] != 'undefined' )
        {
            departurePrice = DEPARTURE_MONTH[ month ] * passengers;
            var subTotal   = $totalPrice.data('price') + departurePrice;

            $departureFee.data('perPerson', DEPARTURE_MONTH[ month ]);
            $totalPrice.text('$' + jQuery.number(subTotal, 2));
            departureText = '$' + jQuery.number(DEPARTURE_MONTH[ month ], 2) + ' X ' + passengers + ' = $' + jQuery.number(departurePrice, 2);
        } else {
            $totalPrice.text('$' + jQuery.number($totalPrice.data('price'), 2));
            departurePrice = 0;
        }
        
        $departureFee.text( departureText );
        $departureFee.data('price', departurePrice);
        jQuery( 'input[name="flight[price]"]' ).val( departurePrice );
        updatePaidAmount();

        forDiscount();
    };

    /**
     * [updatePaidAmount description]
     * @return {[type]} [description]
     */
    function updatePaidAmount (  )
    {
        var f_price = parseInt( jQuery( 'input[name="flight[price]"]' ).val() );
        var p_amt   = parseInt( jQuery( 'input[name="amount"]' ).data('original') );
        p_amt += f_price;
        jQuery( 'input[name="amount"]' ).val( jQuery.number(p_amt, 2, '.', '') );
    };

    /**
     * [disableFlight description]
     * @param  {Boolean} isDisabled [description]
     * @return {[type]}             [description]
     */
    function disableFlight ( isDisabled )
    {
        isDisabled = ( isDisabled != undefined ) ? isDisabled : true;

        var $flightInputs = jQuery( 'div.fligh-details  input[name^="flight["], select[name^="flight["]' );

        $flightInputs.each(function () {
            if( ! jQuery( this ).prop('readonly') )
            {
                jQuery( this ).prop('disabled', isDisabled);
            }
        });

        $departureFee.text( '$' + jQuery.number(0, 2) );
        $totalPrice.text('$' + jQuery.number($totalPrice.data('price'), 2));

        $flightDetail.data('disabled', isDisabled);
        $flightDetail.hide();
        // FLIGHT IS INCLUDED
        if( ! isDisabled )
        {
            $flightDetail.show();

            var subTotal       = $totalPrice.data('price') + $departureFee.data('price');
            var departurePrice = $departureFee.data('price');
            var perPerson      = $departureFee.data('perPerson');
            
            var passengers     = parseInt( $flightDetail.find('input#flight-pax').val() );

            $totalPrice.text('$' + jQuery.number(subTotal, 2));
// console.log( $departureFee.data() );
            $departureFee.text( '$' + jQuery.number(perPerson, 2) + ' X ' + passengers + ' = $' + jQuery.number(departurePrice, 2) );
            departureOnSelect();
        } else {
            forDiscount();
        }
        // updatePaidAmount();
    };

    /**
     * [scrollToItem description]
     * @return {[type]} [description]
     */
    function scrollToItem ()
    {
        var $content = jQuery('div.product-content');

        jQuery('html, body').animate({scrollTop: $content.offset().top - 20}, 250);
    }

    /**
     * [init description]
     * @return {[type]} [description]
     */
    function init (  )
    {
        var selectedPaxVal = parseInt($selectedPax.val());

        (function ($) {
            // console.log( $.datepicker.formatDate('dd/mm/yy', new Date($startend.val())) );
            /*=====================================
            =            FLIGHT SCRIPT            =
            =====================================*/
            $submitButton.show();

            temp_date = new Date($startend  .val());
            temp_date.setDate( temp_date.getDate() + 1 );
// console.log( 'Return Date', temp_date );
            $( 'input.return.flight-date' ).datepicker('option', 'minDate', temp_date);

            temp_date = new Date();
            temp_date.setDate( temp_date.getDate() + 1 );
            $( 'input.departure.flight-date' ).datepicker('option', 'minDate', temp_date);

            disableFlight(  );
            $( 'input[name="includeFlight"]' ).prop('checked', false);

            var temp_date = '';

 

            

            temp_date = new Date($startdate.val());
            temp_date.setDate( temp_date.getDate() - 2 );

            // $flightDetail.find('input#FD-departure-date').val( $.datepicker.formatDate('dd/mm/yy', temp_date) );
            $flightDetail.find('input#FD-departure-date').datepicker( 'setDate', temp_date );
            // $flightDetail.find('input#FD-return-date').val( $.datepicker.formatDate('dd/mm/yy', new Date($startend.val())) );
            temp_date = new Date($startend.val());
            temp_date.setDate( temp_date.getDate() + 1 );

            $flightDetail.find('input#FD-return-date').datepicker( 'setDate', temp_date);
            /*=====  End of FLIGHT SCRIPT  ======*/


            var $list = $( 'div.product-addons-list b' );

            if ( $list.length )
            {
                $('html, body').animate({scrollTop: $list.offset().top - 20}, 250);
            }

            $promo.find('input#promo-code').change(function () {
                discounts.isDiscounted = false;
                $promo.find('label.control-label').text('');

                if( $( this ).val() != '' && typeof discounts.codes[ $( this ).val() ] != 'undefined' )
                {
                    discounts.isDiscounted = true;
                    discounts.activeCode   = $( this ).val();
                }
                forDiscount();
                // console.log( 'discounts', discounts )
            });
            /*$totalSelectedPax.text('0');
            $totalSelectedPax.data('value', 0);
            $('div.product-footer span.max-guests').text( selectedPaxVal );

            $selectedPax.data('previousValue', 0);

            selectionSwitch();
            initializeSupplimentQuantity();
            // $('div.suppliment-footer span.max-guests').data('pax', parseInt( $selectedPax.val() ));
            $selectedPax.focus(function () {
                var $this = $( this );
                var value = parseInt($this.val());

// console.log( 'Focus', value );
                $this.data('previousValue', value);
            }).change(function () {
// console.log('###################################################' );
                var $this = $( $this );
                var pax   = parseInt( $( this ).val() );
                var data  = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                
                $selectedPax.prop('disabled', true);
// console.log( 'Change', pax );
                $('div.suppliment-footer span.max-guests').text( pax );
                updateBookingTotal( data );

                // $( this ).data('previousValue', pax);
                $.when( selectionSwitch() ).then(function() {
// console.info('Trigger Selection change');
// console.log( "$totalSelectedPax.data('value')", $totalSelectedPax.data('value') );
                    initializeSupplimentQuantity();

                    activatePaxSelection();
                });
                // updateFooter( $productWrapper.find('div.suppliment-footer'), {selectedGuests: 0, prices: 0} );


                // initializeSupplimentQuantity();
                // setTimeout( function () {
                //     // $this.prop('disabled', false);
                // }, 5000 );
                // // 
                // $('div.suppliment-footer span.max-guests').data('pax', pax);
            }); */
        })(jQuery);
    };

    /**
     * [forDiscount description]
     * @return {[type]} [description]
     */
    function forDiscount ()
    {
        if ( LOG_SEQUENCE ) { console.info('forDiscount'); }

        // console.log( discounts );
        jQuery( 'input#isDiscounted' ).val( ( ('isDiscounted' in discounts) ? discounts.isDiscounted : false ) + '' );
        var $originalPrice  = jQuery('.price .undescounted');
        var $totalPrice     = jQuery( '#tour-price' );
        var discountedPrice = 0;
        var price           = $totalPrice.data('price');
        var totalPax        = ( typeof $totalSelectedPax.data('value') == undefined ) ? 0 : $totalSelectedPax.data('value');
        var flightPrice     = 0;
        var originalPrice   = price;

        $originalPrice.addClass('sr-only');

        // console.log( '$flightDetail', $flightDetail.data(), '$departureFee', $departureFee.data() );
        $promo.find('label.control-label').text('');
        if( ! $flightDetail.data('disabled') )
        {
            flightPrice = $departureFee.data('price');
            originalPrice += flightPrice;
            $departureFee.text( '$' + jQuery.number( (flightPrice / totalPax), 2 ) + ' X ' + totalPax + ' = $' + jQuery.number( flightPrice, 2 ) );
            // console.log( '$departureFee.text()', $departureFee.text() );
        } else {
            $departureFee.text('$0.00');
        }
        if( discounts.isDiscounted )
        {
            var discount       = discounts.codes[ discounts.activeCode ];
            var discountedDate = new Date();

            if ( discount.category == 'flight' )
            {
                var _split_date = $flightDetail.find('input#FD-departure-date').val().split('/');
                discountedDate  = new Date( _split_date[2] + '-' + _split_date[1] + '-' + _split_date[0] );
            }

            if ( isInRageDate( discount.availabilities.from, discount.availabilities.to, jQuery.datepicker.formatDate('yy-mm-dd', discountedDate) ) )
            {
                // console.info('Date is in range', discount.category);
                var discountAmount = getDiscountValue( discount.type, discount.value, price, totalPax );

                switch ( discount.category )
                {
                    case 'product':
                        price -= discountAmount;
                        $promo.find('label.control-label').text('Discounted by ' + discount.value + '% on land product');
                        break;

                    case 'flight':
                        if( flightPrice > 0 )
                        {
                            flightPrice = discountAmount;
                            $departureFee.text( '$' + jQuery.number( discount.value, 2 ) + ' X ' + totalPax + ' = $' + jQuery.number( discountAmount, 2 ) );
                        }
                        if ( ! $flightDetail.data('disabled') )
                        {
                            $promo.find('label.control-label').text('Flight discounted to $' + jQuery.number( discount.value, 2 ) + ' per passenger.');
                        } else {
                            jQuery( 'input#isDiscounted' ).val( 'false' );
                        }
                        break;
                }
                discountedPrice = price + flightPrice;

                $originalPrice.removeClass('sr-only');
                $originalPrice.find('span').text( '$' + jQuery.number(originalPrice, 2) );
                $totalPrice.text( '$' + jQuery.number(discountedPrice, 2) );
            }
            return false;
        }
        $totalPrice.text( '$' + jQuery.number((flightPrice + $totalPrice.data('price')), 2) );
    };

    /**
     * [isInRageDate description]
     * @param  {[type]}  fDate [description]
     * @param  {[type]}  lDate [description]
     * @param  {[type]}  cDate [description]
     * @return {Boolean}       [description]
     */
    function isInRageDate ( fDate, lDate, cDate )
    {

        fDate = jQuery.datepicker.parseDate('yy-mm-dd', fDate);   
        lDate = jQuery.datepicker.parseDate('yy-mm-dd', lDate);
        cDate = jQuery.datepicker.parseDate("yy-mm-dd", cDate);

        if( (cDate <= lDate && cDate >= fDate) )
        {
            return true;
        }
        return false;
    }

    /**
     * [getDiscountValue description]
     * @param  {[type]} type  [description]
     * @param  {[type]} value [description]
     * @param  {[type]} price [description]
     * @return {[type]}       [description]
     */
    function getDiscountValue ( type, value, price, pax )
    {
        pax = pax || 0;
        // console.log( 'getDiscountValue', type, value, price, pax )
        var computedPrice = 0;
        switch ( type )
        {
            case 'percentage':
                computedPrice = Math.round( price * ( value / 100) );
                break;
                
            case 'amount':
                computedPrice = Math.round( value * pax );
                break;
        }

        return computedPrice;
    }

    /**
     * [activatePaxSelection description]
     * @return {[type]} [description]
     */
    function activatePaxSelection ( )
    {
        if( LOG_SEQUENCE )
        {
            console.info( 'activatePaxSelection' );
        }
        setTimeout(function () {
            var events = supplimentEvents();
// console.log( events );
            if( events.total == events.running )
            {
                $selectedPax.prop('disabled', false);
            } else {
                activatePaxSelection();
            }
        }, 500);
    };

    /**
     * [getAddonsQuantities description]
     * @return {[type]} [description]
     */
    function getAddonsQuantities ()
    {
        var $inputs = jQuery( 'span#side-t-supplement_name1' ).find('input.addon-quantity');
        var total   = 0;

        $inputs.each(function() {
            total += parseInt( jQuery( this ).val() )
        });

        return total;
    };

    /**
     * [assingAnalyzation description]
     * @param  {[type]} analyzation [description]
     * @return {[type]}             [description]
     */
    function assingAnalyzation ( analyzation )
    {
        // console.log( 'analyzation', analyzation );
        analyzationCandidates = analyzation;
    };

    /**
     * [selectionSwitch description]
     * @param  {Boolean} isEnable [description]
     * @return {[type]}           [description]
     */
    function selectionSwitch (  )
    {
        (function ($) {
            var $selections = $('div.product-body select.suppliment-guest');
            var maxPax      = parseInt($selectedPax.val());
            var count       = $selections.length;

// console.log( 'totalSelectedGuests', totalSelectedGuests );
            $selections.each(function() {
// console.log( "$selectedPax.data('previousValue')", $selectedPax.data('previousValue') );
                var $this               = $( this );
                var quantity            = parseInt( $this.val() );
                var totalGuest          = quantity * $this.data('minGuest');
                var style               = $this.attr('style') + '';
                var spanStyle           = $this.next().attr('style') + '';
                var hide                = ' display: none; visibility: hidden;';
                var totalSelectedGuests = $totalSelectedPax.data('value');

                $this.val('0');
                $.when( $this.trigger('change') )
                .done( function( data ) {
                    spanStyle = spanStyle.replace( hide, '' );
                    spanStyle = spanStyle.replace('undefined', '');
                    style     = style.replace( hide, '' );
                    style     = style.replace('undefined', '');
    // console.log( 'totalGuest', totalGuest, $this.data() );
                    if( $this.data('previousValue') == undefined )
                    {
                        $this.data('previousValue', 0);
                    }
                    if( $this.data('minGuest') > maxPax )
                    {
                        $this.prop('disabled', true);
                        $this.attr('style', style + hide)
                        $this.next().attr('style', spanStyle);
                    } else {
                        $this.prop('disabled', false);
                        $this.attr('style', style);
                        $this.next().attr('style', spanStyle + hide);
                    }
                    if( ! --count )
                    {
                        return;
                    }
                });

            });
// $($selections.get().reverse())
            
        })(jQuery);
    };

    /**
     * [emptyPackages description]
     * @return {[type]} [description]
     */
    function emptyPackages ()
    {
        var $packages = jQuery('#side-t-supplement_name1 input.addon-quantity[type="number"]');
        $packages.val('0');
        $packages.change();
    };

    /**
     * [initializeSupplimentQuantity description]
     * @return {[type]} [description]
     */
    function initializeSupplimentQuantity (  )
    {
        (function ( $ ) {
            var $selections = $('div.product-body select.suppliment-guest');
            var maxPax      = parseInt($selectedPax.val());

// console.log( "$totalSelectedPax.data('value')", $totalSelectedPax.data('value') );

            $($selections.get().reverse()).each(function () {
                var $this               = $( this );
                var candidate           = analyzationCandidates[ $this.data('minGuest') ];
                var supplimentKey       = $this.parent().parent().parent().data('supplimentKey');
                var totalSelectedGuests = $totalSelectedPax.data('value');
                var minimumGuest        = $this.data('minGuest');
                var quantity            = parseInt(maxPax / minimumGuest);

                /*if( minimumGuest > maxPax || candidate.key != supplimentKey )
                {
                    return;
                }*/
// console.log( 'candidate', candidate, 'supplimentKey', supplimentKey, 'quantity', quantity );
                if( ((quantity * minimumGuest) + totalSelectedGuests) <= maxPax )
                {
                    $this.val( quantity + '' );
                    // $this.trigger('change', {});
                    $this.trigger('change', {});
                    return;
                }

                quantity = maxPax - totalSelectedGuests;
                if( quantity < 0 ) { quantity *= -1; }
                if( quantity == 0 ) { return; }

                while( true )
                {
                    if( ((quantity * minimumGuest) + totalSelectedGuests) <= maxPax )
                    {
// console.log( 'quantity', quantity );
                        $this.val( quantity + '' );
                        // $this.trigger('change', {});
                        $this.trigger('change', {});
                        break;
                    }

                    if( quantity == 0 )
                    {
                        break;
                    }
                    quantity--;
                }

            });
        })(jQuery);
    }

    /**
     * [generateAddon description]
     * @param  {[type]} addons [description]
     * @return {[type]}        [description]
     */
    function generateAddon ( addons )
    {
        // console.info( 'generateAddon' );
// $addonsWrapper
        (function ($) {
            var $bookInputs = $( 'div.book-addons' );

            $bookInputs.empty();
            $addonsWrapper.empty();

            $.each( addons, function( key, item ) {
                var $input              = $('<input />', { type: 'checkbox', value: item.supplement_name, id: 'addON[' + key + '][name]', name: 'addON[' + key + '][name]', style: 'vertical-align: top' });
                var $_wrapper           = $( '<div class="col-md-12"></div>' );
                var $bookInputs_Wrapper = $('<div></div>');

                $bookInputs_Wrapper.data('itemKey', key);

                $input.data('price'     , item.grossPrice);
                $input.data('commission', item.commission);
                $input.data('netAmount' , item.netAmount);

                $_wrapper.append( $input );
                $_wrapper.append( $( '<span>&nbsp;</span>', {} ) );
                $_wrapper.append( $( '<label />', {
                    text : item['supplement_name'] + ': $' + $.number( truncate(item['grossPrice'], 2), 2 ),
                    style:  "color: #777777; width: 290px",
                    for  : "addON[" + key + "][name]"
                } ) );
                $_wrapper.append( $('<br />') );

                $addonsWrapper.append( $_wrapper );

                $input.change(function () {
                    var $this = $( this );

                    if( $this.prop('checked') )
                    {
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['OriginalBasePrice'], 
                            id      :  'addON[' + key + '][OriginalBasePrice]',
                            name    :  "addON[" + key + "][OriginalBasePrice]",
                            // text    :  item['OriginalBasePrice']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['fromCurrency'], 
                            id      :  'addON[' + key + '][sub_fromCurrency]',
                            name    :  "addON[" + key + "][sub_fromCurrency]",
                            // text    :  item['sub_fromCurrency']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['toCurrency'], 
                            id      :  'addON[' + key + '][sub_toCurrency]',
                            name    :  "addON[" + key + "][sub_toCurrency]",
                            // text    :  item['sub_toCurrency']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['convertedRate'], 
                            id      :  'addON[' + key + '][convertedRate]',
                            name    :  "addON[" + key + "][convertedRate]",
                            // text    :  item['convertedRate']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['comPrice'], 
                            id      :  'addON[' + key + '][comPrice]',
                            name    :  "addON[" + key + "][comPrice]",
                            // text    :  item['comPrice']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['comm_pct_amount'], 
                            id      :  'addON[' + key + '][comm_pct_amount]',
                            name    :  "addON[" + key + "][comm_pct_amount]",
                            // text    :  item['comm_pct_amount']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['grossPrice'], 
                            id      :  'addON[' + key + '][grossPrice]',
                            name    :  "addON[" + key + "][grossPrice]",
                            // text    :  item['grossPrice']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['markup_pct_amount'], 
                            id      :  'addON[' + key + '][markup_pct_amount]',
                            name    :  "addON[" + key + "][markup_pct_amount]",
                            // text    :  item['markup_pct_amount']
                        }) );

                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['netAmount'], 
                            id      :  'addON[' + key + '][netAmount]',
                            name    :  "addON[" + key + "][netAmount]",
                            // text    :  item['netAmount']
                        }) );
                        $bookInputs_Wrapper.append( $('<input />', { 
                            type    :  'hidden', 
                            value   :  item['convertedBasePrice'], 
                            id      :  'addON[' + key + '][convertedBasePrice]',
                            name    :  "addON[" + key + "][convertedBasePrice]",
                            // text    :  item['convertedBasePrice']
                        }) );

                        $bookInputs.append( $bookInputs_Wrapper );
                    } else {
                        $bookInputs_Wrapper.empty();
                    }

                    var data = getSubTotal( $('div#side-t-supplement_name3 div.product-body').find('div[data-product-key]') );
                    updateBookingTotal( data );
                });
            });
        })(jQuery);
    };

    /**
     * [generateSuppliments description]
     * @param  {[type]} pax         [description]
     * @param  {[type]} supplements [description]
     * @return {[type]}             [description]
     */
    function generateSuppliments ( products, suppliments )
    {
        // console.info( 'generateSuppliments' );
        if( products.length == 0 )
        {
            var design = ' background-color: #009688 !important';
            var style  = $submitButton.attr('style');
            style.replace( design, '' );

            $submitButton.attr('style', style + design);
            // $submitButton.prop('disabled', false);
            $submitButton.data('disabled', false);
            $productWrapper.hide();
            return;
        }
        // $submitButton.prop('disabled', true);
        $submitButton.data('disabled', true);

        (function ( $ ) {
            var template = '';
            var status   = {
                pax  : 0,
                price: 0
            };

            $productWrapper.find('div.product-body').empty();

            $.each( products, function ( key, product ) {
                template += buildItem( product, key, suppliments );
            });

            $productWrapper.find('div.product-body').append( $( template ) );

            var $body = $productWrapper.find('div.product-body div[data-product-key]');

            if( $body.length )
            {
                $body.each( function ( key, ele ) {
                    var $productBody = $( this );
                    var $price       = $productBody.find('b.product-price');
                    var $select      = $productBody.find('select.product-guest');

                    var productKey = $productBody.data('productKey');
                    var product    = products[ productKey ];

                    var $radios = $productBody.find('div.suppliments input[type="radio"]');

                    $radios.change(function () {
                        $select.trigger('change');
                    });

                    $select.change(function (e, data) {
                        emptyPackages();
// console.info('Event Triggered');
// console.log( data );
                        var $bodySelect = $( this );
                        // console.log( suppliment, $bodySelect.val() );
                        var selectedMaxGuest = parseInt( $selectedPax.val() );
                        var totalGuest       = guest * product.minguest;

                        var guest            = parseInt( $bodySelect.val() );
                        var total            = 0;
                        var previousValue    = $bodySelect.data('previousValue');
                        $bodySelect.data('eventTrigger', true);

// console.log( 'totalGuest', totalGuest, $totalSelectedPax.data('value'), '$selectedPax', $selectedPax.val(), 'previousValue', previousValue );

                        if( typeof data == 'undefined' )
                        {
                            if( ! e.originalEvent )
                            {
                                selectedMaxGuest = $selectedPax.data('previousValue');
                            }
                        }

                        var $suppliment = $bodySelect.parent().parent().find('div.suppliments');

                        var x           = ( guest % $bodySelect.data('minGuest') );
                        total           = guest * Math.round(product.grossPrice);

                        $suppliment.find('input[type="radio"]').prop('disabled', false);

                        var labelStyle = $suppliment.find('label').attr('style');

                        if( $suppliment.find('label').length )
                        {
                            labelStyle = labelStyle.replace(' text-decoration: line-through;', '');

                            if( x == 0 || ( guest <= $bodySelect.data('minGuest') ) && (guest > $bodySelect.data('minGuest') ) )
                            {
                                $suppliment.find('label').attr('style', labelStyle + ' text-decoration: line-through;');

                                $suppliment.find('input[type="radio"]:checked').prop('checked', false);
                                $suppliment.find('input[type="radio"]').prop('disabled', true);
                                 // text-decoration: line-through;
                                delete product.supplimentKey;
                            } else {
                                $suppliment.find('label').attr('style', labelStyle);
                                if(guest < $bodySelect.data('minGuest') )
                                {
                                    x = ( $bodySelect.data('minGuest') - x );
                                }

                                var price          = 0;
                                var itemSuppliment = {};
                                var itemSupKey     = '';

                                if( $suppliment.find('input[type="radio"]:checked').length )
                                {
                                    itemSupKey = $suppliment.find('input[type="radio"]:checked').data('itemKey');
                                } else {
                                    var $selectedSuppliment = $suppliment.find('input[type="radio"]').first();
                                    $selectedSuppliment.prop('checked', true);

                                    itemSupKey = $selectedSuppliment.data('itemKey');
                                }

                                $suppliment.find('input[type="radio"]:checked').data('quantity', x);


                                itemSuppliment = suppliments[ itemSupKey ];
                                itemSuppliment.grossPrice = Math.round( itemSuppliment.grossPrice );
                                itemSuppliment.netAmount  = Math.round( itemSuppliment.netAmount );

                                total += ( x * itemSuppliment.grossPrice );

                                product.supplimentKey = itemSupKey;
                                product.extra         = x;
                                adddSuppliment( itemSupKey, itemSuppliment );
                            }
                        }
                        

// console.log( "selectedMaxGuest", selectedMaxGuest, 'typeof data', typeof data );
                        /*if( (totalGuest + $totalSelectedPax.data('value')) > selectedMaxGuest )
                        {
                            $bodySelect.val( $bodySelect.data('previousValue') + '' );
                            return;
                        }*/
                        /*if(
                            (((totalGuest - previousValue ) + $totalSelectedPax.data('value')) > selectedMaxGuest)
                        )
                        {
                            $bodySelect.val( $bodySelect.data('previousValue') + '' );
                            return;
                        }*/

                        if( guest == 0 )
                        {
                            total = Math.round(product.grossPrice);
                            // REMOVE THE INPUT SUPPLIMENT
                            removeSupplimentForm( productKey );
                        } else {
                            applyProductForm( product, productKey, guest );
                        }
                        $bodySelect.data('previousValue', guest);

                        // $price.text( '$' + $.number( truncate(total, 2), 2 ) );
                        $price.data('price', total);

                        var data = getSubTotal( $body );
                        updateFooter( $productWrapper.find('div.product-footer'), data );
                        updateBookingTotal( data );

                        $bodySelect.data('eventTrigger', false);
                        
                    });

                    // if( (suppliment.minguest == parseInt($selectedPax.val())) && ($totalSelectedPax.data('value') != parseInt($selectedPax.val())) )
                    // {
                    //     $bodySelect.val('1');
                    //     $bodySelect.trigger('change', {foo: 'bar'});
                    // }
                });
            }

        })(jQuery);
    };

    /**
     * [supplimentEvents description]
     * @return {[type]} [description]
     */
    function supplimentEvents (  )
    {
        var data     = { total: 0, running: 0 };
        var $selects = $productWrapper.find('div.product-body select.suppliment-guest');

        data.total = $selects.length;

        $selects.each(function () {
// console.log( jQuery( this ).data('eventTrigger') );
            if( ! jQuery( this ).data('eventTrigger') )
            {
                data.running++;
            }
        });

        return data;
    };
    
    /**
     * [applyProductForm description]
     * @param  {[type]} suppliment [description]
     * @param  {[type]} key        [description]
     * @return {[type]}            [description]
     */
    function applyProductForm ( product, key, guests )
    {
        (function ($) {
            if( $( 'div.item-product[data-product-key="' + key + '"]' ).length )
            {
                $( 'div.item-product[data-product-key="' + key + '"]' ).remove();
            }

            var $bookProduct = $('div.book-product');
            var $wrapper     = $( '<div class="item-product" data-product-key="' + key + '"></div>' );
            $wrapper.data('itemKey', key);
            

            $wrapper.append( $( ''
                +'<input type="hidden" name="products[' + key + '][extra]"              value="' + ((typeof product.extra != 'undefined' )?product.extra:'') + '">'
                +'<input type="hidden" name="products[' + key + '][supplimentKey]"      value="' + ((typeof product.supplimentKey != 'undefined' )?product.supplimentKey:'') + '">'
                +'<input type="hidden" name="products[' + key + '][name]"               value="' + product.supplement_avail_name + '">'
                +'<input type="hidden" name="products[' + key + '][guests]"             value="' + guests + '">'
                +'<input type="hidden" name="products[' + key + '][minguest]"           value="' + product.minGuest + '">'
                +'<input type="hidden" name="products[' + key + '][OriginalBasePrice]"  value="' + product.OriginalBasePrice + '">'
                +'<input type="hidden" name="products[' + key + '][fromCurrency]"       value="' + product.fromCurrency + '">'
                +'<input type="hidden" name="products[' + key + '][toCurrency]"         value="' + product.toCurrency + '">'
                +'<input type="hidden" name="products[' + key + '][convertedRate]"      value="' + product.convertedRate + '">'
                +'<input type="hidden" name="products[' + key + '][comPrice]"           value="' + product.comPrice + '">'
                +'<input type="hidden" name="products[' + key + '][comm_pct_amount]"    value="' + product.comm_pct_amount + '">'
                +'<input type="hidden" name="products[' + key + '][grossPrice]"         value="' + product.grossPrice + '">'
                +'<input type="hidden" name="products[' + key + '][markup_pct_amount]"  value="' + product.markup_pct_amount + '">'
                +'<input type="hidden" name="products[' + key + '][netAmount]"          value="' + product.netAmount + '">'
                +'<input type="hidden" name="products[' + key + '][convertedBasePrice]" value="' + product.convertedBasePrice + '">'
            ) );
            $wrapper.appendTo( $bookProduct );
        })(jQuery);
    };

    /**
     * [removeSupplimentForm description]
     * @param  {[type]} key [description]
     * @return {[type]}     [description]
     */
    function removeSupplimentForm ( key )
    {
        (function ( $ ) {
            $('div.book-product div.item-product').each(function () {
                if( $( this ).data('itemKey') == key )
                {
                    $( this ).remove();
                    return;
                }
            });
        })(jQuery);
    };

    /**
     * [getAllPax description]
     * @param  {[type]} $body [description]
     * @return {[type]}       [description]
     */
    function getSubTotal ( $body )
    {
        var data = {
            guests        : 0,
            prices        : 0,
            commissions   : 0,
            netAmounts    : 0,
            selectedGuests: 0,
            products      : [],
            suppliments   : {}
        };

        if( $body.length )
        {
            $body.each(function ( key, ele ) {
                var $this   = jQuery( this );
                var $select = $this.find('select.product-guest');
                var guest   = parseInt($select.val());
                var $suppliments = $this.find('div.suppliments input[type="radio"]:checked');

                if( $suppliments.length )
                {
                    // console.log('$suppliments.data()', $suppliments.data());
                    var supplimentDATA = {
                        name      : $suppliments.data('name'),
                        price     : (Math.round($suppliments.data('price'))      * $suppliments.data('quantity')),
                        commission: (Math.round($suppliments.data('commission')) * $suppliments.data('quantity')),
                        netAmount : (Math.round($suppliments.data('netAmount'))  * $suppliments.data('quantity')),
                        quantity  : $suppliments.data('quantity')
                    };
                    if( typeof data.suppliments[ $suppliments.data('itemKey') ] == 'undefined' )
                    {
                        data.suppliments[ $suppliments.data('itemKey') ] = supplimentDATA;
                    } else {
                        data.suppliments[ $suppliments.data('itemKey') ].price      += supplimentDATA.price;
                        data.suppliments[ $suppliments.data('itemKey') ].commission += supplimentDATA.commission;
                        data.suppliments[ $suppliments.data('itemKey') ].netAmount  += supplimentDATA.netAmount;
                        data.suppliments[ $suppliments.data('itemKey') ].quantity   += supplimentDATA.quantity;
                    }

                    /**
                     * Update the commission and net amound..
                     * Price is not included because its already updated...
                     */
                    data.commissions += supplimentDATA.commission;
                    data.netAmounts  += supplimentDATA.netAmount;
                }
                if( guest > 0 )
                {
                    var price      = Math.round($this.find('b.product-price').data('price'));
                    var commission = Math.round($this.find('b.product-price').data('commission'));
                    var netAmount  = Math.round($this.find('b.product-price').data('netAmount'));

                    data.selectedGuests += guest;
                    // data.selectedGuests += guest * $select.data('minGuest');
                    data.prices      += price;
                    data.commissions += commission * guest;
                    data.netAmounts  += netAmount * guest;

                    data.products.push({
                        name      : $this.find('div.product-name').text(),
                        price     : (Math.round($this.find('b.product-price').data('originalPrice')) * guest),
                        commission: (Math.round($this.find('b.product-price').data('commission'))    * guest),
                        netAmount : (Math.round($this.find('b.product-price').data('netAmount'))     * guest),
                        quantity  : guest
                    });
                }
            });
        }
// console.log( data );
        return data;
    };

    /**
     * [updateFooter description]
     * @param  {[type]} $footer [description]
     * @param  {[type]} data    [description]
     * @return {[type]}         [description]
     */
    function updateFooter ( $footer, data )
    {
        if ( LOG_SEQUENCE )
        {
            console.info( 'updateFooter' );
        }
        
        FORM_ERRORS = [];

        $footer.find('span.total-guests').text( data.selectedGuests );
        $footer.find('span.total-guests').data( 'value', data.selectedGuests );
        $footer.find('span.total-price').text( jQuery.number( data.prices, 2 ) );
        data.totalSelectedGuest = data.selectedGuests;
        
        $submitButton.data('disabled', false);
        // $submitButton.prop('disabled', false);
        // $submitButton.prop('disabled', ( ($totalSelectedPax.data('value') > 0)?false:true ));
        if ( ($totalSelectedPax.data('value') == 0) )
        {
            FORM_ERRORS.push('There are no selected items.');
            // $submitButton.prop('disabled', true);
            $submitButton.data('disabled', true);
        }

        if( $flightOption.find('input[name="includeFlight"]:checked').length == 0 )
        {
            FORM_ERRORS.push('Please select a flight option. <a data-hover-to=".flight-details-option" data-click-function="onClickFlightOptions" style="cursor: pointer;">Click here.</a>');
            // $submitButton.prop('disabled', true);
            $submitButton.data('disabled', true);
        } else {
            if ( $flightOption.find('input[name="includeFlight"]:checked').val() == 'true' && ! $flightDetail.find('input#flight-terms').prop('checked') )
            {
                FORM_ERRORS.push('Please agree to the terms and condition of the flight. <a data-hover-to=".flight-terms" data-click-function="onClickFlightTerms" style="cursor: pointer;">Click here.</a>');
                // $submitButton.prop('disabled', true);
                $submitButton.data('disabled', true);
            }
        }
        

        var buttonStyle  = $submitButton.attr('style');
        var buttonDesign = ' background-color: #009688 !important;';

        buttonStyle = buttonStyle.replace( buttonDesign, '' );

        if( ! $submitButton.data('disabled') )
        {
            $submitButton.attr('style', buttonStyle + buttonDesign);
        } else {
            $submitButton.attr('style', buttonStyle);
        }
        forDiscount();
    };

    /**
     * [updateBookingTotal description]
     * @return {[type]} [description]
     */
    function updateBookingTotal ( data )
    {
        if ( LOG_SEQUENCE )
        {
            console.info('updateBookingTotal');
        }
        (function( $ ) {
            var total      = 0;
            var commission = 0;
            var netAmount  = 0;
            var summary    = {
                packages   : [],
                products   : data.products,
                extraBills : [],
                suppliments: data.suppliments
            };
            var $packages   = $('#side-t-supplement_name1 input.addon-quantity[type="number"]');
            var $extraBills = $( 'div.extra-bill input[type="checkbox"]' )

            // var $addons     = $('#side-t-supplement_name2 input[type="checkbox"]:checked');

            if ( data.selectedGuests > 0 )
            {
                $packages.each(function () {
                    var $this       = $( this );
                    var quantity    = parseInt( $this.val() );
                    var packageDATA = {};

                    if( quantity )
                    {
                        var price         = ( Math.round($this.data('price'))      * quantity );
                        var PKGcommission = ( Math.round($this.data('commission')) * quantity );
                        var PKGnetAmount  = ( Math.round($this.data('netAmount'))  * quantity );

                        packageDATA.name       = $this.data('name');
                        packageDATA.price      = price;
                        packageDATA.commission = PKGcommission;
                        packageDATA.netAmount  = PKGnetAmount;
                        packageDATA.quantity   = quantity;

                        /*if( data.selectedGuests == 0 )
                        {
                            packageDATA.name       = $this.data('name');
                            packageDATA.price      = Math.round($this.data('price'));
                            packageDATA.commission = Math.round($this.data('commission'));
                            packageDATA.netAmount  = Math.round($this.data('netAmount'));
                            packageDATA.quantity   = 1;

                        } else {
                            var price         = ( Math.round($this.data('price'))      * data.selectedGuests );
                            var PKGcommission = ( Math.round($this.data('commission')) * data.selectedGuests );
                            var PKGnetAmount  = ( Math.round($this.data('netAmount'))  * data.selectedGuests );

                            packageDATA.name       = $this.data('name');
                            packageDATA.price      = price;
                            packageDATA.commission = PKGcommission;
                            packageDATA.netAmount  = PKGnetAmount;
                            packageDATA.quantity   = data.selectedGuests;
                        }*/

                        total      += packageDATA.price;
                        commission += packageDATA.commission;
                        netAmount  += packageDATA.netAmount;
                        summary.packages.push( packageDATA );

                        /*if( $this.data('included') )
                        {
                            summary.extraBills.push( packageDATA );
                        } else {
                            summary.packages.push( packageDATA );
                        }*/
                    }
                });

                $extraBills.each(function () {
                    var $this = $( this );
                    var EBData = {
                        name      : $this.data('name'),
                        price     : Math.round($this.data('price')),
                        commission: Math.round($this.data('commission')),
                        netAmount : Math.round($this.data('netAmount')),
                        quantity  : 1
                    }

                    if( data.selectedGuests )
                    {
                        EBData.quantity = data.selectedGuests;

                        EBData.price      = ( EBData.price      * data.selectedGuests );
                        EBData.commission = ( EBData.commission * data.selectedGuests );
                        EBData.netAmount  = ( EBData.netAmount  * data.selectedGuests );
                    }

                    total      += EBData.price;
                    commission += EBData.commission;
                    netAmount  += EBData.netAmount;

                    summary.extraBills.push( EBData );
                });
            }

            /*$addons.each(function () {
                var $this = $( this );

                if( data.selectedGuests == 0 )
                {
                    total      += Math.round($this.data('price'));
                    commission += Math.round($this.data('commission'));
                    netAmount  += Math.round($this.data('netAmount'));
                } else {
                    total      += ( Math.round($this.data('price'))      * data.selectedGuests );
                    commission += ( Math.round($this.data('commission')) * data.selectedGuests );
                    netAmount  += ( Math.round($this.data('netAmount'))  * data.selectedGuests );
                }
            });*/

            total      += data.prices;
            commission += data.commissions;
            netAmount  += data.netAmounts;

// console.log( 'total', total );
// console.log( 'commission', commission );
// console.log( 'netAmount', netAmount );
            if( $agentView.length )
            {
                $agentView.find('div.gross-amount div').text( '$' + total );
                $agentView.find('div.commission-amount div').text( '$' + commission );
                $agentView.find('div.net-amount div').text( '$' + netAmount );
            }

            $( 'input[name="passengers"]' ).val( data.selectedGuests );

            $( '#tour-price' ).text( '$' + $.number( truncate( total, 2 ), 2 ) );
            $( '#tour-price' ).data('price', total);
            $( 'span.total-pax' ).text( data.selectedGuests );
            $flightDetail.find('input#flight-pax').val( data.selectedGuests );

            forDiscount();
            updateSummary( summary );

            // if ( data.selectedGuests == 0 ) { disableFlight( true ); jQuery( 'input[name="includeFlight"]' ).prop('checked', false); }
            // else { departureOnSelect() }
        })(jQuery);
    };

    /**
     * [updateSummary description]
     * @param  {[type]} summary [description]
     * @return {[type]}         [description]
     */
    function updateSummary ( summary )
    {
        if ( LOG_SEQUENCE ) { console.info('updateSummary'); }
        var $wrapper    = jQuery( 'div.book-summary' );

        var $products    = $wrapper.find('div.summary-products div.wrapper');
        var $packages    = $wrapper.find('div.summary-packages div.wrapper');
        var $extraBills  = $wrapper.find('div.summary-extra-bills div.wrapper');
        var $suppliments = $wrapper.find('div.summary-suppliments div.wrapper');
// console.log( 'summary', summary );

        if( summary.extraBills.length )
        {
            $extraBills.parent().show();
            $extraBills.empty();
            jQuery.each(summary.extraBills, function (key, item) {
                $extraBills.append( jQuery( buildSummaryWrapper( item ) ) );
            });
        } else { $extraBills.parent().hide(); }
        if( summary.packages.length )
        {
            $packages.parent().show();
            $packages.empty();
            jQuery.each( summary.packages, function ( key, item ) {
                $packages.append( jQuery( buildSummaryWrapper( item ) ) );
            });
        } else { $packages.parent().hide(); }
        if( summary.products.length )
        {
            $products.parent().show();
            $products.empty();
            jQuery.each(summary.products, function ( key, item ) {
                $products.append( jQuery( buildSummaryWrapper( item ) ) );
            });
        } else { $products.parent().hide(); }
        if( ! jQuery.isEmptyObject( summary.suppliments ) )
        {
            $suppliments.parent().show();
            $suppliments.empty();
            jQuery.each(summary.suppliments, function ( key, item ) {
                $suppliments.append( jQuery( buildSummaryWrapper( item ) ) );
            });
        } else { $suppliments.parent().hide(); }

        // console.log( summary );
    };

    /**
     * [buildSummaryWrapper description]
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    function buildSummaryWrapper ( data )
    {
        return '<div class="col-md-8 info">' + data.name + ' X' + data.quantity + '</div><div class="col-md-4">$' + jQuery.number( truncate( data.price, 2 ), 2 ) + '</div>';
    };

    /**
     * [buildItem description]
     * @param  {[type]} suppliment [description]
     * @param  {[type]} key        [description]
     * @return {[type]}            [description]
     */
    function buildItem ( product, key, suppliments )
    {
        var suppliment = '';
// console.log( 'buildItem', product, suppliments );

        suppliment += '<div class="input-group-addon suppliments">';
        suppliment += '<b class="product-price visible-xs" data-original-price="' + product.grossPrice + '" data-commission="' + product.comPrice + '" data-net-amount="' + product.netAmount + '" data-price="' + Math.round( product.grossPrice ) + '">$' + jQuery.number( Math.round( product.grossPrice ), 2 ) + '</b>';

        if( ! jQuery.isEmptyObject(suppliments) && product.minGuest > 1 )
        {
            var random = Math.random() + '-' + key;
            jQuery.each( suppliments, function ( skey, item ) {
                
                suppliment += '<div class="col-md-12 text-left">';
                    suppliment += '<input disable type="radio" id="' + random + '-' + skey + '" name="' + random + '" class="pull-left" style="margin-top: 5px;" data-commission="' + item.comPrice + '" data-net-amount="' + Math.round( item.netAmount ) + '" data-price="' + Math.round( item.grossPrice ) + '" data-item-key="' + skey + '" data-name="' + item.sub_supplement_avail_name + '"/>';
                    suppliment += '<span>&nbsp;</span>';
                    suppliment += '<label for="' + random + '-' + skey + '" style="font-weight: 500; white-space: pre-wrap; line-height: 20px; text-decoration: line-through;">' + item.sub_supplement_avail_name + ' <strong>($' + jQuery.number( Math.round( item.grossPrice ), 2 ) + ')</strong></label>';
                suppliment += '</div>';
            });
        }
        suppliment += '</div>';

        var template = ''
        +'<div class="col-md-12" data-product-key="' + key + '">'
        +    '<div class="input-group">'
        +        '<span class="input-group-addon">'
        +            '<select class="product-guest" data-min-guest="' + product.minGuest + '" data-event-trigger="false" data-has-suppliment="' + suppliments.length + '">'
        +                generateOptions( product.minGuest, (! jQuery.isEmptyObject(suppliments)) )
        +            '</select>'
        +            '<!-- <span class="fa fa-ban not-selectable" style="font-size: 2.5em; color: red;"></span> -->'
        +        '</span>'
        +        '<input type="text" class="form-control" value="" style="padding-left: 0px !important; padding-right: 0px !important; width: 0px; visibility: hidden;"/>'
        +        '<div class="input-group-addon product-name">'
        +           '<span class="hidden-xs">' + product.supplement_avail_name + '</span><span class="visible-xs-inline"><b>' + product.minGuest + '</b> <span class="fa fa-user"></span></span>'
        +        '</div>'
        +         suppliment
        +        '<div class="input-group-addon hidden-xs">'
        +            '<b class="product-price" data-original-price="' + product.grossPrice + '" data-commission="' + product.comPrice + '" data-net-amount="' + product.netAmount + '" data-price="' + product.grossPrice + '">$' + jQuery.number( Math.round( product.grossPrice ), 2 ) + '</b>'
        +        '</div>'
        +    '</div>'
        +'</div>';

        return template;
    };

    /**
     * [adddSuppliment description]
     * @param  {[type]} key        [description]
     * @param  {[type]} suppliment [description]
     * @return {[type]}            [description]
     */
    function adddSuppliment ( key, suppliment )
    {
        var $supplimentsWrapper = jQuery( 'div.book-suppliment' );
        var $wrapper            = jQuery( '<div></div>' );
        $wrapper.data('itemKey', key);

        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][name]"               value="' + suppliment.sub_supplement_avail_name + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][OriginalBasePrice]"  value="' + suppliment.OriginalBasePrice + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][convertedBasePrice]" value="' + suppliment.convertedBasePrice + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][fromCurrency]"       value="' + suppliment.sub_fromCurrency + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][toCurrency]"         value="' + suppliment.sub_toCurrency + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][convertedRate]"      value="' + suppliment.convertedBasePrice + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][comPrice]"           value="' + suppliment.comPrice + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][comm_pct_amount]"    value="' + suppliment.comm_pct_amount + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][grossPrice]"         value="' + suppliment.grossPrice + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][markup_pct_amount]"  value="' + suppliment.markup_pct_amount + '"/>' ) );
        $wrapper.append( jQuery( '<input type="hidden" name="suppliments[' + key + '][netAmount]"          value="' + suppliment.netAmount + '"/>' ) );
        
        $supplimentsWrapper.append( $wrapper );
    };

    /**
     * [generateOptions description]
     * @return {[type]} [description]
     */
    function generateOptions ( minGuest, hasSuppliments )
    {
        var options    = '<option value="0" selected>0</option>';

        for( var i=1; i<=15; i++  )
        {
            var isDisabled = '';
            // if( (! hasSuppliments && ((i % minGuest) != 0)) || ( i < minGuest && i != 1 ) )
            if( (! hasSuppliments && ((i % minGuest) != 0)) )
            {
                isDisabled = 'disabled';
            }

            options += '<option value="' + i + '" ' + isDisabled + ' >' + i + '</option>';
        }

        return options;
    };

    /**
     * [numbersToWord description]
     * @param  {[type]} number [description]
     * @return {[type]}        [description]
     */
    function numbersToWord ( number )
    {
        var word = '';
        switch( number )
        {
            case 1: word = 'Single'; break;
            case 2: word = 'Double'; break;
            case 3: word = 'Tripple'; break;
        }

        return word;
    };

    /**
     * [truncate description]
     * @param  {[type]} num    [description]
     * @param  {[type]} places [description]
     * @return {[type]}        [description]
     */
    function truncate (num, places) {
        if ( num > 0 )
        {
            return Math.floor(num * Math.pow(10, places)) / Math.pow(10, places);
        }
        return Math.ceil(num * Math.pow(10, places)) / Math.pow(10, places);
    }


    </script>

    <script type="text/javascript">
        (function($) {
            console.info('Responsive');

            var $calendar           = $('#calendarHolder');
            var $productBriefInfo   = $( 'div.product-brief-info' );
            var $FABInfo            = $( 'div.fab-info' );
            var $FABUp              = $( 'div.fab-up' );
            var $container          = $( 'section.product-detail.external-tours' );
            var $tourProductWrapper = $container.find('div.tour-product-wrapper');

            var width  = $( window ).width();
            var height = $( window ).height();

            
            $( window ).resize(function () {
                
                width  = $( window ).width();
                height = $( window ).height();


                console.log('Resizing', width);
                if( width <= 900 )
                {
                    $FABInfo.show();
                    // $FABUp.show();
                    $( window ).scroll(function (  ) {
                        hideBacktoTop( ($( window ).scrollTop() == 0) );
                    });

                    $productBriefInfo.css({
                        'z-index'   : 999,
                        'left'      : '0px',
                        'top'       : '100%',
                        'background': '#FFF',
                        'position'  : 'fixed',
                        'width'     : '100%',
                        'height'    : height + 'px',
                        'overflow-y': 'auto'
                    });
                }

                if( width > 900 )
                {
                    $FABInfo.hide();
                    $FABUp.hide();

                    $productBriefInfo.css({
                        'position'  : 'relative',
                        'z-index'   : '',
                        'left'      : '',
                        'top'       : '',
                        'background': '',
                        'width'     : '',
                        'height'    : '',
                        'overflow-y': ''
                    });
                    // $( 'div.items-lists' ).css('padding', '');
                    // console.log( $( 'div.items-lists' ) );
                }

                if( width  <= 600 )
                {
                    changeNumberOfMonths( 1 );

                    $container.css('padding-top', '0px');
                    $container.find('div.container').css('padding', '0px');

                    $tourProductWrapper.css('width', width);

                    $calendar.addClass('adjust');
                }

                if( width > 600 )
                {
                    changeNumberOfMonths( 2 );

                    $container.css('padding-top', '');
                    $container.find('div.container').css('padding', '20px');

                    $tourProductWrapper.css('width', '');

                    $calendar.removeClass('adjust');
                    
                }
                updateDatePickerCells();
            });

            $FABInfo.click(function () {
                $productBriefInfo.animate({'top': ( $productBriefInfo.data('show')?'100%':'0' )}, 250 );
                $productBriefInfo.data('show', !$productBriefInfo.data('show') );
            });
            $FABUp.click(function () {
                $('html, body').animate({scrollTop: 0}, 250);
            });

            initMobilize();

            function initMobilize ()
            {
                console.info('Initializing Mobile');
                $FABUp.hide();
                $FABInfo.hide();
                $FABInfo.data('show', false);
                
                setTimeout(function () {
                    $( window ).resize();
                    $( window ).scroll();
                }, 100);
            };

            /**
             * [hideBacktoTop description]
             * @param  {Boolean} isHide [description]
             * @return {[type]}         [description]
             */
            function hideBacktoTop ( isHide )
            {
                isHide = isHide || true;
                var bottom = $FABInfo.css('bottom');

                if ( isHide )
                {
                    if( bottom != '20px' )
                    {
                        setTimeout(function () {
                            $FABInfo.animate({ 'bottom': '20px' }, 250);
                        }, 0);
                    }
                    
                    $FABUp.hide();
                } else {
                    if( bottom != '80px' )
                    {
                        $FABInfo.animate({ 'bottom': '80px' }, 250);
                    }
                    
                    $FABUp.show();
                }
            };

            /**
             * [changeNumberOfMonths description]
             * @param  {[type]} months [description]
             * @return {[type]}        [description]
             */
            function changeNumberOfMonths ( months )
            {
                $calendar.datepicker('option', 'numberOfMonths', months);
            };

            /**
             * [updateDatePickerCells description]
             * @param  {[type]} dp [description]
             * @return {[type]}    [description]
             */
            function updateDatePickerCells (  ) {
                setTimeout(function () {
                    $('.ui-datepicker tbody tr td > *').each(function (idx, elem) {
                        $( "<div style=\"z-index:10; position:absolute;font-size:11px;font-weight:bold;margin:-23px 0 2px 2px;color:#f44336;\"></div>" ).insertAfter(elem);
                        $( "<span style=\"z-index:10;padding: 0;  position:absolute;margin:-35px 0 2px 1px;width: 0;height:0;border-style: solid;border-width: 12px 12px 0 0;border-color: #f44336 transparent transparent transparent;\"></span>" ).insertAfter(elem);

                        //width: 0;height: 0;border-style: solid;border-width: 12px 12px 0 0;border-color: #007bff transparent transparent transparent;
                        //$(this).insertAfter('<div style="z-index:9999;position:absolute;font-size:10px;font-weight:bold;margin-top:-20px"></div>');
                    });
                }, 0);
            }


        })(jQuery)
    </script>

    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <?php TZ_custom_theme_script( $this->nativesession->get('agencyRealCode') ) ?>
    <?php endif; ?>
</body>
</html>