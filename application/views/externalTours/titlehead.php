<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    
    <!-- TITLE -->
    <title><?=_AGENT_INFO( $this->nativesession->get('agencyRealCode') )['agency_name'] ?> Tours</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <?php
    switch (strtolower($this->nativesession->get('agencyRealCode'))) {
      case 'europetraveldeals':
        ?><link rel="icon" href="/EH-favicon.ico" type="image/x-icon" /><?php
        break;
      
      default:
        # code...
        break;
    }
    ?>
    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:700,600,400,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oswald:400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/awe-booking-font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/jquery-ui.css">

    <!-- MAIN STYLE -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/style.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>static/css/demo.css">

    
    <!-- CSS COLOR -->
    <?php //print_r($agency);die();?>
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/blue.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/dashboard-style.css">



    <link rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/bootstrap-dialog.min.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/waitMe.min.css">
    <?php TZ_includes($this->nativesession->get('agencyRealCode'),'styles') ?>
    <style>

  .awe-navigation .menu-list li:hover > .sub-menu {
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
  }
  .awe-navigation .menu-list li .sub-menu li {
    background-color: #ffffff;
    border-bottom: 1px solid #dfdede;
  }
  .awe-navigation .menu-list li .sub-menu li:last-child {
    border-bottom: 0px;
  }
  .awe-navigation .menu-list li > .sub-menu a {
    color: #625e5e;
    border-bottom: 0px !important;
  }
  .awe-navigation .menu-list li > .sub-menu li:hover {
    background-color: #dfdede;
  }
  /* .awe-navigation .menu-list li a,
  .awe-navigation .menu-list li > .sub-menu > li:hover > a {
    color: #625e5e;
  }
  .awe-navigation .menu-list li > .sub-menu > li:hover > a {
    border-bottom: 1px solid #000;
  }
  .awe-navigation .menu-list li .sub-menu > li:last-child:hover a{
    border-bottom: 0px;
  } */
  .term-and-condition .bootstrap-dialog-footer-buttons button {
    margin-top: 0px;
  }

  .term-and-condition div.modal-dialog {
    top: 70px;
  }

    
    .bs-callout {
        padding: 20px;
        margin: 20px 0;
        border: 1px solid #eee;
        border-left-width: 5px;
        border-radius: 3px;
    }
    .bs-callout h4 {
        margin-top: 0;
        margin-bottom: 5px;
    }
    .bs-callout p:last-child {
        margin-bottom: 0;
    }
    .bs-callout code {
        border-radius: 3px;
    }
    .bs-callout+.bs-callout {
        margin-top: -5px;
    }
    .bs-callout-default {
        border-left-color: #777;
    }
    .bs-callout-default h4 {
        color: #777;
    }
    .bs-callout-primary {
        border-left-color: #428bca;
    }
    .bs-callout-primary h4 {
        color: #428bca;
    }
    .bs-callout-success {
        border-left-color: #5cb85c;
    }
    .bs-callout-success h4 {
        color: #5cb85c;
    }
    .bs-callout-danger {
        border-left-color: #d9534f;
    }
    .bs-callout-danger h4 {
        color: #d9534f;
    }
    .bs-callout-warning {
        border-left-color: #f0ad4e;
    }
    .bs-callout-warning h4 {
        color: #f0ad4e;
    }
    .bs-callout-info {
        border-left-color: #5bc0de;
    }
    .bs-callout-info h4 {
        color: #5bc0de;
    }
    .header, h5, h3, h4, h6 {
        font-family: "Arial";
    }
    
    .passengers label, .fligh-details label {
        color: #000;
    }
    .passengers input, .passengers select,
    .fligh-details input, .fligh-details select {
        color : #2d2525;
        border: 1px solid #d4d4d4 !important;
    }


    .adjust-bootstrap-dialog .modal-dialog {
        top: 50px;
    }
    /*  .ui-widget-header{
       background: none;
       border: none;
        }
        .ui-datepicker{
       border: none;
      font-size: 20px;
      width: 49em !important;
        
        }
        
        .ui-datepicker td{
        padding:0;
        margin: 0;
        
        
        } */

    div#calendarHolder .ui-datepicker-unselectable.ui-state-disabled span.ui-state-default {
      padding-right: 10px;
    }

    div#calendarHolder td.ui-datepicker-days-cell-over a.ui-state-default,
    div#calendarHolder td.ui-datepicker-week-end a.ui-state-default, {
      padding-right: 10px !important;
      position     : relative !important;
      line-height  : 16px !important;
      top          : -3px !important;
      text-align   : right !important;

    }
    
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{
        border:1px solid #f2f2f2;
    }
    .ui-datepicker td.ui-state-disabled>span{
       
    
    }
    .ui-datepicker td.ui-state-disabled{
        opacity: 100;
    }
    
    .ui-datepicker td span{
       width: 46px;
       height: 36px;
       line-height:5px;
    }
      .ui-datepicker td a{
        text-align: right;
         
        font-size: 8px;
         width: 46px;
       height: 36px;
       line-height:5px;
       color: #007bff !important;
    
    
    }
    .ui-state-default{background: none !important;
    
    }
    .ui-datepicker td span{
        text-align: right;
        
        font-size: 8px;
         width: 46px;
       height: 36px;
       line-height:5px;
       color: #000 !important;
    
    
    }
    .ui-datepicker th{
        font-weight: normal;
    }
    .ui-datepicker td a{
        color: #007bff !important;
        font-weight: bold !important;
       
    } 


    .booking-calendar.ui-datepicker {
      width             : 300px !important;
      height            : auto;
      margin            : 5px auto 0;
      font              : 1.2em Arial, sans-serif;
      -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
      -moz-box-shadow   : 0px 0px 10px 0px rgba(0, 0, 0, .5);
      box-shadow        : 0px 0px 10px 0px rgba(0, 0, 0, .5);
    }
    .booking-calendar.ui-datepicker td span, .ui-datepicker td a {
  display    : inline-block !important;
  font-weight: bold !important;
  text-align : center !important;
  /* width      : 30px !important;
  height     : 30px !important;
  line-height: 30px !important; */
  color      : #666666 !important;
  text-shadow: 1px 1px 0px #fff !important;
  filter     : dropshadow(color=#fff, offx=1, offy=1) !important;
}
.booking-calendar .ui-datepicker-calendar .ui-state-default {
    font-size: 1em !important;
  background        : #ededed !important;
  background        : -moz-linear-gradient(top,  #ededed 0%, #dedede 100%) !important;
  background        : -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ededed), color-stop(100%,#dedede)) !important;
  background        : -webkit-linear-gradient(top,  #ededed 0%,#dedede 100%) !important;
  background        : -o-linear-gradient(top,  #ededed 0%,#dedede 100%) !important;
  background        : -ms-linear-gradient(top,  #ededed 0%,#dedede 100%) !important;
  background        : linear-gradient(top,  #ededed 0%,#dedede 100%) !important;
  filter            : progid:DXImageTransform.Microsoft.gradient( startColorstr='#ededed', endColorstr='#dedede',GradientType=0 ) !important;
  -webkit-box-shadow: inset 1px 1px 0px 0px rgba(250, 250, 250, .5) !important;
  -moz-box-shadow   : inset 1px 1px 0px 0px rgba(250, 250, 250, .5) !important;
  box-shadow        : inset 1px 1px 0px 0px rgba(250, 250, 250, .5) !important;
}
.booking-calendar .ui-datepicker-unselectable .ui-state-default {
  background: #f4f4f4 !important;
  color     : #b4b3b3 !important;
}
.booking-calendar .ui-datepicker-calendar .ui-state-hover {
  background: rgba(110, 175, 191, 0.6) !important;
}

    .booking-calendar .ui-datepicker-calendar .ui-state-active {
  font-size: 1.3em;
  background        : #6eafbf !important;
  -webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1) !important;
  -moz-box-shadow   : inset 0px 0px 10px 0px rgba(0, 0, 0, .1) !important;
  box-shadow        : inset 0px 0px 10px 0px rgba(0, 0, 0, .1) !important;
  color             : #e0e0e0 !important;
  text-shadow       : 0px 1px 0px #4d7a85 !important;
  filter            : dropshadow(color=#4d7a85, offx=0, offy=1) !important;
  border            : 1px solid #55838f !important;
  position          : relative !important;
  margin            : -1px !important;
}

div#calendarHolder.adjust .ui-datepicker {
  margin: 0px;
  width: 100% !important;
}

div.floating-button.floating-primary button {
    background-color: #2aabd2;
}

div.floating-button.floating-warning button {
    background-color: #e38d13;
}

div.floating-button.floating-success button {
    background-color: #419641;
}

div.floating-button {
    position: fixed;
    z-index : 9999;
}
div.floating-button span {
    font-size: 0.7em;
}
div.floating-button > button {
    background-color           : #F44336;
    width                      : 50px;
    height                     : 50px;
    line-height                : 10px;
    border-radius              : 100%;
    border                     : none;
    outline                    : none;
    color                      : #FFF;
    font-size                  : 36px;
    box-shadow                 :  0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    transition                 : .3s;  
    -webkit-tap-highlight-color:  rgba(0,0,0,0);
}

.product-footer.col-md-12,
.product-header.col-md-12,
.product-body .col-md-12 {
  padding-left: 0px;
  padding-right: 15px;
}
@media only screen
 and ( min-device-width : 401px )
 and ( max-device-width: 901px )
 {
  div.items-lists {
    padding: 0px !important;
  }
  .hamburger .item {
    background-color: #FFF !important;
  }
  #calendarHolder {
    z-index: 9;
  }
  /* .ui-datepicker {
    width: 100% !important;
    margin: 0px;
  } */
  .product-addons-list.col-md-6 {
    position: unset;
  }
 }

 @media only screen
 and ( max-device-width : 540px )
 {
  div.items-lists {
    overflow: hidden;
    overflow-x: auto;
  }
 }
    </style>
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/jquery.ui.theme.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/jquery.ui.1.10.0.ie.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/theme/theme-<?php echo @$agency['themes']?>.css">
    <!-- <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/ /stylesheets/theme-2.css"> -->
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/css/material-buttons.css">
    <link id="colorreplace" rel="stylesheet" type="text/css" href="<?=base_url()?>static/compass/stylesheets/external-tours.css">
    <!-- CUSTOM CSS <?=json_encode(TZ_custom_theme( $this->nativesession->get('agencyRealCode') )) ?> -->

    <?php if ( TZ_custom_theme( $this->nativesession->get('agencyRealCode') ) ): ?>
        <?php TZ_custom_theme_style( $this->nativesession->get('agencyRealCode') ) ?>
    <?php endif; ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TPN294J');</script>
    <!-- End Google Tag Manager -->

</head>

<!--[if IE 7]> <body class="ie7 lt-ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 8]> <body class="ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]> <body class="ie9 lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TPN294J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- PAGE WRAP -->
    <div id="page-wrap">
   