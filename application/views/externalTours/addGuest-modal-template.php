<!--=============================
=            Product            =
==============================-->

<div class="row products">
	<h6>Product</h6>
	<div class="col-xs-12">
		<div class="form-horizontal tours-info">
			<div class="form-group">
				<label for="" class="control-label col-sm-4"><span class="fa fa-calendar-check-o"></span> Tours starts</label>
				<div class="col-sm-8">
					<p class="form-control-static"><?=$_POST['products']['dates']['start'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-4"><span class="fa fa-calendar-times-o"></span> Tours ends</label>
				<div class="col-sm-8">
					<p class="form-control-static"><?=$_POST['products']['dates']['end'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-4"><span class="fa fa-calendar"></span> Tours duration</label>
				<div class="col-sm-8">
					<p class="form-control-static"><?=$_POST['products']['dates']['duration'] ?> days</p>
				</div>
			</div>
		</div>
		<hr>
		<div class="form-horizontal products-info">
			<?php $items = $_POST['products']['products']; ?>

			<p class="border-bottom"><strong>PRODUCTS</strong></p>
			<?php foreach ( $items['products'] as $key => $product): ?>
				<div class="form-group">
					<label for="" class="control-label col-sm-4">
						<span class="fa fa-check"></span> <?=$product['name'] ?> X<?=$product['quantity'] ?>

						<?php if ( isset( $product['supplement'] ) ): ?>
							<?php $supplement = $product['supplement']; ?>
							<br>
							Supplement: <?=$supplement['name'] ?> X<?=$supplement['quantity'] ?> ( <span class="fa fa-usd"></span><?=number_format($supplement['price'], 2) ?> )
						<?php endif ?>
					</label>
					<div class="col-sm-8">
						<p class="form-control-static"><strong class="hidden-xs">: </strong><span class="fa fa-usd"></span><?=number_format($product['subtotal'], 2) ?></p>
					</div>
				</div>
			<?php endforeach ?>

			<?php if ( isset( $items['packages'] ) ): ?>
				<br>
				<p class="border-bottom"><strong>PACKAGES</strong></p>
				<?php foreach ( $items['packages'] as $key => $package): ?>
					<div class="form-group">
						<label for="" class="control-label col-sm-4"><span class="fa fa-check"></span> <?=$package['name'] ?> X<?=$package['quantity'] ?></label>
						<div class="col-sm-8">
							<p class="form-control-static"><strong class="hidden-xs">: </strong><span class="fa fa-usd"></span><?=number_format($package['subtotal'], 2) ?></p>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>

			<?php if ( isset( $items['extrabills'] ) ): ?>
				<br>
				<p class="border-bottom"><strong>EXTRA BILLS</strong></p>
				<?php foreach ( $items['extrabills'] as $key => $extrabill): ?>
					<div class="form-group">
						<label for="" class="control-label col-sm-4"><span class="fa fa-check"></span> <?=$extrabill['name'] ?> X<?=$extrabill['quantity'] ?></label>
						<div class="col-sm-8">
							<p class="form-control-static"><strong class="hidden-xs">: </strong><span class="fa fa-usd"></span><?=number_format($extrabill['subtotal'], 2) ?></p>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		</div>
		<hr>
		
		<p><strong>Number of passengers: <?=$_POST['products']['passengers'] ?></strong></p>
		
		<?php if ( filter_var($_POST['products']['isAgent'], FILTER_VALIDATE_BOOLEAN) ): ?>
			<div class="form-horizontal">
				<div class="form-group">
					<label for="" class="control-label col-sm-4">Gross amount</label>
					<div class="col-sm-8">
						<p class="form-control-static"><?=$_POST['products']['agency']['gross'] ?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-sm-4">Commission amount</label>
					<div class="col-sm-8">
						<p class="form-control-static"><?=$_POST['products']['agency']['commission'] ?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-sm-4">Due amount</label>
					<div class="col-sm-8">
						<p class="form-control-static"><?=$_POST['products']['agency']['due'] ?></p>
					</div>
				</div>
			</div>
		<?php endif ?>
		
		<?php if ( filter_var($_POST['products']['discounted'], FILTER_VALIDATE_BOOLEAN) ): ?>
		<p><strong><?=$_POST['products']['discount']['message'] ?></strong></p>
		<?php endif ?>
		
		<?php if ( filter_var($_POST['products']['hasFlight'], FILTER_VALIDATE_BOOLEAN) ): ?>
		<p><strong><?=$_POST['products']['flight']['name'] ?> (<?=number_format(($_POST['products']['flight']['price']/$_POST['products']['flight']['quantity']), 2) ?> X <?=$_POST['products']['flight']['quantity'] ?> = $<?=number_format($_POST['products']['flight']['price'], 2) ?>)</strong></p>
		<?php endif ?>
		
		<div class="col-xs-12 text-center booking-price">
			<small>Total for this booking</small>
			<?php if ( filter_var($_POST['products']['discounted'], FILTER_VALIDATE_BOOLEAN) ): ?>
                <h5><del><span class="fa fa-usd"></span><?=number_format($_POST['products']['prices']['before'], 2) ?></del></h5>
            <?php endif ?>
			<h4>TOTAL: <span class="fa fa-usd"></span><?=number_format($_POST['products']['prices'][(filter_var($_POST['products']['discounted'], FILTER_VALIDATE_BOOLEAN)?'after':'before')], 2) ?></h4>
		</div>
	</div>
</div>

<!--====  End of Product  ====-->


<!--================================
=            Passengers            =
=================================-->

<div class="row">
	<h6>Passengers</h6>
	<div class="form-horizontal">
		<?php foreach ( $_POST['passengers'] as $key => $passenger ): ?>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Guest <?=($key+1) ?></label>
			<div class="col-sm-9">
				<p class="form-control-static"><?=$passenger['title'] ?>. <?=$passenger['firstname'] ?> <?=$passenger['lastname'] ?></p>
			</div>
		</div>
		<?php endforeach ?>
	</div>
</div>

<!--====  End of Passengers  ====-->

<!--=============================
=            Voucher            =
==============================-->

<div class="row">
	<h6>Voucher</h6>
	<div class="form-horizontal">
		<?php $voucher = $_POST['voucher'] ?>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Name</label>
			<div class="col-sm-9">
				<p class="form-control-static"><?=$voucher['title'] ?>. <?=$voucher['firstname'] ?> <?=$voucher['lastname'] ?></p>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Email</label>
			<div class="col-sm-9">
				<p class="form-control-static"><?=$voucher['email'] ?></p>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Mobile #</label>
			<div class="col-sm-9">
				<p class="form-control-static"><?=$voucher['phonenum'] ?></p>
			</div>
		</div>
	</div>
</div>

<!--====  End of Voucher  ====-->

<?php if ( isset($_POST['payment']) ): ?>
<!--====================================
=            Payment Method            =
=====================================-->
<div class="row">
	<h6>Payment</h6>
	<div class="form-horizontal">
<?php
	$payment = $_POST['payment'];
	switch ( trim(strtolower($_POST['payment_type'])) ) {
		case 'creditcard':
			?>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Category</label>
				<div class="col-sm-9">
					<p class="form-control-static">Credit Card</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Holder name</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['nm_card_holder'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Card number</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['no_credit_card'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Verification #</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['no_cvn'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Expiry Date</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['dt_expiry_month'] ?>/<?=$payment['dt_expiry_year'] ?></p>
				</div>
			</div>
			<?php
			
			break;

		case 'paypal':
			?>
			<div class="form-group">
				<div class="col-xs-12">
					<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/2000px-PayPal.svg.png" alt="PayPal" class="img-responsive">
				</div>
			</div>
			<?php
			break;

		case 'bankdeposit':
			?>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Category</label>
				<div class="col-sm-9">
					<p class="form-control-static">Bank Deposit</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Name</label>
				<div class="col-sm-9">
					<p class="form-control-static">Select World Travel Westpac BSB 034-216</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">ACCN</label>
				<div class="col-sm-9">
					<p class="form-control-static">261 424</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">SWIFT</label>
				<div class="col-sm-9">
					<p class="form-control-static">WPACAU2S</p>
				</div>
			</div>
			<?php
			break;

		case 'e-nett':
			?>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Category</label>
				<div class="col-sm-9">
					<p class="form-control-static">E-Nett</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Date</label>
				<div class="col-sm-9">
					<p class="form-control-static"><?=$payment['date'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Reference</label>
				<div class="col-sm-9">
					<p class="form-control-static"><?=$payment['reference'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Amount</label>
				<div class="col-sm-9">
					<p class="form-control-static"><?=$payment['amount'] ?></p>
				</div>
			</div>
			<?php
			break;
		
		default:
			# code...
			break;
	}
?>
	</div>
</div>

<!--====  End of Payment Method  ====-->
<?php endif ?>