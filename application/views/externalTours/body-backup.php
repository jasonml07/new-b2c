<div style="bottom: 80px; right: 20px;" class="fab-info floating-button floating-primary"><button type="button"><span class="fa fa-shopping-cart"></span></button></div>
<div style="bottom: 20px; right: 20px;" class="fab-up floating-button floating-primary"><button type="button"><span class="fa fa-arrow-up"></span></button></div>
<form action="<?=base_url(); ?>externalToursAddGuest" method="POST">
 <section class="product-detail external-tours" style="background-color: #f2f2f2; padding-bottom: 0px;">

            <div class="container" style="background-color: #fff; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
                
                    <div class="row">
                        <div class="col-md-9 col-sm-12 tour-product-wrapper">
                    
                            <div class="product-detail__info " style="margin-top: 10px;">
                                 <div class="product-title woocommerce-billing-fields">
                                    <div>
                                            <h3>
                                                    <?=$product['productName'];?>
                                            </h3>
                                    </div>
                                </div>
                                 <div class="product-address" style="margin-top: 10px;">
                                    <h5 style="color: #262626;"><?=$product['productFromAddress'];?></h5>
                                </div>
                                <?php if( ! empty( $product['productSumarryDescription'] ) ) :?>
                                 <div>
                                     <?=$product['productSumarryDescription'];?>
                                 </div>
                                <?php endif; ?>
                            </div>
                    
                        <div class="product-tabs tabs" style="margin-top: 0px;">
                                <ul>
                                    <li>
                                        <a href="#tabs-1" style="color: #000;">Calendar</a>
                                    </li>
                                    <li>
                                        <a href="#tabs-2"  style="color: #000;">List</a>
                                    </li>
                                    
                                </ul>

                                     <div id="tabs-1" style=" background-color: #f2f2f2; padding-top: 30px;">
                    
                                         <div id="calendarHolder" class="col-md-12 col-sm-12" style="background-color: #f2f2f2; padding-bottom: 50px;" >
                                            </div>

                                    </div>




                                     <div id="tabs-2">
                                        <div class="col-md-12 list-type-view-product" style="padding-left: 0px; padding-right: 0px; overflow: hidden; overflow-x: auto;">
                                            <div class="input-group">
                                                <span class="input-group-addon" style="background-color: transparent; min-width: 180px; width: 100%; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;"><strong>TOUR STARTS</strong></span>
                                                <span class="input-group-addon" style="background-color: transparent; min-width: 180px; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;"><strong>TOUR ENDS</strong></span>
                                                <span class="input-group-addon" style="background-color: transparent; min-width: 100px; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;"><strong>PRICE</strong></span>
                                                <span class="input-group-addon hidden-xs" style="background-color: transparent; min-width: 100px; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;"></span>
                                                <input type="text" class="form-control" style="padding-left: 0px !important; padding-right: 0px !important; width: 0px; visibility: hidden;">
                                            </div>
                                            <?php foreach($product['productAvailabilityFinal'] as $key => $val): ?>
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="background-color: transparent; min-width: 180px; width: 100%; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;"><?=$val['fromDayText']; ?></span>
                                                    <span class="input-group-addon" style="background-color: transparent; min-width: 180px; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;"><?=$val['toDayText']; ?></span>
                                                    <span class="input-group-addon" style="background-color: transparent; min-width: 100px; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;">
                                                        <span style="font-size: 1.2em;">$<?=$val['displayPrice']; ?></span>
                                                        <br class="visible-xs-block">
                                                        <a style="margin-top: 10px;" class="btn btn-warning visible-xs-block glyphicon glyphicon-ok list-content-<?=$val['fromMonth']; ?><?=$val['fromDay']; ?><?=$val['fromYear']; ?>"></a>
                                                    </span>
                                                    <span class="input-group-addon hidden-xs" style="background-color: transparent; min-width: 100px; text-align: left; border-bottom-left-radius: 0px; left: 3px; border-right: 1px solid #ccc;">
                                                        <a  class="btn btn-danger list-content-<?=$val['fromMonth']; ?><?=$val['fromDay']; ?><?=$val['fromYear']; ?>">Select</a>
                                                    </span>
                                                    <input type="text" class="form-control" style="padding-left: 0px !important; padding-right: 0px !important; width: 0px; visibility: hidden;">
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                         <!-- <table class="facilities-freebies-table">
                                            <thead>
                                                <tr>
                                                    <th class="room-type">Tour Starts</th>
                                                    <th class="room-type">Tour Ends</th>
                                                   
                                                    <th class="room-price">Price</th>
                                                    <th class="room-condition">&nbsp;</th>
                                                    <th class="room-price"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($product['productAvailabilityFinal'] as $key => $val): ?>
                                                <tr>
                                                    <td class="room-type">
                                                        <?=$val['fromDayText']; ?>
                                                    </td>
                                                    <td class="room-type">
                                                        <?=$val['toDayText']; ?>
                                                    </td>
                                                    <td class="room-price">
                                                        <span class="amount">$<?=$val['displayPrice']; ?></span>
                                                    </td>
                                                    <td class="room-condition"></td>
                                                     <td class="room-price">
                                                    <a  class="btn btn-warning" id="list-content-<?=$val['fromMonth']; ?><?=$val['fromDay']; ?><?=$val['fromYear']; ?>" >Select</a>
                                                       </td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                                                                 </table> -->
                                     </div>

                                    
                                                <div class="col-md-12 product-content" style="visibility: hidden; display: none; border-top: 3px solid #ccc; margin-top: 30px; background-color: #f2f2f2; padding-bottom: 30px;  border-bottom-left-radius: 0px;">
                                                    <!-- <div class = "col-md-6"  style="padding-left: 0px;">
                                                    
                                                             <div class="row" style="border">
                                                                    <div class="woocommerce-billing-fields col-md-12">
                                                                       
                                                                        <div  class="product" style="margin-left: 5px;">
                                                    
                                                                            SINGLE PRICE
                                                                            <h5   id="side-t-single-price-tours" style="margin-top: 40px;">    </h5><br/>
                                                    
                                                                            <div class="clearfix"></div>
                                                    
                                                                            <h4 style="font-size: 18px; margin-top: 30px;">Number of Travelers</h4>   
                                                                                                                                                       
                                                                            <select class="form-control" name="pax" style="margin-left: 15px;">
                                                                               <option>1</option>
                                                                               <option selected="selected">2</option>
                                                                               <option>3</option>
                                                                               <option>4</option>
                                                                               <option>5</option>
                                                                               <option>6</option>
                                                                               <option>7</option>
                                                                               <option>8</option>
                                                                               <option>9</option>
                                                                               <option>10</option>
                                                                                                                                                       </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </div> -->

                                        
                                                    <!-- <div class = "col-md-6  woocommerce-billing-fields pax" style="margin-top: 50px; visibility: hidden;" >
                                                        ADD ON
                                                        <h5   id="side-t-multiple_supplements">   </h5>
                                                        <span id="side-t-supplement_name2">      </span>
                                                                                            
                                                    </div> -->
                                                    <div class="col-md-6 product-addons-list">
                                                        <!-- PRODDUCT -->
                                                        <h5   id="side-t-sub_product_price_list"></h5>
                                                        <span id="side-t-supplement_name1">      </span><br>
                                                    </div>
                                                    <div class="col-md-6 extra-bill">
                                                        <!-- EXTRA -->
                                                        <h5>Extra Bills:</h5>
                                                        <div class="group"></div>
                                                    </div>
                                        
                                                    <div class="items-lists col-md-12  woocommerce-billing-fields pax" style=" margin-top: 50px; visibility: hidden;" >
                                                        <div class="alert alert-info" role="alert">
                                                            <strong><span class="fa fa-info-circle"></span> NOTE: </strong>
                                                            Please review your selection upon booking
                                                        </div>
                                                        <h5 id="side-t-supplement">            </h5>
                                                        <div id="side-t-supplement_name3" class="row">
                                                            <div class="col-md-12 product-header">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <!-- <span class="fa fa-users"></span> -->
                                                                        <strong># of PASSENGERS</strong>
                                                                    </span>
                                                                    <input type="text" class="form-control hide-input" value=""/>
                                                                    <!-- <div class="input-group-addon" style="border-left: 3px solid #CCC; min-width: 120px; max-width: 120px;">
                                                                       <strong># of PAX/<br>PRICE</strong>
                                                                    </div> -->
                                                                    <div class="input-group-addon">
                                                                       <strong>ROOM / CABIN OCCUPANCY</strong>
                                                                    </div>
                                                                    <div class="input-group-addon">
                                                                       <strong>SUPPLEMENTS</strong>
                                                                    </div>
                                                                    <div class="input-group-addon hidden-xs">
                                                                        <strong>PRICE/PERSON</strong>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="product-body">
                                                                
                                                            </div>
                                                            <div class="col-md-12 product-footer">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-user"></span>&nbsp;<span class="total-guests">0</span><!-- /<span class="max-guests">0</span>-->
                                                                    </span>
                                                                    <input type="text" class="form-control hide-input" value=""/>
                                                                    <!-- <div class="input-group-addon" style="border-left: 3px solid #CCC; min-width: 120px; max-width: 120px;">
                                                                    </div> -->
                                                                    <div class="input-group-addon">
                                                                        <strong class="visible-xs-block">TOTAL: <span class="pull-right">$<span class="total-price">0</span></span></strong>
                                                                    </div>
                                                                    <div class="input-group-addon hidden-xs">
                                                                        <strong>TOTAL: <span class="pull-right">$<span class="total-price">0</span></span></strong>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--=============================
                                                    =            FLIGHTS            =
                                                    ==============================-->
                                                    <div class="clearfix"></div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <div class="form-group flight-details-option" style="color: #000; height: 20px;">
                                                            <label for="" class="control-label col-sm-4" style="padding-left: 0px;">Include Flight Details? <span style="color: red;">*</span></label>
                                                            <div class="col-sm-8">
                                                                <label for="includeFlight1" class="radio-inline"><input type="radio" id="includeFlight1" name="includeFlight" value="true"> Yes</label>
                                                                <label for="includeFlight2" class="radio-inline"><input type="radio" id="includeFlight2" name="includeFlight" value="false"> No</label>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">  </div>
                                                        <br>
                                                        <div class="woocommerce-billing-fields fligh-details">

                                                            <div class="panel panel-info" style="border-color: #666;">
                                                                <div class="panel-heading" style="background-color: #666; border-color: #666;">
                                                                    <h3 class="panel-title"><span class="fa fa-plane"></span> Flight Details
                                                                        <div style="margin-right: 10px;" class="pull-right">
                                                                            <div class="form-group">
                                                                                <label for="" class="sr-only">For Show</label>
                                                                                <a id="flight-preferred-airlines" style="cursor: pointer; color: #FFF !important;" class="text-primary"><strong>Airlines Used - subject to availability <span class="fa fa-plane"></span></strong></a>
                                                                            </div>
                                                                        </div>
                                                                    </h3>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="col col-md-12">
                                                                        <div class="col col-md-6" style="padding-left: 0px;">
                                                                            <div class="form-group">
                                                                                <label for="flight-city">Departure City</label>
                                                                                <div class="input-group">
                                                                                    <!-- <input type="text" class="form-control" id="flight-city" name="flight[city]"> -->
                                                                                    <select name="flight[city]" id="flight-city" class="form-control">
                                                                                        <option value="Sydney">Sydney</option>
                                                                                        <option value="Melbourne">Melbourne</option>
                                                                                        <option value="Brisbane">Brisbane</option>
                                                                                        <option value="Adelaide">Adelaide</option>
                                                                                        <option value="Perth">Perth</option>
                                                                                    </select>
                                                                                    <div class="input-group-addon"><span class="fa fa-map-marker"></span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col col-md-6" style="padding-right: 0px;">
                                                                            <div class="form-group">
                                                                                <label for="flight-class">Preferred Class</label>
                                                                                <input type="text" class="form-control" id="flight-class" name="flight[class]" value="Economy" readonly="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="FD-departure-date">Departure Date</label>
                                                                            <div class="input-group">
                                                                                <input type="text" class="flight-date form-control departure" id="FD-departure-date" name="flight[departureDate]" value="<?=date('d/m/Y', strtotime(@$_POST['startdate'] . ' -2 day'))?>">
                                                                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                                                            </div>                                                    
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="FD-return-date">Return Date</label>
                                                                            <div class="input-group">
                                                                                <input type="text" class="flight-date form-control return" id="FD-return-date" name="flight[returnDate]" value="">
                                                                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="flight-pax">Number of Passengers</label>
                                                                            <div class="input-group">
                                                                                <input type="number" class="form-control" name="flight[pax]" id="flight-pax" value="" readonly="">
                                                                                <div class="input-group-addon"><span class="fa fa-users"></span></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-6 sr-only">
                                                                        <div class="form-group">
                                                                            <label for="flight-price">Price</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon"><span class="fa fa-usd"></span></div>
                                                                                <input type="text" class="form-control" name="flight[price]" id="flight-price" value="" data-per-person="0">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group flight-terms">
                                                                            <input type="checkbox" name="flight[terms]" id="flight-terms">
                                                                            <!-- <label style="color: #0091ea; cursor: pointer;">I have read the <a id="flight-terms-and-condition">terms and condition</a> of the flight.</label> -->
                                                                            <label style="color: #0091ea; cursor: pointer;">I have read the <a href="http://ifly.net.au/terms-and-conditions" target="_blank">terms and condition</a> of the flight.</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="alert alert-danger flight-errors"></div> -->
                                                            
                                                            <!-- <div class="panel panel-default">
                                                                <div class="panel-headding">
                                                                    <h3 class="panel-title"></h3>
                                                                </div>
                                                                <div class="panel-body">
                                                                    
                                                                </div>
                                                            </div> -->
                                                    </div>
                                                    <!--====  End of FLIGHTS  ====-->
                                                </div>



                             <?php if( ! empty( $product['productDetailedDescription'] ) ) :?>
                                <div class="col-md-12 woocommerce-billing-fields" style="padding-left: 0px; padding-right: 0px;">
                                     <h3 class="header" style="font-size: 1.5em; padding: 5px 10px;">ITINERARY</h3>

                                     <div class="col-md-12"><?=$product['productDetailedDescription'];?></div>
                                 </div>
                             <?php endif; ?>


                             <?php if( ! empty( $product['productInclusionDescription'] ) ) :?>
                                <div class="col-md-12 woocommerce-billing-fields" style="padding-left: 0px; padding-right: 0px;">
                                     <h3 class="header" style="font-size: 1.5em; padding: 5px 10px;">INCLUSIONS</h3>

                                     <div class="col-md-12"><?=$product['productInclusionDescription'];?></div>
                                 </div>
                             <?php endif; ?>


                             <?php if( ! empty( $product['productExclusionDescription'] ) ) :?>
                                <div class="col-md-12 woocommerce-billing-fields" style="padding-left: 0px; padding-right: 0px;">
                                     <h3 class="header" style="font-size: 1.5em; padding: 5px 10px;">EXCLUSIONS</h3>

                                     <div class="col-md-12"><?=$product['productExclusionDescription'];?></div>
                                 </div>
                             <?php endif; ?>


                             <?php if( ! empty( $product['productConditionsDescription'] ) ) :?>
                                <div class="col-md-12 woocommerce-billing-fields" style="padding-left: 0px; padding-right: 0px;">
                                     <h3 class="header" style="font-size: 1.5em; padding: 5px 10px;">CONDITIONS</h3>

                                     <div class="col-md-12"><?=$product['productConditionsDescription'];?></div>
                                 </div>
                             <?php endif; ?>

                        </div> <!-- end of tab-->
                    

                        
                        </div>
                          <div class="col-md-3 col-sm-12 product-brief-info">
                            <div class="detail-sidebar" style="margin-top: 0px;">
                                <div class="call-to-book panel panel-themes">
                                    <i class="awe-icon awe-icon-phone" style="margin-left:10px; margin-top: 20px; " ></i>
                                    <em style="font-size: 15px; font-weight: bold; margin-top: 20px; margin-left: 20px;">Call to book</em> <span style="padding-bottom:20px; font-size: 14px;"><?=$agency['number']?></span>
                                </div>
                                <div class="booking-info  panel panel-themes" style="border-radius: 5px;">
                                    <!-- <h3 style="text-align: center; font-weight: bolder; font-size: 18px;">Tours info</h3> -->
                    
                                    <h5 style="font-size: 20px; margin-top: 0px;"><?=$product['productName'];?></h5>
                                   <div class="form-elements form-room">
                                        
                                            <label style="font-weight: bold;">Tours Start: </label><br><span id="side-t-start"></span><br>
                                            <label style="font-weight: bold; margin-top: 10px;">Tours End: </label><br><span id="side-t-end" style=""></span><br>
                                            <label style="font-weight: bold; margin-top: 10px; ">Duration: </label><br><span id="side-t-num" style="" ></span><br>
                                            <div class="book-summary" style="padding-top: 10px; padding-bottom: 10px;">
                                                <div class="summary-products">
                                                    <label for="" style="font-weight: bold;">PRODUCTS</label>
                                                    <div class="wrapper"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="summary-packages">
                                                    <label for="" style="font-weight: bold;">ADD ONS/PACKAGES</label>
                                                    <div class="wrapper"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="summary-extra-bills">
                                                    <label for="" style="font-weight: bold;">EXTRA BILLS</label>
                                                    <div class="wrapper"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="summary-suppliments">
                                                    <label for="" style="font-weight: bold;">SUPPLIMENTS</label>
                                                    <div class="wrapper"></div>
                                                </div>
                                            </div>
                                    </div>
                                    <div style="margin-top: 10px;">
                                        <label for="" style="font-weight: bold;">NUMBER OF PAX <span class="total-pax">0</span></label>
                                    </div>
                                    <div class="clearfix"></div>
                                        
                                    <!-- FOR AGENT ONLY -->

                                    <?php if ( @$SESSION && is_array( $SESSION ) ): ?>
                                        <div class="agent-view">
                                            <div class="form-group gross-amount" style="margin-bottom: 0px;">
                                                <label for="" style="padding-left: 0px; padding-right: 0px; font-weight: bold;" class="control-label col-sm-8">Gross Amount:</label>
                                                <div class="col-sm-4">$0.00</div>
                                            </div>
                                            <div class="form-group commission-amount" style="margin-bottom: 0px;">
                                                <label for="" style="padding-left: 0px; padding-right: 0px; font-weight: bold;" class="control-label col-sm-8">Commission Amount:</label>
                                                <div class="col-sm-4">$0.00</div>
                                            </div>
                                            <div class="form-group net-amount" style="margin-bottom: 0px;">
                                                <label for="" style="padding-left: 0px; padding-right: 0px; font-weight: bold;" class="control-label col-sm-8">Nett Amount:</label>
                                                <div class="col-sm-4">$0.00</div>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <?php if (!@$SESSION): ?>
                                        <br>
                                        <div class="promo" style="padding: 5px; border: 2px dashed #FFF;">
                                            <div class="form-group">
                                                <label for="promo-code">PROMO CODE</label>
                                                <input style="font-size: 1em; height: 45px;" type="text" class="form-control" id="promo-code" name="PROMOCODE" value="">
                                            </div>
                                            <small>Note: Promo Code and associated discounts apply to either the land or the flight product only.</small>
                                            <div style="margin-bottom: 0px;" class="form-group"><label for="" class="control-label col-sm-12" style="padding: 0px;"></label></div>
                                        </div>
                                        <br>
                                    <?php endif ?>

                                    <!--====================================
                                    =            RETURN FLIGHTS            =
                                    =====================================-->
                                    
                                    <div style="padding: 0px; margin-bottom: 10px;" class="col-md-12">
                                        <strong> RETURN FLIGHTS PRICE: <span id="departure-fee" data-per-person="0" data-price="0">$0.00</span> </strong>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <!--====  End of RETURN FLIGHTS  ====-->

                                    

                                    <div class="price" style="margin-top: 0px;">
                                        <em>Total for this tour</em>
                                        <span class="undescounted sr-only">
                                            <span>$0.00</span>
                                        </span>
                                        <br>
                                        <span class="amount" id="tour-price">$0.00</span>
                                    </div>
                                   
                                   <input type='hidden' id='previousPrice' name="previousPrice"> 
                                   <input type='hidden' id='isDiscounted' name="isDiscounted"> 
                                   <input type='hidden' id='startdate' name="startdate"> 
                                   <input type='hidden' id='startend' name="startend">
                                   <input type='hidden' id='dateduration' name="dateduration">
                                   <input type='hidden' id='tourTotalPrice' name="tourTotalPrice">
                                   <input type='hidden' name="productName" value="<?=$product['productName']?>">
                                   <input type='hidden' name="productId" value="<?=$product['productId']?>">
                                   <input type='hidden' name="previousURL" value="<?=uri_string() . '?' . $_SERVER['QUERY_STRING']?>">
                                   <input type='hidden' name="token" value="<?=generate_token('externalToursToken')?>">

                                   <input type="hidden" name="passengers" value="0">

                                    <div class="book-package">
                                       <div class="static"></div>
                                       <div class="non-static"></div>
                                    </div>
                                    <div class="book-product"></div>
                                    <div class="book-addons"></div>
                                    <div class="book-suppliment"></div>
                                    
                                    <div class="form-submit" style="margin-top: 10px;">
                                        <div class="add-to-cart" style="color: #aa3939 !important; width: 100%;">
                                            <button class="panel panel-themes form-button btn-block" type="button" style="border-radius: 5px;">
                                                <i class="awe-icon awe-icon-cart"></i> Book Now
                                            </button>
                                        </div>
                                    </div>
                                   

                                </div>
                                
                                </div>
                            </div>
                        
                                     </div>
    
                </div>
 </section>
</form>
