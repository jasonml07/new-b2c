
<?php foreach ( range(1, intval($_GET['passengers'])) as $index): ?>
	<?php $index_d = $index - 1; ?>

	<?php $title     = isset( $_POST['passengers'][$index_d]['title'] ) ? $_POST['passengers'][$index_d]['title'] : '' ?>
	<?php $firstname = isset( $_POST['passengers'][$index_d]['firstname'] ) ? $_POST['passengers'][$index_d]['firstname'] : '' ?>
	<?php $lastname  = isset( $_POST['passengers'][$index_d]['lastname'] ) ? $_POST['passengers'][$index_d]['lastname'] : '' ?>
	<div class="col-sm-6 checkout-passenger">
		<h6 class="text-center header">Guest <?=$index ?></h6>
		<div class="form-group col-sm-6">
			<label for="title-<?=($index - 1) ?>">First name</label>
			<div class="input-group">
				<div class="input-group-addon select">
					<select class="form-control" name="passengers[<?=($index - 1) ?>][title]" id="title-<?=($index - 1) ?>" <?=($index == 1)? 'data-bind="voucher[title]"': '' ?>>
	                    <option value="">Title</option>
	                    <option value="Mr">Mr.</option>
	                    <option value="Ms">Ms.</option>
	                    <option value="Mrs">Mrs.</option>
	                </select>
				</div>
				<input type="text" name="passengers[<?=($index - 1) ?>][firstname]" class="form-control" <?=($index == 1)? 'data-bind="voucher[firstname]"': '' ?> value="<?=$firstname ?>">
			</div>
		</div>
		<div class="form-group col-sm-6">
			<label for="last-name-<?=($index - 1) ?>">Last name</label>
			<input type="text" name="passengers[<?=($index - 1) ?>][lastname]" id="last-name-<?=($index - 1) ?>" class="form-control" <?=($index == 1)? 'data-bind="voucher[lastname]"': '' ?> value="<?=$lastname ?>">
		</div>
	</div>
<?php endforeach ?>