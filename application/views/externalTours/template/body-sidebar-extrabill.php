<?php $extraBills = $_POST['extraBills'] ?>
<?php foreach ($extraBills as $extraBills): ?>
	<div class="form-group">
		<label for="" class="control-label text-left col-sm-7"><?=$extraBills['name'] ?> X<?=$extraBills['quantity'] ?></label>
		<div class="col-sm-5">
			<p class="form-control-static"><strong class="hidden-sm">: </strong><span class="fa fa-usd"></span><?=number_format($extraBills['subtotal'], 2) ?></p>
		</div>
	</div>
<?php endforeach ?>