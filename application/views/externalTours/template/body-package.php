<?php $packages = $_POST['packages'] ?>
<?php foreach ( $packages as $key => $package ): ?>
	<?php $package['markup_pct_amount']  = round($package['markup_pct_amount']) ?>
	<?php $package['comm_pct_amount']    = round($package['comm_pct_amount']) ?>
	<?php $package['convertedBasePrice'] = round($package['convertedBasePrice']) ?>
	<?php $package['grossPrice']         = round($package['grossPrice']) ?>
	<?php $package['comPrice']           = round($package['comPrice']) ?>
	<?php $package['netAmount']          = round($package['netAmount']) ?>

	<div class="form-group no-margin">
		<label for="" class="control-label input col-sm-3">
			<input type="number"
				name          ="package[<?=$key ?>][quantity]"
				min           ="0"
				value         ="0"
				class         ="form-control package-selection"
				data-name     ="<?=$package['supplement_name'] ?>"
				data-subtotal ="0"
				data-price    ="<?=$package['grossPrice'] ?>"
				data-costings ="{OriginalBasePrice: <?=$package['OriginalBasePrice'] ?>, markup_pct_amount: <?=$package['markup_pct_amount'] ?>, comm_pct_amount: <?=$package['comm_pct_amount'] ?>, convertedBasePrice: <?=$package['convertedBasePrice'] ?>, grossPrice: <?=$package['grossPrice'] ?>, comPrice: <?=$package['comPrice'] ?>, netAmount: <?=$package['netAmount'] ?>}">
		</label>
		<div class="col-sm-9">
			<p class="form-control-static"><strong><?=$package['supplement_name'] ?> : </strong><span class="fa fa-usd"></span><?=number_format($package['grossPrice'], 2) ?></p>
		</div>
	</div>
<?php endforeach ?>