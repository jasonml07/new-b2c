<?php $packages = $_POST['packages'] ?>
<?php foreach ($packages as  $key => $package): ?>
	<input type="hidden" name="packages[<?=$key ?>][name]"     value="<?=$package['name'] ?>">
	<input type="hidden" name="packages[<?=$key ?>][quantity]" value="<?=$package['quantity'] ?>">
	<input type="hidden" name="packages[<?=$key ?>][price]"    value="<?=$package['price'] ?>">
	<input type="hidden" name="packages[<?=$key ?>][subtotal]" value="<?=$package['subtotal'] ?>">
	<input type="hidden" name="packages[<?=$key ?>][costing]"  value="">
	<div class="form-group">
		<label for="" class="control-label text-left col-sm-7"><?=$package['name'] ?> X<?=$package['quantity'] ?></label>
		<div class="col-sm-5">
			<p class="form-control-static"><strong class="hidden-sm">: </strong><span class="fa fa-usd"></span><?=number_format($package['subtotal'], 2) ?></p>
		</div>
	</div>
<?php endforeach ?>