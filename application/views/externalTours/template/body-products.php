<?php $products      = $_POST['products'] ?>
<?php $hasSupplement = (isset( $_POST['supplements'] ) && ! empty( $_POST['supplements'] )) ?>
<?php $productKey    = $_POST['reference'] ?>
<?php
/**
 * [productSupplement description]
 * @param  [type] $productIndex [description]
 * @return [type]               [description]
 */
function productSupplement ( $productIndex )
{
	global $hasSupplement;
	if ( $hasSupplement )
	{
		$supplements = $_POST['supplements'];
		?>
		<div>
		<?php foreach ( $supplements as $key => $supplement ) : ?>
		<?php $supplement['markup_pct_amount']  = round($supplement['markup_pct_amount']) ?>
		<?php $supplement['comm_pct_amount']    = round($supplement['comm_pct_amount']) ?>
		<?php $supplement['convertedBasePrice'] = round($supplement['convertedBasePrice']) ?>
		<?php $supplement['grossPrice']         = round($supplement['grossPrice']) ?>
		<?php $supplement['comPrice']           = round($supplement['comPrice']) ?>
		<?php $supplement['netAmount']          = round($supplement['netAmount']) ?>
			<div class="supplement-container">
				<input type="radio" class="sr-only"
					id            ="product-suppliment-<?=$productIndex ?>-<?=$key ?>"
					name          ="products[<?=$productIndex ?>][supplement]"
					value         ="<?=$key ?>"
					data-name     ="<?=$supplement['sub_supplement_avail_name'] ?>"
					data-costings ="{convertedRate: <?=$supplement['convertedRate'] ?>, toCurrency: &quot;<?=$supplement['sub_toCurrency'] ?>&quot;, fromCurrency: &quot;<?=$supplement['sub_fromCurrency'] ?>&quot;, OriginalBasePrice: <?=$supplement['OriginalBasePrice'] ?>, markup_pct_amount: <?=$supplement['markup_pct_amount'] ?>, comm_pct_amount: <?=$supplement['comm_pct_amount'] ?>, convertedBasePrice: <?=$supplement['convertedBasePrice'] ?>, grossPrice: <?=$supplement['grossPrice'] ?>, comPrice: <?=$supplement['comPrice'] ?>, netAmount: <?=$supplement['netAmount'] ?>}">
				<label for="product-suppliment-<?=$productIndex ?>-<?=$key ?>" class="text-line-through"><span class="fa fa-check sr-only text-success"></span> <?=$supplement['sub_supplement_avail_name'] ?> (<span class="fa fa-usd"></span><?=number_format($supplement['grossPrice'], 2) ?>)</label>
			</div>
		<?php endforeach ?>
		</div>
		<?php
	}

} // end of supplement
function productSelections ( $minGuest )
{
	global $hasSupplement;
	?>
	<?php foreach ( range(1, 15) as $index ): ?>
		<?php $moduled = (($index % $minGuest) != 0) ?>
		<?php if ( !(! $hasSupplement && $moduled ) ): ?>
			<option value="<?=$index ?>" data-is-supplement="<?=$moduled ?'true':'false' ?>"><?=$index ?></option>
		<?php endif ?>
	<?php endforeach ?>
	<?php
} // end of productSelections
?>
<?php foreach ( $products as $key => $product ): ?>
	<?php $product['minGuest']           = intval($product['minGuest']) ?>
	<?php $product['comPrice']           = round($product['comPrice']) ?>
	<?php $product['netAmount']          = round($product['netAmount']) ?>
	<?php $product['grossPrice']         = round($product['grossPrice']) ?>
	<?php $product['convertedRate']      = $product['convertedRate'] ?>
	<?php $product['comm_pct_amount']    = round($product['comm_pct_amount']) ?>
	<?php $product['OriginalBasePrice']  = round($product['OriginalBasePrice']) ?>
	<?php $product['markup_pct_amount']  = round($product['markup_pct_amount']) ?>
	<?php $product['convertedBasePrice'] = round($product['convertedBasePrice']) ?>
	<tr data-product-key="<?=$productKey ?>"
		data-item-key ="<?=$key ?>"
		data-name     ="<?=$product['supplement_avail_name'] ?>"
		data-price    ="<?=$product['grossPrice'] ?>"
		data-costings ="{OriginalBasePrice: <?=$product['OriginalBasePrice'] ?>, toCurrency: &quot;<?=$product['toCurrency'] ?>&quot;, fromCurrency: &quot;<?=$product['fromCurrency'] ?>&quot;, convertedRate: <?=$product['convertedRate'] ?>, markup_pct_amount: <?=$product['markup_pct_amount'] ?>, comm_pct_amount: <?=$product['comm_pct_amount'] ?>, convertedBasePrice: <?=$product['convertedBasePrice'] ?>, grossPrice: <?=$product['grossPrice'] ?>, comPrice: <?=$product['comPrice'] ?>, netAmount: <?=$product['netAmount'] ?>}">
		<td class="hidden-md hidden-lg">
			<div class="form-horizontal no-margin">
				<div class="form-group">
					<label for="" class="control-label input col-sm-5"># OF PASSENGERS</label>
					<div class="col-sm-7">
						<select name="products[<?=$key ?>][quantity]" id="" class="form-control" data-minguest="<?=$product['minGuest'] ?>" data-price="<?=$product['grossPrice'] ?>">
							<option selected="" value="0">0</option>
							<?php productSelections( $product['minGuest'] ) ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label input col-sm-5">ROOM/CABIN OCCUPANCY</label>
					<div class="col-sm-7">
						<p class="form-control-static product-name-<?=$key?>"><strong class="hidden-md hidden-lg">: </strong><?=$product['supplement_avail_name'] ?></p>
					</div>
				</div>
				<?php if ( $product['minGuest'] > 1 ): ?>
				<div class="form-group">
					<label for="" class="control-label input col-sm-5">SUPPLEMENTS</label>
					<div class="col-sm-7">
						<?php productSupplement( $key ) ?>
					</div>
				</div>
				<?php endif ?>
				<div class="form-group">
					<label for="" class="control-label input col-sm-5">PRICE/PERSON</label>
					<div class="col-sm-7">
						<p class="form-control-static"><strong><span class="hidden-sm">:</span><span class="fa fa-usd"></span><span class="product-price-<?=$key?>"><?=number_format($product['grossPrice'], 2) ?></span></strong></p>
					</div>
				</div>
			</div>
		</td>
		<td class="hidden-xs hidden-sm align-middle">
			<select name="products[<?=$key ?>][quantity]" id="" class="form-control" data-minguest="<?=$product['minGuest'] ?>" data-price="<?=$product['grossPrice'] ?>">
				<option selected="" value="0">0</option>
				<?php productSelections( $product['minGuest'] ) ?>
			</select>
		</td>
		<td class="hidden-xs hidden-sm product-name-<?=$key?>"><?=$product['supplement_avail_name'] ?></td>
		<td class="hidden-xs hidden-sm">
			<?php if ( intval($product['minGuest']) > 1 ): ?>
				<?php productSupplement( $key ) ?>
			<?php endif ?>
		</td>
		<td class="hidden-xs hidden-sm text-right align-middle"><strong><span class="fa fa-usd"></span><span class="product-price-<?=$key?>"><?=number_format($product['grossPrice'], 2) ?></span></strong></td>
	</tr>
<?php endforeach ?>