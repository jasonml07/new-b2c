<?php $extrabills = $_POST['extrabills'] ?>
<?php foreach ( $extrabills as $key => $extrabill ): ?>
	<?php $extrabill['markup_pct_amount']  = round($extrabill['markup_pct_amount']) ?>
	<?php $extrabill['comm_pct_amount']    = round($extrabill['comm_pct_amount']) ?>
	<?php $extrabill['convertedBasePrice'] = round($extrabill['convertedBasePrice']) ?>
	<?php $extrabill['grossPrice']         = round($extrabill['grossPrice']) ?>
	<?php $extrabill['comPrice']           = round($extrabill['comPrice']) ?>
	<?php $extrabill['netAmount']          = round($extrabill['netAmount']) ?>
	<?php $isMarkup = ($extrabill['isMarkup'] == 'true') ?>
	<div class="form-group">
		<input type="hidden"
			class      ="extrabill-selection"
			data-id    ="<?=$key ?>"
			data-name  ="<?=$extrabill['supplement_name'] ?>"
			data-price ="<?=$isMarkup?$extrabill['grossPrice']:$extrabill['convertedBasePrice'] ?>"
			data-costings="{OriginalBasePrice: <?=$extrabill['OriginalBasePrice'] ?>, isMarkup: <?=$extrabill['isMarkup'] ?>, markup_pct_amount: <?=$extrabill['markup_pct_amount'] ?>, comm_pct_amount: <?=$extrabill['comm_pct_amount'] ?>, convertedBasePrice: <?=$extrabill['convertedBasePrice'] ?>, grossPrice: <?=$extrabill['grossPrice'] ?>, comPrice: <?=$extrabill['comPrice'] ?>, netAmount: <?=$extrabill['netAmount'] ?>}">
		<label for="" class="control-label input text-left col-sm-7">
			<?=$extrabill['supplement_name'] ?>
		</label>
		<div class="col-sm-5">
			<p class="form-control-static"><strong>: </strong><span class="fa fa-usd"></span><?=number_format($extrabill['grossPrice'], 2) ?></p>
		</div>
	</div>
<?php endforeach ?>