<?php $isDiscounted = (isset( $_POST['isDiscounted'] ) && filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN)) ?>
<?php if ( $isDiscounted ): ?>
	<h4><del><span class="fa fa-usd"></span><?=number_format($_POST['price'], 2) ?></del></h4>
<?php endif ?>
<h2>
	<span class="amount" id="total-price"><span class="fa fa-usd"></span><?php if ( $isDiscounted ): ?><?=number_format($_POST['discountedPrice'], 2) ?><?php else: ?><?=number_format($_POST['price'], 2) ?><?php endif ?>
</h5>