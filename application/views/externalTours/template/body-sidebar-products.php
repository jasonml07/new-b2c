<?php $products = $_POST['products'] ?>
<?php foreach ($products as $product): ?>
	<?php $product['subtotal'] = isset( $product['supplement'] ) ? ((float)$product['subtotal'] - (float)$product['supplement']['subtotal']) : $product['subtotal']?>
	<div class="form-group">
		<label for="" class="control-label col-sm-7">
		<?=$product['name'] ?> X<?=$product['quantity'] ?>
		</label>
		<div class="col-sm-5">
			<p class="form-control-static"><strong class="hidden-sm">: </strong><span class="fa fa-usd"></span><?=number_format($product['subtotal'], 2) ?></p>
		</div>
	</div>
	<?php if ( isset( $product['supplement'] ) ): ?>
		<?php $supplement = $product['supplement'] ?>
		<div class="form-group">
			<label for="" class="control-label col-sm-7">Supplement: <?=$supplement['name'] ?> X<?=$supplement['quantity'] ?></label>
			<div class="col-sm-5">
				<p class="form-control-static"><strong class="hidden-sm">: </strong><span class="fa fa-usd"></span><?=number_format($supplement['subtotal'], 2) ?></p>
			</div>
		</div>
	<?php endif ?>
<?php endforeach ?>