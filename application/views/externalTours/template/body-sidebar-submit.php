<?php $hasFlight        = filter_var($_POST['hasFlight'], FILTER_VALIDATE_BOOLEAN) ?>
<?php $isDiscounted     = filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN) ?>
<?php $passengers       = intval($_POST['passengers']) ?>
<?php $previousURL      = $_POST['previousURL'] ?>
<?php $token            = $_POST['token'] ?>
<?php $items            = $_POST['items'] ?>
<?php $tour             = $_POST['tour'] ?>
<?php $prices           = $_POST['prices'] ?>
<?php $product          = $_POST['product'] ?>
<?php $itemPrices       = $_POST['itemPrice'] ?>
<?php $discountedPrices = @$_POST['discountedPrices'] ?>

<input type ="hidden" name="hasFlight"    value="<?=$_POST['hasFlight'] ?>">
<input type ="hidden" name="isDiscounted" value="<?=$_POST['isDiscounted'] ?>">
<input type ="hidden" name="passengers"   value="<?=$_POST['passengers'] ?>">
<input type ="hidden" name="token"        value="<?=$_POST['token'] ?>">
<input type ="hidden" name="previousURL"  value="<?=$_POST['previousURL'] ?>">

<input type="hidden" name="prices[before]" value="<?=$prices['before'] ?>">
<input type="hidden" name="prices[after]"  value="<?=$prices['after'] ?>">

<input type="hidden" name="product[id]"   value="<?=$product['id'] ?>">
<input type="hidden" name="product[name]" value="<?=$product['name'] ?>">

<input type="hidden" name="tour[start]"    value="<?=$tour['start'] ?>">
<input type="hidden" name="tour[end]"      value="<?=$tour['end'] ?>">
<input type="hidden" name="tour[duration]" value="<?=$tour['duration'] ?>">

<input type="hidden" name="itemPrices[product]" value="<?=$itemPrices['product'] ?>">
<input type="hidden" name="itemPrices[flight]"  value="<?=$itemPrices['flight'] ?>">

<?php if ( !! $discountedPrices ): ?>
	<input type="hidden" name="discountedPrices[product]" value="<?=$discountedPrices['product'] ?>">
	<input type="hidden" name="discountedPrices[flight]"  value="<?=$discountedPrices['flight'] ?>">
<?php endif ?>

<?php if ( $hasFlight ): ?>
	<?php $flight = $_POST['flight'] ?>
	<input type="hidden" name="flight[city]"      value="<?=$flight['city'] ?>">
	<input type="hidden" name="flight[class]"     value="<?=$flight['class'] ?>">
	<input type="hidden" name="flight[departure]" value="<?=$flight['departure'] ?>">
	<input type="hidden" name="flight[pax]"       value="<?=$flight['pax'] ?>">
	<input type="hidden" name="flight[price]"     value="<?=str_replace(',', '', $flight['price']) ?>">
	<input type="hidden" name="flight[return]"    value="<?=$flight['return'] ?>">
<?php endif ?>
<?php if ($isDiscounted): ?>
	<input type="hidden" name="PROMOCODE" value="<?=$_POST['PROMOCODE'] ?>">
	<?php $discount = $_POST['discount'] ?>
	
	<input type="hidden" name="discount[type]"                   value="<?=$discount['type'] ?>">
	<input type="hidden" name="discount[category]"               value="<?=$discount['category'] ?>">
	<input type="hidden" name="discount[value]"                  value="<?=$discount['value'] ?>">
	<input type="hidden" name="discount[message]"                value="<?=$discount['message'] ?>">
	<input type="hidden" name="discount[availabilities][from]" value="<?=$discount['availabilities']['from'] ?>">
	<input type="hidden" name="discount[availabilities][to]"   value="<?=$discount['availabilities']['to'] ?>">
<?php endif ?>

<?php if ( isset( $items['products'] ) ): ?>
	<?php $products = $items['products'] ?>
	<?php foreach ($products as $key => $product): ?>
		<input type="hidden" name="items[products][<?=$key ?>][itemKey]"    value="<?=$product['itemKey'] ?>">
		<input type="hidden" name="items[products][<?=$key ?>][name]"       value="<?=$product['name'] ?>">
		<input type="hidden" name="items[products][<?=$key ?>][price]"      value="<?=$product['price'] ?>">
		<input type="hidden" name="items[products][<?=$key ?>][productKey]" value="<?=$product['productKey'] ?>">
		<input type="hidden" name="items[products][<?=$key ?>][quantity]"   value="<?=$product['quantity'] ?>">
		<input type="hidden" name="items[products][<?=$key ?>][subtotal]"   value="<?=$product['subtotal'] ?>">

		<?php print_costings( 'items[products][' . $key . ']', $product['costings']) ?>
		<?php if ( isset( $product['supplement'] ) ): ?>
			<?php $supplement = $product['supplement'] ?>

			<input type="hidden" name="items[products][<?=$key ?>][supplement][key]"      value="<?=$supplement['key'] ?>">
			<input type="hidden" name="items[products][<?=$key ?>][supplement][name]"     value="<?=$supplement['name'] ?>">
			<input type="hidden" name="items[products][<?=$key ?>][supplement][price]"    value="<?=$supplement['price'] ?>">
			<input type="hidden" name="items[products][<?=$key ?>][supplement][quantity]" value="<?=$supplement['quantity'] ?>">
			<input type="hidden" name="items[products][<?=$key ?>][supplement][subtotal]" value="<?=$supplement['subtotal'] ?>">

			<?php print_costings( 'items[products][' . $key . '][supplement]', $supplement['costings']) ?>
		<?php endif ?>
	<?php endforeach ?>
<?php endif ?>

<?php if ( isset( $items['packages'] ) ): ?>
	<?php $packages = $items['packages'] ?>

	<?php foreach ($packages as $key => $package): ?>
		<input type="hidden" name="items[packages][<?=$key ?>][name]"     value="<?=$package['name'] ?>">
		<input type="hidden" name="items[packages][<?=$key ?>][price]"    value="<?=$package['price'] ?>">
		<input type="hidden" name="items[packages][<?=$key ?>][quantity]" value="<?=$package['quantity'] ?>">
		<input type="hidden" name="items[packages][<?=$key ?>][subtotal]" value="<?=$package['subtotal'] ?>">

		<?php print_costings( 'items[packages][' . $key . ']', $package['costings']) ?>
	<?php endforeach ?>
<?php endif ?>

<?php if ( isset( $items['extrabills'] ) ): ?>
	<?php $extrabills = $items['extrabills'] ?>

	<?php foreach ($extrabills as $key => $extrabill): ?>
		<input type="hidden" name="items[extrabills][<?=$key ?>][name]"     value="<?=$extrabill['name'] ?>">
		<input type="hidden" name="items[extrabills][<?=$key ?>][price]"    value="<?=$extrabill['price'] ?>">
		<input type="hidden" name="items[extrabills][<?=$key ?>][quantity]" value="<?=$extrabill['quantity'] ?>">
		<input type="hidden" name="items[extrabills][<?=$key ?>][subtotal]" value="<?=$extrabill['subtotal'] ?>">
		
		<?php print_costings( 'items[extrabills][' . $key . ']', $extrabill['costings']) ?>
	<?php endforeach ?>
<?php endif ?>

<?php
	function print_costings ( $key, $costings )
	{
		?>
		<?php foreach ($costings as $c_key => $costing): ?>
			<input type="hidden" name="<?=$key ?>[costings][<?=$c_key ?>]" value="<?=$costing ?>">
		<?php endforeach ?>
		<?php
	} // end of print_costings
?>