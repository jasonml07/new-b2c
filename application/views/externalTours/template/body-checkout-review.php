<div class="row products">
	<h6>Product Details</h6>
	<div class="col-xs-12">
		<div class="form-horizontal tours-info">
			<div class="form-group">
				<label for="" class="control-label col-sm-4"><span class="fa fa-calendar-check-o"></span> Tours starts</label>
				<div class="col-sm-8">
					<p class="form-control-static"><strong class="hidden-xs">: </strong><?=date('d M Y, l', strtotime($_POST['tour']['start'])) ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-4"><span class="fa fa-calendar-times-o"></span> Tours ends</label>
				<div class="col-sm-8">
					<p class="form-control-static"><strong class="hidden-xs">: </strong><?=date('d M Y, l', strtotime($_POST['tour']['end'])) ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-4"><span class="fa fa-calendar"></span> Tours duration</label>
				<div class="col-sm-8">
					<p class="form-control-static"><strong class="hidden-xs">: </strong><?=$_POST['tour']['duration']?> days</p>
				</div>
			</div>
		</div>
		<hr>
		<div class="form-horizontal products-info">
			<?php foreach ($_POST['items'] as $item_key => $items ): ?>
				<p class="border-bottom"><strong><?=strtoupper($item_key) ?></strong></p>
				<?php foreach ( $items as $key => $item ): ?>
					<div class="form-group">
						<label for="" class="control-label col-sm-4">
							<span class="fa fa-check"></span> <?=$item['name'] ?> X<?=$item['quantity'] ?>
							<?php if ( isset( $item['supplement'] ) ): ?>
								<?php $supplement = $item['supplement']; ?>
								<br>
								Supplement: <?=$supplement['name'] ?> X<?=$supplement['quantity'] ?> ( <span class="fa fa-usd"></span><?=number_format($supplement['price'], 2) ?> )
							<?php endif ?>
						</label>
						<div class="col-sm-8">
							<p class="form-control-static"><strong class="hidden-xs">: </strong><span class="fa fa-usd"></span><?=number_format($item['subtotal'], 2) ?></p>
						</div>
					</div>
				<?php endforeach ?>
			<?php endforeach ?>
		</div>
		<hr>
		<p><strong>Number of passengers: <span class="fa fa-users"></span> <?=count($_POST['passengers']) ?></strong></p>

		<?php if ( filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN) ): ?>
		<p><strong><?=$_POST['discount']['message'] ?></strong></p>
		<?php endif ?>
		<?php if ( filter_var($_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN) ): ?>
		<?php $flight_price = str_replace(',', '', $_POST['flight']['price']) ?>
		<p><strong>Return Flight Price X<?=$_POST['flight']['pax']?> (<span class="fa fa-usd"></span><?=number_format((intval($flight_price)/intval($_POST['flight']['pax'])), 2) ?> X <?=$_POST['flight']['pax'] ?> = <span class="fa fa-usd"></span><?=number_format($flight_price, 2) ?>)</strong></p>
		<?php endif ?>

		<div class="col-xs-12 text-center booking-price">
			<small>Total for this booking</small>
			<?php if ( filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN) ): ?>
                <h5><del><span class="fa fa-usd"></span><?=number_format($_POST['prices']['before'], 2) ?></del></h5>
            <?php endif ?>
			<h4>TOTAL: <span class="fa fa-usd"></span><?=number_format($_POST['prices'][(filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN)?'after':'before')], 2) ?></h4>
		</div>
	</div>
</div>

<!--================================
=            Passengers            =
=================================-->

<div class="row">
	<h6>Passengers</h6>
	<div class="form-horizontal">
		<?php foreach ( $_POST['passengers'] as $key => $passenger ): ?>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Guest <?=($key+1) ?></label>
			<div class="col-sm-9">
				<p class="form-control-static"><strong class="hidden-xs">: </strong><?=$passenger['title'] ?>. <?=$passenger['firstname'] ?> <?=$passenger['lastname'] ?></p>
			</div>
		</div>
		<?php endforeach ?>
	</div>
</div>

<!--====  End of Passengers  ====-->

<!--=============================
=            Voucher            =
==============================-->

<div class="row">
	<h6>Voucher</h6>
	<div class="form-horizontal">
		<?php $voucher = $_POST['voucher'] ?>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Name</label>
			<div class="col-sm-9">
				<p class="form-control-static"><?=$voucher['title'] ?>. <?=$voucher['firstname'] ?> <?=$voucher['lastname'] ?></p>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Email</label>
			<div class="col-sm-9">
				<p class="form-control-static"><?=$voucher['email'] ?></p>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="control-label col-sm-3">Mobile #</label>
			<div class="col-sm-9">
				<p class="form-control-static"><?=$voucher['phonenum'] ?></p>
			</div>
		</div>
	</div>
</div>

<!--====  End of Voucher  ====-->

<?php if ( isset($_POST['payment']) ): ?>
<!--====================================
=            Payment Method            =
=====================================-->
<div class="row">
	<h6>Payment</h6>
	<div class="form-horizontal">
<?php
	$payment = $_POST['payment'];
	switch ( trim(strtolower($_POST['payment_type'])) ) {
		case 'creditcard':
			?>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Category</label>
				<div class="col-sm-9">
					<p class="form-control-static">Credit Card</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Holder name</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['nm_card_holder'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Card number</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['no_credit_card'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Verification #</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['no_cvn'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Expiry Date</label>
				<div class="col-sm-9">
					<p class="form-static"><?=$payment['dt_expiry_month'] ?>/<?=$payment['dt_expiry_year'] ?></p>
				</div>
			</div>
			<?php
			
			break;

		case 'paypal':
			?>
			<div class="form-group">
				<div class="col-xs-12">
					<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/2000px-PayPal.svg.png" alt="PayPal" class="img-responsive">
				</div>
			</div>
			<?php
			break;

		case 'bankdeposit':
			?>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Category</label>
				<div class="col-sm-9">
					<p class="form-control-static">Bank Deposit</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Name</label>
				<div class="col-sm-9">
					<p class="form-control-static">Select World Travel Westpac BSB 034-216</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">ACCN</label>
				<div class="col-sm-9">
					<p class="form-control-static">261 424</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">SWIFT</label>
				<div class="col-sm-9">
					<p class="form-control-static">WPACAU2S</p>
				</div>
			</div>
			<?php
			break;

		case 'e-nett':
			?>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Category</label>
				<div class="col-sm-9">
					<p class="form-control-static">E-Nett</p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Date</label>
				<div class="col-sm-9">
					<p class="form-control-static"><?=$payment['date'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Reference</label>
				<div class="col-sm-9">
					<p class="form-control-static"><?=$payment['reference'] ?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-sm-3">Amount</label>
				<div class="col-sm-9">
					<p class="form-control-static"><span class="fa fa-usd"></span><?=$payment['amount'] ?></p>
				</div>
			</div>
			<?php
			break;
		
		case 'paylater':
			?>
			<div class="bs-callout bs-callout-danger">
				<ul>
					<?php foreach ( $payment['policies'] as $policy ): ?>
						<li><?=$policy ?></li>
					<?php endforeach ?>
				</ul>
			</div>
			<?php
			break;

		default:
			# code...
			break;
	}
?>
	</div>
</div>

<!--====  End of Payment Method  ====-->
<?php endif ?>