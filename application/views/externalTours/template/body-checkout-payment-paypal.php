<?php $supplements = array(); ?>
<?php foreach ( $_POST['items'] as $item_type => $items ): ?>
	<?php foreach ( $items as $key => $item ): ?>
		<?php if ( isset( $item['supplement'] ) ): ?>
			<?php if ( isset($supplements[ $item['supplement']['key'] ]) ): ?>
				<?php $supplements[ $item['supplement']['key'] ]['quantity'] += intval($item['supplement']['quantity'])?>
				<?php $supplements[ $item['supplement']['key'] ]['subtotal'] += (float)$item['supplement']['subtotal'] ?>
			<?php else: ?>
				<?php $supplements[ $item['supplement']['key'] ] = $item['supplement']; ?>
			<?php endif ?>
		<?php endif ?>
		<input type="hidden" name="<?=strtoupper($item_type) ?>[<?=$key?>][name]"     value="<?=$item['name']?> X<?=$item['quantity']?>">
	    <input type="hidden" name="<?=strtoupper($item_type) ?>[<?=$key?>][quantity]" value="<?=$item['quantity']?>">
	    <input type="hidden" name="<?=strtoupper($item_type) ?>[<?=$key?>][price]"    value="<?=$item['price']?>">
	   	<?=PHP_EOL; ?>
	<?php endforeach ?>
<?php endforeach ?>
<?php if ( ! empty( $supplements ) ): ?>
	<?php $x = 0; ?>
	<?php foreach ( $supplements as $key => $supplement ): ?>
		<input type="hidden" name="SUPPLEMENTS[<?=$x?>][name]"     value="<?=$supplement['name']?> X<?=$supplement['quantity']?>">
	    <input type="hidden" name="SUPPLEMENTS[<?=$x?>][quantity]" value="<?=$supplement['quantity']?>">
	    <input type="hidden" name="SUPPLEMENTS[<?=$x?>][price]"    value="<?=$supplement['price']?>">
	    <?php $x++; ?>
	<?php endforeach ?>
<?php endif ?>
<?php if ( filter_var($_POST['hasFlight'], FILTER_VALIDATE_BOOLEAN) ): ?>
	<input type="hidden" name="DEPARTURE[0][name]"     value="Return Flight Price X<?=$_POST['flight']['pax']?>">
    <input type="hidden" name="DEPARTURE[0][quantity]" value="<?=$_POST['flight']['pax']?>">
    <input type="hidden" name="DEPARTURE[0][price]"    value="<?=intval(str_replace(',', '', $_POST['flight']['price'])) / intval($_POST['flight']['pax'])?>">
<?php endif ?>
<?php if ( filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN) ): ?>
	<input type="hidden" name="DISCOUNT[0][name]"     value="Discount">	
	<input type="hidden" name="DISCOUNT[0][quantity]" value="1">	
	<input type="hidden" name="DISCOUNT[0][price]"    value="<?=(intval($_POST['discountedPrices']['product']) - intval($_POST['itemPrice']['product'])) ?>">	
<?php endif ?>
<input type="hidden" name="amount" value="<?=$_POST['prices'][ ( filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN) ? 'after' : 'before' ) ] ?>">