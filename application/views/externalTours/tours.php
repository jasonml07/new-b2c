        <section class="awe-parallax category-heading-section-demo">
            <div class="awe-overlay"></div>
            <div class="container">
                <div class="category-heading-content category-heading-content__2 text-uppercase">

                    <!-- BREADCRUMB -->
                    <div class="find">
                        <h2 class="text-center">Tours </h2>
                        <form class="col-md-12" style="margin-bottom: 150px;">
                            <div class="form-group">
                                <div class="form-elements">
                                    <label>Destination</label>
                                    <div class="form-item">
                                        <i class="awe-icon awe-icon-marker-1"></i>
                                        <input id="destination_area" type="text" required>
                                     <input id="destination_code" type="hidden" name="destination_code" value="" required>
                                    </div>
                                </div>
                                  <div class="form-elements">
                                    <label>Services</label>
                                    <div class="form-item">
                                        <i class="awe-icon awe-icon-marker-1"></i>
                                        <input id="destination_area" type="text" required>
                                     <input id="destination_code" type="hidden" name="destination_code" value="" required>
                                    </div>
                                </div>
                                <div class="form-elements">
                                     <div>
                                        <label>Availability</label>
                                        <div class="form-item">
                                            <i class="awe-icon awe-icon-marker-1"></i>
                                           <input id="hotel_area" type="text">
                                         <input id="hotel_code" type="hidden" name="hotel_code">
                                        </div>
                                    </div>
                                </div>
                            <div class="form-elements">
                                <div>
                                    <label>Date from</label>
                                    <div class="form-item">
                                        <i class="awe-icon awe-icon-calendar"></i>
                                        <input type="text" class="awe-calendar" value="Date">
                                    </div>
                                </div>
                                <div>
                                    <label>Date to</label>
                                   <div class="form-item">
                                        <i class="awe-icon awe-icon-calendar"></i>
                                        <input type="text" class="awe-calendar" value="Date">
                                    </div>

                                </div>   
                            </div>
                                <div>
                                  <a href="<?php echo base_url(); ?>toursResult" class="btn btn-primary btn-md" role="button">Find Tours</a>
                                </div>           
                                        
                            </div>
                              <!--  </div> 
                             <div class="form-group">       -->                              
                               <!--  <div class="form-elements ">
                                    <label>Number of Rooms </label>
                                    <div class="form-item">
                                        <select class="awe-select" onChange="travelrez.room(this.value)"  id="rooms" name="rooms" >
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4" >4</option> 
                                            <option value="5" >5</option> 
                                        </select>
                                    </div>
                                </div> -->
                              <!--   <div class="form-elements">
                                     <div id="roomPax1" style="display:block;">
                                      <label>Room 1</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult1"   name="rooms[1][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                                 </div>
                                             <div class="form-item">
                                            Child  <select class="awe-select" id='child1'   name="rooms[1][child]" onChange='travelrez.child_fnc(this.value,1)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                     </div>
                                    <div id="age1" ></div> 
                                    </div>
                                </div> -->
                                <!-- <div class="form-elements">
                                    <div id="roomPax2" style="display:none;">
                                        <label>Room 2</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult2"   name="rooms[2][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child2'   name="rooms[2][child]" onChange='travelrez.child_fnc(this.value,2)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                        </div>
                                         <div id="age2" ></div> 
                                    </div>
                                </div> -->
                              <!--   <div class="form-elements">
                                    <div id="roomPax3" style="display:none;">
                                        <label>Room 3</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult3"   name="rooms[3][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child3'   name="rooms[3][child]" onChange='travelrez.child_fnc(this.value,3)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                        </div>
                                         <div id="age3" ></div> 
                                    </div>
                                </div> -->
                                <!-- <div class="form-elements">
                                    <div id="roomPax4" style="display:none;">
                                        <label>Room 4</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult4"   name="rooms[4][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child4'   name="rooms[4][child]" onChange='travelrez.child_fnc(this.value,4)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                        </div>
                                          <div id="age4" ></div> 
                                    </div>
                                </div>
                                <div class="form-elements">
                                    <div id="roomPax5" style="display:none;">
                                        <label>Room 5</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult5"   name="rooms[5][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child5'   name="rooms[5][child]" onChange='travelrez.child_fnc(this.value,5)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                                  
                                        </div>
                                        <div id="age5" ></div> 
                                    </div>
                                </div>

 -->

                                

                            <!-- End of Form-Group -->
                        </form>
                    </div> <!-- End of find div -->
                    
                </div>
            </div>
        </section>