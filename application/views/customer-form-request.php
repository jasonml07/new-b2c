<script type="text/javascript">
    console.log('booking_info', <?=json_encode($booking_info)?>);
    console.log('post', <?=json_encode($post)?>);
    console.log('errors', <?=json_encode($errors)?>);
</script>
        <section class="checkout-section-demo" style="padding-left:0 !important;">
            <div class="container">
                <div class="row">
                	<div class="col-md-12">
                		<a href="<?=base_url()?>dashboard/hotel" class="btn btn-primary">
                			<i class="glyphicon glyphicon-search"></i>
                			&nbsp;Back to Search Page
                		</a>
                	</div>
                    <div class="col-lg-12">
                        <div class="checkout-page__top">
                            <div class="title">
                                <h1 class="text-uppercase" style="color: #1e1e1f; margin-left: 40px;">Assign Guest/Customer Information</h1>
                            </div>
                            <span class="phone" style="color: #0091ea; padding-left: 10px !important;">Support Call: +1-888-8765-1234</span>
                        </div>
                    </div>
                    <?php if( isset($post) && count($post['rooms']) > 0 ) : ?>
                    <div class="col-lg-8">
                        <?php if ( isset($errors['main']) ) : ?>
                            <div class="alert alert-danger">
                                <?php foreach ($errors['main'] as $key => $error) : ?>
                                    <p><?=$error?></p>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <form action="<?=base_url()?>booking-validation?id=<?=$_GET['id']?>" method="POST" novalidate>
                            <input type="hidden" name="requiredToPay" value="<?=$post['requiredToPay']?>">
                            <input type="hidden" id="form-active-payment" name="activePayment" value="<?=$post['activePayment']?>">
                            <input type="hidden" name="payway_token" value="<?=$post['payway_token']?>">
                            <div class="checkout-page__content">
                                <div class="customer-content" style="background-color: #fff !important; border: 3px solid #f2f2f2; border-radius: 3px; ">
                                        <?php foreach ($post['rooms'] as $roomkey => $room) : ?>
                                        <input type="hidden" id="rooms[<?=$roomkey?>][id]" name="rooms[<?=$roomkey?>][id]" value="<?=$room['id']?>">
                                        <div class="woocommerce-billing-fields">
                                            <h3 style="border-bottom: 4px solid #0091ea !important; background-color: #e6f3ff; color:#1e1e1f; padding-left: 13px;">Room <?=$roomkey+1?></h3>
                                            <!-- START OF ADULT -->
                                            <div class="col-lg-6" style="padding:1 !important;">
                                                <?php foreach ($room['adult'] as $adultkey => $adult) : ?>
                                                 <h4 style="color: #0091ea !important; margin-top: <?=($adultkey == 0)?'30px':'100px'; ?>; padding-left: 10px; padding-top: 9px; padding-bottom: 9px; ">Adult <?=$adultkey+1?></h4>
                                                <div class="col-md-3"  style="padding:0 !important;">
                                                    <label for="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][title]" style="padding-top: 10px !important;color: #1e1e1f;">Title</label>
                                                        <select  name="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][title]"
                                                            id="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][title]"
                                                            class="adjust-select"
                                                            style="width:98% !important;background-color:#f2f2f2; color: #1e1e1f;">
                                                            <?php foreach (array('Mr', 'Ms', 'Mrs') as $titlekey => $titlevalue) : ?>
                                                            	<option <?=($titlevalue == $adult['title'])?'selected=""':''?>><?=$titlevalue?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                </div>
                                               <div class="col-md-5" style="padding:10 !important;">
                                                    <label for="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][firstname]" style="padding-top: 10px !important;color: #1e1e1f;">First name</label>
                                                    <input type="text"
                                                        name="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][firstname]"
                                                        id="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][firstname]"
                                                        style="width:98% !important;background-color:#fff;"
                                                        class="adjust-input-text"
                                                        value="<?=$adult['firstname']?>">
                                                </div>
                                                <div class="col-md-4" style="padding:0 !important;">
                                                    <label for="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][lastname]" style="padding-top: 10px !important;color: #1e1e1f;">Last name</label>
                                                    <input type="text"
                                                        name="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][lastname]"
                                                        id="rooms[<?=$roomkey?>][adult][<?=$adultkey?>][lastname]"
                                                        style="width:98% !important;background-color:#fff; border-radius: 2px;"
                                                        class="adjust-input-text"
                                                        value="<?=$adult['lastname']?>">
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <!-- END OF ADULTS -->
                                            <?php if ( isset($room['child']) ) : ?>
                                            <div class="col-lg-6" style="padding: 4px !important;">
                                                <?php foreach ($room['child'] as $childKey => $childValue) : ?>
                                               <h4 style="color: #0091ea !important; padding-left: 10px; margin-top: <?=($childKey==0)?'25px':'100px'?>; margin-left: 0px; padding-top: 9px; padding-bottom: 9px;">Child <?=$childKey+1?></h4>
                                               <input type="hidden"
                                               		class="form-control"
                                               		id="rooms[<?=$roomkey?>][child][<?=$childKey?>][age]"
                                               		name="rooms[<?=$roomkey?>][child][<?=$childKey?>][age]"
                                               		value="<?=$childValue['age']?>">
                                               <div class="col-md-6" style="padding:10 !important;">
                                                    <label for="rooms[<?=$roomkey?>][child][<?=$childKey?>][firstname]" style="padding-top: 10px !important;color: #1e1e1f;">First name</label>
                                                    <input type="text"
                                                        id="rooms[<?=$roomkey?>][child][<?=$childKey?>][firstname]"
                                                        name="rooms[<?=$roomkey?>][child][<?=$childKey?>][firstname]"
                                                        style="width:98% !important;background-color:#fff;"
                                                        class="adjust-input-text"
                                                        value="<?=$childValue['firstname']?>">
                                                </div>
                                                <div class="col-md-6" style="padding:0 !important;">
                                                    <label for="rooms[<?=$roomkey?>][child][<?=$childKey?>][lastname]" style="padding-top:10px !important; color: #1e1e1f;">Last name</label>
                                                    <input type="text"
                                                        id="rooms[<?=$roomkey?>][child][<?=$childKey?>][lastname]"
                                                        name="rooms[<?=$roomkey?>][child][<?=$childKey?>][lastname]"
                                                        style="width:98% !important;background-color:#fff;"
                                                        class="adjust-input-text"
                                                        value="<?=$childValue['lastname']?>">
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <?php endif; ?>
                                            <!-- END OF CHILDRENS -->
                                        </div>
                                        <?php endforeach; ?>
                                    </form>
                                    <!-- END OF ROOM CONTAINER -->
                                     <div class="woocommerce-billing-fields">
                                        <div class="col-lg-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;">
                                            <h3 style="border-bottom: 4px solid #0091ea !important; background-color: #e6f3ff; color:#1e1e1f; padding-left: 13px; padding-top: 10px;padding-bottom: 10px">Special Requirements (Optional)</h3>
                                                 <label style="margin-top: 30px; color: #1e1e1f; margin-left: 30px;">Please write your requests in English</label>
                                              <textarea style="margin-top: 0px !important; border-color:#0091ea; width: 90%; margin-left: 30px;"
                                                id="request" name="request" value="<?=$post['request']?>"></textarea>

                                       </div>
                                    </div>

                                    <div class="woocommerce-billing-fields">
                                        <div class="col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;">
                                        <h3 style="border-bottom: 4px solid #0091ea!important; background-color: #e6f3ff; color:#1e1e1f;padding-top: 10px;padding-bottom: 10px;  padding-left: 13px;">Voucher Information</h3>
                                        <!-- ERRORS HERE -->
                                        	<?php if ( isset($errors['voucher']) && !empty($errors['voucher']) ) : ?>
                                        		<div class="alert alert-danger">
                                        			<ul>
                                        				<?php foreach ($errors['voucher'] as $key => $error) : ?>
                                        					<li><?=$error?></li>
                                        				<?php endforeach; ?>
                                        			</ul>
                                        		</div>
                                        	<?php endif; ?>
                                          <div class="col-md-2"  style="padding:0 !important;">
                                                <label style="padding-top: 30px !important;color: #1e1e1f;font-size: 14px; padding-left: 5px; margin-left: 20px;">Title</label>
                                                <select style="width:70% !important;background-color:#EEE;color: #1e1e1f; padding-left: 10px; margin-left: 20px;"
                                                    class="adjust-select" 
                                                    id="voucher[title]" name="voucher[title]">
                                                    <?php foreach (array('Mr', 'Ms', 'Mrs') as $titlekey => $titlevalue) : ?>
                                                    	<option <?=($titlevalue == $post['voucher']['title'])?'selected=""':''?>><?=$titlevalue?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5" style="padding: 8px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >First name</label>
                                                <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                    class="adjust-input-text"
                                                    id="voucher[firstname]" name="voucher[firstname]" value="<?=$post['voucher']['firstname']?>">
                                            </div>
                                             <div class="col-md-5" style="padding:8px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Last name</label>
                                                <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                    class="adjust-input-text"
                                                    id="voucher[lastname]" name="voucher[lastname]" value="<?=$post['voucher']['lastname']?>">
                                            </div>
                                             <div class="col-md-offset-2 col-md-10" style="padding:0 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 50px;font-size: 14px;">Email Address</label>
                                                <input type="email" style="width:74% !important;background-color:#fff;margin-left: 50px;"
                                                    class="adjust-input-text"
                                                    id="voucher[email]" name="voucher[email]" value="<?=$post['voucher']['email']?>">
                                            </div>
                                            <div class="col-md-offset-2 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Home Number</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text"
                                                    id="voucher[home]" name="voucher[home]" value="<?=$post['voucher']['home']?>">
                                            </div>
                                            <div class="col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Phone Number</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text"
                                                    id="voucher[phone]" name="voucher[phone]" value="<?=$post['voucher']['phone']?>">
                                            </div>
                                            <div class="col-md-offset-2 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Address</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text"
                                                    id="voucher[address]" name="voucher[address]" value="<?=$post['voucher']['address']?>">
                                            </div>
                                            <div class="col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">City</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text"
                                                    id="voucher[city]" name="voucher[city]" value="<?=$post['voucher']['city']?>">
                                            </div>
                                            <div class="col-md-offset-2 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Postal Code</label>
                                                <input type="number"
                                                    style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    id="voucher[postCode]"
                                                    name="voucher[postCode]"
                                                    class="adjust-input-text"
                                                    value="<?=$post['voucher']['postCode']?>">
                                            </div>
                                            <div class="col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Country Code</label>
                                                <select name="voucher[countryCode]"
                                                    id="voucher[countryCode]"
                                                    class="adjust-select" 
                                                    style="width:83% !important;background-color:#fff;margin-left: 33px;">
                                                    <option value="DE">DE</option>
                                                </select>
                                            </div>
                                       </div>
                                    </div>
                                    <!-- POLICY -->
                                    <div class="woocommerce-billing-fields">
                                        <h3 style="border-bottom: 4px solid #0091ea !important; background-color: #e6f3ff; color:#1e1e1f; padding-left: 13px; padding-top: 10px;padding-bottom: 10px; margin-top: 10px;">Cancellation Policy</h3>
                                        <div>
                                           <div class="col-md-12" >
                                                <?php
                                                    $policies = $booking_info['response'][0]['content']['policies'];
                                                    $refundable = null;
                                                    foreach ($policies as $key => $policy) : ?>
                                                        <?php if ( date('Y', strtotime($policy['endTime'])) == 1970 ) : ?>
                                                            <label  style="padding: 8px !important; margin-top: 20px; padding-bottom: 20px;" for="payment_method_bacs" ><?=ucfirst($policy['type'])?></label>
                                                            <div class="payment_box payment_method_bacs">
                                                                <p>This item is required to pay</p>
                                                            </div>
                                                        <?php else : ?>
                                                            <label  style="padding: 8px !important; margin-top: 20px; padding-bottom: 20px;" for="payment_method_bacs" ><?=ucfirst($policy['type'])?></label>
                                                            <div class="payment_box payment_method_bacs">
                                                                <p>From <?=((is_null($refundable))?"today's date":$refundable)?> date up to <strong><?=$policy['new_endTime']?></strong> this item is <strong><?=ucfirst($policy['type'])?></strong></p>
                                                            </div>
                                                        <?php if ( $policy['type'] == 'fully-refundable' ) { $refundable = $policy['new_endTime']; }?>
                                                        <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- PAYMENTS -->
                                    <!-- Put some logic about the activePayment form
                                    That will view -->
                                    <div>
                                        <button id="payable-button" type="button" class="btn btn-primary btn-block">Do you want to pay?</button>
                                    </div>
                                     <div id="customer-payment-form-wrapper" class="woocommerce-billing-fields">
                                        <h3 style="border-bottom: 4px solid #0091ea !important; background-color: #e6f3ff; color:#1e1e1f; padding-left: 13px; padding-top: 10px;padding-bottom: 10px; margin-top: 10px;">Credit Card Payment <button type="button" class="btn btn-primary btn-sm pull-right" id="customer-close-button"><span class="fa fa-close"></span></button></h3>
                                        <?php if ( isset($errors['cardPayment']) && !empty($errors['cardPayment']) ) : ?>
                                            <div class="alert alert-danger">
                                                <ul>
                                                    <?php foreach ($errors['cardPayment'] as $key => $error) : ?>
                                                        <li><?=$error?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;  ">
                                            <div class="col-md-6" style="padding: 8px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >Cardholder Name</label>
                                                    <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                        class="adjust-input-text"
                                                        id="creditCard[cardHolderName]" name="creditCard[cardHolderName]" value="<?=$post['creditCard']['cardHolderName']?>">
                                                 <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Email Address</label>
                                                    <input type="text" style="width:78% !important;background-color:#fff;margin-left: 40px;"
                                                        class="adjust-input-text"
                                                        id="creditCard[email]" name="creditCard[email]" value="<?=$post['creditCard']['email']?>">
                                                <label style="padding-top: 25px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Card Verification Number</label>
                                                    <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                        class="adjust-input-text"
                                                        id="creditCard[carVerNum]" name="creditCard[carVerNum]" value="<?=$post['creditCard']['carVerNum']?>">
                                            </div>
                                            <?php $startDate = date('y', strtotime('now')); ?>
                                             <div class="col-md-6" style="padding:8px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Card Number</label>
                                                    <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                        class="adjust-input-text"
                                                        id="creditCard[cardNum]" name="creditCard[cardNum]" value="<?=$post['creditCard']['cardNum']?>">
                                                <label style="color: #1e1e1f;padding-left: 40px;font-size: 14px;padding-top: 20px;">Payment Amount</label>
                                                    <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                        class="adjust-input-text"
                                                        id="creditCard[paymentAmount]" name="creditCard[paymentAmount]" value="<?=$post['creditCard']['paymentAmount']?>">
                                                <div style="padding-left: 40px;">
                                                    <label style="padding-top: 25px;color: #1e1e1f;font-size: 14px;" class="col">Expiry Date</label>
                                                    <br>
                                                    <select name="creditCard[expiration][month]" id="" class="adjust-select">
                                                        <option value="">-- Month --</option>
                                                        <?php for($m=1; $m<=12; ++$m): ?>
                                                            <option value="<?=($m < 10)?'0' . $m: $m?>" <?=($m == $post['creditCard']['expiration']['month'])?'selected':''?>><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                                        <?php endfor; ?>
                                                    </select>
                                                    <select name="creditCard[expiration][year]" id="" class="adjust-select">
                                                        <option value="">-- Year --</option>
                                                        <?php foreach (range(intval($startDate), intval($startDate) + 10) as $key => $value) : ?>
                                                            <option value="<?=$value?>" <?=($value == $post['creditCard']['expiration']['year'])?'selected':''?>><?=$value?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                       </div>

                                        <!-- <ul class="adjust-nav nav nav-tabs" role="tablist">
                                            <li role="presentation" class="<?=( isset($errors['cardPayment']) && !empty($errors['cardPayment']) )?'active':''?>">
                                                <a href="#creditCard"
                                                    aria-controls="creditCard"
                                                    role="tab"
                                                    class="customer-payment-form"
                                                    data-value="credit"
                                                    data-toggle="tab">Credit Card Payments</a>
                                            </li>
                                            <li role="presentation" class="<?=( isset($errors['epayment']) && !empty($errors['epayment']) )?'active':''?>">
                                                <a href="#enet"
                                                    aria-controls="enet"
                                                    role="tab"
                                                    class="customer-payment-form"
                                                    data-value="epayment"
                                                    data-toggle="tab">EFT E-Net Payments</a>
                                            </li>
                                        </ul>
                                        
                                        <div class="tab-content">
                                            CREDIT CARD PAYMENT
                                        
                                            <div role="tabpanel" class="tab-pane <?=( isset($errors['cardPayment']) && !empty($errors['cardPayment']) )?'active':''?>" id="creditCard">
                                                <?php if ( isset($errors['cardPayment']) && !empty($errors['cardPayment']) ) : ?>
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            <?php foreach ($errors['cardPayment'] as $key => $error) : ?>
                                                                <li><?=$error?></li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;  ">
                                                    <div class="col-md-6" style="padding: 8px !important;">
                                                        <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >Cardholder Name</label>
                                                            <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                                class="adjust-input-text"
                                                                id="creditCard[cardHolderName]" name="creditCard[cardHolderName]" value="<?=$post['creditCard']['cardHolderName']?>">
                                                         <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Email Address</label>
                                                            <input type="text" style="width:78% !important;background-color:#fff;margin-left: 40px;"
                                                                class="adjust-input-text"
                                                                id="creditCard[email]" name="creditCard[email]" value="<?=$post['creditCard']['email']?>">
                                                        <label style="padding-top: 25px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Card Verification Number</label>
                                                            <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                                class="adjust-input-text"
                                                                id="creditCard[carVerNum]" name="creditCard[carVerNum]" value="<?=$post['creditCard']['carVerNum']?>">
                                                    </div>
                                                    <?php $startDate = date('Y', strtotime('now')); ?>
                                                     <div class="col-md-6" style="padding:8px !important;">
                                                        <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Card Number</label>
                                                            <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                                class="adjust-input-text"
                                                                id="creditCard[cardNum]" name="creditCard[cardNum]" value="<?=$post['creditCard']['cardNum']?>">
                                                        <label style="color: #1e1e1f;padding-left: 40px;font-size: 14px;padding-top: 20px;">Payment Amount</label>
                                                            <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                                class="adjust-input-text"
                                                                id="creditCard[paymentAmount]" name="creditCard[paymentAmount]" value="<?=$post['creditCard']['paymentAmount']?>">
                                                        <div class="row text-center">
                                                            <label style="padding-top: 25px;color: #1e1e1f;font-size: 14px;" class="col">Expiry Date</label>
                                                            <br>
                                                            <select name="creditCard[expirationFrom][month]" id="" class="adjust-select" style="width: 7.5em;">
                                                                <?php for($m=1; $m<=12; ++$m): ?>
                                                                    <option value="<?=$m?>" <?=($m == $post['creditCard']['expirationFrom']['month'])?'selected':''?>><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                                                <?php endfor; ?>
                                                            </select>
                                                            <select name="creditCard[expirationFrom][year]" id="" class="adjust-select">
                                                                <?php foreach (range(intval($startDate), intval($startDate) + 5) as $key => $value) : ?>
                                                                    <option value="<?=$value?>" <?=($value == $post['creditCard']['expirationFrom']['year'])?'selected':''?>><?=$value?></option>
                                                                <?php endforeach; ?>
                                                            </select> - 
                                                            <select name="creditCard[expirationTo][month]" id="" class="adjust-select" style="width: 7.5em;">
                                                                <?php for($m=1; $m<=12; ++$m): ?>
                                                                    <option value="<?=$m?>" <?=($m == $post['creditCard']['expirationTo']['month'])?'selected':''?>><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                                                <?php endfor; ?>
                                                            </select>
                                                            <select name="creditCard[expirationTo][year]" id="" class="adjust-select">
                                                                <?php foreach (range(intval($startDate), intval($startDate) + 5) as $key => $value) : ?>
                                                                    <option value="<?=$value?>" <?=($value == $post['creditCard']['expirationTo']['year'])?'selected':''?>><?=$value?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane <?=( isset($errors['epayment']) && !empty($errors['epayment']) )?'active':''?>" id="enet">
                                                <?php if ( isset($errors['epayment']) && !empty($errors['epayment']) ) : ?>
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            <?php foreach ($errors['epayment'] as $key => $error) : ?>
                                                                <li><?=$error?></li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;  ">
                                                    <div class="col-md-6" style="padding: 8px !important;">
                                                        <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >E-Net Payment Date</label>
                                                            <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                                class="adjust-input-text" 
                                                                id="ePaymentdate" name="ePayment[date]"
                                                                value="<?=$post['ePayment']['date']?>">
                                                         <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Reference</label>
                                                            <input type="text" style="width:78% !important;background-color:#fff;margin-left: 40px;"
                                                                class="adjust-input-text" 
                                                                id="ePayment[reference]" name="ePayment[reference]"
                                                                value="<?=$post['ePayment']['reference']?>">
                                                        <label style="padding-top: 25px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Amount</label>
                                                            <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                                class="adjust-input-text" 
                                                                id="ePayment[amount]" name="ePayment[amount]"
                                                                value="<?=$post['ePayment']['amount']?>">
                                                    </div>
                                               </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div id="payment" style="margin-top: 0px;">
                                        <div>
                                            <h3 id="ship-to-different-address">
                                                <input id="ship-to-different-address-checkbox" type="checkbox" name="policy_read" value="1" <?=(isset($post['policy_read']) && $post['policy_read'] == 1 )?'checked=""':''?>>
                                                <label style="color: #0091ea !important; margin-top: 40px;font-size: 14px;" for="ship-to-different-address-checkbox">I have read the cancellation policy</label>
                                                
                                            </h3>
                                        </div>
                                        <div class="form-row place-order">
                                            <input type="submit" class="button alt" id="place_order" value="Book Now">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php endif; ?>
                     <div class="col-lg-4">
                        <div class="booking-info" style="border: 4px solid #0091ea !important; background-color: #cce6ff;">
                            <h3>Booking Info</h3>
                            <div class="row" style="color: #000;">
                                <?php foreach ($booking_info['response'][0]['content']['booking-options'][0]['content']['booking-option'] as $key => $bookingOption) : ?>
                                    <?php
                                        $itemPrice = ( isset($bookingOption['content']['room'][0]['price'][0]) )? $bookingOption['content']['room'][0]['price'][0]: $bookingOption['content']['room'][0]['content']['price'][0];
                                        $itemName  = ( isset($bookingOption['content']['room'][0]['name'][0]) )? $bookingOption['content']['room'][0]['name'][0]: $bookingOption['content']['room'][0]['content']['name'][0];
                                        $itemBoard = ( isset($bookingOption['content']['room'][0]['board'][0]['content']) )? $bookingOption['content']['room'][0]['board'][0]['content']: $bookingOption['content']['room'][0]['content']['board'][0]['content'];
                                    ?>
                                    <div class="col-md-12">
                                        <div class="col-md-7"><?=$itemName?><br><em style="font-size: .7em;">(<?=$itemBoard?>)</em></div>
                                        <div class="col-md-5"><span class="fa fa-<?=($itemPrice['new_currency'] != 'AUD')?strtolower($itemPrice['new_currency']):'usd'?>" style="background-color: transparent;"></span>&nbsp;<?=number_format($itemPrice['calculation']['calculation']['due_amount'], 2, '.', '')?></div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="price">
                                <em>Total for this booking</em>
                                <span class="amount"><span class="fa fa-usd" style="background-color: transparent;"></span> <?=number_format($booking_info['response'][0]['content']['calculation']['calculation']['due_amount'], 2, '.', '')?></span>
                                <div class="cart-added">
                                    <i class="awe-icon awe-icon-check"></i>
                                    Added
                                </div>
                            </div>
                        <!--    <div class="form-submit" style="left: -4px !important;">
                                <div class="add-to-cart">
                                    <button type="submit" style="background-color: #0091ea !important;left: -9;">
                                        <i class="awe-icon awe-icon-cart"></i>Add this to Cart
                                    </button>
                                </div>
                            --> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
