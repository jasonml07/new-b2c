 <!-- HEADING PAGE -->
        <section class="awe-parallax category-heading-section-demo">
            <div class="awe-overlay"></div>
            <div class="container">
                <div class="category-heading-content category-heading-content__2 text-uppercase">

                    <!-- BREADCRUMB -->
                    <div class="find">
                        <h2 class="text-center">Find Your Hotel</h2>
                        <form >
                            <div class="form-group">
                                <div class="form-elements">
                                    <label>Destination</label>
                                    <div class="form-item">
                                        <i class="awe-icon awe-icon-marker-1"></i>
                                        <input id="destination_area" type="text" required>
                                     <input id="destination_code" type="hidden" name="destination_code" value="" required>
                                    </div>
                                </div>
                                 <div class="form-elements">
                                    <label>Hotel Name</label>
                                    <div class="form-item">
                                        <i class="awe-icon awe-icon-marker-1"></i>
                                       <input id="hotel_area" type="text">
                                     <input id="hotel_code" type="hidden" name="hotel_code">
                                    </div>
                                </div>
                                <div class="form-elements">
                                    <label>Date from</label>
                                    <div class="form-item">
                                        <i class="awe-icon awe-icon-calendar"></i>
                                        <input type="text"  id="start" value="" name="checkin_date" required />
                                    </div>
                                </div>
                                <div class="form-elements">
                                    <label>Date to</label>
                                    <div class="form-item">
                                        <i class="awe-icon awe-icon-calendar"></i>
                                        <input type="text"  id="end" value="" name="checkout_date" required />
                                    </div>
                                </div>   
                                  <div class="form-actions">
                                    <input id="submit_search"  type="submit" value="Find My Hotel">
                                     <a target="_blank" href = ""> <span id = "" class="label label-success" style="font-size: 13px;position: absolute;right: 16px; top: -120px;width:180px;margin-bottom: 30px;">Go to your site <img src="/static/images/a.png" style="width:20%;"></span></a>
                                </div>
                              <!--  </div> 
                             <div class="form-group">       -->                              
                                <div class="form-elements ">
                                    <label>Number of Rooms </label>
                                    <div class="form-item">
                                        <select class="awe-select" onChange="travelrez.room(this.value)"  id="rooms" name="rooms" >
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4" >4</option> 
                                            <option value="5" >5</option> 
                                        </select>
                                    </div>
                                </div>
                                <div class="form-elements">
                                     <div id="roomPax1" style="display:block;">
                                      <label>Room 1</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult1"   name="rooms[1][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                                 </div>
                                             <div class="form-item">
                                            Child  <select class="awe-select" id='child1'   name="rooms[1][child]" onChange='travelrez.child_fnc(this.value,1)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                     </div>
                                    <div id="age1" ></div> 
                                    </div>
                                </div>
                                <div class="form-elements">
                                    <div id="roomPax2" style="display:none;">
                                        <label>Room 2</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult2"   name="rooms[2][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child2'   name="rooms[2][child]" onChange='travelrez.child_fnc(this.value,2)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                        </div>
                                         <div id="age2" ></div> 
                                    </div>
                                </div>
                                <div class="form-elements">
                                    <div id="roomPax3" style="display:none;">
                                        <label>Room 3</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult3"   name="rooms[3][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child3'   name="rooms[3][child]" onChange='travelrez.child_fnc(this.value,3)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                        </div>
                                         <div id="age3" ></div> 
                                    </div>
                                </div>
                                <div class="form-elements">
                                    <div id="roomPax4" style="display:none;">
                                        <label>Room 4</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult4"   name="rooms[4][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child4'   name="rooms[4][child]" onChange='travelrez.child_fnc(this.value,4)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                        </div>
                                          <div id="age4" ></div> 
                                    </div>
                                </div>
                                <div class="form-elements">
                                    <div id="roomPax5" style="display:none;">
                                        <label>Room 5</label>
                                        <div class="form-item">
                                            Adult <select class="awe-select" id="adult5"   name="rooms[5][adult]">
                                                        <option value="1">1</option> 
                                                        <option value="2" selected="selected">2</option> 
                                                        <option value="3">3</option>
                                                        <option value="4">4</option> 
                                                        <option value="5">5</option>  
                                                 </select> 
                                            Child  <select class="awe-select" id='child5'   name="rooms[5][child]" onChange='travelrez.child_fnc(this.value,5)'>
                                                        <option value="0">0</option> 
                                                        <option value="1">1</option> 
                                                        <option value="2">2</option>
                                                        <option value="3">3</option> 
                                                        <option value="4">4</option> 
                           
                                                   </select>
                                                  
                                        </div>
                                        <div id="age5" ></div> 
                                    </div>
                                </div>



                                

                            </div> <!-- End of Form-Group -->
                        </form>
                    </div> <!-- End of find div -->
                    
                </div>
            </div>
        </section>
        <!-- END / HEADING PAGE -->

        
            <!-- MASONRY -->
        <section class="masonry-section-demo">
            <div class="container">
                <div class="destination-grid-content">
                    <div class="section-title">
                        <h3>More than <a href="destinations-grid.html">238 Destinations</a> is waiting</h3>
                    </div>
                    <div class="row">
                        <div class="awe-masonry">
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/1.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Florenze</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">Italy</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">845</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/2.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Toluca</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">USA</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">132</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/3.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Venice</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">Italy</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">2341</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/4.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Ohio</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">USA</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">2531</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/5.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Venice</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">Italy</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">2531</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/6.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Mandives</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">Mandives</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">2531</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/7.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Istanbul</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">Turkey</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">2531</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/8.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Bali</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">Thailand</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">2531</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                            <!-- GALLERY ITEM -->
                            <div class="awe-masonry__item">
                                <a href="#">
                                    <div class="image-wrap image-cover">
                                        <img src="<?php echo base_url(); ?>static/images/img/9.jpg" alt="">
                                    </div>
                                </a>
                                <div class="item-title">
                                    <h2><a href="#">Phu Quoc</a></h2>
                                    <div class="item-cat">
                                        <ul>
                                            <li><a href="#">Vietnam</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item-available">
                                    <span class="count">2531</span>
                                    available hotel
                                </div>
                            </div>
                            <!-- END / GALLERY ITEM -->
                        </div>
                    </div>
                    <div class="more-destination">
                        <a href="#">More Destinations</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / MASONRY -->
