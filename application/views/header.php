<body>

<!-- PAGE WRAP -->
    <div id="page-wrap">
        <!-- PRELOADER -->
        <div class="preloader"></div>
        <!-- END / PRELOADER -->

        <!-- HEADER PAGE -->
        <header id="header-page">
            <div class="header-page__inner">
                <div class="container">
                    <!-- LOGO -->
                    <div class="logo">
                        <a href="#"><strong style="color: #fff; font-size: 20px;"><i class="glyphicon glyphicon-earphone"></i> 800 242 353 </strong></a>
                    </div>
                    <!-- END / LOGO -->


                    <nav class="navigation awe-navigation" data-responsive="1200">
                        <ul class="menu-list">
                            <li class="current-menu-parent">
                                <a href="<?php echo base_url();?>" class="bx-nav-fcolor">Home</a>
                            </li>
                            <?php
                                if($this->uri->segment(1) !== 'agency') {
                            ?>
                            <!--
                                <li class="current-menu-parent">
                                    <a id="tripLink" href="#tripLink" class="bx-nav-fcolor">Services</a>
                                        <ul class="sub-menu">
                                            <li><a href="#">Trip</a></li>
                                            <li><a href="#">Hotel</a></li>
                                            <li><a href="#">Flight</a></li>
                                            <li><a href="#">Train</a></li>
                                            <li><a href="#">Attraction</a></li>
                                            <li><a href="#">Destination</a></li>
                                        </ul>
                                </li>
                                -->
                                <!--
                                <li class="current-menu-parent">
                                    <a id="aboutUsLink" href="#aboutUs" class="bx-nav-fcolor">About Us</a>
                                </li>

                                <li class="current-menu-parent">
                                    <a id="newsLink" href="#news" class="bx-nav-fcolor">News</a>
                                </li>
                                <li class="current-menu-parent">
                                    <a id="testimonialsLink" href="#testimonials" class="bx-nav-fcolor">Testimonials</a>
                                </li>

                                -->
                            <?php
                                }

                            ?>
                            <li class="current-menu-parent">
                                <a id="contactLink" href="#contact" class="bx-nav-fcolor">Contact</a>
                            </li>
<!--                             <li class="menu-item-has-children">
                                <a href="hotel.html">Hotel</a>
                                <ul class="sub-menu">
                                <li><a href="hotel-detail.html">Hotel Dashboard</a></li>
                                    <li><a href="hotel-detail.html">Hotel Detail</a></li>
                                </ul>
                            </li> -->
                            <?php 
                            if($this->session->userdata('logged_in') == 1){
                                $link = '<li class="current-menu-parent user">';
                                $link.= '   <a class="glyphicon glyphicon-user"> Hi '.$this->session->userdata('username').'</a>';
                                $link.=     '<ul class="sub-menu">
                                <li><a href="index/logout">Logout</a></li>
                                </ul>';
                                $link.= '</li>';

                                echo $link;
                               
                            }
                                 
                            ?>
 
                        </ul>
                    </nav>
                    <!-- END / NAVIGATION -->
                    
                   <!-- 
                    <div class="search-box">
                        <span class="searchtoggle"><i class="glyphicon glyphicon-search"></i></span>
                        <form class="form-search">
                            <div class="form-item">
                                <input type="text" value="Search &amp; hit enter">
                            </div>
                        </form>
                    </div>
                    -->


                    
                    <a class="toggle-menu-responsive" href="#">
                        <div class="hamburger">
                            <span class="item item-1"></span>
                            <span class="item item-2"></span>
                            <span class="item item-3"></span>
                        </div>
                    </a>
                   
                </div>
            </div>
        </header>
        