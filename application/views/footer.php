 <!-- FOOTER PAGE -->
        <footer id="footer-page" class="divs">
            <a name="contact"></a>
            <div class="container">
                <div class="row">

                    <!-- WIDGET -->
                    <div class="col-md-4">
                        <div class="widget widget_about_us">
                            <h3>Contact Us</h3>
                            <div class="widget_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form>
                                                <h5 style="color:#EDEDED;">
                                                    <span class="fa fa-globe"></span>
                                                    RESERVATION Office
                                                </h5>
                                                <address>
                                                 
                                                    <br>
                                                    1/66 Appel St, Surfers Paradise,
                                                    <br>
                                                    QLD 4217, Australia
                                                    <br>
                                                    <label> Phone:</label>
                                                    (07) 5526 2855
                                                    <br>
                                                    <label> Fax:</label>
                                                    (07) 5526 2944
                                                    <br>
                                                    <label> Toll Free:</label>
                                                    1800 242 353
                                                    <br>
                                                </address>
                                                <address>
                                                    </address>
                                                    <address>
                                                    <strong>Hours of Operation </strong>
                                                    <br>
                                                    Mon-Fri: 08.00 am – 17.30 pm
                                                    <br>
                                                    Sat: 24 hour Contact 0433 161 250
                                                    <br>
                                                    Sun: 24 hour Contact 0433 161 250
                                                    <br>
                                                </address>
                                            </form>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="well well-sm" style="background-color: #333;border:0px solid #FFF">
                                                <form accept-charset="utf-8" method="post" action="<?php echo base_url();?>php_mailer/sendMail ">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label> Name</label>
                                                                <input id="name" class="form-control" type="text" required="required" placeholder="Enter name" style="border:1px solid #ddd" name="name" value="<?php echo set_value('name'); ?>">
                                                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="email"> Email Address</label>
                                                                <input id="email" class="form-control" type="email" required="required" placeholder="Enter email" style="border:1px solid #ddd" name="email" value="<?php echo set_value('email'); ?>">
                                                                <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="subject"> Subject</label>
                                                                <input id="subject" class="form-control" type="text" required="required" placeholder="Enter subject" name="subject" style="border:1px solid #ddd" value="<?php echo set_value('subject'); ?>">
                                                                <span class="text-danger"><?php echo form_error('subject'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="name"> Message</label>
                                                                <textarea id="message" class="form-control" placeholder="Message" required="required" cols="25" rows="9" style="border:1px solid #ddd;border-radius: 0px" name="message" value="<?php echo set_value('message'); ?>"></textarea>
                                                                <span class="text-danger"><?php echo form_error('message'); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="qna">What is 3 + 2?</label>
                                                                <input id="qna" class="form-control" type="text" required="required" placeholder="Answer" name="captcha" style="border:1px solid #ddd">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-md-4">
                                                            <button id="btnContactUs" class="btn btn-primary" type="submit"> Send Message</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / WIDGET -->
                </div>
                        <div class="widget widget_follow_us" style="text-align: center;">
                            <div class="widget_content">
                                <div class="awe-social">
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-youtube-play"></i></a>
                                </div>
                            </div>
                        </div>
                <div class="copyright">
                    <p>©2019 TravelRez™ All rights reserved.</p>
                </div>
            </div>
        </footer>
        <!-- END / FOOTER PAGE -->

    </div>
    <!-- END / PAGE WRAP -->

     <!-- LOAD JQUERY -->
  
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery.magnific-popup.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url();?>static/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/wow.min.js"></script>
    <!--load bootstrap.js-->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.min.js"></script>

    <!-- load form validation -->
    <script type="text/javascript" src="<?php echo base_url();?>static/js/formValidation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/js/bootstrap.js"></script>

    <!-- REVOLUTION DEMO -->
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>static/revslider-demo/js/jquery.themepunch.tools.min.js"></script>
    
    <!-- Smooth Scroll -->
    <script>  
        jQuery.noConflict();
        (function($){
            $(document).ready(function() {
          function filterPath(string) {
            return string
              .replace(/^\//,'')  
              .replace(/(index|default).[a-zA-Z]{3,4}$/,'')  
              .replace(/\/$/,'');
          }
          $('a[href*=#]').each(function() {
            if ( filterPath(location.pathname) == filterPath(this.pathname)
            && location.hostname == this.hostname
            && this.hash.replace(/#/,'') ) {
              var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
              var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
               if ($target) {
                 var targetOffset = $target.offset().top;
                 $(this).click(function() {
                   $('html, body').animate({scrollTop: targetOffset}, 1000);
                   return false;
                 });
              }
            }
          });
        });
        })(jQuery);
    </script>
    <!-- WOW.JS-->
    <script type="text/javascript">
              new WOW().init();
    </script>
    <!-- ERROR MESSAGE CLOSE -->
    <script type="text/javascript">
        jQuery.noConflict();
        (function($){
             $(document).ready(function(){
        $(".close").click(function(){
            $("#alert-message").hide();
        });
    });  
        }(jQuery));     
    </script>

    <!-- TERMS MODAL -->
    <script type="text/javascript">

        jQuery.noConflict();
        (function($){
             $(document).ready(function(){
        $("#terms").click(function(){
            $("#terms_modal").modal('show');
        });
    });  
        }(jQuery));     
  
    </script>

    <!-- Agency Sign Up Form Validation-->

    <script type="text/javascript">

            jQuery.noConflict();
        (function($){


             $(document).ready(function() {
                $('#agencyForm').formValidation({
                    message: 'This value is not valid',
                    icon: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        agency_name: {
                            message: 'The agency name is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The agency name is required and can\'t be empty'
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z0-9_\.]+$/,
                                    message: 'The agency name can only consist of alphabetical, number, dot and underscore'
                                }
                            }
                        },
                        agency_code: {
                            validators: {
                                notEmpty: {
                                    message: 'The agency code is required and can\'t be empty'
                                }
                            }
                        },
                 
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'The email address is required and can\'t be empty'
                                },
                                emailAddress: {
                                    message: 'The input is not a valid email address'
                                }
                            }
                        },
                        phone: {
                            validators: {
                                phone: {
                                    message: 'The input is not a valid phone number'
                                }
                            }
                        }, 
                        country: {
                            validators: {
                                notEmpty: {
                                    message: 'The country is required and can\'t be empty'
                                }
                            }
                        },
                        b_email:{
                            validators:{
                                notEmpty: {
                                    message: 'The email address is required and can\'t be empty'
                                },
                                emailAddress: {
                                    message: 'The input is not a valid email address'
                                }
                            }

                        },
                        agree: {
                            validators: {
                                notEmpty: {
                                    message: 'You have to accept the terms and policies'
                                }
                            }
                        }
                    }
                });
            });



        }(jQuery));    


   
</script>
    <script type="text/javascript">
        jQuery.noConflict();
        (function($){
             $(document).ready(function(){
              $('.slider-main').bxSlider({
                  mode: 'horizontal',
                  captions: true,
                  autoplay: true

              });
            });
        }(jQuery));
    </script>

    <script type="text/javascript">
        jQuery.noConflict();
        (function($){
            if($('#slider-revolution').length) {
                $('#slider-revolution').show().revolution({
                    ottedOverlay:"none",
                    delay:10000,
                    startwidth:1600,
                    startheight:650,
                    hideThumbs:200,

                    thumbWidth:100,
                    thumbHeight:50,
                    thumbAmount:5,
                    
                                            
                    simplifyAll:"off",

                    navigationType:"none",
                    navigationArrows:"solo",
                    navigationStyle:"preview4",

                    touchenabled:"on",
                    onHoverStop:"on",
                    nextSlideOnWindowFocus:"off",

                    swipe_threshold: 0.7,
                    swipe_min_touches: 1,
                    drag_block_vertical: false,
                    
                    parallax:"mouse",
                    parallaxBgFreeze:"on",
                    parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                                            
                                            
                    keyboardNavigation:"off",

                    navigationHAlign:"center",
                    navigationVAlign:"bottom",
                    navigationHOffset:0,
                    navigationVOffset:20,

                    soloArrowLeftHalign:"left",
                    soloArrowLeftValign:"center",
                    soloArrowLeftHOffset:20,
                    soloArrowLeftVOffset:0,

                    soloArrowRightHalign:"right",
                    soloArrowRightValign:"center",
                    soloArrowRightHOffset:20,
                    soloArrowRightVOffset:0,

                    shadow:0,
                    fullWidth:"on",
                    fullScreen:"off",

                    spinner:"spinner2",
                                            
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,

                    shuffle:"off",

                    autoHeight:"off",
                    forceFullWidth:"off",
                    
                    
                    
                    hideThumbsOnMobile:"off",
                    hideNavDelayOnMobile:1500,
                    hideBulletsOnMobile:"off",
                    hideArrowsOnMobile:"off",
                    hideThumbsUnderResolution:0,

                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    startWithSlide:0
                });
            }
        }(jQuery));
    </script>

    <script type="text/javascript">
        jQuery.noConflict();
        (function($){
            $(document).ready(function() {
                $( "#destination_area" ).autocomplete({
                    source: "<?php echo base_url(); ?>_ajax/execute_destination.php?action=getDestinationHB",
                    minLength: 3,
                    select: function( event, ui ) { 
                        event.preventDefault()
                        $(this).val(ui.item.label);
                        $("#destination_code").val(ui.item.value);
                        $( "#hotel_area" ).autocomplete('option', 'source', "<?php echo base_url(); ?>_ajax/execute_products.php?action=getHotelProductsHB&destination_code="+ui.item.value)
                          //document.getElementById("country_code").value = ui.item.code;
                    }
                });
                $( "#hotel_area" ).autocomplete({
                    minLength: 1,
                    select: function( event, ui ) { 
                        event.preventDefault()
                        $(this).val(ui.item.label);
                        $("#hotel_code").val(ui.item.value);
                          //document.getElementById("country_code").value = ui.item.code;
                    }
                });

                var forDate = new Date();
                forDate.setDate(forDate.getDate()+2);
                var forDate2 = new Date();
                forDate2.setDate(forDate2.getDate()+3);

                $( "#start" ).datepicker({
                    dateFormat: 'dd/mm/yy',
                    numberOfMonths: 2,
                    minDate: forDate,
                    onClose: function( selectedDate ) {
                       var nDate = selectedDate.split('/')
                                  var ffdate = new Date(nDate[1]+"/"+nDate[0]+"/"+nDate[2]);
                                   ffdate.setDate(ffdate.getDate()+1);

                                  var fthisdate = ffdate.getDate() + '/' + (ffdate.getMonth() +1 ) + '/' +  ffdate.getFullYear(); 
                                   
                                  $('#end').val(fthisdate); 
                    $( "#end" ).datepicker( "option", "minDate", selectedDate );
                       
                    }
                  });
                $( "#end" ).datepicker({
                    dateFormat: 'dd/mm/yy',
                    numberOfMonths: 2,
                    minDate: forDate,
                    onClose: function( selectedDate ) {
                    //$( "#start" ).datepicker( "option", "maxDate", selectedDate );
                    }
                  });

            });

            $('#submit_search').click(function(){

                var forms = $('#form_search').serialize(); 
                
                        if($('#destination_code').val() != "" && $('#start').val() !="" && $('#end').val() !="") {
                            $.ajax({
                                    type:"GET",
                                    url:"<?php echo base_url(); ?>_ajax/execute_search_hb_manager.php?action=doSearchHB",
                                    data:forms, 
                                    dataType:'json',
                                    success:function(data){ 
                                        if(data.status=="ok"){
                                           alert(error);
                                        }else{
                                          alert(error);

                                        }
                                          
                                  
                                    },error:function(data){
                                     alert("error");
                                    }
                            }); 
                      
                    } 
            });
             

            var travelrez = function () {
                return {
                    room: function(roomVal){
                        var roomVal = parseFloat(roomVal);
                        for(var x=1;x<=roomVal;x++){
                            document.getElementById("roomPax"+x).style.display = "block";
                        }
                        var xRoom = parseFloat(roomVal+1);
                        for(var y=xRoom;y<=5;y++){
                            
                            document.getElementById("roomPax"+y).style.display = "none";
                        }
                                
                    },
                    child_fnc: function(numCHild,roomNum){
                        var row_min= $('#age'+roomNum).html('');

                         if(numCHild != '0'){ 
                              for(var x = 0; x<numCHild; x++){
                                 row_min.append('Age <input type="number" name="rooms['+roomNum+'][childAge][]"  value="8" required>');
                            }
                           
                         } 
                                
                    }

                
                }
            }();   
        }(jQuery));      
    </script>

</body>
</html>