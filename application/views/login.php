 

 <section class="awe-parallax login-page-demo">
            <div class="awe-overlay"></div>
            <div class="list-forstart fin_1 wow bounceInLeft" style="opacity: 1;float:left; margin-left: 90px;margin-top: 200px;">
                <img src="http://travelrez.net.au/assets/img/travel.png">
                <h2 class="h-Bold" style="font-size:40px;font-family: 'OpenSans',Sans-serif; font-weight: 100; color: #fff;">Designed For The Travel Agent</h2>
            </div>
            <div class="container">
                <div class="login-register-page__content wow bounceInRight">
                    <div class="content-title"></div>
                      

                      <?php echo validation_errors(); ?>
                      <?php echo form_open('index/verifylogin'); ?>
                      <?php echo $this->session->flashdata('loginError'); ?>
                      <?php echo $this->session->flashdata('mailSent'); ?>
                      <?php echo $this->session->flashdata('mailError'); ?>  

                        <div class="form-item">
                            <label for="username">Username</label>
                            <input type="text" id="username" name="username" placeholder="Username">
                        </div>
                        <div class="form-item">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" placeholder="Enter Password">
                        </div>
                        <a href="#" class="forgot-password">Forgot Password</a>
                        <div class="form-actions">
                            <input type="submit" value="Log In">
                        </div>
                    </form>
                    <div class="login-register-link">
                        Dont have an account yet? <a href="<?php base_url();?>agency">Register HERE</a>
                    </div>
                </div>
            </div>
</section>

<section class="container" style="font:'OpenSans';">
    <a name="aboutus" class="smooth"></a>
    <div class="spacer1"></div>
    <h2 class="text-center">
    <div class="row"></div>
    <h2 class="text-center xxh-Bold">
    <a name="aboutUs">
    Travel
    <span style="color:red;margin-left:-10px">Rez</span>
    - Designed for the Travel Agent
    </a>
    </h2>

    <h5 class="text-center xmedium-h">22 Years Experience – True Travel Agent Partners</h5>
    <div id="about"
     class="row   wow bounceIn">
    <div class="col-md-12 col-xs-12 " style="margin-bottom: 40px;">
     <div class="full-text text-center" > Is a wholesale reservation platform developed by Eastern Eurotours’ dedicated online team. This comprehensive hotel booking site was designed exclusively with the Australian Travel Agent in mind. Eastern Eurotours & Mediterranean Holidays is a well‐established Australian tour operator, specialising in travel products to Europe and the Mediterranean.</div>
    </div>
    <div class="col-md-12 col-xs-12">
     <div class="full-text text-center"> Offers you the possibility to search over 200,000 unique properties around the world and book your clients at the best available rates, meticulously outsourced through a vast network of worldwide hotel contractors. Our range of properties is continuously growing and we have now incorporated ground transfers and tours into the mix, giving you the opportunity to build complex itineraries through a shopping cart mechanism. We will continue to add even more product over the coming months. </div>
    </div>
    </div>
    <div class="offsetY-4"></div>
</section>
<section>
    <a name="news" class="smooth"></a>
    <div class="row">
        <div class="container make-row">
            <div class="row">
                <div class="col-sm-6 wow bounceInLeft" >
                    <h2 class="xh-Bold"><a name="news">MORE GREAT NEWS! Use TravelRez on Your Own Website! </a></h2>
                    <div class="excerpt">
                        Let your internet savvy clients book their accommodation on your exclusive site and earn great commissions! TravelRez now offers you a fully branded retail-booking / white-label tool that you can add to your web site or send out to clients in your email promotions. This personalised link allows you to set your own client discounts and best of all, we will build the booking site to match the rest of your site at NO additional cost to you. Control your pricing & compete with the big guys whilest still earning great commissions. Interested? Please email us at
                        <a href="mailto:info@travelrez.net.au">info@travelrez.net.au</a>
                    </div>
                </div>
                <div class="col-sm-6 wow bounceInRight">
                    <h2 class="xh-Bold">Eastern Eurotours</h2>
                    <div class="excerpt">
                        dedicated ski division “
                        <a href="http://www.skitheworld.net.au">www.SkiTheWorld.net.au</a>
                        ” offers you the largest and most diverse range of ski & snowboard packages throughout Europe and gives you the possibility to earn extra commissions from booking ski school, ski and snowboard rentals, ski passes and resort transfers in many of its featured destinations.
                        <br>
                        Our energetic team is highly motivated and will use their expertise to handle each and every request with care and efficiency. Quote requests are replied to promptly and documents are sent out instantly (electronically) or if paper documents are required within a max of 10 days after full payment is received. Invoices, Vouchers, Itinerary, all other documents can also be accessed instantaneously online and printed as e-docs. With extremely competitive rates we are confident that we can offer the best price and travel solutions.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<section class="container tables" style="background: #fff;">
    <div class="spacer1"></div>
    <h2 class="text-center xxh-Bold">We cover the following travel destinations: </h2>
    <br>
    <div id="covers" class="row covers fadeOutTop">
    <div class="col-md-3 col-xs-6 hov1">

        <img src="<?php echo base_url();?>static/images/eastern_europe.jpg" class="img-circle wow pulse" style="border-radius:150px;height:200px;width:200px;margin:0px 32px 27px;">
        <h4 class="xxsmall-h text-center transition-h"><a style="cursor:default;">Eastern Europe</a></h4>
        <div class="full-text"> Croatia, Slovenia, Macedonia, Serbia, Montenegro, Albania, Czech Republic, Hungary, Romania, Bulgaria, Slovak Republic, Russia, the Ukraine, Lithuania, Latvia, Estonia. </div>
    </div>
    <div class="col-md-3 col-xs-6 hov2">

        <img src="<?php echo base_url();?>static/images/central_europe.jpg" class="img-circle wow pulse" style="border-radius:150px;height:200px;width:200px;margin:0px 32px 27px;">
        <h4 class="xxsmall-h text-center transition-h"><a style="cursor:default;">Central Europe</a></h4>
        <div class="full-text"> Austria, Germany, Switzerland. </div>
    </div>
    <div class="visible-xs">
         <div class="clearfix"></div>
    </div>
    <div class="col-md-3 col-xs-6 hov3">
        <img src="<?php echo base_url();?>static/images/western_eastern_europe.jpg" class="img-circle wow pulse" style="border-radius:150px;height:200px;width:200px;margin:0px 32px 27px;">
        <h4 class="xxsmall-h text-center transition-h"><a style="cursor:default;">Europe Western & Northern</a></h4>

        <div class="full-text"> France, Belgium, Netherlands, Luxembourg, Sweden, Norway, Finland, Great Britain, Ireland, Andorra. </div>
    </div>
    <div class="col-md-3 col-xs-6 hov4">
        <img src="<?php echo base_url();?>static/images/mediterranean_region_middle_east.jpg" class="img-circle wow pulse" style="border-radius:150px;height:200px;width:200px;margin:0px 32px 27px;">
        <h4 class="xxsmall-h text-center transition-h"><a style="cursor:default;">Mediterranean Region & Middle East</a></h4>
        <div class="full-text"> Greece, Turkey Malta, Italy, Spain, Portugal, Morocco, Cyprus, Egypt, Jordan, Israel, Dubai, Abu Dhabi, Oman. </div>
    </div>
    </div>
</section>
<br>
<section id="testimonials" class="container wow pulse divs">
        <a name="testimonials" class="smooth"></a>
        <div class="row">
        <h3 class="slide-title text-center"><a name="testimonials">What people say about us?</a></h3>
        <h5 class="text-center xmedium-h">Testimonals </h5>
                 <ul class="slider-main">
                     <li class="text-center">
                         <div > Thank you so much for the wonderful trip we just have come back from. The Glories of turkey was just so beautiful. Tibet our tour guide was just fantastic, spoke excellent English and her knowledge for each city we visited was unbelievable. She was always well organized and during our long journey on the road always kept us amused with all the funny stories she has had through all her tour guide trips, mostly about the lovely people and off course the most amazing country Turkey is. </div>
                         <br>
                         <span >Alice & Bob Kumar</span>
                     </li>
                     <li class="text-center">
                         <div> Thank you so much for the wonderful trip we just have come back from. The Glories of turkey was just so beautiful. Tibet our tour guide was just fantastic, spoke excellent English and her knowledge for each city we visited was unbelievable. She was always well organized and during our long journey on the road always kept us amused with all the funny stories she has had through all her tour guide trips, mostly about the lovely people and off course the most amazing country Turkey is. </div>
                         <br>
                         <span >Jan Chartres and Renee</span>
                     </li>
                     <li class="text-center">
                         <div >I apologise to not writing to you earlier but things have been busy since arriving back in the country. I would like to say thank you for helping get our trip to Belarussia and Russia organised and especially the follow up calls re: change in our hotel in Moscow. The hotel Russia was in "the" heart of Moscow and could not possibly have been any closer to the main attractions which was an awesome boost for us. Thanks again. </div>
                         <br>
                         <span >Paul and Catherine Cunningham</span>
                     </li>
                     <li class="text-center">
                         <div > Thank you so much for the wonderful trip we just have come back from. The Glories of turkey was just so beautiful. Tibet our tour guide was just fantastic, spoke excellent English and her knowledge for each city we visited was unbelievable. She was always well organized and during our long journey on the road always kept us amused with all the funny stories she has had through all her tour guide trips, mostly about the lovely people and off course the most amazing country Turkey is. </div>
                         <br>
                         <span>Alice & Bob Kumar</span>
                     </li>
                     <li class="text-center">
                         <div> Thank you so much for the wonderful trip we just have come back from. The Glories of turkey was just so beautiful. Tibet our tour guide was just fantastic, spoke excellent English and her knowledge for each city we visited was unbelievable. She was always well organized and during our long journey on the road always kept us amused with all the funny stories she has had through all her tour guide trips, mostly about the lovely people and off course the most amazing country Turkey is. </div>
                         <br>
                         <span >Jan Chartres and Renee</span>
                     </li>
                 </ul>

        </div>
</section>
<br>
<section class="container wow bounceIn" data-anchor="offers" style="background: #fff;">
    <h2 class="text-center">Why choose Travelrez?</h2>
    <h5 class="text-center xmedium-h">We offer</h5>
    <div id="trainings" class="row trainings fadeOutTop">
    <div class="col-md-6 col-xs-6 hov1">
    <div class="full-text">
        <ul class="fa-ul">
            <li><i class="fa-li fa fa-check-square-o"></i> Latest Online Booking site, TravelRez, launched this week </li>
            <li><i class="fa-li fa fa-check-square-o"></i>Personalised professional service and assistance in Australia and at destinations throughout Europe</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Expert destination and product knowledge from a professional reservation team</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Best telesales team & super fast telephones assistance</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Excellent email replies</li>
            <li><i class="fa-li fa fa-check-square-o"></i> No minimum nights stay</li>
            <li><i class="fa-li fa fa-check-square-o"></i> No cancellation fees (before documentation is issued), unless instant hotel puchase</li>
        </ul>
    </div>
    </div>
    <div class="col-md-6 col-xs-6 hov2">
    <div class="full-text">
        <ul class="fa-ul">
            <li><i class="fa-li fa fa-check-square-o"></i> No late booking fees</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Comprehensive brochure with large variety of product and specialist websites</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Free Letter of Invitation for Russia & Ukraine</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Great Groups Department – for your group needs ‐ School, Incentive, Corporate, Sporting, Historic & Pilgrimage to name a few</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Great FIT Product and ease of booking through online dedicated industry site</li>
            <li><i class="fa-li fa fa-check-square-o"></i> Dedicate sales support managers in most states</li>
        </ul>
    </div>
    </div>
    </div>
    <div class="offsetY-4"></div>
</section>
<br>
<br>
