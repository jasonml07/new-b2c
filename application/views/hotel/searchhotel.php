
    
            <div ng-app="Application" ng-controller="AppController" ng-init="data.search.rooms = '<?=( (isset($request)) && count($request['rooms']) > 0 )? count($request['rooms']): 1?>'; data.search.roomsData = <?php print_r(@$json);?>">
                <div class="floating-button loading-button"
                    ng-if         ="data.showRequestStatus"
                    ng-class      ="{'floating-warning': (!data.isAllItem), 'floating-success': data.isAllItem}"
                    style         ="right: 20px; bottom: 80px;"
                    ng-mouseenter ="data.notification.isHotel = true;"
                    ng-mouseleave ="data.notification.isHotel = false;">
                    <!-- ng-click      ="loadAllHotels()"> -->
                    <button type="button">
                        <span class="fa" ng-class="{'fa-refresh fa-spin': (!data.isAllItem), 'fa-check': data.isAllItem}"></span>
                    </button>
                </div>
                <div ng-if="data.showRequestStatus"
                    class="notif notif-hotel"
                    ng-class="{'active': data.notification.isHotel}">
                    <div class="notif-content">{{ data.notification.message }}</div>
                </div>

                <button type="button" id="reattempdata" ng-click="init()"
                    style="visibility: hidden; display: none;"></button>
                <section class="awe-parallax category-heading-section-hotel">
                    <div class="awe-overlay"></div>
                    <div class="container">
                        <div class="category-heading-content category-heading-content__2 text-uppercase">
                            <div class="find">
                                <h2 class="text-center">Find Your Hotel</h2>
                                <form action="<?=base_url();?>hotel/search" method="POST">
                                    <div class="form-group">
                                        <div class="form-elements">
                                            <label>Location</label>
                                            <div class="form-item scrollable-typehead">
                                                <i class="awe-icon awe-icon-marker-1"></i>
                                                <input type="text"
                                                    class="form-control"
                                                    ng-model="hotelCitySelected"
                                                    id="location[name]"
                                                    name="location[name]"
                                                    ng-value="'<?=($request)?$request['location']['name']:'Country, city, airport...'?>'"
                                                    ng-model-options="modelOptions"
                                                    placeholder="Search for city"
                                                    uib-typeahead="address as address.name for address in getHotelLocation($viewValue)"
                                                    typeahead-loading="loadingLocations"
                                                    typeahead-append-to-body="true"
                                                    typeahead-no-results="noResults"
                                                    autocomplete="off">
                                                <i ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></i>
                                                <div ng-show="noResults"1>
                                                    <i  class="glyphicon glyphicon-remove"></i> No Results Found
                                                </div>
                                                <input type="hidden" id="location[id]" name="location[id]" value="<?=($request)?$request['location']['id']:''?>">
                                                <input type="hidden" id="location[city]" name="location[city]" value="<?=($request)?$request['location']['city']:''?>">
                                                <input type="hidden" id="location[country_code]" name="location[country_code]" value="<?=($request)?$request['location']['country_code']:''?>">
                                            </div>
                                        </div>
                                        <div class="form-elements">
                                            <label>Check in</label>
                                            <div class="form-item">
                                                <i class="awe-icon awe-icon-calendar"></i>
                                                <input type="text"
                                                    autocomplete="off"
                                                    class="awe-calendar"
                                                    id="hotel-startdate"
                                                    name="start_date"
                                                    ng-model="data.hotel.startDate"
                                                    ng-change="hoteStartDate()"
                                                    autocomplete="off"
                                                    ng-value="'<?=($request)?$request['start_date']:'Date'?>'">
                                            </div>
                                        </div>
                                        <div class="form-elements  check-out">
                                            <label>Check out</label>
                                            <div class="form-item">
                                                <i class="awe-icon awe-icon-calendar"></i>
                                                <input type="text"
                                                    autocomplete="off"
                                                    class="awe-calendar"
                                                    id="hotel-enddate"
                                                    name="end_date"
                                                    ng-model="data.hotel.endDate"
                                                    autocomplete="off"
                                                    ng-value="'<?=($request)?$request['end_date']:'Date'?>'">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group search-bar-rooms"
                                        style="padding-top: 10px;"
                                        ng-repeat="r in []| range:data.search.rooms">
                                        <div class="form-elements" ng-class="{'hidden': r > 0}">
                                            <label>Rooms</label>
                                            <div class="form-item" ng-if="r == 0">
                                                <select class="awe-select" id="rooms" name="rooms" ng-model="data.search.rooms">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option> 
                                                    <option value="5">5</option> 
                                                </select>
                                            </div>
                                        </div>
                                        <!-- LOOPING -->
                                        <div class="form-elements">
                                            <label>Adult/s</label>
                                            <div class="form-item"
                                                ng-init="data.search.adult[$index] = (data.search.roomsData[$index].adult)?data.search.roomsData[$index].adult:'2'">
                                                <select class="awe-select"
                                                    id="rooms[{{ $index }}][adult]"
                                                    name="rooms[{{ $index }}][adult]"
                                                    ng-model="data.search.adult[$index]">
                                                    <option ng-if="rr != 0" ng-repeat="rr in []|range:6" value="{{ ::rr }}" >{{ ::rr }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-elements">
                                            <label>Children</label>
                                            <div class="form-item"
                                                ng-init="data.search.child[$index] = (data.search.roomsData[$index].child)?data.search.roomsData[$index].child:'0'">
                                                <select class="awe-select" id="rooms[{{ $index }}][child]"
                                                    name="rooms[{{ $index }}][child]"
                                                    ng-model="data.search.child[$index]">
                                                    <option ng-repeat="rr in []|range:5" value="{{ ::rr }}">{{ ::rr }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-elements age" ng-repeat="a in []|range:data.search.child[$index]">
                                            <label>Age</label>
                                            <div class="form-item">
                                                <input type="number" id="rooms[{{ $parent.$index }}][childAges][]" name="rooms[{{ $parent.$index }}][childAges][]" ng-value="(data.search.roomsData[$parent.$index].childAges[$index])?data.search.roomsData[$parent.$index].childAges[$index]:8">
                                            </div>
                                        </div>
                                        <div class="form-actions" ng-if="$last">
                                            <input type="hidden" name="fromTheMainPage" value="0">
                                            <input type="submit" value="Find My Hotel">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END / HEADING PAGE -->
                <section class="filter-page">
                    <div class="container">
                        <div class="row">
                            <!-- FILTER -->
                            <div class="col-md-3">
                                <div class="page-sidebar">
                                    <div class="sidebar-title">
                                        <h2>Hotel filter</h2>
                                        <div class="clear-filter">
                                            <a href="" ng-click="clearFilter()">Clear all</a>
                                        </div>
                                    </div>
                                    <!-- WIDGET -->
                                    <div class="widget widget_has_radio_checkbox">
                                        <h3>Hotel Type</h3>
                                        <ul>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.hoteltype[1].value" ng-true-value="1" ng-model-options="{debounce: 500}" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    Hotel
                                                    <span class="badge pull-right">{{ data.counts.hotels[1] }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.hoteltype[0].value" ng-true-value="2" ng-model-options="{debounce: 500}" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    Apartment
                                                    <span class="badge pull-right">{{ data.counts.hotels[0] }}</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- END / WIDGET -->
                
                                    <!-- WIDGET -->
                                    <div class="widget widget_price_filter">
                                        <h3>Price Level</h3>
                                        <div class="price-slider-wrapper">
                                            <div class="price-slider" id="price-slider"></div>
                                            <div class="price_slider_amount">
                                                <div class="price_label">
                                                    <span class="from"></span> - <span class="to"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END / WIDGET -->
                
                                    <!-- WIDGET -->
                                    <div class="widget widget_has_radio_checkbox">
                                        <h3>Star Rating</h3>
                                        <ul>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.rating[5].value" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    <span class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                                    <span class="badge pull-right">{{ data.counts.ratings[5] }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.rating[4].value" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    <span class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                                    <span class="badge pull-right">{{ data.counts.ratings[4] }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.rating[3].value" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    <span class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                                    <span class="badge pull-right">{{ data.counts.ratings[3] }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.rating[2].value" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    <span class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                                    <span class="badge pull-right">{{ data.counts.ratings[2] }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.rating[1].value" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    <span class="rating">
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                                    <span class="badge pull-right">{{ data.counts.ratings[1] }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.rating[0].value" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    <span class="rating">
                                                        Unrated
                                                        <span class="badge pull-right">{{ data.counts.ratings[0] }}</span>
                                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- END / WIDGET -->
                
                                    <!-- WIDGET -->
                                    <div class="widget widget_has_radio_checkbox">
                                        <h3>Board Type</h3>
                                        <ul>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.boardtypes[4].value" ng-true-value="'RO'" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    Room Only
                                                    <span class="badge pull-right">{{ data.counts.boardtypes.RO }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.boardtypes[3].value" ng-true-value="'BB'" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    Bed and Breakfast
                                                    <span class="badge pull-right">{{ data.counts.boardtypes.BB }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.boardtypes[2].value" ng-true-value="'HB'" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    Half Board
                                                    <span class="badge pull-right">{{ data.counts.boardtypes.HB }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.boardtypes[1].value" ng-true-value="'FB'" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    Full Board
                                                    <span class="badge pull-right">{{ data.counts.boardtypes.FB }}</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" ng-model="data.filters.boardtypes[0].value" ng-true-value="'AI'" ng-model-options="data.options" ng-change="updateHotels();">
                                                    <i class="awe-icon awe-icon-check"></i>
                                                    All Inclusive
                                                    <span class="badge pull-right">{{ data.counts.boardtypes.AI }}</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- END / WIDGET -->
                                    <div class="widget widget_has_radio_checkbox">
                                        <h3>Point of Interest</h3>
                                        <ul>
                                            <li><input type="text" class="form-control" ng-model="data.searchLandMark" ng-model-options="data.options" placeholder="Search for the landmarks . . ."></li>
                                            <div id="point-of-interest-landmark-wrapper">
                                                <li ng-repeat="region in data.counts.landmarks | filter:{name: data.searchLandMark}" >
                                                    <label ng-click="assignLandmarkFilter(region, region.type);">
                                                        <!-- <input type="checkbox" ng-model="data.filters.boardtypes[3].value" ng-true-value="'BB'" ng-model-options="data.options" ng-change="updateHotels();"> -->
                                                        <div class="col-sm-10" style="padding: 5px 0px;">
                                                            <i class="fa " ng-class="{'fa-map-marker': (! region.isSelected), 'fa-check': region.isSelected}"></i>
                                                            {{ region.name }}
                                                        </div>
                                                        <div class="col-sm-2" style="padding: 5px 0px;">
                                                            <span class="badge pull-right">{{ region.count }}</span>
                                                        </div>
                                                    </label>
                                                    <!-- <div class="dropdown">
                                                        <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                            type         ="button"
                                                            id           ="meseumsDropdown"
                                                            data-toggle  ="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="true">{{key | ucfirst}} <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu scrollable-menu" aria-labelledby="meseumsDropdown" role="menu">
                                                            <li style="padding-top: 0px;">
                                                                <input type="text" ng-model="menu.searchText" class="form-control" placeholder="Search name on {{key | ucfirst}}" style="height: 30px; border-radius: 0px;">
                                                            </li>
                                                            <li ng-repeat="menu in landmark.regions | filter:{name: menu.searchText} track by menu.id">
                                                                <a href="" ng-click="assignLandmarkFilter(menu, key); updateHotels();">
                                                                    <div class="row">
                                                                        <div class="col-md-12" style="padding: 0px;">
                                                                            <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                            <span class="badge pull-right">{{ menu.count }}</span>
                                                                            {{ menu.name }}
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div> -->
                                                </li>
                                            </div><!-- 
                                            <li>
                                                <div class="dropdown">
                                                    <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                        type         ="button"
                                                        id           ="monumentDropdown"
                                                        data-toggle  ="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">Monument <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu scrollable-menu" aria-labelledby="monumentDropdown">
                                                        <li ng-repeat="menu in data.counts.landmarks.monument.regions track by menu.id">
                                                            <a href="" ng-click="assignLandmarkFilter(menu, 'monument'); updateHotels();">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="padding: 0px;">
                                                                        <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                        <span class="badge pull-right">{{ menu.count }}</span>
                                                                        {{ menu.name }}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown">
                                                    <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                        type         ="button"
                                                        id           ="monumentDropdown"
                                                        data-toggle  ="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">Stadium <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu scrollable-menu" aria-labelledby="monumentDropdown">
                                                        <li ng-repeat="menu in data.counts.landmarks.stadium.regions track by menu.id">
                                                            <a href="" ng-click="assignLandmarkFilter(menu, 'stadium'); updateHotels();">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="padding: 0px;">
                                                                        <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                        <span class="badge pull-right">{{ menu.count }}</span>
                                                                        {{ menu.name }}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown">
                                                    <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                        type         ="button"
                                                        id           ="monumentDropdown"
                                                        data-toggle  ="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">Theater <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu scrollable-menu" aria-labelledby="monumentDropdown">
                                                        <li ng-repeat="menu in data.counts.landmarks.theater.regions track by menu.id">
                                                            <a href="" ng-click="assignLandmarkFilter(menu, 'theater'); updateHotels();">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="padding: 0px;">
                                                                        <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                        <span class="badge pull-right">{{ menu.count }}</span>
                                                                        {{ menu.name }}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown">
                                                    <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                        type         ="button"
                                                        id           ="monumentDropdown"
                                                        data-toggle  ="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">Historic <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu scrollable-menu" aria-labelledby="monumentDropdown">
                                                        <li ng-repeat="menu in data.counts.landmarks.historic.regions track by menu.id">
                                                            <a href="" ng-click="assignLandmarkFilter(menu, 'historic'); updateHotels();">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="padding: 0px;">
                                                                        <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                        <span class="badge pull-right">{{ menu.count }}</span>
                                                                        {{ menu.name }}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown">
                                                    <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                        type         ="button"
                                                        id           ="monumentDropdown"
                                                        data-toggle  ="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">Shopping <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu scrollable-menu" aria-labelledby="monumentDropdown">
                                                        <li ng-repeat="menu in data.counts.landmarks.shopping.regions track by menu.id">
                                                            <a href="" ng-click="assignLandmarkFilter(menu, 'shopping'); updateHotels();">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="padding: 0px;">
                                                                        <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                        <span class="badge pull-right">{{ menu.count }}</span>
                                                                        {{ menu.name }}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown">
                                                    <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                        type         ="button"
                                                        id           ="monumentDropdown"
                                                        data-toggle  ="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">Casino <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu scrollable-menu" aria-labelledby="monumentDropdown">
                                                        <li ng-repeat="menu in data.counts.landmarks.casino.regions track by menu.id">
                                                            <a href="" ng-click="assignLandmarkFilter(menu, 'casino'); updateHotels();">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="padding: 0px;">
                                                                        <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                        <span class="badge pull-right">{{ menu.count }}</span>
                                                                        {{ menu.name }}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="dropdown">
                                                    <button class    ="btn btn-default btn-sm dropdown-toggle btn-block"
                                                        type         ="button"
                                                        id           ="monumentDropdown"
                                                        data-toggle  ="dropdown"
                                                        aria-haspopup="true"
                                                        aria-expanded="true">Medical <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu scrollable-menu" aria-labelledby="monumentDropdown">
                                                        <li ng-repeat="menu in data.counts.landmarks.medical.regions track by menu.id">
                                                            <a href="" ng-click="assignLandmarkFilter(menu, 'medical'); updateHotels();">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="padding: 0px;">
                                                                        <span ng-if="menu.isSelected" class="fa fa-check pull-left"></span>
                                                                        <span class="badge pull-right">{{ menu.count }}</span>
                                                                        {{ menu.name }}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </div>
                                    <!-- END OF LANDMARK FILTER -->
                                </div>
                                <div id="slide-menu-button" class="text-center floating-button">
                                    <button data-status="up"><span class="fa fa-chevron-down"></span></button>
                                </div>
                            </div>

                            <div class="loading-animation col-md-9 text-center" ng-if="data.status == 'working' || data.status == 'failed'">
                                    <section class="main">
                                    <div class="bar">
                                        <i class="sphere"></i>
                                    </div>
                                </section>
                            </div>
                            <!-- LIST OF HOTEL -->
                            <div class="col-md-9" id="result-viewer">
                                <div class="page-top" ng-if="data.hotels.length > 0">
                                    <div class="col-xs-9 col-sm-9 col-md-8">
                                        <h5><strong>{{ data.hotels.length }}</strong> Hotels Found</h5>
                                    </div>
                                    <!-- <div class="col-xs-3 col-sm-3 col-md-4" ng-if="data.showRequestStatus">
                                        <button class="btn btn-warning btn-block" ng-if="!data.isAllItem && data.status == 'done' && data.hotels.length > 0">
                                            <span class="fa fa-refresh fa-spin"></span>&nbsp;<span class="hidden-xs hidden-sm">Finding more hotels</span>
                                        </button>
                                        <button class="btn btn-success btn-block" ng-click="loadAllHotels();" ng-if="data.isAllItem && data.status == 'done' && data.hotels.length > 0">
                                            <span id="load-hotels" class="fa fa-check"></span>&nbsp;<span class="hidden-xs hidden-sm">Click here to see more hotels</span>
                                        </button>
                                    </div> -->
                                    <div class="col-xs-12 col-sm-8 col-md-8" ng-if="data.hotels.length > 0">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" ng-click="data.viewByMap = (data.viewByMap)?false:true; updateHotels();">
                                                    <span class="fa" ng-class="{'fa-th-list': data.viewByMap, 'fa-map-marker': (! data.viewByMap)}"></span>&nbsp;<span class="hidden-sm">{{ (! data.viewByMap)?'Map':'List' }}</span>
                                                </button>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Search hotel name here . . ." ng-model="data.searchHotelName.hotelDetails.hotelname" ng-model-options="{debounce: 500}" ng-change="updateHotels();" style="height: 34px;" >
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" ng-click="data.searchHotelName.hotelDetails.hotelname = ''"><span class="fa fa-close"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 text-right">
                                        <select class="awe-select" ng-model="data.filters.price">
                                            <option value="price">From Lowest price</option>
                                            <option value="-price">From Highest price</option>
                                        </select>
                                    </div>
                                </div>
                                <button id="filter-hotels" style="visibility:hidden; display: none" ng-click="data.filtered = ((data.hotels |filter:data.searchHotelName.hotelDetails.hotelname |filter:priceRange() |filter:ratingRange(data.filters.rating) |filter:filterByLandmarks(data.filters.activeLandmarks.region, data.filters.activeLandmarks.selectedRegions) |filter:boardTypes(data.filters.boardtypes) |filter:hotelType(data.filters.hoteltype)) |orderBySpecialDeals |orderByRating); getMap( data.filtered );"></button>
                                <!-- <button id="map-search-filteration" style="visibility:hidden; display: none" ng-click="getMap( (data.hotels |filter:data.searchHotelName.hotelDetails.hotelname |filter:priceRange |filter:ratingRange(data.filters.rating) |filter:boardTypes(data.filters.boardtypes) |filter:hotelType(data.filters.hoteltype)) );"></button> -->


                                <div ng-if="data.viewByMap && data.hotels.length" class="col-md-12">
                                    <ng-map default-style="false" id="search-map" zoom="8" center="<?=@$request['location']['city']?>" style="height: 500px; display:block;">
                                        <!-- <marker position="-33.890542, 151.274856" icon="https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png"></marker> -->
                                        <!-- <marker id="{{hotel.id}}" ng-repeat="hotel in data.hotels |filter:data.searchHotelName.hotelDetails.hotelname |filter:priceRange |filter:ratingRange(data.filters.rating) |filter:boardTypes(data.filters.boardtypes) |filter:hotelType(data.filters.hoteltype) |orderBy:data.filters.price track by hotel.id"
                                            position="{{ hotel.details.latitude }}, {{ hotel.details.longitude }}"
                                            title="{{ hotel.details.hotelname }}"
                                            icon="{{ (hotel.details.propertytype == 1)?data.image.HOTEL:data.image.APARTLE }}"
                                            on-click="showDetail(hotel)"></marker> -->

                                        <info-window id="hotel-info" data-value="{{ data.currentHotel.id }}" max-width="700">
                                            <div ng-non-bindable="">
                                                <div class="hotel-item map-search" style="margin-bottom: 0px; min-height: 166px; min-width: 600px;">
                                                    <div class="item-media">
                                                        <div class="image-cover">
                                                            <img ng-if="data.currentHotel.details.image" ng-src="http://c775792.r92.cf2.rackcdn.com/sml/{{data.currentHotel.details.image.url}}" alt="{{data.currentHotel.details.image.title}}">
                                                            <img ng-if="! data.currentHotel.details.image" width="600" ng-src="https://maps.googleapis.com/maps/api/staticmap?center=<?=@$request['location']['city']?>&zoom=10&scale=false&size=600x600&maptype=roadmap&format=png&visual_refresh=true" alt="Google Map of <?=@$request['location']['city']?>">
                                                        </div>
                                                    </div>
                                                    <div class="item-body" style="padding-top: 0px; padding-bottom: 0px">
                                                        <div class="item-title">
                                                            <h2>
                                                                <a href="" ng-bind="data.currentHotel.details.hotelname"></a>
                                                            </h2>
                                                        </div>
                                                        <div class="item-hotel-star">
                                                            <i class="fa fa-star" ng-repeat="star in [] | range: (data.currentHotel.details.rating|convertToNumber)"></i>
                                                        </div>
                                                        <a target="_blank" ng-href="{{ ((data.currentHotel.details.reviews)?data.currentHotel.details.reviews.url:'') }}">
                                                            <small><span style="color: #000; font-size: .9em;"><strong>TripAdvisor Traveller Rating:</strong></span></small>
                                                            <br>
                                                            <img style="width: 30%;" ng-src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/{{(((data.currentHotel.details.reviews)?data.currentHotel.details.reviews.average:0)|number:1)}}-11367-5.gif"/>
                                                            <br>
                                                            <small><span style="font-size: .9em;"><strong ng-bind="'Based on' + ((data.currentHotel.details.reviews)?data.currentHotel.details.reviews.count:0) + ' traveller reviews'"></strong></span></small>
                                                        </a>
                                                        <div class="item-address">
                                                            <i class="awe-icon awe-icon-marker-2"></i>
                                                            <span ng-bind="data.currentHotel.details.address"></span>, <?=($request)?$request['location']['name']:'Country, city, airport...'?>
                                                        </div>
                                                        <div>
                                                            <span ng-bind="data.currentHotel.request.availableBoards | filterBoard"></span>
                                                            <br>
                                                            <span ng-if="data.currentHotel.request.content['special-deals']" style="background-color: #a94442;" class="badge" tooltip-class="my-tooltip" tooltip-placement="top" uib-tooltip-template="'myTooltipTemplateSpecialDealsInfoWindow.html'" tooltip-append-to-body="true"><span class="fa fa-check"></span>&nbsp;Special Deals</span>
                                                        </div>
                                                    </div>
                                                    <!-- PRICE -->
                                                    <div class="adjust-item-price-more item-price-more" style="margin-top: 0px; margin-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                        <div style="padding-left: 5px; padding-right: 5px;" class="price search-price text-center">
                                                            <span style="color: #AA3939; font-size: 1.5em; font-weight: 900; text-decoration: line-through;" ng-if="data.currentHotel.calculation.calculation.gross_amount > data.currentHotel.calculation.calculation.due_amount" class="amount">
                                                                <my-currency
                                                                    currency="data.currentHotel.currency"
                                                                    amount="data.currentHotel.calculation.calculation.gross_amount"></my-currency>
                                                            </span>
                                                            <span class="amount">
                                                                <my-currency
                                                                    currency="data.currentHotel.currency"
                                                                    amount="data.currentHotel.calculation.calculation.due_amount"></my-currency>
                                                            </span>
                                                            <span ng-bind="data.currentHotel.numberOfRoom + ' room(s)'"></span>
                                                        </div>
                                                        <div class="text-center">
                                                            <a ng-href="<?=base_url();?>hotel/details?id={{ data.currentHotel.id }}" class="btn btn-primary">SELECT</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </info-window>
                                        <script type="text/ng-template" id="myTooltipTemplateSpecialDealsInfoWindow.html">
                                             <div class="row">
                                                 <div class="col-md-12" ng-repeat="deal in data.currentHotel.request.content['special-deals'][0]['special-deal'] track by $index">
                                                    {{ deal }}
                                                 </div>
                                             </div>
                                        </script>
                                        <script type="text/ng-template" id="myTooltipTemplateHotelInfoWindow.html">
                                             <div class="row">
                                                 <div class="col-md-7 text-left"><strong>Gross Amount</strong>: </div>
                                                 <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{data.currentHotel.calculation.calculation.gross_amount|number:2}}</div>
                                                 <div class="col-md-7 text-left"><strong>Commission</strong>:</div>
                                                 <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{data.currentHotel.calculation.calculation.commission|number:2}}</div>
                                                 <div class="col-md-7 text-left"><strong>Nett Amount</strong>:</div>
                                                 <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{data.currentHotel.calculation.calculation.due_amount|number:2}}</div>
                                             </div>
                                        </script>
                                    </ng-map>
                                </div>

                                <div ng-if="!data.viewByMap">
                                    <div style="top: 20px;" class="alert alert-danger" ng-if="data.status == 'error'">{{ data.error.message }}</div>
                                    
                                    <h2 ng-if="data.hotels.length == 0 && data.status == 'done'">There are no result!!!</h2>
                                    <div style="margin-bottom: 0px;" id="table-wrapper" class="filter-page__content" ng-if="data.status == 'done' || data.status == 'load-more-hotels'">
                                        <div class="filter-item-wrapper">
                                            <!-- ITEM -->
                                            <!-- <div class="hotel-item" dir-paginate="hotel in hotels = (data.filtered |orderBy:data.filters.price | itemsPerPage:6 | record:this) track by $index" current-page="data.currentPage"> -->
                                            <div class="hotel-item" ng-repeat="hotel in hotels = (data.filtered |limitTo: paginationLimit()) track by $index">
                                                <div class="item-media" ng-init="hotel.showRooms = false">
                                                    <div class="image-cover">
                                                        <img ng-if="hotel.details.image" ng-src="http://c775792.r92.cf2.rackcdn.com/sml/{{hotel.details.image.url}}" alt="{{hotel.details.image.title}}">
                                                        <img ng-if="! hotel.details.image" width="600" ng-src="https://maps.googleapis.com/maps/api/staticmap?center=<?=@$request['location']['city']?>&zoom=10&scale=false&size=600x600&maptype=roadmap&format=png&visual_refresh=true" alt="Google Map of <?=@$request['location']['city']?>">
                                                    </div>
                                                </div>
                                                <div class="item-body">
                                                    <div class="item-title">
                                                        <h2>
                                                            <a href="" ng-bind="hotel.details.hotelname"></a>
                                                        </h2>
                                                    </div>
                                                    <div class="item-hotel-star">
                                                        <i class="fa fa-star" ng-repeat="star in []|range: (hotel.details.rating|convertToNumber)"></i>
                                                    </div>
                                                    <a target="_blank" ng-href="{{ ((hotel.details.reviews)?hotel.details.reviews.url:'') }}">
                                                        <small><span style="color: #000; font-size: .9em;"><strong>TripAdvisor Traveller Rating:</strong></span></small>
                                                        <br>
                                                        <img style="width: 30%;" ng-src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/{{(((hotel.details.reviews)?hotel.details.reviews.average:0)|number:1)}}-11367-5.gif"/>
                                                        <br>
                                                        <small><span style="font-size: .9em;"><strong ng-bind="'Based on' + ((hotel.details.reviews)?hotel.details.reviews.count:0) + ' traveller reviews'"></strong></span></small>
                                                    </a>
                                                    <div class="item-address">
                                                        <i class="awe-icon awe-icon-marker-2"></i>
                                                        <span ng-bind="hotel.details.address"></span>, <?=($request)?$request['location']['name']:'Country, city, airport...'?>
                                                    </div>
                                                    <div>
                                                        <span ng-bind="hotel.request.availableBoards | filterBoard"></span>
                                                        <br>
                                                        <span ng-if="hotel.request.content['special-deals']" style="background-color: #a94442;" class="badge" tooltip-class="my-tooltip" tooltip-placement="top" uib-tooltip-template="'myTooltipTemplateSpecialDeals.html'" tooltip-append-to-body="true"><span class="fa fa-check"></span>&nbsp;Special Deals</span>
                                                    </div>
                                                </div>
                                                <div id="hotel-price-{{ hotel.id }}" class="adjust-item-price-more item-price-more">
                                                    <!-- <div ng-if="hotel.fetchingRooms || hotel.rooms.rooms.length > 0"> -->
                                                    <div>
                                                        <div style="padding-left: 5px; padding-right: 5px;" class="price search-price text-center">
                                                            <span style="color: #AA3939; font-size: 1.5em; font-weight: 900; text-decoration: line-through;" ng-if="hotel.calculation.calculation.gross_amount > hotel.calculation.calculation.due_amount" class="amount">
                                                                <my-currency
                                                                    currency="hotel.currency"
                                                                    amount="hotel.calculation.calculation.gross_amount"></my-currency>
                                                            </span>
                                                            <span class="amount">
                                                                <my-currency
                                                                    currency="hotel.currency"
                                                                    amount="hotel.calculation.calculation.due_amount"></my-currency>
                                                            </span>
                                                            <span ng-bind="hotel.numberOfRoom + ' room(s)'"></span>
                                                        </div>
                                                        <div class="text-center">
                                                            <a ng-href="<?=base_url();?>hotel/details?id={{ hotel.id }}" style="margin-top: 10px;" class="awe-btn">View Details</a>
                                                            <!-- ng-click="hotel.showRooms = hotel.showRooms?false:true" -->
                                                            <button type="button" 
                                                                class="btn btn-sm btn-danger for-rooms"
                                                                style="margin-top:10px" 
                                                                ng-click="getHotelRooms( hotel )"
                                                                ng-disabled="hotel.isDisableRoomButton"
                                                                id="{{'hotel-button-' + hotel.id}}">
                                                                <i ng-class="{'glyphicon glyphicon-plus': ! hotel.showRooms, 'glyphicon glyphicon-minus': hotel.showRooms}"></i>
                                                                &nbsp;Rooms
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="{{hotel.id}}" class="col-md-12" ng-if="hotel.showError">
                                                    <div class="alert alert-danger" ng-bind="((hotel.errorMessage)?hotel.errorMessage:'An error occured while fetching rooms.')"></div>
                                                </div>
                                                <div class="col-md-12"
                                                    ng-if="hotel.showRooms && ! hotel.showError && hotel.fetchingRooms"
                                                    id="{{ hotel.id }}"
                                                    style="padding-bottom: 10px;margin-top: 15px;">
                                                    <div>
                                                        <ul class="adjust-table-provider nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="active">
                                                                <a href="#rooms-{{hotel.id}}" role="tab" data-toggle="tab"><span class="text">ROOMS</span> <span class="label label-success" ng-bind="hotel.rooms.rooms[0].room.length"></span></a>
                                                            </li>
                                                            <li role="presentation" ng-if="hotel.rooms.combinations[0].combination.length > 0">
                                                                <a href="#conbinations-{{hotel.id}}" role="tab" data-toggle="tab"><span class="text">COMBINATIONS</span> <span class="label label-success" ng-bind="hotel.rooms.combinations[0].combination.length"></span></a>
                                                            </li>
                                                        </ul>
                                                        <!-- ROOMS -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" id="rooms-{{hotel.id}}"
                                                                ng-init="hotel.roomOptions = {hasCancellation: true, sortPrice: false, sortPax: false, totalPrice: 0, totalRooms: 0, adults: 0, children: 0, selectedRooms: []};">
                                                                <div style="overflow-y: scroll;">
                                                                    <table style="margin-bottom:0px;" class="table table-striped table-bordered">
                                                                        <thead>
                                                                            <tr style="color: #666;">
                                                                                <th class="text-center" 
                                                                                    style="max-width: 70px; min-width: 70px; width: 70px;">QTY #</th>
                                                                                <th style="max-width: 160px; min-width: 160px; width: 160px;">Board Type</th>
                                                                                <th>Room Type</th>
                                                                                <th style="max-width: 60px; min-width: 60px; width: 60px;"
                                                                                    class="text-center">Pax<br> <span class="fa fa-user"></span> - <span class="fa fa-child"></span></th>
                                                                                <th style="max-width: 115px; min-width: 115px; width: 115px;"
                                                                                    ng-if="hotel.roomOptions.hasCancellation">Policy</th>
                                                                                <th style="max-width: 120px; width: 120px; min-width: 120px; cursor: pointer;"
                                                                                    ng-click="hotel.roomOptions.sortPrice = (hotel.roomOptions.sortPrice)?false:true">Price <span class="fa pull-right" ng-class="{'fa-sort-asc': hotel.roomOptions.sortPrice, 'fa-sort-desc': !hotel.roomOptions.sortPrice}"></span></th>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </div>
                                                                <div style="height: 350px; overflow-y: scroll; border-bottom: 1px solid #DDD;">
                                                                    <table class="table table-striped table-bordered"
                                                                        style="margin-bottom: 0px;" id="table-hotel-{{ hotel.id }}">
                                                                        <tbody>
                                                                            <!-- For Safety check the calculation if exist... -->
                                                                            <tr style="color: #666;" ng-repeat="(roomKey, room) in hotel.rooms.rooms[0].room | orderBy:comaprator.price:hotel.roomOptions.sortPrice track by $index"
                                                                                ng-class="{'success': room.selectedQuantity > 0, '': room.selectedQuantity == 0}"
                                                                                ng-init="room.selectedQuantity = '0'; room.previous = {};">
                                                                    
                                                                                <td style="max-width: 70px; min-width: 70px; width: 70px;">
                                                                                    <select id="room-{{ hotel.id }}-{{ room.resultid.replace('.', '') }}" ng-disabled="hotel.roomOptions.readyToSubmit && room.selectedQuantity == '0'" data-status="{{ (room.selectedQuantity > 0)?'selected':'' }}" data-key="{{ roomKey }}" ng-model="room.selectedQuantity" ng-change="calculateHotelSelections(room, hotel)">
                                                                                        <option value="0">0</option>
                                                                                        <option ng-if="room.minquantity != room.maxquantity" value="{{ i }}" ng-repeat="i in [] | range: ((room.maxquantity|convertToNumber) - (room.minquantity|convertToNumber)):((room.minquantity|convertToNumber)+1)" ng-bind="::i" ></option>
                                                                                        <option ng-if="room.minquantity == room.maxquantity" value="{{ room.maxquantity }}" ng-bind="::room.maxquantity"></option>
                                                                                    </select>
                                                                                </td>
                                                                                <td style="max-width: 160px; min-width: 160px; width: 160px;" ng-bind="room.content.board[0].content"></td>
                                                                                <td>
                                                                                    <span ng-bind="room.content.name[0]"></span>
                                                                                    <span class="badge pull-right" ng-bind="room.maxquantity"></span>
                                                                                    <br> 
                                                                                    <span ng-if="room.content['special-deals']" style="background-color: #a94442;" class="badge" tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'myTooltipTemplateSpecialDealsForRooms.html'"><span class="fa fa-check"></span>&nbsp;Special Deals</span>
                                                                                </td>
                                                                                <td class="text-center"
                                                                                    style="max-width: 60px; min-width: 60px; width: 60px;">
                                                                                    <strong ng-bind="((room.content.pax[0].adults)?room.content.pax[0].adults[0]:0) + ' - ' + ((room.content.pax[0].children)?room.content.pax[0].children[0]:0)"></strong>
                                                                                </td>
                                                                                <td style="max-width: 115px; min-width: 115px; width: 115px;" 
                                                                                    ng-if="hotel.roomOptions.hasCancellation">
                                                                                    <span ng-if="!room.content.cancellation && hotel.roomOptions.hasCancellation">
                                                                                        {{ hotel.roomOptions.hasCancellation = false; "" }}
                                                                                    </span>
                                                                                    <div ng-if="room.content.cancellation" ng-repeat="policy in ::room.content.cancellation[0].frame" style="margin-bottom: 3px;">
                                                                                        <span tooltip-placement="top" uib-tooltip="From {{ policy.startTime }} {{ (policy.endTimeFormatted != null)?'up to ' + policy.endTimeFormatted:'' }} this item is {{ policy.type }}" class="label label-primary" ng-bind="::policy.type"></span>
                                                                                    </div>
                                                                                </td>
                                                                                <td style="max-width: 120px; width: 120px; min-width: 120px;">
                                                                                    <!-- <div tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'myTooltipTemplateRoom.html'"> -->
                                    
                                                                                    <div style="color: #AA3939; font-size: .9em; font-weight: 900; text-decoration: line-through;" ng-if="room.content.price[0].calculation.calculation.gross_amount > room.content.price[0].calculation.calculation.due_amount">
                                                                                        <span class="fa fa-usd"></span> {{ room.content.price[0].calculation.calculation.gross_amount|number:2 }}
                                                                                    </div>
                                                                                    <div tooltip-placement="top">
                                                                                        <my-currency
                                                                                            currency="room.content.price[0].new_currency"
                                                                                            amount="(room.previous.totalPrice)?room.previous.totalPrice:room.content.price[0].calculation.calculation.due_amount"
                                                                                            async="true">
                                                                                        </my-currency>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <br>
                                                                <div>
                                                                    <form action="{{ '<?=base_url()?>hotel/booker-form?id=' + hotel.id }}" method="POST">
                                                                        <input type="hidden" name="token" value="<?=$token?>" >
                                                                        <div ng-repeat="selected in hotel.roomOptions.selectedRooms track by $index">
                                                                            <input type="hidden" id="selectedRooms[{{ $index }}][id]" name="selectedRooms[{{ $index }}][id]" value="{{ selected.resultid }}" >
                                                                            <input type="hidden" id="selectedRooms[{{ $index }}][qty]" name="selectedRooms[{{ $index }}][qty]" value="{{ selected.selectedQuantity }}" >
                                                                        </div>
                                                
                                                                        <div class="col-sm-12 col-md-4" id="booking-info-indicator" style="color: #666;">
                                                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                                                <i class="fa fa-user" style="font-size: 1.5em;"></i><br> {{ hotel.roomOptions.adults }}/{{ ::data.searchValues.adults }} 
                                                                            </div>
                                                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                                                <i class="fa fa-child" style="font-size: 1.5em;"></i><br> {{ hotel.roomOptions.children }}/{{ ::data.searchValues.childrens }} 
                                                                            </div>
                                                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                                                <i class="fa fa-tags" style="font-size: 1.5em;"></i><br> {{ hotel.roomOptions.totalRooms }}/{{ ::data.searchValues.rooms }} 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 text-center">
                                                                            <h5>TOTAL: <span class="fa fa-usd"></span> {{ hotel.roomOptions.totalPrice |number:2 }}</h5>
                                                                        </div>
                                                                        <div class="col-md-4 text-right">
                                                                            <button type="button" class="btn btn-primary btn-success" ng-disabled="hotel.roomOptions.selectedRooms.length == 0" ng-click="clearSelections( (hotel.rooms.rooms[0].room|filter: {isAdded: true}), hotel.id )">Clear</button>
                                                                            <button type="submit" class="btn btn-primary btn-danger" ng-disabled="!hotel.roomOptions.readyToSubmit">BOOK ROOMS</button>
                                                                            <!-- <button type="button" class="btn btn-primary btn-success" ng-disabled="hotel.roomOptions.selectedRooms.length == 0" ng-click="clearSelections( hotel.rooms.rooms[0].room, hotel.id )">Clear</button> -->
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!-- END OF ROOMS -->
                                                            <!-- COMBINATIONS -->
                                                            <div role="tabpanel" class="tab-pane" id="conbinations-{{hotel.id}}">
                                                                <div class="row center-block">
                                                                    <div class="panel panel-primary col-md-3"
                                                                        style="padding:0px; margin: 10px;"
                                                                        ng-repeat="combination in hotel.rooms.combinations[0].combination|orderBy:'calculation.calculation.due_amount'">
                                                                        <div class="panel-heading" ng-bind="combination.board | filterBoard"></div>
                                                                        <div class="panel-body">
                                                                            <combination-description pax="data.searchValues"></combination-description>
                                                                            <div class="text-center">
                                                                                <!-- <h6 tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'myTooltipTemplateCombination.html'"> -->
                                                                                <h6>
                                                                                    <div style="color: #AA3939; font-size: .9em; font-weight: 900; text-decoration: line-through;" ng-if="combination.calculation.calculation.gross_amount > combination.calculation.calculation.due_amount">
                                                                                        <span class="fa fa-usd"></span> {{ combination.calculation.calculation.gross_amount|number:2 }}
                                                                                    </div>
                                                                                    <span>
                                                                                        <my-currency
                                                                                            currency="combination.new_currency"
                                                                                            amount="combination.calculation.calculation.due_amount"
                                                                                            async="true">
                                                                                        </my-currency>
                                                                                    </span>
                                                                                </h6>
                                                                            </div>
                                                                            <form action="{{ '<?=base_url()?>hotel/booker-form?id=' + hotel.id }}" method="POST">
                                                                                <input type="hidden" name="token" value="<?=$token?>">
                                                                                <div ng-repeat="combRoom in ::combination.content.rooms[0].room">
                                                                                    <input type="hidden" id="selectedRooms[{{ $index }}][id]" name="selectedRooms[{{ $index }}][id]" value="{{ ::combRoom.resultid }}">
                                                                                    <input type="hidden" id="selectedRooms[{{ $index }}][qty]" name="selectedRooms[{{ $index }}][qty]" value="{{ ::combRoom.count }}">
                                                                                </div>
                                                                                    <button type="submit" class="btn btn-danger btn-block">BOOK NOW!</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END OF COMBINATIONS -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROOMS AND COMBINATIONS ITEM -->
                                        </div>
                                    </div>
                                    <div infinite-scroll="showMoreItems()" infinite-scroll-distance="1"></div>
                                    <!-- PAGINATION -->
                                    <!-- <button type="button" class="btn btn-block btn-primary" ng-click="showMoreItems()">Show more</button> -->
                                    <!-- <div class="text-center">
                                        <dir-pagination-controls
                                            boundary-links="true"
                                            template-url="<?=base_url()?>static/js/dirPagination.tpl.html"
                                            auto-hide="true">
                                            on-page-change="onPageChange()"
                                        </dir-pagination-controls>
                                    </div> -->
                                    <!-- END / PAGINATION -->
                                    <script type="text/ng-template" id="myTooltipTemplateSpecialDealsForRooms.html">
                                         <div class="row">
                                             <div class="col-md-12" ng-repeat="deal in ::room.content['special-deals'][0]['special-deal']">
                                                 <div class="col-md-12 text-center">{{ deal.title[0] }}</div>
                                                 <div class="col-md-12">{{ deal.description[0] }}</div>
                                             </div>
                                         </div>
                                    </script>
                                    <script type="text/ng-template" id="myTooltipTemplateSpecialDeals.html">
                                         <div class="row">
                                             <div class="col-md-12" ng-repeat="deal in ::hotel.request.content['special-deals'][0]['special-deal'] track by $index">
                                                {{ deal }}
                                             </div>
                                         </div>
                                    </script>
                                    <script type="text/ng-template" id="myTooltipTemplateHotel.html">
                                         <div class="row">
                                             <div class="col-md-7 text-left"><strong>Gross Amount</strong>: </div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{hotel.calculation.calculation.gross_amount|number:2}}</div>
                                             <div class="col-md-7 text-left"><strong>Commission</strong>:</div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{hotel.calculation.calculation.commission|number:2}}</div>
                                             <div class="col-md-7 text-left"><strong>Nett Amount</strong>:</div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{hotel.calculation.calculation.due_amount|number:2}}</div>
                                         </div>
                                    </script>
                                    <script type="text/ng-template" id="myTooltipTemplateRoom.html">
                                         <div class="row">
                                             <div class="col-md-7 text-left"><strong>Gross Amount</strong>: </div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ room.content.price[0].calculation.calculation.gross_amount|number:2 }}</div>
                                             <div class="col-md-7 text-left"><strong>Commission</strong>:</div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ room.content.price[0].calculation.calculation.commission|number:2 }}</div>
                                             <div class="col-md-7 text-left"><strong>Nett Amount</strong>:</div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ room.content.price[0].calculation.calculation.due_amount|number:2 }}</div>
                                         </div>
                                    </script>
                                    <script type="text/ng-template" id="myTooltipTemplateCombination.html">
                                         <div class="row">
                                             <div class="col-md-7 text-left"><strong>Gross Amount</strong>: </div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ combination.calculation.calculation.gross_amount|number:2 }}</div>
                                             <div class="col-md-7 text-left"><strong>Commission</strong>:</div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ combination.calculation.calculation.commission|number:2 }}</div>
                                             <div class="col-md-7 text-left"><strong>Nett Amount</strong>:</div>
                                             <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ combination.calculation.calculation.due_amount|number:2 }}</div>
                                         </div>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>