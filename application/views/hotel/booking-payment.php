<?php $policies = $booking_info['response'][0]['content']['byRoomPolicies']; ?>

<section class="hotel-checkout">
	<div class="container">
		<div class="col-sm-12">
			<a href="#" class="btn btn-primary"><span class="fa fa-search">Back to Search Page</span></a>
			<div class="page-header">
				<h4 class="text-uppercase">assign guest/customer information</h4>
			</div>
		</div>

		<!--=============================
		=            Content            =
		==============================-->
		
		<div class="col-md-8 content">
			<div class="passengers">
				<form id="passengers">
					<?php foreach ( $request['rooms'] as $key => $room ): ?>
					<div class="page-header">
						<h5>Room <small>( <?=$policies[$key]['name'] ?> )</small></h5>
					</div>
					<input type="hidden" name="rooms[<?=$key ?>][id]" value="<?=$result_ids[ $key ]['id'] ?>">
					<div class="wrapper pull-left">
						<div class="col-md-6 adult">
							<?php foreach ( range(1, intval($room['adult'])) as $index ): ?>
								<div class="passenger" data-adult="<?=$index ?>">
									<div class="page-header">
										<h6>Adult <?=$index ?></h6>
									</div>
									<div class="first-name form-group col-sm-6">
										<label for="passenger-adult-first-name-<?=$index . '' . $key ?>">First name</label>
										<div class="input-group">
											<span class="input-group-addon select">
												<select name="rooms[<?=$key ?>][adults][<?=$index ?>][title]" <?=($key == 0 && $index == 1) ? 'data-bind="voucher[title]"': '' ?>>
													<option value="">Title</option>
													<option value="Mr">Mr.</option>
													<option value="Ms">Ms.</option>
													<option value="Mrs">Mrs.</option>
												</select>
											</span>
											<input type="text" class="form-control" name="rooms[<?=$key ?>][adults][<?=$index ?>][firstname]" id="passenger-adult-first-name-<?=$index . '' . $key ?>" <?=($key == 0 && $index == 1) ? 'data-bind="voucher[firstname]"': '' ?>>
										</div>
									</div>
									<div class="last-name form-group col-sm-6">
										<label for="passenger-adult-last-name-<?=$index . '' . $key ?>">Last name</label>
										<input type="text" class="form-control" name="rooms[<?=$key ?>][adults][<?=$index ?>][lastname]" id="passenger-adult-last-name-<?=$index . '' . $key ?>" <?=($key == 0 && $index == 1) ? 'data-bind="voucher[lastname]"': '' ?>>
									</div>
								</div>
							<?php endforeach ?>
						</div>
						<div class="col-md-6 adult">
							<?php if ( intval($room['child']) ): ?>
								<?php foreach ( range(1, intval($room['child'])) as $index ): ?>
									<input type="hidden" class="form-control" name="rooms[<?=$key ?>][child][<?=$index ?>][age]" value="<?=$request['rooms'][0]['childAges'][($index - 1)]?>">
									<div class="passenger" data-child="<?=$index ?>">
										<div class="page-header">
											<h6>Child <?=$index ?></h6>
										</div> 
										<div class="first-name form-group col-sm-6">
											<label for="passenger-child-first-name-<?=$index . '' . $key ?>">First name</label>
											<div class="input-group">
												<span class="input-group-addon select">
													<select name="rooms[<?=$key ?>][child][<?=$index ?>][title]" id="">
														<option value="">Title</option>
														<option value="Mr">Mr.</option>
														<option value="Ms">Ms.</option>
														<option value="Mrs">Mrs.</option>
													</select>
												</span>
												<input type="text" class="form-control" name="rooms[<?=$key ?>][child][<?=$index ?>][firstname]" id="passenger-child-first-name-<?=$index . '' . $key ?>">
											</div>
										</div>
										<div class="last-name form-group col-sm-6">
											<label for="passenger-child-last-name-<?=$index . '' . $key ?>">Last name</label>
											<input type="text" class="form-control" name="rooms[<?=$key ?>][child][<?=$index ?>][lastname]" id="passenger-child-last-name-<?=$index . '' . $key ?>">
										</div>
									</div>
								<?php endforeach ?>
							<?php endif ?>
						</div>
					</div>
					<div class="clearfix"></div>
					<?php endforeach ?>
				</form>
			</div>
			<div class="clearfix"></div>
			<div class="voucher">
				<div class="page-header">
					<h5>Voucher Information</h5>
				</div>
				<div class="wrapper pull-left">
					<br>
					<form class="col-md-12 voucher" id="voucher">
						<div class="form-group col-sm-6">
							<label for="voucher-firstname">First name</label>
							<div class="input-group">
								<span class="input-group-addon select">
									<select name="voucher[title]" id="">
										<option value="">Title</option>
										<option value="Mr">Mr.</option>
										<option value="Ms">Ms.</option>
										<option value="Mrs">Mrs.</option>
									</select>
								</span>
								<input type="text" class="form-control" name="voucher[firstname]" id="voucher-firstname">
							</div>
						</div>
						<div class="form-group col-sm-6">
							<label for="voucher-lastname">Last name</label>
							<input type="text" class="form-control" name="voucher[lastname]" id="voucher-lastname">
						</div>
						<div class="form-group col-sm-6">
							<label for="voucher-email">Email Address</label>
							<input type="text" class="form-control" name="voucher[email]" id="voucher-email">
						</div>
						<div class="form-group col-sm-6">
							<label for="voucher-phone">Phone Number</label>
							<input type="text" class="form-control" name="voucher[phone]" id="voucher-phone">
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="special-requirements">
				<div class="page-header">
					<h5>Special Requirements <i>( Optional )</i></h5>
				</div>
				<div class="wrapper pull-left col-xs-12">
					<div class="form-group">
						<label for="request">Please write your request in English</label>
						<textarea name="request" id="request" cols="30" rows="10" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="cancellation-policy">
				<div class="page-header">
					<h5>Cancellation Policy</h5>
				</div>

				<div class="wrapper pull-left col-xs-12">
					<?php foreach ( $policies as $key => $policy ): ?>
						<?php if( count($policy['cancellation']) > 0 ) :?>
						<p><strong><?=$policy['name'] ?></strong></p>
                        <ul>
                            <?php foreach ($policy['cancellation'] as $keys => $value) :?>
                                <li>From <strong><?=$value['startTime']?></strong> <?=(!is_null($value['endTimeFormatted']))?'up to ' . $value['endTimeFormatted']:''?> this item is <strong><?=$value['type']?></strong></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php else : ?>
                            No policy
                        <?php endif; ?>
					<?php endforeach ?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="payment">
				<div class="page-header">
					<h5>Payment</h5>
				</div>
				<div class="wrapper pull-left col-xs-12">
					<form id="payment-credit-card">
						<div class="form-group col-sm-6">
							<label for="credit-card-cardholder-name">Card Holder Name</label>
							<input type="text" name="nm_card_holder" id="credit-card-cardholder-name" class="form-control">
						</div>
						<div class="form-group col-sm-6">
							<label for="credit-card-card-number">Card Number</label>
							<input type="text" name="no_credit_card" id="credit-card-card-number" class="form-control">
						</div>
						<div class="form-group col-sm-6">
							<label for="credit-card-verification-number">Card Verification Number</label>
							<input type="text" name="no_cvn" id="credit-card-verification-number" class="form-control">
						</div>
						<div class="form-group col-sm-6">
							<label for="credit-card-expiry-month">Expiry Date</label>
							<div>
								<select name="dt_expiry_month" id="credit-card-expiry-month">
									<option value="">-- Month --</option>
									<?php for($m=1; $m<=12; ++$m): ?>
                                        <option value="<?=($m < 10)?'0' . $m: $m?>"><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                    <?php endfor; ?>
								</select><select name="dt_expiry_year" id="">
									<option value="">-- Year --</option>
									<?php $startDate = date('y', strtotime('now')); ?>
									<?php foreach (range(intval($startDate), intval($startDate) + 10) as $key => $value) : ?>
                                        <option value="<?=$value?>">20<?=$value?></option>
                                    <?php endforeach; ?>
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
			<div>
				<h6 id="ship-to-different-address">
	                <input id="ship-to-different-address-checkbox" type="checkbox">
	                <label for="ship-to-different-address-checkbox">I have read the cancellation policy</label>
	            </h6>
			</div>
			<hr>
			<div class="text-right"><button type="button" class="CD-btn btn-3 btn-3e">Book Now </button></div>
		</div>
		
		<!--====  End of Content  ====-->
		
		

		<!--=============================
		=            Sidebar            =
		==============================-->
		
		<div class="col-md-4 side-bar">
			<div class="alert alert-danger header">
				<div class="page-header">
					<h5 class="text-center text-uppercase">booking info</h5>
				</div>
			</div>
			<div class="alert alert-danger body">
				<div class="form-horizontal">
					<?php $bookingPrice = 0 ?>
					<?php $bookingOptions = $booking_info['response'][0]['content']['booking-options'][0]['content']['booking-option']; ?>

					<?php foreach ( $bookingOptions as $key => $bookingOption ): ?>
					<?php
						$itemPrice = ( isset($bookingOption['content']['room'][0]['price'][0]) )? $bookingOption['content']['room'][0]['price'][0]: $bookingOption['content']['room'][0]['content']['price'][0];
						$itemName  = ( isset($bookingOption['content']['room'][0]['name'][0]) )? $bookingOption['content']['room'][0]['name'][0]: $bookingOption['content']['room'][0]['content']['name'][0];
						$itemBoard = ( isset($bookingOption['content']['room'][0]['board'][0]['content']) )? $bookingOption['content']['room'][0]['board'][0]['content']: $bookingOption['content']['room'][0]['content']['board'][0]['content'];
						$itemKey   = array_search($bookingOption['token'], array_map(function ($element) { return $element['id']; }, $result_ids));
						if( is_integer($itemKey) ) {
                            $itemPrice['calculation']['calculation']['due_amount'] = (float)$itemPrice['calculation']['calculation']['due_amount'] * (int)$result_ids[ $itemKey ]['qty'];
                        }
                        $bookingPrice += (float)$itemPrice['calculation']['calculation']['due_amount'];
					?>
					<div class="form-group">
						<label for="" class="control-label col-sm-7 text-left">
							<?=$itemName ?> <br>
							<small><i>(<?=$itemBoard ?>)</i></small>
						</label>
						<div class="col-sm-5">
							<p class="form-control-static"><strong>:</strong> <span class="fa fa-usd"></span><?=number_format($itemPrice['calculation']['calculation']['due_amount'], 2) ?></p>
						</div>
					</div>
					<?php endforeach ?>
				</div>
			</div>
			<div class="alert alert-danger footer">
				<small>Total for this booking</small>
				<h4><span class="fa fa-usd"></span><?=number_format(round($bookingPrice), 2) ?></h4>
			</div>
		</div>
		
		<!--====  End of Sidebar  ====-->
		
	</div>
</section>