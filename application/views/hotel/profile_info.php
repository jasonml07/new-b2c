<div class="container" >
      <div class="row">
            <div class="col-md-6" style="margin-bottom: 50px; margin-top: 20px;">
                <div class="panel panel-themes" style="margin-top: 10%; ">
                        <div class="panel-themes">
                                <span class="pull-right" style="margin-top: -110px; margin-right: 5px; margin-bottom: 60px;">
                                    <div class='time-frame' style ="background-color:#fff; color: #333; font-weight: bold; font-family: Arial; width:160px; text-align:center; margin-top: 20px;  border-radius: 10px; margin-left: 30px; padding: 10px; border: 2px solid #f2f2f2;">
                                    <div id='date-part'></div>
                                    <div id='time-part'></div>
                                    </div>
                                </span> 
                                    <h3 class="panel-title" style=" margin-top: 20px; margin-left: 20px; color: #fff;"><?=$data['firstname']." ".$data['middlename']." ".$data['lastname']?>
                                    </h3>
                        </div>
                    <div class="panel-body" style="background-color: #fff; margin-top: 30px; margin-bottom: 30px;>
                            <div class="row">
                                 <div class="col-md-3 col-lg-3 " align="center"> 
                                        <?php if(!$data['profile_picture']):?>
                                                <img alt="User Pic" id="profilepic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive">
                                        <?php else:?>
                                                <img alt="User Pic" id="profilepic" src="<?php echo base_url()."uploads/".$data['profile_picture'].".jpg"?>" class="img-circle img-responsive">
                                        <?php endif;?>    
                                                <input type="hidden" class="form-control" name="user_pic" value="" id="user_pic">
                                        <form action="" method="POST">
                                                <div class="col-md-offset-3 col-md-6" style="margin-top: 20px;margin-left:-25px;" >
                                                    <style type="text/css">                       
                                                             .uploadify {
                                                                position: relative;
                                                            }
                                                            .uploadify-button {
                                                                color: #fff;
                                                                border-radius: 8px;
                                                                background-color: #000154;

                                                            }
                                                            .uploadify:hover .uploadify-button {
                                                                background-position: center bottom;
                                                                color: #fff;
                                                                background-color: #337ab7;

                                                            }
                                                            .uploadify-button.disabled {
                                                                background-color: #D0D0D0;
                                                                color: #808080;
                                                            }
                                                            .uploadify-queue {
                                                                background-color: #c3daee;
                                                                color:#9cc2e3;
                                                                margin-bottom: 10px;       
                                                            }
                                                            .uploadify-queue-item {
                                                                background-color: #ebf3f9;
                                                                border-radius: 2px;
                                                                color: #337ab7;
                                                              }
                                                            .uploadify-error {
                                                                background-color: #FDE5DD !important;
                                                            }
                                                            .uploadify-queue-item .cancel a {
                                                                background: url('../img/uploadify-cancel.png') 0 0 no-repeat;     
                                                            }
                                                            .uploadify-queue-item.completed {
                                                                background-color: #E5E5E5;
                                                            }
                                                            .uploadify-progress {
                                                                background-color: #E5E5E5; 
                                                            }
                                                            .uploadify-progress-bar {
                                                                background-color: #0099FF; 
                                                            }
                                                    </style>
                                                    <button type="button" name="userfile" id="upload_btn" class= "btn btn-success btn-sm">
                                                    </button>
                                                    <a href="#modalProf" data-original-title="Edit this user" data-toggle="modal" type="button" class="btn btn-sm btn-success" style = "height:30;width:120px;border-radius:8px; margin-top: 15px;">Profile Settings
                                                    </a>
                                                    <a href="#modalAccount" data-original-title="Edit this user" data-toggle="modal" type="button" class="btn btn-sm btn-success" style = "height:30;width:120px;border-radius:8px;margin-top:15px;">Account Settings
                                                    </a>
                                                    
                                                </div>
                                        </form>
                                </div>
                                
                                <div class="col-md-9 col-lg-9" style="margin-bottom: 20px; margin-top: 30px;"> 
                                        <table class="table table-user-information">
                                                <tbody >
                                                    <tr>
                                                         <td>Email:</td>
                                                        <td style="font-weight: bold;" ><?=$data['user_email']?></td>
                                                    </tr>
                                                     <tr>
                                                        <td>Contact Number:</td>
                                                        <td style="font-weight: bold;"><?=$data['contact']?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>City Address:</td>
                                                        <td style="font-weight: bold;"><?=$data['cityAdd']?></td>
                                                     </tr>
                                                    <tr>
                                                        <td >Country:</td>
                                                        <td style="font-weight: bold;"><?=$data['country']?></td>
                                                    </tr>
                                                     <tr>
                                                    <td >Date of Birth</td>
                                                    <td style="font-weight: bold;"><?=$data['birthday']?></td>
                                                  </tr>
                                                     <tr>
                                                         <tr>
                                                    <td >Gender</td>
                                                    <td style="font-weight: bold;" ><?=$data['gender']?></td>
                                                    </tr>
                                                </tbody>
                                        </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modalProf" role="dialog" aria-labelledby="myModalLabel" style="top: 60px;">
        
<div class="modal-dialog" role="document">
    <div class="modal-content">
            <div class="modal-header panel-themes">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title center" style="font-size: 18px; color: #fff;">Profile Information</h4>
             </div>

             <div class="modal-body"  >
                    <div id="page-wrap" style="background-color: #fff;" >
                            <form role="form" id = "customer_form" method = "POST" action = "<?php echo base_url();?>/hotel/saveCustomerInfo"  >
                                <div class="col-md-12" style="margin-top: 10px; ">
                                <div class="tabbable" style="margin-top: 10px;">
                                    <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class ="btn-sm btn panel-themes"  style="">Section 1</a></li>
                                    <li><a href="#tab2" data-toggle="tab" class = "btn-sm panel-themes">Section 2</a></li>
                                    <!-- I SHOULD CHANGE THE HOVER COLOR BUUUUT WHHHY!!!?? -->
                                    </ul>
                                <div class="tab-content" >
                                    <div class="tab-pane active" id="tab1">    
                                    <div class="col-md-12" style="margin-top: 20px;">
                                        <div class="form-group ">
                                            <label for="InputName" style="font-size: 12px;">Firstname</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter Firstname" value="<?=$data['firstname']?>">
                                                <span class="input-group-addon"><span id="fnames" class="glyphicon glyphicon-asterisk"></span></span>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-12">    
                                    <div class="form-group">
                                        <label for="InputEmail" style="font-size: 12px;">Middle Name</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="mname" name="mname" value="<?=$data['middlename']?>" placeholder="Enter Middle Name">
                                            <span class="input-group-addon"><span id="mnames" class="glyphicon glyphicon-asterisk"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="InputEmail" style="font-size: 12px;">Lastname</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="lname" name="lname" value="<?=$data['lastname']?>" placeholder="Enter Lastname">
                                            <span class="input-group-addon"><span id="lnames" class="glyphicon glyphicon-asterisk"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                 <div class="form-group">
                                      <label for="gender">Gender</label>
                                      <select class="form-control" id="sel1" name = "gender">
                                        <option value = "Male">Male</option>
                                        <option value = "Female">Female</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <div class="form-group">
                                        <label for="InputEmail" style="margin-bottom: 5px; font-size: 12px;">Birthday</label>
                                        <div class="input-group" id = "birthday">
                                            <fieldset class='birthday-picker'>
                                            <label>Month</label>
                                             <select class='birth-month' id ="month" name='birth[month]'></select>
                                            <label>Day</label> 
                                             <select class='birth-day' id ="day" name='birth[day]'></select>
                                            <label>Year</label> 
                                             <select class='birth-year' id ="year" name='birth[year]'></select>
                                            </fieldset>
                                            
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="tab-pane" id="tab2">
                                <div class="col-md-12" style="margin-top: 30px;">    
                                    <div class="form-group">
                                        <label for="InputEmail" style="font-size: 12px;">Contact Number</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="contact" name="contact" value="<?=$data['contact']?>" placeholder="Enter Contact Number">
                                            <span class="input-group-addon"><span id="contacts" class="glyphicon glyphicon-asterisk"></span></span>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-12">    
                                    <div class="form-group">
                                        <label for="InputEmail" style="font-size: 12px;">City Address</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="cityAdd" name="cityAdd" value="<?=$data['cityAdd']?>" placeholder="Enter City Address">
                                            <span class="input-group-addon"><span id="cityAdds" class="glyphicon glyphicon-asterisk"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">    
                                    <div class="form-group">
                                        <label for="InputEmail" style="font-size: 12px;">Country</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="country" name="country" value="<?=$data['country']?>" placeholder="Enter Country">
                                            <span class="input-group-addon"><span id="countrys" class="glyphicon glyphicon-asterisk"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">    
                                    <div class="form-group">
                                        <label for="InputEmail" style="font-size: 12px;">Zip Code</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="zipCode" name="zipCode" value="<?=$data['zip']?>" placeholder="Enter Zip Code">
                                            <span class="input-group-addon"><span id="zipCodes" class="glyphicon glyphicon-asterisk"></span></span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div style="margin-bottom: 20px; margin-right: 20px;" class = "pull-right" >
                                        <button type="submit" name="submit" id="submit" class="btn btn-success btn-sm">Save</button>
                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                </div>
                                
                            </div>   
                            </div> 
                            </div> 
                    </form> 
                    </div>
                </div>
                </div>
            </div> 
        </div>
 </div>

<div id="modalAccount" class="modal fade" role="dialog" style="margin-top:40px;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header panel-themes">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: #fff; font-size: 18px;">Account Settings</h4>
      </div>
      <div class="modal-body">
        <div id="page-wrap" style="border: 2px #fff;">
            <form role="form" id = "customerAccount" method = "POST" action = "<?php echo base_url();?>/hotel/saveCustomerAccount"  >
                <div class="col-md-12" style="background-color: #fff; margin-top: 20px; ">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#newtab1" data-toggle="tab" class = "btn-sm panel-themes" onClick = "changeuser()" style="font-size: 12px;">Change Username</a></li>
                    <li><a href="#newtab2" data-toggle="tab" class = "btn-sm panel-themes" onClick = "changepass()">Change Password</a></li>
                    </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="newtab1">    
                    <div class="col-md-12" style="margin-top: 30px;">
                        <div class="form-group">
                            <label for="InputName" style="font-size: 12px;">Old Username</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="Ouname" id="Ouname" value="">
                                <span class="input-group-addon"><span id="Ounames" class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                    </div>
                <div class="col-md-12">    
                    <div class="form-group">
                        <label for="InputEmail" style="font-size: 12px;">New Username</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="Nuname" name="Nuname" value="">
                            <span class="input-group-addon"><span id="Nunames" class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                </div>
                </div>
                <div class="tab-pane" id="newtab2">
                <div class="col-md-12" style="margin-top: 30px;">    
                    <div class="form-group">
                        <label for="InputEmail" style="font-size: 12px;">Old Password</label>
                        <div class="input-group">
                            <input type="password" class="form-control" id="Opass" name="Opass" value="">
                            <span class="input-group-addon"><span id="Opassword" class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                </div>
                 <div class="col-md-12">    
                    <div class="form-group">
                        <label for="InputEmail" style="font-size: 12px;">New Password</label>
                        <div class="input-group">
                            <input type="password" class="form-control" id="Npass" name="Npass" value="" >
                            <span class="input-group-addon"><span id="cityAdds" class="glyphicon glyphicon-asterisk"></span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">    
                    <div class="form-group">
                        <label for="InputEmail" style="font-size: 12px;"    >Confirm Password</label>
                        <div class="input-group">
                            <input type="password" class="form-control" id="Cpass" name="Cpass" value="" >
                            <span class="input-group-addon"><span id="Cpassword" class="glyphicon glyphicon-asterisk"></span></span>
                        </div> 
                        <label id = "warning-label"></label>   
                    </div>
                </div>
                </div>
                <div style="margin-bottom: 20px; margin-top: 30px;" class = "pull-right">
                        <button type="submit" name="submit" id="submitAccount" class="btn btn-success btn-sm" disabled="disabled">Save</button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
                
            </div>   
            </div> 
            </div> 
    </form> 
    </div>
      </div>
    </div>

  </div>
</div>

 <script type="text/javascript" src="<?php echo base_url();?>static/js/jquery-1.11.2.min.js"></script>
 <script type="text/javascript">
     jQuery(document).ready(function(){
            var count = 0;
            var months = [];
            while (count < 12) months.push(moment().month(count++).format("MMMM"));
            //console.log(months[0]);
            //var x = 0;
            for(x = 0;months.length > x;x++){
              //console.log(months[x]);
            var variable2 = months[x].substring(0, 3);  
            jQuery('.birth-month').append(jQuery("<option></option>").attr("value",variable2).text(variable2)); 
            }
            for(a = 1;a <= 31;a++){
            jQuery('.birth-day').append(jQuery("<option></option>").attr("value",a).text(a));     
            }
            for(b = 1970;b <= 2015;b++){
            jQuery('.birth-year').append(jQuery("<option></option>").attr("value",b).text(b));     
            }

            var birthday = "<?php echo $data['birthday'];?>";
            var month = birthday.split(" ");
            var dayYear = month[1].split(",");
            //console.log(month[0]);
            jQuery('#month option[value=' + month[0] + ']').attr('selected', true);
            jQuery('#day option[value=' + dayYear[0] + ']').attr('selected', true);
            jQuery('#year option[value=' + dayYear[1] + ']').attr('selected', true);
            
        });
 </script>