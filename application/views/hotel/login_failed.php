<div id="page-wrap" style="">
        <div class="container" style="margin-top: 20px; background-color: #f2f2f2; border-radius: 10px;">
                    <form role="form" id = "customer_form" method = "POST" action = "<?php echo base_url();?>hotel/customerLogin">
                            <div class="col-md-12">
                                    <div class="col-md-6 panel-themes" style="float: none; margin: 0 auto; padding-bottom: 50px; margin-top: 60px; margin-bottom: 60px; border-radius: 10px;">
                                            <span class="glyphicon glyphicon-exclamation-sign alert alert-danger" style="width: 100%; height: 30%; margin-top: 30px; text-indent: 2px; font-size: 16px;">
                                                    <span>
                                                        Error: Username/Email or Password is incorrect!
                                                    </span>
                                            </span>
                                            <div class="form-group" style="margin-top: 20px;">
                                                    <input type = "hidden" name = "agencyCode" value = "<?php echo $this->session->userdata('agencycode');?>">
                                                    <label for="InputName" style="color: #fff;">Username</label>
                                                    <div class="input-group">
                                                            <input type="text" class="form-control" name="username" id="username" placeholder="Enter Username or Email" required>
                                                            <span class="input-group-addon"></span>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label for="InputEmail" style="color: #fff;">Password</label>
                                                        <div class="input-group">
                                                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
                                                            <span class="input-group-addon"></span>
                                                        </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 30px; margin-bottom: 10px;">
                                                    <input type="submit" name="submit" id="submit" value="Login" class="btn btn-info pull-right" >
                                            </div>
                                    </div>
                            </div>


                    </form>
        </div>
        <!-- <div class="container panel-themes" style="margin-top: 20px; ">
            <div class="row ">
                        <form role="form" id = "customer_form" method = "POST" action = "<?php echo base_url();?>hotel/customerLogin">
                            <div class="col-lg-6 panel-themes" style = "float: none;margin: 0 auto; margin-top: 30px;">
                                    <div class="alert alert-danger" role="alert">
                                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                      <span class="sr-only">Error:</span>
                                      Username or Password Error!
                                    </div>

                                    <div class="form-group">
                                        <input type = "hidden" name = "agencyCode" value = "<?php echo $this->session->userdata('agencycode');?>">
                                            <label for="InputName">Username</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="username" id="username" placeholder="Enter Firstname" required>
                                                <span class="input-group-addon"></span>
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <label for="InputEmail">Password</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="password" name="password" placeholder="Enter Middle Name" required>
                                                <span class="input-group-addon"></span>
                                            </div>
                                    </div>
                                    <input type="submit" name="submit" id="submit" value="Login" class="btn btn-info pull-right">
                            </div>
                        </form>
            </div>
        </div> -->
</div>









