<?php //print_r($this->session->userdata('session_data'));?>
<div class="container">
    <div class="row">
    	<div class="col-md-4 col-md-offset-4">
    		<div class="panel panel-default">
			  	<div class="panel-heading" style = "text-align:center">
			    	<h3 class="panel-title" id = "response">Login via site</h3>
			 	</div>
			  	<div class="panel-body">
			    	<form accept-charset="UTF-8" role="form" id = "customerCredentials">
		                    <fieldset>
					    	  	<div class="form-group">
					    		    <input class="form-control" placeholder="Email/Username" name="email" type="text">
					    		</div>
					    		<div class="form-group">
					    			<input class="form-control" placeholder="Password" name="password" type="password" value="">
					    		</div>
					    		<div class="checkbox">
					    	    	<label>
					    	    		<input name="remember" type="checkbox" value="Remember Me"> Remember Me
					    	    	</label>
					    	    </div>
					    		<input class="btn btn-lg btn-success btn-block" type="button" value="Login" onCLick = "logMe()">
					    	</fieldset>
			      	</form>
                      <hr/>
                    <center><h4 style="font-size: 12px !important;">OR</h4></center>
                    <input class="btn btn-lg btn-facebook btn-block" type="submit" value="Login via facebook">
			    </div>
			</div>
		</div>
	</div>
</div>