
<div id="page-wrap">
<div class="container">
    <div class="row">
        <form role="form" id = "customer_form" method = "POST" action = "<?php echo base_url();?>/hotel/saveCustomerInfo">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="InputName">Firstname</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter Firstname" required>
                        <span class="input-group-addon"><span id="fnames" class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Middle Name</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="mname" name="mname" placeholder="Enter Middle Name" required>
                        <span class="input-group-addon"><span id="mnames" class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Lastname</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter Lastname" required>
                        <span class="input-group-addon"><span id="lnames" class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Contact Number</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter City Address" required>
                        <span class="input-group-addon"><span id="contacts" class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label for="InputMessage">Enter Message</label>
                    <div class="input-group">
                        <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" required></textarea>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div> -->
                <div class="form-group">
                    <label for="InputEmail">City Address</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="cityAdd" name="cityAdd" placeholder="Enter City Address" required>
                        <span class="input-group-addon"><span id="cityAdds" class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Country</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="country" name="country" placeholder="Enter Country" required>
                        <span class="input-group-addon"><span id="countrys" class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Zip Code</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="zipCode" name="zipCode" placeholder="Enter Zip Code" required>
                        <span class="input-group-addon"><span id="zipCodes" class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
            </div>
        </form>
        <!-- <div class="col-lg-5 col-md-push-1">
            <div class="col-md-12">
                <div class="alert alert-success">
                    <strong><span class="glyphicon glyphicon-ok"></span> Success! Message sent.</strong>
                </div>
                <div class="alert alert-danger">
                    <span class="glyphicon glyphicon-remove"></span><strong> Error! Please check all page inputs.</strong>
                </div>
            </div>
        </div> -->
    </div>
</div>
</div>









