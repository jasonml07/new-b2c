
        <?php
            if ( !is_array($rooms['response'][0]['content']['result'][0]['content']['rooms'][0]['room']) ) {
                // redirect(base_url() + 'hotel');
            }
        ?>
        <div ng-app="DetailApplication" ng-controller="DetailController" ng-init="rooms = []; data.numberOfGuest = <?php print_r(str_replace("\"", "'", json_encode($numberOfGuest))); ?>">
            <!-- BREADCRUMB -->
            <section >
                <div class="container">
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?=base_url();?>hotel">Dashboard</a></li>
                            <li><a href="<?=base_url();?>hotel/result">Go to result list</a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- BREADCRUMB -->

            <section class="product-detail" style="padding-bottom: 50px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="product-detail__info">
                                <div class="product-title">
                                    <h2><?=$hotel['details']['hotelname']?></h2>
                                    <div class="hotel-star">
                                    <?php for ($i=0; $i < intval($hotel['details']['rating']); $i++) : ?>
                                        <i class="fa fa-star"></i>
                                    <?php endfor; ?>
                                    </div>
                                </div>
                                <div class="product-address">
                                    <span><?=$hotel['details']['address']?> | <?=$hotel['details']['cityid']?></span>
                                </div>
                                <a target="_blank" href="<?=((isset($hotel['details']['reviews']['average']))?$hotel['details']['reviews']['url']:'')?>">
                                    <small><span style="color: #000; font-size: .9em;"><strong>TripAdvisor Traveller Rating:</strong></span></small>
                                    <br>
                                    <img src="http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/<?=number_format(((isset($hotel['details']['reviews']['average']))?$hotel['details']['reviews']['average']: 0), 1, '.', '')?>-11367-5.gif"/>
                                    <br>
                                    <small><span style="font-size: .9em;"><strong>Based on <?=((isset($hotel['details']['reviews']['average']))?$hotel['details']['reviews']['count']: 0)?> traveller reviews</strong></span></small>
                                </a>
                                <?php if( isset($hotel['request']['content']['special-deals']) ) :?>
                                    <div>
                                        <span style="background-color: #a94442;" class="badge" tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'hotelSpecialDeals.html'"><span class="fa fa-check"></span> Special Deals</span>

                                        <script type="text/ng-template" id="hotelSpecialDeals.html">
                                            <div class="row">
                                                <?php foreach ($hotel['request']['content']['special-deals'][0]['special-deal'] as $key => $value) :?>
                                                    <div class="col col-md-12"><?=$value?></div>
                                                <?php endforeach; ?>
                                            </div>
                                        </script>
                                    </div>
                                <?php endif; ?>
                                <div class="product-descriptions">
                                    <p><?=$hotel['details']['description']?></p>
                                </div>
                                <!-- <div class="property-highlights">
                                    <h3>Property highlights</h3>
                                    <div class="property-highlights__content">
                                        <div class="item">
                                            <i class="awe-icon awe-icon-unlock"></i>
                                            <span>Room service</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-beds"></i>
                                            <span>Bunkbed available</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-beds"></i>
                                            <span>Bunkbed available</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-laundry"></i>
                                            <span>Laundry</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-shower"></i>
                                            <span>Shower</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-shower"></i>
                                            <span>Shower</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-pool"></i>
                                            <span>Outside Pool</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-meal"></i>
                                            <span>Room meal service</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-meal"></i>
                                            <span>Room meal service</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-key"></i>
                                            <span>High security</span>
                                        </div>
                                        <div class="item">
                                            <i class="awe-icon awe-icon-tv"></i>
                                            <span>TV in room</span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product-detail__gallery">
                                <div class="product-slider-wrapper">
                                    <div class="product-slider">
                                        <?php if ( is_null($hotel['hotelImages']) ) : ?>
                                            <div class="item" style="height: 240px; width: 100%;">
                                                <img class="img-responsive" src="http://maps.googleapis.com/maps/api/staticmap?center=<?=$hotel['details']['cityid']?>&zoom=7&scale=false&size=600x600&maptype=roadmap&format=png&visual_refresh=true" alt="Google Map of <?=$hotel['details']['cityid']?>">
                                            </div>
                                        <?php else : ?>
                                            <?php foreach ($hotel['hotelImages'] as $key => $image) : ?>
                                                <div class="item">
                                                    <img src="http://c775792.r92.cf2.rackcdn.com/sml/<?=$image['url']?>" alt="<?=$image['title']?>">
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="product-slider-thumb-row">
                                        <div class="product-slider-thumb">
                                            <?php if ( !is_null($hotel['hotelImages']) ) : ?>
                                                <?php foreach ($hotel['hotelImages'] as $key => $image) : ?>
                                                    <div class="item">
                                                        <img src="http://c775792.r92.cf2.rackcdn.com/sml/<?=$image['url']?>" alt="<?=$image['title']?>">
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="product-map">
                                    <div data-latlong="21.036697, 105.834871"></div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-12">
                            <iframe style="height: 450px; width: 100%;" src="https://www.tripadvisor.co.uk/WidgetEmbed%C2%ADcdspropertydetail?partnerId=C6F7D3BC2D1A43E2AD371C2ED6F93D9F&display=true&locationId=<?=$_GET['id']?>&lang=en_uk" frameborder="0"></iframe>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-tabs tabs">
                                <ul>
                                    <li>
                                        <a href="#tabs-1">Room detail</a>
                                    </li>
                                    <li>
                                        <a href="#tabs-2">Facilities &amp; freebies</a>
                                    </li>
                                    <li>
                                        <a href="#tabs-3">Good to know</a>
                                    </li>
                                    <li>
                                        <a href="#tabs-4">Review &amp; rating</a>
                                    </li>
                                </ul>
                                <div class="product-tabs__content">
                                    <?php $roomss = $rooms['response'][0]['content']['result'][0]['content']['rooms'][0]['room']; ?>
                                    <!-- <script type="text/javascript">console.log('number guest', <?=json_encode($roomss)?>);</script> -->
                                    <?php $combinations = ( isset($rooms['response'][0]['content']['result'][0]['content']['combinations']) )? $rooms['response'][0]['content']['result'][0]['content']['combinations'][0]['combination']: array() ?>
                                    <div id="tabs-1">
                                        <div>
                                            <ul class="adjust-table-provider nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active">
                                                    <a href="#rooms" role="tab" data-toggle="tab"><span class="text">ROOMS</span> <span class="label label-success"><?=count($roomss)?></span></a>
                                                </li>
                                                <?php if( count($combinations) > 0 ) :?>
                                                    <li role="presentation">
                                                        <a href="#combinations" role="tab" data-toggle="tab"><span class="text">COMBINATIONS</span> <span class="label label-success"><?=count($combinations)?></span></a>
                                                    </li>
                                                <?php endif; ?>
                                            </ul>
                                            <div class="tab-content">
                                            <!-- ROOMS TABS -->
                                                <div role="tabpanel" class="tab-pane active" id="rooms">
                                                    <div class="row" style="margin:0px;">
                                                        <div class="col-md-9" style="padding-left: 0px;">
                                                            <div style="overflow-y: scroll;">
                                                                <table class="hidden-xs room-type-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="" class="adjust room-type">&nbsp;&nbsp;&nbsp;Room type</th>
                                                                            <th style="" class="room-people text-center">Guest/Pax</th>
                                                                            <th style="" class="adjust room-condition">Condition</th>
                                                                            <th style="" class="adjust room-price text-center">Today price</th>
                                                                            <th style="text-align: right;">Qty # &nbsp;&nbsp;</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                            <div style="height: 800px; overflow-y: scroll;">
                                                                <table class="room-type-table">
                                                                    <tbody>
                                                                        <?php usort($roomss, function ($a, $b) {
                                                                                $t1 = $a['content']['price'][0]['new_content'];
                                                                                $t2 = $b['content']['price'][0]['new_content'];

                                                                                return ($t1 < $t2)?-1: 1;
                                                                            }); ?>
                                                                        <?php foreach ($roomss as $roomkey => $room) : ?>
                                                                            <tr ng-init="data.rooms[<?=$roomkey?>] = <?=str_replace('"', "'", json_encode($room))?>; data.rooms[<?=$roomkey?>].selectedQuantity = '0'; data.rooms[<?=$roomkey?>].previous = {};"
                                                                                ng-style="{'background-color': ((data.rooms[<?=$roomkey?>].selectedQuantity == '0')?'':'#dff0d8') }">
                                                                                <td class="room-type" style="padding-left: 10px; padding-right: 20px;">
                                                                                    <div class="room-title" style="text-overflow: ellipsis;">
                                                                                        <h4><?=$room['content']['name'][0]?></h4>
                                                                                    </div>
                                                                                    <div class="room-descriptions">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">Board Type: <?=$room['content']['board'][0]['content']?></div>
                                                                                            <div class="col-md-12">Prices are per room</div>
                                                                                            <div class="visible-xs col-md-12" style="font-size: 1.5em;">
                                                                                                <span class="fa fa-user"></span>&nbsp;<?=$room['content']['pax'][0]['adults'][0]?> |
                                                                                                <span class="fa fa-child"></span>&nbsp;<?=(isset($room['content']['pax'][0]['children'])?$room['content']['pax'][0]['children'][0]:0)?>
                                                                                            </div>
                                                                                            <div class="visible-xs col-md-12">
                                                                                                <?php $refundable = null; ?>
                                                                                                <?php if( isset($room['content']['cancellation']) ) :?>
                                                                                                    <?php foreach ($room['content']['cancellation'][0]['frame'] as $key => $condition) : ?>
                                                                                                        <span tooltip-placement="top" uib-tooltip="From <?=$condition['startTime']?> <?=(!is_null($condition['endTimeFormatted']))?'up to ' . $condition['endTimeFormatted']:''?> this item is <?=$condition['type']?>" class="label label-primary"><?=$condition['type']?></span><br>
                                                                                                    <?php endforeach; ?>
                                                                                                <?php endif; ?>
                                                                                            </div>
                                                                                            <div class="visible-xs col-md-12">
                                                                                                <span class="amount" style="font-size: 2em;" tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'myTooltipTemplateRoom<?=$roomkey?>.html'">
                                                                                                    <span class="fa fa-<?=($room['content']['price'][0]['new_currency'] != 'AUD')?strtolower($room['content']['price'][0]['new_currency']):'usd'?>"></span>&nbsp;{{ ((data.room[<?=$roomkey?>].previous.totalPrice)?data.room[<?=$roomkey?>].previous.totalPrice: data.rooms[<?=$roomkey?>].content.price[0].calculation.calculation.due_amount)|number:2 }}
                                                                                                </span>
                                                                                                <br>
                                                                                                <em><?=$room['maxquantity']?> room available</em>
                                                                                                <!-- <div class="text-center">
                                                                                                    <button type="submit" class="btn btn-block" ng-class="{'btn-danger': !rooms[<?=$roomkey?>].isClick, 'btn-success': rooms[<?=$roomkey?>].isClick}" ng-click="selectRoom(<?=$roomkey?>, <?php print_r(str_replace("\"", "'", json_encode($room)))?>)">SELECT <span ng-if="rooms[<?=$roomkey?>].isClick" class="fa fa-check"></span></button>
                                                                                                </div> -->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php if( isset($room['content']['special-deals']) ) :?>
                                                                                        <div class="room-type-footer">
                                                                                            <span style="background-color: #a94442;" class="badge" tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'myTooltipSpecialDeals<?=$roomkey?>.html'"><span class="fa fa-check"></span> Special Deals</span>
                                                                                        </div>
                                                                                        <script type="text/ng-template" id="myTooltipSpecialDeals<?=$roomkey?>.html">
                                                                                            <div class="row">
                                                                                                <?php foreach ($room['content']['special-deals'][0]['special-deal'] as $key => $specilaDeal) : ?>
                                                                                                    <div class="col-md-12 text-center"><?=$specilaDeal['title'][0]?></div>
                                                                                                    <div class="col-md-12"><?=$specilaDeal['description'][0]?></div>
                                                                                                <?php endforeach; ?>
                                                                                            </div>
                                                                                        </script>
                                                                                    <?php endif; ?>
                                                                                </td>
                                                                                <td class="room-people text-center hidden-xs" style="font-size: 1.5em">
                                                                                    <span class="fa fa-user"></span>&nbsp;<?=$room['content']['pax'][0]['adults'][0]?><br>
                                                                                    <span class="fa fa-child"></span>&nbsp;<?=(isset($room['content']['pax'][0]['children'])?$room['content']['pax'][0]['children'][0]:0)?>
                                                                                </td>
                                                                                <td class="room-condition hidden-xs">
                                                                                    <?php $refundable = null; ?>
                                                                                    <?php if( isset($room['content']['cancellation']) ) :?>
                                                                                        <?php foreach ($room['content']['cancellation'][0]['frame'] as $key => $condition) : ?>
                                                                                            <span style="display: block; margin-bottom: 2px;" tooltip-placement="top" uib-tooltip="From <?=$condition['startTime']?> <?=(!is_null($condition['endTimeFormatted']))?'up to ' . $condition['endTimeFormatted']:''?> this item is <?=$condition['type']?>" class="label label-primary"><?=$condition['type']?></span>
                                                                                        <?php endforeach; ?>
                                                                                    <?php endif; ?>
                                                                                </td>
                                                                                <td class="adjust room-price hidden-xs">
                                                                                    <div class="price text-center">
                                                                                        <!-- <span class="amount" style="width: 150px;" tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'myTooltipTemplateRoom<?=$roomkey?>.html'"> -->
                                                                                        <?php if( $room['content']['price'][0]['calculation']['calculation']['gross_amount'] > $room['content']['price'][0]['calculation']['calculation']['due_amount'] ) :?>
                                                                                            <span class="amount" style="color: #AA3939; font-size: 1.5em; font-weight: 900; text-decoration: line-through; width: 150px;">
                                                                                                <span class="fa fa-usd"></span>&nbsp;<?=number_format($room['content']['price'][0]['calculation']['calculation']['gross_amount'], 2, '.', '')?>
                                                                                            </span>
                                                                                        <?php endif; ?>
                                                                                        <span class="amount" style="width: 150px;">
                                                                                            <span class="fa fa-usd"></span>&nbsp;{{ ((data.room[<?=$roomkey?>].previous.totalPrice)?data.room[<?=$roomkey?>].previous.totalPrice: data.rooms[<?=$roomkey?>].content.price[0].calculation.calculation.due_amount)|number:2 }}
                                                                                        </span>
                                                                                        <em><?=$room['maxquantity']?> room available</em>
                                                                                        <!-- <div class="text-center">
                                                                                            <button type="submit" class="btn btn-block" ng-class="{'btn-danger': !rooms[<?=$roomkey?>].isClick, 'btn-success': rooms[<?=$roomkey?>].isClick}" ng-click="selectRoom(<?=$roomkey?>, <?php print_r(str_replace("\"", "'", json_encode($room)))?>)">SELECT <span ng-if="rooms[<?=$roomkey?>].isClick" class="fa fa-check"></span></button>
                                                                                        </div> -->
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 50px; padding-left: 10px;">
                                                                                    <select ng-disabled="data.selectRoom.readyToBook && data.rooms[<?=$roomkey?>].selectedQuantity == '0'" id="room-<?=str_replace('.', '', $room['resultid'])?>"
                                                                                        id="room-<?=str_replace('.', '', $room['resultid'])?>"
                                                                                        ng-model="data.rooms[<?=$roomkey?>].selectedQuantity"
                                                                                        ng-change="selectRoom(<?=$roomkey?>, data.rooms[<?=$roomkey?>])">
                                                                                        <option value="0">0</option>
                                                                                        <?php if( $room['minquantity'] != $room['maxquantity'] ) :?>
                                                                                            <?php $total = (int)$room['maxquantity'];
                                                                                                $start = (int)$room['minquantity'];
                                                                                            ?>
                                                                                            <?php foreach (range($start, $total) as $i) : ?>
                                                                                                <?php if( $i > 0 ) :?>
                                                                                                    <option value="<?=$i?>"><?=$i?></option>
                                                                                                <?php endif; ?>
                                                                                            <?php endforeach; ?>
                                                                                        <?php endif; ?>
                                                                                        <?php if( $room['minquantity'] == $room['maxquantity'] ) :?>
                                                                                            <option value="<?=$room['maxquantity']?>"><?=$room['maxquantity']?></option>
                                                                                        <?php endif; ?>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <script type="text/ng-template" id="myTooltipTemplateRoom<?=$roomkey?>.html">
                                                                             <div class="row">
                                                                                 <div class="col-md-7 text-left"><strong>Commission</strong>:</div>
                                                                                 <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ data.rooms[<?=$roomkey?>].content.price[0].calculation.calculation.commission|number:2 }}</div>
                                                                                 <div class="col-md-7 text-left"><strong>Nett Amount</strong>:</div>
                                                                                 <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ data.rooms[<?=$roomkey?>].content.price[0].calculation.calculation.due_amount|number:2 }}</div>
                                                                                 <div class="col-md-7 text-left"><strong>Gross Amount</strong>: </div>
                                                                                 <div class="col-md-5 text-left"><span class="fa fa-usd"></span>{{ data.rooms[<?=$roomkey?>].content.price[0].calculation.calculation.gross_amount|number:2 }}</div>
                                                                                 <!-- <div class="col-md-5 text-left"><span class="fa fa-<?=($room['content']['price'][0]['calculation']['calculation']['currency'] != 'AUD')?strtolower($room['content']['price'][0]['calculation']['calculation']['currency']):'usd'?>"></span><?=number_format($room['content']['price'][0]['calculation']['calculation']['gross_amount'], 2, '.', '')?></div>
                                                                                 <div class="col-md-7 text-left"><strong>Commission</strong>:</div>
                                                                                 <div class="col-md-5 text-left"><span class="fa fa-<?=($room['content']['price'][0]['calculation']['calculation']['currency'] != 'AUD')?strtolower($room['content']['price'][0]['calculation']['calculation']['currency']):'usd'?>"></span><?=number_format($room['content']['price'][0]['calculation']['calculation']['commission'], 2, '.', '')?></div>
                                                                                 <div class="col-md-7 text-left"><strong>Nett Amount</strong>:</div>
                                                                                 <div class="col-md-5 text-left"><span class="fa fa-<?=($room['content']['price'][0]['calculation']['calculation']['currency'] != 'AUD')?strtolower($room['content']['price'][0]['calculation']['calculation']['currency']):'usd'?>"></span><?=number_format($room['content']['price'][0]['calculation']['calculation']['due_amount'], 2, '.', '')?></div> -->
                                                                             </div>
                                                                        </script>
                                                                        <?php endforeach; ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="padding: 0px;">
                                                            <!-- <pre>{{ data.rooms |json:4 }}</pre> -->
                                                            <div class="detail-sidebar">
                                                                <div class="booking-info">
                                                                    <h3>Booking info</h3>
                                                                    <div class="row" ng-if="data.hasSelection">
                                                                        <div ng-repeat="room in data.rooms |filter: {isAdded: true}">
                                                                            <div class="col-md-8 col-sm-8">{{ room.content.name[0] }}<br><small>({{ room.content.board[0].content }})</small></div>
                                                                            <div class="col-md-4 col-sm-4" style="padding-left: 0px; padding-right: 0px;">$ {{ room.content.price[0].calculation.calculation.due_amount|number:2 }}</div>
                                                                        </div>
                                                                    </div>
                                                                    <div ng-if="!data.hasSelection">
                                                                        No Selection
                                                                    </div>
                                                                    <div class="price">
                                                                        <div id="booking-info-indicator" class="row" style="color: #666;">
                                                                            <div class="col-xs-4 col-sm-4 col-md-4 text-center">
                                                                                <span class="fa fa-user"></span><br>{{ data.selectRoom.adults }}/{{ data.numberOfGuest.adults }}
                                                                            </div>
                                                                            <div class="col-xs-4 col-sm-4 col-md-4 text-center">
                                                                                <span class="fa fa-child"></span><br>{{ data.selectRoom.childs }}/{{ data.numberOfGuest.childrens }}
                                                                            </div>
                                                                            <div class="col-xs-4 col-sm-4 col-md-4 text-center">
                                                                                <span class="fa fa-tags"></span><br>{{ data.selectRoom.room }}/{{ data.numberOfGuest.rooms }}
                                                                            </div>
                                                                        </div>
                                                                        <em>Total for this booking</em>
                                                                        <span class="amount">
                                                                            <span class="fa fa-usd" style="background-color: transparent;"></span> {{ data.totalPrice|number:2 }}
                                                                            <a href="" ng-disabled="(data.rooms |filter: {isAdded: true}).length == 0" class="my-clear-selection pull-right" ng-click="clearAllSelection( (data.rooms |filter: {isAdded: true}) )">Clear All <span style="background-color: transparent;" class="fa fa-times"></span></a>
                                                                        </span>
                                                                    </div>
                                                                    <div class="form-submit">
                                                                        <div class="add-to-cart" style="float: right; width: 100%">
                                                                            <form action="<?=base_url()?>hotel/booker-form?id=<?=$_GET['id']?>" method="POST">
                                                                                <input type="hidden" name="token" value="<?=$token?>" >
                                                                                <div ng-repeat="room in data.rooms |filter: {isAdded: true}">
                                                                                    <input type="hidden" id="selectedRooms[{{ $index }}][id]" name="selectedRooms[{{ $index }}][id]" value="{{ room.resultid }}" >
                                                                                    <input type="hidden" id="selectedRooms[{{ $index }}][qty]" name="selectedRooms[{{ $index }}][qty]" value="{{ room.selectedQuantity }}" >
                                                                                </div>
                                                                                <button type="submit" ng-disabled="!data.selectRoom.readyToBook" style="width: 100%;">
                                                                                    <i class="awe-icon awe-icon-cart"></i>Book Now!
                                                                                </button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- COMBINATION TAB -->
                                                <div role="tabpanel" class="tab-pane" id="combinations">
                                                    <?php if(count($combinations) > 0) : ?>
                                                        <div class="row" style="margin:10px;">
                                                            <?php foreach ($combinations as $key => $combination) : ?>
                                                                <div class="col-md-3 panel panel-primary" style="padding:0px; margin: 5px">
                                                                    <div class="panel-heading"><?=strtr($combination['board'], array('AI'=>'All Inclusive', 'FB' => 'Full Board', 'HB' => 'Half Board', 'BB' => 'Bed & Breakfast', 'RO' => 'Room Only'))?></div>
                                                                    <div class="panel-body">
                                                                        <div style="font-size: 1.2em;">
                                                                            Good for <strong><?=$numberOfGuest['rooms']?> rooms</strong>, <strong><?=$numberOfGuest['adults']?> Adult/s</strong> and <strong><?=$numberOfGuest['childrens']?> Child/s</strong>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <!-- <h6 tooltip-placement="top" tooltip-class="my-tooltip" tooltip-append-to-body="true" uib-tooltip-template="'combination<?=$key?>.html'"> -->
                                                                            <?php if( $combination['calculation']['calculation']['gross_amount'] > $combination['calculation']['calculation']['due_amount'] ) :?>
                                                                                <h6 class="text-center" style="color: #AA3939; font-size: 1.2em; text-decoration: line-through">
                                                                                    <span class="fa fa-usd"></span> <?=number_format($combination['calculation']['calculation']['gross_amount'], 2, '.', '')?>
                                                                                </h6>
                                                                            <?php endif; ?>
                                                                            <h6>
                                                                                
                                                                                <span class="fa fa-usd"></span> <?=number_format($combination['calculation']['calculation']['due_amount'], 2, '.', '')?>
                                                                                <script type="text/ng-template" id="combination<?=$key?>.html">
                                                                                     <div class="row">
                                                                                         <div class="col-md-7 text-left"><strong>Gross Amount</strong>: </div>
                                                                                         <div class="col-md-5 text-left"><span class="fa fa-<?=($combination['calculation']['calculation']['currency'] != 'AUD')?strtolower($combination['calculation']['calculation']['currency']):'usd'?>"></span><?=number_format($combination['calculation']['calculation']['gross_amount'], 2, '.', '')?></div>
                                                                                         <div class="col-md-7 text-left"><strong>Commission</strong>:</div>
                                                                                         <div class="col-md-5 text-left"><span class="fa fa-<?=($combination['calculation']['calculation']['currency'] != 'AUD')?strtolower($combination['calculation']['calculation']['currency']):'usd'?>"></span><?=number_format($combination['calculation']['calculation']['commission'], 2, '.', '')?></div>
                                                                                         <div class="col-md-7 text-left"><strong>Nett Amount</strong>:</div>
                                                                                         <div class="col-md-5 text-left"><span class="fa fa-<?=($combination['calculation']['calculation']['currency'] != 'AUD')?strtolower($combination['calculation']['calculation']['currency']):'usd'?>"></span><?=number_format($combination['calculation']['calculation']['due_amount'], 2, '.', '')?></div>
                                                                                     </div>
                                                                                </script>
                                                                            </h6>
                                                                        </div>
                                                                        <form action="<?=base_url()?>hotel/booker-form?id=<?=$_GET['id']?>" method="POST">
                                                                            <input type="hidden" name="token" value="<?=$token?>">
                                                                            <?php foreach ($combination['content']['rooms'][0]['room'] as $roomkey => $room) : ?>
                                                                                <div>
                                                                                    <input type="hidden" id="selectedRooms[<?=$roomkey?>][id]" name="selectedRooms[<?=$roomkey?>][id]" value="<?=$room['resultid']?>">
                                                                                    <input type="hidden" id="selectedRooms[<?=$roomkey?>][qty]" name="selectedRooms[<?=$roomkey?>][qty]" value="<?=$room['count']?>">
                                                                                </div>
                                                                            <?php endforeach; ?>
                                                                            <button type="submit" class="btn btn-danger btn-block">BOOK NOW!</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    <?php else : ?>
                                                        THERE ARE NO COMBINATIONS!!!
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END OF TAB 1 -->
                                    <div id="tabs-2">
                                        <div class="row" style="margin: 0px;">
                                            <div class="col-md-offset-2 row" style="margin-right: 0px;">
                                                <?php $facilities      = (isset($hotel['facilities']))?$hotel['facilities']:array(); ?>
                                                <?php if ( ! is_null($facilities) && count($facilities) > 0 ) : ?>
                                                    <?php
                                                        $sliced          = array();
                                                        $facility_height = count($facilities) / 2;
                                                        array_push($sliced, array_slice($facilities, 0, $facility_height));
                                                        array_push($sliced, array_slice($facilities, $facility_height));

                                                        if( count($sliced[0]) <= count($sliced[1]) ) {
                                                            $temp = $sliced[0];
                                                            $sliced[0] = $sliced[1];
                                                            $sliced[1] = $temp;
                                                        }
                                                    ?>
                                                    <?php foreach ($sliced as $key => $value) : ?>
                                                        <div class="col col-md-5">
                                                            <div class="list-group">
                                                                <?php foreach ($value as $key => $facility) : ?>
                                                                    <a href="" class="list-group-item">
                                                                        <i class="glyphicon glyphicon-ok-circle" style="color: #4cae4c;"></i>
                                                                        &nbsp;
                                                                        <?=ucfirst($facility['title'])?>
                                                                    </a>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    THERE ARE NO FACILITIES!!!
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END OF TAB 2 -->
                                    <div id="tabs-3">
                                        <table class="good-to-know-table">
                                            <tbody>
                                                <tr>
                                                    <th>
                                                        <p>Check in</p>
                                                    </th>
                                                    <td>
                                                        <p>From 15:00 hours</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <p>Check out</p>
                                                    </th>
                                                    <td>
                                                        <p>Until 11:00 hours</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <p>Cancellation / prepayment</p>
                                                    </th>
                                                    <td>
                                                        <p>Cancellation and prepayment policies vary according to room type. Please check the room conditions when selecting your room above.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <p>Children and extra beds</p>
                                                    </th>
                                                    <td>
                                                        <p>The maximum number of children’s cots/cribs in a room is 1.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <p>Internet</p>
                                                    </th>
                                                    <td>
                                                        <p>free! WiFi is available in all areas and is free of charge.</p>
                                                        <p><span class="light">Free</span>children under 2 years stay free of charge when using existing beds.</p>
                                                        <p><span class="light">Free</span>children under 2 years stay free of charge when using existing beds.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <p>Pets</p>
                                                    </th>
                                                    <td>
                                                        <p>Pets are allowed. Charges may be applicable.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <p>Groups</p>
                                                    </th>
                                                    <td>
                                                        <p>When booking for more than 11 persons, different policies and additional supplements may apply.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <p>Accepted cards for payment</p>
                                                    </th>
                                                    <td>
                                                        <p><img src="<?php echo base_url(); ?>static/images/paypal2.png" alt=""></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END OF TAB 3 -->
                                    <div id="tabs-4">
                                        <div id="reviews">
                                            <div class="rating-info">
                                                <div class="average-rating-review good">
                                                    <span class="count">7.5</span>
                                                    <em>Average rating</em>
                                                    <span>Good</span>
                                                </div>
                                                <ul class="rating-review">
                                                    <li>
                                                        <em>Facility</em>
                                                        <span>7.5</span>
                                                    </li>
                                                    <li>
                                                        <em>Human</em>
                                                        <span>9.0</span>
                                                    </li>
                                                    <li>
                                                        <em>Service</em>
                                                        <span>9.5</span>
                                                    </li>
                                                    <li>
                                                        <em>Interesting</em>
                                                        <span>8.7</span>
                                                    </li>
                                                </ul>
                                                <a href="#" class="write-review">Write a review</a>
                                            </div>
                                            <div id="add_review">
                                                <h3 class="comment-reply-title">Add a review</h3>
                                                <form>
                                                    <div class="comment-form-author">
                                                        <label for="author">Name <span class="required">*</span></label>
                                                        <input id="author" type="text">
                                                    </div>
                                                    <div class="comment-form-email">
                                                        <label for="email">Email <span class="required">*</span></label>
                                                        <input id="email" type="text">
                                                    </div>
                                                    <div class="comment-form-rating">
                                                        <h4>Your Rating</h4>
                                                        <div class="comment-form-rating__content">
                                                            <div class="item facility">
                                                                <label>Facility</label>
                                                                <select class="awe-select">
                                                                    <option>5.0</option>
                                                                    <option>6.5</option>
                                                                    <option>7.5</option>
                                                                    <option>8.5</option>
                                                                    <option>9.0</option>
                                                                    <option>10</option>
                                                                </select>
                                                            </div>
                                                            <div class="item human">
                                                                <label>Human</label>
                                                                <select class="awe-select">
                                                                    <option>5.0</option>
                                                                    <option>6.5</option>
                                                                    <option>7.5</option>
                                                                    <option>8.5</option>
                                                                    <option>9.0</option>
                                                                    <option>10</option>
                                                                </select>
                                                            </div>
                                                            <div class="item service">
                                                                <label>Service</label>
                                                                <select class="awe-select">
                                                                    <option>5.0</option>
                                                                    <option>6.5</option>
                                                                    <option>7.5</option>
                                                                    <option>8.5</option>
                                                                    <option>9.0</option>
                                                                    <option>10</option>
                                                                </select>
                                                            </div>
                                                            <div class="item interesting">
                                                                <label>Interesting</label>
                                                                <select class="awe-select">
                                                                    <option>5.0</option>
                                                                    <option>6.5</option>
                                                                    <option>7.5</option>
                                                                    <option>8.5</option>
                                                                    <option>9.0</option>
                                                                    <option>10</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="comment-form-comment">
                                                        <label for="comment">Your Review</label>
                                                        <textarea id="comment"></textarea>
                                                    </div>
                                                    <div class="form-submit">
                                                        <input type="submit" class="submit" value="Submit">
                                                    </div>
                                                </form>
                                            </div>
                                            <div id="comments">
                                                <ol class="commentlist">
                                                    <li>
                                                        <div class="comment-box">
                                                            <div class="avatar">
                                                                <img src="<?php echo base_url(); ?>static/images/img/demo-thumb.jpg" alt="">
                                                            </div>
                                                            <div class="comment-body">
                                                                <p class="meta">
                                                                    <strong>Nguyen Gallahendahry</strong>
                                                                    <span class="time">December 10, 2012</span>
                                                                </p>
                                                                <div class="description">
                                                                    <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                                </div>
    
                                                                <div class="rating-info">
                                                                    <div class="average-rating-review good">
                                                                        <span class="count">7.5</span>
                                                                        <em>Average rating</em>
                                                                        <span>Good</span>
                                                                    </div>
                                                                    <ul class="rating-review">
                                                                        <li>
                                                                            <em>Facility</em>
                                                                            <span>7.5</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Human</em>
                                                                            <span>9.0</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Service</em>
                                                                            <span>9.5</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Interesting</em>
                                                                            <span>8.7</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="comment-box">
                                                            <div class="avatar">
                                                                <img src="<?php echo base_url(); ?>static/images/img/demo-thumb.jpg" alt="">
                                                            </div>
                                                            <div class="comment-body">
                                                                <p class="meta">
                                                                    <strong>James Bond not 007</strong>
                                                                    <span class="time">December 10, 2012</span>
                                                                </p>
                                                                <div class="description">
                                                                    <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                                </div>
    
                                                                <div class="rating-info">
                                                                    <div class="average-rating-review good">
                                                                        <span class="count">7.5</span>
                                                                        <em>Average rating</em>
                                                                        <span>Good</span>
                                                                    </div>
                                                                    <ul class="rating-review">
                                                                        <li>
                                                                            <em>Facility</em>
                                                                            <span>7.5</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Human</em>
                                                                            <span>9.0</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Service</em>
                                                                            <span>9.5</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Interesting</em>
                                                                            <span>8.7</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="comment-box">
                                                            <div class="avatar">
                                                                <img src="<?php echo base_url(); ?>static/images/img/demo-thumb.jpg" alt="">
                                                            </div>
                                                            <div class="comment-body">
                                                                <p class="meta">
                                                                    <strong>Bratt not Pitt</strong>
                                                                    <span class="time">December 10, 2012</span>
                                                                </p>
                                                                <div class="description">
                                                                    <p>Takes me back to my youth. I love the design of this soda machine. A bit pricy though..!</p>
                                                                </div>
    
                                                                <div class="rating-info">
                                                                    <div class="average-rating-review fine">
                                                                        <span class="count">5.0</span>
                                                                        <em>Average rating</em>
                                                                        <span>Fine</span>
                                                                    </div>
                                                                    <ul class="rating-review">
                                                                        <li>
                                                                            <em>Facility</em>
                                                                            <span>7.5</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Human</em>
                                                                            <span>9.0</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Service</em>
                                                                            <span>9.5</span>
                                                                        </li>
                                                                        <li>
                                                                            <em>Interestings</em>
                                                                            <span>8.7</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END OF TAB 4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>