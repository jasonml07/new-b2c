<div class="container" >
<form name = "saveFormType" id = "saveFormType">
  <fieldset>
      <div class="col-md-12 panel" style="margin-bottom: 7%; margin-top: 20px; border-radius: 20px;">
          <div id="legend" class="" style="margin-bottom: 30px; background-color-opacity: 0.50!important;">
                <legend class="" style="margin-top: 20px; font-weight: bold; margin-bottom: 30px; padding-left: 20px; padding-bottom: 10px;  color: #666; font-size: 30px!important;">Register</legend>
                <span class = "" id = "fields"></span>
          </div>
                <div class="col-md-3 panel-themes" style=" margin-bottom: 100px; margin-left: 20px;margin-top: 1%; border: 2px solid #fff; border-radius: 10px; border: 2px solid #f2f2f2;"> 
                   <div style=" margin-right: 30px; margin-top: 20px; margin-left: 20px;">      
                          <div class="control-group" style="color: #fff; margin-top: 30px;">
                              <label class="control-label"  for="username">Username</label>
                                <div class="controls">
                                    <input type="text" id="username" name="username" placeholder="" class="form-control" required="required" >
                                    <p id ="username_warning"></p>
                                </div>
                          </div>
                          <div class="control-group" style="color: #fff; margin-top: 20px;">
                               <label class="control-label" for="email">E-mail</label>
                                  <div class="controls">
                                    <input type="email" id="email" name="email" placeholder="" class="form-control" required="required" >
                                    <p  id = "email_warning"></p>
                                  </div>
                          </div>
                           <div class="control-group" style="color: #fff; margin-top: 20px;">
                                <label class="control-label" for="password">Password</label>
                                    <div class="controls">
                                          <input type="password" id="password" name="password" placeholder="" class="form-control" required="required">
                                          <p id = "password_warning"></p>
                                    </div>
                           </div>
                          <div class="control-group" style="color: #fff; margin-top: 20px;">   
                              <label class="control-label"  for="password_confirm">Password (Confirm)</label>
                                  <div class="controls">
                                        <input type="password" id="password_confirm" name="password_confirm" placeholder="" class="form-control" required="required">
                                       <p  id = "confirm_warning"></p>
                                  </div>
                          </div>
                <div class="control-group" style="margin-top: 30px; margin-bottom: 30px;">
                          <!-- Button -->
                    <div class="controls">
                            <button type ="submit" class="btn btn-success" id = "regButton">Register</button>
                    </div>
                </div>
                </div>
          </div>
       </div>
  </fieldset>
</form>
</div>

<!-- <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="border-radius:10px;top:100px;">

        <div class="modal-dialog" role="document" style="border-radius: 10px !important; background-color: #f2f2f2;">
            <div class="modal-content" style="background-color: #fff !important; border-radius: 10px;" >
                <div class="modal-header" style="background-color:#1D2629 !important;  padding: 30px; border-radius: 10px;" >
                    <button type="button" class="close" onClick = "reload()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" style="text-align:center !important;padding: 20px; color: #666;">
                    <h3>Credentials Successfully Created!<h3>
                </div>

                <div class="modal-footer" style="background-color: #1D2629!important; border-radius: 10px;">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #0077b3!important; color: #fff;" onClick = "reload()">OK</button>
                </div>
            </div>
        </div>
 </div>  -->