<script type="text/javascript">
    // console.log(<?=json_encode($booking_info)?>);
</script>


        <section class="checkout-section-demo" style="padding-left:0 !important;" ng-app="PaymentApplication" ng-controller="PaymentController" ng-init="data.hotelId = <?=$_GET['id'];?>; data.isPayable = <?=($this->session->userdata('paymentIsRequired'))?'true':'false';?>; data.isTest = <?=(PAYWAY_IS_TEST)?'true':'false'?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?=base_url()?>hotel/searchResult" class="btn btn-primary">
                            <i class="glyphicon glyphicon-search"></i>
                            &nbsp;Back to Search Page
                        </a>
                    </div>
                    <div class="col-md-12">
                        <div class="checkout-page__top">
                            <div class="title">
                                <h1 class="text-uppercase" style="color: #1e1e1f; margin-left: 40px;">Assign Guest/Customer Information</h1>
                            </div>
                        </div>
                    </div>
                    <?php if( isset($request) && count($request['rooms']) > 0 ) : ?>
                    <div class="col-md-8" id="waiters">
                        <!-- Main Errors -->
                        <div ng-if="data.errors.main.length > 0" class="alert alert-danger" ng-repeat="error in data.errors.main">
                            <p>{{ error }}</p>
                        </div>
                        <form novalidate id="paxForm" name="paxForm">
                            <input type="hidden" name="requiredToPay" value="0">
                            <input type="hidden" id="form-active-payment" name="activePayment" value="">
                            
                            <div class="checkout-page__content">
                                <div class="customer-content" style="background-color: #fff !important; border: 3px solid #f2f2f2; border-radius: 3px; ">
                                        <?php foreach ($request['rooms'] as $key => $room) : ?>
                                        <input type="hidden" id="rooms[<?=$key?>][id]" name="rooms[<?=$key?>][id]" value="<?=$result_ids[$key]['id']?>">
                                        <div class="woocommerce-billing-fields" <?=($key == 0)?'style="margin-top: 0px;"':''; ?> >
                                            <h3 class="header" >Room <?=$key+1?></h3>
                                            <!-- START OF ADULT -->
                                            <div class="col-sm-6 col-md-6" style="padding:1 !important;">
                                                <?php for ($i=0; $i < intval($room['adult']); $i++) : ?>
                                                 <h4 class="pax-header-adult" style="margin-top: <?=($i == 0)?'0px':'70px'; ?>;">Adult <?=$i+1?></h4>
                                                <div class="col-sm-3 col-md-3"  style="padding:0 !important;">
                                                    <label for="rooms[<?=$key?>][adult][<?=$i?>][title]" style="padding-top: 10px !important;color: #1e1e1f;">Title</label>
                                                        <select  name="rooms[<?=$key?>][adult][<?=$i?>][title]"
                                                            id="rooms[<?=$key?>][adult][<?=$i?>][title]"
                                                            class="adjust-select"
                                                            style="width:98% !important;background-color:#f2f2f2; color: #1e1e1f;"
                                                            <?=($i == 0)?'ng-model="data.voucherOwner.title"':''?>>
                                                            <option value="">-- Title --</option>
                                                            <option <?=(TEST_INPUTS)?'selected=""': ''?> value="Mr">Mr</option>
                                                            <option value="Ms">Ms</option>
                                                            <option value="Mrs">Mrs</option>
                                                        </select>
                                                </div>
                                               <div class="col-sm-5 col-md-5" style="padding:10 !important;">
                                                    <label for="rooms[<?=$key?>][adult][<?=$i?>][firstname]" style="padding-top: 10px !important;color: #1e1e1f;">First name</label>
                                                    <input type="text"
                                                        name="rooms[<?=$key?>][adult][<?=$i?>][firstname]"
                                                        id="rooms[<?=$key?>][adult][<?=$i?>][firstname]"
                                                        style="width:98% !important;background-color:#fff;"
                                                        class="adjust-input-text"
                                                        <?=($i == 0)?'ng-model="data.voucherOwner.firstname"':''?>
                                                        data-value="name">
                                                </div>
                                                <div class="col-sm-4 col-md-4" style="padding:0 !important;">
                                                    <label for="rooms[<?=$key?>][adult][<?=$i?>][lastname]" style="padding-top: 10px !important;color: #1e1e1f;">Last name</label>
                                                    <input type="text"
                                                        name="rooms[<?=$key?>][adult][<?=$i?>][lastname]"
                                                        id="rooms[<?=$key?>][adult][<?=$i?>][lastname]"
                                                        style="width:98% !important;background-color:#fff; border-radius: 2px;"
                                                        class="adjust-input-text"
                                                        <?=($i == 0)?'ng-model="data.voucherOwner.lastname"':''?>
                                                        data-value="name">
                                                </div>
                                                <?php endfor; ?>
                                            </div>
                                            <!-- END OF ADULTS -->
                                            <?php if ( intval($room['child']) > 0 ) : ?>
                                            <div class="col-sm-6 col-md-6" style="padding: 4px !important;">
                                                <?php foreach ($room['childAges'] as $ageKey => $age) : ?>
                                               <h4 class="pax-header-child" style="margin-top: <?=($ageKey==0)?'-5px':'70px'?>;">Child <?=$ageKey+1?></h4>
                                               <input type="hidden" class="form-control" id="rooms[<?=$key?>][child][<?=$ageKey?>][age]" name="rooms[<?=$key?>][child][<?=$ageKey?>][age]" placeholder="Child Age" value="<?=$age?>">
                                               <div class="col-sm-6 col-md-6" style="padding:10 !important;">
                                                    <label for="rooms[<?=$key?>][child][<?=$ageKey?>][firstname]" style="padding-top: 10px !important;color: #1e1e1f;">First name</label>
                                                    <input type="text"
                                                        id="rooms[<?=$key?>][child][<?=$ageKey?>][firstname]"
                                                        name="rooms[<?=$key?>][child][<?=$ageKey?>][firstname]"
                                                        style="width:98% !important;background-color:#fff;"
                                                        class="adjust-input-text"
                                                        data-value="name">
                                                </div>
                                                <div class="col-sm-6 col-md-6" style="padding:0 !important;">
                                                    <label for="rooms[<?=$key?>][child][<?=$ageKey?>][lastname]" style="padding-top:10px !important; color: #1e1e1f;">Last name</label>
                                                    <input type="text"
                                                        id="rooms[<?=$key?>][child][<?=$ageKey?>][lastname]"
                                                        name="rooms[<?=$key?>][child][<?=$ageKey?>][lastname]"
                                                        style="width:98% !important;background-color:#fff;"
                                                        class="adjust-input-text"
                                                        data-value="name">
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <?php endif; ?>
                                            <!-- END OF CHILDRENS -->
                                        </div>
                                        <?php endforeach; ?>
                                    <div class="woocommerce-billing-fields">
                                        <div class="col-sm-12 col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;">
                                        <h3 class="header">Voucher Information</h3>
                                        <!-- ERRORS HERE -->
                                            <div ng-if="data.errors.voucher.length > 0" class="alert alert-danger">
                                                <ul>
                                                    <li ng-repeat="error in data.errors.voucher">{{ error }}</li>
                                                </ul>
                                            </div>
                                          <div class="col-sm-2 col-md-2"  style="padding:0 !important;">
                                                <label style="padding-top: 10px !important;color: #1e1e1f;font-size: 14px; padding-left: 5px; margin-left: 20px;">Title</label>
                                                <select style="width:70% !important;background-color:#EEE;color: #1e1e1f; padding-left: 10px; margin-left: 20px;"
                                                    class="adjust-select" 
                                                    readonly=true
                                                    ng-model="data.voucherOwner.title"
                                                    id="voucher[title]" name="voucher[title]">
                                                    <option value="">-- Title --</option>
                                                    <option <?=(TEST_INPUTS)?'selected=""': ''?> value="Mr">Mr</option>
                                                    <option>Ms</option>
                                                    <option>Mrs</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-5 col-md-5" style="padding: 8px !important;">
                                                <label style="padding-top: 0px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >First name</label>
                                                <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                    class="adjust-input-text" 
                                                    id="voucher[firstname]" name="voucher[firstname]" data-value="name"
                                                    readonly=true
                                                    ng-model="data.voucherOwner.firstname">
                                            </div>
                                             <div class="col-sm-5 col-md-5" style="padding:8px !important;">
                                                <label style="padding-top: 0px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Last name</label>
                                                <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                    class="adjust-input-text" 
                                                    id="voucher[lastname]" name="voucher[lastname]" data-value="name"
                                                    readonly=true
                                                    ng-model="data.voucherOwner.lastname">
                                            </div>
                                             <div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10" style="padding:0 !important;">
                                                <label style="padding-top: 0px !important;color: #1e1e1f; padding-left: 50px;font-size: 14px;">Email Address</label>
                                                <input type="email" style="width:87% !important;background-color:#fff;margin-left: 50px;"
                                                    class="adjust-input-text"
                                                    id="voucher[email]" name="voucher[email]" data-value="email">
                                            </div>
                                            <!-- value="{{ fakerEmail() }}" -->
                                            <div class="col-sm-offset-2 col-sm-5 col-md-offset-2 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Phone Number</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text" 
                                                    id="voucher[phone]" name="voucher[phone]" data-value="phone">
                                            </div>
                                            <!-- <div class="col-sm-5 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Phone Number</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text" 
                                                    id="voucher[phone]" name="voucher[phone]" data-value="phone">
                                            </div> -->
                                            <!-- <div class="col-md-offset-2 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Address</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text" 
                                                    id="voucher[address]" name="voucher[address]" data-value="address">
                                            </div>
                                            <div class="col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">City</label>
                                                <input type="text" style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    class="adjust-input-text" 
                                                    id="voucher[city]" name="voucher[city]" data-value="city">
                                            </div>
                                            <div class="col-md-offset-2 col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Postal Code</label>
                                                <input type="number"
                                                    style="width:83% !important;background-color:#fff;margin-left: 33px;"
                                                    id="voucher[postCode]"
                                                    name="voucher[postCode]"
                                                    class="adjust-input-text"
                                                    value="<?=(TEST_INPUTS)?'9000': ''?>">
                                            </div>
                                            <div class="col-md-5" style="padding:10 !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Country Code</label>
                                                <select name="voucher[countryCode]"
                                                    id="voucher[countryCode]"
                                                    class="adjust-select" 
                                                    style="width:83% !important;background-color:#fff;margin-left: 33px;">
                                                    <option selected="" value="">Select Country *</option>
                                                    <?php foreach ($country_codes as $key => $value) : ?>
                                                        <option value="<?=$key?>"><?=$value?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div> -->
                                       </div>
                                    </div>
                                    <!-- END OF ROOM CONTAINER -->
                                     <div class="woocommerce-billing-fields">
                                        <div class="col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;">
                                            <h3 class="header">Special Requirements (Optional)</h3>
                                                 <label style="margin-top: 30px; color: #1e1e1f; margin-left: 30px;">Please write your requests in English</label>
                                              <textarea class="special-offer" id="request" name="request">No Smoking</textarea>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- If the payment is not required..
                        There will be an optional payment
                        So the logic should be when the user wants to pay the submit the form from handofUrl
                        Else submit the form without payments -->

                        <!-- <form ng-submit="submitPaymentForm(paymentForm, <?=($this->session->userdata('bookingId'))?'true':'false'?>)" novalidate="" id="paymentForm" name="paymentForm" action="<?=$handOfUrl?>" target="my_iframe" method="POST"> -->
                        <!-- <form novalidate="" id="paymentForm" name="paymentForm" action="<?=htmlentities($handOffUrl)?>" target="my_iframe" method="POST"> -->
                        <!-- action="<?=base_url()?>dashboard/payWayCallBackTest2" -->
                        <form novalidate="" id="paymentForm" name="paymentForm" method="POST">
                            <input type="hidden" name="token" value="">
                            <?php if( PAYWAY_IS_TEST ) : ?>
                                <!-- <input type="hidden" name="amount" value="<?=number_format($booking_info['response'][0]['content']['calculation']['calculation']['due_amount'], 2, '.', '')?>"> -->
                            <?php endif; ?>
                            <input type="hidden" name="biller_code" value="<?=htmlentities(PAYWAY_BILLER_CODE)?>">
                            <input type="hidden" name="action" value="MakePayment">
                            <!-- PAYMENTS -->
                            <!-- This item only shows when the payment is required of the user wants to pay
                            if the user wants to pay, the button will appear
                            but when the payment is indeed requred the payment form will appear -->
                            <!-- Check first for the session if is required -->

                            <div class="checkout-page__content">
                                <div class="customer-content" style="background-color: #fff !important; border: 3px solid #f2f2f2; border-radius: 3px; ">
                                    <!-- POLICY -->
                                    <div class="woocommerce-billing-fields" style="margin-top: 0px;">
                                        <h3 class="header">Cancellation Policy</h3>
                                        <div>
                                            <div class="col-md-12" >
                                                <?php
                                                    $policies = $booking_info['response'][0]['content']['byRoomPolicies'];
                                                ?>
                                                <!-- <script type="text/javascript">console.log(<?=json_encode($policies)?>)</script> -->
                                                <?php foreach ($policies as $key => $policy) :?>
                                                    <label  style="padding: 8px !important; margin-top: 20px; padding-bottom: 20px;" for="payment_method_bacs" ><?=$policy['name']?></label>
                                                    <div class="payment_box payment_method_bacs">
                                                        <?php if( count($policy['cancellation']) > 0 ) :?>
                                                        <ul>
                                                            <?php foreach ($policy['cancellation'] as $keys => $value) :?>
                                                                <li>From <strong><?=$value['startTime']?></strong> <?=(!is_null($value['endTimeFormatted']))?'up to ' . $value['endTimeFormatted']:''?> this item is <strong><?=$value['type']?></strong></li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                        <?php else : ?>
                                                            No policy
                                                        <?php endif; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- this button only shows when payment is not neccessary -->
                                    <div>
                                        <button ng-if="!data.isPayable" id="payable-button" ng-click="data.isPayable = true;" type="button" class="btn btn-primary btn-block">Do you want to pay?</button>
                                    </div>
                                    <div  ng-if="data.isPayable" id="customer-payment-form-wrapper" class="woocommerce-billing-fields">
                                        <h3 class="header">
                                            Credit Card Payment&nbsp;
                                                <span ng-if="data.isPaymentApproved == 'approved'" class="fa fa-check-circle" style="color: #3c763d"></span>
                                                <span ng-if="data.isPaymentApproved == 'declined'" class="fa fa-times-circle" style="color: #a94442"></span>
                                            <?php if ( $this->session->userdata('paymentIsRequired') === FALSE ) : ?>
                                                <button ng-click="data.isPayable = false;" class="btn btn-primary btn-sm pull-right" id="customer-close-button" type="button">
                                                    <span class="fa fa-close"></span>
                                                </button>
                                            <?php endif; ?>
                                        </h3>
                                        <div ng-if="data.errors.payment.length > 0" class="alert alert-danger">
                                            <ul>
                                                <li ng-repeat="error in data.errors.payment">{{ error }}</li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-12 col-md-12" style="padding-left: 0px !important; font-size: 12px; margin-top: 10px;  ">
                                            <div class="col-sm-6 col-md-6" style="padding: 8px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px; font-size: 14px;" >Cardholder Name</label>
                                                    <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                        class="adjust-input-text" 
                                                        id="nm_card_holder" name="nm_card_holder"
                                                        ng-model="data.payment.nm_card_holder">
                                                <label style="padding-top: 25px !important;color: #1e1e1f;padding-left: 40px;font-size: 14px;">Card Verification Number</label>
                                                    <input type="text" style="width:80% !important;background-color:#fff;margin-left: 40px;"
                                                        class="adjust-input-text" 
                                                        id="no_cvn" name="no_cvn"
                                                        ng-model="data.payment.no_cvn">
                                            </div>
                                            <?php $startDate = date('y', strtotime('now')); ?>
                                             <div class="col-sm-6 col-md-6" style="padding:8px !important;">
                                                <label style="padding-top: 20px !important;color: #1e1e1f; padding-left: 40px;font-size: 14px;">Card Number</label>
                                                    <input type="text" style="width:78% !important;background-color:#fff; margin-left: 40px;"
                                                        class="adjust-input-text" 
                                                        id="no_credit_card" name="no_credit_card"
                                                        ng-model="data.payment.no_credit_card">
                                                <div style="padding-left: 40px;">
                                                    <label style="padding-top: 25px ;color: #1e1e1f; font-size: 14px;" class="col">Expiry Date</label>
                                                    <br>
                                                    <select name="dt_expiry_month" id="" class="adjust-select" ng-model="data.payment.dt_expiry_month">
                                                        <option value="">-- Month --</option>
                                                        <?php for($m=1; $m<=12; ++$m): ?>
                                                            <option value="<?=($m < 10)?'0' . $m: $m?>"><?=date('F', mktime(0, 0, 0, $m, 1))?></option>
                                                        <?php endfor; ?>
                                                    </select>
                                                    <select name="dt_expiry_year" id="" class="adjust-select" ng-model="data.payment.dt_expiry_year">
                                                        <option value="">-- Year --</option>
                                                        <?php foreach (range(intval($startDate), intval($startDate) + 10) as $key => $value) : ?>
                                                            <option value="<?=$value?>"><?=$value?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                       </div>
                                    </div>
                                    <div id="payment" style="margin-top: 0px;">
                                        <div>
                                            <hr>
                                            <h3 id="ship-to-different-address">
                                                <input id="ship-to-different-address-checkbox" type="checkbox">
                                                <!-- name="policy_read" -->
                                                <label style="color: #0091ea !important; margin-top: 40px;font-size: 14px;" for="ship-to-different-address-checkbox">I have read the cancellation policy</label>
                                            </h3>
                                        </div>
                                        <div class="form-row place-order">
                                            <!-- Check first if the booking id session does exist then warned the user that this item will be added to the existing book -->
                                            <?php if( IS_BOOKING ) : ?>
                                            <input type="button" ng-click="submitForm(<?=($this->session->userdata('bookingId'))?'true':'false'?>)" class="button alt" id="place_order" value="Book Now">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php endif; ?>
                    <div class="col-md-4">
                        <div id="booking-information" class="detail-sidebar">
                            <div class="booking-info">
                                <h3>Booking Info</h3>
                                <div class="row" style="color: #000;">
                                    <?php $bookingPrice = 0; ?>
                                    <?php foreach ($booking_info['response'][0]['content']['booking-options'][0]['content']['booking-option'] as $key => $bookingOption) : ?>
                                        <?php
                                            $itemPrice = ( isset($bookingOption['content']['room'][0]['price'][0]) )? $bookingOption['content']['room'][0]['price'][0]: $bookingOption['content']['room'][0]['content']['price'][0];
                                            $itemName  = ( isset($bookingOption['content']['room'][0]['name'][0]) )? $bookingOption['content']['room'][0]['name'][0]: $bookingOption['content']['room'][0]['content']['name'][0];
                                            $itemBoard = ( isset($bookingOption['content']['room'][0]['board'][0]['content']) )? $bookingOption['content']['room'][0]['board'][0]['content']: $bookingOption['content']['room'][0]['content']['board'][0]['content'];

                                            $itemKey = array_search($bookingOption['token'], array_map(function ($element) { return $element['id']; }, $result_ids));
                                            if( is_integer($itemKey) ) {
                                                $itemPrice['calculation']['calculation']['due_amount'] = (float)$itemPrice['calculation']['calculation']['due_amount'] * (int)$result_ids[ $itemKey ]['qty'];
                                            }
                                            $bookingPrice += (float)$itemPrice['calculation']['calculation']['due_amount'];
                                        ?>
                                        <div class="col-md-12">
                                            <div class="col-md-7 col-sm-7"><?=$itemName?><br><em style="font-size: .7em;">(<?=$itemBoard?>)</em></div>
                                            <div class="col-md-5 col-sm-5"><span class="fa fa-usd" style="background-color: transparent;"></span>&nbsp;<?=number_format($itemPrice['calculation']['calculation']['due_amount'], 2, '.', ',')?></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="price">
                                    <em>Total for this booking</em>
                                    <span class="amount"><span class="fa fa-usd" style="background-color: transparent;"></span> <?=number_format($bookingPrice, 2, '.', ',')?></span>
                                    <div class="cart-added">
                                        <i class="awe-icon awe-icon-check"></i>
                                        Added
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="payment-modal"
                class="modal fade"
                tabindex="-1"
                role="dialog"
                aria-labelledby="paymentModal"
                data-backdrop="static">
                <div class="modal-dialog modal-md" style="top: 50px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 style="color: #3c763d" class="modal-title text-center" id="paymentModal">Evaluating <span class="fa fa-refresh"></span></h4>
                        </div>
                        <div class="modal-body">
                            <iframe id="my_iframe" style="width: 100%; height: 300px;" name="my_iframe" src="" frameborder="0"></iframe>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" ng-click="closePaymentModal()">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal"
                class="modal fade"
                tabindex="-1"
                role="dialog"
                aria-labelledby="successModal"
                data-backdrop="static">
                <div class="modal-dialog modal-md" style="top: 50px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 style="color: #3c763d" class="modal-title text-center" id="successModal">Success <span class="fa fa-check-circle"></span></h4>
                        </div>
                        <div class="modal-body">
                            Thank you for booking,<br>
                            Your booking details was been sent to your email.
                        </div>
                        <div class="modal-footer">
                            <!-- <a ng-if="data.bookingId" ng-href="{{ '<?=base_url()?>dashboard/bookingDetails/' + data.bookingId }}" class="btn btn-warning">View</a> -->
                            <a href="<?=base_url()?>hotel/searchResult" class="btn btn-danger"><span class="fa fa-search"></span> Return to search result.</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
