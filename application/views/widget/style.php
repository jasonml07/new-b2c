<?php 
$color = '#0091EA';
if( count($_GET) > 0 ) {
	// Initialize data
	if( isset($_GET['version']) ) {
		switch ($_GET['version']) {
			case '1':
				$color = '#AA3939';
				break;
			
			case '2':
				$color = '#343434';
				break;
			
			case '3':
				$color = '#8A0651';
				break;
		}
	}
}
?>

@media screen and ( max-width: 1090px ) {
	.hotel-destination {
		width: 96% !important;
	}
/*	div.form-childs,
	div.flight-class {
	    width: 98% !important;
	}*/
	/*div.other-adults {
	    margin-left: 15px;
	}*/
	.find-hotel {
		width: 100% !important;
	}
	/*form#flights div.widget-adjust-padding {
		padding-right: 6px !important;
	}*/
}

@media screen and (max-width: 540px) {
	div#main-wrapper {
		min-width: 0px !important;
	}
	.bootstrap-iso div.tab-content {
		min-width: 0px !important;
	}
/*	div.form-group.has-feedback {
		width: 93% !important;
	}
	div.form-group.col-md-3 {
		width: 97% !important;
	}*/
}

@media screen and ( max-width: 361px ) {
	li#title-tab {
		width: 50% !important;
	}
}

@media screen and ( min-width: 361px ) {
	li#title-tab {
		width: 55% !important;
	}
}

@media screen and ( max-width: 1025px ) {
	div.form-childs {
		width: 33.33333333% !important;
	}
	div.find-hotel {
		width: 31% !important;
	}
}

@media screen and ( max-width: 414px ) {
	 .bootstrap-iso div.tab-content {
		height     : 260px !important;
		min-height : 260px !important;
	}
}

@media screen and ( max-width: 344px ) {
	 .bootstrap-iso div.tab-content {
		height     : 240px !important;
		min-height : 240px !important;
	}
}

@media screen and ( max-width: 320px ) {
	li#title-tab {
		width: 100% !important;
	}
	div.widget-flight-type {
		display: block !important;
	}
	div.widget-flight-type div.form-group {
		width: 100% !important;
		float: none !important;
	}
	div.widget-flight-type .text-center {
		text-align: left !important;
	}
}

@media screen and ( max-width: 800px ) {
	/* .bootstrap-iso div.tab-content {
		height     : 400px !important;
		min-height : 400px !important;
	} */
	form#flights div.widget-adjust-padding {
		padding-right: 15px !important;
	}
	div.find-hotel {
		width: 100% !important;
	}
	div.child-ages div {
		margin-bottom: 5px;
	}
	div.widget-flight-type {
		display: -webkit-box;
		display: box;
	}
	div.widget-flight-type div.form-group {
		width: 33%;
		float: left;
	}
}

.container {
	padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}

.bootstrap-iso .find-hotel {
	width: 31%;
}
.bootstrap-iso li#title-tab {
	width: 66%;
}
.bootstrap-iso i.fa-calendar,
.bootstrap-iso i.fa-map-marker {
	color    : #db1f1a;
	top      : 23px !important;
	right    : 15px;
	font-size: 1.5em;
	z-index  : 1;
}

.bootstrap-iso .hotel-destination i.form-control-feedback {
	right: 0px;
}

.bootstrap-iso div#main-wrapper {
	min-width: 540px;
}
.bootstrap-iso div.tab-content {
	overflow-x : hidden;
	overflow-y : auto;
	white-space: nowrap;
	padding    : 10px;
	border-left: 1px solid;
	border-top : none;
	height     : 286px;
	min-height : 286px;
	min-width  : 540px;
	/*margin-top : 25px;*/

	border-left  : 1px solid <?=$color?>;
	border-right : 1px solid <?=$color?>;
	border-bottom: 1px solid <?=$color?>;
}
.bootstrap-iso ul.nav.nav-tabs {
	background-color: <?=$color?>;
	border: none;
	border-bottom: 1px solid <?=$color?>;
}
.bootstrap-iso ul.nav.nav-tabs > li.active {
	/*border-top: 1px solid <?=$color?>;*/
}

.bootstrap-iso ul.nav.nav-tabs > li.active > a {
	color: <?=$color?>;
}
.bootstrap-iso ul.nav.nav-tabs > li > a {
	color:  #FFF;
}
.bootstrap-iso ul.nav.nav-tabs > li > a {
	border       : 1px solid #FFF;
	border-top   : 1px solid <?=$color?>;
	border-left  : none;
	border-bottom: 1px solid <?=$color?>;
	border-radius: 0;
	margin-right : 0px;
	padding-left : 20px;
	padding-right: 20px;
}
.bootstrap-iso ul.nav.nav-tabs > li > a:hover {
	outline         : none;
	background-color: <?=$color?>;
	color           : #FFF;
}
.bootstrap-iso ul.nav.nav-tabs > li.active > a:hover {
	outline         : none;
	background-color: #FFF;
	color           : <?=$color?>;
}
.bootstrap-iso ul.nav.nav-tabs > li > a {
	border-top   : 1px solid <?=$color?>;
	border-bottom: 1px solid <?=$color?>;
	padding      : 5px 20px 5px 20px;
	font-size    : 1.5em;
}
/*ul.nav.nav-tabs > li:first-child > a { border-top-left-radius: 5px; }*/
.bootstrap-iso ul.nav.nav-tabs > li:first-child > a:focus, .bootstrap-iso ul.nav.nav-tabs > li > a:focus,
.bootstrap-iso ul.nav.nav-tabs > li:first-child > a:visited, .bootstrap-iso ul.nav.nav-tabs > li > a:visited,
.bootstrap-iso ul.nav.nav-tabs > li:first-child > a:hover, .bootstrap-iso ul.nav.nav-tabs > li > a:hover,
.bootstrap-iso ul.nav.nav-tabs > li:first-child > a:active {
	border-top   : 1px solid <?=$color?>;
	border-left  : 0px !important;
	border-top   : 1px solid <?=$color?>;
	border-bottom: 1px solid <?=$color?>;
}
.bootstrap-iso ul.nav.nav-tabs > li:first-child { border-left: 1px solid <?=$color?>; /* border-top-left-radius: 5px; */ }
/*ul.nav.nav-tabs > li:first-child > a:hover { border-top-left-radius: 5px; }*/
.bootstrap-iso ul.nav-tabs>li { margin-top: 0px; }

.bootstrap-iso .form-control {
	border-radius: 2px;
	
}
.bootstrap-iso .form-group {
	margin-bottom: 5px !important;
}
.bootstrap-iso table.ui-datepicker-calendar > tbody > tr > td > a.ui-state-hover,
.bootstrap-iso td.ui-datepicker-current-day > a.ui-state-active {
	background-color: <?=$color?> !important;
	border-color    : <?=$color?> !important;
	color           : #FFF;
}
.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
	color     : #333;
	border    : 1px solid #c5c5c5;
	background: none;
}
table.ui-datepicker-calendar > tbody > tr > td > a,
table.ui-datepicker-calendar > tbody > tr > td > span {
	text-align    : center;
	padding-left  : 7px;
	padding-right : 7px;
	padding-top   : 5px;
	padding-bottom: 5px;
}
table.ui-datepicker-calendar > tbody > tr > td > a {
	background-color: #DDD !important;
}

<?php if( isset($_GET['theme']) ) :?>
<?php switch ($_GET['theme']) {
	case 'ifly':
		# code...
		?>

		.bootstrap-iso i.fa-calendar,
		.bootstrap-iso i.fa-map-marker {
			color: #db1f1a !important;
			top: 23px;
			right: 15px;
			font-size: 1.5em;
			z-index: 1;
		}
		.bootstrap-iso div#main-wrapper {
			/* border-top-left-radius: 5px;
			border-top-right-radius: 5px; */
			padding: 0px;
			background: #07365d; /* Old browsers */
			background: -moz-radial-gradient(center, ellipse cover, #2991c2  0%, #07365d  100%); /* FF3.6+ */
			background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#04a0dd), color-stop(100%,#232589)); /* Chrome,Safari4+ */
			background: -webkit-radial-gradient(center, ellipse cover, #2991c2  0%,#07365d  100%); /* Chrome10+,Safari5.1+ */
			background: -o-radial-gradient(center, ellipse cover, #2991c2  0%,#07365d  100%); /* Opera 12+ */
		}
		.bootstrap-iso ul.nav-tabs {
			background-color: transparent !important;
			border-bottom: none !important;
		}
		.bootstrap-iso ul.nav.nav-tabs > li:first-child {
			border-left: none !important;
		}
		
		.bootstrap-iso ul.nav-tabs > li,
		.bootstrap-iso ul.nav-tabs > li > a:hover {
			background-color: #db1f1a !important;
		}

		.bootstrap-iso ul.nav-tabs > li.active > a,
		.bootstrap-iso ul.nav-tabs > li.active > a:hover {
			color           : #FFF !important;
			background-color: rgb(2, 105, 154) !important;
			border-top      : none !important;
			border-bottom   : none !important;
		}
		.bootstrap-iso ul.nav-tabs > li.active {
			border-top   : 1px solid rgb(2, 105, 154);
			border-bottom: 1px solid rgb(2, 105, 154);
		}

		.bootstrap-iso div.tab-content {
			color        : #FFF;
			border-left  : 1px solid transparent !important;
			border-right : 1px solid rgb(2, 105, 154);
			border-bottom: none;
			margin-top   : 25px;
		}

		.bootstrap-iso button.btn-primary,
		.bootstrap-iso button.btn-primary:active,
		.bootstrap-iso button.btn-primary:hover,
		.bootstrap-iso button.btn-primary:focus {
			/*background-color: <?=$color?>;*/
			border-color : #FFF;
			border-radius: 2px;
			/*border-bottom: 2px solid #782828;*/
			border: none;
			background   : #969696; /* Old browsers */
			background   : -moz-linear-gradient(top, #969696 0%, #636363 100%, #1c1c1c 100%, #2c2c2c 100%, #111111 100%, #000000 100%, #131313 100%); /* FF3.6+ */
			background   : -webkit-gradient(linear, left top, left bottom, color-stop(0%,#969696), color-stop(100%,#636363), color-stop(100%,#1c1c1c), color-stop(100%,#2c2c2c), color-stop(100%,#111111), color-stop(100%,#000000), color-stop(100%,#131313)); /* Chrome,Safari4+ */
			background   : -webkit-linear-gradient(top, #969696 0%,#636363 100%,#1c1c1c 100%,#2c2c2c 100%,#111111 100%,#000000 100%,#131313 100%); /* Chrome10+,Safari5.1+ */
			background   : -o-linear-gradient(top, #969696 0%,#636363 100%,#1c1c1c 100%,#2c2c2c 100%,#111111 100%,#000000 100%,#131313 100%); /* Opera 11.10+ */
			-webkit-box-shadow: 0px 6px 9px 0px rgba(50, 48, 50, 0.59);
			-moz-box-shadow:    0px 6px 9px 0px rgba(50, 48, 50, 0.59);
			box-shadow:         0px 6px 9px 0px rgba(50, 48, 50, 0.59);
		}
		table.ui-datepicker-calendar > tbody > tr > td > a.ui-state-hover,
		td.ui-datepicker-current-day > a.ui-state-active {
			background-color: <?=$color?> !important;
			border-color    : <?=$color?> !important;
			color           : #FFF;
		}
		.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
			color     : #333;
			border    : 1px solid #c5c5c5;
			background: none;
		}
		table.ui-datepicker-calendar > tbody > tr > td > a,
		table.ui-datepicker-calendar > tbody > tr > td > span {
			text-align    : center;
			padding-left  : 7px;
			padding-right : 7px;
			padding-top   : 5px;
			padding-bottom: 5px;
		}
		table.ui-datepicker-calendar > tbody > tr > td > a {
			background-color: #DDD !important;
		}

		<?php
		break;
	
	default:
		# code...
		break;
} ?>

<?php endif; ?>