(function ($) {
	$(function () {
		console.log('Widget Script Loaded');
		$.datepicker.setDefaults({
			minDate   : new Date(),
			dateFormat: 'dd/mm/yy'
		});

		var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"C",
			"C++",
			"Clojure",
			"COBOL",
			"ColdFusion",
			"Erlang",
			"Fortran",
			"Groovy",
			"Haskell",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
	    ];

	    $('ul.nav-tabs li[role="presentation"]').click(function () {
	    	var $this = $( this );
	    	if( $this.data('title') ) {
	    		$( 'li#title-tab strong' ).text( $this.data('title') );
	    	}
	    });

		// ######## FLIGHTS ############

		$('form#flights input#depart').datepicker({
			onSelect: function ( dateText, inst ) {
				// console.log( 'Depart',  );
				var link = $( this ).data('update');
				var date = new Date(dateText);
				date.setDate( date.getDate() + 3 );

				if( typeof link != 'undefined' ) {
					$( link ).datepicker('option', 'minDate', new Date( dateText ) );
					$( link ).datepicker('setDate', $.datepicker.formatDate('mm/d/yy', date));
				}


				var nDate = dateText.split('/');
		 
			    $('#SyearMonth').val(nDate[2]+""+nDate[1]);
			    $('#Sday').val(nDate[0]);
			    $('#Smonth').val(nDate[1]);
			    $('#Syear').val(nDate[2]);


			    if(parseInt(date.getMonth() +1 )<10){
				var temp = date.getMonth() +1;
				$('#S2yearMonth').val(date.getFullYear()+"0"+temp);
			    }else{
				var temp = date.getMonth() +1;
				$('#S2yearMonth').val(date.getFullYear()+""+temp);
			    }
			     
			    if(parseInt(date.getDate())<10){
			     $('#S2day').val("0"+date.getDate());
			    }else{
			     $('#S2day').val(date.getDate());
			    }
			    if(parseInt(date.getMonth() +1 )<10){
				 $('#S2month').val("0"+(date.getMonth() +1 ));
			    }else{
				 $('#S2month').val((date.getMonth() +1 ));
			    }
			    $('#S2year').val(date.getFullYear());
			}
		});

		$('form#flights input#return-input').datepicker({
			minDate: new Date(),
			onSelect: function ( dateText, inst ) {

				var nDate = dateText.split('/')
				$('#S2yearMonth').val(nDate[2]+""+nDate[1]);
			    $('#S2day').val(nDate[0]);
			    $('#S2month').val(nDate[1]);
			    $('#S2year').val(nDate[2]);
			}
		});

		$.each( $('form#flights input.fligh-options'), function ( key, ele ) {

			$( this ).change(function () {
				// console.log( $( this ).prop('checked') );
				var options = $( this ).data();

				if( typeof options.hide != 'undefined' ) {
					$( options.hide ).parent().attr('style', 'visibility: hidden');
				}
				if( typeof options.show != 'undefined' ) {
					$( options.show ).parent().attr('style', 'visibility: visible');
				}
				if( typeof options.reDirect != 'undefined' && options.reDirect == true ) {
					window.location.href='https://reservations.ifly.net.au/searchAir.do';
				}
			});
		});

		$('form#flights input#departCity').focus();
		$('form#flights input#departCity').autocomplete({
			minLength: 4,
			source: function ( request, response ) {
				// console.log( request, response );
				$.ajax({
					url     : "https://ifly.sabreexplore.com.au:443/citySearchJson.aj?sn=ifly&ida=true",
					dataType: "jsonp",
					data    : {
						term: request.term
					},
					success: function ( data ) {
						response( $.map( data.query.results.result, function ( item ) {
							return {
								id   : item.code,
								label: item.display,
								value: item.select
							};
						}) );
					}
				});
			},
			select: function( event, ui ) { 
			    if($("form#flights input:radio[name ='return']:checked").val() == "R"){
				  $('form#flights input#arrivalCity2').val( ui.item.value );
			    }
			}
		});
		$('form#flights input#arrivalCity').autocomplete({
			minLength: 4,
			source: function ( request, response ) {
				// console.log( request, response );
				$.ajax({
					url     : "https://ifly.sabreexplore.com.au:443/citySearchJson.aj?sn=ifly&ida=true",
					dataType: "jsonp",
					data    : {
						term: request.term
					},
					success: function ( data ) {
						response( $.map( data.query.results.result, function ( item ) {
							return {
								id   : item.code,
								label: item.display,
								value: item.select
							};
						}) );
					}
				});
			},
			select: function( event, ui ) { 
			    if($("form#flights input:radio[name ='return']:checked").val() == "R"){
				  $('form#flights input#departCity2').val( ui.item.value );
			    }
			}
		});


		// ############# HOTELS ####################

		$.each( $( 'select.my-select' ), function ( key, eleMain ) {
			$( this ).change( function () {

				var type = $( this ).data('type');
				switch ( type ) {
					case 'rooms':
						// console.log( 'Do the logic for ' + $( this ).val() + ' rooms' );
						var rooms  = $( this ).val();
						var $myEle = $( this ).parent().next();
						
						if( ! $myEle.length ) {
							$myEle = $( this ).parent().parent().next();
						}
						var roomLength = $myEle.parent().find('div.adult-child-wrapper').length;

						if( parseInt(roomLength) > parseInt( rooms ) ) {

							var $removeExtraRooms = $myEle.parent().find('div.adult-child-wrapper').slice( parseInt( rooms ), parseInt( roomLength ) );

							$.each( $removeExtraRooms, function ( key, ele ) {
								$( ele ).remove();
							});
						} else {
							// insert new rooms
							for( var i= parseInt( roomLength ) ; i<parseInt( rooms ); i++ ) {
								var $clone = $myEle.clone( true );
								$clone.find('div.child-ages').remove();
								$clone.find('div.widget-hotel-adult-wrapper').attr('style', 'padding-left: 0px;')

								$clone.attr('data-index', i);
								$clone.addClass('col col-md-12');
								$clone.attr('style', 'padding-top: 10px; padding-left: 0px; padding-right: 0px;');
								$clone.find( 'select.my-adults' ).attr('name', 'rooms[' + i + '][adult]');
								$clone.find( 'select.my-childs' ).attr('name', 'rooms[' + i + '][child]');
								// console.log( $clone.find( 'select.my-adults' ).parent() );
								$clone.find( 'select.my-adults' ).parent().addClass('other-adults');
 

								$myEle.parent().append( $clone );
							}
						}

						break;
					case 'childs':

						var value = $( this ).val();
						var index = $( this ).parent().parent().attr('data-index');
						var $myEle = $( this ).parent();
						var $clone = $myEle.clone( true );
						var isAgesExist = $myEle.parent().find( 'div.child-ages' ).length;
						// console.log(  );

						if( parseInt( value ) == 0 ) {
							if( isAgesExist > 0 ) {
								$myEle.parent().find( 'div.child-ages' ).remove();
							}
						} else {
							if( isAgesExist == 0 ) {
								var $element = $('<div class="col col-md-12 child-ages"></div>');

								for( var i = 0; i < parseInt( $(this).val() ); i++ ) {
									$element.append( '<div class="col col-md-3 col-xs-3"><label for="rooms[' + index + '][childAges][' + i + ']">Age</label><input type="text" name="rooms[' + index + '][childAges][]" id="rooms[' + index + '][childAges][' + i + ']" value="8" class="form-control"></div>' );
								}

								if( $myEle.next().length > 0 ) {
									$myEle.next().remove();
								}

								// $myEle.append( $element );
								$element.insertAfter( $myEle );	
							} else {
								// Add new Age or remove
								console.log( value );
								var $ages = $myEle.parent().find( 'div.child-ages' ).children();

								if( $ages.length > parseInt( value ) ) {
									// Remove the extra age
									var $removeAges = $ages.slice( parseInt( value ) );

									$.each( $removeAges, function ( key, ele ) {
										$( ele ).remove();
									});
								} else {
									// Add ages
									var newAges = parseInt( value ) - $ages.length;
									var ageLength = $ages.length;

									for( var i=ageLength; i < (ageLength + newAges); i++ ) {
										$myEle.parent().find( 'div.child-ages' ).append( '<div class="col col-md-3 col-xs-3"><label for="rooms[' + index + '][childAges][' + i + ']">Age</label><input type="text" name="rooms[' + index + '][childAges][]" id="rooms[' + index + '][childAges][' + i + ']" value="8" class="form-control"></div>' );
									}
								}
							}
						}
						
						break;
				}
			});
		});

		


		$('input#destination').each(function ( key, ele ) {
			// console.log( $( this ).data() );

			var formId = $( this ).data('form');

			$( this ).autocomplete({
				select: function ( event, ui ) {
					console.log( ui );
					$( 'form#' + formId + ' input[id="location[id]"]' ).val( ui.item.id );
					$( 'form#' + formId + ' input[id="location[city]"]' ).val( ui.item.city );
					$( 'form#' + formId + ' input[id="location[country_code]"]' ).val( ui.item.code );
				},
				source: function ( request, response ) {
					// console.log( request, response );
					// BASE URL
					$.ajax({
						url: '<?=base_url()?>api/search-city',
						dataType: 'json',
						data: {
							address: request.term
						},
						success: function ( data ) {
							// console.log(data);

							response( $.map(data, function ( item ) {
								return {
									'value': item.cityname + ((item.citystate.trim() == item.cityname.trim())?'':item.citystate) + item.countryname,
									'id'   : item.id,
									'city' : (item.cityname).trim(),
									'code' : item.countrycode
								}
							}) );
						}
					});
				}
			});
		});
<!-- console.log( $('input.my-datepicker').datepicker() ); -->
		setTimeout(function () {
			$('input.my-datepicker').datepicker({
				onSelect: function ( dateText, inst ) {
					console.log('Datepicker');
					var split = dateText.split('/');
					console.log( 'split', split );
					var format = split[1] + '/' + split[0] + '/' + split[2];
					var date = new Date( format );
					console.log( 'date', date );
					date.setDate( date.getDate() + 1 );

					var link = $(this).data('update');

					if( typeof link != 'undefined' ) {
						$( link ).datepicker('option', 'minDate', new Date( format ) );
						$( link ).datepicker('setDate', $.datepicker.formatDate('dd/mm/yy', date));
					}
				}
			});
		}, 1);

	});
}(jQuery));