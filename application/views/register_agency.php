<div class="main-wrapper content">
	<div class="jumbotron jumbotron-sm" style="margin-bottom: 0!important; ">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<h1 class="h1">
					NEW AGENCY
					<small>SIGN UP</smsall>
					</h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="col-md-8 ">
					<div class="col-lg-12 padd-right padd-left room-gues guest" style="margin-top:10px">
						<div class="guest-title">
							<div class="clearfix"></div>
						</div>

						<div class="col-md-12 " style="padding-top:10px;">
							<?php echo validation_errors(); ?>
							<form action="agency/signup" method="post" id="agencyForm">
								<div class="col-md-12">
									<?php echo $this->session->flashdata('signupError');?>
									<?php echo $this->session->flashdata('signupSuccess');?>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input id="agency_name" class="form-control" type="text" required="required" name="agency_name" value="" placeholder="Agency Name *" style="border:1px solid #ddd">
										
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input id="agency_code" class="form-control" type="text" required="required" name="agency_code" value="" placeholder="Agency Code *" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="phone" class="form-control" type="text" required="required" name="phone" value="" placeholder="Phone (including area code) *" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="fax" class="form-control" type="text" name="fax" value="" placeholder="Fax (including area code)" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="email" class="form-control" type="text" required="required" name="email" value="" placeholder="Email *" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="currency" class="form-control" type="text" name="currency" value="" placeholder="Currency" style="border:1px solid #ddd">
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<input id="address" class="form-control" type="text" required="required" name="address" value="" placeholder="Address *" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="row" style="margin-left: 1px;">
									<div class="col-md-4">
										<div class="form-group">
											<input id="city" class="form-control" type="text" required="required" name="city" value="" placeholder="City *" style="border:1px solid #ddd">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input id="state" class="form-control" type="text" required="required" name="state" value="" placeholder="State *" style="border:1px solid #ddd">
										</div>
									</div>
									<div class="col-md-4">
										<div class="row">
											
												<select id="country" class="styled hasCustomSelect" required="required" name="country" style="border:1px solid #ddd;  width: 200px; font-size: 12px;">
													<option value="">Select Country *</option>
													<option value="Afghanistan">Afghanistan</option>
													<option value="Albania">Albania</option>
													<option value="Algeria">Algeria</option>
													<option value="American Samoa">American Samoa</option>
													<option value="Andorra">Andorra</option>
													<option value="Angola">Angola</option>
													<option value="Anguilla">Anguilla</option>
													<option value="Antarctica">Antarctica</option>
													<option value="Antigua and Barbuda">Antigua and Barbuda</option>
													<option value="Argentina">Argentina</option>
													<option value="Armenia">Armenia</option>
													<option value="Aruba">Aruba</option>
													<option value="Australia">Australia</option>
													<option value="Austria">Austria</option>
													<option value="Azerbaijan">Azerbaijan</option>
													<option value="Bahamas">Bahamas</option>
													<option value="Bahrain">Bahrain</option>
													<option value="Bangladesh">Bangladesh</option>
													<option value="Barbados">Barbados</option>
													<option value="Belarus">Belarus</option>
													<option value="Belgium">Belgium</option>
													<option value="Belize">Belize</option>
													<option value="Benin">Benin</option>
													<option value="Bermuda">Bermuda</option>
													<option value="Bhutan">Bhutan</option>
													<option value="Bolivia">Bolivia</option>
													<option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
													<option value="Botswana">Botswana</option>
													<option value="Bouvet Island">Bouvet Island</option>
													<option value="Brazil">Brazil</option>
													<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
													<option value="Brunei Darussalam">Brunei Darussalam</option>
													<option value="Bulgaria">Bulgaria</option>
													<option value="Burkina Faso">Burkina Faso</option>
													<option value="Burundi">Burundi</option>
													<option value="Cambodia">Cambodia</option>
													<option value="Cameroon">Cameroon</option>
													<option value="Canada">Canada</option>
													<option value="Cape Verde">Cape Verde</option>
													<option value="Cayman Islands">Cayman Islands</option>
													<option value="Central African Republic">Central African Republic</option>
													<option value="Chad">Chad</option>
													<option value="Chile">Chile</option>
													<option value="China">China</option>
													<option value="Christmas Island">Christmas Island</option>
													<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
													<option value="Colombia">Colombia</option>
													<option value="Comoros">Comoros</option>
													<option value="Congo">Congo</option>
													<option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
													<option value="Cook Islands">Cook Islands</option>
													<option value="Costa Rica">Costa Rica</option>
													<option value="Cote d'Ivoire">Cote d'Ivoire</option>
													<option value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
													<option value="Cuba">Cuba</option>
													<option value="Cyprus">Cyprus</option>
													<option value="Czech Republic">Czech Republic</option>
													<option value="Denmark">Denmark</option>
													<option value="Djibouti">Djibouti</option>
													<option value="Dominica">Dominica</option>
													<option value="Dominican Republic">Dominican Republic</option>
													<option value="East Timor">East Timor</option>
													<option value="Ecuador">Ecuador</option>
													<option value="Egypt">Egypt</option>
													<option value="El Salvador">El Salvador</option>
													<option value="Equatorial Guinea">Equatorial Guinea</option>
													<option value="Eritrea">Eritrea</option>
													<option value="Estonia">Estonia</option>
													<option value="Ethiopia">Ethiopia</option>
													<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
													<option value="Faroe Islands">Faroe Islands</option>
													<option value="Fiji">Fiji</option>
													<option value="Finland">Finland</option>
													<option value="France">France</option>
													<option value="France Metropolitan">France Metropolitan</option>
													<option value="French Guiana">French Guiana</option>
													<option value="French Polynesia">French Polynesia</option>
													<option value="French Southern Territories">French Southern Territories</option>
													<option value="Gabon">Gabon</option>
													<option value="Gambia">Gambia</option>
													<option value="Georgia">Georgia</option>
													<option value="Germany">Germany</option>
													<option value="Ghana">Ghana</option>
													<option value="Gibraltar">Gibraltar</option>
													<option value="Greece">Greece</option>
													<option value="Greenland">Greenland</option>
													<option value="Grenada">Grenada</option>
													<option value="Guadeloupe">Guadeloupe</option>
													<option value="Guam">Guam</option>
													<option value="Guatemala">Guatemala</option>
													<option value="Guinea">Guinea</option>
													<option value="Guinea-Bissau">Guinea-Bissau</option>
													<option value="Guyana">Guyana</option>
													<option value="Haiti">Haiti</option>
													<option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
													<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
													<option value="Honduras">Honduras</option>
													<option value="Hong Kong">Hong Kong</option>
													<option value="Hungary">Hungary</option>
													<option value="Iceland">Iceland</option>
													<option value="India">India</option>
													<option value="Indonesia">Indonesia</option>
													<option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
													<option value="Iraq">Iraq</option>
													<option value="Ireland">Ireland</option>
													<option value="Israel">Israel</option>
													<option value="Italy">Italy</option>
													<option value="Jamaica">Jamaica</option>
													<option value="Japan">Japan</option>
													<option value="Jordan">Jordan</option>
													<option value="Kazakhstan">Kazakhstan</option>
													<option value="Kenya">Kenya</option>
													<option value="Kiribati">Kiribati</option>
													<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
													<option value="Korea, Republic of">Korea, Republic of</option>
													<option value="Kuwait">Kuwait</option>
													<option value="Kyrgyzstan">Kyrgyzstan</option>
													<option value="Lao, People's Democratic Republic">Lao, People's Democratic Republic</option>
													<option value="Latvia">Latvia</option>
													<option value="Lebanon">Lebanon</option>
													<option value="Lesotho">Lesotho</option>
													<option value="Liberia">Liberia</option>
													<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
													<option value="Liechtenstein">Liechtenstein</option>
													<option value="Lithuania">Lithuania</option>
													<option value="Luxembourg">Luxembourg</option>
													<option value="Macau">Macau</option>
													<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
													<option value="Madagascar">Madagascar</option>
													<option value="Malawi">Malawi</option>
													<option value="Malaysia">Malaysia</option>
													<option value="Maldives">Maldives</option>
													<option value="Mali">Mali</option>
													<option value="Malta">Malta</option>
													<option value="Marshall Islands">Marshall Islands</option>
													<option value="Martinique">Martinique</option>
													<option value="Mauritania">Mauritania</option>
													<option value="Mauritius">Mauritius</option>
													<option value="Mayotte">Mayotte</option>
													<option value="Mexico">Mexico</option>
													<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
													<option value="Moldova, Republic of">Moldova, Republic of</option>
													<option value="Monaco">Monaco</option>
													<option value="Mongolia">Mongolia</option>
													<option value="Montserrat">Montserrat</option>
													<option value="Morocco">Morocco</option>
													<option value="Mozambique">Mozambique</option>
													<option value="Myanmar">Myanmar</option>
													<option value="Namibia">Namibia</option>
													<option value="Nauru">Nauru</option>
													<option value="Nepal">Nepal</option>
													<option value="Netherlands">Netherlands</option>
													<option value="Netherlands Antilles">Netherlands Antilles</option>
													<option value="New Caledonia">New Caledonia</option>
													<option value="New Zealand">New Zealand</option>
													<option value="Nicaragua">Nicaragua</option>
													<option value="Niger">Niger</option>
													<option value="Nigeria">Nigeria</option>
													<option value="Niue">Niue</option>
													<option value="Norfolk Island">Norfolk Island</option>
													<option value="Northern Mariana Islands">Northern Mariana Islands</option>
													<option value="Norway">Norway</option>
													<option value="Oman">Oman</option>
													<option value="Pakistan">Pakistan</option>
													<option value="Palau">Palau</option>
													<option value="Panama">Panama</option>
													<option value="Papua New Guinea">Papua New Guinea</option>
													<option value="Paraguay">Paraguay</option>
													<option value="Peru">Peru</option>
													<option value="Philippines">Philippines</option>
													<option value="Pitcairn">Pitcairn</option>
													<option value="Poland">Poland</option>
													<option value="Portugal">Portugal</option>
													<option value="Puerto Rico">Puerto Rico</option>
													<option value="Qatar">Qatar</option>
													<option value="Reunion">Reunion</option>
													<option value="Romania">Romania</option>
													<option value="Russian Federation">Russian Federation</option>
													<option value="Rwanda">Rwanda</option>
													<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
													<option value="Saint Lucia">Saint Lucia</option>
													<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
													<option value="Samoa">Samoa</option>
													<option value="San Marino">San Marino</option>
													<option value="Sao Tome and Principe">Sao Tome and Principe</option>
													<option value="Saudi Arabia">Saudi Arabia</option>
													<option value="Senegal">Senegal</option>
													<option value="Seychelles">Seychelles</option>
													<option value="Sierra Leone">Sierra Leone</option>
													<option value="Singapore">Singapore</option>
													<option value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
													<option value="Slovenia">Slovenia</option>
													<option value="Solomon Islands">Solomon Islands</option>
													<option value="Somalia">Somalia</option>
													<option value="South Africa">South Africa</option>
													<option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
													<option value="Spain">Spain</option>
													<option value="Sri Lanka">Sri Lanka</option>
													<option value="St. Helena">St. Helena</option>
													<option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
													<option value="Sudan">Sudan</option>
													<option value="Suriname">Suriname</option>
													<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
													<option value="Swaziland">Swaziland</option>
													<option value="Sweden">Sweden</option>
													<option value="Switzerland">Switzerland</option>
													<option value="Syrian Arab Republic">Syrian Arab Republic</option>
													<option value="Taiwan, Province of China">Taiwan, Province of China</option>
													<option value="Tajikistan">Tajikistan</option>
													<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
													<option value="Thailand">Thailand</option>
													<option value="Togo">Togo</option>
													<option value="Tokelau">Tokelau</option>
													<option value="Tonga">Tonga</option>
													<option value="Trinidad and Tobago">Trinidad and Tobago</option>
													<option value="Tunisia">Tunisia</option>
													<option value="Turkey">Turkey</option>
													<option value="Turkmenistan">Turkmenistan</option>
													<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
													<option value="Tuvalu">Tuvalu</option>
													<option value="Uganda">Uganda</option>
													<option value="Ukraine">Ukraine</option>
													<option value="United Arab Emirates">United Arab Emirates</option>
													<option value="United Kingdom">United Kingdom</option>
													<option value="United States">United States</option>
													<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
													<option value="Uruguay">Uruguay</option>
													<option value="Uzbekistan">Uzbekistan</option>
													<option value="Vanuatu">Vanuatu</option>
													<option value="Venezuela">Venezuela</option>
													<option value="Vietnam">Vietnam</option>
													<option value="Virgin Islands (British)">Virgin Islands (British)</option>
													<option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
													<option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
													<option value="Western Sahara">Western Sahara</option>
													<option value="Yemen">Yemen</option>
													<option value="Yugoslavia">Yugoslavia</option>
													<option value="Zambia">Zambia</option>
													<option value="Zimbabwe">Zimbabwe</option>
												</select>
						
											
										</div>
									</div>
								</div>
								
								<div class="col-lg-12">
									<br><hr>
									<label><h5>Business Contact</h5></label>
		
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="first_name" class="form-control" type="text" name="first_name" value="" placeholder="First Name" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="last_name" class="form-control" type="text"  name="last_name" value="" placeholder="Last Name" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input id="b_address" class="form-control" type="text" name="b_address" value="" placeholder="Address" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="b_city" class="form-control" type="text"  name="b_city" value="City" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input id="b_state" class="form-control" type="text" name="b_state" value="" placeholder="State" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<input id="b_country" class="form-control" type="text"  name="b_country" value="" placeholder="Country" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<input id="b_phone" class="form-control" type="text"  name="b_phone" value="" placeholder="Phone" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<input id="b_email" class="form-control" type="text"  name="b_email" value="" placeholder="Email" style="border:1px solid #ddd">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<input id="b_fax" class="form-control" type="text"  name="b_fax" value="" placeholder="Fax" style="border:1px solid #ddd">
									</div>
								</div>
<!-- 								<div class="col-lg-12">
									<label>Which regions does your agency specialise in?</label>
								</div>
								<div class="col-md-12 " style="padding-bottom:10px">
									<div class="form-group">
										<input id="spicials" class="form-control" type="text" required="required" name="spicials" value="Australia" placeholder="Specialise" style="border:1px solid #ddd">
								</div> -->
									<div class="clearfix"></div>
								</div>
								<br>
								<div class="row" style="margin-left:20px;">
									<div class="col-md-9" >
										<div class="form-group">
											<div class="checkbox">
												<label>
													<input id="agree" type="checkbox" required="required" name="agree" value="YES" style="border:1px solid #ddd">
													I have read and agree with the
													<a id="terms" href="#terms">terms and conditions</a>
												</label>
											</div>
										</div>
									</div>
									<div class="col-md-3" style="padding-bottom:10px">
										<input class="btn btn-primary" type="submit" value="Sign Up">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="hidden-xs hidden-sm col-md-4 pull-right">
					<div class="panel panel-default " style="margin-top: -760px;">
						<div class="panel-heading">
							<h3 class="panel-title">Why Travelrez?</h3>
						</div>
						<ul class="list-group">
							<li class="list-group-item firstItem">TravelRez is a 21st Century booking platform for savvy Travel Agents. </li>
							<li class="list-group-item">Comprehensive, global system, backed by personalised service. One consultant handles your booking from start to finish backed up by our local and European staff on the ground.</li>
							<li class="list-group-item">No minimum nights stay</li>
							<li class="list-group-item">No cancellation fees (subject to document production & 3rd party instant purchase rules) </li>
							<li class="list-group-item">Extensive brochure and specialist websites </li>
							<li class="list-group-item">Document specialists in Russia & Ukraine </li>
							<li class="list-group-item">Premier Groups Department – Specialists in School, Incentive, Corporate, Sporting, Historic & Pilgrimage and any custom trips. </li>
							<li class="list-group-item">Leading FIT Product and ease of booking through online dedicated industry site </li>
							<li class="list-group-item lastItem">Local Account Managers visiting your agency in person. </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- TERMS MODAL-->
<!-- Modal -->
<div id="terms_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-content">
			<div class="modal-header" style="">
				<h4 class="modal-title">Terms and Conditions</h4>
			</div>
			<div class="modal-body" style="overflow:auto ;max-height:500px; color:#666666;text-align: justify;">
				<div>
					<h4>OUR COMMITMENT TO YOU</h4>
					<br>
					<p>
					<strong>Reservation on TravelRez:</strong>
					<br>
					When you make a booking through TravelRez, either directly online or through your TravelRez booking consultant, we allocate the products to you, subject to availability, and issue a confirmation/invoice. You understand that TravelRez is a division of Magic Tours International Pty Ltd, a company registered in Australia and all bookings are processed through the company’s wholesale divisions. Some documents might make mention of these and other company owned brands, depending on contracts negotiated by our suppliers with the individual company brands.
					<br>
					<br>
					<strong>TravelRez Price Guarantee:</strong>
					<br>
					All prices in this brochure are based in AUD and are calculated at the best daily exchange rate. We guarantee that whatever happens to the value of the AUD in 2015 we will maintain the level of your holiday price to the best of our ability. Should costs related to suppliers' charges change, it may be necessary for us to make a surcharge on the price of your holiday. You will be entitled to cancel your holiday within 7 days of notification, with a full refund of all monies paid, except for any amendment charges levied by us before the notification of change. Rail and Ferry Services will incur a 100% cancellation fee once booking is confirmed as per supplier's instructions.
					<br>
					<br>
					<strong>If We Change Your Booking:</strong>
					<br>
					Should circumstances beyond our control dictate, we reserve the right to make amendments to your holiday and will inform you about these as soon as they arise. If the minimum number of persons required for each tour is not reached, we reserve the right to cancel the tour. Should it be necessary to cancel your booking, we will offer an alternative from our brochure. If this is not available or suitable to you, we will refund all monies paid by you. We cannot, however, accept responsibility for changes or cancellations due to "force majeure" – threat of war, political unrest, fire, adverse weather conditions, natural disasters or any other such circumstance beyond our control. In such instances we will refund all monies, less reasonable expenses incurred by our organisations.
					<br>
					<br>
					</p>
					<h4>YOUR COMMITMENT TO US</h4>
					<br>
					<br>
					<strong>Payment:</strong>
					<br>
					In line with TravelRez booking conditions, a deposit of AUD$200 per person might be required for FIT bookings and 30% per person for Escorted Tours and Cruising before we can make a booking. TravelRez will display booking and payment conditions at the time of booking. The deposit will only be refunded if the requested holiday reservation is unavailable. Payment by you of the deposit implies acceptance of the terms and conditions of this agreement. All money received by Magic Tours International Pty Ltd will be deposited as required by law and dispersed to our suppliers according to their conditions of payment. The balance of the purchase price is payable sixty - (60) days prior to your holiday departure or before you depart Australia. If the balance remains unpaid after this date, we reserve the right to cancel your booking and charge a cancellation fee in accordance with the scale set out below. If you book within sixty - (60) days of your departure from Australia, the full amount as stated in our brochure must be forwarded to us immediately.
					<br>
					<br>
					<strong>Booking Amendments:</strong>
					<br>
					All alterations that you wish to make to the confirmed booking arrangements will be sent to us in writing and signed by the person who made the initial booking. We will make every effort to satisfy your request. An amendment charge will apply to cover some of our expenses after documentation has been issued.
					<br>
					<br>
					<strong>Cancellations:</strong>
					<br>
					Any cancellations will be in writing and signed by the person who made the initial reservation. In order for us to cover our expenses and losses, we will apply the following scale of cancellation fees:
					<br>
					<br>
					More than 60 days: Deposit only 60 – 29 days: 40% of package price 28 – 15 days: 60% of package price 14 – 4 days: 80% of package price 3 – Day of Departure: 100% of package price
					<br>
					<br>
					Service provider fees may also apply to the above cancellation percentages. Your clients’ travel insurance policy may cover the refunding of these charges if the reason for your cancellation falls within the terms of the insurance policy.
					<br>
					<br>
					<strong>Liability:</strong>
					<br>
					Please note that you do not receive monies as an agent for Magic Tours International Pty Ltdand we do not take responsibility for such monies until we receive the monies from you.
					<br>
					<br>
					<strong>If You Have A Complaint:</strong>
					<br>
					All complaints should be reported to your hotel and/or local ground operator staff, who will do their best to settle the matter immediately. Any unresolved complaint must be registered with us within 30 days of your return from your holiday in Europe.
					<br>
					<br>
					<strong>Passport and Visas:</strong>
					<br>
					A valid passport and respective visas are the responsibility of the passenger. We are not responsible for visas.
					<br>
					<br>
					<strong>Responsibility:</strong>
					<br>
					By accepting this booking with Magic Tours International Pty Ltd& Mediterranean Holidays, you acknowledge that you have read and agreed to our booking conditions. You, also acknowledge that although all holidays contained in this brochure are packaged by Magic Tours International Pty Ltd& Mediterranean Holidays, we do not own, manage control or operate any aircrafts, hotels or coaches. All bookings made by Magic Tours International Pty Ltd on your behalf with the persons and companies providing transportation accommodation and other services ("suppliers") are made subject to the terms and conditions imposed by the suppliers in providing those services. Magic Tours International Pty Ltd cannot be held liable for any breach of this contract arising as a result of any act or omission by or on behalf of the suppliers or for any loss, injury or damage arising as a result of such acts or omissions. Magic Tours International Pty Ltd cannot be held liable for any loss, injury or damage resulting from occurrences beyond its control including strikes, theft, delays or cancellations as well as changes in schedules. No refunds will be given for any unused portions of the tour. Magic Tours International Pty Ltd reserves the right to provide travel documents at the holiday departure points in Europe.
					<br>
					<br>
					<strong>Airline Responsibility Clause:</strong>
					<br>
					This brochure is not issued on behalf of and does not commit any of the airlines whose services are used in the course of our tours.
					<br>
					<br>
					<strong>Booking Conditions:</strong>
					<br>
					Upon receipt by you of our confirmation/invoice, this terms & conditions page will form the basis of a binding contract between you and Magic Tours International Pty Ltd and it is important that you read all parts of the terms and conditions on our website before booking your clients’ holiday. Everything on the website is subject to availability and might change without notice. The contract is governed by Queensland Law with any action under this contract being heard within the jurisdiction of Queensland Courts.
					<p></p>
				</div>
			</div>
		</div>
    </div>
  </div>
</div>
