<?php

$lang['LOGIN'] = 'You are successfully login.';
$lang['USER_NOT_FOUND'] = 'Invalid credentials.';
$lang['INVALID_PIN'] = 'Invalid pin.';
$lang['PIN_EXPIRED'] = 'OTP code expired. Please resend';
$lang['EXPIRED_TOKEN'] = 'Token expired.';
$lang['CONTINUE'] = 'Continue on your work.';
$lang['DISCOUNT_CREATED'] = 'Discount created.';
$lang['DISCOUNT_CODE_EXISTS'] = 'The code field already exists.';