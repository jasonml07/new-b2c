<?php

/**
* 
*/
class Xml_model extends CI_Model
{
    private $CI = null;
    public function __construct (  )
    {
        parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->model('log_model');
    }

    /**
     * [destinationRooms description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function destinationRooms ( $data, $isSync = 0 )
    {
        if( $this->checkFile( 'destination-response.xml' ) === TRUE )
        {
            $xml = file_get_contents(TEST_XML_FILES_PATH . 'destination-response.xml', true);
            return $xml;
        }

        $xml = '<hotel-search entityType="location" entityID="' . $data['location']['id'] . '">';
            if ( $isSync )
            {
                # Code here
                $xml .= '<nopolls-timeout>' . $isSync . '</nopolls-timeout>';
            } // end of if statement
            $xml .= '<dates from="' . $data['start_date']. '" to="' . $data['end_date']. '"/>';
            $xml .= '<filters>';
                $xml .= '<filter type="payAtTheHotel">0</filter>';
                $xml .= '<filter type="onRequest">0</filter>';
                $xml .= '<filter type="showSpecialDeals">1</filter>';
                $xml .= '<filter type="getPackageRates">0</filter>';
            $xml .= '</filters>';
            $xml .= '<pax-group customerCountryCode="' . $data['location']['country_code'] . '">';

           foreach ( $data["rooms"] as $key => $value )
           {
                $xml .= '<pax>';
                $xml .= '<adults count="' . $value['adult'] . '" />';
                $xml .= '<children count="' .$value['child'] . '">';
                if( isset($value['childAges']) && count($value['childAges']) > 0 )
                {
                    foreach ( $value['childAges'] as $childKey => $childValue )
                    {
                         $xml .= '<age>' . $childValue . '</age>';
                    }
                }
                $xml .= '</children>';
                $xml .= '</pax>';
           }
            // The number of rooms should be loop
           
            $xml .= '</pax-group>';
            $xml .= '<currencies default="' . DEFAULT_CURRENCY . '">';
                $xml .= '<currency code="EUR"/>';
                $xml .= '<currency code="GBP"/>';
                $xml .= '<currency code="USD"/>';
            $xml .= '</currencies>';
        $xml .= '</hotel-search>';

        $xml = $this->XML_REQUEST_GEN($xml);

        $response = $this->buildLog( $xml, 'destination' );
        return $response;
    }

    /**
     * [pollInitiate description]
     * @param  [type] $sess [description]
     * @return [type]       [description]
     */
    public function pollInitiate ( $sess )
    {
        if( $this->checkFile( 'poll-initiate-response.xml' ) === TRUE )
        {
            $xml = file_get_contents(TEST_XML_FILES_PATH . 'poll-initiate-response.xml', true);
            return $xml;
        }

        $xml = $this->XML_REQUEST_GEN('<hotel-poll session="'. $sess .'" last="0"/>');

        $response = $this->buildLog( $xml, 'poll-initiate' );
        return $response;
    }

    /**
     * [requestRooms description]
     * @param  [type] $session  [description]
     * @param  [type] $hotel_id [description]
     * @return [type]           [description]
     */
    public function requestRooms ( $session, $hotel_id )
    {

        if( $this->checkFile( 'rooms-response.xml' ) === TRUE )
        {
            $xml = file_get_contents(TEST_XML_FILES_PATH . 'rooms-response.xml', true);
            return $xml;
        }

        $xml = $this->XML_REQUEST_GEN('<hotel-rooms session="' . $session . '" id="' . $hotel_id . '"/>');

        $response = $this->buildLog( $xml, 'rooms' );
        return $response;
    }

    /**
     * [bookingInfo description]
     * @param  [type] $sess          [description]
     * @param  [type] $selectedRooms [description]
     * @return [type]                [description]
     */
    public function bookingInfo ( $sess, $selectedRooms )
    {
        if( $this->checkFile( 'booking-info-response.xml' ) === TRUE )
        {
            $xml = file_get_contents(TEST_XML_FILES_PATH . 'booking-info-response.xml', true);
            return $xml;
        }

        $xml = '<hotel-booking-info session="' . $sess . '">';
            $xml .= '<results>';
                foreach ($selectedRooms as $key => $selectedRoom) {
                    $xml .= '<result>' . $selectedRoom['id'] . '</result>';
                }
            $xml .= '</results>';
        $xml .= '</hotel-booking-info>';
        $xml = $this->XML_REQUEST_GEN($xml);

        $response = $this->buildLog( $xml, 'booking-info' );

        return $response;
    }

    /**
     * [booking description]
     * @param  [type] $sess [description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function booking ( $sess, $data )
    {
        if( $this->checkFile( 'booking-response.xml' ) === TRUE )
        {
            $xml = file_get_contents(TEST_XML_FILES_PATH . 'booking-response.xml', true);
            return $xml;
        }

        $location = $this->nativesession->get('session_post')['location'];

        $xml = '<hotel-book session="' . $sess . '" clientRef="' . $data['references'] . '" customerCountryCode="' . $location['country_code'] . '">';
            $xml .= '<customer>';
                $xml .= '<title>' . $data['voucher']['title'] . '</title>';
                $xml .= '<firstName>' . $data['voucher']['firstname'] . '</firstName>';
                $xml .= '<lastName>' . $data['voucher']['lastname'] . '</lastName>';
                if( isset($data['voucher']['phone']) && ! empty($data['voucher']['phone']) ) {
                    $xml .= '<phone type="mobile">' . $data['voucher']['phone'] . '</phone>';
                }
                if( isset($data['voucher']['home']) && ! empty($data['voucher']['home']) ) {
                    $xml .= '<phone type="home">' . $data['voucher']['home'] . '</phone>';
                }
                $xml .= '<email>' . $data['voucher']['email'] . '</email>';
                $xml .= '<address>Austrilia Queens Land</address>';
                $xml .= '<postCode>5000</postCode>';
                $xml .= '<city>Queens Land</city>';
                $xml .= '<countryCode>' . $location['country_code'] . '</countryCode>';
            $xml .= '</customer>';
            $xml .= '<rooms>';
            // TODO
            // Loop on the rooms
                foreach ( $data['rooms'] as $key => $room )
                {
                    $xml .= '<room resultId="' . $room['id'] . '">';
                        $xml .= '<pax-group>';

                            foreach ( $room['adult'] as $key => $adult )
                            {
                                $xml .= '<pax>';
                                    $xml .= '<title>' . $adult['title'] . '</title>';
                                    $xml .= '<firstName>' . $adult['firstname'] . '</firstName>';
                                    $xml .= '<lastName>' . $adult['lastname'] . '</lastName>';
                                $xml .= '</pax>';
                            }
                            if( isset($room['child']) && count($room['child']) > 0 )
                            {
                                foreach ($room['child'] as $key => $child) {
                                    $xml .= '<pax>';
                                        $xml .= '<title>C</title>';
                                        $xml .= '<age>' . $child['age'] . '</age>';
                                        $xml .= '<firstName>' . $child['firstname'] . '</firstName>';
                                        $xml .= '<lastName>' . $child['lastname'] . '</lastName>';
                                    $xml .= '</pax>';
                                }
                            }
                        $xml .= '</pax-group>';
                    $xml .= '</room>';
                }
            $xml .= '</rooms>';
            $xml .= '<special-request-fields>';
                $xml .= '<field code="0">' . $data['request'] . '</field>';
            $xml .= '</special-request-fields>';
        $xml .= '</hotel-book>';
        $xml = $this->XML_REQUEST_GEN($xml);

        $response = $this->buildLog( $xml, 'booking');
        return $response;
    }

    /**
     * [reservationDetail description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function reservationDetail ( $id )
    {
        if( $this->checkFile( 'reservation-response.xml' ) === TRUE )
        {
            $xml = file_get_contents(TEST_XML_FILES_PATH . 'reservation-response.xml', true);
            return $xml;
        }


        $xml = $this->XML_REQUEST_GEN('<hotel-reservation-details reservationId="' . $id . '"/>');
        $response = $this->buildLog( $xml, 'reservation');

        return $response;
    }

    /**
     * [cancelBooking description]
     * @param  [type] $reservationId [description]
     * @param  string $message       [description]
     * @return [type]                [description]
     */
    public function cancelBooking ( $reservationId, $message = '' )
    {
        if( $this->checkFile( 'cancellation-response.xml' ) === TRUE )
        {
            $xml = file_get_contents(TEST_XML_FILES_PATH . 'cancellation-response.xml', true);
            return $xml;
        }


        $xml = $this->XML_REQUEST_GEN('<hotel-cancel id="' . $reservationId . '"> <cancellation-reason>' . $message . '</cancellation-reason></hotel-cancel>');
        $response = $this->buildLog( $xml, 'cancellation');

        return $response;
    }

    /**
     * [curlRequest description]
     * @param  [type] $xml [description]
     * @return [type]      [description]
     */
    private function curlRequest ( $xml )
    {
        $soapUrl         = "http://mishor4.innstant-servers.com/"; // asmx URL of WSDL 
        $soapUser        = INSTANT_USER;  //  username 
        $soapPassword    = INSTANT_PASS; // password 
        
        $xml_post_string = $xml;

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt($ch, CURLOPT_URL, $soapUrl); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc 
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 120); 
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request 

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * 
     * @param [type] $xmlData [description]
     */
    private function XML_REQUEST_GEN ( $xmlData )
    {
        $xml = '<request version="4.0">';
        $xml .= '<auth>';
            $xml .= '<username>'.INSTANT_USER.'</username>';
            $xml .= '<password>'.INSTANT_PASS.'</password>';
            $xml .= '<agent>shayne</agent>';
            $xml .= '<clientIp>travelrez.net.au</clientIp>';
            $xml .= '<clientUserAgent>example user agent</clientUserAgent>';
        $xml .= '</auth>';

        
    
        $xml .= $xmlData;
        $xml .= '</request>';


        return  $xml;
    }

    /**
     * [buildLog description]
     * @param  [type] $xml      [description]
     * @param  [type] $fileName [description]
     * @return [type]           [description]
     */
    private function buildLog ( $xml, $fileName )
    {
        if( LOG_XML_RESPONSE )
        {
            $this->CI->log_model->hotel(
                $fileName. '-request.xml',
                readable_xml($xml)
            );
        }
            $response = $this->curlRequest($xml);
        if( LOG_XML_RESPONSE )
        {
            $this->CI->log_model->hotel(
                $fileName. '-response.xml',
                readable_xml($response)
            );
        }
        return $response;
    }

    /**
     * [checkFile description]
     * @param  [type] $file [description]
     * @return [type]       [description]
     */
    private function checkFile ( $file )
    {
        if( GET_DATA_FROM_XMLS )
        {
            if( ! file_exists(TEST_XML_FILES_PATH . $file) )
            { 
                $content = 'File does not exist "' . $file .'" at ' . get_class($this);
                $this->log_model->debug( $content );
                die();
                return FALSE;
            }
            return TRUE;
        }
        return FALSE;
    }
}