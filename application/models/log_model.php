<?php

/**
 * 
 */
class Log_model extends CI_Model
{
	
	private $baseFiles = array( 'hotel', 'flight', 'bus');
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
	}

	/**
	 * [debug description]
	 * @param  [type] $content [description]
	 * @return [type]          [description]
	 */
	public function debug ( $content )
	{
		$destination = LOG_XML_MAIN_PATH . 'debug.log';
		$content     = date('F d, Y H:i:s', strtotime('now')) . ':: ' . $content . PHP_EOL;

		if( ! file_exists( $destination ) )
		{
			$this->createFile( $destination, $content);
			return FALSE;
		}
		file_put_contents($destination, $content, FILE_APPEND);
	}

	/**
	 * [hotelSearch description]
	 * @param  [type] $fileName [description]
	 * @param  [type] $content  [description]
	 * @return [type]           [description]
	 */
	public function hotel ( $fileName, $content )
	{
		$destination = LOG_XML_MAIN_PATH . 'hotel/' . $this->getSearchTime();
		if ( file_exists( $destination . $fileName ) )
		{
			# Code here
			$this->hotel( $fileName . '-' . strtotime('now'), $content );
		} // end of if statement
		$this->buildFile( $destination, $fileName, $content );
	}

	/**
	 * [bookingId description]
	 * @param  [type] $bookingId [description]
	 * @param  [type] $baseFile  integer an link to the array of @baseFiles
	 * @return [type]            [description]
	 */
	public function bookingId ( $bookingId, $baseFile )
	{
		$destination = $this->buildDestination( $baseFile );
		if( $destination === FALSE )
		{
			return $destination;
		}
		$this->buildFile( $destination, 'booking-id-' . $bookingId . '.txt', $bookingId );
	}

	/**
	 * [something description]
	 * @param  [type]  $fileName [ should has a file extention or either optional ]
	 * @param  [type]  $content  [description]
	 * @param  integer $baseFile [description]
	 * @return [type]            [description]
	 */
	public function something ( $fileName, $content, $baseFile, $append = TRUE, $rootDir = FALSE )
	{
		$destination = $this->buildDestination( $baseFile );
		if( $rootDir )
		{
			$destination = LOG_XML_MAIN_PATH;
		}
		if( $destination === FALSE )
		{
			return $destination;
		}

		$content .= PHP_EOL;
		if( $append === TRUE && file_exists($destination . '/' . $fileName) )
		{
			file_put_contents($destination . '/' . $fileName, $content, FILE_APPEND);
			return TRUE;
		}
		$this->buildFile( $destination, $fileName, $content );
	}

	/**
	 * [paymentReference description]
	 * @param  [type] $reference [description]
	 * @param  [type] $baseFile  integer an link to the array of @baseFiles
	 * @return [type]            [description]
	 */
	public function paymentReference ( $reference, $baseFile )
	{
		$destination = $this->buildDestination( $baseFile );
		if( $destination === FALSE )
		{
			return $destination;
		}
		$this->buildFile( $destination, 'payment-ref-' . $reference . '.txt', $reference );
	}

	/**
	 * [buildDestination description]
	 * @param  [type] $baseFile [description]
	 * @return [type]           [description]
	 */
	private function buildDestination ( $baseFile )
	{
		if( ! is_int($baseFile) || $baseFile < 1 || ! array_key_exists(((int)$baseFile - 1), $this->baseFiles) )
		{
			return FALSE;
		}
		$destination = LOG_XML_MAIN_PATH . $this->baseFiles[((int)$baseFile -1)] . '/' . $this->getSearchTime();
		return $destination;
	}

	/**
	 * [getSearchTime description]
	 * @return [type] [description]
	 */
	public function getSearchTime (  )
	{
		if( ! $this->nativesession->get( SYSTEM_TYPE . 'search_time') )
		{
			$time = get_next_id('log_model');
			$this->nativesession->set( SYSTEM_TYPE . 'search_time', $time);
			return $time;
		}
		return $this->nativesession->get( SYSTEM_TYPE . 'search_time');
	}

	/**
	 * [buildFile description]
	 * @param  [type] $destination [description]
	 * @param  [type] $file        [description]
	 * @param  [type] $content     [description]
	 * @return [type]              [description]
	 */
	private function buildFile ( $destination, $file, $content )
	{
		$this->createDestination( $destination );
		$this->createFile( $destination . '/' . $file, $content );
	}

	/**
	 * [createDestination description]
	 * @param  [type] $destination [description]
	 * @return [type]              [description]
	 */
	private function createDestination ( $destination )
	{
		if( ! file_exists($destination) )
		{
			mkdir($destination, 0755, TRUE);
		}
	}

	/**
	 * [createFile description]
	 * @param  [type] $file    [description]
	 * @param  [type] $content [description]
	 * @return [type]          [description]
	 */
	private function createFile ( $file, $content )
	{
		write_file( $file, $content );
	}
}