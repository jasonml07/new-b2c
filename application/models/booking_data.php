 <?php

 /**
 * 
 */
 class Booking_data extends CI_Model
 {
 	private $DB_NAME = 'db_booking_data';
 	
 	function __construct()
 	{
 		# code...
 		parent::__construct();
 	}

 	/**
	 * Sessioned data on the booking
	 * @return [type] [description]
	 */
	public function getSessionedData (  )
	{
		# Code here
		$reference = $this->nativesession->get('payway_reference_for_credit_card');

		$POST = $this->booking_data->get( $reference );

		return $POST;
	} // end of getSessionedData

 	/**
 	 * [get description]
 	 * @param  [type] $reference  [description]
 	 * @param  string $collection [description]
 	 * @return [type]             [description]
 	 */
 	public function get ( $reference, $collection = 'b2b' )
 	{
 		# Code here
 		$connection = new MongoClient();
		$db = $connection->selectDb( $this->DB_NAME );

		$collection = $db->selectCollection( $collection );

		$data = $collection->findOne(array('reference' => $reference));
		$connection->close();

		return $data;
 	} // end of get

 	/**
 	 * [reference description]
 	 * @param  string $duration [description]
 	 * @param  string $prefix   [description]
 	 * @return [type]           [description]
 	 */
 	public function reference ( $prefix = '', $duration = '50 minutes' )
 	{
 		# Code here
 		$connection = new MongoClient();
		$db = $connection->selectDb( $this->DB_NAME );

		$id = $prefix . strtoupper(random_string('alnum', 10));

		$db->references->ensureIndex(array('expireAt' => 1), array('expireAfterSeconds' => 0));

		$exists = $db->references->find(array('_id' => $id))->count();

		if ( $exists > 0 )
		{
			# Code here
			$connection->close();
			$this->reference();
		} // end of if statement
		$db->references->insert(array('_id' => $id, 'expireAt' => new MongoDate(strtotime('now +' . $duration))));
		$connection->close();
		
		return $id;
 	} // end of reference

 	/**
 	 * [insert description]
 	 * @param  [type] $data       [description]
 	 * @param  string $collection [description]
 	 * @return [type]             [description]
 	 */
 	public function insert ( $data, $collection = 'b2b' )
 	{
 		# Code here
 		$connection = new MongoClient();
 		$db = $connection->selectDb( $this->DB_NAME );

 		$collection = $db->selectCollection( $collection );

 		$collection->ensureIndex(array('expireAt' => 1), array('expireAfterSeconds' => 0));

 		$collection->insert( $data );
 		$connection->close();

 		return TRUE;
 	} // end of insert

 	/**
 	 * [update description]
 	 * @param  [type] $reference [description]
 	 * @return [type]            [description]
 	 */
 	public function update ( $reference, $data, $collection = 'b2b' )
 	{
 		# Code here
 		$connection = new MongoClient();
 		$db = $connection->selectDb( $this->DB_NAME );

 		$collection = $db->selectCollection( $collection );

 		$collection->update(array(
 			'reference' => $reference
 		), array('$set' => $data) );

 		$connection->close();

 		return TRUE;
 	} // end of update
 }