<?php
set_time_limit(0);
/**
* 
*/

class NotficationModelException extends Exception {}

class Notification_model extends CI_Model
{
    private $CI           = null;
    private $EMAIL_CONFIG = null;

    function __construct()
    {
        parent::__construct();
        $this->EMAIL_CONFIG = json_decode(EMAIL_CONFIG, TRUE);
        $this->CI =& get_instance();
        $this->CI->load->model('log_model');
    }

    /**
     * BOTH
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function recordItem ( $data )
    {
        $connection = new MongoClient();
        
        $db      = $connection->db_instant;
        $hotel   = $db->searchresult->findOne( array( 'id' => $data['hotelId'] ), array( 'hotelDetails' => 1, '_id' => 0 ) );
        
        $db      = $connection->db_system;
        $booking = $db->booking->findOne( array('booking_id' => (int)$data['bookingId']), array( '_id' => 0, 'booking_creation_date' => 1 ) );

        $data['bookingDate']   = date( 'd/m/Y', $booking['booking_creation_date']->sec );
        $data['hotelCode']     = $hotel['hotelDetails']['id'];
        $data['hotelName']     = $hotel['hotelDetails']['hotelname'];
        $data['hotelLocation'] = $hotel['hotelDetails']['address'];

        $db->recordItem->insert( $data );

        $connection->close();
    }

    /**
     * B2B and B2C
     * Send email to the customer when failure in booking
     * @param  [type] $data             [description]
     * @param  [type] $paymentReference [description]
     * @return [type]                   [description]
     */
    public function failureToReserve ( $data )
    {
        $to      = ( (EMAIL_IS_TEST)?EMAIL_TEST:$data['voucher']['email'] );
        $from    = EMAIL_USERNAME;
        $subject = '';
        $message = '';

        $message = $this->load->view( 'template/failedinbookingpayment', $data, TRUE );
        $this->CI->log_model->something( 'failed-booking-template.html', $message, 1, FALSE );
        // $this->load->library('email', $this->EMAIL_CONFIG);
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->from( $from, $subject = $this->nativesession->get('agency_name') );
        $this->email->to( $to );
        // [Agency name] failure on reservation
        $subject = $this->nativesession->get('agency_name') . ' failure on reservation';
        $this->email->subject( $subject );
        $this->email->message( $message );

        generate_DKIM( $to, $from, $subject, $message );

        if( $this->email->send() )
        {
            return array('success' => TRUE, 'message' => 'Your email was successfully delivered');
            $this->CI->log_model->something('log.TR', 'E-mail was successfully send for failure to reserve', 1);
        } else {
            return array('success' => FALSE, 'message' => 'Email was unsuccessfully delivered');
            $this->CI->log_model->something('failureToReserve', $this->email->print_debugger(), 1, FALSE);
        }
    }

    /**
     * B2B and B2C
     * @param  [type] $data [bookingId]
     * @return [type]       [description]
     */
    public function emailPayment ( $data, $isEnet = FALSE )
    {
        // TODO
        // 1. Get the booking consultant. E-mail, Name
        $connection        = new MongoClient();
        $db                = $connection->db_system;
        $booking           = $db->booking->findOne( array( 'booking_id' => (int)$data['bookingId'] ), array('booking_agency_consultant_code' => 1, '_id' => 0, 'booking_creation_date' => 1) );
        $agnecy_consultant = $db->agency_consultants->findOne( array( 'code' => $booking['booking_agency_consultant_code'] ), array( '_id' => 0 ) );

        // Get all items that are being paid on this day
        $aggregate = array(
            array( '$match' => array( 'bookingId' => (int)$data['bookingId'], 'itemId' => array( '$in' =>  $data['itemIds'] ) ) ),
            array( '$group' => array(
                '_id'   => '$bookingId',
                'items' => array(
                    '$addToSet' => array(
                        'price'     => '$itemCostings.totalDueAmt',
                        'name'      => '$itemName'
                    )
                ),
                'total' => array(
                    '$sum' => '$itemCostings.totalDueAmt'
                )
            ) )
        );
        $items = $db->items->aggregate( $aggregate );

        $voucher = $db->item_vouchers->find( array('itemId' => $data['itemIds'][0] ), array( '_id' => 0 ) )->sort( array( 'voucherId' => -1 ) );
        $voucher = iterator_to_array($voucher);
        $connection->close();
        
        if ( count( $voucher ) == 0 )
        {
            # Code here
            $voucher['voucher_reference'] = '0000';
        } else {
            $voucher = $voucher[0];
        } // end of if statement


        $to      = ((EMAIL_IS_TEST)? EMAIL_TEST: $data['voucherEmail']);
        $from    = EMAIL_USERNAME;
        $subject = '';
        $message = '';


        $data['isEnet']     = $isEnet;
        $data['items']      = $items['result'][0];
        $data['created_at'] = date('F d Y', $booking['booking_creation_date']->sec);
        $data['consultant'] = $agnecy_consultant['fname'] . ( (!empty($agnecy_consultant['mname']))? ' ' . $agnecy_consultant['mname'] . '. ': ' ' ) . $agnecy_consultant['lname'];
        $message = $this->load->view( 'template/payment', $data, TRUE );
        $this->CI->log_model->something( 'email-payment-template.html', $message, 1, FALSE );


        # $this->load->library('email', $this->EMAIL_CONFIG);
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->from( $from, $subject = $this->nativesession->get('agency_name'));
        $this->email->to( $to );
        
        $subject = $this->nativesession->get('agency_name') . ' payment receieved';
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->attach( ADMIN_SYSTEM_DOCS . 'voucher-' . $voucher['voucher_reference'] . '.pdf' );

        generate_DKIM( $to, $from, $subject, $message );

        if( $this->email->send() )
        {
            return array('success' => TRUE, 'message' => 'Your email was successfully delivered');
            $this->CI->log_model->something('log.TR', 'E-mail was successfully send for payment', 1);
        } else {
            return array('success' => FALSE, 'message' => 'Email was unsuccessfully delivered');
            $this->CI->log_model->something('emailPayment', $this->email->print_debugger(), 1, FALSE);
        }
    }

    /**
     * B2B and B2C
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function log ( $array )
    {
        $data = array(
            'notificationLoginReference' => '',
            'voucherName'                => '',
            'voucherEmail'               => '',
            'amount'                     => 0,
            'description'                => '',
            
            'itemIds'   => '',
            'receiptId' => 0,
            'bookingId' => '',

            'isSearching'  => 0,
            'isEnett'      => 0,
            'isPayment'    => 0,
            'isCreditCard' => 0,
            'isBooked'     => 0,
            'payment_type' => 0,

            'subSystem'     => '',
            'system'        => SYSTEM_TYPE,
            'xml_reference' => ''
        );
        #print_r($data);
        #die();
        
        foreach ( $array as $key => $value )
        {
            # code...
            if ( isset( $data[ $key ] ) )
            {
                # Code here
                $data[ $key ] = $value;
            } // end of if statement
        }
        
        $connection = new MongoClient();
        $db = $connection->db_system;

        $data['dateCreated']            = new MongoDate(strtotime('now'));
        $data['bookingConsultant_code'] = '';
        $data['bookingConsultant_name'] = '';


        if( $data['bookingId'] > 0 )
        {
            $booking = $db->booking->findOne( array('booking_id' => (int)$data['bookingId'] ) );
            $data['bookingId']              = (int)$data['bookingId'];
            $data['bookingConsultant_code'] = $booking['booking_consultant_code'];
            $data['bookingConsultant_name'] = $booking['booking_consultant_name'];
        }

        $db->notification_payment->insert( $data );

        $connection->close();
    }

    /**
     * B2B and B2C
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function logOnStatus ( $data )
    {
        $connection = new MongoClient();
        $db         = $connection->db_system;
        
        $agency     = $db->agency->findOne( array( 'agency_code' => $data['agency_code'] ) );
        $consultant = $db->agency_consultants->findOne( array( '_id' => $data['user_id'] ) );

        $id = get_next_id('notification_login');

        $this->session->set_userdata('loginNotificationReference', $data['agency_code'] . '-' . $id);
        $db->notification_login->insert(array(
            'notification_id'       => (int)$id,
            'isLogin'               => $data['is_login'],
            'agencyCode'            => $data['agency_code'],
            'agencyName'            => $agency['agency_name'],
            'dateCreated'           => new MongoDate(strtotime('now')),
            'agencyConsultant_code' => $consultant['code'],
            'agencyConsultant_name' => $consultant['fname'] . '' . ((!empty($consultant['lname']))? ' ' . $consultant['lname']: '')
        ));

        $connection->close();
    }

    /**
     * [bookingEmail description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function bookingEmail ( $data )
    {
        # Code here
        $_sending          = array();
        $_sending['email'] = array(
            'content' => 'template/booking-info',
            // 'content' => 'template/payment/booked',
            'to'      => ( EMAIL_IS_TEST?EMAIL_TEST: EMAIL_USERNAME ),
            'from'    => EMAIL_USERNAME,
            'name'    => EMAIL_NAME,
            'subject' => 'Agent online booking',
            'CC'      => EMAIL_CC
        );
        $_sending['information'] = array(
            'agencyName' => $this->CI->nativesession->get('agency_name')
        );

        $_sending['information'] = array_merge( $_sending['information'], $data );
        $this->sendMail( $_sending );
    } // end of bookingEmail

    /**
     * [sendMail description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function sendMail ( $data )
    {
        # Code here
        $_to       = '';
        $_from     = '';
        $_subject  = '';
        $_message  = '';
        $_fromName = '';

        if ( ! isset( $data['email'] ) )
        {
            # Code here
            response(array('success' => FALSE, 'message' => 'E-mail information is not defined'));
        } // end of if statement
        /*if ( $useDefault )
        {
            # Code here
            $_to = EMAIL_TEST;
            $_from = EMAIL_USERNAME;
            $_subject = 'No Subject';
            $_message = 'Test';
        } // end of if statement*/

        $__email = $data['email'];
        $__data  = $data['information'];

        $_to       = $__email['to'];
        $_from     = EMAIL_IS_TEST?EMAIL_TEST: $__email['from'];
        $_subject  = $__email['subject'];
        $_message  = $this->load->view($__email['content'], $__data, TRUE);
        $_fromName = $__email['name'];



        $this->load->library('email');
        // $this->load->library('email', $this->EMAIL_CONFIG);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        #$this->email->set_mailtype("html");
        $this->email->from( $_from, $_fromName );
        $this->email->reply_to( EMAIL_REPLY );
        $this->email->to( $_to );

        if ( EMAIL_IS_TEST === FALSE )
        {
            # Code here
            if ( isset( $__email['CC'] ) )
            {
                # Code here
                $this->email->cc( $__email['CC'] );
            } // end of if statement
        } // end of if statement
        
        #$subject = $this->nativesession->get('agency_name') . ' payment receieved';
        $this->email->subject( $_subject );
        $this->email->message($_message);
        if ( isset( $__email['attachments'] ) && ! empty( $__email['attachments'] ) )
        {
            # Code here
            foreach ( $__email['attachments'] as $key => $attachment )
            {
                # code...
                $this->email->attach( $attachment );
            }
        } // end of if statement

        generate_DKIM( $_to, $_from, $_subject, $_message );
        if( $this->email->send() )
        {
            return array('success' => TRUE, 'message' => 'Your email was successfully delivered');
        } else {
            log_message('error', 'Failed to send email!');
            log_message('error', $this->email->print_debugger());
            # throw new NotficationModelException();
            return array('success' => FALSE, 'message' => 'Email was unsuccessfully delivered');
        }
    } // end of sendMail
}