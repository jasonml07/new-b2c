<?php

class Tour_model extends CI_Model
{
  const BOOK = 'book';
  const BOOK_PRODUCT = 'book_product';
  const BOOK_TOUR = 'book_tour';
  const BOOK_BEDS = 'book_beds';
  const BOOK_EXTRA_BILLS = 'book_extra_bills';
  const BOOK_GUESTS = 'book_guests';
  const BOOK_FLIGHT = 'book_flight';

  private $CONNECTION = null;
  private $DATABASE = null;

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('date');
  }
  /**
   * [startConnection description]
   * @return [type] [description]
   */
  public function startConnection()
  {
    if(!$this->CONNECTION) {
      $this->CONNECTION = new MongoClient();
      $this->DATABASE = $this->CONNECTION->tour_db;
    }
  }
  /**
   * [closeConnection description]
   * @return [type] [description]
   */
  public function closeConnection()
  {
    $this->CONNECTION->close();
    $this->CONNECTION = null;
  }
  /**
   * [getLastBooked description]
   * @param  boolean $closeConnection [description]
   * @return [type]                   [description]
   */
  public function getLastBooked($closeConnection = true)
  {
    $this->startConnection();
    $lastBook = $this->DATABASE->selectCollection(self::BOOK)->find(['reference' => ['$exists' => true]])->sort(['$natural' => -1])->limit(1);
    $lastBook = iterator_to_array($lastBook);
    if(count($lastBook) > 0) {
      $lastBook = $lastBook[0];
    } else {
      $lastBook = null;
    }
    if($closeConnection) {
      $this->closeConnection();
    }
    return $lastBook;
  }
  /**
   * [bookingRefExists description]
   * @param  [type] $ref [description]
   * @return [type]      [description]
   */
  public function bookingRefExists($ref, $closeConnection = true)
  {
    $this->startConnection();
    $exists = $this->DATABASE
      ->selectCollection(self::BOOK)
      ->findOne(['reference' => $ref], ['_id' => 0]);
    if($closeConnection) {
      $this->closeConnection();
    }

    return !!$exists;
  }
  /**
   * [gen_book_referehce description]
   * @return [type] [description]
   */
  public function getBookRef()
  {
    $this->startConnection();
    $reference = 'REF'.mdate('%Y%m%d');
    $incrementRef = $reference.random_string('numeric', 6);
    $exists = $this->bookingRefExists($incrementRef, false);
    while($exists) {
      $incrementRef = $reference.random_string('numeric', 6);
      $exists = $this->bookingRefExists($incrementRef, false);
    }
    $this->closeConnection();
    return $incrementRef;
  }
  /**
   * [saveBooking description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function saveBooking($data)
  {
    $this->startConnection();
    $this->DATABASE
      ->selectCollection(self::BOOK)
      ->insert($data);
    $this->closeConnection();
    return $data;
  }
  /**
   * [insert_book_product description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function saveBookProduct($data)
  {
    $this->startConnection();
    $this->DATABASE
      ->selectCollection(self::BOOK_PRODUCT)
      ->insert($data);
    $this->closeConnection();
    return $data;
  }
  /**
   * [saveBookTour description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function saveBookTour($data)
  {
    $this->startConnection();
    $tour = $this->DATABASE
      ->selectCollection(self::BOOK_TOUR)
      ->insert($data);
    $this->closeConnection();
    return $tour;
  }
  /**
   * [saveBookBeds description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function saveBookBeds($data)
  {
    $this->startConnection();
    $collection = $this->DATABASE
      ->selectCollection(self::BOOK_BEDS);
    if(is_array($data)) {
      foreach($data as $key => $item) {
        $collection->insert($item);
      }
    } else {
      $collection->insert($data);
    }
    $this->closeConnection();
    return $data;
  }
  /**
   * [saveBookExtraBills description]
   * @param  [type] $extraBills [description]
   * @return [type]             [description]
   */
  public function saveBookExtraBills($extraBills)
  {
    $this->startConnection();
    $collection = $this->DATABASE
      ->selectCollection(self::BOOK_EXTRA_BILLS);
    if(is_array($extraBills)) {
      foreach($extraBills as $key => $item) {
        $collection->insert($item);
      }
    } else {
      $collection->insert($extraBills);
    }
    $this->closeConnection();
    return $extraBills;
  }
  /**
   * [saveBookGuests description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function saveBookGuests($data)
  {
    $this->startConnection();
    $collection = $this->DATABASE
      ->selectCollection(self::BOOK_GUESTS);
    if(is_array($data)) {
      foreach($data as $key => $item) {
        $collection->insert($item);
      }
    } else {
      $collection->insert($data);
    }
    $this->closeConnection();
    return $data;
  }
  /**
   * [saveBookFlight description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function saveBookFlight($data)
  {
    $this->startConnection();
    $collection = $this->DATABASE
      ->selectCollection(self::BOOK_FLIGHT);
    $collection->insert($data);
    $this->closeConnection();
    return $data;
  }
  /**
   * [getBookingDetails description]
   * @param  [type] $reference [description]
   * @return [type]            [description]
   */
  public function getBookingDetails($reference)
  {
    $this->startConnection();
    $booking = $this->DATABASE->selectCollection(self::BOOK)
      ->findOne(['reference' => $reference]);
    if(!!$booking) {
      $booking['product'] = $this->DATABASE->selectCollection(self::BOOK_PRODUCT)
        ->findOne(['bookedId' => $booking['_id']], ['_id' => 0, 'bookedId' => 0]);
      
      $booking['tour'] = $this->DATABASE->selectCollection(self::BOOK_TOUR)
        ->findOne(['bookedId' => $booking['_id']], ['_id' => 0, 'bookedId' => 0]);
      
      $booking['flight'] = $this->DATABASE->selectCollection(self::BOOK_FLIGHT)
        ->findOne(['bookedId' => $booking['_id']], ['_id' => 0, 'bookedId' => 0]);
      
      $booking['beds'] = $this->DATABASE->selectCollection(self::BOOK_BEDS)
        ->find(['bookedId' => $booking['_id']], ['_id' => 0, 'bookedId' => 0]);
      $booking['beds'] = iterator_to_array($booking['beds']);
      
      $booking['extrabills'] = $this->DATABASE->selectCollection(self::BOOK_EXTRA_BILLS)
        ->find(['bookedId' => $booking['_id']], ['_id' => 0, 'bookedId' => 0]);
      $booking['extrabills'] = iterator_to_array($booking['extrabills']);
      
      $booking['guests'] = $this->DATABASE->selectCollection(self::BOOK_GUESTS)
        ->find(['bookedId' => $booking['_id']], ['_id' => 0, 'bookedId' => 0]);
      $booking['guests'] = iterator_to_array($booking['guests']);
      unset($booking['_id']);
    }
    $this->closeConnection();
    return $booking;
  }
}