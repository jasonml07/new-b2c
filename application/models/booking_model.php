<?php
	class Booking_model extends CI_Model {

		
		public function __construct() {
        	parent::__construct();
    	}

    	/**
    	 * This will get first and last policy only
    	 * @param  [type] $cancellation [description]
    	 * @return [type]               [description]
    	 */
    	public function parseCancellation ( $cancellation )
    	{
    		$policies = array();
			// TODO
			// Search for Fully-refundable
			// Search for non-refundable if this exist, eliminate other policy
			// If the non-refundable does not exist sort all policies base on the endTime
			// Then get the last policy and change it to non-refundable
			
			$fullyRefundableKey = array_search('fully-refundable', array_map(array($this, 'searchOnArray'), $cancellation));
			$nonRefundables = array_filter($cancellation, function ($item) {
				return ( $item['type'] != 'fully-refundable' );
			});

			if( $fullyRefundableKey > -1 )
			{
				$cancellation[ $fullyRefundableKey ]['startTime']        = 'Todays date';
				$cancellation[ $fullyRefundableKey ]['endTime']          = date('Y-m-d', strtotime($cancellation[ $fullyRefundableKey ]['endTime'] . ' -1 day'));
				$cancellation[ $fullyRefundableKey ]['endTimeFormatted'] = date('F d, Y', strtotime($cancellation[ $fullyRefundableKey ]['endTime']));
				array_push($policies, $cancellation[ $fullyRefundableKey ]);
			}

			if( count($nonRefundables) > 0 )
			{
				// If the fully refundable exist
				// Set the startTime to the endTime of fully-refundable
				// When the fully refundable does not exist
				// * Check the endTime of non-refundable
				//   When the time is less than by the day today
				//   then the startTime will be set to Todays
				//   else the endTime will be the date today
				
				usort($nonRefundables, array($this, 'sortCancellationEndtime'));
				$nonRefundable            = end($nonRefundables);
				$nonRefundable['type']    = 'non-refundable';
				$nonRefundable['endTime'] = date('Y-m-d', strtotime($nonRefundable['endTime'] . ' -1 day'));
				$nonRefundable['endTimeFormatted'] = date('F d, Y', strtotime($nonRefundable['endTime']));

				if( $fullyRefundableKey > -1 )
				{
					// Change the start time of non-refundable to the 
					$nonRefundable['startTime'] = $cancellation[ $fullyRefundableKey ]['endTimeFormatted'];
				} else {
					if( strtotime($nonRefundable['endTime']) > strtotime('now') )
					{
						$nonRefundable['startTime'] = 'Todays date';
					} else {
						$nonRefundable['startTime']        = 'this day';
						$nonRefundable['endTime']          = date('F d, Y', strtotime('now'));
						$nonRefundable['endTimeFormatted'] = null;
					}
				}

				array_push($policies, $nonRefundable);
			}

			return $policies;
		}

		/**
		 * [searchOnArray description]
		 * @param  [type] $item [description]
		 * @return [type]       [description]
		 */
		private function searchOnArray ( $item )
		{
			return $item['type'];
		}

		/**
		 * Ascending Order
		 * @param  [type] $a [description]
		 * @param  [type] $b [description]
		 * @return [type]    [description]
		 */
		private function sortCancellationEndtime ( $a, $b )
		{
			$t1 = strtotime($a['endTime']);
			$t2 = strtotime($b['endTime']);

			return ($t1 < $t2)?-1: 1;
		}

		/**
		 * [eNetPayment description]
		 * @param  [type] $bookingId [description]
		 * @param  [type] $data      [description]
		 * @return [type]            [description]
		 */
    	public function eNetPayment ( $bookingId, $data )
    	{
    		$receipt = array();

    		$receipt['receiptData'] = array(
				"receiptTypeCode"            => "EFTN",
				"receiptTypeName"            => "EFT E-Nett",
				"receiptDescription"         => (isset($data['description'])?$data['description']:"E-Nett"),
				"receiptReferenceNumber"     => $data['reference'],
				"receiptChequeNumber"        => "",
				"receiptABAnumber"           => "",
				"receiptAccountChequeNumber" => "",
				"receiptDrawersName"         => "",
				"receiptBankCode"            => "",
				"receiptBankName"            => "",
				"receiptCCTypeCode"          => "",
				"receiptCCTypeName"          => "",
				"receiptCCNumber"            => "",
				"receiptCCExpiryDate"        => "",
				"receiptCCSecurityCode"      => "",
				"receiptDisplayAmout"        => 0,
				"receiptAmout"               => 0,
				"receiptDepositedAmout"      => $data['amount'],
				"receiptRefundAmout"         => $data['amount'],
				"receiptDate"                => $data['receipt_date']
				// "receiptDate"                => date('d/m/Y', strtotime('now'))
    		);
    		// Get all unpaid Item
    		$receipt['receiptItemData'] = array();

			$connection = new MongoClient();
			$db         = $connection->db_system;

    		$unpaidItems = $db->items->find(
    			array(  'bookingId' => (int)$bookingId, '$or' => array( array('itemIsPaid' => 0), array('itemIsReceipt' => 0) ) ),
    			array( '_id' => 0, 'itemId' => 1 )
    		);

    		$totalDueAmt = $db->items->aggregate(array(
    			array( '$match' => array( 'bookingId' => (int)$bookingId, '$or' => array( array('itemIsPaid' => 0), array('itemIsReceipt' => 0) ) ) ),
    			array( '$group' => array( '_id' => null, 'count' => array( '$sum' => 1 ), 'dueAmount' => array( '$sum' => '$itemCostings.totalDueAmt' ) ) )
    		));

    		$connection->close();

    		$receipt['receiptData']['receiptDisplayAmout'] = $totalDueAmt['result'][0]['dueAmount'];

    		$unpaidItems = $this->getUnpaidItems( $bookingId );
    		$receipt['receiptItemData'] = $this->getItemsAndGuests( $unpaidItems );
    		return $receipt;
    	}

    	/**
    	 * [getUnpaidItems description]
    	 * @param  [type] $bookingId [description]
    	 * @return [type]            [description]
    	 */
    	public function getUnpaidItems ( $bookingId )
    	{
    		$connection = new MongoClient();
    		$db = $connection->db_system;

    		$unpaidItems = $db->items->find(
    			array(  'bookingId' => (int)$bookingId, '$or' => array( array('itemIsPaid' => 0), array('itemIsReceipt' => 0) ) ),
    			array( '_id' => 0, 'itemId' => 1 )
    		);

    		$connection->close();

    		$unpaidItems = iterator_to_array( $unpaidItems );
    		$unpaidItems = array_map( function ( $item )  { return $item['itemId']; }, $unpaidItems);

    		return $unpaidItems;
    	}

    	/**
    	 * [getItemAllocated description]
    	 * @param  [type] $itemIds [description]
    	 * @return [type]          [description]
    	 */
		public function getItemAllocated ( $itemIds )
		{
			$connection = new MongoClient();
			$db         = $connection->db_system;

    		$itemAllocated = $db->item_allocated->find(
    			array( 'item_id' => array( '$in' => $itemIds ), 'is_active' => 0 )
    		);
    		$connection->close();

    		$allocatedAmmount = array();
    		$itemAllocated = iterator_to_array( $itemAllocated );
    		foreach ( $itemAllocated as $key => $iA )
    		{
    			if( array_key_exists( $iA['item_id'], $allocatedAmmount ) )
    			{
    				array_push($allocatedAmmount[ $iA['item_id'] ], $iA);
    			} else {
    				$allocatedAmmount[ $iA['item_id'] ] = array( $iA );
    			}
    		}

    		foreach ( $allocatedAmmount as $key => $aA )
    		{
    			$allocated = $aA;
    			$totalAmount = 0;
    			foreach ($allocated as $key1 => $amount)
    			{
    				$totalAmount += (float)$amount['amount_paid'];
    			}
    			$allocatedAmmount[ $key ] = array(
    				'itemOutStandingAllocated' => (float) $allocated[0]['itemCostings']['totalDueAmt'] - $totalAmount,
    				'itemTotalAmountAllocated' => $totalAmount
    			);
    		}

    		return $allocatedAmmount;
    	}

    	/**
    	 * [getItemsAndGuests description]
    	 * @param  [type]  $ids          [description]
    	 * @param  boolean $isForReceipt [description]
    	 * @return [type]                [description]
    	 */
    	public function getItemsAndGuests ( $ids, $isForReceipt = TRUE )
    	{
    		$connection = new MongoClient();
    		$db = $connection->db_system;
    		$guestKey = 'itemGuest';

    		$itemsIncludes = array(
				'_id'             => 0,
				'itemId'          => 1,
				'itemCode'        => 1,
				'itemName'        => 1,
				'itemCostings'    => 1,
				'itemServiceCode' => 1,
				'itemServiceName' => 1,
				'itemStartDate'   => 1,
				'itemEndDate'     => 1
			);

			if( ! $isForReceipt )
			{
				$itemsIncludes = array( '_id' => 0 );
				$guestKey .= 's';
			}

			$items  = $db->items->find( array( 'itemId' => array( '$in' => $ids ) ), $itemsIncludes );
			$guests = $db->item_guest->find( array( 'item_id' => array( '$in' => $ids ) ), array('_id' => 0) );
			$items  = iterator_to_array( $items );
			$guests = iterator_to_array( $guests );
			$connection->close();

			$guestItems = array();

			foreach ( $guests as $key => $guest )
			{
				if( array_key_exists($guest['item_id'], $guestItems) )
				{
					array_push($guestItems[ $guest['item_id'] ], $guest);
				} else {
					$guestItems[ $guest['item_id'] ] = array( $guest );
				}
			}

			foreach ( $items as $key => $item )
			{
				$item[$guestKey]     = array();
				$item['itemID']      = $item['itemId'];
				$item['itemService'] = $item['itemServiceName'];


				if( isset($guestItems[ $item['itemID'] ]) &&  count( $guestItems[ $item['itemID'] ] ) > 0 )
				{
					$item[$guestKey] = $guestItems[ $item['itemID'] ];
				}

				$items[ $key ] = $item;
			}
			return $items;

    	}

    	/**
    	 * [getItemsAndGuest description]
    	 * @param  [type] $itemId [description]
    	 * @return [type]         [description]
    	 */
    	public function getItemsAndGuest ( $itemId )
    	{
    		$connection = new MongoClient();
    		$db = $connection->db_system;

    		$itemsIncludes = array(
				'_id'             => 0,
				'itemCode'        => 1,
				'itemName'        => 1,
				'itemCostings'    => 1,
				'itemServiceCode' => 1,
				'itemServiceName' => 1,
				'itemStartDate'   => 1,
				'itemEndDate'     => 1
			);
			$items                = $db->items->findOne( array('itemId' => (int)$itemId), $itemsIncludes );
			$itemsGuest           = $db->item_guest->find( array('item_id' => (int)$itemId), array('_id' => 0) );
			$itemsGuest           = iterator_to_array($itemsGuest);
			$items['itemGuest']   = $itemsGuest;
			$items['itemID']      = (int)$itemId;
			$items['itemService'] = $items['itemServiceName'];

    		$connection->close();

    		return $items;
    	}

    	public function saveFormData ( $post )
    	{
    		$connection = new MongoClient();
    		$db = $connection->db_instant;

    		$db->payment_form->drop();
    		$db->payment_form->insert( $post );

    		$connection->close();
    	}

    	public function getFormdata ()
    	{
    		$connection = new MongoClient();
    		$db = $connection->db_instant;

    		$form = iterator_to_array($db->payment_form->find(array(), array('_id' => 0)));
    		$connection->close();

    		return $form[0];
    	}

    	/**
    	 * [checkBooking description]
    	 * @param  [type] $bookingID [description]
    	 * @return [type]            [description]
    	 */
    	public function checkBooking ( $bookingID )
    	{

    		if ( ! $bookingID )
    		{
    			return FALSE;
    		}

    		$connection = New MongoClient();
    		$db = $connection->db_system;

    		$count = $db->booking->find(array('booking_id'=> intval($bookingID)))->count();
    		$connection->close();

    		return ($count > 0);
    	}

    	public function newBooking (
    		$agencyCode,
    		$isMultiDayTours = FALSE,
    		$book_id=FALSE,
    		$type='hotel',
    		$isPaid=1,
    		$isCancelled=0,
    		$isRefundable=0,
    		$paymentDetails=[],
    		$bookingDatas=[]
    	)
    	{
    		$connection = new MongoClient();
    		$db = $connection->db_system;

    		$agency = $db->agency->findOne(array('agency_code' => $agencyCode), array('_id' => 0, 'agency_code' => 0));
    		// $consoltant = $db->agency_consultants->findOne(array('_id' => new MongoId($user_id)), array('_id' => 0));
    		$_bookingId = 0;
    		if ($book_id) {
    			$_bookingId=(int)$book_id;
    		} else {
    			while ( TRUE ) {
	    			# code...
	    			$_bookingId = get_next_id('booking');
	    			$book = $db->booking->findOne( array('booking_id' => (int) $_bookingId), array('_id' => 0) );

	    			if ( empty( $book ) || is_null( $book ) )
	    			{
	    				# Code here
	    				break;
	    			} // end of if statement
	    		}
    		}

			$booking                                = array();
			$booking['booking_id']                  = $_bookingId;
			$booking['booking_consultant_code']     = 'B2C-SYSTEM';
			$booking['booking_consultant_name']     = 'B2C-SYSTEM';
			$booking['booking_agency_code']         = $agencyCode;
			$booking['booking_agency_name']         = $agency['agency_name'];
			$booking['booking_agency_state']        = $agency['city'];
			$booking['booking_creation_date']       = new MongoDate(strtotime("now"));
			$booking['booking_source']              = 'Agent Online Booking';
			$booking['booking_status']              = null;
			$booking['booking_status_message']      = '';
			$booking['booking_final_voucher_date']  = '';
			$booking['booking_final_payment_date']  = '';
			$booking['booking_services_start_date'] = '';
			$booking['booking_services_end_date']   = '';
			$booking['isMultiDayTours']             = $isMultiDayTours;
			// $booking['booking_agency_consultant_code'] = $consoltant['code'];
			// $booking['booking_agency_consultant_name'] = $consoltant['fname'] . ' ' . $consoltant['lname'];
			$booking['booking_agency_consultant_code'] = 'CONSULTANT_CODE';
			$booking['booking_agency_consultant_name'] = 'Customer';
			$booking['is_from_b2b'] = 1;
			$booking['is_paid'] = $isPaid;
			$booking['bookType'] = $type;
			$booking['is_cancelled'] = $isCancelled;
			$booking['is_refundable'] = $isRefundable;
			$booking['paymentDetails'] = $paymentDetails;
			$booking['bookingDatas'] = $bookingDatas;

			$SESSION = $this->nativesession->get('ETLOGIN');
    		if ( ! empty( $SESSION ) && is_array( $SESSION ) )
    		{
    			# Code here
    			$consultant = $db->agency_consultants->findOne(array('_id' => new MongoId($SESSION['ID'])), array('_id' => 0));
    		$booking['booking_agency_code']         = $consultant['code'];
				$booking['booking_consultant_code']        = $consultant['code'];
				$booking['booking_consultant_name']        = $consultant['fname'] . ' ' . $consultant['lname'];
				$booking['booking_agency_consultant_code'] = $consultant['code'];
				$booking['booking_agency_consultant_name'] = $booking['booking_consultant_name'];
    		} // end of if statement

    		$db->booking->update(array('booking_id'=>$_bookingId),$booking,array('upsert'=>TRUE));
			$connection->close();

			return $booking['booking_id'];
    	}

    	/**
    	 * [allocationFields description]
    	 * @param  [type] $bookingId       [description]
    	 * @param  [type] $receiptId       [description]
    	 * @param  [type] $itemId          [description]
    	 * @param  [type] $amountToDeposit [description]
    	 * @return [type]                  [description]
    	 */
    	public function allocationFields ( $bookingId, $receiptId, $itemId, $amountToDeposit )
    	{
    		$fields = array(
				'action'          => 'createAllocation',
				'allocationDatas' => '',
				'bookingID'       => $bookingId
    		);
    		// Get the receipt
    		$connection = new MongoClient();
    		$db = $connection->db_system;

    		$includes = array(
				"receiptId"              => 1,
				"receipt_reference"      => 1,
				"receiptTypeName"        => 1,
				"receiptDescription"     => 1,
				"receiptReferenceNumber" => 1,
				"receiptChequeNumber"    => 1,
				"receiptABAnumber"       => 1,
				"receiptDrawersName"     => 1,
				"receiptBankCode"        => 1,
				"receiptBankName"        => 1,
				"receiptCCTypeCode"      => 1,
				"receiptCCTypeName"      => 1,
				"receiptCCNumber"        => 1,
				"receiptCCExpiryDate"    => 1,
				"receiptCCSecurityCode"  => 1,
				"receiptDisplayAmout"    => 1,
				"receiptAmout"           => 1,
				"receiptDepositedAmout"  => 1,
				"receiptRefundAmout"     => 1,
				"documentId"             => 1,
				"is_active"              => 1,
				"is_refunded"            => 1,
				"is_allocated"           => 1,
				"created_datetime"       => 1,
				"receipt_datetime"       => 1,
				"_id"                    => 0

    		);
    		$receiptData = array();
			$receiptData['receiptData']               = $db->receipt->findOne( array( 'receiptId' => intval($receiptId) ), $includes );

			$receiptData['receiptId']                 = $receiptId;
			$receiptData['receiptDepositedAmout']     = (float)$amountToDeposit;
			$receiptData['allocate_amout_to_refund']  = 0;
			$receiptData['allocate_amout_to_receipt'] = 0;
			$receiptData['allocate_datetime']         = date('d/m/y', strtotime('now'));
			$receiptData['allocateAmount']            = (float)$amountToDeposit;


			$itemData = $db->items->findOne( array('itemId' => intval($itemId)), array('_id' => 0) );
			$itemData['itemGuests'] = $db->item_guest->find( array('item_id' => intval($itemId)) );
			$itemData['itemGuests'] = iterator_to_array( $itemData['itemGuests'] );

			// get all allocated items

			$allocated = $db->item_allocated->find( array('item_id' => intval($itemId), 'is_active' => 0) );
			$allocated = iterator_to_array( $allocated );

			$itemData['itemOutStandingAllocated'] = (float)$itemData['itemCostings']['totalDueAmt'];
			$itemData['itemTotalAmountAllocated'] = 0;
			$itemData['amountAlloc'] = (float)$amountToDeposit;


			if( count($allocated) > 0 )
			{
				foreach ( $allocated as $key => $al )
				{
					$itemData['itemTotalAmountAllocated'] += (float)$al['amount_paid'];
				}

				$itemData['itemOutStandingAllocated'] -= $itemData['itemTotalAmountAllocated'];
			}

			$receiptData['itemData'] = array($itemData);

			$fields['allocationDatas'] = json_encode($receiptData);
			return $fields;
    	}

    	/**
    	 * [parseReservationItemFields description]
    	 * @param  [type] $reservation_item [description]
    	 * @param  [type] $booking_id       [description]
    	 * @param  [type] $reservationId    [description]
    	 * @return [type]                   [description]
    	 */
    	public function parseReservationItemFields ( $reservation_item, $booking_id, $reservationId )
    	{

			$item = array();
			// Must get the Supplier Code
			
			// TODO
			// 1. Get the city
			// 2. Get the Hotel Details
			// 3. Must return the booking ID

			$item['bookingId']            = $booking_id;
			$item['itemId']               = 'New';
			$item['itemName']             = $reservation_item['hotelDetails']['hotelname'];
			$item['itemCreatedType']      = 'DB';
			$item['itemRatings']          = $reservation_item['hotelDetails']['rating'];
			$item['itemPropertyCategory'] = $reservation_item['hotelDetails']['propertytype'];
			$item['itemSeq']              = 0;
			$item['itemLogsReference']    = $this->nativesession->get( SYSTEM_TYPE . 'search_time');
			$item['systemType']           = SYSTEM_TYPE;

			if( $reservation_item['isBillable'] == 1 )
			{
				$item['itemSupplierCode'] = DEFAULT_SUPPLIER_CODE;
				$item['itemSupplierName'] = DEFAULT_SUPPLIER_NAME;
			} else {
				$suppliers = json_decode(SUPPLIERS, TRUE);

				if( ! array_key_exists(strtolower($reservation_item['provider']), $suppliers) ) {
					return FALSE;
				}
				
				$item['itemSupplierCode'] = $suppliers[ strtolower($reservation_item['provider']) ]['code'];
				$item['itemSupplierName'] = $suppliers[ strtolower($reservation_item['provider']) ]['name'];
			}

			if( is_null( $item['itemSupplierName'] ) )
			{
				$item['itemSupplierName'] = $reservation_item['provider'];
			}

			$item['itemCode']                     = $item['itemSupplierCode'] . "-" . $reservation_item['hotelDetails']['id'];
			$item['itemServiceCode']              = 'ACC';
			$item['itemServiceName']              = 'Accommodation';
			$item['itemSumarryDescription']       = 'Online Booking Provider: '. $reservation_item['provider'];
			// List of Board Types and names of rooms
			$item['itemInclusionDescription']     = $reservation_item['roomNamed'];

			$item['itemConditionsDescription']    = $reservation_item['remarks'];
			$item['itemStatus']                   = 'Quote';
			$item['itemSuplierConfirmation']      = $reservation_item['reference'];
			$item['itemSuplierOtherConfirmation'] = $reservationId;
			// This should be a vouchers city
			$item['itemFromCountry']              = $reservation_item['city']['countryname'];
			$item['itemFromCity']                 = $reservation_item['city']['cityname'];
			$item['itemFromAddress']              = $reservation_item['hotelDetails']['address'];

			$endDate   = explode("/", $reservation_item['end_date']);
			$startDate = explode("/", $reservation_item['start_date']);
			$diff      = strtotime($startDate[2]."-".$startDate[0]."-".$startDate[1]) - strtotime($endDate[2]."-".$endDate[0]."-".$endDate[1]);
			$range     = $diff/86400;
			// $item['itemStartDate']                = new MongoDate(strtotime($startDate[2]."-".$startDate[0]."-".$startDate[1]." 12:00:00"));
			// $item['itemEndDate']                  = new MongoDate(strtotime($endDate[2]."-".$endDate[0]."-".$endDate[1]." 12:00:00"));
			$item['itemStartDate']                = $startDate[1]."/".$startDate[0]."/".$startDate[2];
			$item['itemEndDate']                  = $endDate[1]."/".$endDate[0]."/".$endDate[2];
			$item['itemStartTime']                = '12:00:00';
			$item['itemEndTime']                  = '12:00:00';
			$item['itemNumDays']                  = ($range < 0)?($range * -1): $range;
			$item['itemNumNights']                = (($range-1) < 0)?(($range-1) * -1): ($range-1);
			$item['itemIsLeadPax']                = 1;
			$item['itemIsFullPax']                = 1;
			$item['itemIsVoucherable']            = 1;
			$item['itemIsAutoInvoice']            = 1;
			$item['itemIsInvoicable']             = 1;
			$item['itemIsIterary']                = 1;
			$item['itemAdultMin']                 = 1;
			$item['itemAdultMax']                 = intval($reservation_item['totalPax']['adults']);
			$item['itemChildMax']                 = intval($reservation_item['totalPax']['childrens']);
			$item['itemIsPaxAllocated']           = 1;
			$item['itemIsLive']                   = 1;
			$item['itemCancellation']             = $reservation_item['cancellation'];

			$item['itemCostings']['discount_amt']      = $reservation_item['calculation']['calculation']['discount_amt'];
			
			$item['itemCostings']['fromCurrency']      = $reservation_item['calculation']['calculation']['old_currency'];
			$item['itemCostings']['fromRate']          = 1;
			$item['itemCostings']['toCurrency']        = $reservation_item['calculation']['calculation']['currency'];
			$item['itemCostings']['toRate']            = $reservation_item['calculation']['calculation']['rateTo'];;
			$item['itemCostings']['originalAmount']    = $reservation_item['calculation']['calculation']['originalPrice'];
			$item['itemCostings']['orginalNetAmt']     = $reservation_item['calculation']['calculation']['converterdPrice'];
			$item['itemCostings']['netAmountInAgency'] = $reservation_item['calculation']['calculation']['net_amount_agency'];
			$item['itemCostings']['markupInAmount']    = $reservation_item['calculation']['calculation']['markup_amount'];
			$item['itemCostings']['markupInPct']       = $reservation_item['calculation']['percentage']['markup_pct'];
			$item['itemCostings']['grossAmt']          = $reservation_item['calculation']['calculation']['gross_amount'];
			$item['itemCostings']['commInPct']         = $reservation_item['calculation']['percentage']['comm_pct'];
			$item['itemCostings']['commInAmount']      = $reservation_item['calculation']['calculation']['commission'];
			$item['itemCostings']['commAmt']           = $reservation_item['calculation']['calculation']['commission'];
			$item['itemCostings']['totalLessAmt']      = $reservation_item['calculation']['calculation']['total_less_amount'];
			$item['itemCostings']['totalDueAmt']       = $reservation_item['calculation']['calculation']['due_amount'];
			$item['itemCostings']['totalProfitAmt']    = $reservation_item['calculation']['calculation']['profit_amount'];

			$item = $this->bookinglib->buildItemArray( $item );

			return $item;
		}
		

    	public function submitEnetReceipt( $agencyCode,$bookingID,$post )
    	{
    		$connection = new MongoClient();
			$db = $connection->db_system;

			$receiptID = get_next_id("receipt");
			$today = new MongoDate(strtotime("now"));

			$bookingData = $db->booking->findOne(array("booking_id"=>$bookingID));
			$agencyData  = $db->agency->findOne(array("agency_code"=>$agencyCode));

			$this->load->model('document_model');

			//$checBooking = $this->booking_model->generateDocs($agencyCode,$bookingID);
			//$doc =  new Document();
			//print_r($datas->receiptItemData);
			//$content = $doc->generateReceiptDoc2($datas->receiptItemData,$bookingData,$agencyData,$datas->receiptData,(int)$receiptID.'-'.$bookingID);
			$otherData = array();
			$otherData['receiptTypeName'] = 'EFT E-Nett';
	        $otherData['receiptDescription'] = "B2B Payments: ".$post['reference'];
	        $otherData['receiptReferenceNumber'] = $post['reference'];
	         $otherData['receiptDepositedAmout'] = (float)$post['amount'];

			$content = $this->document_model->generateReceiptDoc($bookingData,$agencyData,$post,(int)$receiptID.'-'.$bookingID,$otherData);
			$docID = $this->document_model->generateDocs($content,"RECEIPT","receipt-".$receiptID."-".$bookingID.".pdf","RCP",$receiptID."-".$bookingID);


			//print_r($bookingData);
			//print_r($agencyData);
			$insertData = array();
			$insertData['receiptId']         = (int)$receiptID;
			$insertData['bookingId']         = $bookingID;
			$insertData['receipt_reference'] = (int)$receiptID."-".$bookingID;


			$insertData['receiptTypeName']            = 'EFT E-Nett';
			$insertData['receiptDescription']         = "B2B Payments: ".$post['reference'];
			$insertData['receiptReferenceNumber']     = $post['reference'];
			$insertData['receiptChequeNumber']        = "";
			$insertData['receiptABAnumber']           =  "";
			$insertData['receiptAccountChequeNumber'] =  "";
			$insertData['receiptDrawersName']         =  "";
			$insertData['receiptBankCode']            =  "";
			$insertData['receiptBankName']            =  "";
			$insertData['receiptCCTypeCode']          =  "";
			$insertData['receiptCCTypeName']          =  "";
			$insertData['receiptCCNumber']            =  "";
			$insertData['receiptCCExpiryDate']        =  "";
			$insertData['receiptCCSecurityCode']      =  "";
			$insertData['receiptDisplayAmout']        =  (float)$post['displayAmount'];
			$insertData['receiptAmout']               = (float)$post['displayAmount']-(float)$post['amount'];
			$insertData['receiptDepositedAmout']      = (float)$post['amount'];
			$insertData['receiptRefundAmout']         = 0;


			$xDate       = explode("/", $post['receipt_date']);
			$receiptDate = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 12:00:00"));
			$insertData['booking_agency_code']            = $bookingData['booking_agency_code'];
			$insertData['booking_agency_name']            = $bookingData['booking_agency_name'];
			$insertData['booking_services_start_date']    = $bookingData['booking_services_start_date'];
			$insertData['booking_agency_consultant_code'] = $bookingData['booking_agency_consultant_code'];
			$insertData['booking_agency_consultant_name'] = $bookingData['booking_agency_consultant_name'];
			$insertData['documentId']                     = (int)$docID;
			$insertData['is_active']                      = 1;
			$insertData['is_refunded']                    = 0;
			$insertData['is_allocated']                   = 0;
			$insertData['created_datetime']               = $today;
			$insertData['receipt_datetime']               = $receiptDate;
			$insertData['is_forwarded']                   = 0;
			//print_r($insertData);
			$db->receipt->insert($insertData);

			//print_r($post);

			$connection->close();
			return true;
    	}


    	public function checkOwnBooking( $agencyCode, $bookingID )
    	{
			$connection = new MongoClient();
			$db         = $connection->db_system;
			$whereData  = array("booking_agency_code"=>$agencyCode,"booking_id"=>(int)$bookingID);

			
			$dataCollectionsArray = $db->booking->findOne($whereData);
			$connection->close();
			if( !empty($dataCollectionsArray) )
			{
				return true;
			}
			return false;
    	}
    	
    	public function getSingleItemDetails( $bookingID, $itemID )
    	{
    		$connection = new MongoClient();
			$db = $connection->db_system;
    		$whereData = array("bookingId"=>(int)$bookingID,"itemId"=>(int)$itemID);
    		$dataCollectionsResults = $db->items->findOne($whereData,array("_id"=>0));

    		foreach( $dataCollectionsResults['itemSupplements'] as $key2 => $row2 )
    		{
				if( (int)$row2['supplement_qty'] >0 )
				{

					$dataCollectionsResults['itemCostings']['originalAmount']    += (float)$row2['supplement_costings']['originalAmount'];
					$dataCollectionsResults['itemCostings']['orginalNetAmt']     += (float)$row2['supplement_costings']['orginalNetAmt'];
					$dataCollectionsResults['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
					$dataCollectionsResults['itemCostings']['markupInAmount']    += (float)$row2['supplement_costings']['markupInAmount'];
					$dataCollectionsResults['itemCostings']['grossAmt']          += (float)$row2['supplement_costings']['grossAmt'];
					$dataCollectionsResults['itemCostings']['commInAmount']      += (float)$row2['supplement_costings']['commInAmount'];
					$dataCollectionsResults['itemCostings']['commAmt']           += (float)$row2['supplement_costings']['commAmt'];
					$dataCollectionsResults['itemCostings']['totalLessAmt']      += (float)$row2['supplement_costings']['totalLessAmt'];
					$dataCollectionsResults['itemCostings']['totalDueAmt']       += (float)$row2['supplement_costings']['totalDueAmt'];
					$dataCollectionsResults['itemCostings']['totalProfitAmt']    += (float)$row2['supplement_costings']['totalProfitAmt'];
					$dataCollectionsResults['itemCostings']['gstAmt']            += (float)$row2['supplement_costings']['gstAmt'];
						
				}
	            
			}
    		$connection->close();
    		return $dataCollectionsResults;
    		
    	}


    	public function getSingleBookingDetails( $agencyCode, $bookingID )
    	{
    		$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("booking_agency_code"=>$agencyCode);


			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("booking_agency_code"=>$agencyCode,"booking_id"=>(int)$bookingID);

			$resultArray = array();
			
			//print_r($whereData);
			$dataCollectionsArray = $db->booking->findOne($whereData);



			$whereData = array('bookingId'=>(int)$bookingID);

			$bookingStartDateSec = 1000000000000;
			$bookingEndDateSec = 0;
			//print_r($whereData);
			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0))->sort(array('itemSeq' => 1));

			$totalPrice = 0;
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach( $dataArray as $key => $row )
			   {
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach( $dataArray2 as $key2 => $row2 )
						 {
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);
						 }
					} catch (Exception $e) {
					   $dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;

					foreach( $row['itemSupplements'] as $key2 => $row2 )
					{
						if( (int)$row2['supplement_qty'] >0 )
						{

							$dataArray[$key]['itemCostings']['originalAmount']    += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt']     += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount']    += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt']          += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount']      += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt']           += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt']      += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt']       += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt']    += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt']            += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}
					if( (int)$row['itemIsCancelled'] != 1 )
					{
						$totalPrice += $dataArray[$key]['itemCostings']['totalDueAmt'];
					}
					
					//print_r($bookingData);
					if( is_object($row['itemStartDate']) )
					{
						//print_r($row['itemStartDate']);
						if( $bookingStartDateSec>$row['itemStartDate']->sec )
						{
							$bookingStartDateSec = $row['itemStartDate']->sec;
						}
						
					}
					if( is_object($row['itemEndDate']) )
					{
						//print_r($row['itemEndDate']);
						if($bookingEndDateSec<$row['itemEndDate']->sec){
							
							$bookingEndDateSec = $row['itemEndDate']->sec;
						}
						
					}

					$voucherArray                   = array();
			   		$whereData                      = array('itemId'=>(int)$row['itemId']);
			   		$dataArray[$key]['itemVoucher'] = array();
					$dataCollectionsResults3        = $db->item_vouchers->find($whereData,array("_id"=>0))->sort(array("voucherId"=>-1))->limit(1);
					try {
						$dataArray3 = iterator_to_array($dataCollectionsResults3);
						 foreach( $dataArray3 as $key3 => $row3 )
						 {
						 	$dataArray[$key]['itemVoucher'] = $row3;
						 }
						// print_r($voucherArray);
					} catch (Exception $e) {
					   //$dataArray = array();
						
					}
			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}

			$receiptedItems       = $db->receipt->find(array("bookingId"=>(int)$bookingID,"is_active"=>1,"is_allocated"=>1),array("_id"=>0));
	 		$ReceiptBookingData   =  array();
	 		$totalDepositedAmount = 0;
		 	try {
				$dataArrayReceiptDataItems = iterator_to_array($receiptedItems);
				foreach( $dataArrayReceiptDataItems as $key3=>$row3 )
				{
					$totalDepositedAmount += (float)$row3['receiptDepositedAmout'];
					array_push($ReceiptBookingData,$row3);
				}
				 
			} catch (Exception $e) {
			   $ReceiptBookingData =  array();
			}

			$whereData       = array("bookingId"=>(int)$bookingID);
			$dataCollections = $db->costings->find($whereData,array("_id"=>0))->sort(array("costingsId"=>-1))->limit(1);
			
			try {
				$dataArrayCostings = iterator_to_array($dataCollections);
				 
			} catch (Exception $e) {
			   $dataArrayCostings =  array();
			}

			$whereData = array("bookingId"=>(int)$bookingID);
			$dataCollections = $db->itinerary->find($whereData,array("_id"=>0))->sort(array("itineraryId"=>-1))->limit(1);
			

			try {
				$dataArrayIti = iterator_to_array($dataCollections);
					 
			} catch (Exception $e) {
				$$dataArrayIti = array();
			}
			

			$dataCollectionsArray['totalPrice'] = $totalPrice;
			$dataCollectionsArray['startDate'] = "";
			if( $bookingStartDateSec != 1000000000000 )
			{
				$dataCollectionsArray['startDate'] = date('d/m/Y', $bookingStartDateSec);
			}
			$dataCollectionsArray['endDate'] = "";
			if( $bookingEndDateSec !=0 )
			{
				$dataCollectionsArray['endDate'] = date('d/m/Y', $bookingEndDateSec);
			}
			$dataCollectionsArray['totalDeposited'] = $totalDepositedAmount;
			$resultArray['bookingData']             =$dataCollectionsArray;
			$resultArray['itemDatas']               =$dataArray;
			$resultArray['receiptDatas']            =$ReceiptBookingData;
			$resultArray['costingsDatas']           =$dataArrayCostings;
			$resultArray['itineraryDatas']          =$dataArrayIti;

		
			$connection->close();
  	  		return $resultArray;


    	}


		public function getBookingDetails( $agencyCode,$searchParams = array() )
		{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("booking_agency_code"=>$agencyCode);

			if( !empty($searchParams) )
			{
				#$whereData['$or'] = array();
			}
			
			//print_r($whereData);
			$dataCollections = $db->booking->find($whereData)->sort(array("booking_id"=>-1));

			

			$dataArray = iterator_to_array($dataCollections);
			//print_r($dataArray);
			$arrayResults = array();
			foreach( $dataArray as $key => $row )
			{
				$data = array();
				$data['booking_id']                     = $row['booking_id'];
				$data['booking_consultant_name']        = $row['booking_consultant_name'];
				$data['booking_agency_name']            = $row['booking_agency_name'];
				$data['booking_creation_date']          = date('d/m/Y', $row['booking_creation_date']->sec);
				$data['booking_agency_consultant_name'] =$row['booking_agency_consultant_name'];
				$data['booking_item_details']           = array();
				$arrItem                                = $db->items->find(array("bookingId"=> (int)$row['booking_id']),array("_id"=>0));

				$dataArrayItem = iterator_to_array($arrItem);

				$hasGuest      = false;
				$ctr           = 0;
				$statusStack   = array("cancel"=>0,"quote"=>0,"confirmed"=>0);

				$data['booking_dueAmount'] = 0;
				foreach ( $dataArrayItem as $key1 => $value1 )
				{
					//print_r($value1);
					if( (int)$value1['itemIsCancelled'] !=1 )
					{
						$data['booking_dueAmount'] += (float) $value1['itemCostings']['totalDueAmt'];
						foreach( $value1['itemSupplements'] as $key2 => $row2 )
						{
							if( (int)$row2['supplement_qty'] >0 )
							{
								$data['booking_dueAmount'] += (float) $row2['supplement_costings']['totalDueAmt'];
							}
				            
						}
					}

					


					$itemDetail = array();

					$itemDetail["itemName"] = $value1['itemName'];
					$itemDetail["itemServiceName"] = $value1['itemServiceName'];
					if( $value1['itemServiceCode'] == "SFEE" )
					{
						$itemDetail["itemStartDate"] ="";
						$itemDetail["itemEndDate"]   ="";
					} else {
						if( is_object($value1['itemStartDate']) )
						{
							$itemDetail["itemStartDate"] = date('d/m/Y', $value1['itemStartDate']->sec);
							$itemDetail["itemEndDate"]   = date('d/m/Y', $value1['itemEndDate']->sec);
						}else{
							$itemDetail["itemStartDate"] = "";
							$itemDetail["itemEndDate"]   = "";
						}
						
					}
					
					$itemDetail["itemStartTime"]   = $value1['itemStartTime'];
					$itemDetail["itemEndTime"]     = $value1['itemEndTime'];
					$itemDetail["itemServiceCode"] = $value1['itemServiceCode'];
					$itemDetail["itemFromCountry"] = $value1['itemFromCountry'];
					$itemDetail["itemFromCity"]    = $value1['itemFromCity'];
					$itemDetail["itemFromAddress"] = $value1['itemFromAddress'];
					$itemDetail["itemToCountry"]   = $value1['itemToCountry'];
					$itemDetail["itemToCity"]      = $value1['itemToCity'];
					$itemDetail["itemToAddress"]   = $value1['itemToAddress'];

					$itemDetail["itemCostings"] = $dataArrayItem[$key1]['itemCostings'];
					
					$itemDetail["itemStatus"]   = "Quote";
					if( 1==$value1['itemIsCancelled'] )
					{
						$itemDetail["itemStatus"] = "Cancelled";
						$statusStack["cancel"] = 1;
					} elseif ( $value1['itemIsConfirmed']==0 && $value1['itemIsCancelled'] == 0 ) {
						$itemDetail["itemStatus"] = "Quote";
						$statusStack["quote"] = 1;
					} elseif ( $value1['itemIsPaxAllocated'] ==1 && $value1['itemIsConfirmed']==1 && $value1['itemIsCancelled'] == 0 ) {
						$itemDetail["itemStatus"] = "Confirmed";
						$statusStack["confirmed"] = 1;
					}
					



					/*$itemDetail["itemGuest"] = array();

					$dataCollectionsResultsGuest = $db->item_guest->find(array("item_id"=> (int)$value1['itemId']),array("_id"=>0));
					$dataArrayGuest = iterator_to_array($dataCollectionsResultsGuest);
					//print_r($dataArrayGuest);
					$hasGuest = false;
					foreach ($dataArrayGuest as $key2 => $value2) {
						$strGuest = $value2['guest_title'].'. '.$value2['guest_first_name'].' '.$value2['guest_last_name'];
						

						array_push($itemDetail["itemGuest"], $strGuest);
					}*/

					//array_push($data["booking_item_details"], $itemDetail);


					//$data["booking_item_details"] = $itemDetail;
					

					//print_r($data["booking_item_details"]);
					$ctr++;
				}


				$receiptedItems = $db->receipt->find(array("bookingId"=>(int)$row['booking_id'],"is_active"=>1,"is_allocated"=>1));
				$totalDepositedAmount = 0;


		 		try {
					$dataArrayReceiptDataItems = iterator_to_array($receiptedItems);
					foreach( $dataArrayReceiptDataItems as $key2 => $row2 )
					{
					 	$totalDepositedAmount += (float)$row2['receiptDepositedAmout']; 	
					}
				} catch (Exception $e) {
				   
				}

				$data['booking_dueAmount']        = number_format($data['booking_dueAmount'],2,'.','');
				$data['booking_deposited_Amount'] = number_format($totalDepositedAmount,2,'.','');
				$data['booking_status']           = "Quote";
				if( $statusStack["cancel"] == 1 && $statusStack["quote"] == 0 && $statusStack["confirmed"] == 0 )
				{
					$data['booking_status'] = "Cancelled";
				}elseif($statusStack["confirmed"] == 1){
					$data['booking_status'] = "Confirmed";
				}
				

				$data['itemNumber'] = $ctr;

				if($ctr != 0){
					array_push($arrayResults,$data);
				}
				/*if(isset($_GET['guestName']) && $_GET['guestName'] != ""){
					if($hasGuest){
						array_push($arrayResults,$data);
					}
				}else{
					array_push($arrayResults,$data);
				}*/
				
			 	
			}

		
			$connection->close();
  	  		return $arrayResults;
  	  		
		}

		public function signup( $insertData )
		{
			$mongoConnect = $this->mongo_db;
			$insertAgency= $mongoConnect->insert('agency', $insertData);

			return $insertAgency;
		}
	}
?>