<?php
set_time_limit(0);

/**
* 
*/
class Hotel_model extends CI_Model
{

	private $db         = null;
	private $CI         = null;
	private $connection = null;
	
	public function __construct()
	{
		parent::__construct();

		$this->connection = new MongoClient();
		$this->db = $this->connection->db_instant;
		$this->CI =& get_instance();
		$this->CI->load->model('booking_model');
	}

	/**
	 * [isExist description]
	 * @param  [type]  $hotelId [description]
	 * @return boolean          [description]
	 */
	public function isExist ( $hotelId )
	{
		$count = $this->db->hotels->find( array('id' => intval($hotelId)) )->count();
		return ($count > 0);
	}

	/**
	 * [parseRoomsData description]
	 * @param  [type] $xml [description]
	 * @return [type]	  [description]
	 */
	public function parseRoomsData ( $xml )
	{

		$xml['response'][0]['content'] = $this->roomCurrencyConverter($xml['response'][0]['content']);
		if( $xml['response'][0]['content'] === FALSE )
		{
			return FALSE;
		}
		$rooms      = $xml['response'][0]['content']['result'][0]['content']['rooms'][0]['room'];
		$groupRooms = array();

		
		foreach ( $rooms as $key => $room )
		{
			if( array_key_exists($room['content']['provider'][0], $groupRooms) )
			{
				array_push($groupRooms[ $room['content']['provider'][0] ], $room);
			} else {
				$groupRooms[ $room['content']['provider'][0] ] = array($room);
			}
		}

		$whosTheWinningProvider = array();
		foreach ( $groupRooms as $key => $value )
		{
			usort($value, array($this, 'sortRoomPrice'));
			array_push($whosTheWinningProvider, array(
				'provider'     => $key,
				'minimumPrice' => $value[0]['content']['price'][0]['content']
			));
		}

		usort($whosTheWinningProvider, function ( $a, $b ) {
			return strcmp($a['minimumPrice'], $b['minimumPrice']);
		});

		$xml['response'][0]['content']['result'][0]['content']['rooms'][0]['room'] = $groupRooms[ $whosTheWinningProvider[0]['provider'] ];

		return $xml;
	}

	/**
	 * [sortRoomPrice description]
	 * @param  [type] $a [description]
	 * @param  [type] $b [description]
	 * @return [type]    [description]
	 */
	public function sortRoomPrice ( $a, $b )
	{
        // $priceA = $a['content']['price'][0]['calculation']['calculation']['due_amount'];
        // $priceB = $b['content']['price'][0]['calculation']['calculation']['due_amount'];
        $priceA = $a['content']['price'][0]['content'];
        $priceB = $b['content']['price'][0]['content'];
        return strcmp($priceA, $priceB);
    }

    /**
     * [extraHotelDetails description]
     * @param  [type] $ids [description]
     * @return [type]      [description]
     */
	public function extraHotelDetails( $ids )
	{
		$return = array(
			'details'    => array(),
			'facilities' => array()
		);
		
		$details     = $this->db->hotels_with_location->find( array('id' => array('$in'=>$ids)), array('_id' => 0) )->timeout(-1);
		$details     = iterator_to_array($details);

		$refHotels = array();
		foreach( $details as $key => $val )
		{
			$refHotels[ $val['id'] ] = $val;
		}
		$return['details'] = $refHotels;
		$facilities    = $this->db->hotel_facilities->find(array('hotelId'=>array('$in'=>$ids)), array('_id'=>0))->timeout(-1);
		$facilities    = iterator_to_array($facilities);
		$refFacilities = array();
		foreach( $facilities as $key => $val )
		{
			$refFacilities[ $val['hotelId'] ] = $val;
		}
		$return['facilities'] = $refFacilities;
		return $return;
	}

	/**
	 * [getConvertCurrencyEx description]
	 * @return [type] [description]
	 */
	public function getConvertCurrencyEx (  )
	{
		// Get the rate by $price['currency']
		$db_system = $this->connection->db_system;
		$currency  = $db_system->currency_ex->find(array(), array('_id'=>0));
		$currency  = iterator_to_array($currency);

		$currEx = array();
		foreach( $currency as $key=>$val )
		{
			$currEx[$val['curr_from']] = (float) $val['rate_to'];
		}
		return $currEx;
	}

	/**
	 * [roomCurrencyConverter Rooms and Combinations]
	 * @param  [type] $content [description]
	 * @return [type]		  [description]
	 */
	public function roomCurrencyConverter ( $content )
	{

        $connection = new MongoClient();
        $db = $connection->db_system;
        
        $getCurrencyExchanged = $this->getConvertCurrencyEx();
        // Loop through the rooms and convert the price and currency
        if( isset($content['result'][0]['content']['rooms'][0]['room']) )
        {

        	$rooms = $content['result'][0]['content']['rooms'][0]['room'];
	        if( is_array($rooms) ) {
	        	foreach ( $rooms as $roomKey => $room )
	        	{

		            $roomCurrency = $room['content']['price'][0];
					$roomCurrency['new_content']  = $getCurrencyExchanged[ $roomCurrency['currency'] ] * ((float) $roomCurrency['content']);
					$roomCurrency['new_currency'] = CONVERTION_CURRENCY;
					
					$roomCurrency['calculation']  = $this->calculateTotalCost( $roomCurrency['new_content'], $roomCurrency['currency'], (float) $roomCurrency['content'] );
					
					$room['content']['price'][0]  = $roomCurrency;
		            if( isset($room['content']['cancellation']) )
		            {
		            	$room['content']['cancellation'][0]['frame'] = $this->CI->booking_model->parseCancellation( $room['content']['cancellation'][0]['frame'] );
		            }

		            $rooms[ $roomKey ] = $room;
		        }
	        }

	        $content['result'][0]['content']['rooms'][0]['room'] = $rooms;
        } else {
        	return FALSE;
        }

        if( isset($content['result'][0]['content']['combinations']) )
        {
            $combinations = $content['result'][0]['content']['combinations'][0]['combination'];
            foreach ( $combinations as $key => $combination )
            {
				$combination['new_price']    = $getCurrencyExchanged[ $combination['currency'] ] * ((float) $combination['price']);
				$combination['new_currency'] = CONVERTION_CURRENCY;
				$combination['calculation']  = $this->calculateTotalCost( $combination['new_price'], $combination['currency'], (float) $combination['price'] );

                // Loop throug the rooms
                $rooms = $combination['content']['rooms'][0]['room'];
                foreach ( $rooms as $roomkey => $room )
                {
					$room['new_price']   = $getCurrencyExchanged[ $combination['currency'] ] * ((float) $room['price']);
					$room['calculation'] = $this->calculateTotalCost( $room['new_price'], $combination['currency'], (float) $room['price'] );

                    $rooms[$roomkey] = $room;
                }
                $combination['content']['rooms'][0]['room'] = $rooms;
                $combinations[$key] = $combination;
            }

            $content['result'][0]['content']['combinations'][0]['combination'] = $combinations;
        }

        $connection->close();
        return $content;
    }

    /**
     * [sortCancellationEnTime description]
     * @param  [type] $a [description]
     * @param  [type] $b [description]
     * @return [type]    [description]
     */
    public function sortCancellationEnTime ( $a, $b )
    {
    	$t1 = strtotime($a['endTime']);
		$t2 = strtotime($b['endTime']);

		return ($t1 < $t2)?-1: 1;
    }

    /**
     * [convertToDecimal description]
     * @param  [type] $val [description]
     * @return [type]      [description]
     */
	public function convertToDecimal ( $val )
	{
		return ( (float) $val / 100 );
	}

	/**
	 * [calculateTotalCost description]
	 * @param  [type]  $price         [description]
	 * @param  [type]  $old_currency  [description]
	 * @param  integer $originalPrice [description]
	 * @return [type]                 [description]
	 */
	public function calculateTotalCost ( $price, $old_currency, $originalPrice = 0 )
	{

		$calculation                 = array();
		$calculation['isDiscounted'] = FALSE;
    	// $angency_hotel_config = $this->session->userdata('costing_calculation')['hotelConfig'];
    	$angency_hotel_config = $this->nativesession->get('costing_calculation')['hotelConfig'];

    	$getCurrencyExchanged = $this->getConvertCurrencyEx();
    	// Already converterd
    	// This order should not be moved
		$net_amount_agency = $price;
		$markup_amount     = ($net_amount_agency * $this->convertToDecimal( $angency_hotel_config['markup_pct'] ) );
		$gross_amount      = $markup_amount + $net_amount_agency;
		$commission        = $gross_amount * $this->convertToDecimal( $angency_hotel_config['comm_pct']);
		$total_less_amount = $commission;
		$due_amount        = $commission - $gross_amount;
		$profit_amount     = $commission - $markup_amount;

		

		$calculation['originalPrice']         = $originalPrice;
		$calculation['formatOriginalPrice']   = $originalPrice . ' ' . $old_currency;
		$calculation['converterdPrice']       = $price;
		$calculation['formatConvertionPrice'] = $price . ' ' . CONVERTION_CURRENCY;
		$calculation['rateTo']                = $getCurrencyExchanged[ $old_currency ];

		$calculation['currency']          = CONVERTION_CURRENCY;
		$calculation['discount_amt']      = 0;
		$calculation['old_currency']      = $old_currency;
		$calculation['commission']        = ($commission        < 0)? $commission        / -1: $commission;
		$calculation['due_amount']        = $gross_amount;
		$calculation['gross_amount']      = ($gross_amount      < 0)? $gross_amount      / -1: $gross_amount;
		$calculation['markup_amount']     = ($markup_amount     < 0)? $markup_amount     / -1: $markup_amount;
		$calculation['profit_amount']     = ($profit_amount     < 0)? $profit_amount     / -1: $profit_amount;
		$calculation['net_amount_agency'] = ($net_amount_agency < 0)? $net_amount_agency / -1: $net_amount_agency;
		$calculation['total_less_amount'] = ($total_less_amount < 0)? $total_less_amount / -1: $total_less_amount;
		


		if( isset( $angency_hotel_config['discount_ptc'] ) && (float)$angency_hotel_config['discount_ptc'] > 0 )
		{
			$calculation['isDiscounted']      = TRUE;
			// Discount Percentage
			$c = $angency_hotel_config['comm_pct'] * $this->convertToDecimal( $angency_hotel_config['discount_ptc'] );
			$angency_hotel_config['comm_pct'] = $angency_hotel_config['comm_pct'] - $c;
			$angency_hotel_config['comm_pct'] = ($angency_hotel_config['comm_pct'] < 0)? $angency_hotel_config['comm_pct'] / -1: $angency_hotel_config['comm_pct'];
			$calculation['discount_amt']      = $gross_amount * $this->convertToDecimal( $c );
			$calculation['commission']        = $gross_amount * $this->convertToDecimal( $angency_hotel_config['comm_pct'] );
			$calculation['due_amount']        = $gross_amount - $calculation['discount_amt'];
			
			
			$calculation['commission']        = $gross_amount * $this->convertToDecimal( $angency_hotel_config['comm_pct'] );
		}
    	return array('calculation' => $calculation, 'percentage' => $angency_hotel_config);
	}
}