<?php
	class Agency_model extends CI_Model {

		public function __construct() {
        	parent::__construct();
    	}
        
        public function user ( $id, $username )
        {
            # Code here
            $connection = new MongoClient();
            $db = $connection->db_system;

            $user = $db->agency_consultants->findOne(array(
                '_id' => new MongoId($id),
                'username' => $username
            ));
            $connection->close();

            return $user;
        } // end of user

        public function signIn ( $data )
        {
            # Code here
            $connection = new MongoClient();
            $db = $connection->db_system;

            $consultant = $db->agency_consultants->findOne(array(
                'username' => $data['username'],
                'password' => md5( $data['password'] )
            ));

            $connection->close();

            if ( ! is_string( $consultant['agency_code'] ) )
            {
                # Code here
                $consultant = array();
            } // end of if statement

            return $consultant;
        } // end of signIn

        /**
         * [updateItemCosting description]
         * @param  [type] $agencyCode [description]
         * @return [type]             [description]
         */
    	public function updateItemCosting ( $agencyCode )
        {
			$connection = new MongoClient();
	        $db = $connection->db_system;
             
            $findAgencyCosting = array( 'agency_code' => $agencyCode );
            $disableField      = array( '_id' => 0, 'agency_code' => 0 );
            $item_costing      = $db->agency_b2b_b2c->findOne($findAgencyCosting, $disableField);


            $agencyDB = $connection->db_agency;
            $setup    = $agencyDB->agency_b2c_price_setup->findOne( array('agency_code' => base64_encode($agencyCode)) );
            $post     = $this->nativesession->get('session_post');


            if( isset($setup['default_commission']) && (int)$setup['default_commission'] > 0 )
            {
                $item_costing['hotelConfig']['discount_ptc'] = (int)$setup['default_commission'];
            }

            if( isset($setup['location_disc']) )
            {
                $locationDiscount = $setup['location_disc'];
                $location = array_search($post['location']['id'], array_map(function ( $item ) { return $item['location_id']; }, $locationDiscount));

                if( $location > -1 )
                {
                    $item_costing['hotelConfig']['discount_ptc'] = $locationDiscount[ $location ]['pct'];
                }
            }

            $this->nativesession->set('costing_calculation', $item_costing);
            $connection->close();
		}
		
		public function checkDatabase( $agencyName )
        {

			$mongoConnect = $this->mongo_db;
			$checkAgency = $mongoConnect->get_where(
										'agency', 
											array('agency_name' => $agencyName
												  )
											);

			return $checkAgency;
		}

		public function signup( $insertData )
        {

			$mongoConnect = $this->mongo_db;
			$insertAgency= $mongoConnect->insert('agency', $insertData);

			return $insertAgency;
        }
        
	}
?>