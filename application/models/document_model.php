<?php
	class Document_model extends CI_Model {

		
		public function __construct() {
        	parent::__construct();
        	$this->load->library('m_pdf');

    	}
    	public function generateDocs($content,$title,$fileName,$type,$code){
			$html = '
				<html>

				<head>
				</head>
				<body>
				<div class="container">
						<div class="header">'.$title.'</div>
						<div>
				            <div class="logo">
				              <img style="height:50px;" src="'.base_url().'static/images/travelrez-logo.jpg" alt="logo" />
				              <img style="height:50px;" src="'.base_url().'static/images/logo.jpg" alt="logo" />
				            </div>
						
						</div>
						
					 '.$content.'

				</div>
				</body>
				</html>

				';
			$mpdf = $this->m_pdf->load();
			//$mpdf=new mPDF('c','A4', 11, 'Georgia, Sans', 5, 5, 5, 15,10,10);
			$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2016</span></div>');
			$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2016</span></div>','E');
			$stylesheet = $this->load->view('bookings/stylePDF',$data,true );

			$mpdf->WriteHTML($stylesheet,1); 
			$mpdf->WriteHTML(utf8_encode($html));


			//$mpdf->Output();
			$mpdf->Output(directory_save_path.$fileName);
			
			
			$connection = new MongoClient();
			$db = $connection->db_system;
			$documentID = $this->getNextID("document");

			$dt = new DateTime(date('Y-m-d'));
			$ts = $dt->getTimestamp();
			$today = new MongoDate($ts);

			$insertData = array();
			$insertData['text'] = $html;
			$insertData['created_datetime'] = $today;
			$insertData['filename'] = $fileName;
			$insertData['type'] = $type;
			$insertData['code'] = $code;
			$insertData['documentId'] = $documentID;

			$db->documents->insert($insertData);

			return $documentID;
		}
		public function generateReceiptDoc($bookingdata,$agencydata,$receiptdata,$receiptRef,$otherData = array()){
		 	//$xDate2 = explode("/", $receiptData->receiptDate); 
			//print_r($receiptdata->receiptDate);
			//$xDate2 = explode("/", $receiptdata->receiptDate);
			//echo $xDate2[1];
			//die();
			
			//$startDate =  date('d, M Y', $bookingdata['booking_services_start_date']->sec);
			$bookingCreatedDate =  date('d, M Y', $bookingdata['booking_creation_date']->sec);

			$xDate2 = explode("/", $receiptdata['receipt_date']);
			$receiptCreateDate = date('d, M Y',strtotime($xDate2[2]."-".$xDate2[1]."-".$xDate2[0]));

			//$xDate2 = explode("/", $receiptdata->receiptDate);
			
			$content = '';
			$content .= '<center>Note: This receipt is not for used. Receipt is to validate refund amount.</center>';
			
			$content .= '<div style="clear:both"></div>
					<div class="invoice-detail">
				
			            <span class="invoice-date">
			            	Receipt Date: <br> 
			            	'.$receiptCreateDate.'
			            	<p class="address">
			            		'.$agencydata["agency_name"].'<br>
								<span style="font-size:10px;">'.$agencydata["address"].', <br>'.$agencydata['state'].', '.$agencydata['city'].', '.$agencydata['postCode'].', '.$agencydata['country'].'
								<br><br>
								Phone: '.$agencydata["phone"].'<br>
								Fax: '.$agencydata["fax"].'
								</span>
								
							</p>
			            </span> 

						 	  <div class="invoice-details">
						            <table id="meta">
						                <tr>
						                    <td class="meta-head">Receipt Reference:#: </td>
						                    <td>'.$receiptRef.'</td>
						                </tr>
						                
						                <tr>
						                    <td class="meta-head">Receipt Date:</td>
						                    <td>'.$receiptCreateDate.'</td>
						                </tr>
						                <tr>
											<td class="meta-head">Receipt Type:</td>
						                    <td>'.$otherData['receiptTypeName'].'</td>
						                </tr>
						                 <tr>
						                    <td class="meta-head">Description:</td>
						                    <td><div>'.$otherData['receiptDescription'].'</div></td>
						                </tr>
						                <tr>
						                    <td class="meta-head">Total Amount Paid:</td>
						                    <td><div>$'.$this->moneyFormat($otherData['receiptDepositedAmout']).'</div></td>
						                </tr>
						               

						            </table>
					          </div>

	       			</div>

		
		 
		 
		 </div>';
		 
		 
		 return $content;
		 
		 
		 }
		public function getNextID($collection){
    		$connection = new MongoClient();
			$db = $connection->db_system;
			$id = 0;
			//echo $collection;
			
	        //$res = $db->counters->findAndModify(array("query"=>array("_id"=>1),"update"=>array("$inc"=>array("seq"=>1)),"new"=>true));
			$res = $db->counters->findAndModify(
				array('_id'=>$collection),
				//array('$set' => array("seq"=>array('$inc'=>array('seq'=>1)))),
				array('$inc'=>array('seq'=>1)),
				array("seq"=>1,"_id"=>0),
				array("new"=>true)
				);
			//print_r($res);
			
			$connection->close();
		    return $res['seq'];
    	}
    	public function moneyFormat($amt){
	
			$val = number_format($amt,2,'.','');
			return $val;
			
		}
    	/*public (){

			//actually, you can pass mPDF parameter on this load() function
			$pdf = $this->m_pdf->load();
			//generate the PDF!
			$pdf->WriteHTML($html);
			//offer it to user via browser download! (The PDF won't be saved on your server HDD)
			$pdf->Output($pdfFilePath, "D");
    	}*/
    }
?>