<?php

class Result_model extends CI_Model {
	
    private $db         = null;
    private $connection = null;

    public function __construct()
    {
        parent::__construct();

        $this->connection = new MongoClient();
        $this->db = $this->connection->db_search;
    }

    /**
     * [saveResult description]
     * @param  [type] $result [description]
     * @return [type]         [description]
     */
    public function saveResult ( $result )
    {
        $collection = $this->getCollection();
        $collection->remove(array());
        $collection->batchInsert( $result );
        $collection->ensureIndex(array('details.location' => '2dsphere'));

        return $this->getFilterCounts();
    }

    /**
     * [getAllResult description]
     * @return [type] [description]
     */
    public function getAllResult (  )
    {
        $collection = $this->getCollection();
        $result     = $collection->find(array(), array('_id'=>0));

        return array('result' => iterator_to_array($result), 'counts' => $this->getFilterCounts(), 'isDatabase' => true);
    }

    /**
     * [getFilterCounts description]
     * @return [type] [description]
     */
    public function getFilterCounts (  )
    {
        $collection = $this->getCollection();
        $hotels    = array();
        $hotels[1] = $collection->find(array('details.propertytype' => "1"))->count();
        $hotels[0] = $collection->find(array('details.propertytype' => "2"))->count();

        $ratings = array();
        foreach ( range(0, 5) as $value )
        {
            array_push($ratings, $collection->find(array('details.rating' => $value))->count());
        }

        $boardtypes = array();
        $boards = array('AI', 'FB', 'HB', 'BB', 'RO');
        foreach ( range(0, 4) as $value )
        {
            $regEx = new MongoRegex('/' . $boards[$value] . '/');
            $boardtypes[$boards[$value]] = $collection->find(array('request.availableBoards' => $regEx))->count();
        }

        $price = array();
        // FOR B2C only
        $maxMin = $collection->aggregate(array(
            array('$group' => array(
                '_id' => null,
                'max' => array('$max' => array('$cond' => array( array('$gt' => array('$calculation.calculation.gross_amount', '$calculation.calculation.due_amount')), '$calculation.calculation.gross_amount', '$calculation.calculation.due_amount' ))),
                'min' => array('$min' => array('$cond' => array( array('$gt' => array('$calculation.calculation.gross_amount', '$calculation.calculation.due_amount')), '$calculation.calculation.gross_amount', '$calculation.calculation.due_amount' ))) 
            ))
        ));
        $price['max'] = intval($maxMin['result'][0]['max']) + 5;
        $price['min'] = intval($maxMin['result'][0]['min']) - 5;

        // $landmarks     = array('museums' => array('count' => 0), 'monument' => array('count' => 0), 'stadium' => array('count' => 0), 'theater' => array('count' => 0), 'historic' => array('count' => 0), 'shopping' => array('count' => 0), 'casino' => array('count' => 0), 'medical' => array('count' => 0) );
        $landmarks     = array();
        $session       = $this->nativesession->get('session_post');
        $city          = trim($session['location']['city']);

        $POI = $this->db->PointOfInterest_modified->aggregate(array(
            array('$match' => array('RegionNameLong' => new MongoRegex("/$city/i"))),
            array('$project' => array(
              '_id'      => 0,
              'id'       => '$RegionID',
              'name'     => '$RegionName',
              'location' => 1,
              'type'     => '$SubClassification'
            )),
            array('$sort' => array(
                'name' => 1
            ))
            /*array('$group' => array(
                '_id'     => '$SubClassification',
                'regions' => array('$push' => array('id' => '$RegionID', 'name' => '$RegionName', 'location' => '$location', 'type' => '$SubClassification'))
            ))*/
        ));

        foreach ( $POI['result'] as $key => $region )
        {
            # code...
            $count = $collection->find(array(
                'details.location' => array(
                    '$near' => array(
                        '$geometry' => array(
                            'type'        => 'Point',
                            'coordinates' => $region['location']['coordinates']
                        ),
                        '$maxDistance' => 5000
                ))
            ))->count();

            $POI['result'][ $key ]['count'] = $count;

            if( $count > 0 ) {
                array_push($landmarks, $POI['result'][ $key ]);
            }
        }

        /*foreach ( $POI['result'] as $key => $landmark )
        {
            $regions = array();
            foreach ( $landmark['regions'] as $key => $region )
            {
                # code...
                $count = $collection->find(array(
                    'details.location' => array(
                        '$near' => array(
                            '$geometry' => array(
                                'type'        => 'Point',
                                'coordinates' => $region['location']['coordinates']
                            ),
                            '$maxDistance' => 5000
                    ))
                ))->count();

                $region['count'] = $count;
                if ( $count > 0 )
                {
                    # Code here
                    array_push($regions, $region);
                } // end of if statement
            }
            $landmarks[ $landmark['_id'] ]['regions'] = $regions;
            $landmarks[ $landmark['_id'] ]['count']   = 0;
            // $landmarks[ $landmark['_id'] ]['count']   = $hotelsInLandMark;
        }*/

        return array(
            'hotels'     => $hotels,
            'ratings'    => $ratings,
            'boardtypes' => $boardtypes,
            'price'      => $price,
            // 'landmarks'  => $landmarks
            'landmarks'  => $landmarks
        );
    }

    /**
     * [getHotel description]
     * @param  [type] $hotel_id [description]
     * @return [type]           [description]
     */
    public function getHotel ( $hotel_id )
    {
        $collection               = $this->getCollection();
        $hotelInfo                = $collection->findOne(array('id'=> $hotel_id));

        $connection = new MongoClient();
        $db_instant = $connection->db_instant;
        $images     = $db_instant->images_informal->find(array('hotelId' => (int)$hotel_id))->limit(15);
        $hotelInfo['hotelImages'] = iterator_to_array($images);
        $connection->close();

        return $hotelInfo;
    }

    /**
     * [getCollection description]
     * @return [type] [description]
     */
    private function getCollection (  )
    {
        # Code here
        $collectionName = $this->nativesession->get( SYSTEM_TYPE . 'search_time');
        $collection     = $this->db->selectCollection( SYSTEM_TYPE . '_' . $collectionName );
        return $collection;
    } // end of getCollection
}