<?php
class Discount_model extends CI_Model {

    private $DISCOUNTS      = null;
    private $DISCOUNT       = null;
    private $FLIGHT_DETAILS = null;
    private $PRICES         = null;
    private $DATE_TODAY     = null;
    private $PASSENGERS     = 0;

	public function __construct() {
    	parent::__construct();

        $this->DISCOUNTS  = json_decode(DISCOUNTS, TRUE);
        $this->DATE_TODAY = strtotime('now');
	}

    /**
     * [config description]
     * @param  [type] $param [FLIGHT, PRICES, PASSENGERS]
     * @return [type]        [description]
     */
    public function config ( $param, $PROMO_CODE )
    {
        # Code here
        if ( isset( $param['FLIGHT'] ) )
        {
            # Code here
            $this->FLIGHT_DETAILS = $param['FLIGHT'];
        } // end of if statement
        if ( isset( $param['PRICES'] ) )
        {
            # Code here
            $this->PRICES = $param['PRICES'];
        } // end of if statement

        if ( isset( $param['PASSENGERS'] ) )
        {
            # Code here
            $this->PASSENGERS = $param['PASSENGERS'];
        } // end of if statement
        if ( ! isset( $this->DISCOUNTS[ $PROMO_CODE ] ) )
        {
            # Code here
            return FALSE;
        } // end of if statement
        $this->DISCOUNT = $this->DISCOUNTS[ $PROMO_CODE ];

        if ( ! $this->isInRange() )
        {
            # Code here
            return FALSE;
        } // end of if statement
        return TRUE;
    } // end of config

    /**
     * Get the total discounted amount of the original price
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function getDiscountedAmount (  )
    {
        /**
        
            TODO:
            - Type of the amount
            - Type of the Discount
        
         */
        $discount = $this->getDiscount();

        if ( is_array( $discount['category'] ) )
        {
            $key    = array_search('product', $discount['category']);
            $price  = $this->getPrice( $discount['category'][ $key ] );
            $amount = $this->getDifference( $price, $discount['type'][ $key ], $discount['value'][ $key ] );

            return $amount;
        } // end of if statement

        $price  = $this->getPrice( $discount['category'] );
        $amount = $this->getDifference( $price, $discount['type'], $discount['value'] );

        return $amount;
    } // end of getDiscountedAmount

    /**
     * [parseDiscount description]
     * @param  [type] $discount [description]
     * @return [type]           [description]
     */
    private function parseDiscount ( $discount )
    {
        # Code here
        if ( count(explode('+', $discount['category'])) > 1 )
        {
            $discount['type']     = explode('+', $discount['type']);
            $discount['category'] = explode('+', $discount['category']);
            return $discount;
        } // end of if statement

        return $discount;
    } // end of parseDiscount

    /**
     * [getPrice description]
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    private function getDifference ( $price, $type, $value )
    {
        $discount = $this->getDiscount();

        if ( ! $discount )
        {
            # Code here
            return 0;
        } // end of if statement

        $p = 0;
        switch ( trim(strtolower($type)) )
        {
            case 'percentage':
                $p = $this->getPercentage( $price, $value );
                break;

            case 'amountperson':
                $p = $this->getAmountPerson( $price, $value );
                break;

            case 'lessamountperson':
                $p = $this->getLessAmountPerson( $price, $value );
                break;
        }

        return $p;
    } // end of getPrice

    /**
     * [getLessAmountPerson description]
     * @param  [type] $price [description]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    private function getLessAmountPerson ( $price, $value )
    {
        # Code here
        $p = $price - round(intval($value) * $this->getPassengers());
        $p = $price - $p;

        return $p;
    } // end of getLessAmountPerson

    /**
     * [getAmountPerson description]
     * @param  [type] $price [description]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    private function getAmountPerson ( $price, $value )
    {
        # Code here
        $p = round(intval($value) * $this->getPassengers() );
        $p = $price = $p;
        return $p;
    } // end of getAmountPerson

    /**
     * [getPercentage description]
     * @param  [type] $price [description]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    private function getPercentage ( $price, $value )
    {
        # Code here
        return round($price * (intval($value)/100));
    } // end of getPercentage

    /**
     * [getPrice description]
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    private function getPrice ( $key )
    {
        # Code here
        if ( ! isset( $this->PRICES[ $key ] ) )
        {
            # Code here
            return $this->PRICES;
        } // end of if statement

        return intval($this->PRICES[ $key ]);
    } // end of getPrice

    /**
     * [getDiscount description]
     * @return [type] [description]
     */
    public function getDiscount (  )
    {
        # Code here
        $discount = $this->parseDiscount( $this->DISCOUNT );
        return $discount;
    } // end of getDiscount

    /**
     * [isInRange description]
     * @return boolean [description]
     */
    private function isInRange (  )
    {
        $discount = $this->getDiscount();

        $date = date( 'Y-m-d', $this->DATE_TODAY );
        switch ( $discount['category'] )
        {
            case 'flight':
                $date = date('Y-m-d', strtotime(str_replace('/', '-', $this->FLIGHT_DETAILS['departure'])));
                break;
        }

        $isInRange = is_in_range_date( $discount['availabilities']['from'], $discount['availabilities']['to'], $date );
        return $isInRange;
    } // end of isInRange

    /**
     * [getPassengers description]
     * @return [type] [description]
     */
    public function getPassengers (  )
    {
        # Code here
        return intval($this->PASSENGERS);
    } // end of getPassengers

    /**
     * [setPassengers description]
     * @param [type] $pax [description]
     */
    public function setPassengers ( $pax )
    {
        # Code here
        $this->PASSENGERS = $pax;
    } // end of setPassengers
    /**
     * [setFlight description]
     * @param [type] $flight [description]
     */
    public function setFlight ( $flight )
    {
        # Code here
        $this->FLIGHT_DETAILS = $flight;
    } // end of setFlight

    /**
     * [message description]
     * @return [type] [description]
     */
    public function message (  )
    {
        # Code here
        return $this->DISCOUNT['message'];
    } // end of message
}