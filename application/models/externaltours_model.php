<?php
	class Externaltours_model extends CI_Model {

		
		public function __construct() {
        	parent::__construct();
    	}
    	/**
    	 * [getTour description]
    	 * @param  [type]  $id            [description]
    	 * @param  boolean $isAgencyLogin [description]
    	 * @return [type]                 [description]
    	 */
    	public function getTour($id, $isAgencyLogin = false)
    	{

				$connection   = new MongoClient();
				$db           = $connection->db_system;
				$whereData    = array("productId"=>(int)$id,"is_active"=>1);
				$arrayResults = $db->products_inventory->findOne($whereData,array('_id'=>0));
				// print_r($arrayResults);
				$DISPLAY_MAX_SIZE=(isset($arrayResults['displayMaxPrice']))?$arrayResults['displayMaxPrice']:0;
				$agency_b2b_b2c = array();
				if($isAgencyLogin){
					$agency_b2b_b2c['toursConfig']['markup_pct'] = 33;
					$agency_b2b_b2c['toursConfig']['comm_pct']   = 12;
				}else{
					$agency_b2b_b2c = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'], "is_active_b2c"=>1),array('_id'=>0));

				}
				if (!$arrayResults||!isset($arrayResults['productAvailability'])) {
					return array();
				}
				$currEx = array();
				$tempArray = array();
				foreach($arrayResults['productAvailability'] as $key=>$val){
					if (strtotime(str_replace('/', '-', $val['dateFrom']))<strtotime('01-07-2020')) {
						continue;
					}
					$date_range = date_difference( str_replace('/', '-', $val['dateFrom']) );
					#echo $val['dateFrom'] . '<br>' . PHP_EOL;
					#echo $date_range . '<br>' . PHP_EOL;

					if( $date_range < 3 )
					{
						continue;
					}

					if($val['isMultiPrice'] == 1){
						$suppArray = array();

						$hasDouble    = FALSE;
						$singlePrices = array();
						$doublePrices = array();
						$options=array();

						foreach ($val['multiPrice'] as $key1 => $value1) {

							$curr = $db->currency_ex->findOne(array('curr_from'=>trim($value1['fromCurrency'])),array('_id'=>0));
							//print_r($curr);
							$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

							#$ch = curl_init(); 
					        #curl_setopt($ch, CURLOPT_URL, 'http://198.58.117.129:1212/cron/currency_exchange.php?fromCurrency=' . $value1['fromCurrency']); 
					        #curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					        #$currEx = curl_exec($ch);
							#$currEx = json_decode($currEx, TRUE);
							#curl_close($ch);

							#print_r($currEx[$curr['curr_from']]);
							#print_r(' ');

							$suppData = array();
							$suppData['basePrice']             = $value1['basePrice'];
							$suppData['fromCurrency']          = $value1['fromCurrency'];
							$suppData['toCurrency']            = $value1['toCurrency'];
							$suppData['convertedRate']         = $currEx[$curr['curr_from']];
							$suppData['supplement_avail_name'] = $value1['supplement_avail_name'];
							$suppData['minGuest']              = ( isset( $value1['minGuest'] )?$value1['minGuest']: 0 );
							$suppData['maxGuest']              = ( isset( $value1['minGuest'] )?$value1['minGuest']: 0 );
							$suppData['hasSupplement']         = ( isset( $value1['hasSupplement'] )?$value1['hasSupplement']: FALSE );
							$suppData['supplement']         = ( isset( $value1['supplement'] )?$value1['supplement']: array() );
							array_push($suppArray,$suppData);
							//$tArray[$key1] = $value1['basePrice'];
							$convertedBasePrice = $value1['basePrice'] * $currEx[$curr['curr_from']];
							$markupDivide       = $agency_b2b_b2c['toursConfig']['markup_pct']/100;
							$grossPrice 		= ($convertedBasePrice * $markupDivide) + $convertedBasePrice;
							preg_match('/(option)(\s|-)([0-9])/i', $suppData['supplement_avail_name'], $matches, PREG_OFFSET_CAPTURE);
							if (count($matches)) {
								if (isset($matches[3])) {
									if (!in_array($matches[3][0], $options)) {
										array_push($options, $matches[3][0]);
									}
								}
							}
							/*$suppData['sub_data'] = array();
							$suppData['sub_data']['convertedBasePrice'] = $convertedBasePrice;
							$suppData['sub_data']['markupDivide']       = $markupDivide;
							$suppData['sub_data']['markupAmount']       = ($convertedBasePrice * $markupDivide);
							$suppData['sub_data']['grossPrice']         = $grossPrice;*/
							// Display Price for double only
							#if ( $suppData['minGuest'] == 2 )
							#{
							#	# Code here
							#} // end of if statement
							$hasDouble = ( ! $hasDouble? ($suppData['minGuest'] == 2): $hasDouble );

							if ( $hasDouble && $suppData['minGuest'] == 2 )
							{
								# Code here
								array_push($doublePrices, $grossPrice);
								continue;
							} // end of if statement
							array_push($singlePrices, $grossPrice);
						}

						$min_value=0;
						$toGetMin=($hasDouble?$doublePrices:$singlePrices);
						// $toGetMin=$singlePrices;
						?>
						<?php
						if (count($toGetMin)>0) {
							$min_value = $DISPLAY_MAX_SIZE?max( $toGetMin ):min( $toGetMin );
						}
						$basePricemultiPrice = array();
						foreach ($suppArray as $key2 => $value2) {
							 $basePricemultiPrice[$key2]  = $value2['basePrice'];
						}
						//print_r($basePricemultiPrice);
						//print_r($suppArray);
						// Sort items by baseprice
						$doesHasOtherPax=false;

						$_activeMinGuest=0;
						foreach ($suppArray as $_key => $_value) {
							if ($_activeMinGuest==0) {
								$_activeMinGuest=$_value['minGuest'];
							}
							if ($_value['minGuest']!=$_activeMinGuest) {
								$doesHasOtherPax=true;
							}
						}
						if ($doesHasOtherPax) {
							usort($suppArray, function($a,$b){
								if ($a['minGuest'] == $b['minGuest']) {
				            return 0;
				        }
				        return ($a['minGuest'] > $b['minGuest']) ? -1 : 1;
							});
						} else {
							usort($suppArray, function($a,$b){
								if ($a['basePrice'] == $b['basePrice']) {
				            return 0;
				        }
				        return ($a['basePrice'] > $b['basePrice']) ? -1 : 1;
							});
						}
						
						// array_multisort($basePricemultiPrice, SORT_ASC, $suppArray);
					   // $suppArray=array_multisort($suppArray,array('basePrice'=>SORT_ASC));
						#print_r($suppArray);
					    ////////////////////////////Sub Product Price List///////////////////////////////////////////////////////////////
					    $suppArray2 = array();
					    if (empty($val['supplemultiPrice'])) {

						    $val['supplemultiPrice'] = array();

						}else{
							$basePricesupplemultiPrice=array();

							foreach ($val['supplemultiPrice'] as $key3 => $value3) {

								$curr = $db->currency_ex->findOne(array('curr_from'=>$value3['sub_fromCurrency']),array('_id'=>0));
								//print_r($curr);
								$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

								$suppData2 = array();
								$suppData2['sub_basePrice'] = $value3['sub_basePrice'];
								$suppData2['sub_fromCurrency'] = $value3['sub_fromCurrency'];
								$suppData2['sub_toCurrency'] = $value3['sub_toCurrency'];
								$suppData2['convertedRate'] = $currEx[$curr['curr_from']];
								$suppData2['sub_supplement_avail_name'] = $value3['sub_supplement_avail_name'];
								array_push($suppArray2,$suppData2);
							}

							foreach ($suppArray2 as $key5 => $value5) {
								 $basePricesupplemultiPrice[$key5]  = $value5['sub_basePrice'];
							}
						    array_multisort($basePricesupplemultiPrice, SORT_ASC, $suppArray2);
						}


					    //////////////////////Add On/////////////////////////////////////////////////////////////////////

						$suppArray3 = array();
					    if(empty($arrayResults['productSupplements'])){

					    	$arrayResults['productSupplements'] = array();

					    }else{

							foreach ($arrayResults['productSupplements'] as $key4 => $value4) {

								$curr = $db->currency_ex->findOne(array('curr_from'=>$value4['fromCurrency']),array('_id'=>0));
								//print_r($curr);
								$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

								#print_r($currEx[$curr['curr_from']]);
								#print_r(' ');

								$suppData3 = array();
								$suppData3['supplement_name'] = $value4['supplement_name'];
								$suppData3['fromCurrency']    = $value4['fromCurrency'];
								$suppData3['toCurrency']      = $value4['toCurrency'];
								$suppData3['convertedRate']   = $currEx[$curr['curr_from']];
								$suppData3['basePrice']       = $value4['basePrice'];
								$suppData3['minguest']        = $value4['minguest'];
								$suppData3['maxguest']        = $value4['maxguest'];
								$suppData3['isMarkup']        = ($value4['maxguest'] == 1);

								array_push($suppArray3,$suppData3);
							}

							foreach ($suppArray3 as $key6 => $value6) {
								 $basePriceproductSupplements[$key6]  = $value6['basePrice'];
							}

						    array_multisort($basePriceproductSupplements, SORT_ASC, $suppArray3);

					    }

					    //////////////////////Supplements/////////////////////////////////////////////////////////////////////

						$dateArr = explode('/',$val['dateFrom']);
						$dateArr2 = explode('/',$val['dateTo']);

						if ( strlen( $dateArr[2] ) == 2 )
						{
							# Code here
							$dateArr[2] = '20' . $dateArr[2];
							$dateArr2[2] = '20' . $dateArr2[2];
						} // end of if statement
						$newDateFrom=date_create($dateArr[2]."-".$dateArr[1]."-".$dateArr[0]);
						$newDateTo=date_create($dateArr2[2]."-".$dateArr2[1]."-".$dateArr2[0]);

						$priceBase = (float)$min_value;
						$displayPrice = round($priceBase);

						$SubProductPriceListArray = array();
						foreach ($suppArray as $key7 => $value7) {
							$SubProductPriceListData = array();

							$SubProductPriceListData['supplement_avail_name'] = $value7['supplement_avail_name'];
							
							$SubProductPriceListData['OriginalBasePrice']     = $value7['basePrice'];
							$SubProductPriceListData['fromCurrency']          = $value7['fromCurrency'];
							$SubProductPriceListData['toCurrency']            = $value7['toCurrency'];
							$SubProductPriceListData['convertedRate']         = $value7['convertedRate'];
							$SubProductPriceListData['markup_pct_amount']     = $agency_b2b_b2c['toursConfig']['markup_pct'];
							$SubProductPriceListData['comm_pct_amount']       = $agency_b2b_b2c['toursConfig']['comm_pct'];
							$SubProductPriceListData['mark_up_amount']       = 0;

							$convertedBasePrice = $value7['basePrice']*$value7['convertedRate'];
							$SubProductPriceListData['convertedBasePrice'] = $convertedBasePrice;

							$markupDivide = $agency_b2b_b2c['toursConfig']['markup_pct']/100;
							$grossPrice   = ($convertedBasePrice * $markupDivide) + $convertedBasePrice;
							$SubProductPriceListData['grossPrice'] = ceil($grossPrice);

							$comDivide = $agency_b2b_b2c['toursConfig']['comm_pct']/100;
							$comPrice  = $grossPrice * $comDivide;
							$SubProductPriceListData['comPrice'] = $comPrice;

							$SubProductPriceListData['netAmount'] = $grossPrice - $comPrice;
							$SubProductPriceListData['minGuest']  = ( isset( $value7['minGuest'] )?$value7['minGuest']: 0 );
							$SubProductPriceListData['minGuest']  = ( isset( $value7['minGuest'] )?$value7['minGuest']: 0 );

							$SubProductPriceListData['hasSupplement']=$value7['hasSupplement'];
							
							if ($SubProductPriceListData['hasSupplement']) {
								$multi_sup=$value7['supplement'];

								$curr = $db->currency_ex->findOne(array('curr_from'=>$multi_sup['sub_fromCurrency']),array('_id'=>0));
								$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

								if ($multi_sup['type']=='PERCENTAGE') {
									$multi_sup['sub_basePrice']=$value7['basePrice']*($multi_sup['sub_basePrice']/100);
								}
								$multi_sup['OriginalBasePrice'] = $multi_sup['sub_basePrice'];
								$multi_sup['convertedRate']     = $currEx[$curr['curr_from']];
								$multi_sup['markup_pct_amount'] = $agency_b2b_b2c['toursConfig']['markup_pct'];
								$multi_sup['comm_pct_amount']   = $agency_b2b_b2c['toursConfig']['comm_pct'];

								$convertedBasePrice = $multi_sup['sub_basePrice']*$multi_sup['convertedRate'];
								$multi_sup['convertedBasePrice'] = $convertedBasePrice;

								$markupDivide = $agency_b2b_b2c['toursConfig']['markup_pct']/100;
								$grossPrice = $convertedBasePrice * $markupDivide + $convertedBasePrice;
								$multi_sup['grossPrice'] = ceil($grossPrice);

								$comDivide = $agency_b2b_b2c['toursConfig']['comm_pct']/100;
								$comPrice = $grossPrice * $comDivide;
								$multi_sup['comPrice'] = $comPrice;

								$multi_sup['netAmount'] = $grossPrice - $comPrice;
								$SubProductPriceListData['supplement']=$multi_sup;
							}
							// $SubProductPriceListData['subdata']  = array();
							// $SubProductPriceListData['subdata']['convertedBasePrice'] = $convertedBasePrice;
							// $SubProductPriceListData['subdata']['markupDivide']       = $markupDivide;
							// $SubProductPriceListData['subdata']['markupAmount']       = ($convertedBasePrice * $markupDivide);
							// $SubProductPriceListData['subdata']['grossPrice']         = $grossPrice;
							// $SubProductPriceListData['subdata']['comDivide']          = $comDivide;
							// $SubProductPriceListData['subdata']['comPrice']           = $comPrice;

							array_push($SubProductPriceListArray, $SubProductPriceListData);
						}
						#print_r('Sub Product Price List: ');
						#print_r($SubProductPriceListArray);
						
						$AddOnArray = array();
						foreach ($suppArray2 as $key8 => $value8) {
							$AddOnArrayData = array();

							$AddOnArrayData['sub_supplement_avail_name'] = $value8['sub_supplement_avail_name'];

							$AddOnArrayData['OriginalBasePrice'] = $value8['sub_basePrice'];
							$AddOnArrayData['sub_fromCurrency']  = $value8['sub_fromCurrency'];
							$AddOnArrayData['sub_toCurrency']    = $value8['sub_toCurrency'];
							$AddOnArrayData['convertedRate']     = $value8['convertedRate'];
							$AddOnArrayData['markup_pct_amount'] = $agency_b2b_b2c['toursConfig']['markup_pct'];
							$AddOnArrayData['comm_pct_amount']   = $agency_b2b_b2c['toursConfig']['comm_pct'];

							$convertedBasePrice = $value8['sub_basePrice']*$value8['convertedRate'];
							$AddOnArrayData['convertedBasePrice'] = $convertedBasePrice;

							$markupDivide = $agency_b2b_b2c['toursConfig']['markup_pct']/100;
							$grossPrice = $convertedBasePrice * $markupDivide + $convertedBasePrice;
							$AddOnArrayData['grossPrice'] = ceil($grossPrice);

							$comDivide = $agency_b2b_b2c['toursConfig']['comm_pct']/100;
							$comPrice = $grossPrice * $comDivide;
							$AddOnArrayData['comPrice'] = $comPrice;

							$AddOnArrayData['netAmount'] = $grossPrice - $comPrice;

							$AddOnArray['ITEM' . $key8 ] = $AddOnArrayData;
							// array_push($AddOnArray, $AddOnArrayData);
						}
						#print_r('Add On: ');
						#print_r($AddOnArray);

						$SupplementsArray = array();
						foreach ($suppArray3 as $key9 => $value9) {
							$SupplementsArrayData = array();

							$SupplementsArrayData['supplement_name'] = $value9['supplement_name'];

							$SupplementsArrayData['OriginalBasePrice'] = $value9['basePrice'];
							$SupplementsArrayData['fromCurrency']      = $value9['fromCurrency'];
							$SupplementsArrayData['toCurrency']        = $value9['toCurrency'];
							$SupplementsArrayData['convertedRate']     = $value9['convertedRate'];
							$SupplementsArrayData['markup_pct_amount'] = $agency_b2b_b2c['toursConfig']['markup_pct'];
							$SupplementsArrayData['comm_pct_amount']   = $agency_b2b_b2c['toursConfig']['comm_pct'];
							
							$convertedBasePrice                         = $value9['basePrice']*$value9['convertedRate'];
							$SupplementsArrayData['convertedBasePrice'] = $convertedBasePrice;

							$markupDivide                       = $agency_b2b_b2c['toursConfig']['markup_pct']/100;
							$grossPrice                         = $convertedBasePrice * $markupDivide + $convertedBasePrice;
							$SupplementsArrayData['grossPrice'] = ceil($grossPrice);

							$comDivide = $agency_b2b_b2c['toursConfig']['comm_pct']/100;
							$comPrice  = $grossPrice * $comDivide;
							$SupplementsArrayData['comPrice'] = $comPrice;

							$SupplementsArrayData['netAmount'] = $grossPrice - $comPrice;

							$SupplementsArrayData['minguest'] = $value9['minguest'];
							$SupplementsArrayData['maxguest'] = $value9['maxguest'];
							$SupplementsArrayData['isMarkup'] = $value9['isMarkup'];
							$SupplementsArray['ITEM' . $key9] = $SupplementsArrayData;
							#array_push($SupplementsArray, $SupplementsArrayData);
						}

						usort($SupplementsArray, function ( $a, $b ) {
							return $a['minguest'] - $b['minguest'];
						});
						
						$_double=array_filter($SubProductPriceListArray,function($item){
							return $item['minGuest']==2;
						});
						usort($_double, function ( $a, $b ) {
							return  $a['OriginalBasePrice'] - $b['OriginalBasePrice'];
						});
						$_tripple=array_filter($SubProductPriceListArray,function($item){
							return $item['minGuest']==3;
						});
						$_quad=array_filter($SubProductPriceListArray,function($item){
							return $item['minGuest']==4;
						});
						usort($_tripple, function ( $a, $b ) {
							return  $a['OriginalBasePrice'] - $b['OriginalBasePrice'];
						});
						$_single=array_filter($SubProductPriceListArray,function($item){
							return $item['minGuest']==1;
						});
						usort($_single, function ( $a, $b ) {
							return  $a['OriginalBasePrice'] - $b['OriginalBasePrice'];
						});
						$_merge=array_merge($_double,$_tripple);
						$_merge=array_merge($_merge,$_single);
						$_merge=array_merge($_merge,$_quad);
						$SubProductPriceListArray=$_merge;
						#print_r('Supplements: ');
						#print_r($SupplementsArray);

						#print_r($agency_b2b_b2c['toursConfig']['markup_pct']);
						$_days=0;
						if ($dateArr[1]==$dateArr2[1]) {
							$_days=(int)$dateArr2[0]-((int)$dateArr[0]-1);
						} else {
							$_start_days=cal_days_in_month(CAL_GREGORIAN, $dateArr[1], $dateArr[2]);
							$_start_days-=(int)$dateArr[0];
							$_start_days+=((int)$dateArr2[0]+1);
							$_days=$_start_days;
						}
						
						$now=strtotime($dateArr[2].'-'.$dateArr[1].'-'.$dateArr[0]);
						$end=strtotime($dateArr2[2].'-'.$dateArr2[1].'-'.$dateArr2[0]);
						$days=round(($end - $now) / (60 * 60 * 24));

						$newAvailabilityFormat = array(
							"isMultiPrice"                   => $val['isMultiPrice'],
							"duration"                       =>$_days,
							"fromDayText"                    =>date_format($newDateFrom,'d M Y, l'),
							"fromMonth"                      =>(int)$dateArr[1],
							"fromDay"                        =>(int)$dateArr[0],
							"fromYear"                       =>(int)$dateArr[2],
							"toDayText"                      =>date_format($newDateTo,'d M Y, l'),
							"toMonth"                        =>(int)$dateArr2[1],
							"toDay"                          =>(int)$dateArr2[0],
							"toYear"                         =>(int)$dateArr2[2],
							"displayPrice"                   => $displayPrice,
							"supplement_avail_name_data"     => $SubProductPriceListArray,
							"options"                        =>$options,
							"sub_supplement_avail_name_data" => $AddOnArray,
							"product_supplements"            => $SupplementsArray,
							'analyzation'                    => $this->analyCandicates( $SupplementsArray )
							);
						array_push($tempArray,$newAvailabilityFormat);

					}else{
						$suppArray2 = array();
					    if (empty($val['supplemultiPrice'])) {

						    $val['supplemultiPrice'] = array();

						}else{

							foreach ($val['supplemultiPrice'] as $key3 => $value3) {

								$curr = $db->currency_ex->findOne(array('curr_from'=>$value3['sub_fromCurrency']),array('_id'=>0));
								//print_r($curr);
								$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

								$suppData2 = array();
								$suppData2['sub_basePrice'] = $value3['sub_basePrice'];
								$suppData2['sub_fromCurrency'] = $value3['sub_fromCurrency'];
								$suppData2['sub_toCurrency'] = $value3['sub_toCurrency'];
								$suppData2['convertedRate'] = $currEx[$curr['curr_from']];
								$suppData2['sub_supplement_avail_name'] = $value3['sub_supplement_avail_name'];
								array_push($suppArray2,$suppData2);
							}

							foreach ($suppArray2 as $key5 => $value5) {
								 $basePricesupplemultiPrice[$key5]  = $value5['sub_basePrice'];
							}
							
						    array_multisort($basePricesupplemultiPrice, SORT_ASC, $suppArray2);
						}

						$AddOnArray = array();
						foreach ($suppArray2 as $key8 => $value8) {
							$AddOnArrayData = array();

							$AddOnArrayData['sub_supplement_avail_name'] = $value8['sub_supplement_avail_name'];

							$AddOnArrayData['OriginalBasePrice'] = $value8['sub_basePrice'];
							$AddOnArrayData['sub_fromCurrency']  = $value8['sub_fromCurrency'];
							$AddOnArrayData['sub_toCurrency']    = $value8['sub_toCurrency'];
							$AddOnArrayData['convertedRate']     = $value8['convertedRate'];
							$AddOnArrayData['markup_pct_amount'] = $agency_b2b_b2c['toursConfig']['markup_pct'];
							$AddOnArrayData['comm_pct_amount']   = $agency_b2b_b2c['toursConfig']['comm_pct'];

							$convertedBasePrice = $value8['sub_basePrice']*$value8['convertedRate'];
							$AddOnArrayData['convertedBasePrice'] = $convertedBasePrice;

							$markupDivide = $agency_b2b_b2c['toursConfig']['markup_pct']/100;
							$grossPrice = $convertedBasePrice * $markupDivide + $convertedBasePrice;
							$AddOnArrayData['grossPrice'] = ceil($grossPrice);

							$comDivide = $agency_b2b_b2c['toursConfig']['comm_pct']/100;
							$comPrice = $grossPrice * $comDivide;
							$AddOnArrayData['comPrice'] = $comPrice;

							$AddOnArrayData['netAmount'] = $grossPrice - $comPrice;
							array_push($AddOnArray, $AddOnArrayData);
						}

						##################

						$suppArray3 = array();
					    if(empty($arrayResults['productSupplements'])){

					    	$arrayResults['productSupplements'] = array();

					    }else{

							foreach ($arrayResults['productSupplements'] as $key4 => $value4) {

								$curr = $db->currency_ex->findOne(array('curr_from'=>$value4['fromCurrency']),array('_id'=>0));
								//print_r($curr);
								$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

								#print_r($currEx[$curr['curr_from']]);
								#print_r(' ');

								$suppData3 = array();
								$suppData3['supplement_name'] = $value4['supplement_name'];
								$suppData3['fromCurrency']    = $value4['fromCurrency'];
								$suppData3['toCurrency']      = $value4['toCurrency'];
								$suppData3['convertedRate']   = $currEx[$curr['curr_from']];
								$suppData3['basePrice']       = $value4['basePrice'];
								$suppData3['minguest']        = $value4['minguest'];
								$suppData3['maxguest']        = $value4['maxguest'];
								$suppData3['isMarkup']        = ($value4['maxguest'] == 1);
								array_push($suppArray3,$suppData3);
							}

							foreach ($suppArray3 as $key6 => $value6) {
								 $basePriceproductSupplements[$key6]  = $value6['basePrice'];
							}

						    array_multisort($basePriceproductSupplements, SORT_ASC, $suppArray3);

					    }

						$SupplementsArray = array();
						foreach ($suppArray3 as $key9 => $value9) {


							$SupplementsArrayData = array();

							$SupplementsArrayData['supplement_name']   = $value9['supplement_name'];
							
							$SupplementsArrayData['OriginalBasePrice'] = $value9['basePrice'];
							$SupplementsArrayData['fromCurrency']      = $value9['fromCurrency'];
							$SupplementsArrayData['toCurrency']        = $value9['toCurrency'];
							$SupplementsArrayData['convertedRate']     = $value9['convertedRate'];
							$SupplementsArrayData['markup_pct_amount'] = $agency_b2b_b2c['toursConfig']['markup_pct'];
							$SupplementsArrayData['comm_pct_amount']   = $agency_b2b_b2c['toursConfig']['comm_pct'];

							$convertedBasePrice = $value9['basePrice']*$value9['convertedRate'];
							$SupplementsArrayData['convertedBasePrice'] = $convertedBasePrice;

							$markupDivide = $agency_b2b_b2c['toursConfig']['markup_pct']/100;
							$grossPrice = $convertedBasePrice * $markupDivide + $convertedBasePrice;
							$SupplementsArrayData['grossPrice'] = ceil($grossPrice);

							$comDivide = $agency_b2b_b2c['toursConfig']['comm_pct']/100;
							$comPrice = $grossPrice * $comDivide;
							$SupplementsArrayData['comPrice'] = $comPrice;

							$SupplementsArrayData['netAmount'] = $grossPrice - $comPrice;

							$SupplementsArrayData['minguest'] = $value9['minguest'];
							$SupplementsArrayData['maxguest'] = $value9['maxguest'];

							$SupplementsArray['ITEM' . $key9] = $SupplementsArrayData;
							#array_push($SupplementsArray, $SupplementsArrayData);
						}
						usort($suppArray3, function ( $a, $b ) {
							return $a['minguest'] - $b['minguest'];
						});

						$dateArr = explode('/',$val['dateFrom']);
						$dateArr2 = explode('/',$val['dateTo']);

						$newDateFrom=date_create($dateArr[2]."-".$dateArr[1]."-".$dateArr[0]);
						$newDateTo=date_create($dateArr2[2]."-".$dateArr2[1]."-".$dateArr2[0]);

						if(!array_key_exists($val['fromCurrency'], $currEx)){
							$curr = $db->currency_ex->findOne(array('curr_from'=>$val['fromCurrency']),array('_id'=>0));
							//print_r($curr);
							$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

						}

						$priceBase = (float)$val['basePrice'];
						$comPrice  = $priceBase*((int)$agency_b2b_b2c['toursConfig']['comm_pct']/100);
						$lessNow   = $priceBase-$comPrice;

						$convertedPrice = $lessNow*$currEx[$val['fromCurrency']];

						$markupPrice = $convertedPrice*((int)$agency_b2b_b2c['toursConfig']['markup_pct']/100);
						$grossPrice  = $convertedPrice+$markupPrice;
						$netAmount   = $grossPrice - $comPrice;
						//$displayPrice = round($grossPrice);
						$displayPrice = round($priceBase);
						$newAvailabilityFormat = array(
							"isMultiPrice"                   => $val['isMultiPrice'],
							
							"fromDayText"                    =>date_format($newDateFrom,'d M Y, l'),
							"fromMonth"                      =>(int)$dateArr[1],
							"fromDay"                        =>(int)$dateArr[0],
							"fromYear"                       =>(int)$dateArr[2],
							"toDayText"                      =>date_format($newDateTo,'d M Y, l'),
							"toMonth"                        =>(int)$dateArr2[1],
							"toDay"                          =>(int)$dateArr2[0],
							"toYear"                         =>(int)$dateArr2[2],
							"fromCurrency"                   => $val['fromCurrency'],
							"toCurrency"                     => $val['toCurrency'],
							"convertedRate"                  => $currEx[$val['fromCurrency']],
							"comm_pct_amount"                => $agency_b2b_b2c['toursConfig']['comm_pct'],
							"markup_pct_amount"              => $agency_b2b_b2c['toursConfig']['markup_pct'],
							"netAmount"                      => $netAmount,
							"convertedBasePrice"             => $convertedPrice,
							"displayPrice"                   => $displayPrice,
							"grossPrice"                     => ceil($grossPrice),
							"markupPrice"                    => $markupPrice,
							"comPrice"                       => $comPrice,
							"product_supplements"            => $SupplementsArray,
							'sub_supplement_avail_name_data' => $AddOnArray,
							'analyzation'                    => $this->analyCandicates( $SupplementsArray )
							);
						array_push($tempArray,$newAvailabilityFormat);
					}
				}
				#exit;

				#print_r($tempArray);
				$arrayResults['productAvailabilityFinal'] = $tempArray;
				$currEx = array();
				$tempArray = array();
				foreach($arrayResults['productSupplements'] as $key=>$val){
					
					if(!array_key_exists($val['fromCurrency'], $currEx)){
						$curr = $db->currency_ex->findOne(array('curr_from'=>$val['fromCurrency']),array('_id'=>0));
						//print_r($curr);
						$currEx[$curr['curr_from']] = (float)$curr['rate_to'];

					}

					$priceBase = (float)$val['basePrice'];
					$comPrice = $priceBase*((int)$val['commPct']/100);
					$lessNow = $priceBase-$comPrice;

					$convertedPrice = $lessNow*$currEx[$val['fromCurrency']];


					$markupPrice = $convertedPrice*((int)$val['markupPct']/100);
					$grossPrice = $convertedPrice+$markupPrice;
					$displayPrice = round($grossPrice);
					$newAvailabilityFormat = array(
						"supp_name" =>$val['supplement_name'],
						"displayPrice" => $displayPrice,
						"grossPrice" => ceil($grossPrice),
						"markupPrice" => $markupPrice,
						"comPrice" => $comPrice
						);
					array_push($tempArray,$newAvailabilityFormat);
				}


				$arrayResults['productSupplementsFinal'] = $tempArray;
				$connection->close();


				usort($arrayResults['productAvailabilityFinal'], function ( $a, $b ) {
					return strtotime( $a['fromDayText'] ) - strtotime( $b['fromDayText'] );
				});
		    return $arrayResults;
    	}
    	
    	/**
    	 * [analyCandicates description]
    	 * @param  [type] $suppliment [description]
    	 * @return [type]             [description]
    	 */
		private function analyCandicates ( $suppliments )
			{
				# Code here
				$items = array();

				foreach ( $suppliments as $key => $suppliment )
				{
					# code...
					if( ! isset( $items[ $suppliment['minguest'] ] ) )
					{
						$items[ $suppliment['minguest'] ] = array(
							array('key' => $key, 'price' => $suppliment['grossPrice'])
						);
						continue;
					}

					array_push($items[ $suppliment['minguest'] ], array('key' => $key, 'price' => $suppliment['grossPrice']));
				}

				foreach ( $items as $key => $item )
				{
					# code...
					usort($items[ $key ], function( $a, $b ) {
						return $a['price'] - $b['price'];
					});
				}

				#GET THE FIRST ITEM
				foreach ( $items as $key => $item )
				{
					# code...
					$items[ $key ] = $item[0];
				}

				ksort($items);

				return $items;
			} // end of analyCandicates	

	}
?>