<?php

class Travelrezdiscount_model extends CI_Model
{
  const DISCOUNT = 'discounts';

  private $CONNECTION = null;
  private $DATABASE = null;

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('date');
  }
  /**
   * [startConnection description]
   * @return [type] [description]
   */
  public function startConnection()
  {
    if(!$this->CONNECTION) {
      $this->CONNECTION = new MongoClient();
      $this->DATABASE = $this->CONNECTION->tour_db;
    }
  }
  /**
   * [closeConnection description]
   * @return [type] [description]
   */
  public function closeConnection()
  {
    $this->CONNECTION->close();
    $this->CONNECTION = null;
  }
  /**
   * [find description]
   * @return [type] [description]
   */
  public function findOne($where, $options = [])
  {
    $this->startConnection();
    $discount = $this->DATABASE->selectCollection(self::DISCOUNT)->findOne($where, $options);
    $this->closeConnection();

    return $discount;
  }
  /**
   * [find description]
   * @param  [type] $where   [description]
   * @param  array  $options [description]
   * @return [type]          [description]
   */
  public function find($where, $options = [], $sort = [])
  {
    $this->startConnection();
    $discounts = $this->DATABASE
      ->selectCollection(self::DISCOUNT)
      ->find($where, $options)
      ->sort($sort);
    $this->closeConnection();
    return array_values(iterator_to_array($discounts));
  }
  /**
   * [updateUser description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function update($where, $data, $option = array())
  {
    $this->startConnection();
    $discount = $this->DATABASE->selectCollection(self::DISCOUNT)
      ->update($where, ['$set' => $data], $option);
    $this->closeConnection();
    return $discount;
  }
  /**
   * [create description]
   * @param  [type] $payload [description]
   * @return [type]          [description]
   */
  public function create($payload)
  {
    $this->startConnection();
    $this->DATABASE
      ->selectCollection(self::DISCOUNT)
      ->insert($payload);
    $this->closeConnection();
  }
  /**
   * [remove description]
   * @param  [type] $where [description]
   * @return [type]        [description]
   */
  public function remove($where)
  {
    $this->startConnection();
    $this->DATABASE
      ->selectCollection(self::DISCOUNT)
      ->remove($where);
    $this->closeConnection();
  }

  /**
   * [expired description]
   * @param  [type] $discount [description]
   * @return [type]           [description]
   */
  public function expired($discount)
  {
    return (strtotime('now') < $discount['startDate']->sec || strtotime('now') > $discount['endDate']->sec);
  }

  /**
   * [percent description]
   * @param  [type] $price    [description]
   * @param  [type] $discount [description]
   * @return [type]           [description]
   */
  public function percent($price, $discount)
  {
    if($discount) {
      $percent = 0;
      if($discount['type'] == 'PERCENTAGE') {
        $percent = (float) $discount['value'];
      }

      return $percent;
    }

    return 0;
  }
}