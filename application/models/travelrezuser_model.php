<?php

class Travelrezuser_model extends CI_Model
{
  const USER = 'users';

  private $CONNECTION = null;
  private $DATABASE = null;

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('date');
  }
  /**
   * [startConnection description]
   * @return [type] [description]
   */
  public function startConnection()
  {
    if(!$this->CONNECTION) {
      $this->CONNECTION = new MongoClient();
      $this->DATABASE = $this->CONNECTION->tour_db;
    }
  }
  /**
   * [closeConnection description]
   * @return [type] [description]
   */
  public function closeConnection()
  {
    $this->CONNECTION->close();
    $this->CONNECTION = null;
  }
  /**
   * [findUser description]
   * @param  [type] $email [description]
   * @return [type]        [description]
   */
  public function findUser($email)
  {
    $this->startConnection();
    $user = $this->DATABASE->selectCollection(self::USER)->findOne(['email' => $email]);
    $this->closeConnection();

    return $user;
  }
  /**
   * [find description]
   * @return [type] [description]
   */
  public function findOne($where, $options = [])
  {
    $this->startConnection();
    $user = $this->DATABASE->selectCollection(self::USER)->findOne($where, $options);
    $this->closeConnection();

    return $user;
  }
  /**
   * [updateUser description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function update($where, $data, $option = array())
  {
    $this->startConnection();
    $user = $this->DATABASE->selectCollection(self::USER)
      ->update($where, ['$set' => $data], $option);
    $this->closeConnection();
  }
}