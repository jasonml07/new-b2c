<?php

/**
* 			
*/
class Runtime
{
	private $TSTART = 0;
	private $TEND   = 0;
	private $STOP   = FALSE;
	function __construct()
	{
		# code...
	}

	/**
	 * [start description]
	 * @return [type] [description]
	 */
	public function start ( $get = FALSE )
	{
		# Code here
		$this->STOP   = FALSE;
		$this->TSTART = microtime(true);
		if ( $get )
		{
			# Code here
			return $this->TSTART;
		} // end of if statement
	} // end of start

	/**
	 * [start description]
	 * @return [type] [description]
	 */
	public function stop ( $get = FALSE )
	{
		# Code here
		$this->STOP  = TRUE;
		$this->TEND = microtime(true);
		if ( $get )
		{
			# Code here
			return $this->TEND;
		} // end of if statement
	} // end of end

	public function result ( )
	{
		# Code here
		$this->stop();
		return $this->timeCalc();
	} // end of result

	private function timeCalc ( )
	{
		# Code here
		return (round($this->TEND - $this->TSTART, 2));
	} // end of timeCalc
}