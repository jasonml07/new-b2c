<?php if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );
/**
* 
*/
class Paypal
{
	private $CI;

	private $IS_SANDBOX;
	private $IS_CERTIFICATE;
	private $API_USERNAME;
	private $API_PASSWORD;
	private $API_SIGNATURE;
	private $API_CERTIFICATE;
	private $API_SUBJECT;
	private $API_VERSION;
	private $API_ENDPOINTURL;

	private $ROOT_DIR;
	private $LIB_IS_USABLE  = TRUE;
	private $LOG            = FALSE;
	private $PATH           = '';
	private $FILE_EXTENSION = array( 'REQUEST', 'RESPONSE' );

	/**
	 * [__construct description]
	 * @param array $config [description]
	 */
	function __construct( $config = array() )
	{
		$CI =& get_instance();
		$this->ROOT_DIR = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
		$_logPath  = $this->ROOT_DIR . '/paypal-logs';
		# code...
		if ( ! empty( $config ) )
		{
			# Code here
			$this->IS_SANDBOX      = (@$config['Sandbox'])       ? $config['Sandbox']      : '';
			$this->IS_CERTIFICATE  = (@$config['Certificate'])   ? $config['Certificate']  : '';
			$this->API_USERNAME    = (@$config['APIUsername'])   ? $config['APIUsername']  : '';
			$this->API_PASSWORD    = (@$config['APIPassword'])   ? $config['APIPassword']  : '';
			$this->API_SIGNATURE   = (@$config['APISignature'])  ? $config['APISignature'] : '';
			$this->API_CERTIFICATE = (@$config['APICertificate'])? $config['APICertificate'] : '';
			$this->API_VERSION     = '<ebl:Version>' . ((@$config['APIVersion'])   ? $config['APIVersion']   : '') . '</ebl:Version>';

			$this->LOG  = (@$config['LOG']) ? $config['LOG'] : FALSE;
			$this->PATH = (@$config['PATH'])? $config['PATH']: $_logPath;

			$this->initEPU();
			return TRUE;
		} // end of if statement

		if ( $CI->config->load('paypal') )
		{
			# Code here
			$this->IS_SANDBOX      = $this->config->item('Sandbox');
			$this->IS_CERTIFICATE  = $this->config->item('Certificate');
			$this->API_USERNAME    = $this->config->item('APIUsername');
			$this->API_PASSWORD    = $this->config->item('APIPassword');
			$this->API_SIGNATURE   = $this->config->item('APISignature');
			$this->API_CERTIFICATE = $this->config->item('APICertificate');
			$this->API_VERSION     = '<ebl:Version>' . (($this->config->item('APIVersion'))   ? $this->config->item('APIVersion')   : '') . '</ebl:Version>';

			$this->LOG  = $this->config->item('LOG');
			$_PATH = $this->config->item('PATH');
			$this->PATH = (( $_PATH && ! empty( $_PATH ) )?$_PATH: $_logPath);

			$this->initEPU();
			return TRUE;	
		} // end of if statement

		$this->LIB_IS_USABLE = FALSE;
		return $this->LIB_IS_USABLE;
	}

	/**
	 * [initEPU description]
	 * @return [type] [description]
	 */
	private function initEPU (  )
	{
		# Code here
		$sandbox = '';
		$live    = '';
		if ( $this->IS_CERTIFICATE )
		{
			# Code here
			$sandbox = 'https://api.sandbox.paypal.com/2.0/';
			$live    = 'https://api.paypal.com/2.0/';
		} else {
			$sandbox = 'https://api-3t.sandbox.paypal.com/2.0/';
			$live    = 'https://api-3t.paypal.com/2.0/';
		} // end of if statement
		if ( ! $this->IS_SANDBOX )
		{
			# Code here
			$this->API_ENDPOINTURL = $live;
			return TRUE;
		} // end of if statement
		$this->API_ENDPOINTURL = $sandbox;
	} // end of initEPU

	/**
	 * [expressCheckout description]
	 * @return [type] [description]
	 */
	public function expressCheckout (  )
	{
		# Code here
		#$this->XML_PROCESS = $this->buildCredentials();
		#return $this;
	} // end of expressCheckout

	public function getExpressCheckout ( $token )
	{
		# Code here
		$xml = $this->buildCredentials();

		$xml .= '<ns:GetExpressCheckoutDetailsReq>';
			$xml .= '<ns:GetExpressCheckoutDetailsRequest>';
				$xml .= $this->API_VERSION;
				$xml .= '<ns:Token>' . $token . '</ns:Token>';
			$xml .= '</ns:GetExpressCheckoutDetailsRequest>';
		$xml .= '</ns:GetExpressCheckoutDetailsReq>';

		$this->finalTouch( $xml );

		$result = $this->cURL( $xml );
		return $result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetExpressCheckoutDetailsResponse'];
	} // end of getExpressCheckout

	/**
	 * [setExpressCheckout description]
	 * @param [type] $fields [description]
	 */
	public function setExpressCheckout ( $data )
	{
		# Code here
		$_availableFields = array(
			'ReqConfirmShipping'       => 'xs:string',
			'Token'                    => 'xs:string',
			'MaxAmount'                => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'),
			'ReturnURL'                => 'xs:string',
			'CancelURL'                => 'xs:string',
			'CallbackURL'              => 'xs:string',
			'CallbackTimeout'          => 'int',
			'NoShipping'               => 'xs:string',
			#'FlatRateShippingOptions'  => 'ebl:ShippingOptionsType',
			#'PaymentDetails'           => 'ebl:PaymentDetailsType', # REQUIRED
			'AddressOverride'          => 'xs:string',
			'LocaleCode'               => 'xs:string',
			'cpp-logo-image'           => 'xs:string',
			'BuyerEmail'               => 'ebl:EmailAddressType',
			'SolutionType'             => 'ebl:SolutionTypeType',
			'LandingPage'              => 'ebl:LandingPageType',
			'ChannelType'              => 'ebl:ChannelType',
			'TotalType'                => 'ebl:TotalType',
			'giropaySuccessURL'        => 'xs:string',
			'giropayCancelURL'         => 'xs:string',
			'BillingAgreement Details' => 'ebl:BillingAgreementDetailsType',
			'Enhanced CheckoutData'    => 'ed:EnhancedCheckoutDataType',
			'OtherPaymentMethods'      => 'ebl:OtherPaymentMethodDetailsType',
			'BuyerDetails'             => 'ebl:BuyerDetailsType',
			'BrandName'                => 'xs:string',
			'FundingSourceDetails'     => 'ebl:FundingSourceDetailsType',
			'PaymentReason'            => 'ebl:PaymentReasonType',
		);
		$_paymentDetails = array(
			'OrderTotal'    => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # REQUIRE with currency ATTR: currencyID
			'ItemTotal'     => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # REQUIRE with currency ATTR: currencyID
			'ShippingTotal' => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # REQUIRE with currency ATTR: currencyID
			'HandlingTotal' => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # ATTR: currencyID
			'TaxTotal'      => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # ATTR: currencyID
			'PaymentAction' => 'ebl:PaymentActionCodeType', #

			#'ButtonSource' => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # ATTR: currencyID
			
			'InsuranceTotal'   => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # ATTR: currencyID
			'ShippingDiscount' => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # ATTR: currencyID
			'OrderDescription' => 'xs:string', #
			
			'InsuranceOptionOffered' => 'xs:boolean', # 
			'Custom' => 'xs:string', #
			'InvoiceID' => 'xs:string', #
			'NotifyURL' => 'xs:string', #
			'ShipToAddress' => 'ebl:AddressType', #
			'MultiShipping' => 'xs:string', #
			'PaymentDetailsItem' => 'ebl:PaymentDetailsItemType', #
			'EnhancedPaymentData' => 'ed:EnhancedPaymentDataType', #
			'FulfillmentAddress' => 'ebl:AddressType', #
			'PaymentCategoryType' => 'ebl:PaymentCategoryType', #
			'NoteText' => 'xs:string', #
			'NoteToBuyer' => 'xs:string', #
			'TransactionId' => 'xs:string', #
			'AllowedPaymentMethod' => 'ebl:AllowedPaymentMethodType', #
			'PaymentRequestID' => 'xs:string', #
		);
		$_Address = array(
			'Name'            => 'xs:string',
			'Street1'         => 'xs:string',
			'Street2'         => 'xs:string', # OPTIONAL
			'CityName'        => 'xs:string',
			'StateOrProvince' => 'xs:string',
			'PostalCode'      => 'xs:string',
			'Country'         => 'ebl:CountryCodeType',
			'Phone'           => 'xs:string', # OPTIONAL
		);
		$_PaymentDetailsItem = array(
			'Name'         => 'xs:string',
			'Description'  => 'xs:string', # OPTIONAL
			'Amount'       => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'),
			'Number'       => 'xs:string', # OPTIONAL
			'Quantity'     => 'xs:integer',
			'Tax'          => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # OPTIONAL
			'ItemCategory' => 'ebl:ItemCategoryType', # OPTIONAL
			'ItemWeight'   => 'xs:integer', # OPTIONAL
			'ItemLength'   => 'xs:integer', # OPTIONAL
			'ItemWidth'    => 'xs:integer', # OPTIONAL
			'ItemHeight'   => 'xs:integer', # OPTIONAL
		);
		$_FlatRateShippingOptions = array(
			'ShippingOptionIsDefault' => 'xs:boolean', # *
			'ShippingOptionName'      => 'xs:string', # *
			'ShippingOptionAmount'    => array('xs:type' => 'ebl:BasicAmountType', 'currencyID' => 'USD'), # *
		);

		#$_paymentDetails['FlatRateShippingOptions'] = $_FlatRateShippingOptions;
		$_paymentDetails['ShipToAddress']           = $_Address;
		$_paymentDetails['PaymentDetailsItem']      = $_PaymentDetailsItem;

		$_availableFields['FlatRateShippingOptions'] = $_FlatRateShippingOptions;
		$_availableFields['PaymentDetails']          = $_paymentDetails;

		$xml = $this->buildCredentials();

		$xml .= '<ns:SetExpressCheckoutReq>';
			$xml .= '<ns:SetExpressCheckoutRequest>';
				$xml .= $this->API_VERSION;
 				$xml .= '<ebl:SetExpressCheckoutRequestDetails>';
 					$xml .= $this->buildFields( $_availableFields, $data );
				$xml .= '</ebl:SetExpressCheckoutRequestDetails>';
			$xml .= '</ns:SetExpressCheckoutRequest>';
		$xml .= '</ns:SetExpressCheckoutReq>';

		$this->finalTouch( $xml );


		#echo PHP_EOL;
		#echo $this->xml_to_dom( $xml );
		#die();

		$result = $this->cURL( $xml );
		return $result['SOAP-ENV:Envelope']['SOAP-ENV:Body']['SetExpressCheckoutResponse'];
	} // end of setExpressCheckout

	/**
	 * [get_balance description]
	 * @param  array  $fields [description]
	 * @return [type]         [description]
	 */
	public function get_balance ( $fields = array() )
	{
		# Code here
		$_availableFields = array('ReturnAllCurrencies' => 'xs:string');

		$xml = $this->buildCredentials();

		$xml .= '<ns:GetBalanceReq>';
			$xml .= '<ns:GetBalanceRequest>';
				$xml .= $this->API_VERSION;
				$xml .= $this->buildFields( $_availableFields, $fields );
			$xml .= '</ns:GetBalanceRequest>';
		$xml .= '</ns:GetBalanceReq>';

		$this->finalTouch( $xml );

		return $this->cURL( $xml );
	} // end of get_balance

	/**
	 * [buildFields description]
	 * @param  [type] $fields [Available fields]
	 * @param  [type] $data   [Data]
	 * @return [type]         [description]
	 */
	private function buildFields ( $fields, $data )
	{
		# Code here
		if ( ! empty( $data ) )
		{
			# Code here
			$xml = '';
			foreach ( $data as $key => $value )
			{
				# code...
				if ( @$fields[ $key ] )
				{
					# Code here
					if ( is_array( $value ) && ! isset( $value['value'] ) )
					{
						# Code here
						if ( array_keys( $value ) === range(0, count($value) - 1) )
						{
							# Code here
							foreach ( $value as $subElement )
							{
								# code...
								#file_put_contents(dirname(__FILE__).'/errorlog.txt', '$subElement' . PHP_EOL . json_encode( $subElement ) . PHP_EOL , FILE_APPEND | LOCK_EX);
								#file_put_contents(dirname(__FILE__).'/errorlog.txt', '$fields[ $key ]' . PHP_EOL . json_encode( $fields[ $key ] ) . PHP_EOL , FILE_APPEND | LOCK_EX);
								$xml .= '<ebl:' . $key . '>';
									$xml .= $this->buildFields( $fields[ $key ], $subElement );
								$xml .= '</ebl:' . $key . '>';
							}
							continue;
						} // end of if statement
						$xml .= '<ebl:' . $key . '>';
							$xml .= $this->buildFields( $fields[ $key ], $value );
						$xml .= '</ebl:' . $key . '>';

						continue;
					} // end of if statement
					$attributes = $this->buildFieldsAttributes( $fields[ $key ], $value );
					$xml .= '<ebl:' . $key . $attributes . '>' . ( is_array( $value )?$value['value']:$value ) . '</ebl:' . $key . '>';
				} // end of if statement
			}

			return $xml;
		} // end of if statement

		return FALSE;
	} // end of buildFields


	/**
	 * [buildFieldsAttributes description]
	 * @param  [type] $array [description]
	 * @return [type]        [description]
	 */
	private function buildFieldsAttributes ( $attrs, $values )
	{
		# Code here
		$attributes = ' ';
		if ( is_array( $attrs ) )
		{
			# Code here
			foreach ( $attrs as $attr => $value )
			{
				# code...
				$attributes .= $attr . '="' . ( ( is_array( $values ) && isset( $values[ $attr ] ))?$values[ $attr ]:$value ) . '" ';
			}
		} // end of if statement

		if ( is_string( $attrs ) )
		{
			# Code here
			$attributes .= 'xs:type="' . $attrs . '"';
		} // end of if statement
		return $attributes;
	} // end of buildFieldsAttributes

	/**
	 * Calls everytime sending a request
	 * @return [type] [description]
	 */
	private function buildCredentials (  )
	{
		# Code here
		$xml = '<soapenv:Envelope xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cc="urn:ebay:apis:CoreComponentTypes" xmlns:ebl="urn:ebay:apis:eBLBaseComponents" xmlns:ed="urn:ebay:apis:EnhancedDataTypes" xmlns:ns="urn:ebay:api:PayPalAPI" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
			$xml .= '<soapenv:Header>';
				$xml .= '<ns:RequesterCredentials>';
					$xml .= '<ebl:Credentials>';
						$xml .= '<ebl:Username>' . $this->API_USERNAME . '</ebl:Username>';
						$xml .= '<ebl:Password>' . $this->API_PASSWORD . '</ebl:Password>';
						
						if ( $this->IS_SANDBOX )
						{
							# Code here
							if ( ! $this->IS_CERTIFICATE )
							{
								# Code here
								$xml .= '<ebl:Signature>' . $this->API_SIGNATURE . '</ebl:Signature>';
							} // end of if statement
						} else {
							$xml .= '<ebl:Signature/>';
						}// end of if statement

						$xml .= ( @$this->API_SUBJECT ) ? '<ebl:Subject>' . $this->API_SUBJECT . '</ebl:Subject>' : '<ebl:Subject/>';
					$xml .= '</ebl:Credentials>';
				$xml .= '</ns:RequesterCredentials>';
			$xml .= '</soapenv:Header>';
			$xml .= '<soapenv:Body>';

		return $xml;
	} // end of buildCredentials

	/**
	 * [finalTouch Adding end tag of the request wrapper]
	 * @param  [type] &$xml [description]
	 * @return [type]       [description]
	 */
	private function finalTouch ( &$xml )
	{
		# Code here
		$xml .= '</soapenv:Body>';
		$xml .= '</soapenv:Envelope>';
	} // end of finalTouch

	/**
	 * [cURL sending request to paypal]
	 * @param  [type] $xml [description]
	 * @return [type]      [description]
	 */
	private function cURL ( $xml )
	{
		# Code here

		
		$fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');
		$this->save_XML( $xml, 0 );
		$ch = curl_init(); 
		#curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		
		curl_setopt($ch, CURLOPT_URL, $this->API_ENDPOINTURL); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_USERPWD, $this->API_USERNAME.":".$this->API_PASSWORD);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 50); 
		curl_setopt($ch, CURLOPT_POST, TRUE); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

		if ( $this->IS_CERTIFICATE )
		{
			curl_setopt($ch, CURLOPT_SSLCERT, $this->API_CERTIFICATE . 'paypal_cert_key_pem.txt');
			/*curl_setopt($ch, CURLOPT_SSLCERT, $this->API_CERTIFICATE . 'file.crt.pem');
			curl_setopt($ch, CURLOPT_SSLKEY, $this->API_CERTIFICATE . 'file.key.pem');
			curl_setopt($ch, CURLOPT_SSLCERTPASSWD, '123456');
			curl_setopt($ch, CURLOPT_SSLKEYPASSWD, '123456');*/
		} // end of if statement

		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_STDERR, $fp);


		$result = curl_exec( $ch );
		curl_close( $ch );

		// echo 'RESULT';
		// var_dump( $result );
		#die();

		$this->save_XML( $result, 1 );

		return $this->xml_to_array( $result );
	} // end of cURL

	/**
	 * [xml_to_array Converting xml to array]
	 * @param  [type] $xml [description]
	 * @return [type]      [description]
	 */
	private function xml_to_array ( $root )
	{
		# Code here
		if ( is_string( $root ) )
		{
			# Code here
			$xmlDOM = new DOMDocument();
			$xmlDOM->loadXML( $root );

			$root = $xmlDOM;
		} // end of if statement
		

		$result = array();

		if ($root->hasAttributes()) {
	        $attrs = $root->attributes;
	        foreach ($attrs as $attr) {
	            $result['@attributes'][ $attr->name ] = $attr->value;
	        }
	    }

	    if ($root->hasChildNodes()) {
	        $children = $root->childNodes;
	        if ($children->length == 1) {
	            $child = $children->item(0);
	            if ($child->nodeType == XML_TEXT_NODE) {
	                $result['_value'] = $child->nodeValue;
	                return count($result) == 1
	                    ? $result['_value']
	                    : $result;
	            }
	        }
	        $groups = array();
	        foreach ($children as $child) {
	            if (!isset($result[$child->nodeName])) {
	                $result[$child->nodeName] = $this->xml_to_array($child);
	            } else {
	                if (!isset($groups[$child->nodeName])) {
	                    $result[$child->nodeName] = array($result[$child->nodeName]);
	                    $groups[$child->nodeName] = 1;
	                }
	                $result[$child->nodeName][] = $this->xml_to_array($child);
	            }
	        }
	    }
	    return $result;
	} // end of xml_to_array

	/**
	 * [xml_to_dom description]
	 * @param  [type] $xml [description]
	 * @return [type]      [description]
	 */
	private function xml_to_dom ( $xml )
	{
		# Code here
		#error_reporting(0);
		$dom = new DOMDocument();

		$dom->preserveWhiteSpace = FALSE;
		$dom->loadXML( $xml );
		$dom->formatOutput = TRUE;

		return $dom->saveXml();
	} // end of xml_to_dom

	/**
	 * [save_XML description]
	 * @param  [type] $xml [description]
	 * @return [type]      [description]
	 */
	private function save_XML ( $xml, $extention )
	{
		# Code here

		if ( $this->LOG )
		{
			$xml = $this->xml_to_dom( $xml );
			# Code here
			// Create the folder if not exist
			if ( ! file_exists( $this->PATH ) )
			{
				# Code here
				mkdir($this->PATH, 0755, TRUE);
			} // end of if statement

			/**
			 * Create the file
			 */
			$_filename = strtotime('now') . '-' . $this->FILE_EXTENSION[ $extention ] . '.xml';
			$_file     = $this->PATH . '/' . $_filename;
			if ( file_exists( $_file ) )
			{
				# Code here
				$this->save_XML( $xml );
			} // end of if statement

			write_file( $_file, $xml );
		} // end of if statement
	} // end of save_XML

	/**
	 * [formatArray description]
	 * @param  [type] &$array [description]
	 * @return [type]         [description]
	 */
	public function formatArray ( $array )
	{
		# Code here
		$result = array();

		foreach ( $array as $key => $arr )
		{
			# code...
			if ( ! is_array( $arr ) )
			{
				$result[ $key ] = $arr;
			} else if ( is_array( $arr ) ) {

				$arr = $this->clearAttributesAndValues( $arr );
				$result[ $key ] = $arr;
				if ( ! count( $result[ $key ] ) )
				{
					# Code here
					$result[ $key ] = '';
				} // end of if statement

				if ( is_array( $arr ) )
				{
					# Code here
					foreach ($arr as $keyARR => $valueARR )
					{
						# code...
						$valueARR = $this->clearAttributesAndValues( $valueARR );

						if ( count( $valueARR ) > 2 && is_array( $valueARR ) )
						{
							# Code here
							$result[ $key ][ $keyARR ] = $this->formatArray( $valueARR );
						} else {
							if ( ! count( $valueARR ) )
							{
								# Code here
								$result[ $key ][ $keyARR ] = '';
								continue;
							} // end of if statement
							$result[ $key ][ $keyARR ] = $valueARR;
						} // end of if statement
					}
				} // end of if statement
				
				
				#if ( isset( $arr['@attributes'] ) )
				#{
				#	# Code here
				#	if ( count( $arr['@attributes'] ) > 1 )
				#	{
				#		# Code here
				#		unset( $arr['@attributes']['type'] );
				#		foreach ( $arr['@attributes'] as $keyATTR => $valueATTR )
				#		{
				#			# code...
				#			$result[ $key ][ $keyATTR ] = $valueATTR;
				#		}
				#		if ( isset( $arr['_value'] ) )
				#		{
				#			# Code here
				#			$result[ $key ]['value'] = $arr['_value'];
				#			unset( $arr['_value'] );
				#		} // end of if statement
				#	} // end of else if statement
				#	unset( $arr['@attributes'] );
				#} // end of if statement
				#if ( isset( $arr['_value'] ) )
				#{
				#	# Code here
				#	$result[ $key ] = $arr['_value'];
				#	unset( $arr['_value'] );
				#} // end of if statement


			} // end of if statement
		}

		return $result;
	} // end of formatArray

	/**
	 * [checkAttributesAndValues description]
	 * @param  [type] $array [description]
	 * @return [type]        [description]
	 */
	private function clearAttributesAndValues ( &$arr )
	{
		# Code here
		$attributes = array();

		if ( ! is_array( $arr ) )
		{
			# Code here
			return $arr;
		} // end of if statement

		if ( count( $arr ) == 0 )
		{
			# Code here
			return '';
		} // end of if statement

		if ( isset( $arr['@attributes'] ) )
		{
			# Code here
			if ( count( $arr['@attributes'] ) > 1 )
			{
				# Code here
				unset( $arr['@attributes']['type'] );

				foreach ( $arr['@attributes'] as $keyATTR => $valueATTR )
				{
					# code...
					$attributes[ $keyATTR ] = $valueATTR;
				}
				if ( isset( $arr['_value'] ) )
				{
					# Code here
					$attributes['value'] = $arr['_value'];
					unset( $arr['_value'] );
				} // end of if statement
			} // end of else if statement
			unset( $arr['@attributes'] );
		} // end of if statement
		if ( isset( $arr['_value'] ) )
		{
			# Code here
			$arr = $arr['_value'];
		} // end of if statement

		if ( ! is_array( $arr ) )
		{
			# Code here
			return $arr;
		} // end of if statement
		$result = array_merge( $attributes, $arr );

#print_r($result);
#die();
		return $result;
	} // end of checkAttributesAndValues
}
