<?php

// session_start();
// set_time_limit(0);
// -------------------------------------------------------------------------
// Configuration file.  Ensure that only the user thant PHP runs under can
// access this file on your web server disk.
// -------------------------------------------------------------------------
// $isLive = true;
// $TAILORED = true;

// Replace _YOUR_ENCRYPTION_KEY_ with your encryption key from the PayWay web site.
// $encryptionKey =  '4aQ0FWxXkxjEPeoDjXwh/Q==';

// Replace _LOG_DIR_ with the directory to log debug messages to.
// $logDir =  './logs';

// Replace _BILLER_CODE_ with your biller code from the PayWay web site.
// $billerCode =  '172817';

// Replace _USERNAME_ with your PayWay Net username from the PayWay web site.
// $username =  'Q17281';

// Replace _PASSWORD_ with your PayWay Net password from the PayWay web site.
// $password =  'N4mk67axi';

// Replace _CA_FILE_ with the full path location of the cacerts.crt file.
// $caCertsFile =  'cacerts.crt';

// Change these parameters if your server requires a proxy server to connect
// to the PayWay server
$proxyHost = null;
$proxyPort = null;
$proxyUser = null;
$proxyPassword = null;

// Change this to your merchant ID when you are ready to go live
// $merchantId = 'TEST';
//$merchantId = '24206542';

// Change this to your configured paypal email address, if applicable
// $paypalEmail = 'test@example.com';

// Leave this entry as is
// $payWayBaseUrl = 'https://www.payway.com.au/';

if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[strtolower(str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))))] = $value;
               
           }
       }
       return $headers;
    }
}

if (!defined('BASEPATH')) exit('No direct script access allowed');
class PaymentWestpac {
	
    protected $CI;
    protected $isLive        = TRUE;
    protected $TAILORED      = TRUE;
    protected $encryptionKey = '';
    protected $logDir        = '';
    protected $billerCode    = '';
    protected $username      = '';
    protected $password      = '';
    protected $caCertsFile   = '';
    protected $merchantId    = '';
    protected $paypalEmail   = '';
    protected $payWayBaseUrl = '';

    public function __construct ( $config = array() ) {
        $this->CI =& get_instance();

        $default = array(
            'Production'    => FALSE,
            'isTailored'    => FALSE,
            'Certificate'   => '',
            'EncryptionKey' => '',
            'BillerCode'    => '',
            'Username'      => '',
            'Password'      => '',
            'MerchantId'    => '',
            'Email'         => '',
            'BaseURL'       => '',
        );

        $this->CI->config->load('PayWay');
        foreach ( $default as $key => $value )
        {
            # code...
            if ( isset( $config[ $key ] ) )
            {
                # Code here
                $default[ $key ] = $value;
                continue;
            } // end of if statement

            $default[ $key ] = $this->CI->config->item( $key );
        }

        $this->caCertsFile   = FCPATH . $default['Certificate'];
        $this->isLive        = $default['Production'];
        $this->TAILORED      = $default['isTailored'];
        $this->encryptionKey = $default['EncryptionKey'];
        $this->billerCode    = $default['BillerCode'];
        $this->username      = $default['Username'];
        $this->password      = $default['Password'];
        $this->merchantId    = $default['MerchantId'];
        $this->paypalEmail   = $default['Email'];
        $this->payWayBaseUrl = $default['BaseURL'];
        
        if ( ! file_exists( $this->caCertsFile ) )
        {
            # Code here
            echo "Certificate does not exists.";
            die();
        } // end of if statement
    }

    public function getEncryptionKey () { return $this->encryptionKey; }
    private function getBillerCode () { return $this->billerCode; }
    private function getUsername () { return $this->username; }
    private function getPassword () { return $this->password; }
    private function getMerchantId () { return $this->merchantId; }
    private function getPayPalEmail () { return $this->paypalEmail; }
    public function getPayWayBaseURL () { return $this->payWayBaseUrl; }

    private function isTailored () { return $this->TAILORED; }
    private function setTailored ( $isTailored ) { $this->TAILORED = $isTailored; }
    private function isLive () { return $this->isLive; }
    private function setLive ( $isLive ) { $this->isLive = $isLive; }
	
    /**
     * [getToken description]
     * @param  [type] $parameters [description]
     * @return [type]             [description]
     */
	public function getToken( $parameters ){
        
		global $proxyHost;
        global $proxyPort;
        global $proxyUser;
        global $proxyPassword;
        global $payWayBaseUrl;

        $credential = array(
            'username'     => $this->username,
            'password'     => $this->password,
            'merchant_id'  => $this->merchantId,
            'biller_code'  => $this->billerCode,
            'paypal_email' => $this->paypalEmail
        );
        
        $parameters = array_merge($credential, $parameters);

        // Find the port setting, if any.
        $payWayUrl = $this->getPayWayBaseURL();
        $port = 443;
        $portPos = strpos( $this->getPayWayBaseURL(), ":", 6 );
        $urlEndPos = strpos( $this->getPayWayBaseURL(), "/", 8 );
        if ( $portPos !== false && $portPos < $urlEndPos )
        {
            $port = (int)substr( $this->getPayWayBaseURL, ((int)$portPos) + 1, ((int)$urlEndPos));
            $payWayUrl = substr( $this->getPayWayBaseURL, 0, ((int)$portPos))
                . substr( $this->getPayWayBaseURL, ((int)$urlEndPos), strlen($this->getPayWayBaseURL));
            //debugLog( "Found alternate port setting: " . $port );
        }
        $ch = curl_init( $payWayUrl . "RequestToken" );
       // debugLog( "Configuring token request to : " . $payWayUrl . "RequestToken" );

        if ( $port != 443 )
        {
            curl_setopt( $ch, CURLOPT_PORT, $port );
            //debugLog( "Set port to: " . $port );
        }

        curl_setopt( $ch, CURLOPT_FAILONERROR, true );
        curl_setopt( $ch, CURLOPT_FORBID_REUSE, true );
        curl_setopt( $ch, CURLOPT_FRESH_CONNECT, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  
        // Set proxy information as required
        if ( !is_null( $proxyHost ) && !is_null( $proxyPort ) )
        {
            curl_setopt( $ch, CURLOPT_HTTPPROXYTUNNEL, true );
            curl_setopt( $ch, CURLOPT_PROXY, $proxyHost . ":" . $proxyPort );
            if ( !is_null( $proxyUser ) )
            {
                if ( is_null( $proxyPassword ) )
                {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, $proxyUser . ":" );
                }
                else
                {
                    curl_setopt( $ch, CURLOPT_PROXYUSERPWD, 
                        $proxyUser . ":" . $proxyPassword );
                }
            }
        }
  
        // Set timeout options
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
  
        // Set references to certificate files
        curl_setopt( $ch, CURLOPT_CAINFO, $this->caCertsFile );
  
        // Check the existence of a common name in the SSL peer's certificate
        // and also verify that it matches the hostname provided
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 2 );   
  
        // Verify the certificate of the SSL peer
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );
        
        // Build the parameters string to pass to PayWay
        $parametersString = '';
        $init = true;
        foreach ( $parameters as $paramName => $paramValue )
        {
            if ( $init )
            {
            		$init = false;
            }
            else
            {
                $parametersString = $parametersString . '&';
            }  
            $parametersString = $parametersString . urlencode($paramName) . '=' . urlencode($paramValue);
        }
        
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $parametersString );
        
       // debugLog( "Token Request POST: " . $parametersString );

        // Make the request  
        $responseText = curl_exec($ch);

        //debugLog( "Token Response: " . $responseText );

        // Check the response for errors
        $errorNumber = curl_errno( $ch );
        if ( $errorNumber != 0 )
        {
            trigger_error( "CURL Error getting token: Error Number: " . $errorNumber . 
                ", Description: '" . curl_error( $ch ) . "'" );
            echo "Errors: " . curl_error( $ch );
            //debugLog( "Errors: " . curl_error( $ch ) );
        }

        curl_close( $ch );

         // Split the response into parameters
        $responseParameterArray = explode( "&", $responseText );
        $responseParameters = array();
        foreach ( $responseParameterArray as $responseParameter )
        {
            list( $paramName, $paramValue ) = explode( "=", $responseParameter, 2 );
            $responseParameters[ $paramName ] = $paramValue;
        }

        if ( array_key_exists( 'error', $responseParameters ) )
        {
            trigger_error( "Error getting token: " . $responseParameters['error'] );
        }        
        else
        {
            return $responseParameters['token'];
        }
		
	}
	public function pkcs5_unpad($text)
    {
        $pad = ord($text{strlen($text)-1});
        if ($pad > strlen($text)) return false;
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false;
        return substr($text, 0, -1 * $pad);
    }

    public function decrypt_parameters( $base64Key, $encryptedParametersText, $signatureText )
    {
        $key = base64_decode( $base64Key );
        $iv = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
        $td = mcrypt_module_open('rijndael-128', '', 'cbc', '');

        // Decrypt the parameter text
        mcrypt_generic_init($td, $key, $iv);
        $parametersText = mdecrypt_generic($td, base64_decode( $encryptedParametersText ) );
        $parametersText = self::pkcs5_unpad( $parametersText );
        mcrypt_generic_deinit($td);

        // Decrypt the signature value
        mcrypt_generic_init($td, $key, $iv);
        $hash = mdecrypt_generic($td, base64_decode( $signatureText ) );
        $hash = bin2hex(self::pkcs5_unpad( $hash ) );
        mcrypt_generic_deinit($td);

        mcrypt_module_close($td);

        // Compute the MD5 hash of the parameters
        $computedHash = md5( $parametersText );

        // Check the provided MD5 hash against the computed one
        if ( $computedHash != $hash )
        {
            trigger_error( "Invalid parameters signature" );
        }

        $parameterArray = explode( "&", $parametersText );
        $parameters = array();

        // Loop through each parameter provided
        foreach ( $parameterArray as $parameter )
        {
            list( $paramName, $paramValue ) = explode( "=", $parameter );
            $parameters[ urldecode( $paramName ) ] = urldecode( $paramValue );
        }
        return $parameters;
    }
	
}

?>

