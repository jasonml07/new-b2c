<?php if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );

class Nativesession
{
    public function __construct()
    {
        $s_id = session_id();
        if( empty($s_id) ) {
            session_start();
        }
    }

    public function set( $key, $value )
    {
        $_SESSION[$key] = $value;
    }

    public function get( $key )
    {
        return isset( $_SESSION[$key] ) ? $_SESSION[$key] : null;
    }

    public function regenerateId( $delOld = false )
    {
        session_regenerate_id( $delOld );
    }

    public function delete( $key )
    {
        unset( $_SESSION[ $key ] );
    }

    /**
     * [timeout_get description]
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function timeout_get ( $key )
    {
        # Code here
        $session = $this->get( $key );

        if ( ! is_null( $session ) && isset( $session['LAST_ACTIVITY'] ) && (time() - $session['LAST_ACTIVITY']) > 600 )
        {
            # Code here
            $this->delete( $key );
            return FALSE;
        } // end of if statement

        return $session;
    } // end of timeout_get

    /**
     * [timeout_get description]
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function timeout_set ( $key, $data )
    {
        # Code here
        $data['LAST_ACTIVITY'] = time();
        $this->set( $key, $data );
    } // end of timeout_set
}