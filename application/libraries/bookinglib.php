<?php

/**
* 
*/
class BookingLib
{

  private $CI;
  private $BOOKING_ID = 0;
  private $ITEMS_ID   = 0;
  
  function __construct()
  {
    # code...
    $this->CI =& get_instance();
    $this->CI->load->model('booking_model');
    $this->CI->load->model('notification_model');
    $this->CI->load->helper('file');
  }

  /**
   * [init description]
   * @param  [type] $bookingId [description]
   * @param  [type] $itemsId   [description]
   * @return [type]            [description]
   */
  public function init ( $bookingId, $itemsId )
  {
    # Code here
    $this->BOOKING_ID = (int)$bookingId;

    if ( is_array( $itemsId ) )
    {
      # Code here
      $itemsId = array_map('intval', $itemsId);
    } else {
      $itemsId = (int) $itemsId;
    } // end of if statement

    $this->ITEMS_ID   = $itemsId;
  } // end of init

  /**
   * [checkInit description]
   * @return [type] [description]
   */
  private function checkInit (  )
  {
    # Code here
    $e = new Exception();
    $trace = $e->getTrace();
    if ( (int)$this->BOOKING_ID == 0 )
    {
      # Code here
      echo 'Booking ID are not defined from line ' . $trace[1]['line'];
      exit();
    } // end of if statement 
    if ( (int)$this->ITEMS_ID == 0 )
    {
      # Code here
      echo 'Items ID are not defined from line ' . $trace[1]['line'];
      exit();
    } // end of if statement
    return TRUE;
  } // end of checkInit


  /**
   * [buildCCParameter description]
   * @param  [type] $array [Parameters on Payway]
   * @return [type]        [List of items with its names]
   */
  public function buildCCParameters ( $array, $items )
  {
    # Code here
    $parameters = array();
    $parameters['username']    = PAYWAY_USERNAME;
    $parameters['password']    = PAYWAY_PASSWORD;
    $parameters['merchant_id'] = PAYWAY_MERCHAND_ID;
    $parameters['biller_code'] = PAYWAY_BILLER_CODE;


    $parameters = array_merge( $parameters, $array );

    $parameters = array_merge( $parameters, $items );

    return $parameters;
  } // end of buildCCParameter

  /**
   * [buildCCReceipt description]
   * @param  [type] $array [description]
   * @return [type]        [description]
   */
  public function buildCCReceipt ( $array )
  {
    # Code here
    $this->checkInit();

    $receipt                    = array();
    $receipt['receiptItemData'] = array();
    $receipt['receiptData']     = array( 'receiptTypeCode' => '',
      'receiptTypeName' => '',
      'receiptDescription' => '',
      'receiptReferenceNumber' => '',
      'receiptChequeNumber' => '',
      'receiptABAnumber' => '',
      'receiptAccountChequeNumber' => '',
      'receiptDrawersName' => '',
      'receiptBankCode' => '',
      'receiptBankName' => '',
      'receiptCCTypeCode' => '',
      'receiptCCTypeName' => '',
      'receiptCCNumber' => '',
      'receiptCCExpiryDate' => '',
      'receiptCCSecurityCode' => '',
      'receiptDisplayAmout' => '',
      'receiptAmout' => '',
      'receiptDepositedAmout' => '',
      'receiptRefundAmout' => '',
      'receiptDate' => '');

    foreach ( $array as $key => $value )
    {
      # code...
      $receipt['receiptData'][ $key ] = $value;
    }
    
    if ( ! is_array( $this->ITEMS_ID ) )
    {
      # Code here
      $receipt['receiptItemData'] = array( $this->CI->booking_model->getItemsAndGuest( $this->ITEMS_ID ) );
    } else {
      $receipt['receiptItemData'] = $this->CI->booking_model->getItemsAndGuests( $this->ITEMS_ID, $this->BOOKING_ID );
    } // end of else if statement
    $receipt['receiptData']['receiptDate'] = date('d/m/Y', strtotime('now'));

    return array(
      'action'       => 'createReceipt',
      'receiptDatas' => json_encode( $receipt ),
      'bookingID'    => $this->BOOKING_ID
    );
  } // end of buildCCReceipt

  /**
   * [buildCCReceiptAllocation description]
   * @param  [type]  $totalAmount  [description]
   * @param  [type]  $receiptId    [description]
   * @param  boolean $isCreditCard [description]
   * @return [type]                [description]
   */
  public function buildCCReceiptAllocation ( $totalAmount, $receiptId, $isCreditCard = FALSE )
  {
    # Code here
    $this->checkInit();

    $itemData            = $this->CI->booking_model->getItemsAndGuests( $this->ITEMS_ID, $this->BOOKING_ID, FALSE );
    $allocatedItemAmount = $this->CI->booking_model->getItemAllocated( $this->ITEMS_ID );

    foreach ( $itemData as $key => $ig )
    {
      if ( $isCreditCard )
      {
        # Code here
        $totalAmount -= (float)$ig['itemCostings']['totalDueAmt'];
      } // end of if statement
      
      $itemData[ $key ]['amountAlloc'] = (float)$ig['itemCostings']['totalDueAmt'];

      if( array_key_exists($ig['itemId'], $allocatedItemAmount) )
      {
        $itemData[ $key ] = array_merge( $itemData[ $key ], $allocatedItemAmount[ $ig['itemId'] ] );
      } else {
        if ( $isCreditCard )
        {
          # Code here
          $totalAmount -= (float)$ig['itemCostings']['totalDueAmt'];
        } // end of if statement
        $itemData[ $key ]['itemOutStandingAllocated'] = (float)$ig['itemCostings']['totalDueAmt'];
        $itemData[ $key ]['itemTotalAmountAllocated'] = 0;
      }
    }

    $includes = array("receiptId" => 1,"receipt_reference" => 1,"receiptTypeName" => 1,"receiptDescription" => 1,"receiptReferenceNumber" => 1,"receiptChequeNumber" => 1,"receiptABAnumber" => 1,"receiptDrawersName" => 1,"receiptBankCode" => 1,"receiptBankName" => 1,"receiptCCTypeCode" => 1,"receiptCCTypeName" => 1,"receiptCCNumber" => 1,"receiptCCExpiryDate" => 1,"receiptCCSecurityCode" => 1,"receiptDisplayAmout" => 1,"receiptAmout" => 1,"receiptDepositedAmout" => 1,"receiptRefundAmout" => 1,"documentId" => 1,"is_active" => 1,"is_refunded" => 1,"is_allocated" => 1,"created_datetime" => 1,"receipt_datetime" => 1,"_id" => 0);
    $connection = new MongoClient();
    $db         = $connection->db_system;

    $receiptData                              = array();
    $receiptData                              = $db->receipt->findOne( array( 'receiptId' => intval($receiptId) ), $includes );
    $receiptData['receiptId']                 = $receiptId;
    $receiptData['receiptDepositedAmout']     = $totalAmount;
    $receiptData['allocate_amout_to_refund']  = 0;
    $receiptData['allocate_amout_to_receipt'] = 0;
    $receiptData['allocate_datetime']         = date('d/m/y', strtotime('now'));
    $receiptData['allocateAmount']            = $totalAmount;
    $receiptData['itemData']                  = $itemData;

    $allocations = array(
      'action'          => 'createAllocation',
      'allocationDatas' => json_encode($receiptData),
      'bookingID'       => $this->BOOKING_ID
    );

    return $allocations;
  } // end of buildCCReceiptAllocation

  /**
   * [buildENParameters description]
   * @param  [type] $array [description]
   * @return [type]        [description]
   */
  public function buildENParameters ( $array )
  {
    # Code here
    $this->checkInit();

    // $receipt = $this->CI->booking_model->eNetPayment( $this->BOOKING_ID, $array, ((! is_array($this->ITEMS_ID))?array($this->ITEMS_ID):$this->ITEMS_ID) );
    $receipt = $this->CI->booking_model->eNetPayment( $this->BOOKING_ID, $array );

    return array(
      'action'       => 'createReceipt',
      'receiptDatas' => json_encode($receipt),
      'bookingID'    => $this->BOOKING_ID
    );
  } // end of buildENParameters

  /**
   * [updateItems description]
   * @param  [type]  $receiptId   [description]
   * @param  integer $isConfirmed [description]
   * @return [type]               [description]
   */
  public function updateItems ( $receiptId, $isConfirmed = 0, $otherProperty = array() )
  {
    # Code here
    $this->checkInit();
    
    $connection = new MongoClient();
    $db = $connection->db_system;

    $match = array(
      'bookingId' => $this->BOOKING_ID,
      'itemId'    => array('$in' => (( is_array( $this->ITEMS_ID ) )? $this->ITEMS_ID: array( $this->ITEMS_ID )))
    );
    $update = array(
      'itemIsReceipt'   => 1,
      'itemIsConfirmed' => $isConfirmed
    );

    if ( ! empty( $otherProperty ) )
    {
      # Code here
      $update = array_merge($update, $otherProperty);
    } // end of if statement

    $db->items->update(
      $match,
      array( '$set'     => $update ),
      array( 'multiple' => TRUE )
    );

    $db->receipt->update(
      array('receiptId' => (int)$receiptId ),
      array( '$set'     => array( 'is_allocated' => 1 ) )
    );

    $connection->close();
  } // end of updateItems

  /**
   * [buildItemArray description]
   * @param  [type] $array [description]
   * @return [type]        [description]
   */
  public function buildItemArray ( $array )
  {
    # Code here
    $item = array();
    $item['bookingId']                         = '';
    $item['itemId']                            = '';
    $item['itemName']                          = '';
    $item['itemCreatedType']                   = '';
    $item['itemPhone']                         = '';
    $item['itemRatings']                       = 0;
    $item['itemPropertyCategory']              = 0;
    $item['itemSeq']                           = 0;
    $item['itemStockType']                     = '';
    $item['itemStockTypeName']                 = '';
    $item['itemSupplierCode']                  = '';
    $item['itemSupplierName']                  = '';
    $item['itemLogsReference']                 = '';
    $item['itemSupplierCode']                  = '';
    $item['itemSupplierName']                  = '';
    $item['itemCode']                          = '';
    $item['itemServiceCode']                   = '';
    $item['itemServiceName']                   = '';
    $item['itemSumarryDescription']            = '';
    $item['itemDetailedDescription']           = '';
    $item['itemCostingsDescription']           = '';
    // List of Board Types and names of room''s
    $item['itemInclusionDescription']          = '';
    
    $item['itemExclusionDescription']          = '';
    $item['itemConditionsDescription']         = '';
    $item['itemStatus']                        = '';
    $item['itemSuplierConfirmation']           = '';
    $item['itemSuplierOtherConfirmation']      = '';
    $item['itemSuplierResReq']                 = '';
    // This should be a vouchers city
    $item['itemFromCountry']                   = '';
    $item['itemFromCity']                      = '';
    $item['itemFromAddress']                   = '';
    $item['itemToCountry']                     = '';
    $item['itemToCity']                        = '';
    $item['itemToAddress']                     = '';
    $item['itemStartDate']                     = '';
    $item['itemEndDate']                       = '';
    $item['itemStartTime']                     = '12:00:00';
    $item['itemEndTime']                       = '12:00:00';
    $item['itemNumDays']                       = '';
    $item['itemNumNights']                     = '';
    $item['itemIsLeadPax']                     = 0;
    $item['itemIsFullPax']                     = 0;
    $item['itemIsVoucherable']                 = 0;
    $item['itemIsAutoInvoice']                 = 0;
    $item['itemIsInvoicable']                  = 0;
    $item['itemIsIterary']                     = 0;
    $item['itemIsGstApplicable']               = 0;
    $item['itemIsDeposited']                   = 0;
    $item['itemIsFullyPaid']                   = FALSE;
    $item['itemAdultMin']                      = 0;
    $item['itemAdultMax']                      = 0;
    $item['itemChildMin']                      = 0;
    $item['itemChildMax']                      = 0;
    $item['itemInfantMin']                     = 0;
    $item['itemInfantMax']                     = 0;
    $item['itemIsCancelled']                   = 0;
    $item['itemIsPaxAllocated']                = 0;
    $item['itemIsReceipt']                     = 0;
    $item['itemIsInvoiced']                    = 0;
    $item['itemIsPaid']                        = 0;
    $item['itemIsConfirmed']                   = 0;
    $item['itemIsBlank']                       = 0;
    $item['itemIsLive']                        = 0;
    $item['itemCancellation']                  = array();
    $item['itemSupplements']                   = array();
    $item['itemCostings']                      = array();
    
    $item['itemCostings']['fromCurrency']      = '';
    $item['itemCostings']['fromRate']          = 0;
    $item['itemCostings']['toCurrency']        = '';
    $item['itemCostings']['toRate']            = 0;
    $item['itemCostings']['originalAmount']    = 0;
    $item['itemCostings']['orginalNetAmt']     = 0;
    $item['itemCostings']['netAmountInAgency'] = 0;
    $item['itemCostings']['markupInAmount']    = 0;
    $item['itemCostings']['markupInPct']       = 0;
    $item['itemCostings']['grossAmt']          = 0;
    $item['itemCostings']['commInPct']         = 0;
    $item['itemCostings']['commInAmount']      = 0;
    $item['itemCostings']['commAmt']           = 0;
    $item['itemCostings']['discInPct']         = 0;
    $item['itemCostings']['discInAmount']      = 0;
    $item['itemCostings']['discAmt']           = 0;
    $item['itemCostings']['totalLessAmt']      = 0;
    $item['itemCostings']['totalDueAmt']       = 0;
    $item['itemCostings']['totalProfitAmt']    = 0;
    $item['itemCostings']['gstInPct']          = 0;
    $item['itemCostings']['gstInAmount']       = 0;
    $item['itemCostings']['gstAmt']            = 0;
    $item['systemType']                        = SYSTEM_TYPE;
    $item['application']                       = '';
    $item['PAYMENT_TYPE']                      = '';
    $item['VOUCHER_EMAIL']                     = '';


    foreach ( $array as $key => $value )
    {
      # code...
      if ( isset( $item[ $key ] ) )
      {
        # Code here
        if ( is_array( $value ) ) {
          # code...
          if ( ! empty( $value ) )
          {
            # Code here
            foreach ( $value as $subKey => $subValue )
            {
              # code...
              $item[ $key ][ $subKey ] = $subValue;
            }
          } // end of if statement
        } else {
          # code...
          $item[ $key ] = $value;
        }
        
      } // end of if statement
    }

    $item['itemCreatedDate'] = new MongoDate(strtotime("now"));

    return $item;
  } // end of buildItemArray

  /**
   * [buildGuest description]
   * @param  [type]  $title [description]
   * @param  [type]  $fname [description]
   * @param  [type]  $lname [description]
   * @param  integer $age   [description]
   * @return [type]         [description]
   */
  public function buildGuest ( $title, $fname, $lname, $age = 0, $agency = '' )
  {
    # Code here
    $guest = array();
    $guest['action']                = 'createGuest';
    $guest['guestTitle_combo_name'] = ($title != 'C')? $title: '';
    $guest['guest_last_name']       = $lname;
    $guest['guest_agency_code']     = $this->CI->session->userdata('agencycode');
    $guest['guest_agency_name']     = $agency;
    $guest['guest_business']        = '';
    $guest['guest_fax']             = '';
    $guest['guest_first_name']      = $fname;
    $guest['guest_home']            = '';
    $guest['guest_mobile']          = '';
    $guest['guest_age']             = $age;

    return $guest;
  } // end of buildGuest

  /**
   * [buildAllocationFields description]
   * @param  [type] $receiptId       [description]
   * @param  [type] $amountToDeposit [description]
   * @return [type]                  [description]
   */
  public function buildAllocationFields ( $receiptId, $amountToDeposit )
  {
    # Code here
    $this->checkInit();

    $fields = array(
      'action'          => 'createAllocation',
      'allocationDatas' => '',
      'bookingID'       => $this->BOOKING_ID
    );
    // Get the receipt
    $connection = new MongoClient();
    $db         = $connection->db_system;
    
    $includes   = array(
      "receiptId"              => 1,
      "receipt_reference"      => 1,
      "receiptTypeName"        => 1,
      "receiptDescription"     => 1,
      "receiptReferenceNumber" => 1,
      "receiptChequeNumber"    => 1,
      "receiptABAnumber"       => 1,
      "receiptDrawersName"     => 1,
      "receiptBankCode"        => 1,
      "receiptBankName"        => 1,
      "receiptCCTypeCode"      => 1,
      "receiptCCTypeName"      => 1,
      "receiptCCNumber"        => 1,
      "receiptCCExpiryDate"    => 1,
      "receiptCCSecurityCode"  => 1,
      "receiptDisplayAmout"    => 1,
      "receiptAmout"           => 1,
      "receiptDepositedAmout"  => 1,
      "receiptRefundAmout"     => 1,
      "documentId"             => 1,
      "is_active"              => 1,
      "is_refunded"            => 1,
      "is_allocated"           => 1,
      "created_datetime"       => 1,
      "receipt_datetime"       => 1,
      "_id"                    => 0

    );
    $receiptData = array();
    $receiptData['receiptData']               = $db->receipt->findOne( array( 'receiptId' => intval($receiptId) ), $includes );

    $receiptData['receiptId']                 = $receiptId;
    $receiptData['receiptDepositedAmout']     = (float)$amountToDeposit;
    $receiptData['allocate_amout_to_refund']  = 0;
    $receiptData['allocate_amout_to_receipt'] = 0;
    $receiptData['allocate_datetime']         = date('d/m/y', strtotime('now'));
    $receiptData['allocateAmount']            = (float)$amountToDeposit;


    $itemData = $db->items->findOne( array('itemId' => intval($this->ITEMS_ID)), array('_id' => 0) );
    $itemData['itemGuests'] = $db->item_guest->find( array('item_id' => intval($this->ITEMS_ID)) );
    $itemData['itemGuests'] = iterator_to_array( $itemData['itemGuests'] );

    // get all allocated items

    $allocated = $db->item_allocated->find( array('item_id' => intval($this->ITEMS_ID), 'is_active' => 0) );
    $allocated = iterator_to_array( $allocated );

    $itemData['itemOutStandingAllocated'] = (float)$itemData['itemCostings']['totalDueAmt'];
    $itemData['itemTotalAmountAllocated'] = 0;
    $itemData['amountAlloc']              = (float)$amountToDeposit;


    if( count($allocated) > 0 )
    {
      foreach ( $allocated as $key => $al )
      {
        $itemData['itemTotalAmountAllocated'] += (float)$al['amount_paid'];
      }

      $itemData['itemOutStandingAllocated'] -= $itemData['itemTotalAmountAllocated'];
    }

    $receiptData['itemData']   = array($itemData);
    
    $fields['allocationDatas'] = json_encode($receiptData);
    return $fields;
  } // end of buildAllocationFields

  /**
   * [executeItemGuests description]
   * @param [type] $guests [description]
   */
  public function executeItemGuests ( $guests )
  {
    # Code here
    $this->checkInit();

    $paxIds = array();

        foreach ( $guests as $key => $guest )
        {
          $result = $this->cURL( 'execute_guest_manager', $guest );

      if( isset($result['success']) && $result['success'] && isset($result['guestPaxID']) )
      {
        // $this->log_model->debug('execute_guest_manager (Success) -> ' . json_encode($result));
        $params = array(
          'action'    => 'addGuestToBooking',
          'bookingID' => $this->BOOKING_ID,
          'paxID'     => $result['guestPaxID']
        );
        array_push($paxIds, trim($result['guestPaxID']));

        $result = $this->cURL( 'execute_pax_item', $params );
        /*if( isset($result['success']) && $result['success'] )
        {
          // Data was susccessfuly inserted
        } else {
          // echo json_encode($result);
          $this->log_model->debug('execute_pax_item: ' . json_encode($result));
        }*/
      } else {
        return array('success' => FALSE, 'data' => $result );
      }
        }

        return array('success' => TRUE, 'paxIds' => $paxIds);
  } // end of executeItemGuests

  /**
   * [executeAllocateGuests description]
   * @param  [type] $paxIds [description]
   * @return [type]         [description]
   */
  public function executeAllocateGuests ( $paxIds )
  {
    # Code here
    $this->checkInit();
    
    $paxIds = implode(',', $paxIds);
        $params = array(
      $this->ITEMS_ID => $paxIds,
      'action'        => 'saveGuest',
      'bookingID'     => $this->BOOKING_ID
        );

        // $this->log_model->debug(PHP_EOL . 'execute_guest_manager: ' . json_encode($params));
        $result = $this->cURL( 'execute_pax_item', $params );

        return $result;
  } // end of executeAllocateGuests

  /**
   * [executeConvertItem description]
   * @param  [array/int] $itemId [description]
   * @return [type]         [description]
   */
  public function executeConvertItem (  )
  {
    # Code here
    $this->checkInit();
    
    $params = array(
      'action'    => 'convertItem',
      'itemId'    => $this->ITEMS_ID,
      'bookingID' => $this->BOOKING_ID
    );

    $result = $this->cURL( 'execute_item_manager', $params );
    
    return $result;
  } // end of executeConvertItem

  /**
   * [executeVoucher description]
   * @return [type] [description]
   */
  public function executeVoucher (  )
  {
    # Code here
    $items = ( is_array( $this->ITEMS_ID )? $this->ITEMS_ID: array((int)$this->ITEMS_ID) );
    $voucherParams = array(
      'action'    => 'createVoucherItems',
      'itemIDs'   => json_encode( $items ),
      'bookingID' => $this->BOOKING_ID
    );
    $result = $this->cURL( 'execute_document_manager', $voucherParams );

    return $result;
  } // end of executeVoucher

  /**
   * [executeReceipt description]
   * @param  [type] $params [description]
   * @return [type]         [description]
   */
  public function executeReceipt ( $params )
  {
    # Code here
    $result = $this->cURL( 'execute_receipt_manager2', $params );

    return $result;
  } // end of executeReceipt

  /**
   * [executeReceiptAllocation description]
   * @param  [type] $params [description]
   * @return [type]         [description]
   */
  public function executeReceiptAllocation ( $params )
  {
    # Code here
    return $this->executeReceipt( $params );
  } // end of executeReceiptAllocation


  /**
   * [executeAddItemToBooking description]
   * @param  [type] $params [description]
   * @return [type]         [description]
   */
  public function executeAddItemToBooking ( $data )
  {
    $code='';
    switch ($this->CI->nativesession->get('agencyRealCode')) {
      case 'EET':
        $code=$this->CI->nativesession->get('agencyRealCode');
        break;
      case 'EuropeTravelDeals':
        $code='EH';
        break;

      case 'ifly':
                                $code='ifly';
                                break;
    }
    $data['descType']=$code;
    $params = array('action' => 'addItemToBooking', 'postDetails' => json_encode($data));
  $result = $this->cURL('execute_item_manager', $params);
  
  return $result;
  } // end of executeAddItemToBooking


  /**
   * [executeItirenary description]
   * @return [type] [description]
   */
  public function executeItirenary (  )
  {
    # Code here
    $params = array(
      'action'    => 'allItineraryItem',
      'itemIDs'   => json_encode(( is_array( $this->ITEMS_ID )? $this->ITEMS_ID: array( $this->ITEMS_ID ) )),
      'bookingID' => $this->BOOKING_ID
    );
    $result = $this->cURL('execute_itinerary_manager', $params);

    return $result;
  } // end of executeItirenary

  /**
   * [getBookingId description]
   * @return [type] [description]
   */
  public function getBookingId (  )
  {
    # Code here
    return $this->BOOKING_ID;
  } // end of getBookingId

  /**
   * [getItemIds description]
   * @return [type] [description]
   */
  public function getItemIds (  )
  {
    # Code here
    return $this->ITEMS_ID;
  } // end of getItemIds


  /**
   * [getItemsInfo description]
   * @param  boolean $isAgent [description]
   * @return [type]           [description]
   */
  public function getItemsInfo ( $isAgent = FALSE )
  {
    # Code here
    $connection = new MongoClient();
    $db = $connection->db_system;

    $priceType = ($isAgent?'grossAmt':'totalDueAmt');

    /*$aggregate = array(
        array( '$match' => array( 'bookingId' => $this->BOOKING_ID, 'itemId' => array( '$in' =>  ( is_array( $this->ITEMS_ID )? $this->ITEMS_ID: array( $this->ITEMS_ID ) ) ) ) ),
        array( '$group' => array(
        '_id'   => '$bookingId',
        'items' => array(
          '$addToSet' => array(
            'price'     => '$itemCostings.' . $priceType,
            'discount'  => '$itemCostings.discInAmount',
            'name'      => '$itemName',
            'startdate' => '$itemStartDate',
            'enddate'   => '$itemEndDate',
            )
          ),
          'total' => array(
            '$sum' => '$itemCostings.' . $priceType
          ),
          'discount' => array(
            '$sum' => '$itemCostings.discInAmount'
          )
        ) )
      );
    $items = $db->items->aggregate( $aggregate );

    $connection->close();*/
    $items=$db->items->find(array('bookingId'=>$this->BOOKING_ID),array('_id'=>0,'itemCostings'=>1,'itemSupplements'=>1,'itemId'=>1,'itemName'=>1,'itemStartDate'=>1,'itemEndDate'=>1,'itemServiceCode'=>1));
    $connection->close();

    $items=iterator_to_array($items);

    $_items=array();
    // Loop through items and supplements
    foreach ($items as $key => $item) {
        $_item=array(
            'id'              =>$item['itemId'],
            'type'            =>(($item['itemServiceCode']=='FLT')?'flight':'product'),
            'name'            =>$item['itemName'],
            'startdate'       =>$item['itemStartDate'],
            'enddate'         =>$item['itemEndDate'],
            'price'           =>$item['itemCostings'][$priceType],
            'totalLessAmount' =>$item['itemCostings']['totalLessAmt'],
        );
        if (strtolower($item['itemServiceCode'])=='service fee') {
          $_item['type']="service";
        }
        array_push($_items, $_item);
        if (isset($item['itemSupplements'])&&!empty($item['itemSupplements'])) {
            foreach ($item['itemSupplements'] as $key => $supplement) {
                $_item=array(
                    'id'              =>$item['itemId'],
                    'type'            =>'supplement',
                    'name'            =>$supplement['supplement_name'].' X'.$supplement['supplement_qty'],
                    'startdate'       =>$item['itemStartDate'],
                    'enddate'         =>$item['itemEndDate'],
                    'price'           =>$supplement['supplement_costings'][$priceType],
                    'totalLessAmount' =>$supplement['supplement_costings']['totalLessAmt'],
                );
                array_push($_items, $_item);
            }
        }
    }
    $total=0;
    foreach ($_items as $key => $value) {
        $total+=$value['price'];
    }
    return array(
      'items'=>$_items,
      'total'=>$total
    );
  } // end of getItemsInfo

  public function sendVoucher ( $data, $isReceipt = FALSE, $disableVoucher=FALSE )
  {
    # Code here
    $this->checkInit();
    // On the $data
    // * Amount Paid
    // * Voucher data
    // TODO
    // * Get the booking created date
    // * Booking ID
    // * Items availed
    $_sending = array();
    $_sending['information'] = array(
      'bookingID'   => $this->BOOKING_ID,
      'AGENCY_INFO' => _AGENT_INFO( $this->CI->nativesession->get('agencyRealCode') )
    );
    # 'voucherName' => $data['voucher']['name'],
    # 'paidAmount'  => ( isset( $data['payment']['paid'] )? $data['payment']['paid']: '' ),
    # 'bookingID'   => $this->BOOKING_ID
    #);

    if ( isset( $data['data'] ) )
    {
      # Code here
      $_sending['information'] = array_merge($_sending['information'], $data['data']);
    } // end of if statement
    $_sending['email'] = array(
      'to'          => $data['emailTo'],
      'from'        => EMAIL_USERNAME,
      'subject'     => $_sending['information']['AGENCY_INFO']['agency_name'] . ' ' . (isset($_sending['information']['productType'])? $_sending['information']['productType'] : '') . ' Booking Advice #' . $this->BOOKING_ID,
      'name'        => (( SYSTEM_TYPE == 'B2B' )?EMAIL_NAME:$this->CI->nativesession->get('agency_name')),
      'content'     => ( isset( $data['content'] )? $data['content']: 'No Content' ),
      'attachments' => array()
    );
    if ($this->CI->nativesession->get('agencyRealCode')=='EET') {
      $_sending['email']['CC']=EMAIL_CC.', reservstions@easterneurotours.com.au';
    }

    $connection = new MongoClient();
    $db = $connection->db_system;

    $book = $db->booking->findOne(
      array( 'booking_id' => $this->BOOKING_ID ),
      array('booking_agency_consultant_code' => 1, '_id' => 0, 'booking_creation_date' => 1)
    );
    $_sending['information']['bookCreated'] = date('F d, Y', $book['booking_creation_date']->sec);


    #$aggregate = array(
      # array( '$match' => array( 'bookingId' => $this->BOOKING_ID, 'itemId' => array( '$in' =>  ( is_array( $this->ITEMS_ID )? $this->ITEMS_ID: array( $this->ITEMS_ID ) ) ) ) ),
      # array( '$group' => array(
    #   '_id'   => '$bookingId',
    #   'items' => array(
    #     '$addToSet' => array(
    #       'price'     => '$itemCostings.' . (@$data['data']['isAgent']?'totalDueAmt':'grossAmt'),
    #       'name'      => '$itemName',
    #       'startdate' => '$itemStartDate',
    #       'enddate'   => '$itemEndDate',
      #     )
      #   ),
      #   'total' => array(
      #     '$sum' => '$itemCostings.' . (@$data['data']['isAgent']?'totalDueAmt':'grossAmt')
      #   )
      # ) )
      #);
    #$items   = $db->items->aggregate( $aggregate );
    $_sending['information']['product'] = $this->getItemsInfo( ( @$data['data']['isAgent'] && $data['data']['isAgent'] ) );

      if ( $isReceipt )
      {
        # Code here
        array_push($_sending['email']['attachments'], ADMIN_SYSTEM_DOCS . 'receipt-' . $data['payment']['receiptId'] . '-' . $this->BOOKING_ID . '.pdf');
      } else {

        /*=====================================
        =            VOUCHER BLOCK            =
        =====================================*/
        
        if ( ( @$data['data']['isAgent'] && $data['data']['isAgent'] ) || $data['data']['PAYMENT_TYPE'] == 'PAYPAL' )
        {
          # Code here
          $voucher = $db->item_vouchers->find( array('bookingId' => $this->BOOKING_ID ), array( '_id' => 0 ) )->sort( array( 'voucherId' => -1 ) )->limit(1);
        $voucher = iterator_to_array( $voucher );
          if ( ! empty( $voucher )&&$disableVoucher==FALSE )
          {
            # Code here
            $voucher = $voucher[0];

            array_push($_sending['email']['attachments'], ADMIN_SYSTEM_DOCS . 'voucher-' . $voucher['voucher_reference'] . '.pdf');
          } // end of if statement
        } // end of if statement
        
        /*=====  End of VOUCHER BLOCK  ======*/
        
      } // end of else if statement
      if ( $data['bookType'] == 'External Tours' )
      {
        # Code here
        $itirenary = $db->itinerary->find( array('bookingId' => $this->BOOKING_ID), array('_id' => 0) )->sort(array('itineraryId' => -1))->limit(1);
        $itinerary = iterator_to_array($itirenary);

        if ( count( $itirenary ) > 0 )
        {
          # Code here
          $itinerary = $itinerary[0];
          $filename = 'itinerary-'.$itinerary['itinerary_reference'] . '.pdf';
          $path = SYS_ADMIN_IP.'_documents/'.$filename;
          $location = FCPATH.'documents/'.$filename;
          save_url_file($path, $location, 'r+', true);
          if ( isset( $_sending['email']['attachments'] ) )
          {
            # Code here
            if ( count( $_sending['email']['attachments'] ) > 0 )
            {
              # Code here
              // $document = SYS_ADMIN_IP.'_documents/'.'itinerary-' . $itinerary['itinerary_reference'] . '.pdf';
              // $response = file_get_contents($document);
              // $path = FCPATH.'documents/'. 'itinerary-' . $itinerary['itinerary_reference'] . '.pdf';
              array_push($_sending['email']['attachments'], $location);
            } else {
              $_sending['email']['attachments'] = array($location);
            }// end of if statement
          } // end of if statement
        } // end of if statement
      } // end of if statement
      $connection->close();

      // response(array(
      //  'success' => FALSE,
      //  'debug' => $_sending
      // ), 403);
      $this->CI->notification_model->log(array(
        'amount'                     => ( isset( $data['payment']['paid'] )? $data['payment']['paid']: '' ),
        'itemIds'                    => ( is_array( $this->ITEMS_ID )? $this->ITEMS_ID: array( $this->ITEMS_ID ) ),
        'isPayment'                  => 1,
        'receiptId'                  => ( isset( $data['payment']['receiptId'] )? $data['payment']['receiptId']: '' ),
        'bookingId'                  => $this->BOOKING_ID,
        'description'                => ( isset( $data['payment']['type'] )? 'A payment from the booking via ' . $data['payment']['type']: '' ),
        'isCreditCard'               => 0,
        'isEnett'                    => 0,
        'payment_type'               => (@$data['payment']['type']?$data['payment']['type']:''),
        'notificationLoginReference' => 'B2C',
        'voucherName'                => $data['voucher']['name'],
        'voucherEmail'               => $data['voucher']['email'],
        'subSystem'                  => $data['bookType'],
        'isBooked'                   => 1,
        'xml_reference'              => $this->CI->nativesession->get( SYSTEM_TYPE . 'search_time')
      ));
      $this->CI->notification_model->bookingEmail(array(
        'bookingId'  => $this->BOOKING_ID,
        'created_at' => $_sending['information']['bookCreated'],
        'items'      => $_sending['information']['product'],
        'payment'    => $data['payment'],
        'pax'        => $data['pax'],
        'data'       => ( isset( $data['data'] )? $data['data']: array() )
      ));
      $this->CI->notification_model->sendMail( $_sending );
  } // end of emailVoucher


  public function sendFlighInfo ( $data )
  {
    # Code here
    $data['booking_id'] = $this->BOOKING_ID;

    $data['items'] = $this->getItemsInfo();

    $data['receiver']    = EMAIL_NAME;
    $data['AGENCY_INFO'] = _AGENT_INFO( $this->CI->nativesession->get('agencyRealCode') );

    $_sending['email'] = array(
      'to'      => ( EMAIL_IS_TEST?EMAIL_TEST: EMAIL_USERNAME ),
      'from'    => $data['AGENCY_INFO']['email'],
      'subject' => 'Flight Info',
      'name'    => EMAIL_NAME,
      'content' => 'emails/tour/flight-info',
      'CC'      => 'czarbaluran@gmail.com,info@travelrez.net.au,sally@easterneurotours.com.au'
    );
    $_sending['information'] = $data;

    $this->CI->notification_model->sendMail( $_sending );
  } // end of sendFlighInfo

  /**
   * [sendCreditCardInfo description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function sendCreditCardInfo ( $card, $isAgent = FALSE )
  {
    # Code here
    $data['booking_id'] = $this->BOOKING_ID;

    
    $data['product']     = $this->getItemsInfo( $isAgent );
    $data['receiver']    = 'Select World Travel';
    $data['card']        = $card;
    $data['AGENCY_INFO'] = _AGENT_INFO( $this->CI->nativesession->get('agencyRealCode') );

    $_sending['email'] = array(
      'to'      => ( EMAIL_IS_TEST?EMAIL_TEST: 'info@selectworldtravel.com.au' ),
      'from'    => 'info@selectworldtravel.com.au',
      'subject' => $data['AGENCY_INFO']['agency_name'] . ' Card Details',
      'name'    => EMAIL_NAME,
      'content' => 'emails/tour/payment/credit-card-info',
      'CC'      => 'czarbaluran@gmail.com,info@travelrez.net.au',
    );
    $_sending['information'] = $data;

    $this->CI->notification_model->sendMail( $_sending );
  } // end of sendCreditCardInfo

  /**
   * [saveBooking description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function saveBooking ( $collection, $data )
  {
    # Code here
    $connection = new MongoClient();
    $db         = $connection->bookings;
    $collection = $db->selectCollection( $collection );

    $collection->insert( $data );
    $connection->close();
  } // end of saveBooking

  /**
   * [clear description]
   * @return [type] [description]
   */
  public function clear (  )
  {
    # Code here
    $this->ITEMS_ID   = 0;
    $this->BOOKING_ID = 0;
  } // end of clear

  public function rollback(){
    $connection=new MongoClient();
    $db=$connection->db_system;
    $db->booking->remove(array('booking_id'=>$this->BOOKING_ID));
    $db->items->remove(array('bookingId'=>$this->BOOKING_ID));
    $connection->close();
  }

  public function failed($message){
    $connection=new MongoClient();
    $db=$connection->db_system;
    $db->booking->update(array('booking_id'=>$this->BOOKING_ID),array('$set'=>array('booking_status_message'=>$message)));
    $connection->close();
  }
  /**
   * [cURL description]
   * @param  [type] $url       [description]
   * @param  [type] $params    [description]
   * @param  string $customURL [description]
   * @return [type]            [description]
   */
  private function cURL ( $url, $params, $customURL = '' )
  {
    # Code here
    $mainURL = SYS_ADMIN_IP . '_ajax/' . $url . '.php?agency_code=' . $this->CI->nativesession->get('agencyRealCode');
    if ( ! empty( $customURL ) )
    {
      # Code here
      $mainURL = $customURL;
    } // end of if statement
    $ch = curl_init(); 
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
        curl_setopt($ch, CURLOPT_URL, $mainURL); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);


  $result = json_decode($result, TRUE);
    
    // $CI->log_model->debug( 'CURL :: ' . json_encode(curl_getinfo($ch)) );
    curl_close($ch);
    return $result;
  } // end of cURL
}
