<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Payway {
	protected $CI;
	private $CONFIG;

	public function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->config('payway');
		$this->CONFIG=array(
			'ENV'=>$this->CI->config->item('ENV'),
			'API_KEY'=>$this->CI->config->item('API_KEY'),
			'SEC_KEY'=>$this->CI->config->item('SEC_KEY'),
			'MERCHANT'=>$this->CI->config->item('MERCHANT'),
		);
	}

	public function takePayment($data){
		$data['merchantId']=$this->CONFIG['MERCHANT'];
		return $this->cURL('transactions',$data,FALSE);
	}
	public function singleUseToken($data){
		return $this->cURL('single-use-tokens',$data);
	}
	public function getCustomer($ref){
		return $this->cURL('customers/'.$ref,array());
	}

	private function cURL($action,$data=array(),$is_api_key=TRUE){
		$key=$is_api_key?$this->CONFIG['API_KEY']:$this->CONFIG['SEC_KEY'];
		
		$ch = curl_init();
		$header = array();
		if (!empty($data)) {
			$header[] = 'Content-type: application/x-www-form-urlencoded';
		}
		$header[] = 'Accept: application/json';
		$header[] = 'Authorization: Basic '.base64_encode($key.':');
		curl_setopt($ch, CURLOPT_URL, "https://api.payway.com.au/rest/v1/".$action);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
		// curl_setopt($ch, CURLOPT_HTTP200ALIASES, array(400,403));
		
		if (!empty($data)) {
			$strData='';

			foreach ($data as $key => $value) {
				if (!empty($strData)) {
					$strData.='&';
				}
				$strData.=$key.'='.$value;
			}

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$strData);
		}
		$output = curl_exec($ch);
		$output=json_decode($output,TRUE);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$badHttpCodes=array(400,401,403,404,405,406,407,409,415,422,429,500,501,503);
		if (in_array($httpcode, $badHttpCodes)) {
			throw new Exception((isset($output['data'][0]['message'])?$output['data'][0]['message']:'Invalid credit card.'), 1);
		}
		curl_close($ch);
		return $output;
	}
}