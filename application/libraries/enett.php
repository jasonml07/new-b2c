<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Enett {
	protected $CI;
	private $CONFIG;

	public function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->config('enett');
		$this->CONFIG=array(
			'ENV'=>$this->CI->config->item('ENV'),
			'INTEGRATOR'=>$this->CI->config->item('INTEGRATOR'),
			'KEY'=>$this->CI->config->item('KEY'),
			'ECN'=>$this->CI->config->item('ECN'),
			'URL'=>$this->CI->config->item('URL'),
		);
	}

	public function processDebit($data){
		return $this->cURL('DebitService/debitservice.asmx/Process_Direct_Debit',$data);
	}

	private function cURL($action,$data=array(),$is_api_key=TRUE){
		$data['integratorKey']=$this->CONFIG['KEY'];
		$data['integrator']=$this->CONFIG['INTEGRATOR'];
		$data['ECN']=$this->CONFIG['ECN'];
		$data['version']='1.0';
		// print_r($data);

		$ch = curl_init();
		$header = array();
		// if (!empty($data)) {
		// 	$header[] = 'Content-type: application/x-www-form-urlencoded';
		// }
		// $header[] = 'Accept: application/json';
		// $header[] = 'Authorization: Basic '.base64_encode($key.':');
		curl_setopt($ch, CURLOPT_URL, $this->CONFIG['URL'].$action.'?'.http_build_query($data));
		// print_r($header);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
		// curl_setopt($ch, CURLOPT_HTTP200ALIASES, array(400,403));
		
		/*if (!empty($data)) {
			$strData='';

			foreach ($data as $key => $value) {
				if (!empty($strData)) {
					$strData.='&';
				}
				$strData.=$key.'='.$value;
			}

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$strData);
		}*/
		$output = curl_exec($ch);
		// echo $output;
		$output=str_replace(array("\n", "\r", "\t"), '', $output);
		$output = trim(str_replace('"', "'", $output));
		// echo $output;
		// print_r($output);
		// $output=json_decode($output,TRUE);
		/*$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$badHttpCodes=array(400,401,403,404,405,406,407,409,415,422,429,500,501,503);
		if (in_array($httpcode, $badHttpCodes)) {
			throw new Exception("Request Error: ".$output['data'][0]['message'], 1);
		}*/
		curl_close($ch);
		try{
			$output=simplexml_load_string($output);
			if (isset($output->successful)&&$output->successful=='false') {
				if (isset($output->errorMessage)) {
					throw new Exception($output->errorMessage, 1);
				} else {
					throw new Exception('Error processing request.', 1);
				}
			}
		} catch(Exception $e){
			throw new Exception($e->getMessage(), 1);
		}
		
		return $output;
	}
}