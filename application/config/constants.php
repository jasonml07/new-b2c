<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',              'rb');
define('FOPEN_READ_WRITE',            'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',    'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',          'ab');
define('FOPEN_READ_WRITE_CREATE',       'a+b');
define('FOPEN_WRITE_CREATE_STRICT',       'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',    'x+b');

define('APPLICATION_CODES', json_encode(array(
  'H'  => 'HOTEL',
  'ET' => 'EXTERNALTOURS'
)));

define('AGENCY_CODENAMES', json_encode(array(
  'EET' => 'Eastern Eurotours & Mediterranean Holidays'
)));

/**

  TODO:
  - percentage
    * Subtract the percentage amount to the original price
  - amount
    * Replace amount
  - amountperson
    * Replace the amount per person
  - lessamountperson
    * Each passenger can less an amount of (base the value)
  - lessamount
    * Substract value to the original price

 */

define('DISCOUNTS', json_encode(array(
  'ETD15' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-6-30'), # yy-mm-dd
    'value'          => 15,
    'message'        => 'Discounted by 15% on Land product'
  ),
  'TZOO20' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2019-06-30'), # yy-mm-dd
    'value'          => 20,
    'message'        => 'Discounted by 20% on Land product'
  ),
  'EH20' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-12-31'), # yy-mm-dd
    'value'          => 20,
    'message'        => 'Discounted by 20% on Land product'
  ),
  'Insider15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-6-30'), # yy-mm-dd
    'value'          => 15,
    'message'        => 'Discounted by 15% on Land product',
    'products'       => array(26,28)
  ),
  'TZOO25'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-12-31'), # yy-mm-dd
    'value'          => 25,
    'message'        => 'Discounted by 25% on Land product',
    'products'       => array(1)
  ),
  'ITALIA15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-12-31'), # yy-mm-dd
    'value'          => 15,
    'message'        => 'Discounted by 15% on Land product',
    'products'       => array(207,721,262,334,345,734,735,211,212,264,217,731,732,225,271,790,273,733,215)
  ),
  'SUN15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-12-31'), # yy-mm-dd
    'value'          => 15,
    'message'        => 'Discounted by 15% on Land product',
    'products'       => array(744,746,655,693,656,234,231,236,369,619,363,630,631,367,391,647,615)
  ),
  'AMEX15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-12-31'), # yy-mm-dd
    'value'          => 15,
    'message'        => 'Discounted by 15% on Land product',
  ),
  'EARLYBIRD15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-12-31'), # yy-mm-dd
    'value'          => 15,
    'message'        => 'Discounted by 15% on Land product',
  ),
  'EARLYBIRD15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-08-31'), # yy-mm-dd
    'value'          => 15,
    'message'        => 'Discounted by 15% on Land product',
  ),
  'FRIEND50'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-09-27'), # yy-mm-dd
    'value'          => 25,
    'pax'            =>2,
    'message'        => 'Discounted by 25% on Land product',
    'products'       => array(300,370,373,353,381,632,973,827,828,356,358,334,342,351,896,752,347,345,614,149,150,157,953,954,806,814,689,805,648,445,690,922,923,920,921,803,361,390,675,628,368,375,758,411,412,415,640,419,421,917,918,819,910,911,826,403,438,439,440,442,398,399,435,924,925,785,793,800,640,645,353,360,387,630,391,389,392,454,457,367,371,631,369,458,660,378,842,348,350,467,174,177,418,951,890,201,891,205,610,873,874,17,607,604,466,877,878,634,228,879,880,231,234,236)
  ),
  'NEW16'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-12-31'), # yy-mm-dd
    'value'          => 0,
    'commission'     => 16,
    'message'        => 'Commissioned by 16% on Land product',
    'agent'          =>array('EET'),
  ),
  'BLACK30'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2018-11-30'), # yy-mm-dd
    'value'          => 15,
    'commission'     => 0,
    'message'        => 'Discounted by 15% on Land product',
    'agent'          =>array('EuropeTravelDeals'),
  ),
  'NewYear15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2019-02-28'), # yy-mm-dd
    'value'          => 15,
    'commission'     => 0,
    'message'        => 'Discounted by 15% on Land product',
    'agent'          =>array('EuropeTravelDeals'),
  ),
  'Valentine30'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2019-02-21'), # yy-mm-dd
    'value'          => 15,
    'commission'     => 0,
    'message'        => 'Discounted by 15% on Land product',
    // 'agent'          =>array('EuropeTravelDeals'),
  ),
  'MadMarch15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2019-03-31'), # yy-mm-dd
    'value'          => 15,
    'commission'     => 0,
    'message'        => 'Discounted by 15% on Land product',
    // 'agent'          =>array('EuropeTravelDeals'),
  ),
  'Easter30'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2017-01-20', 'to' => '2019-04-30'), # yy-mm-dd
    'value'          => 15,
    'commission'     => 0,
    'message'        => 'Discounted by 15% on Land product',
    // 'agent'          =>array('EuropeTravelDeals'),
  ),
  'May40'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-05-01', 'to' => '2019-05-31'), # yy-mm-dd
    'value'          => 20,
    'commission'     => 0,
    'message'        => 'Discounted by 20% on Land product',
    'agent'          =>array('EuropeTravelDeals'),
  ),
  'June20'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-06-01', 'to' => '2019-06-30'), # yy-mm-dd
    'value'          => 20,
    'commission'     => 0,
    'message'        => 'Discounted by 20% on Land product',
    'agent'          =>array('EuropeTravelDeals'),
  ),
  'July20'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-07-01', 'to' => '2019-07-30'), # yy-mm-dd
    'value'          => 20,
    'commission'     => 0,
    'message'        => 'Discounted by 20% on Land product',
    'agent'          =>array('EuropeTravelDeals'),
  ),
  'July15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-07-01', 'to' => '2019-07-30'), # yy-mm-dd
    'value'          => 15,
    'commission'     => 0,
    'message'        => 'Discounted by 15% on Land product',
    'agent'          =>array('EuropeTravelDeals'),
  ),

  'CROATIA20'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-08-30', 'to' => '2019-10-31'), # yy-mm-dd
    'value'          => 20,
    'commission'     => 0,
    'message'        => 'Discounted by 20% on Land product',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(648,814)
  ),
  'DALMATIAN10'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-08-30', 'to' => '2019-10-31'), # yy-mm-dd
    'value'          => 10,
    'commission'     => 0,
    'message'        => 'Discounted by 10% on Land product',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(917,918,412,910,911,826)
  ),
  'RIVER15'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-08-30', 'to' => '2019-10-31'), # yy-mm-dd
    'value'          => 15,
    'commission'     => 0,
    'message'        => 'Discounted by 15% on Land product',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(138,844,720,701,621)
  ),
  'ITALYTOP5'=>array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-08-30', 'to' => '2019-10-31'), # yy-mm-dd
    'value'          => 5,
    'commission'     => 0,
    'message'        => 'Discounted by 5% on Land product',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(721,974,937,938,738,971,741)
  ),


  'EUROPE300' => array(
    'type'           => 'amountperson',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-03', 'to' => '2019-12-31'),
    'value'          => 300,
    'message'        => 'Discounted by $300.00 per person',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(960,603,62,64,29,30,26,28,961,58,1,358,124,345,614,334,632,157,271,273)
  ),


  'AUG15' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2022-01-01', 'to' => '2022-08-31'),
    'value'          => 7.5,
    'message'        => 'Discounted by 7.5% per person',
    'agent'          => array('ifly'),
    'products'       => array(249)
  ),
  'EUROPE10' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2019-12-31'),
    'value'          => 10,
    'message'        => 'Discounted by 10% per person',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(467,827,441)
  ),
  'EUROPE15' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2022-01-01', 'to' => '2022-08-31'),
    'value'          => 7.5,
    'message'        => 'Discounted by 7.5% per person',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(249)
  ),
  

  'BESTNORWAY' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2019-11-30'),
    'value'          => 12.5,
    'message'        => 'Discounted by 25% off 2nd person',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(1150)
  ),
  'BESTCROATIA' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2019-11-30'),
    'value'          => 12.5,
    'message'        => 'Discounted by 25% off 2nd person',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(412),
  ),
  'BESTPORTUGAL' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2019-11-30'),
    'value'          => 12.5,
    'message'        => 'Discounted by 25% off 2nd person',
    'agent'          => array('EuropeTravelDeals'),
    'products'       => array(249)
  ),

  'EUROPE30' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2019-11-30'),
    'value'          => 15,
    'message'        => 'Discounted by 30% off 2nd person',
    'agent'          => array('EuropeTravelDeals'),
    // 'products'      => array(249)
  ),

  'NEWYEAR300' => array(
    'type'           => 'lessamountperson',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2020-02-29'),
    'value'          => 150,
    'message'        => 'Discounted by $150 per person',
    'agent'          => array('EuropeTravelDeals'),
    // 'products'      => array(249)
  ),

  'BF100' => array(
    'type'           => 'lessamountperson',
    'category'       => 'product',
    'availabilities' => array('from' => '2022-08-01', 'to' => '2022-11-30'),
    'value'          => 100,
    'message'        => 'Discounted by $100 per person'
    // 'products'      => array(249)
  ),

  'EH5' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2022-09-20', 'to' => '2022-12-31'),
    'value'          => 5,
    'message'        => 'Discounted by 5% per person'
    // 'products'      => array(249)
  ),

  'STARR5' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2022-09-20', 'to' => '2023-02-31'),
    'value'          => 5,
    'message'        => 'Discounted by 5% per person'
    // 'products'      => array(249)
  ),

  'BOXINGDAY15' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2022-12-26', 'to' => '2023-01-15'),
    'value'          => 15,
    'message'        => 'Discounted by 15% per person'
    // 'products'      => array(249)
  ),

  'BOXINGDAY15' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2022-12-26', 'to' => '2023-01-15'),
    'value'          => 15,
    'message'        => 'Discounted by 15% per person'
    // 'products'      => array(249)
  ),



  

  /////////////////////////////////////////
  // Promo setup for item base on nights //
  /////////////////////////////////////////
  
  /*'TESTNIGHTS' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2019-12-30'),
    'value'          => 15,
    'nihgts'         => '7+',
    'message'        => 'Testing nights by 30% off 2nd person',
    // 'agent'          => array('EuropeTravelDeals'),
    // 'products'      => array(249)
  ),*/

  /*'TESTNIGHTSRANGE' => array(
    'type'           => 'percentage',
    'category'       => 'product',
    'availabilities' => array('from' => '2019-10-02', 'to' => '2019-12-30'),
    'value'          => 15,
    'nihgts'         => '7-12',
    'message'        => 'Testing nights range by 30% off 2nd person',
    // 'agent'          => array('EuropeTravelDeals'),
    // 'products'      => array(249)
  ),*/

  /////////////////////////////////////////
  // Promo setup for item base on nights //
  /////////////////////////////////////////

  /*'FLT999' => array(
    'type'           => 'amountperson',
    'category'       => 'flight',
    'availabilities' => array('from' => '2017-2-01', 'to' => '2019-11-30'),
    'value'          => 999,
    'message'        => 'Discounted by $999.00 per passenger on flight'
  ),*/
  /*'FLT250' => array(
    'type'           => 'lessamountperson',
    'category'       => 'flight',
    'availabilities' => array('from' => '2017-2-01', 'to' => '2017-11-30'),
    'value'          => 250,
    'message'        => 'Discounted by $250.00 per passenger on flight'
  ),*/
  /*'APRSAVE' => array(
    'type'           => array('percentage', 'lessamountperson'),
    'category'       => array('product','flight'),
    'availabilities' => array('from' => '2017-2-01', 'to' => '2017-11-30'),
    'value'          => array(5, 250),
    'message'        => 'Discounted by $250 per person of every flight (all departures) + 5% off #the price of the tour/cruise/island hopping package'
  )*/
)));
// 2018 Price
/*define('DEPARTURE_MONTH', json_encode(array(
  'JANUARY'   => 1485,
  'FEBRUARY'  => 1355,
  'MARCH'     => 1385,
  'APRIL'     => 1475,
  'MAY'       => 1355,
  'JUNE'      => 1355,
  'JULY'      => 1795,
  'AUGUST'    => 1485,
  'SEPTEMBER' => 1485,
  'OCTOBER'   => 1385,
  'NOVEMBER'  => 1355,
  'DECEMBER'  => 1795,
)));*/
// 2019 Prices
define('DEPARTURE_MONTH', json_encode(array(
  'JANUARY'   => 1200,
  'FEBRUARY'  => 1200,
  'MARCH'     => 1200,
  'APRIL'     => 1250,
  'MAY'       => 1250,
  'JUNE'      => 1400,
  'JULY'      => 1400,
  'AUGUST'    => 1400,
  'SEPTEMBER' => 1200,
  'OCTOBER'   => 1200,
  'NOVEMBER'  => 1200,
  'DECEMBER'  => 1400,
)));

define('AVAILABLE_AIRLINES', json_encode(array(
  'Etihad Airways', 'China Southern', 'Emirates, Qantas Airways', 'Qatar Airways', 'Singapore Airlines', 'British Airways', 'Cathay Pacific', 'Lufthansa', 'Swiss Airways', 'Austrian Airlines', 'Air France', 'KLM', 'Finnair', 'Virgin Australia', 'Korean Airlines', 'Malaysian Airlines'
)));
define('PAYMENT_TYPES', json_encode(array(
  'CC' => 'CREDITCARD',
  'PP' => 'PAYPAL',
  'EN' => 'E-NETT',
  'BD' => 'BANKDEPOSIT',
  'PL' => 'PAYLATER',
)));
define('PAYMENT_TYPES_TEXT', json_encode(array(
  'CREDITCARD'  => 'Credit Card',
  'PAYPAL'      => 'PayPal',
  'E-NETT'      => 'E-Nett',
  'PAYLATER'    => 'Pay Later',
  'BANKDEPOSIT' => 'Bank Deposit',
)));

define('CUSTOM_THEME', json_encode(array(
  'rorFCB5GK' => array(
    'HEADER' => 'header',
    'FOOTER' => 'footer',
    'STYLE'  => 'style',
    'SCRIPT' => 'script',
  ),
  'EuropeTravelDeals' => array(
    'HEADER' => 'header',
    'FOOTER' => 'footer',
    'STYLE'  => 'style',
    'SCRIPT' => 'script',
  ),
  'ifly' => array(
    'HEADER' => 'header',
    'FOOTER' => 'footer',
    'STYLE'  => 'style',
    'SCRIPT' => 'script',
  ),
  'EET' => array(
    'HEADER' => 'header',
    'FOOTER' => 'footer',
    'STYLE'  => 'style',
    'SCRIPT' => 'script',
    'LIBRARY'=>array(
      'styles'=>array(
        'bower_components/Yamm3/yamm/yamm.css'
        ),
      )
  )
)));

define('AGENCY_SOCIALS', json_encode(array(
  'EuropeTravelDeals' => array(
    array(
      'title' => 'Facebook',
      'icon' => '',
      'link' => 'https://www.facebook.com/europeholidaydeals/'
    ),
    array(
      'title' => 'Twitter',
      'icon' => '',
      'link' => 'https://twitter.com/europe_holidays'
    ),
    array(
      'title' => 'Instagram',
      'icon' => '',
      'link' => 'https://www.instagram.com/Europe_Holidays/'
    )
  )
)));
/* End of file constants.php */
/* Location: ./application/config/constants.php */


define('AGENCY_CUSTOM_FUNC', json_encode(array(
  'EET'=>array(
    'HIDE_FLIGHT'=>TRUE,
    'DISABLE_FLIGHT'=>TRUE,
    'PAYMENT_METHOD'=>array(
      'CREDIT_CARD'=>array(
        'INFO'=>'1.5% for Visa and Mastercard and 3% for American Express and Diners',
        // Consist of title,icon
        'TAB'=>array()
      ),
      'PAYPAL'=>array(),
      'BANK_DEPOSIT'=>array(
        'NAME'=>'Europe Holidays Client ACC',
        'ACCN'=>'034-215',
        'SWIFT'=>'462080'
      ),
      'E-NETT'=>array(),
      // Set the sorting of mayment method for agent
      'AGENT'=>array('CREDIT_CARD','BANK_DEPOSIT','E-NETT'),
      // 'AGENT'=>array('CREDIT_CARD','BANK_DEPOSIT'),
      'CLIENT'=>array('CREDIT_CARD','PAYPAL','BANK_DEPOSIT'),
      // 'CLIENT'=>array('CREDIT_CARD','PAYPAL','E-NETT'),
    ),
    'CHECKOUT_SUCCESS_ON_LOAD'=>array(
      'title'=>'Hello There!',
      'message'=>'Do you want to book flights? <a target="_blank" href="https://eet.agentflights.com.au/searchAir.do">Click here</a>'
    )
  )
)));

define('AGENCY_CODE_MAP', json_encode(array(
  'EuropeTravelDeals'=>'EH'
)));