<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting PayPal's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */
$config['Production']    = TRUE;
$config['Certificate']   = 'application/libraries/cacerts.crt' ; // Location of the Certificate

$config['isTailored']    = FALSE;
$config['EncryptionKey'] = '4aQ0FWxXkxjEPeoDjXwh/Q==';
$config['BillerCode']    = '172817';
$config['Username']      = 'Q17281';
$config['Password']      = 'N4mk67axi';
$config['MerchantId']    = '24206542';
$config['Email']         = 'czarbaluran@example.com';
$config['BaseURL']       = 'https://www.payway.com.au/';

if ( ! $config['Production'] )
{
	# Code here
	$config['MerchantId'] = 'TEST';
} // end of if statement