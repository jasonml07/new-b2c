<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting PayPal's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */
$config['Sandbox']     = FALSE;
$config['Certificate'] = TRUE;
// When using sandbox please turn certificate in FALSE

/**
 * Log XML files
 * If Log Path is empty it will be defaulted somewhere
 * TODO
 * 	* filenames is either timestamp or user control
 */
$config['LOG']  = TRUE;
$config['PATH'] = '';

/* 
 * PayPal API Version
 * ------------------
 * The library is currently using PayPal API version 123.0.
 * You may adjust this value here and then pass it into the PayPal object when you create it within your scripts to override if necessary.
 */
$config['APIVersion'] = '204.0';

/*
 * PayPal Gateway API Credentials
 * ------------------------------
 * These are your PayPal API credentials for working with the PayPal gateway directly.
 * These are used any time you're using the parent PayPal class within the library.
 * 
 * We're using shorthand if/else statements here to set both Sandbox and Production values.
 * Your sandbox values go on the left and your live values go on the right.
 * 
 * You may obtain these credentials by logging into the following with your PayPal account.
 * Sandbox: https://www.sandbox.paypal.com/us/cgi-bin/webscr?cmd=_login-api-run
 * Live: https://www.paypal.com/us/cgi-bin/webscr?cmd=_login-api-run
 *
 */
$config['APIUsername']    = $config['Sandbox'] ? 'czarB2_api1.ifly.net.au' : 'shayne_api1.ifly.net.au';
$config['APIPassword']    = $config['Sandbox'] ? 'YQ4C4RKZ868VDF8N' : 'HWWX5K9CKBENKWPE';
$config['APISignature']   = $config['Sandbox'] ? 'AFcWxV21C7fd0v3bYYYRCpSSRl31AqgHSN5FIsqLlMSdCXwYta1TccXT' : 'PRODUCTION_SIGNATURE_GOES_HERE';
$config['APICertificate'] = $config['Sandbox'] ? '' : FCPATH . 'application/libraries/';

#$config['APIUsername'] = $config['Sandbox'] ? 'bloodsuckermarch61994-facilitator_api1.gmail.com' : 'bloodsuckermarch61994_api1.gmail.com';
#$config['APIPassword'] = $config['Sandbox'] ? 'H7AEZSWFLJZGN2A5' : 'B7HYP2B87TEQQQA8';
#$config['APISignature'] = $config['Sandbox'] ? 'AFcWxV21C7fd0v3bYYYRCpSSRl31Ar1r8t4.MR7RIjRfa9i0B.Er43hJ' : 'AFcWxV21C7fd0v3bYYYRCpSSRl31A4G7pfP1rGCR8nk3nTNtewyLZn3s';

/*
 * Payflow Gateway API Credentials
 * ------------------------------
 * These are the credentials you use for your PayPal Manager:  http://manager.paypal.com
 * These are used when you're working with the PayFlow child class.
 * 
 * We're using shorthand if/else statements here to set both Sandbox and Production values.
 * Your sandbox values go on the left and your live values go on the right.
 * 
 * You may use the same credentials you use to login to your PayPal Manager, 
 * or you may create API specific credentials from within your PayPal Manager account.
 */
$config['PayFlowUsername'] = $config['Sandbox'] ? 'SANDBOX_USERNAME_GOES_HERE' : 'PRODUCTION_USERNAME_GOGES_HERE';
$config['PayFlowPassword'] = $config['Sandbox'] ? 'SANDBOX_PASSWORD_GOES_HERE' : 'PRODUCTION_PASSWORD_GOES_HERE';
$config['PayFlowVendor'] = $config['Sandbox'] ? 'SANDBOX_VENDOR_GOES_HERE' : 'PRODUCTION_VENDOR_GOES_HERE';
$config['PayFlowPartner'] = $config['Sandbox'] ? 'SANDBOX_PARTNER_GOES_HERE' : 'PRODUCTION_PARTNER_GOES_HERE';

/*
 * PayPal Application ID
 * --------------------------------------
 * The application is only required with Adaptive Payments applications.
 * You obtain your application ID but submitting it for approval within your 
 * developer account at http://developer.paypal.com
 *
 * We're using shorthand if/else statements here to set both Sandbox and Production values.
 * Your sandbox values go on the left and your live values go on the right.
 * The sandbox value included here is a global value provided for developrs to use in the PayPal sandbox.
 */
$config['ApplicationID'] = $config['Sandbox'] ? 'APP-80W284485P519543T' : 'PRODUCTION_APP_ID_GOES_HERE';

/*
 * PayPal Developer Account Email Address
 * This is the email address that you use to sign in to http://developer.paypal.com
 */
$config['DeveloperEmailAccount'] = 'bloodsuckermarch61994@email.com';

/**
 * Third Party User Values
 * These can be setup here or within each caller directly when setting up the PayPal object.
 */
$config['DeviceID'] = 'DEVICE_ID_GOES_HERE';


/* End of file paypal.php */
/* Location: ./system/application/config/paypal.php */