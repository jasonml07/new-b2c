<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "hotel";
$route['404_override'] = 'error404';

$route['theme/(:num)'] = "hotel/theme/$1";

$route['hotel/details']            = "hotel/hotelDetails";
$route['hotel/search']             = "hotel/hotel_search";
$route['hotel/result']             = "hotel/searchResult";
$route['hotel/booker-form']        = "hotel/bookerInformation";
$route['hotel/booking-validation'] = "hotel/bookingValidation";
$route['hotel/session/(:any)']     = "hotel/session/$1";

$route['reservation-details/(:any)'] = "dashboard/reservationDetails/$1";
$route['api/search-city']        = "api/Database/searchCity";
$route['api/search-flight-city'] = "api/Database/searchFlightInCity";
$route['api/widget-style']       = "api/Database/widgetStyle";
$route['api/widget-script']      = "api/Database/widgetScript";

$route['api/agency/login']= "api/Database/agencyLogin";
$route['api/agency/status']= "api/Database/agencyStatus";
$route['api/agency/logout/(:any)']= "api/Database/agencyLogout/$1";

$route['api/search-result/initial/(:num)'] = "api/Cron/searchResult/$1";
$route['api/hotel-rooms/(:any)']           = "api/Cron/requestRoom/$1";

$route['external-tours/booked'] = 'externaltours/booked';

$route['external-tours-checkout/(:any)'] = 'externalToursAddGuest/index/$1';


$route['api/v2/tour/detail'] = 'v2/APITour/detail';
$route['api/v2/tour/book'] = 'v2/APITour/book';
$route['api/v2/tour/booking-detail'] = 'v2/APITour/bookingDetail';
$route['api/v2/tour/discount/check'] = 'v2/APITour/discountCheck';
$route['v2/tour'] = 'v2/Tour';


// Travelrez
$route['api/v2/travelrez/login'] = 'v2/APITravelrez/login';
$route['api/v2/travelrez/otp'] = 'v2/APITravelrez/otp';
$route['api/v2/travelrez/check'] = 'v2/APITravelrez/check';
$route['api/v2/travelrez/discounts'] = 'v2/APITravelrez/discounts';
$route['api/v2/travelrez/discount/create'] = 'v2/APITravelrez/discountCreate';
$route['api/v2/travelrez/discount/delete'] = 'v2/APITravelrez/discountDelete';
/* End of file routes.php */
/* Location: ./application/config/routes.php */