<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Australia/Sydney'); 
/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| WARNING: You MUST set this value!
|
| If it is not set, then CodeIgniter will try guess the protocol and path
| your installation, but due to security concerns the hostname will be set
| to $_SERVER['SERVER_ADDR'] if available, or localhost otherwise.
| The auto-detection mechanism exists only for convenience during
| development and MUST NOT be used in production!
|
| If you need to allow multiple domains, remember that this file is still
| a PHP script and you can easily do that on your own.
|
*/
$isSecure = ($_SERVER['SERVER_PORT'] == 443) ? TRUE : FALSE;
$emailConfig = array(
	'mailtype'  => 'html',
    'charset'   => 'iso-8859-1',
    'wordwrap'  => TRUE,

	'protocol'  => 'sendmail',
	'smtp_host' => 'localhost',
 	'smtp_port' => 25,
);

$emailDetails = array(
	'name'  => 'TravelRez',
	'user'  => 'info@travelrez.net.au',
	'reply' => 'info@travelrez.net.au'
);
$DOMAIN_D    = 'travelrez.net.au';
$DOMAIN_PRIV = '-----BEGIN RSA PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQClWO2Ou+bDFPe8
4pRSUfWw0Sz1Az2F3sn8gPCZDEku7rXkQ7V8iMvQKRncwIRVyZX+0PZmO3W7Vu9a
Tn41641FCQIClyqqtpUvy+ru5qHEo4gPgNI8+FGWDvzyEjt5ozMy5SNjuTqxK3wN
8LMBUjyBCnP1RpfP5csXNvbW49e6xDy+NX21bSGC7VG1BZs6I9TQmB6lL3Q3lyG/
KCVxSCEhBhiwIVdzegVgqRyFPVrUN3ch+9ZiAkr/AisTBCnZVVQw6n8+5wTgz9M+
DE7zpC0e2p3mna+UKGihYdQM7Mxdh9OvQzM1PVA2cY/ctpx8/f8NQHE0FrdpI3ix
iRQdz4OPAgMBAAECggEARWbSL3sHHRML+ShRgUwNiC4EZML7a9lbhrTISDUz8pMX
nbwdzSvOAI1/4XydDzUR1Gb5RWfnvr56sLkJmLBKF5P0WAoDSqxgajN5ZNVBSTwq
u/ciR50jAY1A9opYlHG/57jt5cm1rO/HnkJNoFxYMQGmMmlzBzw4xoG3kfMIdceD
zAEd5by3VPQbLyr9vyWYjaFkoUupOhhdPS3/VYogBdz9qyegJNH2onkNPk7ymdgR
2d+0pSvpOncwXIBYAZleZB+urG6wMBK6P7esOyP75u339By+unvsEU7TKJF3ZSIL
HF8Bujh124vEfRcINHTwNP1M4Alg1mMixplrREZi4QKBgQDQw2bHshFNTdPrTxER
P8CQ7tsAkBo0802bRnB30nPxN43/vgSHZ9kIux8nmOodho0FREZYKImDm+FVicxP
U0r0TJb0kJmcLLed4B8uQkzWdNVWWxfg3oYcY6TdYjhAV9PQzPCeFHuoE92cGye3
pv5a9N/PG7YSuiS/4ZNSEHjO/QKBgQDKwqmw7IPfqEe+l6GOBAkIg6bV6gwth/LL
xWjBWrFK038jLnFuzuT3PrAKhRjS+tnyiWDuMNSOACRFEQwu7trWcPt6MFsQo7At
wAjvh2LQGpxG0YAMXLb4shQHnC51/2+WpaARClOK/MdlxRlcXzOG94fQdz6S5Fbl
/O03S/lQewKBgQCnTuLRSlF5fdVIMY7BHzHKNdNIaTTHmPJqbhJ2xTBscpSYEIZ6
qmyTBbxbNL70OMBOJ0ClKJ/E3SnDLvOR43QfxD369ORW+LOTkGoM3c/M08eiNNE3
Q7+Bgw4+6pegYYL9RTHVzx6nPnyNvmmfUvNom/W+ezv5WDtZmIRD8bWQVQKBgQCz
RaQ1ixkaMjak/HpaD5VREt36+wIja7Uvl+6ELsTc7NzXZWPj3kgxlxqdYorNWABF
4lNypnf4yYVID8ee449X1PN0WLbbom7ONKFx71lduHl+YBOAWqVohEMTa3Lk36Fx
KCW7i07Mb615fOhsdLwhVFCf8B7CT+PJW3T91E2uawKBgQCVjT83hgoA7OF1E0aB
QkMInU85A8vESXnkohB1tpY5CM1DvWGcP5vad3wTb/NRPf1IQcG2aJh08ZoUjI3L
u9K7IY7bmM+t42ujgzpTxr+P8MbgK5GcKaNBmiGmiMCqLS+x6fDSbyZTUkfoYi2p
ITEsOAW9rUFd9vtq+Qv2cupr9g==
-----END RSA PRIVATE KEY-----';

if(in_array($_SERVER['HTTP_HOST'], ['localhost:4444', 'localhost:3000', 'new-b2c.test'])){
	define('BRAND_NAME', 'Localhost');
	$config['base_url'] = 'https://new-b2c.test/';

	define('ADMIN_SYSTEM_DOCS'  , '/usr/local/var/www/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'http://host.docker.internal:8100/');
	define('B2B_IP'             , 'http://localhost:3333/');
	define('AGENCY_CODE'        , 'EET');

	define('fileDocsPath'       , 'http://localhost:1212/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	//$config['fileDocsPath'] = 'http://localhost/sys_admin/_documents/';
	define('IS_LIVE', FALSE);
}elseif($_SERVER['HTTP_HOST'] == '192.168.1.103:4444'){
	define('BRAND_NAME', '192.168.1.103');
	$config['base_url'] = 'http://192.168.1.103:4444/';

	define('ADMIN_SYSTEM_DOCS'  , '/usr/share/nginx/PROJECTS/PHP/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'http://192.168.1.103:1212/');
	define('B2B_IP'             , 'http://202.191.63.95/');
	define('AGENCY_CODE'        , 'EET');

	define('fileDocsPath'       , 'http://192.168.1.103/sys_admin/_documents/');
	define('directory_save_path', '../sys_admin/_documents/');
	//$config['fileDocsPath'] = 'http://localhost/sys_admin/_documents/';
	define('IS_LIVE', FALSE);
}elseif($_SERVER['HTTP_HOST'] == 'itchy-swan-2.localtunnel.me'){
	define('BRAND_NAME', 'itchy-swan-2.localtunnel.me');
	$config['base_url'] = 'https://itchy-swan-2.localtunnel.me/';

	define('ADMIN_SYSTEM_DOCS'  , '/usr/share/nginx/PROJECTS/PHP/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'http://192.168.1.101:1212/');
	define('B2B_IP'             , 'http://202.191.63.95/');
	define('AGENCY_CODE'        , 'EET');

	define('fileDocsPath'       , 'http://192.168.1.101/sys_admin/_documents/');
	define('directory_save_path', '../sys_admin/_documents/');
	//$config['fileDocsPath'] = 'http://localhost/sys_admin/_documents/';
	define('IS_LIVE', FALSE);
}elseif($_SERVER['HTTP_HOST'] == '198.58.117.129:4444'){
	define('BRAND_NAME', 'Test Server');
	$config['base_url'] = 'http://198.58.117.129:4444';
	define('fileDocsPath'       , 'http://198.58.117.129:1212/_documents/');
	define('directory_save_path', '/home/system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/home/system_admin/_documents/');
	//$config['fileDocsPath'] = 'http://208.97.188.14/_documents/';
	define('SYS_ADMIN_IP'       , 'http://198.58.117.129:1212/');
	define('B2B_IP'             , 'http://198.58.117.129:3333/');
	define('AGENCY_CODE'        , 'EET');

	define('IS_LIVE', FALSE);

}elseif($_SERVER['HTTP_HOST'] == '192.168.1.101:2222'){
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://192.168.1.101:2222';
	define('fileDocsPath'       , 'http://198.58.117.129:1212/_documents/');
	define('directory_save_path', '/home/system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/home/system_admin/_documents/');
	//$config['fileDocsPath'] = 'http://208.97.188.14/_documents/';
	define('SYS_ADMIN_IP'       , 'http://198.58.117.129:1212/');
	define('B2B_IP'             , 'http://198.58.117.129:3333/');
	define('AGENCY_CODE'        , 'EET');

	define('IS_LIVE', FALSE);

} elseif ($_SERVER['HTTP_HOST'] == '202.191.63.95:4444') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://202.191.63.95:4444/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://202.191.63.95/');
	define('AGENCY_CODE'        , 'EET');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'whitelabel.travelrez.net.au') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://whitelabel.travelrez.net.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EET');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'easterneurotours.travelrez.net.au') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://easterneurotours.travelrez.net.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EET');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'b2c.travelrez.net.au') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://b2c.travelrez.net.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EET');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'tours.europeholidaydeals.com') {
	define('BRAND_NAME', 'Europe Holiday Deals');
	$config['base_url'] = 'http://tours.europeholidaydeals.com/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'https://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EET');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'testhotel.ifly.net.au') {
	define('BRAND_NAME', 'iFly');
	$config['base_url'] = 'https://testhotel.ifly.net.au/';
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'https://travelrez.net.au/');
	define('AGENCY_CODE'        , 'FghMM8C9K');
	

	define('IS_LIVE', FALSE);
	
}
elseif ($_SERVER['HTTP_HOST'] == 'tours.europeholidays.com.au' ) {
	define('BRAND_NAME', 'Europe Holidays');
	$config['base_url'] = 'http' . (($isSecure) ? 's' : '') . '://tours.europeholidays.com.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EuropeTravelDeals');
	
	$emailConfig['protocol'] = 'smtp';
	$emailConfig['smtp_host'] = 'bugatti.websitewelcome.com';
	$emailConfig['smtp_user'] = 'reservations@europeholidays.com.au';
	$emailConfig['smtp_pass'] = 'reservation!2';
	$emailConfig['smtp_port'] = 465;


	$emailDetails['name']  = 'Europe Holidays';
	$emailDetails['user']  = 'reservations@europeholidays.com.au';
	$emailDetails['reply'] = 'reservations@europeholidays.com.au';

	$DOMAIN_D    = 'europeholidays.com.au';
	$DOMAIN_PRIV = '-----BEGIN RSA PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDsaPvQrGEv4U46
KTcIGJKf2AuXyTa5EjV7jXRGTdFMBVCFM7e/ze6X3NxdFYt1R1fMyfAeAweuAOY6
CQD+l6CC95bDwOo8NIt7MiZ+MB/Mn9btpaYizefDuvf9G5TQovIJzYWn3eULmufS
G+bz7WODAfie7I56MXbe/FhuvGYvn6TmNX9IaJV2aqvZeHlgO7DsmIVtOiU7WNAU
P8qxu4n0Haz1qOM1toDG1pEXA0MESO5FlHhXb4ps+YiodCWZ9w2YyGsSEhjI7JrL
e41JK41evc9u2wmJgwYDlRwv1BT3wccO4Rj9Wt7G+2pPg5xuoAYDFldKOn5gU5Mu
P+HNUcf5AgMBAAECggEBAKbDr86VqsDxuBMC+9T0MIodBqB9yeJnW97Z+8mUHy33
GcO8pcPhaospuzDWf9etCdxmHsGs9X/jFoSd0VvGbe205V8U+KabkkDHMawWGnhn
0F83WKh4gyLpAwJCsIk0SHRty/iMaIP1M85c7F/pdyb0/wJscFz2A5pse50xvQud
wJ/zEQmO21hnpTBd56G4tsOsXm7fqmf9VVbx01Lwz6QPcrwlOKbZpLD/5h+elVcw
SohdXnmbUba4Dl6Er3yleT1sbpeUlDVSso48+UFCEXBAvkywsqDWSuMqJdc+YfKa
snxjdbDDQYINRVczeHITVM4g2a89RrPfQsVnQjH/WvkCgYEA/cT+UQVSj9wayuH4
F9D1D0qC/O+PdvBcxDGZLD+Fj1Ioy89USvYfw98lwbUUoVoF1Nyjlxhara6Cev8K
SlNUgBbn7zGzZkS1XarOCRV5N3sP/+pV0tMN7OJp+xu/ZpPfBp/l0Zah7r/acoB8
08i8h0aITIF/lPzLA1tgsNIVCecCgYEA7nzuCRoNsxNVtN2oRgHKRFAC+1rB8UQF
ELOTTXMyHydC0kRgmH3cvB16nRURPFoJUny14N1NCetA/W8CRYAhoO2SlPNHlbjL
joCDTRSAfm7yYmMcym83sEBzJQ7JIbK/Z7deUp5655qC6HprbL/qkTDKHUG98lHs
nxTbXJDdIx8CgYBy5/8r2pge0dBD63v19MTyDpAvTZ4NghrPL5Im6gBk/BPC7XdC
lh2eCG3FP2Q/B086qklcwkdU+37TiKY1L3REp5qPwVZ1SUlCR3QRwupnR87f11DT
LIPSrlbEwDSzkuNa0Y4zj9x1CeQ1Ep7zGqLbMgHHep+1a53VQEwQ5J9oIwKBgAcl
n0yOh1KShcMB0YUnkJQzgaNeuOkuDKjPeN6dUSc12f7LJcnsG61XhNEoTHfuitD6
A8lVWHWvoyb1KLBEgr174SStlbua6h6/IzAwKRmg4YqpRbebVaPmiozVwJlN7gaB
rEX1keFW9A1bfdSoe7MWLzhpO/QDGIA788b2YGopAoGAWB508PWLq2mUI0Ssv6L/
zi6iHSeubvJrB+FCL5YW143ZHKNTaUHyLi6frLnfdBVdrUTUIbuiXPHym5tCWHh6
korbf04ZdbphG/jI1Lei8oktJJ498UrYdNV2ivwkQO1sENa4KD/l20xdUjSgtuks
tT3a8MyMaNJUt494nEvKLpM=
-----END RSA PRIVATE KEY-----';

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'tours.iflygo.com.au' ) {
	define('BRAND_NAME', 'iFlyGo');
	$config['base_url'] = 'http' . (($isSecure) ? 's' : '') . '://tours.iflygo.com.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'ifly');
	
	$emailConfig['protocol'] = 'smtp';
	$emailConfig['smtp_host'] = 'mail.iflygo.com.au';
	$emailConfig['smtp_user'] = 'reservation@iflygo.com.au';
	$emailConfig['smtp_pass'] = 'reservation!2';
	$emailConfig['smtp_port'] = 585;


	$emailDetails['name']  = 'iFlyGo';
	$emailDetails['user']  = 'reservation@iflygo.com.au';
	$emailDetails['reply'] = 'reservation@iflygo.com.au';

	$DOMAIN_D    = 'iflygo.com.au';
	$DOMAIN_PRIV = '-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC7Zed18544DeWY
7d3qw0uoK6a0gvb/R26xScNy2btHnXk1DbT3GKJwi8nM13OCA0McwJGJXPOxyiXS
I5N8kcyrAdir7i8+jeqzxVgzNkw6+SJ9A/llNsAI6ODSYviXg0jL0RwIaYfj/Byx
zDEjpj7OBeTBZkQel9s3qjkIXDY9MjMWYt7s0ROqCnrD3Dt8fVhE2PWAOE4qwA9W
sDdUDn7oAIno4Fw7WT12VrHNLF1SvaNvKiOKcnoKS+UUgEQWRwodohc+8jiN0NyN
1Qii8nn9fO3DR7RHxvpKxZQqkpxhnzL7qIlnL9fK7vu7umkV3J4kXhMyecOzVTTi
ubCFKSE1AgMBAAECggEALhOmT7gw5PN1yp5h/EE8rwLcoHG7LZVwwCK07d5fUAKG
NOdD4aWq+yVD3xvyiLsomz82CaJTQRnFJuAceXqX6kwv7EZKU4UtejpBr4XgRHfR
ZQ+/GIXXvdkUvmYbXZWoJoUp8V+RTBtHKuhcBLnxo1KLponwod0Qx6URFN8nbvAm
tsJZ8GA2vRBfBXGiMNhnk/FKGECNvq4H2iECOCPqkNmPaKkoiYDRaq4hXw8bUX8a
n3CYi/Mmc3iCgIkkdcgfeTS9CSIwFzEshSLUmcJbH1cPUz8USWMUw1q5iOZUozAt
K+l9uaJxW9pdHTgiiU/OOVGte17wVvVJS9q9efkhWQKBgQDwWaxemSiuT5bzaSTf
mn7eMCV/H5bsAJhs2MEiomhb3y2OI6IfD40OkhZoA2pyO7b2TftnFn8wo0WQIiqj
zMKXjqmtVvhJJNEtkXnzEHu2J6V/L5pML+5knv7udoLNYZV9qVf0Sz69tVRIAHK0
cppduTx9HfptxSis0Ttar5v7cwKBgQDHmZYMoKSQvFwkWcjO9Y6ghXvLYRCfd9hJ
nlxwqQlCYgK8MB7b/zKbT6czwAb01bl/DJsNd4uqps+WGQI0XorQZYjB46rc7YGk
ZnVNOumUsCe2mk/G9jUzzSV6NhTeMhGE1Wb088b8b1ZfmgZqK07XY4nzKPwVXjax
7YZyvKGWtwKBgQCKU7HGh/NHzvqbgIn+PtaBXchWz4F2tikDjAQrwLi7F0kKoytt
IPAX0LAgkMa0k/WK7/qeq+3ruiwQ9fkAaJody7eP0Zv3zL6usSk0YOoyPcMb3fO5
oWRceL1T1NUXCvCUb9/081FasX+eKNPbUpHRGwjbIWO9xVjRjWHRoXvuTQKBgBRv
nzNPx0miX3TBVJQMBDxfLluWZpNNO/Hdqu1GAVKwokMOUjqIGHBaaW4c/RlYQJPH
gKCSWtRwG9TsXpNqEqBfpO7lFntkPd/NNblgjMXSe15w0jY2wx+4o1ut6dkOG0kU
aGQwCXGTHK7RIFMCeCIJaPhie9zcNpFlFlzs/0ZlAoGARdmU4DvKf16PFrqrK85T
SZaY2+tAvqAr6ormKo5311AN71n2+6KjSR+/qR2Z9pggryupnr4FHAMTGXdKMcrQ
kx4r4jNUORphyQ2tKeC+NEFxMdxF13zmbFh1zs4ERp0NK0pWuuYNUHNp9SPyh5UA
JgKrHNs6DXGhy/kz5H3n0S4=
-----END PRIVATE KEY-----';

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'tours.easterneurotours.com.au' ) {
	define('BRAND_NAME', 'Europe Holidays');
	$config['base_url'] = 'http' . (($isSecure) ? 's' : '') . '://tours.easterneurotours.com.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EET');
	
	$emailConfig['protocol'] = 'smtp';
	$emailConfig['smtp_host'] = 'bugatti.websitewelcome.com';
	$emailConfig['smtp_user'] = 'reservations@europeholidays.com.au';
	$emailConfig['smtp_pass'] = 'reservation!2';
	$emailConfig['smtp_port'] = 465;

	$emailDetails['name']  = 'Eastern EuroTours';
	$emailDetails['user']  = 'reservations@easterneurotours.com.au';
	$emailDetails['reply'] = 'reservations@easterneurotours.com.au';

	$DOMAIN_D    = 'easterneurotours.com.au';
	$DOMAIN_PRIV = '-----BEGIN RSA PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDsaPvQrGEv4U46
KTcIGJKf2AuXyTa5EjV7jXRGTdFMBVCFM7e/ze6X3NxdFYt1R1fMyfAeAweuAOY6
CQD+l6CC95bDwOo8NIt7MiZ+MB/Mn9btpaYizefDuvf9G5TQovIJzYWn3eULmufS
G+bz7WODAfie7I56MXbe/FhuvGYvn6TmNX9IaJV2aqvZeHlgO7DsmIVtOiU7WNAU
P8qxu4n0Haz1qOM1toDG1pEXA0MESO5FlHhXb4ps+YiodCWZ9w2YyGsSEhjI7JrL
e41JK41evc9u2wmJgwYDlRwv1BT3wccO4Rj9Wt7G+2pPg5xuoAYDFldKOn5gU5Mu
P+HNUcf5AgMBAAECggEBAKbDr86VqsDxuBMC+9T0MIodBqB9yeJnW97Z+8mUHy33
GcO8pcPhaospuzDWf9etCdxmHsGs9X/jFoSd0VvGbe205V8U+KabkkDHMawWGnhn
0F83WKh4gyLpAwJCsIk0SHRty/iMaIP1M85c7F/pdyb0/wJscFz2A5pse50xvQud
wJ/zEQmO21hnpTBd56G4tsOsXm7fqmf9VVbx01Lwz6QPcrwlOKbZpLD/5h+elVcw
SohdXnmbUba4Dl6Er3yleT1sbpeUlDVSso48+UFCEXBAvkywsqDWSuMqJdc+YfKa
snxjdbDDQYINRVczeHITVM4g2a89RrPfQsVnQjH/WvkCgYEA/cT+UQVSj9wayuH4
F9D1D0qC/O+PdvBcxDGZLD+Fj1Ioy89USvYfw98lwbUUoVoF1Nyjlxhara6Cev8K
SlNUgBbn7zGzZkS1XarOCRV5N3sP/+pV0tMN7OJp+xu/ZpPfBp/l0Zah7r/acoB8
08i8h0aITIF/lPzLA1tgsNIVCecCgYEA7nzuCRoNsxNVtN2oRgHKRFAC+1rB8UQF
ELOTTXMyHydC0kRgmH3cvB16nRURPFoJUny14N1NCetA/W8CRYAhoO2SlPNHlbjL
joCDTRSAfm7yYmMcym83sEBzJQ7JIbK/Z7deUp5655qC6HprbL/qkTDKHUG98lHs
nxTbXJDdIx8CgYBy5/8r2pge0dBD63v19MTyDpAvTZ4NghrPL5Im6gBk/BPC7XdC
lh2eCG3FP2Q/B086qklcwkdU+37TiKY1L3REp5qPwVZ1SUlCR3QRwupnR87f11DT
LIPSrlbEwDSzkuNa0Y4zj9x1CeQ1Ep7zGqLbMgHHep+1a53VQEwQ5J9oIwKBgAcl
n0yOh1KShcMB0YUnkJQzgaNeuOkuDKjPeN6dUSc12f7LJcnsG61XhNEoTHfuitD6
A8lVWHWvoyb1KLBEgr174SStlbua6h6/IzAwKRmg4YqpRbebVaPmiozVwJlN7gaB
rEX1keFW9A1bfdSoe7MWLzhpO/QDGIA788b2YGopAoGAWB508PWLq2mUI0Ssv6L/
zi6iHSeubvJrB+FCL5YW143ZHKNTaUHyLi6frLnfdBVdrUTUIbuiXPHym5tCWHh6
korbf04ZdbphG/jI1Lei8oktJJ498UrYdNV2ivwkQO1sENa4KD/l20xdUjSgtuks
tT3a8MyMaNJUt494nEvKLpM=
-----END RSA PRIVATE KEY-----';

	define('IS_LIVE', TRUE);
	
}
elseif ($_SERVER['HTTP_HOST'] == 'hotels.selectworldtravel.com.au') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://hotels.selectworldtravel.com.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'lTcZ8WX5M');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'tours.europetraveldeals.com.au') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://tours.europetraveldeals.com.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EuropeTravelDeals');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'tours.europeantourdeals.com.au') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://tours.europeantourdeals.com.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EuropeTravelDeals');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'tours.europeantourdeals.com') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://tours.europeantourdeals.com/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EuropeTravelDeals');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == 'hotel.ifly.net.au') {
	$isHttpProtocol=isset($_SERVER['HTTPS'] )?'https':'http';
	define('BRAND_NAME', '');
	$config['base_url'] = $isHttpProtocol.'://hotel.ifly.net.au/'; 
	define('fileDocsPath'       , 'https://eetstaff.travelrez.net.au/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin/_documents/');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('B2B_IP'             , 'https://travelrez.net.au/');
	define('AGENCY_CODE'        , 'FghMM8C9K');
	

	define('IS_LIVE', TRUE);
	
}elseif ($_SERVER['HTTP_HOST'] == '202.191.63.95:2222') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://202.191.63.95:2222/';
	define('fileDocsPath'       , 'http://208.97.188.14:7777/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('B2B_IP'             , 'http://travelrez.net.au/');
	define('AGENCY_CODE'        , 'EET');
	define('SYS_ADMIN_IP'       , 'https://eetstaff.travelrez.net.au/');
	define('ADMIN_SYSTEM_DOCS'  , '/opt/apps/system_admin');

	define('IS_LIVE', TRUE);
	

}elseif ($_SERVER['HTTP_HOST'] == '198.58.117.129:5555') {
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://198.58.117.129:5555/';
	define('fileDocsPath'       , 'http://208.97.188.14:7777/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('B2B_IP'             , 'http://198.58.117.129:3333/');
	define('AGENCY_CODE'        , 'EET');
	define('SYS_ADMIN_IP'       , 'http://198.58.117.129:1212/');
	define('ADMIN_SYSTEM_DOCS'  , '/home/system_admin/_documents/');
	
}else if ($_SERVER['HTTP_HOST'] == 'localhost:2222'){
	define('BRAND_NAME', '');
	$config['base_url'] = 'http://localhost:2222/';
	define('fileDocsPath'       , 'http://208.97.188.14:7777/_documents/');
	define('directory_save_path', '../system_admin/_documents/');
	define('B2B_IP'             , 'http://localhost:3333/');
	define('AGENCY_CODE'        , 'EET');
	define('SYS_ADMIN_IP'       , 'http://localhost:1212/');
	define('ADMIN_SYSTEM_DOCS'  , '/home/travelrezpc4/php-projects/system_admin/_documents/');

	#define('IS_LIVE', FALSE);

	
	//$config['fileDocsPath'] = 'http://208.97.188.14/_documents/';
}
if ( defined('IS_LIVE') && IS_LIVE == TRUE )
{
	# Code here
	define('IS_BOOKING'         , TRUE);
	define('TEST_BOOKING'       , FALSE);
	define('TEST_BOOKING_ERROR' , FALSE);
	// This will disble add test inputs on forms
	define('TEST_INPUTS'        , FALSE);
	// This will trigger the Booking payment to use the payway
	define('PAYWAY_IS_TEST'     , FALSE);
	define('APPLICATION_IS_TEST', TRUE);
	// Sending of email via test emails
	define('EMAIL_IS_TEST'      , FALSE);
	define('GET_DATA_FROM_XMLS' , FALSE);
	define('LOG_XML_RESPONSE'   , TRUE);
} else {
	define('IS_BOOKING'         , TRUE);
	define('TEST_BOOKING'       , FALSE);
	define('TEST_BOOKING_ERROR' , FALSE);
	// This will disble add test inputs on forms
	define('TEST_INPUTS'        , TRUE);
	// This will trigger the Booking payment to use the payway
	define('PAYWAY_IS_TEST'     , TRUE);
	define('APPLICATION_IS_TEST', TRUE);
	// Sending of email via test emails
	define('EMAIL_IS_TEST'      , TRUE);
	define('GET_DATA_FROM_XMLS' , TRUE);
	define('LOG_XML_RESPONSE'   , TRUE);
} // end of if statement


define('SYSTEM_TYPE'         , 'B2C');
define('SCENARIO'           , 12);
define('TEST_XML_FILES_PATH', './booking-log/hotel/NORMAL/');
define('LOG_XML_MAIN_PATH'  , './booking-log/');
define('LOG_RESPONSE_PATH'  , '/home/downloads/instantDB/request-and-response/');

// This currency will only be applied on the XML request default currency
define('DEFAULT_CURRENCY'   , 'USD');
define('CONVERTION_CURRENCY', 'AUD');

// define('DISPLAY_EXTRA_INFORMATION', TRUE);

// define('INSTANT_USER'   , 'EastEUROPtestXML');
// define('INSTANT_PASS'   , 'shayne2016');
define('INSTANT_USER'       , 'EastEUROPXMLlive');
define('INSTANT_PASS'       , 'EastEUROPXMLlive2');

define('SUPPLIERS', json_encode(array(
	'hotusa'    => array('code' => 'Restel'         , 'name' => 'Restel Euro'),
	'hotelspro' => array('code' => 'Hotels Pro Euro', 'name' => 'Hotels Pro Euro'),
	'hotelbeds' => array('code' => 'HOTELBEDS'      , 'name' => 'Hotelbeds'),
	'gta'       => array('code' => 'GTA'            , 'name' => 'GTA')
)));

define('DEFAULT_SUPPLIER_CODE', 'INNTVL');
define('DEFAULT_SUPPLIER_NAME', 'Instant Travel');

define('EMAIL_USERNAME'           , $emailDetails['user']);
// define('EMAIL_USERNAME'        , 'bloodsuckermarch61994@gmail.com');
define('EMAIL_TEST'               , 'dongskay@gmail.com');
define('EMAIL_NAME'               , $emailDetails['name']);
define('EMAIL_DEFAILT_RECEPIENT'  , 'reception@easterneurotours.com.au');
define('EMAIL_CC'                 , 'sally@easterneurotours.com.au, info@selectworldtravel.com.au');
define('EMAIL_REPLY'              , $emailDetails['reply']);


if ( defined('IS_LIVE') && IS_LIVE === TRUE )
{
	# Code here
	$emailConfig = array_merge( $emailConfig, array(
		'protocol'  => 'sendmail',
		'smtp_host' => 'localhost',
	 	'smtp_port' => 25,

	 	#'protocol'  => 'smtp',
		#'smtp_host' => 'bugatti.websitewelcome.com',
    #'smtp_user' => 'reservations@europeholidays.com.au', // change it to yours
    #'smtp_pass' => 'reservation!2', // change it to yours
    #'smtp_port' => 465
	));
} else {
	$emailConfig = array_merge( $emailConfig, array(
		'mailtype'=>'html',
		'protocol'  => 'smtp',
		'smtp_host' => 'ssl://smtp.gmail.com',
	    'smtp_user' => 'bloodsuckermarch61994@gmail.com', // change it to yours
	    'smtp_pass' => 'losewinniwesol123.', // change it to yours
	    'smtp_port' => 465
	));
	
} // end of else if statement
define('EMAIL_CONFIG', json_encode( $emailConfig ));

define('PAYWAY_IS_LIVE'       , FALSE);
define('PAYWAY_TAILORED'      , FALSE);
define('PAYWAY_ENCRYPTION_KEY', '4aQ0FWxXkxjEPeoDjXwh/Q==');
define('PAYWAY_BILLER_CODE'   , '172817');
define('PAYWAY_USERNAME'      , 'Q17281');
define('PAYWAY_PASSWORD'      , 'N4mk67axi');

// define('PAYWAY_MERCHAND_ID', 'TEST');
if( PAYWAY_IS_TEST ) {
	define('PAYWAY_MERCHAND_ID', 'TEST');
} else {
	define('PAYWAY_MERCHAND_ID', '24206542');
}

define('PAYWAY_PAYPAL_EMAIL', 'czarbaluran@example.com');
define('PAYWAY_BASE_URL'    , 'https://www.payway.com.au/');

// DKIM CONFIGURATION
define('DOMAIN_D', $DOMAIN_D);
define('DOMAIN_S', 'mail');
define('DOMAIN_PRIV', $DOMAIN_PRIV);

/*
|--------------------------------------------------------------------------
| Index File
|--------------------------------------------------------------------------
|
| Typically this will be your index.php file, unless you've renamed it to
| something else. If you are using mod_rewrite to remove the page set this
| variable so that it is blank.
|
*/
$config['index_page'] = '';

/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of 'AUTO' works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'			Default - auto detects
| 'PATH_INFO'		Uses the PATH_INFO
| 'QUERY_STRING'	Uses the QUERY_STRING
| 'REQUEST_URI'		Uses the REQUEST_URI
| 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
|
*/
$config['uri_protocol']	= 'AUTO';

/*
|--------------------------------------------------------------------------
| URL suffix
|--------------------------------------------------------------------------
|
| This option allows you to add a suffix to all URLs generated by CodeIgniter.
| For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/urls.html
*/

$config['url_suffix'] = '';

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| This determines which set of language files should be used. Make sure
| there is an available translation if you intend to use something other
| than english.
|
*/
$config['language']	= 'english';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = 'UTF-8';

/*
|--------------------------------------------------------------------------
| Enable/Disable System Hooks
|--------------------------------------------------------------------------
|
| If you would like to use the 'hooks' feature you must enable it by
| setting this variable to TRUE (boolean).  See the user guide for details.
|
*/
$config['enable_hooks'] = FALSE;


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = 'MY_';


/*
|--------------------------------------------------------------------------
| Allowed URL Characters
|--------------------------------------------------------------------------
|
| This lets you specify with a regular expression which characters are permitted
| within your URLs.  When someone tries to submit a URL with disallowed
| characters they will get a warning message.
|
| As a security measure you are STRONGLY encouraged to restrict URLs to
| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
|
| Leave blank to allow all characters -- but only if you are insane.
|
| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
|
*/
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';


/*
|--------------------------------------------------------------------------
| Enable Query Strings
|--------------------------------------------------------------------------
|
| By default CodeIgniter uses search-engine friendly segment based URLs:
| example.com/who/what/where/
|
| By default CodeIgniter enables access to the $_GET array.  If for some
| reason you would like to disable it, set 'allow_get_array' to FALSE.
|
| You can optionally enable standard query string based URLs:
| example.com?who=me&what=something&where=here
|
| Options are: TRUE or FALSE (boolean)
|
| The other items let you set the query string 'words' that will
| invoke your controllers and its functions:
| example.com/index.php?c=controller&m=function
|
| Please note that some of the helpers won't work as expected when
| this feature is enabled, since CodeIgniter is designed primarily to
| use segment based URLs.
|
*/
$config['allow_get_array']		= TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger']	= 'c';
$config['function_trigger']		= 'm';
$config['directory_trigger']	= 'd'; // experimental not currently in use

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = 4;

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| application/logs/ folder. Use a full server path with trailing slash.
|
*/
$config['log_path'] = '';

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = 'Y-m-d H:i:s';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
|
| Leave this BLANK unless you would like to set something other than the default
| system/cache/ folder.  Use a full server path with trailing slash.
|
*/
$config['cache_path'] = '';

/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Session class you
| MUST set an encryption key.  See the user guide for info.
|
*/
$config['encryption_key'] = '5uZA18N0P7533nja62IgE092R4qC0Yw2';

/*
|--------------------------------------------------------------------------
| Session Variables
|--------------------------------------------------------------------------
|
| 'sess_cookie_name'		= the name you want for the cookie
| 'sess_expiration'			= the number of SECONDS you want the session to last.
|   by default sessions last 7200 seconds (two hours).  Set to zero for no expiration.
| 'sess_expire_on_close'	= Whether to cause the session to expire automatically
|   when the browser window is closed
| 'sess_encrypt_cookie'		= Whether to encrypt the cookie
| 'sess_use_database'		= Whether to save the session data to a database
| 'sess_table_name'			= The name of the session database table
| 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
| 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
| 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
|
*/
$config['sess_cookie_name']		= 'ci_session';
$config['sess_expiration']		= 7200;
$config['sess_expire_on_close']	= FALSE;
$config['sess_encrypt_cookie']	= FALSE;
$config['sess_use_database']	= FALSE;
$config['sess_table_name']		= 'ci_sessions';
$config['sess_match_ip']		= FALSE;
$config['sess_match_useragent']	= TRUE;
$config['sess_time_to_update']	= 300;

/*
|--------------------------------------------------------------------------
| Cookie Related Variables
|--------------------------------------------------------------------------
|
| 'cookie_prefix' = Set a prefix if you need to avoid collisions
| 'cookie_domain' = Set to .your-domain.com for site-wide cookies
| 'cookie_path'   =  Typically will be a forward slash
| 'cookie_secure' =  Cookies will only be set if a secure HTTPS connection exists.
|
*/
$config['cookie_prefix']	= "";
$config['cookie_domain']	= "";
$config['cookie_path']		= "/";
$config['cookie_secure']	= FALSE;

/*
|--------------------------------------------------------------------------
| Global XSS Filtering
|--------------------------------------------------------------------------
|
| Determines whether the XSS filter is always active when GET, POST or
| COOKIE data is encountered
|
*/
$config['global_xss_filtering'] = FALSE;

/*
|--------------------------------------------------------------------------
| Cross Site Request Forgery
|--------------------------------------------------------------------------
| Enables a CSRF cookie token to be set. When set to TRUE, token will be
| checked on a submitted form. If you are accepting user data, it is strongly
| recommended CSRF protection be enabled.
|
| 'csrf_token_name' = The token name
| 'csrf_cookie_name' = The cookie name
| 'csrf_expire' = The number in seconds the token should expire.
*/
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;

/*
|--------------------------------------------------------------------------
| Output Compression
|--------------------------------------------------------------------------
|
| Enables Gzip output compression for faster page loads.  When enabled,
| the output class will test whether your server supports Gzip.
| Even if it does, however, not all browsers support compression
| so enable only if you are reasonably sure your visitors can handle it.
|
| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
| means you are prematurely outputting something to your browser. It could
| even be a line of whitespace at the end of one of your scripts.  For
| compression to work, nothing can be sent before the output buffer is called
| by the output class.  Do not 'echo' any values with compression enabled.
|
*/
$config['compress_output'] = FALSE;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are 'local' or 'gmt'.  This pref tells the system whether to use
| your server's local time as the master 'now' reference, or convert it to
| GMT.  See the 'date helper' page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = 'local';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = FALSE;


/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '';


/* End of file config.php */
/* Location: ./application/config/config.php */
