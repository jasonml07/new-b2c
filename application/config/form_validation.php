<?php 

$config = array();

$config['hotel_SEARCH'] = array(
	array('field' => 'location[name]', 'label' => 'Location'      , 'rules' => 'required' ),
	array('field' => 'location[id]'  , 'label' => 'Location'      , 'rules' => 'required' ),
	array('field' => 'start_date'    , 'label' => 'Check in date' , 'rules' => 'required|required|callback_valid_date' ),
	array('field' => 'end_date'      , 'label' => 'Check out date', 'rules' => 'required|required|callback_valid_date' ),
);

$config['hotel_booking_VOUCHER'] = array(
	array('field' => 'voucher[title]'      ,'label' => 'Title'                 ,'rules' => 'required' ),
	array('field' => 'voucher[firstname]'  ,'label' => 'First Name'            ,'rules' => 'required' ),
	array('field' => 'voucher[lastname]'   ,'label' => 'Last Name'             ,'rules' => 'required' ),
	array('field' => 'voucher[email]'      ,'label' => 'Email Address'         ,'rules' => 'required' ),
	array('field' => 'voucher[phone]'    ,'label' => 'phone number' ,'rules' => 'required' ),
	array('field' => 'policy_read'         ,'label' => 'Policy'                ,'rules' => 'required' ),
);

$config['hotel_booking_CREDIT_CARD'] = array(
	array('field' => 'payment[dt_expiry_month]','label' => 'expiry month'           ,'rules' => 'required|numeric' ),
	array('field' => 'payment[dt_expiry_year]' ,'label' => 'expiry year'            ,'rules' => 'required|numeric' ),
	array('field' => 'payment[nm_card_holder]' ,'label' => 'card holder name'       ,'rules' => 'required' ),
	array('field' => 'payment[no_credit_card]' ,'label' => 'card number'            ,'rules' => 'required|max_length[16]|min_length[14]' ),
	array('field' => 'payment[no_cvn]'         ,'label' => 'car verification number','rules' => 'required|max_length[4]|min_length[3]' )
);

$config['hotel_booking_ENETT'] = array(
	array('field' => 'payment[date]'      ,'label' => 'Date'      ,'rules' => 'required' ),
	array('field' => 'payment[reference]' ,'label' => 'reference' ,'rules' => 'required' ),
	array('field' => 'payment[amount]'    ,'label' => 'Amount'    ,'rules' => 'required' )
);

/*======================================
=            External Tours            =
======================================*/

$config['ET_booking_VOUCHER'] = array(
	array('field' => 'voucher[title]'     ,'label' => 'title'         ,'rules' => 'required' ),
	array('field' => 'voucher[firstname]' ,'label' => 'first name'    ,'rules' => 'required' ),
	array('field' => 'voucher[lastname]'  ,'label' => 'last name'     ,'rules' => 'required' ),
	array('field' => 'voucher[email]'     ,'label' => 'email address' ,'rules' => 'required' ),
	array('field' => 'voucher[phonenum]'  ,'label' => 'phone number'  ,'rules' => 'required' ),
);
$config['ET_booking_CREDIT_CARD'] = array(
	array('field' => 'payment[booking_ref_num]','label' => 'booking reference'      ,'rules' => 'required' ),
	array('field' => 'payment[dt_expiry_month]','label' => 'expiry month'           ,'rules' => 'required|numeric' ),
	array('field' => 'payment[dt_expiry_year]' ,'label' => 'expiry year'            ,'rules' => 'required|numeric' ),
	array('field' => 'payment[nm_card_holder]' ,'label' => 'card holder name'       ,'rules' => 'required' ),
	array('field' => 'payment[no_credit_card]' ,'label' => 'card number'            ,'rules' => 'required|max_length[16]|min_length[14]' ),
	array('field' => 'payment[no_cvn]'         ,'label' => 'car verification number','rules' => 'required|max_length[4]|min_length[3]' )
);
$config['ET_booking_ENETT'] = array(
	array('field' => 'payment[consultant_name]','label' => 'consultant name','rules' => 'required' ),
	array('field' => 'payment[agent_id]'       ,'label' => 'agent id'       ,'rules' => 'required' ),
	// array('field' => 'payment[date]'        ,'label' => 'Date'           ,'rules' => 'required' ),
	// array('field' => 'payment[reference]'   ,'label' => 'reference'      ,'rules' => 'required' ),
	array('field' => 'payment[amount]'         ,'label' => 'Amount'         ,'rules' => 'required' )
);
$config['ET_FLIGHT'] = array(
	array('field' => 'flight[city]'         , 'label' => 'City'          , 'rules' => 'required'),
	array('field' => 'flight[class]'        , 'label' => 'Class'         , 'rules' => 'required'),
	array('field' => 'flight[airline]'      , 'label' => 'Airline'       , 'rules' => 'required'),
	array('field' => 'flight[departureDate]', 'label' => 'Departure Date', 'rules' => 'required'),
	array('field' => 'flight[returnDate]'   , 'label' => 'Return Date'   , 'rules' => 'required'),
);

/*=====  End of External Tours  ======*/


$config['SYSTEM_ROUTE_LOGIN'] = array(
	array('field' => 'username', 'label' => 'Username', 'rules' => 'required'),
	array('field' => 'password', 'label' => 'Password', 'rules' => 'required')
);