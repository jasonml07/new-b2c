<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['ENV']='prod';

$data=array();
switch ($config['ENV']) {
	case 'dev':
	case 'DEV':
		$config['INTEGRATOR']='SELECTDEMO';
		$config['KEY']='LWNb1FoGvooUFfv97TETJi6BChUPyNV9c6iHYOopKMEqq6KeeS6ADpGcKa92WeTi';
		$config['ECN']='500499';
		$config['URL']='https://enett-demo.com/';
		break;
	case 'prod':
	case 'PROD':
		$config['INTEGRATOR']='EET';
		$config['KEY']='w8sMZlJdf0bsbdGhXzjPOPk2as6ti0GgjdCrkzjEo6GP7c3yvzGkU9xMR5zVXQg2';
		$config['ECN']='200141';
		$config['URL']='https://enett.com/';
		break;
	default:
		$config['INTEGRATOR']='';
		$config['KEY']='';
		$config['ECN']='';
		$config['URL']='https://enett-demo.com/';
		break;
}