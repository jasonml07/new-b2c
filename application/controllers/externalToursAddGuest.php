<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_id() == '') {
	session_start();
}
class externalToursAddGuest extends CI_Controller {

	
	public function __construct()
    {
        parent::__construct();

        $this->load->model('booking_model');
        $this->load->library('PaymentWestpac', 'paymentwestpac');
    }

    /**
     * [testing description]
     * @return [type] [description]
     */
    public function review ( $token )
    {
    	# Code here
    	response(array('SUCCESS' => TRUE));
    } // end of testing

	public function index( $name )
	{
		// echo '<script>console.log(' . json_encode(_AGENT_INFO( $this->nativesession->get('agencyRealCode') )) . ')</script>';
#echo date_difference( $_POST['startdate'] );
#print_r($_POST);
#echo "\n";
#die();
		$SESSION         = $this->nativesession->get('ETLOGIN');
		$isAgent         = $SESSION && is_array( $SESSION );
		$_itemsUsedPrice = $isAgent?'due_amount':'grossPrice';


		if ( ! isset( $_POST ) || empty( $_POST ) || $this->nativesession->get('externalToursToken') != $_POST['token'] )
		{
			# Code here
			$redirectURL = '';
			if ( isset( $_POST['previousURL'] ) )
			{
				# Code here
				$redirectURL = $_POST['previousURL'];
			} // end of if statement
			echo 'Please select a product to book<br>';
			echo 'If not redirected <a href="' . $redirectURL .'">Click here</a>';
			d_redirect( $redirectURL );
		} // end of if statement

		$totalCosting = array(
			'originalCurrency'  => '',
			'convertedCurrency' => CONVERTION_CURRENCY,
			'rateOfConvertion'  => '',

			'OriginalBasePrice'  => 0,
			'convertedBasePrice' => 0,
			
			'commission'        => 0,
			'gross_amount'      => 0,
			'markup_amount'     => 0,
			'total_less_amount' => 0,
			'due_amount'        => 0,
			'profit_amount'     => 0,

			'comm_pct_amount'   => 0,
			'markup_pct_amount' => 0,

			'discount_pct'    => 0,
			'discount_amount' => 0,

		);
		$inclusions  = '';
		$supplements = array();

		$passengers = intval( $_POST['passengers'] );

		if ( isset( $_POST['items'] ) )
		{
			# Code here
			$items = $_POST['items'];
			if ( isset( $items['products'] ) )
			{
				# Code here
				$inclusions .= 'Products: ';
				$products   = $items['products'];

				foreach ( $products as $productKey => $product )
				{
					# code...
					$suppliment = array();
					$guests     = intval($product['quantity']);
					$inclusions .= $product['name'] . ' X' . $guests . ' ($' . number_format($product['price'], 2) . ')';

					if ( isset( $product['supplement'] ) )
					{
						# Code here
						$supplement = $product['supplement'];
						$s_cost     = $supplement['costings'];
						$s_quantity = intval($supplement['quantity']);

						#if ( empty( $supplimentInclusions ) )
						#{
						#	# Code here
						#	$supplimentInclusions .= '<br>Supplement: ';
						#} // end of if statement
						#$supplimentInclusions .= $supplement['name'] . ' X' . $supplement['quantity'] . ' ($' . number_format($supplement['price'], 2) . '), ';

						$supCosting = item_costing( $s_cost['convertedBasePrice'], $s_cost['comm_pct_amount'], $s_cost['markup_pct_amount'] );
						$supCosting = array_map(function( $item ) { return round( $item ); }, $supCosting);

						$totalCosting['OriginalBasePrice']  += ((float)$s_cost['OriginalBasePrice']   * intval( $s_quantity ));
						$totalCosting['convertedBasePrice'] += (((float)$s_cost['convertedBasePrice'] * intval( $s_quantity )));

						$totalCosting['commission']        += (((float)$supCosting['commission']       * intval( $s_quantity )));
						$totalCosting['gross_amount']      += (((float)$supCosting['gross_amount']     * intval( $s_quantity )));
						$totalCosting['markup_amount']     += ((float)$supCosting['markup_amount']     * intval( $s_quantity ));
						$totalCosting['total_less_amount'] += ((float)$supCosting['total_less_amount'] * intval( $s_quantity ));
						$totalCosting['due_amount']        += (((float)$supCosting['due_amount']       * intval( $s_quantity )));
						$totalCosting['profit_amount']     += ((float)$supCosting['profit_amount']     * intval( $s_quantity ));

						$s_cost['due_amount'] = (((float)$supCosting['due_amount'] * intval( $s_quantity )));

						$_POST['items']['products'][ $productKey ]['supplement']['costings']['due_amount'] = $s_cost['due_amount'];

						if ( empty( $supplements ) )
						{
							# Code here
							$supplements[ $supplement['key'] ] = array(
								'name'               => $supplement['name'],
								'OriginalBasePrice'  => $s_cost['OriginalBasePrice'],
								'convertedBasePrice' => $s_cost['convertedBasePrice'],
								'fromCurrency'       => $s_cost['fromCurrency'],
								'toCurrency'         => $s_cost['toCurrency'],
								'convertedRate'      => $s_cost['convertedRate'],
								'comPrice'           => $s_cost['comPrice'],
								'comm_pct_amount'    => $s_cost['comm_pct_amount'],
								'grossPrice'         => $s_cost['grossPrice'],
								'markup_pct_amount'  => $s_cost['markup_pct_amount'],
								'netAmount'          => $s_cost['netAmount'],
								'included'           => true,
								'quantity'           => $s_quantity,
								'price'              => $supplement['price'],
								'due_amount'         => $s_cost['due_amount'],
							);
						} else {
							if ( isset($supplements[ $supplement['key'] ]) )
							{
								# Code here
								$supplements[ $supplement['key'] ]['quantity'] += $s_quantity;
							} // end of if statement
						} // end of if statement

					} // end of if statement

					/*if ( isset( $product['supplimentKey'] ) && ! empty( $product['supplimentKey'] ) )
					{
						# Code here
						if ( isset( $_POST['suppliments'][ $product['supplimentKey'] ] ) )
						{
							# Code here
							$suppliment = $_POST['suppliments'][ $product['supplimentKey'] ];
							$suppliment['included'] = TRUE;

							if ( isset( $suppliment['quantity'] ) )
							{
								# Code here
								$suppliment['quantity'] += intval($product['extra']);
								$suppliment['price']    += ($suppliment['grossPrice'] * intval($product['extra']));
							} else {
								$suppliment['quantity'] = intval($product['extra']);
								$suppliment['price']    = ($suppliment['grossPrice'] * intval($product['extra']));
							} // end of if statement

						} // end of if statement
					} // end of if statement*/

					/*if ( ! empty( $suppliment ) )
					{
						# Code here
						#$inclusions .= ' with suppliment ' . $suppliment['name'] . ' X' . $product['extra'] . ' (' . $suppliment['grossPrice'] . ')';
						$supCosting = item_costing( $suppliment['convertedBasePrice'], $suppliment['comm_pct_amount'], $suppliment['markup_pct_amount'] );
						$supCosting = array_map(function( $item ) { return round( $item ); }, $supCosting);

						$totalCosting['OriginalBasePrice']  += ((float)$suppliment['OriginalBasePrice']   * intval( $product['extra'] ));
						$totalCosting['convertedBasePrice'] += (((float)$suppliment['convertedBasePrice'] * intval( $product['extra'] )));

						$totalCosting['commission']        += (((float)$supCosting['commission']          * intval( $product['extra'] )));
						$totalCosting['gross_amount']      += (((float)$supCosting['gross_amount']        * intval( $product['extra'] )));
						$totalCosting['markup_amount']     += ((float)$supCosting['markup_amount']        * intval( $product['extra'] ));
						$totalCosting['total_less_amount'] += ((float)$supCosting['total_less_amount']    * intval( $product['extra'] ));
						$totalCosting['due_amount']        += (((float)$supCosting['due_amount']          * intval( $product['extra'] )));
						$totalCosting['profit_amount']     += ((float)$supCosting['profit_amount']        * intval( $product['extra'] ));

						$suppliment['due_amount'] = (((float)$supCosting['due_amount'] * intval( $product['extra'] )));
						$_POST['suppliments'][ $product['supplimentKey'] ] = $suppliment;
					} // end of if statement*/


					$inclusions .= ( $productKey == (count( $products ) - 1) )?'. ': ', ';
					$costing    = $product['costings'];
					if ( $totalCosting['comm_pct_amount'] == 0 )
					{
						# Code here
						$totalCosting['comm_pct_amount'] = (float)$costing['comm_pct_amount'];
					} // end of if statement
					if ( $totalCosting['markup_pct_amount'] == 0 )
					{
						# Code here
						$totalCosting['markup_pct_amount'] = (float)$costing['markup_pct_amount'];
					} // end of if statement
					if ( empty( $totalCosting['originalCurrency'] ) )
					{
						# Code here
						$totalCosting['originalCurrency'] = $costing['fromCurrency'];
					} // end of if statement
					if ( empty( $totalCosting['rateOfConvertion'] ) )
					{
						# Code here
						$totalCosting['rateOfConvertion'] = (float)$costing['convertedRate'];
					} // end of if statement
	#print_r($costing);

					$costings = item_costing( $costing['convertedBasePrice'], $costing['comm_pct_amount'], $costing['markup_pct_amount'] );
					$costings = array_map(function ( $item ) { return round( $item ); }, $costings);

					$totalCosting['OriginalBasePrice']  += ((float)$costing['OriginalBasePrice']  * $guests);
					$totalCosting['convertedBasePrice'] += ((float)$costing['convertedBasePrice'] * $guests);

					$totalCosting['commission']        += ((float)$costings['commission']        * $guests);
					$totalCosting['gross_amount']      += ((float)$costings['gross_amount']      * $guests);
					$totalCosting['markup_amount']     += ((float)$costings['markup_amount']     * $guests);
					$totalCosting['total_less_amount'] += ((float)$costings['total_less_amount'] * $guests);
					$totalCosting['due_amount']        += ((float)$costings['due_amount']        * $guests);
					$totalCosting['profit_amount']     += ((float)$costings['profit_amount']     * $guests);


					$_POST['items']['products'][ $productKey ]['costings']['due_amount'] = ((float)$costings['due_amount'] * $guests);
				}
			} // end of if statement
			
			if ( isset( $items['packages'] ) )
			{
				# Code here
				$inclusions   .= '<br>Add On/Packages: ';
				$packages     = $items['packages'];
				$packagesSize = count( $packages );


				foreach ( $packages as $key => $package )
				{
					# code...
					$quantity = intval( $package['quantity'] );
					$costing  = $package['costings'];


					$costings    = item_costing( $costing['convertedBasePrice'], $costing['comm_pct_amount'], $costing['markup_pct_amount'] );
					$inclusions .= $package['name'] . ' X' . $quantity . ' ($' . number_format($package['price'], 2) . ')';

					$inclusions .= ( $key == (count( $packages ) - 1) )?'. ': ', ';

					if ( $totalCosting['comm_pct_amount'] == 0 )
					{
						# Code here
						$totalCosting['comm_pct_amount'] = (float)$costing['comm_pct_amount'];
					} // end of if statement
					if ( $totalCosting['markup_pct_amount'] == 0 )
					{
						# Code here
						$totalCosting['markup_pct_amount'] = (float)$costing['markup_pct_amount'];
					} // end of if statement
					/*if ( empty( $totalCosting['originalCurrency'] ) )
					{
						# Code here
						$totalCosting['originalCurrency'] = $supplement['fromCurrency'];
					} // end of if statement*/
		#print_r($costing);
					$_dueAmount = (round((float)$costings['due_amount']) * $quantity);

					$totalCosting['OriginalBasePrice']  += (round((float)$costing['OriginalBasePrice'])  * $quantity);
					$totalCosting['convertedBasePrice'] += (round((float)$costing['convertedBasePrice']) * $quantity);

					$totalCosting['commission']        += (round((float)$costings['commission'])        * $quantity);
					$totalCosting['gross_amount']      += (round((float)$costings['gross_amount'])      * $quantity);
					$totalCosting['markup_amount']     += (round((float)$costings['markup_amount'])     * $quantity);
					$totalCosting['total_less_amount'] += (round((float)$costings['total_less_amount']) * $quantity);
					$totalCosting['due_amount']        += $_dueAmount;
					$totalCosting['profit_amount']     += (round((float)$costings['profit_amount'])     * $quantity);

					$_POST['items']['packages'][ $key ]['costings']['due_amount'] = $_dueAmount;
				}
			} // end of if statement
			
			/*=====  End of PACKAGES  ======*/
			
			/*==================================
			=            EXTRABILLS            =
			==================================*/
			
			if ( isset( $items['extrabills'] ) )
			{
				# Code here
				$inclusions     .= '<br>Extra Bills: ';
				$extraBills     = $items['extrabills'];


				foreach ( $extraBills as $key => $extraBill )
				{
					# code...
					$quantity   = intval($extraBill['quantity']);
					$inclusions .= $extraBill['name'] . ' X' . $quantity . ' ($' . number_format($extraBill['price'], 2) . ')';
					$costing    = $extraBill['costings'];
					$costings   = item_costing( $costing['convertedBasePrice'], $costing['comm_pct_amount'], $costing['markup_pct_amount'] );

					$inclusions .= ( $key == (count( $extraBills ) - 1) )?'. ': ', ';

					if ( $totalCosting['comm_pct_amount'] == 0 )
					{
						# Code here
						$totalCosting['comm_pct_amount'] = (float)$costing['comm_pct_amount'];
					} // end of if statement
					if ( $totalCosting['markup_pct_amount'] == 0 )
					{
						# Code here
						$totalCosting['markup_pct_amount'] = (float)$costing['markup_pct_amount'];
					} // end of if statement
					/*if ( empty( $totalCosting['originalCurrency'] ) )
					{
						# Code here
						$totalCosting['originalCurrency'] = $supplement['fromCurrency'];
					} // end of if statement*/
		#print_r($costing);
					$_dueAmount = 0;
					if ( ! filter_var($costing['isMarkup'], FILTER_VALIDATE_BOOLEAN) )
					{
						# Code here
						$_dueAmount = (round((float)$costing['convertedBasePrice']) * $quantity);

						$totalCosting['OriginalBasePrice']  += (round((float)$costing['OriginalBasePrice'])  * $quantity);
						$totalCosting['convertedBasePrice'] += (round((float)$costing['convertedBasePrice']) * $quantity);

						#$totalCosting['commission']        += (round((float)$costing['commission'])        * $quantity);
						$totalCosting['gross_amount']      += (round((float)$costing['convertedBasePrice']) * $quantity);
						#$totalCosting['markup_amount']     += (round((float)$costing['markup_amount'])     * $quantity);
						#$totalCosting['total_less_amount'] += (round((float)$costing['total_less_amount']) * $quantity);
						$totalCosting['due_amount']        += $_dueAmount;
						#$totalCosting['profit_amount']     += (round((float)$costing['profit_amount'])     * $quantity);
						$_POST['items']['extrabills'][ $key ]['costings']['due_amount'] = $_dueAmount;
						continue;
					} // end of if statement

					$_dueAmount = (round((float)$costings['due_amount']) * $quantity);

					$totalCosting['OriginalBasePrice']  += (round((float)$costing['OriginalBasePrice'])  * $quantity);
					$totalCosting['convertedBasePrice'] += (round((float)$costing['convertedBasePrice']) * $quantity);

					$totalCosting['commission']        += (round((float)$costings['commission'])        * $quantity);
					$totalCosting['gross_amount']      += (round((float)$costings['gross_amount'])      * $quantity);
					$totalCosting['markup_amount']     += (round((float)$costings['markup_amount'])     * $quantity);
					$totalCosting['total_less_amount'] += (round((float)$costings['total_less_amount']) * $quantity);
					$totalCosting['due_amount']        += $_dueAmount;
					$totalCosting['profit_amount']     += (round((float)$costings['profit_amount'])     * $quantity);

					$_POST['items']['extrabills'][ $key ]['costings']['due_amount'] = $_dueAmount;
				}
			} // end of if statement

			/*=====  End of EXTRABILLS  ======*/

			if ( ! empty( $supplements ) )
			{
				# Code here
				$inclusions .= '<br>Supplements: ';

				foreach ( $supplements as $key => $supplement )
				{
					$inclusions .= $supplement['name'] . ' X' . $supplement['quantity'] . ' ($' . number_format($supplement['price'], 2) . ')';
					$inclusions .= ( $key == (count( $supplements ) - 1) )?'. ': ', ';
				}
			} // end of if statement
		} // end of if statement

		$tour = $_POST['tour'];

		// $FORMATTED_ITEMS = $this->formatProductItems( $_POST, $isAgent );
		// $inclusions      = $this->inclussions( $FORMATTED_ITEMS );

		$remaining_days = date_difference( $tour['start'] );
		$policy         = array();
		
		if ( $remaining_days >= 30 )
		{
			# Code here
			if ( $remaining_days >= 67 )
			{
				# Code here
				array_push($policy, 'Must pay a deposit of 30 percent or $' . ($totalCosting['due_amount'] * 0.30) . ' before the end of ' . date('F d, Y', strtotime('now +7 day')));
			} // end of if statement
			array_push($policy, 'Must be fully paid before the end of ' . date('F d, Y', strtotime( $tour['start'] . ' -15 day')));
		} // end of if statement


		/*==============================
		=            FLIGHT            =
		==============================*/
		
		if ( filter_var(@$_POST['hasFlight'], FILTER_VALIDATE_BOOLEAN) )
		{
			# Code here
			$_flight = $_POST['flight'];
			// $totalCosting['gross_amount'] += round( (float) $_POST['itemPrices']['flight'] );

			$inclusions .= '<br>With return Flights fee of $' . number_format($_flight['price'], 2) . '.';
		} // end of if statement
		
		/*=====  End of FLIGHT  ======*/


		$discountMessage = '';

		$this->load->model('discount_model');
		$isDiscounted = $this->discount_model->config(
			array(
				'FLIGHT'     => @$_POST['flight'],
				'PRICES'     => $_POST['itemPrices'],
				'PASSENGERS' => $_POST['passengers'],
			),
			@$_POST['PROMOCODE']
		);
		if ( $isDiscounted )
		{
			$inclusions .= '<br>';

			$discount = $this->discount_model->getDiscount();
			$discounted_amount = $this->discount_model->getDiscountedAmount();
			$discountMessage = $this->discount_model->message();

			$inclusions .= $discountMessage;
			$totalCosting['discount_amount'] = $discounted_amount;
			
			if ( is_array( $discount['category'] ) )
			{
				# Code here
				$key = array_search('product', $discount['category']);
				$discount['type']     = $discount['type'][ $key ];
				$discount['value']    = $discount['value'][ $key ];
				$discount['category'] = $discount['category'][ $key ];
			} // end of if statement
			if ( $discount['type'] == 'percentage' )
			{
				# Code here
				$totalCosting['discount_pct'] = $discount['value'];
			} // end of if statement

			if ( $discount['category'] == 'product' )
			{
				# Code here
				$totalCosting['due_amount']    -= $totalCosting['discount_amount'];
				$totalCosting['gross_amount']  -= $totalCosting['discount_amount'];
				$totalCosting['profit_amount'] = round($totalCosting['due_amount'] - $totalCosting['convertedBasePrice']);
			} // end of if statement

			#$totalCosting['gross_amount'] -= $totalCosting['discount_amount'];
			#$totalCosting['profit_amount'] = round($totalCosting['due_amount'] - $totalCosting['convertedBasePrice']);
		} // end of if statement

		/*if ( isset($_POST['isDiscounted']) &&  filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN) && @$DISCOUNTS[ $_POST['PROMOCODE'] ] )
		{
			# Code here
			$_discount = $DISCOUNTS[ $_POST['PROMOCODE'] ];

			$_discountedDate = date('Y-m-d', strtotime('now'));

			if ( $_discount['category'] == 'flight' )
			{
				# Code here
				$_discountedDate = date('Y-m-d', strtotime( str_replace('/', '-', $_POST['flight']['departure']) ));
			} // end of if statement

			if (  is_in_range_date( $_discount['availabilities']['from'], $_discount['availabilities']['to'], $_discountedDate )  )
			{
				# Code here
				#die('date is in range');
				$inclusions .= '<br>Discounted ';

				$_discountAmount = $this->discountInAmount( $_discount['type'], $_discount['value'], $totalCosting['gross_amount'], $passengers );

				switch ( $_discount['category'] )
				{
					case 'product':
						# code...
						$totalCosting['discount_pct']    = $_discount['value'];
						$totalCosting['discount_amount'] = $_discountAmount;

						$discountMessage = 'Discounted by ' . $_discount['value'] . ' percent on land product.';
						$inclusions .= 'with land product of ' . $_discount['value'] . ' percent';
						break;

					case 'flight':
						# code...
						$totalCosting['discount_pct']    = 0;
						$totalCosting['discount_amount'] = 0;

						// $_POST['flight']['price'] = $_discountAmount;

						$inclusions .= 'with flight of $' . number_format($_discount['value'], 2) . ' per pax.';
						$discountMessage = 'Flight discounted to $' . number_format($_discount['value'], 2) . ' per passenger.';
						break;
				}

			} // end of if statement
			#$totalCosting['discount_pct']    = (int)$DISCOUNTS[ $_POST['PROMOCODE'] ];
			#$percent                         = $totalCosting['discount_pct'] / 100;
			#$totalCosting['discount_amount'] = round($totalCosting['gross_amount'] * $percent);

			$totalCosting['due_amount']   -= $totalCosting['discount_amount'];
			$totalCosting['gross_amount'] -= $totalCosting['discount_amount'];

			$totalCosting['profit_amount'] = round($totalCosting['due_amount'] - $totalCosting['convertedBasePrice']);
		} // end of if statement*/

		$data = array();
		$data['remaining_days']  = $remaining_days;
		$data['policies']        = $policy;
		$data['costing']         = $totalCosting;
		#$data['inclusions']     = $inclusions;
		$data['SESSION']         = $this->nativesession->get('ETLOGIN');
		$data['IS_AGENT']        = $isAgent;
		$data['displatPrice']    = $isAgent? 'due_amount': 'gross_amount';
		$data['departurePrice']  = @json_decode(DEPARTURE_MONTH, TRUE)[strtoupper(date('F', strtotime($_POST['startdate'])))];
#		$data['FORMATTED_ITEMS'] = $FORMATTED_ITEMS;
		$data['inclusions']      = $inclusions;
		$data['FLIGHT']          = @$_POST['flight'];
		$data['discountMessage'] = $discountMessage;
		$data['IS_DISCOUNTED']   = filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN);
		$data['SUPPLEMENTS']     = $supplements;
		$data['FINAL_PRICE']     = ($data['IS_DISCOUNTED']? $_POST['prices']['after'] : $_POST['prices']['before']);
		#print_r($data);
		#die();
#echo '<script>console.log(' . json_encode($data['FORMATTED_ITEMS']) . ')</script>';
		$agencycode =base64_encode($this->session->userdata('agencycode'));

		$connections = new MongoClient();
        $db = $connections->db_agency;

        $agencyRes = $db->agency_b2c_setup->findOne(array("agency_code"=>$agencycode),array("_id"=>0));
        //$data['agencyData'] = $this->session->userdata('result');
		$whereData['agency_code'] = $agencycode;
		$res = $db->agency_menu_setup->find($whereData,array("_id"=>0));
        $arrMenu = iterator_to_array($res);
		$result = $this->treeview($arrMenu);
		$data['menu'] = $result;
        $data['agency'] = _AGENT_INFO($this->nativesession->get('agencyRealCode'));
		

		?>
		<script type="text/javascript">console.log('COSTING', <?=json_encode($totalCosting);?>)</script>
		<script type="text/javascript">console.log( 'POST', <?=json_encode($_POST) ?> )</script>
		<?php
		$this->load->view('externalTours/titlehead',$data); 
		$this->load->view('externalTours/header',$data); 
		$this->load->view('externalTours/addGuest',$data); 
		$this->load->view('externalTours/addGuestFooter');

	}

	/**
	 * Formatting inclussions of iterenary
	 * @param  [type] $formattedItems [description]
	 * @return [type]                 [description]
	 */
	private function inclussions ( $formattedItems )
	{
		# Code here
		$inclusion = '';

		foreach ( $formattedItems as $title => $items )
		{
			# code...
			$inclusion .= ucfirst( $title ) . ': ';

			foreach ( $items as $index => $item )
			{
				# code...
				$inclusion .= ( $index > 0 ) ? ', ' : '';
				$inclusion .= $item['name'] . ' ( $' . number_format($item['price'], 2) . ' )';
			}

			$inclusion .= '.<br>';
		}

		return $inclusion;
	} // end of inclussions


	/**
	 * [discountInAmount description]
	 * @param  [type] $type  [description]
	 * @param  [type] $value [description]
	 * @param  [type] $price [description]
	 * @param  [type] $pax   [description]
	 * @return [type]        [description]
	 */
	private function discountInAmount ( $type, $value, $price, $pax )
	{
		# Code here
		$computedPrice = 0;

		switch ( $type )
		{
			case 'percentage':
				# code...
				$computedPrice = round( $price * ( $value / 100 ) );
				break;
			
			case 'amount':
				# code...
				$computedPrice = round( $value * $pax );
				break;
		}

		return $computedPrice;
	} // end of discountInAmount

	/**
	 * [formatProductItems description]
	 * @param  [type] $data    [description]
	 * @param  [type] $isAgent [description]
	 * @return [type]          [description]
	 */
	private function formatProductItems ( $data, $isAgent )
	{
		# Code here
		$_items          = array();
		$_itemPriceKey   = ( $isAgent?'due_amount':'grossPrice' );
		$_productPOSTKey = array(
			'products'    => array( 'quantity' => 'guests', 'name' => 'PRODUCTS' ),
			'packages'    => array( 'quantity' => 'quantity', 'name' => 'PACKAGES' ),
			'extraBills'  => array( 'name' => 'EXTRABILLS' ),
			'suppliments' => array( 'quantity' => 'quantity', 'name' => 'SUPPLIMENTS' )
		);

		foreach ( $_productPOSTKey as $POSTKEY => $OPTIONS )
		{
			# code...
			if ( @$data[ $POSTKEY ] )
			{
				# Code here
				$_items[ $OPTIONS['name'] ] = array();
				foreach ( $data[ $POSTKEY ] as $product )
				{
					# code...
					$item = array();
					if ( isset( $product['included'] ) && ! $product['included'] )
					{
						# Code here
						continue;
					} // end of if statement

					$_quantity            = intval( ( isset( $OPTIONS['quantity'] ) ? $product[ $OPTIONS['quantity'] ] : $data['passengers'] ) );
					$item['name']         = $product['name'] . ' X' . $_quantity;
					$item['displayPrice'] = '$' . number_format((round($product['grossPrice']) * $_quantity), 2, '.', ',');
					$item['price']        = 0;
					$item['quantity']     = $_quantity;

					switch ( $POSTKEY ) {
						case 'extraBills':
							# code...
							$item['displayPrice'] = round(( filter_var($product['isMarkedUp'], FILTER_VALIDATE_BOOLEAN)?$product[ 'grossPrice' ]:$product['convertedBasePrice'] )) * $_quantity;
							$item['displayPrice'] = '$' . number_format($item['displayPrice'], 2, '.', ',');
							if ( filter_var($product['isMarkedUp'], FILTER_VALIDATE_BOOLEAN) )
							{
								# Code here
								if ( $isAgent )
								{
									# Code here
									$item['price'] = round( $product[ $_itemPriceKey ] );
									break;
								} // end of if statement
								$item['price'] = round( $product[ $_itemPriceKey ] ) * $_quantity;
								break;
							} // end of if statement
							$item['price'] = round((float)$product['convertedBasePrice'] * $_quantity);
							break;
						
						case 'suppliments':
							# code...
							$item['price'] += round($product[ ( $isAgent?'due_amount':'price' ) ]);
							break;
							
						default:
							# code...
							if ( $isAgent )
							{
								# Code here
								$item['price'] = round($product[ $_itemPriceKey ]);
								break;
							} // end of if statement
							$item['price'] += round($product[ $_itemPriceKey ]) * $_quantity;
							break;
					}
					$item['price'] = $item['price'] / $_quantity;
					array_push($_items[ $OPTIONS['name'] ], $item);
				}
			} // end of if statement
		}

		return $_items;
	} // end of formatProductItems

	public function treeview($array, $id = 0){
	        $return = array();
	        for ($i = 0; $i < count($array); $i++)
	         {

	            if($array[$i]['parent_id']==$id) {
	               $return[] = array(
	                    'val' => $array[$i],
	                    'sub_arr' => $this->treeview($array, $array[$i]['id'])
	                );

	            }
	         }
	        return empty($return) ? null : $return;    
	    }

	 /**
	  * [valid_phones description]
	  * @param  [type] $str      [description]
	  * @param  [type] $homenum  [description]
	  * @param  [type] $phonenum [description]
	  * @return [type]           [description]
	  */
	 public function valid_phones ( $str, $other )
	 {
	 	_POST();
	 	if ( empty( $str ) && empty( $_POST['voucher'][ $other ] ) )
	 	{
	 		# Code here
	 		$this->form_validation->set_message('valid_phones', 'Either %s is required');
	 		return FALSE;
	 	} // end of if statement
	 	return TRUE;
	 } // end of valid_phones

	/**
	 * [validate description]
	 * @return [type] [description]
	 */
	public function validate (  )
	{
		#response(array('SUCCESS' => TRUE, 'PAYPAL' => array('STATUS' => 'Success', 'REDIRECT_URL' => "https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=EC-1SM82547JB105271B")));
		#response(array('SUCCESS' => FALSE), 403);
		# Code here
		_POST();

		
		$rules       = array();

		$rulesOnPaymentCreditCard = $this->config->item('ET_booking_CREDIT_CARD');
		$rulesOnPaymentEnett      = $this->config->item('ET_booking_ENETT');
		$rulesOnVoucher           = $this->config->item('ET_booking_VOUCHER');
		$ruleETFlight             = $this->config->item('ET_FLIGHT');

		if ( filter_var($_POST['isPayable'], FILTER_VALIDATE_BOOLEAN) )
		{
			# Code here
			
			#$rules = array_merge(( isset( $_POST['isCreditCard'] )? (( $_POST['isCreditCard'] === 'true' )?$rulesOnPaymentCreditCard:$rulesOnPaymentEnett) : $rulesOnPaymentCreditCard ), $rulesOnVoucher);
			$_paymentRules = array();

			switch ( $_POST['payment_type'] )
			{
				case 'PAYPAL':
					break;
				case 'BANKDEPOSIT':
					# code...
					break;
				case 'CREDITCARD':
					# code...
					$_paymentRules = $rulesOnPaymentCreditCard;
					break;
				case 'E-NETT':
					$_paymentRules = $rulesOnPaymentEnett;
					break;
			}

			$rules = array_merge( $_paymentRules, $rulesOnVoucher );
		} else {
			$rules = $rulesOnVoucher;
		} // end of if statement
#print_r($rules);
#die();
		#if ( filter_var( @$_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN ) )
		#{
		#	# Code here
		#	$rules = array_merge( $rules, $ruleETFlight );
		#} // end of if statement
		$this->form_validation->set_rules( $rules );
		$this->form_validation->set_error_delimiters( '', '' );

		$errors = array();

		if ( $this->form_validation->run() === FALSE )
		{
			# Code here
			#$errors = array_merge($errors, $this->getErrorsOnArray( array('voucher', 'payment', 'flight') ));
			$errors = array_merge($errors, $this->getErrorsOnArray( array('voucher', 'payment') ));
		} // end of if statement

		if ( $this->isEmptyValueOnPassengers( 'passengers' ) )
		{
			# Code here
			$errors['passenger'] = array('There are some field which left empty.');
		} // end of if statement

		$errors['main'] = '';
		if ( $_POST['policy_read'] === 'false' )
		{
			# Code here
			$errors['main'] = '<li>Please read the cancellation policy and check the box. <a data-locate="#checkout-policy" class="cursor-pointer">Click here</a></li>';
		} // end of if statement

		if ( isset( $errors['passenger'] ) && ! empty( $errors['passenger'] ) )
		{
			# Code here
			$errors['main'] .= '<li>Some of the guests field was left empty. <a data-locate="#checkout-passengers" class="cursor-pointer">Click here</a></li>';
		} // end of if statement
		if ( isset( $errors['voucher'] ) && ! empty( $errors['voucher'] ) )
		{
			# Code here
			$errors['main'] .= '<li>Some voucher fields was not valid. <a data-locate="#checkout-voucher" class="cursor-pointer">Click here</a></li>';
		} // end of if statement
		if ( isset( $errors['payment'] ) && ! empty( $errors['payment'] ) )
		{
			# Code here
			$errors['main'] .= '<li>Please re-check your payment inputs. <a data-locate="#payment-methods" class="cursor-pointer">Click here</a></li></li>';
		} // end of if statement

		#if ( ! @$_POST['includeFlight'] )
		#{
		#	# Code here
		#	$btn = '<a style="cursor: pointer;" data-scroll-to="div.flight-details-option">Click here.</a>';
		#	if ( @$errors['main'] )
		#	{
		#		# Code here
		#		$errors['main'] .= '<li>Please select a flight option. ';
		#	} else {
		#		$errors['main'] = '<ul><li>Please select a flight option. ';
		#	}// end of if statement
		#	$errors['main'] .= $btn . '</li>';
		#} // end of if statement

		#if ( ! @$_POST['flight']['terms'] && filter_var(@$_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN) )
		#{
		#	# Code here
		#	$btn  = ' <a style="cursor: pointer;" data-scroll-to="input#flight-terms">Click here.</a>';
		#	$text = 'Please read the flight terms and conditions.';
		#	
		#	if ( @$errors['main'] )
		#	{
		#		# Code here
		#		$errors['main'] .= '<li>' . $text;
		#	} else {
		#		$errors['main'] = '<ul><li>' . $text;
		#	}// end of if statement
		#	$errors['main'] .= $btn . '</li>';
		#} // end of if statement

		if ( ! empty($errors['main']) )
		{
			# Code here
			$str = '<ul>' . $errors['main'] . '</ul>';
			$errors['main'] = $str;
		} else {
			unset($errors['main']);
		} // end of if statement


		if ( ! empty( $errors ) )
		{
			# Code here
			response( array('success' => FALSE, 'errors' => $errors, 'data' => (bool)$_POST['policy_read'], 'err' => form_error('voucher')), 403 );
		} // end of if statement
		/**
		 * Replay PayWal to PayPal
		 */
		
		$response = array( 'SUCCESS' => TRUE );

		if ( filter_var($_POST['isPayable'], FILTER_VALIDATE_BOOLEAN) )
		{
			$_PAYMENT_TYPES = json_decode(PAYMENT_TYPES, TRUE);

			switch ( $_POST['payment_type'] ) {
				case 'PAYPAL':

					$_products   = $_POST['payment'];
					$_vouchers   = $_POST['voucher'];

					$_PaymentDetailsItems = array();
					foreach ( $_products as $_product )
					{
						# code...
						if ( is_array( $_product ) )
						{
							# Code here
							foreach ( $_product as $product )
							{
								# code...
								if ( is_array( $product ) )
								{
									# Code here
									if ( ! intval($product['price']) )
									{
										# Code here
										continue;
									} // end of if statement
									array_push($_PaymentDetailsItems, array(
										'Name'         => $product['name'],
										'Quantity'     => $product['quantity'],
										'Amount'       => array('value' => $product['price'], 'currencyID' => 'AUD'),
										'ItemCategory' => 'Physical',
									));
								} // end of if statement
							}
						} // end of if statement
					}

					#$_ShipToAddress = array(
					#	'Name'            => $_vouchers['firstname'] . ' ' . $_vouchers['firstname'],
					#	'Street1'         => 'NONE',
					#	'CityName'        => 'NONE',
					#	'StateOrProvince' => 'NONE',
					#	'PostalCode'      => 'NONE',
					#	'Country'         => 'NONE',
					#);

					#$_payment_amount = array('value' => (@$_products['discount']?((float)$_products['amount'] + (float)$_products['discount']):(float)$_products['amount']), 'currencyID' => 'AUD');
					#$_payment_amount['value'] = number_format($_payment_amount['value'], 2, '.', ',');

					$_payment_amount = array('value' => number_format($_products['amount'], 2, '.', ','), 'currencyID' => 'AUD');
					
					// if ( @$_products['discount'] )
					// {
					// 	# Code here
					// 	#$_PaymentDetails['ShippingDiscount'] = array('value' => number_format($_products['discount'], 2, '.', ','), 'currencyID' => 'AUD');
					// 	array_push($_PaymentDetailsItems, array(
					// 		'Name'         => 'Discount',
					// 		'Quantity'     => 1,
					// 		'Amount'       => array('value' => '-' . number_format($_products['discount']), 'currencyID' => 'AUD'),
					// 		'ItemCategory' => 'Physical',
					// 	));
					// } // end of if statement

					$_PaymentDetails['OrderTotal']         = $_payment_amount;
					$_PaymentDetails['ItemTotal']          = $_payment_amount;
					$_PaymentDetails['PaymentDetailsItem'] = $_PaymentDetailsItems;
					$_PaymentDetails['PaymentAction']      = 'Sale';

					#'ItemTotal'        => 
					#'ShippingTotal'     => 5,
					#'TaxTotal'          => 6,
					#'PaymentRequestID'  => random_string('alnum', 16),
					#'ShipToAddress'      => $_ShipToAddress,
					#'PaymentDetailsItem' => ,
					#'PaymentAction'      => ,

					$params = array(
						'ReturnURL'          => base_url() . 'externalToursAddGuest/callbackPayPal',
						'CancelURL'          => base_url() . 'externalToursAddGuest/canceledPayPal',
						'NoShipping'         => 0,
						'ReqConfirmShipping' => 0,
						'AllowNote'          => 0,
						'BrandName'          => BRAND_NAME,
						'PaymentDetails'     => $_PaymentDetails,
					);

					#response(array('SUCCESS' => FALSE, 'DEBUG' => $params), 403);
					try{
						$this->load->library('paypal');
						$result = $this->paypal->setExpressCheckout( $params );

						$response[ $_PAYMENT_TYPES['PP'] ] = array(
							'STATUS' => $result['Ack'],
							#'DEBUG'  => $result,
						);
						if ( @$result['Ack'] && $result['Ack'] != 'Success' )
						{
							# Code here
							$PPError = $this->paypal->formatArray( $result );
							response(array(
								'success' => FALSE,
								'errors'  => array(
									'main' => '<ul><li>There was an error while processing payment operation.</li></ul>'
								),
								'DEBUG' => $PPError,
							), 403 );
						}
						$token     = $result['Token']['_value'];

						#$this->config->load('paypal');
						#$payPalURL = 'https://www.' . ($this->config->item('Sandbox')?'sandbox.':'') . 'paypal.com/webscr?cmd=_express-checkout&token=' . $token;

						$this->load->model('booking_data');
						$response['PAYPAL']['TOKEN']     = $token;
						$response['PAYPAL']['REFERENCE'] = $this->booking_data->reference();
					} catch(Exception $e){
						response(array(
								'success' => FALSE,
								'errors'  => array(
									'main' => '<ul><li>There was an error while validating inputs.</li></ul>'
								),
								'DEBUG' => $e->getMessage(),
							), 403 );
					}
					
					break;

				case 'CREDITCARD':
					$this->load->library('payway');
						
					try {
						$cc_output=$this->payway->singleUseToken(array(
							'paymentMethod'   =>'creditCard',
							'cardNumber'      =>$_POST['payment']['no_credit_card'],
							'cardholderName'  =>$_POST['payment']['nm_card_holder'],
							'cvn'             =>$_POST['payment']['no_cvn'],
							'expiryDateMonth' =>$_POST['payment']['dt_expiry_month'],
							'expiryDateYear'  =>$_POST['payment']['dt_expiry_year'],
						));

						$response[ $_PAYMENT_TYPES['CC'] ] = array(
							'TOKEN'=>$cc_output['singleUseTokenId'],
							'TIME'   => date('H:i:s', strtotime('now')),
							'DATE'   => date('Y-m-d', strtotime('now')),
							'STATUS' => 'approved',
							'AMOUNT' => $_POST['payment']['amount']
						);
					} catch (Exception $e) {
						$cc_msg=$e->getMessage();
						$cc_msg=preg_replace(array('/by your/'), array('by our'), $cc_msg);
						response(array(
							'success' => FALSE,
							'errors'  => array(
								'main' => '<ul><li>'.$cc_msg.'</li></ul>'
							)
						), 403 );
					}
				default:
					# code...
					break;
			}
		}

		response( $response );
	} // end of validate

	/**
	 * [savePayPalData description]
	 * @param  [type] $data      [description]
	 * @param  [type] $refenrece [description]
	 * @return [type]            [description]
	 */
	public function savePayPalData (  )
	{
		# Code here
		_POST();
		#$_POST['reference'] = $reference;
		$_POST['expireAt']  = new MongoDate(strtotime('now +50 minutes'));

		$this->load->model('booking_data');
		$this->booking_data->insert( $_POST, 'tours' );

		$this->nativesession->set('paypal_reference', trim($_POST['reference']));

		$this->config->load('paypal');
		$payPalURL = 'https://www.' . ($this->config->item('Sandbox')?'sandbox.':'') . 'paypal.com/webscr?cmd=_express-checkout&token=' . trim($_POST['token']);
		response(array('SUCCESS' => TRUE, 'REDIRECT' => $payPalURL));
		exit();
	} // end of savePayPalData

	/**
	 * [isEmptyValueOnPassengers description]
	 * @return boolean [description]
	 */
	private function isEmptyValueOnPassengers ( $property = 'adults' )
	{
		# Code here
		# This means that the property is CHILDS
		# This leads to the situation where adults has no empty fields
		#if ( ! isset( $_POST['passenger'][ $property ] ) )
		if ( ! isset( $_POST['passengers'] ) )
		{
			# Code here
			return TRUE;
		} // end of if statement

		#foreach ( $_POST['passenger'][ $property ] as $key => $passenger )
		foreach ( $_POST['passengers'] as $key => $passenger )
		{
			# code...
			foreach ( $passenger as $key => $value )
			{
				# code...
				if ( empty( $value ) )
				{
					# Code here
					return TRUE;
				} // end of if statement
			}
		}

		#if ( $property === 'adults' )
		#{
		#	# Code here
		#	$this->isEmptyValueOnPassengers( 'childs' );
		#} // end of if statement
		return FALSE;
	} // end of isEmptyValueOnPassengers


	/**
	 * [getErrorsOnArray description]
	 * @param  [type] $subArray [description]
	 * @return [type]           [description]
	 */
	private function getErrorsOnArray ( $subArray )
	{
		# Code here
		$errors = array();
		foreach ( $subArray as $key => $value )
		{
			# code...
			$errors[ $value ] = array();
			if ( isset( $_POST[ $value ] ) )
			{
				# Code here
				foreach ( $_POST[ $value ] as $post_key => $post_value )
				{
					# code...
					$msg = form_error( $value . '[' . $post_key .']' );

					if ( ! empty( $msg ) )
					{
						# Code here
						array_push($errors[ $value ], $msg);
					} // end of if statement
				}
			} // end of if statement
		}
		foreach ( $errors as $key => $error )
		{
			# code...
			if ( empty( $error ) )
			{
				# Code here
				unset( $errors[ $key ] );
			} // end of if statement
		}
		if( isset( $_POST['isCreditCard'] ) && $_POST['isCreditCard'] == 'false' )
		{
			 if( isset( $errors['payment'] ) )
			 {
			 	$errors['e-nett-payment'] = $errors['payment'];
				unset( $errors['payment'] );
			 }
		}

		return $errors;
	} // end of getErrorsOnArray
	
	/**
	 * [canceledPayPal description]
	 * @return [type] [description]
	 */
	public function canceledPayPal (  )
	{
		# Code here
		$this->load->model('booking_data');
		$refenrece = $this->nativesession->get('paypal_reference');
		$sessioned = $this->booking_data->get( $refenrece, 'tours' );
		?>
<p>If not automatically redirected please <a href="<?=base_url() . $sessioned['previous_link']?>">Click here</a></p>
		<?php
		redirect( $sessioned['previous_link'] );
	} // end of canceledPayPal

	/**
	 * [callbackPayway description]
	 * @return [type] [description]
	 */
	public function callbackPayPal (  )
	{
		# Code here;
		$this->load->library('paypal');
		$this->load->model('booking_data');

		$collectionsName = 'tours';
		$reference = $this->nativesession->get('paypal_reference');

		$data = $this->paypal->getExpressCheckout( $_GET['token'] );
		$data = $this->paypal->formatArray( $data );

		$sessioned = $this->booking_data->get( $reference, $collectionsName );

		if ( strtolower( $data['Ack'] ) != 'success' )
		{
			# Code here
			?>
<p>Sorry, there was an error while processing your request.</p>
<p>If not automatically redirected please <a href="<?=base_url() . $sessioned['previous_link']?>">Click here</a></p>
			<?php
			redirect( $sessioned['previous_link'] );
			die();
		} // end of if statement
		$details   = $data['GetExpressCheckoutDetailsResponseDetails'];
		$date_time = strtotime($data['Timestamp']);
		$payment   = array(
			'reference'      => $details['PayerInfo']['PayerID'],
			'card_type'      => 'PAYPAL-Express checkout',
			'bank_reference' => $details['Token'],
			'payment_amount' => $details['PaymentDetails']['OrderTotal']['value'],
			'payment_date'   => date('Y-m-d', $date_time),
			'payment_number' => $date_time,
			'payment_status' => 'approved',
			'payment_time'   => date('H:i', $date_time),
			'discount'       => $sessioned['costing']['discount_amount']
		);

		$this->booking_data->update( $reference, array('payment' => $payment), $collectionsName);
		?>
<p>If not automatically redirected in a few seconds please <a href="<?=base_url()?>externalToursAddGuest/createBooking?payment=true&type=paypal&name=<?=$collectionsName ?>">Click here</a></p>
		<?php
		redirect('externalToursAddGuest/createBooking?payment=true&type=paypal&name=' . $collectionsName);

		#echo '<script type="text/javascript">';
		#	echo 'window.opener.IS_WIN_FROM_API = true;';
		#	echo 'window.opener.paypalPaymentConfirmation( ' . str_replace("\"", "'", json_encode( $data )) . ' );';
		#	echo 'window.close();';
		#echo '</script>';
	} // end of callbackPayway


	/**
	 * [createBooking description]
	 * @return [type] [description]
	 */
	public function createBooking (  )
	{
		_POST();
		#die();
		// echo 'test';
		// echo json_encode($_POST);
		// response(array('success' => FALSE, 'message' => 'TEST','map'=>TZ_agency_code_map($this->session->userdata('agencycode'))), 403);
		# Code here

		// response( array('success' => FALSE, 'message' => 'Create booking', 'data' => $_POST) );
		// Used for Payway Callback
		if ( isset( $_GET['payment'] ) && filter_var($_GET['payment'], FILTER_VALIDATE_BOOLEAN) )
		{
			# Code here
			$this->load->model('booking_data');
			switch ( trim($_GET['type']) ) {
				case 'paypal':
					$_POST = $this->booking_data->get( trim($this->nativesession->get('paypal_reference')), trim($_GET['name']) );
					break;
				
				default:
					# code...
					break;
			}
		} // end of if statement
		#response(array('SUCCESS' => FALSE, 'DEBUG' => $_POST), 403);
		
		$DATE_CREATED      = '';
		$_PAYMENT_TYPES    = json_decode(PAYMENT_TYPES, TRUE);
		$_passengers       = $_POST['passengers'];
		$_payment          = @$_POST['payment'];
		$_voucher          = $_POST['voucher'];
		#$_bookingItem      = $_POST['item'];
		$_agency             = _AGENCY( $this->session->userdata('agencycode') );
		$_costing            = $_POST['costings'];
		$_final_costing      = $_POST['final_costing'];
		$_flight             = @$_POST['flight'];
		$_item_product       = $_POST['product'];
		$_item_tour          = $_POST['tour'];
		$_item_supplements   = @$_POST['supplements'];
		$_item_list_of_items =$_POST['items'];

		$_IS_DISCOUNTED = filter_var($_POST['isDiscounted'], FILTER_VALIDATE_BOOLEAN);
		$_HAS_FLIGHT    = filter_var($_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN);
		$_IS_PAYABLE    = filter_var($_POST['isPayable'], FILTER_VALIDATE_BOOLEAN);
		$_IS_RECEIPTED  = FALSE;

		$unique_transaction_id   =null;
		if ( $_IS_PAYABLE )
		{
			switch ( $_POST['payment_type'] ) {
				case 'PAYPAL':
					break;
				case 'CREDITCARD':
					# code...
					$this->load->library('payway');
					$cc_ref=TZ_agency_code_map($this->session->userdata('agencycode')) .'-'. preg_replace('/[^0-9]/', '', $_payment['booking_ref_num']);
					$unique_transaction_id=preg_replace('/[^0-9]/', '', $_payment['booking_ref_num']);
					try {
						if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
   							 $ip = $_SERVER['HTTP_CLIENT_IP'];
						} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    							$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
							} else {
    							$ip = $_SERVER['REMOTE_ADDR'];
						}
						$output=$this->payway->takePayment(array(
							'singleUseTokenId'=>$_payment['token'],
							'customerNumber'=>$cc_ref,
							'transactionType'=>'payment',
							'principalAmount'=>$_payment['payment_amount'],
							'currency'=>'aud',
							'customerIpAddress'=>$ip
						));
						if ($output['status']!=='approved') {
							$message=$output['responseText'];
							response( array( 'success' => TRUE, 'message' => 'Sorry, we cannot process your booking request. Please see payment details below. <br><ul><li>'.$message.'</li></ul>'), 403 );
						}
					} catch (Exception $e) {
						$message=$e->getMessage();
						response(array('success' => TRUE, 'message' => 'Sorry, we cannot process your booking request. Please see payment details below. <br><ul><li>'.$e->getMessage().'</li></ul>'), 403);
					}
					break;
				case 'E-NETT':
					$this->load->library('enett');
					try{
						$unique_transaction_id=get_next_id('booking');
						$_enett=$this->enett->processDebit(array(
							'transID'=>date('Y-m-d H:i:s',strtotime('now')),
							// 'agentID'=>'500423',
							'agentID'=>$_payment['agent_id'],
							// 'agentID'=>'',
							'payer'=>$_payment['agent_id'],
							'source'=>base_url(),
							'primaryRef'=>$unique_transaction_id,
							'secondaryRef'=>$unique_transaction_id,
							'passengerName'=>$_payment['consultant_name'],
							'notes'=>'B2C '.TZ_agency_code_map($this->session->userdata('agencycode')).' '.$_payment['consultant_name'],
							'amount'=>$_payment['payment_amount'],
							'currency'=>'AUD',
							'paymentDate'=>date('m-d-Y',strtotime('now')),
							'departureDate'=>date('m-d-Y',strtotime($_item_tour['end'])),
						));
						$_trasaction='E'.$_enett->transactionID.'-'.$unique_transaction_id;
						$_POST['payment']['reference']='E'.$_enett->transactionID;
						$_POST['payment']['trasaction']=$_trasaction;
					}catch (Exception $e) {
						// echo $e->getMessage();
						response( array( 'success' => TRUE, 'message' => 'Sorry, we cannot process your booking request. Please see payment details below. <br><ul><li>'.$e->getMessage().'</li></ul>'), 403 );
					}
					break;
				default:
					# code...
					break;
			}
		}
		$_bookingId=$this->booking_model->newBooking( $this->nativesession->get('agencyRealCode'), TRUE,(is_null($unique_transaction_id)?FALSE:$unique_transaction_id) );
#echo date('Y-m-d', strtotime('now')) . '<br>' . PHP_EOL;
#echo 'Deposit before the end of ', date('M d, Y', strtotime('now +7 day')) . '<br>' . PHP_EOL; // Before this day the client must pay a deposit payment
#echo 'Full payment before the end of ', date('M d, Y', strtotime(#$_bookingItem['startdate'])) . '<br>' . PHP_EOL; // Before this day the client must pay a full payment
#header('Response', TRUE, 403);
#die();
		#if ( filter_var( @$_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN ) )
		#{
		#	# Code here
		#	$_POST['inclusion'] .= ' With return Flights fee of $' . number_format($_flight['price'], 2, '.', ',') . '.';
		#	#$_costing['gross_amount'] += round( intval($_flight['price']) );
		#} // end of if statement

		$connections = new MongoClient();
		$db          = $connections->db_system;
		$_product    = $db->products_inventory->findOne( array('productId' => (int)$_item_product['id']), array('_id' => 0) );

		$connections->close();

		$dateDiffStart = date_create( $_item_tour['start'] );
		$dateDiffEnd   = date_create( $_item_tour['end'] );
		$_dateDiff     = date_diff( $dateDiffStart, $dateDiffEnd );

		$item = array();
		$item['bookingId']       = $_bookingId;
		$item['itemId']          = 'New';
		$item['itemName']        = $_item_product['name'];
		$item['itemCreatedType'] = 'DB - External Tours';
		$item['itemCode']        = "ET-" . $_product['productId'];
		$item['itemServiceCode'] = $_product['productServiceCode'];
		$item['itemServiceName'] = $_product['productServiceName'];

		$item['itemSupplierCode'] = $_product['productSupplierCode'];
		$item['itemSupplierName'] = $_product['productSupplierName'];

		// $item['itemStatus']              = ( $_POST['payment_type'] == 'PAYPAL' ) ? 'Confirmed' : 'Quote';
		$item['itemStatus']              = 'Quote';
		$item['itemSuplierConfirmation'] = ( isset( $_payment['reference'] )? $_payment['reference']: '' );
		// $item['itemSuplierOtherConfirmation'] = date('d F, Y', strtotime('now'));

		$item['itemFromCountry'] = $_product['productFromCountry'];
		$item['itemFromCity']    = $_product['productFromCity'];
		$item['itemFromAddress'] = $_product['productFromAddress'];
		$item['itemToCountry']   = $_product['productToCountry'];
		$item['itemToCity']      = $_product['productToCity'];
		$item['itemToAddress']   = $_product['productToAddress'];
		
		$item['itemStartDate']     = date('d/m/Y', strtotime($_item_tour['start']) );
		$item['itemEndDate']       = date('d/m/Y', strtotime($_item_tour['end']) );
		$item['itemStartTime']     = '12:00:00';
		$item['itemEndTime']       = '12:00:00';
		$item['itemNumDays']       = $_dateDiff->days;
		$item['itemNumNights']     = $_dateDiff->days;
		$item['itemIsLeadPax']     = 1;
		$item['itemIsFullPax']     = 1;
		$item['itemIsVoucherable'] = 1;
		$item['itemIsAutoInvoice'] = 1;
		$item['itemIsInvoicable']  = 1;
		$item['itemIsIterary']     = 1;
		$item['itemAdultMin']      = 1;
		$item['itemAdultMax']      = count( $_passengers );

		#$item['itemChildMax']       = ( ( isset( $_passengers['childs'] ) )? count( $_passengers['childs'] ): 0 );
		$item['itemIsPaxAllocated'] = 1;
		$item['itemIsLive']         = 1;
		// $item['itemIsConfirmed']    = ( $_POST['payment_type'] == 'PAYPAL' ) ? 1 : 0;
		$item['itemIsConfirmed']    = 0;
		$item['application']        = _APPCODE('ET');
		$item['PAYMENT_TYPE']       = $_POST['payment_type'];
		$item['VOUCHER_EMAIL']      = $_voucher['email'];

		$item['itemSumarryDescription']    = $_product['productSumarryDescription'];
		$item['itemDetailedDescription']   = $_product['productDetailedDescription'];
		$item['itemExclusionDescription']  = $_product['productExclusionDescription'];
		$item['itemConditionsDescription'] = $_product['productConditionsDescription'];
		$item['itemInclusionDescription']  = $_POST['inclusion'];

		$item['itemCancellation'] = array();

		$remaining_days = 0;

		if( ! $_IS_PAYABLE )
		{
			if ( $_POST['payment_type'] == $_PAYMENT_TYPES['BD'] )
			{
				# Code here
				$_startDate = strtotime( 'now' );
				$_endDate   = strtotime( 'now +1 day' );

				array_push( $item['itemCancellation'], array(
					'dateFrom'         => date('Y-m-d', $_startDate),
					'dateTo'           => date('Y-m-d', $_endDate),
					'typeCancellation' => 'Percentage',
					'percent'          => 100,
					'amount'           => ((float)$_costing['due_amount'])
				));

				$item['itemStartTime'] = date('H:i', $_startDate);
				$item['itemEndTime']   = date('H:i', $_endDate);
			} else {
				$remaining_days = date_difference( $_item_tour['start'] );
				// If the date is less than 7 days then it should be fully paid before the start date
				// Else a cancellation policy should remind that before the departure customer should deposit a 30 percent of the total price
				if ( $remaining_days >= 67 )
				{
					# Code here
					array_push( $item['itemCancellation'], array(
						'dateFrom'         => date('Y-m-d', strtotime( 'now' )),
						'dateTo'           => date('Y-m-d', strtotime( 'now +7 day' )),
						'typeCancellation' => 'Percentage',
						'percent'          => 30,
						'amount'           => ((float)$_costing['due_amount'] * 0.30)
					));
				} // end of if statement

				array_push( $item['itemCancellation'], array(
					'dateFrom'         => date('Y-m-d', strtotime( 'now +7 day' )),
					'dateTo'           => $_item_tour['start'],
					'typeCancellation' => 'Percentage',
					'percent'          => 100,
					'amount'           => (float)$_costing['due_amount']
				));
			} // end of if statement
		}

		$item['itemCostings'] = array(
			'fromCurrency'      => $_costing['originalCurrency'],
			'toCurrency'        => $_costing['convertedCurrency'],
			'toRate'            => (float)$_costing['rateOfConvertion'],
			
			'originalAmount'    => (float)$_costing['OriginalBasePrice'],
			'orginalNetAmt'     => (float)$_costing['convertedBasePrice'],
			'grossAmt'          => (float)$_costing['gross_amount'],
			
			'netAmountInAgency' => (float)$_costing['convertedBasePrice'],
			'markupInAmount'    => (float)$_costing['markup_amount'],
			'markupInPct'       => (float)$_costing['markup_pct_amount'],
			
			'commInPct'         => (float)$_costing['comm_pct_amount'],
			'commInAmount'      => (float)$_costing['commission'],
			'commAmt'           => (float)$_costing['commission'],
			
			'totalLessAmt'      => (float)$_costing['total_less_amount'],
			'totalDueAmt'       => (float)$_costing['due_amount'],
			'totalProfitAmt'    => (float)$_costing['profit_amount'],

			'discInPct'         => (float)$_costing['discount_pct'],
			'discInAmount'      => (float)$_costing['discount_amount'],
			'discAmt'           => (float)$_costing['discount_amount']
		);
		if ( $_item_supplements )
		{
			# Code here
			$item['itemSupplements'] = array_values($_item_supplements);
		} // end of if statement

		

		$item = $this->bookinglib->buildItemArray( $item );

		$DATE_CREATED = strtotime('now');
		$_item = $this->bookinglib->executeAddItemToBooking( $item );
		
		/*==============================
		=            FLIGHT            =
		==============================*/
		
		
		if (isset($_item_list_of_items['extrabills'])&&!empty($_item_list_of_items['extrabills'])) {
			foreach ($_item_list_of_items['extrabills'] as $key => $_data_extra_bill) {
				$_extra_bill = $item;
				$_extra_bill['itemName']        = $_data_extra_bill['name'];
				$_extra_bill['itemServiceCode'] = 'Service Fee';
				$_extra_bill['itemServiceName'] = 'Service Fee';
				$_extra_bill['itemSupplements'] =array();
				$_extra_bill['itemCostings'] =array(
					'fromCurrency'      => $_data_extra_bill['costings']['fromCurrency'],
					'toCurrency'        => $_data_extra_bill['costings']['toCurrency'],
					'toRate'            => (float)$_data_extra_bill['costings']['rate'],
					
					'originalAmount'    => (float)$_data_extra_bill['costings']['basePrice'],
					'orginalNetAmt'     => (float)$_data_extra_bill['costings']['converted'],
					'grossAmt'          => (float)$_data_extra_bill['costings']['gross_amount'],
					
					'netAmountInAgency' => (float)$_data_extra_bill['costings']['converted'],
					'markupInAmount'    => (float)$_data_extra_bill['costings']['markup_amount'],//
					'markupInPct'       => (float)$_data_extra_bill['costings']['markup_pct'],
					
					'commInPct'         => (float)$_data_extra_bill['costings']['commission_pct'],
					'commInAmount'      => (float)$_data_extra_bill['costings']['commission_amount'],
					'commAmt'           => (float)$_data_extra_bill['costings']['commission_amount'],
					
					'totalLessAmt'      => (float)$_data_extra_bill['costings']['total_less_amount'],
					'totalDueAmt'       => (float)$_data_extra_bill['costings']['due_amount'],
					'totalProfitAmt'    => (float)$_data_extra_bill['costings']['profit_amount'],

					'discInPct'         => (float)$_data_extra_bill['costings']['discount_pct'],
					'discInAmount'      => (float)$_data_extra_bill['costings']['discount_amount'],
					'discAmt'           => (float)$_data_extra_bill['costings']['discount_amount']
				);
				/*foreach (array('originalAmount',
					'convertedBasePrice',
					'orginalNetAmt',
					'netAmountInAgency',
					'markupInAmount',
					// 'markupInPct',
					'grossAmt',
					'commInAmount',
					'commAmt',
					// 'discInPct',
					'discInAmount',
					'discAmt',
					// 'totalLessAmt',
					// 'totalDueAmt',
					// 'totalProfitAmt',
					) as $_key => $_value) {
					if (isset($_extra_bill['itemCostings'][$_value])) {
						$_extra_bill['itemCostings'][$_value]=$_extra_bill['itemCostings'][$_value]*(int)$_data_extra_bill['quantity'];
					}
				}*/
				// $_extra_bill['itemCostings']['grossAmt']=$_extra_bill['itemCostings']['netAmountInAgency']+$_extra_bill['itemCostings']['markupInAmount'];
				// $_extra_bill['itemCostings']['totalLessAmt']=(float)$_extra_bill['itemCostings']['commInAmount']+(float)$_extra_bill['itemCostings']['discInAmount'];
				// supplement.supplement_costings.totalLessAmt=(supplement.supplement_costings.commInAmount+supplement.supplement_costings.discInAmount);
				// $_extra_bill['itemCostings']['totalDueAmt']=(float)$_extra_bill['itemCostings']['grossAmt']-(float)$_extra_bill['itemCostings']['totalLessAmt'];
				// supplement.supplement_costings.totalDueAmt=(supplement.supplement_costings.grossAmt-supplement.supplement_costings.totalLessAmt);
				// $_extra_bill['itemCostings']['totalProfitAmt']=(float)$_extra_bill['itemCostings']['markupInAmount']-(float)$_extra_bill['itemCostings']['totalLessAmt'];
				// supplement.supplement_costings.totalProfitAmt=(supplement.supplement_costings.markupInAmount-supplement.supplement_costings.totalLessAmt);
				$_extra_bill  = $this->bookinglib->buildItemArray( $_extra_bill );
				$this->bookinglib->executeAddItemToBooking( $_extra_bill );
			}
		}
		if ( $_HAS_FLIGHT )
		{
			# Code here

			$dateDiffStart = date_create( date('Y-m-d', strtotime(str_replace('/', '-', $_flight['departure']))) );
			$dateDiffEnd   = date_create( date('Y-m-d', strtotime(str_replace('/', '-', $_flight['return']))) );
			$_dateDiff     = date_diff( $dateDiffStart, $dateDiffEnd );

			$_flight_item = $item;
			$_flight_item['itemName']        = 'Tour Flight - ' . $_flight['city'];
			$_flight_item['itemServiceCode'] = 'FLT';
			$_flight_item['itemServiceName'] = 'Flights';
			$_flight_item['itemStatus']      = 'Quote';
			$_flight_item['itemIsConfirmed'] = 0;

			$_flight_item['itemStartDate'] = $_flight['departure'];
			$_flight_item['itemEndDate']   = $_flight['return'];
			$_flight_item['itemStartTime'] = '12:00:00';
			$_flight_item['itemEndTime']   = '12:00:00';
			$_flight_item['itemNumDays']   = $_dateDiff->days;
			$_flight_item['itemNumNights'] = $_dateDiff->days;
			
			$_flight_item['itemFromCity']  = $_flight['city'];
			$_flight_item['itemSupplements']=array();
			$_flight_item['itemCostings'] = array(
				'fromCurrency'      => 'AUD',
				'toCurrency'        => 'AUD',
				'toRate'            => 1,
				
				'originalAmount'    => (float)$_flight['subtotal'],
				'orginalNetAmt'     => 0,
				'grossAmt'          => (float)$_flight['subtotal'],
				
				'netAmountInAgency' => 0,
				'markupInAmount'    => 0,
				'markupInPct'       => 0,
				
				'commInPct'         => 0,
				'commInAmount'      => 0,
				'commAmt'           => 0,
				
				'totalLessAmt'      => (float)0,
				'totalDueAmt'       => (float)$_flight['subtotal'],
				'totalProfitAmt'    => (float)0,

				'discInPct'         => 0,
				'discInAmount'      => (float)$_flight['discount'],
				'discAmt'           => (float)$_flight['discount']
			);
			$_flight_item['itemCostings']['discInPct']=($_flight_item['itemCostings']['discInAmount']/$_flight_item['itemCostings']['originalAmount'])*100;
			$_flight_item['itemCostings']['totalLessAmt']=$_flight_item['itemCostings']['commInAmount']+$_flight_item['itemCostings']['discInAmount'];
			$_flight_item['itemCostings']['totalDueAmt']=$_flight_item['itemCostings']['grossAmt']-$_flight_item['itemCostings']['totalLessAmt'];
			// $_flight_item['itemCostings']['totalProfitAmt']=$_flight_item['itemCostings']['markupInAmount']-$_flight_item['itemCostings']['totalLessAmt'];

			$_flight_item  = $this->bookinglib->buildItemArray( $_flight_item );
			$this->bookinglib->executeAddItemToBooking( $_flight_item );
		} // end of if statement

		/*=====  End of FLIGHT  ======*/
		
		// unset( $item );

		if ( ! isset( $_item['status'] ) || $_item['status'] === FALSE )
		{
			# Code here
			$message='There was an error while process your request';
			$this->bookinglib->failed($message);
			response( array('success' => FALSE, 'message' => $message), 403 );
		} // end of if statement

		$this->bookinglib->init( $_bookingId, $_item['item_id'] );

		$_guests = array();

		foreach ( $_passengers as $key => $pax )
		{
			# code...
			array_push( $_guests, $this->bookinglib->buildGuest( $pax['title'], $pax['firstname'], $pax['lastname'], 33, $_agency['agency_name'] ) );
		}

		$paxIds = $this->bookinglib->executeItemGuests( $_guests );

		if ( ! $paxIds['success'] )
		{
			# Code here
			$message='There was an error while processing guests';
			$this->bookinglib->failed($message);
			// response( array( 'success' => FALSE, 'message' => $message, 'debug' => $paxIds ), 403 );
			response( array( 'success' => FALSE, 'message' => $message ), 403 );
		} // end of if statement

		$allocatedGuest = $this->bookinglib->executeAllocateGuests( $paxIds['paxIds'] );

		if ( ! isset( $allocatedGuest['status'] ) || ! $allocatedGuest['status'] )
		{
			# Code here
			$message='There was an error while allocating guests';
			$this->bookinglib->failed($message);
			// response( array( 'success' => FALSE, 'message' => 'There was an error while allocating guests', 'debug' => $allocatedGuest ), 403 );
			response( array( 'success' => FALSE, 'message' => $message), 403 );
		} // end of if statement

		/*$convertedItem = $this->bookinglib->executeConvertItem(  );

		if ( ! isset( $convertedItem['status'] ) || ! $convertedItem['status'] )
		{
			# Code here
			response( array( 'success' => FALSE, 'message' => 'There was an error while converting item', 'debug' => $convertedItem ), 403 );
		} // end of if statement*/

		// PAYMENT BLOCK
		if ( $_IS_PAYABLE )
		{
			# Code here
			$receipt             = array();
			$_PAYMENT_TYPES_TEXT = json_decode(PAYMENT_TYPES_TEXT, TRUE);

			switch ( $_POST['payment_type'] ) {
				case 'PAYPAL':
					$receipt = $this->bookinglib->buildCCReceipt(array(
						'receiptTypeCode'       => 'RA',
						'receiptTypeName'       => 'Receipt Allocation',
						'receiptDescription'    => 'External Tours: PayPal Payment [' . $_payment['reference'] . '-'. $_bookingId . ']',
						'receiptCCTypeCode'     => $_PAYMENT_TYPES_TEXT[ $_POST['payment_type'] ],
						'receiptCCTypeName'     => $_PAYMENT_TYPES_TEXT[ $_POST['payment_type'] ],
						'receiptCCNumber'       => 000000000000000,
						'receiptCCExpiryDate'   => '12/20',
						'receiptCCSecurityCode' => 123,
						'receiptDisplayAmout'   => 0,
						'receiptAmout'          => 0,
						'receiptDepositedAmout' => $_payment['payment_amount'],
						'receiptRefundAmout'    => 0
					));
					
					break;
				case 'CREDITCARD':
					# code...
					$cc_ref=TZ_agency_code_map($this->session->userdata('agencycode')) .'-'. $_bookingId;
					$cc_connection=new MongoClient();
					$cc_db=$cc_connection->db_system;
					$cc_db->payway->insert(array(
						'customerRef'=>$cc_ref,
						'bookingId'=>$_bookingId,
						'itemName'=>$item['itemName'],
						'status'=>$output['status'],
						'guests'=>$_passengers,
						'voucher'=>$_voucher,
						'costing'=>$item['itemCostings'],
						'domain'=>base_url(),
						'system'=>'B2C',
						'createdAt'=>new MongoDate(strtotime('now')),
					));
					$cc_connection->close();
					$receipt = $this->bookinglib->buildCCReceipt(array(
						'receiptTypeCode'       => 'PAYWAY',
						'receiptTypeName'       => 'Payway Gateway',
						'receiptDescription'    => 'External Tours: Credit Card Payment [reference: '.$cc_ref. ']',
						'receiptCCTypeCode'     => $_PAYMENT_TYPES_TEXT[ $_POST['payment_type'] ],
						'receiptCCTypeName'     => $_PAYMENT_TYPES_TEXT[ $_POST['payment_type'] ],
						'receiptCCNumber'       => $_payment['no_credit_card'],
						'receiptCCExpiryDate'   => $_payment['dt_expiry_month'] . '/' . $_payment['dt_expiry_year'],
						'receiptCCSecurityCode' => $_payment['no_cvn'],
						'receiptDisplayAmout'   => 0,
						'receiptAmout'          => 0,
						'receiptDepositedAmout' => $_payment['payment_amount'],
						'receiptRefundAmout'    => 0
					));
					break;
				case 'E-NETT':
					$receipt = $this->bookinglib->buildENParameters(array(
						'reference'    => $_POST['payment']['trasaction'],
						'description'  => $_POST['payment']['trasaction'].' - eNett ('.TZ_agency_code_map($this->session->userdata('agencycode')).') '.$_payment['consultant_name'],
						'amount'       => $_payment['payment_amount'], 
						'receipt_date' => date('d/m/Y', strtotime('now')),
					));
					break;
				default:
					# code...
					break;
			}

			// response( array('success' => FALSE, 'message' => 'TEST', 'debug' => json_decode($receipt['receiptDatas'], TRUE)), 403 );
			if (!empty($receipt)) {
				$receipted = $this->bookinglib->executeReceipt( $receipt );

				if ( ! isset( $receipted['status'] ) || ! $receipted['status'] )
				{
					$message='There was an error while processing receipt';
					$this->bookinglib->failed($message);
					// response( array( 'success' => FALSE, 'message' => 'There was an error while processing receipt', 'debug' => $receipted ), 403 );
					response( array( 'success' => FALSE, 'message' => $message ), 403 );
				} // end of if statement

				$receiptAllocations = $this->bookinglib->buildAllocationFields( $receipted['receipt_id'], $_payment['payment_amount'] );

				$allocatedReceipt = $this->bookinglib->executeReceiptAllocation( $receiptAllocations );

				if ( ! isset( $allocatedReceipt['status'] ) || ! $allocatedReceipt['status'] )
				{
					$message='There was an error while allocating receipt';
					$this->bookinglib->failed($message);
					// response( array( 'success' => FALSE, 'message' => 'There was an error while allocating receipt', 'debug' => $allocatedReceipt ), 403 );
					response( array( 'success' => FALSE, 'message' => 'There was an error while allocating receipt'), 403 );
				} // end of if statement

				$_IS_RECEIPTED = TRUE;
				// $this->bookinglib->updateItems( $receipted['receipt_id'], (filter_var( @$_POST['includeFlight'], FILTER_VALIDATE_BOOLEAN )?0:1) );
			}
		} // end of if statement

		// END OF PAYMENT BLOCK

		$this->bookinglib->executeItirenary(  );
		$this->bookinglib->executeVoucher(  );
		$_PAYMENT_TYPES_TEXT = json_decode(PAYMENT_TYPES_TEXT, TRUE);

		$emailData = array(
			#'paidAmount'  => $_payment['payment_amount'],
			'emailTo'  => ( EMAIL_IS_TEST?EMAIL_TEST: $_voucher['email'] ),
			'content'  => ( $_IS_PAYABLE?(( $_POST['payment_type'] == $_PAYMENT_TYPES['EN'] )?'template/payment/e-nett':'template/payment/credit-card'): 'template/payment/booked' ),
			'bookType' => 'External Tours',
			'voucher'  => array(
				'name'  => $_voucher['firstname'] . ' ' . $_voucher['lastname'],
				'email' => $_voucher['email']
			),
			'payment' => array(),
			'pax'     => array(
				'adults' => count( $_passengers ),
				#'childs' => ( isset( $_passengers['childs'] )? count( $_passengers['childs'] ): 0 )
			),
			'data' => array(
				'voucherName'   => $_voucher['firstname'],
				'paidAmount'    => '',
				'payment_type'  => $_PAYMENT_TYPES_TEXT[ $_POST['payment_type'] ],
				'isBooked'      => TRUE,
				'startDate'     => ( $remaining_days < 67 )? date('M d, Y l', strtotime( $_item_tour['start'] . ' -15 day' )): date('M d, Y l', strtotime( 'now +7 day' )),
				'isFullPayment' => ( $remaining_days < 67 ),
				'isPayable'     => $_IS_PAYABLE,
				'costing'       => $_final_costing,
				'isAgent'       => is_array( $this->nativesession->get('ETLOGIN') ),
				'agencyCode'    => $this->nativesession->get('agencyRealCode'),
				'PAYMENT_TYPE'  => $_POST['payment_type'],
				'TODAYS_DATE'   => date('Y-m-d H:i:s', strtotime('now')),
				'agencyname'    => '',
				'productType'   => 'Tour',
				#'isFullyPaid' => FALSE, // For hotel only
			)
		);

		$_AGENCYNAMES = json_decode(AGENCY_CODENAMES, TRUE);
		if ( $emailData['data']['isAgent'] && @$_AGENCYNAMES[ $this->nativesession->get('agencyRealCode') ] )
		{
			# Code here
#response(array('SUCCESS' => FALSE, 'MESSAGE' => ''), 403);
			$emailData['data']['agencyname'] = $_AGENCYNAMES[ $this->nativesession->get('agencyRealCode') ];
		} // end of if statement

		if ( $_IS_PAYABLE )
		{
			# Code here
			$emailData['payment']['type']      = $_PAYMENT_TYPES_TEXT[ $_POST['payment_type'] ];
			$emailData['payment']['paid']      = $_payment['payment_amount'];
			$emailData['payment']['receiptId'] = (int)$receipted['receipt_id'];
			
			$emailData['data']['paidAmount']   = $emailData['payment']['paid'];
		} // end of if statement

		if ( $_HAS_FLIGHT )
		{
			$emailData['data']['returnFlight'] = $_flight;
			$_payment['returnFlightFee']       = $_flight['subtotal'];

			$_flight_info = $_flight;
			$_flight_info['VOUCHER'] = $_voucher;
			$this->bookinglib->sendFlighInfo( $_flight_info );
		}

		switch ( $_POST['payment_type'] )
		{
			case 'CREDITCARD':
				# SEND EMAIL
				if ( $_IS_PAYABLE )
				{
					# Code here.
					$_payment_data = $_payment;
					$_payment_data['REF']=TZ_agency_code_map($this->session->userdata('agencycode')) .'-'. $_bookingId;
					$_payment_data['VOUCHER'] = $_voucher;
					if ( isset( $emailData['data']['returnFlight'] ) )
					{
						# Code here
						$_payment_data['returnFlight'] = $emailData['data']['returnFlight'];
					} // end of if statement
					$this->bookinglib->sendCreditCardInfo( $_payment_data, $emailData['data']['isAgent'] );
				} // end of if statement
				break;

			case 'BANKDEPOSIT':
				$emailData['payment']['type'] = $_PAYMENT_TYPES_TEXT[ $_POST['payment_type'] ];
				break;
			
			default:
				# code...
				break;
		}
		
		$this->bookinglib->sendVoucher($emailData, $_IS_RECEIPTED, ($this->nativesession->get('agencyRealCode')=='EET'));
		#$this->bookinglib->sendVoucher($emailData, (filter_var($_POST['isPayable'], FILTER_VALIDATE_BOOLEAN) && $_POST['payment_type'] != $_PAYMENT_TYPES['CC']));
		generate_token( 'externalToursToken' );

		/*=============================================
		=            SAVE BOOK INFORMATION            =
		=============================================*/

		$data = array();

		$data['BOOKING_ID']    = $this->bookinglib->getBookingId();
		$data['ITEM_ID']       = $this->bookinglib->getItemIds();
		$data['DATE_CREATED']  = new MongoDate($DATE_CREATED);
		$data['PAYMENT_TYPE']  = $_POST['payment_type'];
		
		$data['product']       = $_item_product;
		$data['startdate']     = new MongoDate( strtotime($_item_tour['start']) );
		$data['enddate']       = new MongoDate( strtotime($_item_tour['end']) );
		$data['startTime']     = $item['itemStartTime'];
		$data['endTime']       = $item['itemEndTime'];
		$data['duration']      = $_item_tour['duration'];
		
		$data['IS_RECEIPTED']  = $_IS_RECEIPTED;
		$data['IS_AGENT']      = is_array( @$this->nativesession->get('ETLOGIN') );
		$data['agentName']     = $emailData['data']['agencyname'];
		
		$data['IS_DISCOUNTED'] = $_IS_DISCOUNTED;
		$data['costing']       =$_final_costing;
		$data['discount']      = array();
		$data['total']         = $_POST['total'];
		
		$data['IS_PAYABLE']    = $_IS_PAYABLE;
		$data['payment']       = array();
		
		$data['HAS_FLIGHT']    = $_HAS_FLIGHT;
		$data['flight']        = array();
		
		$_payment              = isset($_POST['payment'])?$_POST['payment']:array();
		$data['payment']       = array();

		if ( $data['IS_PAYABLE'] )
		{
			# Code here
			$data['payment']['reference']=(isset($_payment['reference'])?$_payment['reference']:'');
			$data['payment']['amount']   = (@$_payment['amount'])?$_payment['amount']:$_payment['payment_amount'];
			$data['payment']['discount'] = intval(@$_payment['discount']);
			$data['payment']['type']     = $_POST['payment_type'];
		} // end of if statement

		if ( $data['IS_DISCOUNTED'] )
		{
			# Code here
			$_DISCOUNTS       = json_decode(DISCOUNTS, TRUE);
			$data['discount'] = array(
				'code'    => $_POST['PROMOCODE'],
				'details' => $_DISCOUNTS[ $_POST['PROMOCODE'] ],
				'amount'  => @$_payment['discount']
			);
		} // end of if statement
		if ( $data['HAS_FLIGHT'] )
		{
			# Code here
			$data['flight'] = $_POST['flight'];
			$data['flight']['departureDate'] = str_replace('/', '-', $data['flight']['departure']);
			$data['flight']['returnDate']    = str_replace('/', '-', $data['flight']['return']);
		} // end of if statement

		$data['passengers']    = $_POST['passengers'];
		$data['voucher']       = $_POST['voucher'];
		$data['costing']       = $_POST['costings'];
		$data['previous_link'] = $_POST['previous_link'];


		$data['items'] = $_POST['items'];
		#$data['products']    = array_values($_POST['item']['products']);
		#$data['extraBills']  = array_values($_POST['item']['extraBills']);
		#$data['packages']    = array_values($_POST['item']['packages']);
		#$data['suppliments'] = array_values($_POST['item']['suppliments']);

		$data['policy'] = $item['itemCancellation'];

		$this->bookinglib->saveBooking( 'EXTERNAL_TOURS', $data);
		
		/*=====  End of SAVE BOOK INFORMATION  ======*/
		if ( isset( $_GET['payment'] ) && filter_var($_GET['payment'], FILTER_VALIDATE_BOOLEAN) )
		{
			# Code here
			$this->load->model('booking_data');
			switch ( trim($_GET['type']) ) {
				case 'paypal':
					$_POST = $this->booking_data->get( trim($this->nativesession->get('paypal_reference')), trim($_GET['name']) );
					?>
<p>If not redirected automatically in a few seconds. <a href="<?=base_url()?>externaltours/review?book=<?=$this->bookinglib->getBookingId() ?>&item=<?=$this->bookinglib->getItemIds() ?>">Click here</a></p>
					<?php
					redirect('externaltours/review?book=' . $this->bookinglib->getBookingId() . '&item=' . $this->bookinglib->getItemIds());
					break;
				
				default:
					# code...
					break;
			}
			exit();
		} // end of if statement
		log_message('debug', 'Version 1 booking ID '.$this->bookinglib->getBookingId());
		response(array(
			'success'   => TRUE,
			'bookingId' => $this->bookinglib->getBookingId(),
			'itemId'    => $this->bookinglib->getItemIds()
		));
	} // end of createBooking
}


