<?php
	class Agency extends CI_Controller {
   

	    public function __construct()
	    {
	        parent::__construct();
	        $this->load->model('agency_model');
	    }   



	    ############## PAGES VIES ####################
		public function index(){

			$this->load_view('register_agency');

		}

		

		public function load_view($viewFilename , $data = array()){


    		echo $this->load->view('titlehead'); 
			echo $this->load->view('header'); 
			echo $this->load->view($viewFilename, $data);
			echo $this->load->view('footer');
	    
		}
		public function signup(){


			$insertData = array(
					'agency_name'  => $this->input->post('agency_name'),
					'agency_code'  => $this->input->post('agency_code'),
					'is_active'    => 0,
					'address'      => $this->input->post('address'),
					'city'         => $this->input->post('city'),
					'state'        => $this->input->post('state'),
					'country'      => $this->input->post('country'),
					'currency'     => $this->input->post('currency'),
					'email'        => $this->input->post('email'),
					'phone'        => $this->input->post('phone'),
					'fax'          => $this->input->post('fax'),
					'b_first_name' => $this->input->post('b_first_name'),
					'b_last_name'  => $this->input->post('b_last_name'),
					'b_address'    => $this->input->post('b_address'),
					'b_city'       => $this->input->post('b_city'),
					'b_state'      => $this->input->post('b_state'),
					'b_country'    => $this->input->post('b_country'),
					'b_email'      => $this->input->post('b_email'),
					'b_phone'      => $this->input->post('b_phone'),
					'b_fax'        => $this->input->post('b_fax')
				);

				$result = $this->agency_model->checkDatabase($this->input->post('agency_name'));

				if($result){

					$this->session->set_flashdata('signupError', 
						'<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
		 				<a href="#" class="close" data-dismiss="alert">×</a>
		 				Agency already exists!
		 				</div>
		 				');

					redirect('agency');
				}else{

				$success = $this->agency_model->signup($insertData);

					$this->session->set_flashdata('signupSuccess', 
						'<div id="alert-message" class="alert alert-success" style="border: 0; background-color:#03A9F4;color:rgba(255, 255, 255, 0.843137)">
		 				<a href="#" class="close" data-dismiss="alert">×</a>
		 				Congratulations! Agency is now registered! 
		 				</div>
		 				');

					redirect('agency');

				}	
		}






	}
?>