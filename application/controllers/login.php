<?php
	class Login extends CI_Controller {

		public $View;

		public function __construct()
	    {
	        parent::__construct();
	        $this->load->model('login_model');


	    }  

		public function index(){

			if($this->session->userdata('logged_in') == 0) { //Session does not exists

				$this->load_view('login');

			}else if($this->session->userdata('logged_in') == 1){  // Session exists
				redirect('dashboard');
			}else{
				$viewFilename = "login";
				$this->load_view('login');
			}



		}

		public function load_view($viewFilename , $data = array()){


    		echo $this->load->view('titlehead'); 
			echo $this->load->view('header'); 
			echo $this->load->view($viewFilename, $data);
			echo $this->load->view('footer');
	    
		}

		public function verifylogin(){

		   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		 
		   if($this->form_validation->run() == FALSE)
		   {
		     	//Field validation failed.  User redirected to login page
     	        $this->form_validation->set_message('check_database', 'Invalid username or password');
				$this->load_view('login'); 
		   }
		   else
		   {

		   	   //Field validation succeeded.  Validate against database
			   $username = $this->input->post('username');
			   $password = $this->input->post('password');
			 
			   //query the database
			   $result = $this->login_model->checkDatabase($username, $password);


			   if($result){


				 foreach ($result as $key => $row) {

				 	$sess_array = array(
			         'user_id' => $row['_id'],
			         'username' => $row['username'],
			         'logged_in' => 1
			      	 );
			        $this->session->set_userdata($sess_array);
				 	redirect('dashboard');

				 }

			 	}else{
			 		//Display error message to Login page
		 			$this->session->set_flashdata('loginError', 
		 				'<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
		 				<a href="#" class="close" data-dismiss="alert">×</a>
		 				Invalid Username or Password! 
		 				</div>
		 				<style>
		 				#username, #password{
		 				    box-shadow: 0 0 5px red;
    						border: 1px solid red;
    					}
		 				</style>
		 				');	

			 		 $sess_array = array(
			         'logged_in' => 0
			      	 );
			        $this->session->set_userdata($sess_array);
					redirect('login');


			 	}
				


		   
		    }
		}

		public function logout(){

			$this->session->sess_destroy();
			redirect('');
		}

	}
?>