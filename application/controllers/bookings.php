<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_id() == '') {
	session_start();
}

class Bookings extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
         $this->load->model('booking_model');
    }  


    ############ PAGES VIEWS ##################
	public function index()
	{
		
		if($this->session->userdata('logged_in') != 1){
			header("Location: ".base_url()."");
		}

		$agencyCode = $this->session->userdata('agencycode');
		
		$res = $this->booking_model->getBookingDetails($agencyCode);
		//print_r($res);
		//$session_data = $this->session->userdata('username');



		die();

		$this->load_view('bookings/body',$data ); 
		
		//$this->load->view('dashboard/index2');  
	}
	public function load_view($viewFilename, $data = array()){


	echo $this->load->view('titlehead'); 
	echo $this->load->view('dashboard/header'); 
	echo $this->load->view($viewFilename, $data);
	echo $this->load->view('footer');
    
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */