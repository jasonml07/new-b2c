<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// set_time_limit ( 0 );
class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function info (  )
	{
		# Code here
		#phpinfo();
		$data = array('isPaid' => TRUE);
		var_dump(((@$data['isPaid'])?'TRUE':'FALSE'));
	} // end of info

	public function setTestAgency ( $agencycode ) {
		echo $agencycode;
		
		$connection = new MongoClient();
		$db = $connection->db_agency;

		$result = $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($agencycode)),array("_id"=>0));
		$this->session->set_userdata('agencycode', $agencycode);
		$this->session->set_userdata('result', $result);
		$connection->close();
	}

	public function testEmail () {
		print_r($_GET);
		$from    = $_GET['emailFrom'];
		$to      = $_GET['emailTo'];
		$subject = $_GET['subject'];
		$body    = '<img src="https://tours.europeholidays.com.au/assets/images/header-logo.png" alt=""> <b>TESTING</b>';
		// $body    = $this->load->view('forgot-password/template-verification-code', array(), TRUE);

		generate_DKIM( $to, $from, $subject, $body );

		$emailConfig = array(
			/*'mailtype'  => 'html',
		    'charset'   => 'iso-8859-1',
		    'wordwrap'  => TRUE,
			'protocol'  => 'sendmail',
			'smtp_host' => 'localhost',
		 	'smtp_port' => 25,*/

		 	#'protocol'  => 'smtp',
			#'smtp_host' => 'bugatti.websitewelcome.com',
		    #'smtp_user' => 'reservation@europeholidays.com.au', // change it to yours
		    #'smtp_pass' => 'reservation!2', // change it to yours
		    #'smtp_port' => 465
		    'mailtype'  => 'html',
		    'protocol'  => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
		    'smtp_user' => 'bloodsuckermarch61994@gmail.com', // change it to yours
		    'smtp_pass' => 'losewinniwesol123.', // change it to yours
		    'smtp_port' => 465
		);
		$this->load->library('email', $emailConfig);
		// $this->email->addEmbeddedImage('assets/images/header-logo.png','header-logo');
		$this->email->set_newline("\r\n");
    $this->email->set_mailtype("html");

    $this->email->from( $from );
    $this->email->to( $to );

    $this->email->subject( $subject );
    $this->email->message( $body );

    $this->email->send();

    print_r( $this->email->print_debugger() );
	}

	public function prototypingRequest (  )
	{
		ini_set('max_execution_time', 0);
		# Code here
		$destinationDATA = array(
			'start_date' => '2017-02-23',
			'end_date'   => '2017-02-24',
			'location'   => array(
				'name'         => ' Paris  France ',
				'id'           => 4591,
				'city'         => 'Paris',
				'country_code' => 'FR',
			),
			'rooms' => array(
				array(
					'adult' => 2,
					'child' => 0,
				)
			)
		);


		$this->nativesession->set( SYSTEM_TYPE . 'search_time', get_next_id('log_model'));
		$this->load->model('xml_model', 'XM');
		$this->load->model('log_model', 'LM');

		$response = $this->XM->destinationRooms( $destinationDATA, ( @$_GET['timeout']?intval($_GET['timeout']) :50 ) );
		$response = wrapp_xml( $response );
		$xml = xml_to_array( $response );

		if ( @$xml['response'][0]['success'] && $xml['response'][0]['success'] == 1 )
		{
			# Code here
			echo 'Polling hotels start log files ' . $this->nativesession->get( SYSTEM_TYPE . 'search_time' ) . PHP_EOL;
			$this->runtime->start();

			$session  = $xml['response'][0]['session'];

			while ( TRUE  )
			{
				# code...
				$response = $this->XM->pollInitiate( $session );
				$response = wrapp_xml( $response );
				$response = xml_to_array( $response );

				if ( !@$response['response'][0]['content']['job'][0]['status'] )
				{
					# Code here
					echo 'Job node is no where to found!!! ', $this->nativesession->get( SYSTEM_TYPE . 'search_time' ) . PHP_EOL;
					print_r($xml);
					exit;
				} // end of if statement

				#echo 'Do it again STATUS => ', $response['response'][0]['content']['job'][0]['status'], PHP_EOL;
				#exit;
				if ( $response['response'][0]['content']['job'][0]['status'] === 'done' )
				{
					# Code here
					break;
				} // end of if statement
			}

			echo 'Polling hotels end at ', $this->runtime->result(), ' ms' . PHP_EOL;
			exit;
		} // end of if statement
		echo 'Something went wrong!!! ', $this->nativesession->get( SYSTEM_TYPE . 'search_time' );
		print_r($xml);
	} // end of prototypingRequest
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */