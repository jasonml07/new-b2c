<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	
    public function __construct()
    {
        parent::__construct();
    }  
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('titlehead');
		$this->load->view('header');
		$this->load->view('login'); 
		$this->load->view('footer');
	}
	public function login()
	{

		$connection = new MongoClient();
		$db = $connection->db_system;
		
		#echo md5($_POST['password']);
		$whereData = array("username"=>$_POST['username'],"password"=>$_POST['password'],"is_active"=>(int)1);
		$Result    = $db->agency_consultants->findOne($whereData);
		
		$cResult   = count($Result);

		echo $cResult ;
		die();

		$whereData = array("agency_code"=>$Result['agency_code'],"is_active"=>(int)1);
		$Result2   = $db->agency->findOne($whereData);
		$cResult2  = count($Result2);
		if($cResult>0 && $cResult2>0 ){
			$_SESSION['is_logged_B2B'] = true;
			
			$_SESSION['sys_consultant_code_B2B'] = $Result['code'];
			$_SESSION['sys_consultant_name_B2B'] = $Result['fname']." ".$Result['lname'];
			
			$_SESSION['sys_agency_code_B2B']     = $Result2['agency_code'];
			$_SESSION['sys_agency_name_B2B']     = $Result2['agency_name'];
			$_SESSION['sys_agency_id_B2B']       = (string)$Result2['_id'];
			$connection->close();
			header("Location: ".base_url()."dashboard");
			//$response = array ( "success" => true);
		}else{
			$_SESSION['is_logged_B2B'] = false;
			$connection->close();
			header("Location: ".base_url()."");
			//$response = array ( "failure" => true);
		}
	  		
		//print json_encode($response);
	   		exit;
		print_r($_POST);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */