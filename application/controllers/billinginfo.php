<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(session_id() == '') {
	session_start();
}
class Billinginfo extends CI_Controller {

	
	public function __construct()
    {
        parent::__construct();
    }  

    ############ PAGES VIEWS ###########
	public function index()
	{
		
		$data  = array();
		
		$this->load->view('titlehead'); 
		$this->load->view('header'); 
		$this->load->view('checkout-customer/billinginfo'); 
		$this->load->view('footer');
  
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */