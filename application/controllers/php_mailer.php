<?php

	
	class Php_mailer extends CI_Controller{

		public function __construct()
	    {
	        parent::__construct();
			
		}   

		public function index(){


		}

		public function sendMessage (  )
		{
			# Code here
			//set validation rules
	        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|callback_alpha_space_only');
	        $this->form_validation->set_rules('email', 'Emaid ID', 'trim|required|valid_email');
	        $this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
	        $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

	        //run validation on form input
	        if ( $this->form_validation->run() == FALSE )
	        {
	            //validation fails
	            			                //error
	            $this->session->set_flashdata('mailError','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
	 				<a href="#" class="close" data-dismiss="alert">×</a>
	 				Invalid input!
	 				</div>');
	            redirect('hotel');
	        } else {
	        	if($this->input->post('captcha') == 5){
        				// code to send mail
			        	//get the form data
			            $name = $this->input->post('name');
			            $from_email = $this->input->post('email');
			            $subject = $this->input->post('subject');
			            $message = $this->input->post('message');
			        
			            //set to_email id to which you want to receive mails
			            $to_email = (isset($_POST['to']))?$_POST['to']:'info@travelrez.net.au';
             			$cc_email = 'czarbaluran@gmail.com,sally@easterneurotours.com.au,chip@easterneurotours.com.au';
			            //send mail
			            $config = json_decode(EMAIL_CONFIG, TRUE);
			            $this->load->library('email', $config);
			            $this->email->set_newline("\r\n");
        				$this->email->set_mailtype("html");
			            $this->email->from($from_email, $name);
			            $this->email->to($to_email);
			            //$this->email->cc($cc_email); 
			            $this->email->subject($subject);
			            $this->email->message($message);

			            generate_DKIM( $to_email, $from_email, $subject, $message );

			            if ( $this->email->send() )
			            {
			                // mail sent
			                $this->session->set_flashdata('mailSent','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#4CAF50;color:rgba(255, 255, 255, 0.843137)">
		 				<a href="#" class="close" data-dismiss="alert">×</a>
		 				The email has been sent!
		 				</div>');
			                redirect( ((isset($_POST['redirectLink']))?$_POST['redirectLink']:'hotel') );
			            } else {
			                //error
			            $this->session->set_flashdata('mailError','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
		 				<a href="#" class="close" data-dismiss="alert">×</a>
		 				Error in sending mail! Please try again later!
		 				</div>');
			                redirect( ((isset($_POST['redirectLink']))?$_POST['redirectLink']:'hotel') );

			            }
		               #echo $this->email->print_debugger();
        		} else {
			            $this->session->set_flashdata('mailError','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
			 				<a href="#" class="close" data-dismiss="alert">×</a>
			 				Wrong captcha! Please try again!
			 				</div>');
		                redirect( ((isset($_POST['redirectLink']))?$_POST['redirectLink']:'hotel') );
        		}
	        }
		} // end of sendMessage

		public function sendMail(){


		        //set validation rules
		        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|callback_alpha_space_only');
		        $this->form_validation->set_rules('email', 'Emaid ID', 'trim|required|valid_email');
		        $this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
		        $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

		        //run validation on form input
		        if ($this->form_validation->run() == FALSE)
		        {
		            //validation fails
		            			                //error
			            $this->session->set_flashdata('mailError','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
		 				<a href="#" class="close" data-dismiss="alert">×</a>
		 				Invalid input!
		 				</div>');
			            redirect('hotel');
		        }
		        else
		        {
			          
		        		if($this->input->post('captcha') == 5){
		        				// code to send mail
					        	//get the form data
					            $name = $this->input->post('name');
					            $from_email = $this->input->post('email');
					            $subject = $this->input->post('subject');
					            $message = $this->input->post('message');
					        
					            //set to_email id to which you want to receive mails
					            $to_email = (isset($_GET['to']))?$_GET['to']:'info@travelrez.net.au';
		             			$cc_email = 'czarbaluran@gmail.com,sally@easterneurotours.com.au,chip@easterneurotours.com.au';
					            //send mail
					            $config = json_decode(EMAIL_CONFIG, TRUE);
					            $this->load->library('email', $config);
					            $this->email->from($from_email, $name);
					            $this->email->to($to_email);
					            $this->email->cc($cc_email); 
					            $this->email->subject($subject);
					            $this->email->message($message);

					            generate_DKIM( $to_email, $from_email, $subject, $message );

					            if ($this->email->send())
					            {
					                // mail sent
					                $this->session->set_flashdata('mailSent','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#4CAF50;color:rgba(255, 255, 255, 0.843137)">
				 				<a href="#" class="close" data-dismiss="alert">×</a>
				 				The email has been sent!
				 				</div>');
					                #redirect( ((isset($_POST['redirectLink']))?$_POST['redirectLink']:'hotel') );
					            }
					            else
					            {
					                //error
					            $this->session->set_flashdata('mailError','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
				 				<a href="#" class="close" data-dismiss="alert">×</a>
				 				Error in sending mail! Please try again later!
				 				</div>');
					                #redirect( ((isset($_POST['redirectLink']))?$_POST['redirectLink']:'hotel') );

					            }
					               echo $this->email->print_debugger();
		        		}else{
					            $this->session->set_flashdata('mailError','<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
				 				<a href="#" class="close" data-dismiss="alert">×</a>
				 				Wrong captcha! Please try again!
				 				</div>');
					                redirect( ((isset($_POST['redirectLink']))?$_POST['redirectLink']:'hotel') );

		        		}

			            

		        }
   		 }





		
	}
?>