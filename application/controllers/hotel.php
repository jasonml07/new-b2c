<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*if(session_id() == '') {
	session_start();
}
*/
/**
 * 
 */
class Hotel extends CI_Controller {

	
	public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('PaymentWestpac', 'paymentwestpac');
        $this->load->model(array(
        	'xml_model', 'result_model', 'hotel_model', 'agency_model', 'booking_model', 'notification_model', 'log_model'
        ));
    }  


    ########## PAGES VIEWS ###############
	public function index( $data = array() )
	{
		/*if( ! $this->session->userdata('agencycode') && APPLICATION_IS_TEST )
		{
		    redirect('error404');
	    }*/
	    $this->nativesession->set('SYSTEM', 'HOTEL');
	    if(!isset($_GET['agencyCode'])){
	    	$agencycode = AGENCY_CODE;
	    }else{
	    	$agencycode = $_GET['agencyCode'];
	    }

	    $errors = $this->nativesession->get('searchErrors');

	    if ( is_array($errors) )
	    {
	    	# Code here
	    	$data['searchErrors'] = $errors;
	    } // end of if statement

	    $this->session->set_userdata('agencycode',$agencycode);
	    $this->nativesession->set('agencyRealCode', $agencycode);

		$connections = new MongoClient();
        $db = $connections->db_agency;
		$this->session->set_userdata('result', $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($agencycode)),array("_id"=>0)));
		if( ! $this->session->userdata('result') )
		{
			redirect('error404');
		}
		$data['agencyData'] = $this->session->userdata('result');
		$whereData['agency_code'] = base64_encode($agencycode);
		$res     = $db->agency_menu_setup->find($whereData,array("_id"=>0));
		$arrMenu = iterator_to_array($res);
		$result  = $this->treeview($arrMenu);

		#get_next_id('notification_login');
		$this->session->set_userdata('loginNotificationReference', $agencycode . '-' . SYSTEM_TYPE);

		$data['menu'] = $result;
		$this->nativesession->set('headermenu', $result);
		$connections->close();


	   /*//die($_GET['agencyCode']);
		if( ! $this->session->userdata('agencycode'))
		{
			$this->session->set_userdata('agencycode',$agencycode);

			$connections = new MongoClient();
	        $db = $connections->db_agency;
			$this->session->set_userdata('result', $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($agencycode)),array("_id"=>0)));
			if( ! $this->session->userdata('result') )
			{
				redirect('error404');
			}
			$data['agencyData'] = $this->session->userdata('result');
			$whereData['agency_code'] = base64_encode($agencycode);
			$res     = $db->agency_menu_setup->find($whereData,array("_id"=>0));
			$arrMenu = iterator_to_array($res);
			$result  = $this->treeview($arrMenu);

			$data['menu'] = $result;
			$this->nativesession->set('headermenu', $result);
			$connections->close();
		} else {
			if($agencycode != $this->session->userdata('agencycode'))
			{
				$this->session->unset_userdata('agencycode');
				$this->session->unset_userdata('customerData');
				$this->session->set_userdata('agencycode',$agencycode);
				$agencycode = $this->session->userdata('agencycode');
				
			}
			


			$agencycode = $this->session->userdata('agencycode');
			$this->session->set_userdata('agencycode',$agencycode);

			$connections = new MongoClient();
	        $db = $connections->db_agency;
			$this->session->set_userdata('result', $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($agencycode)),array("_id"=>0)));
			if( ! $this->session->userdata('result') )
			{
				show_404();
			}
			$whereData['agency_code'] = base64_encode($agencycode);
			$res                = $db->agency_menu_setup->find($whereData,array("_id"=>0));
			$arrMenu            = iterator_to_array($res);
			$result             = $this->treeview($arrMenu);
			$data['menu']       = $result;
			$data['agencyData'] = $this->session->userdata('result');
			$this->nativesession->set('headermenu', $result);
		}*/
		$this->nativesession->set('site_logo', 'https://ci3.googleusercontent.com/proxy/-X9kqeUrkd3cjgGzb7XMHvT9PaYwsLe5T9Doyk7CI_EKYWLaQ1xgABA7l7qFFHu1_KkZp2sxSFyyFNXQAqItEkp4is3QfHAP6k0_Sg=s0-d-e1-ft#http://travelrez.net.au/assets/img/TravelRez-logo3.png');
		$pageSetup = $this->session->userdata('result');

		if( isset($pageSetup['img_url']) && ! empty($pageSetup['img_url']) )
		{
			$image = @getimagesize(B2B_IP . "uploads/". $pageSetup['img_url']);
			if( is_array($image) )
			{
				$this->nativesession->set('site_logo', B2B_IP . 'uploads/' . $pageSetup['img_url']);
			}
		}

		$connection = new MongoClient();
		$db         = $connection->db_system;
		#$agency     = $db->agency->findOne(array('agency_code' => $this->session->userdata('agencycode')), array('_id' => 0));
		$agency     = _AGENT_INFO( $this->session->userdata('agencycode') );
		$this->nativesession->set('agency_name' , @$agency['agency_name']);
		$this->nativesession->set('agency_email', @$agency['email']);
		$connection->close();

#echo '<script>console.log(' . json_encode($agency) . ')</script>';
		$this->load_view('dashboard/body', $data, TRUE, array('app/hotel/app-search.js'));
	}


	public function treeview($array, $id = 0)
	{
        $return = array();
        for ( $i = 0; $i < count($array); $i++ )
        {

            if( $array[$i]['parent_id']==$id )
            {
               $return[] = array(
                    'val' => $array[$i],
                    'sub_arr' => $this->treeview($array, $array[$i]['id'])
                );

            }
        }
        return empty($return) ? null : $return;
    }


    public function session()
    {
    	if ($this->uri->segment(3) === FALSE)
		{
		    redirect('error404');
		}
		else
		{
		    $agencycode = $this->uri->segment(3);
				$this->session->set_userdata('agencycode',$agencycode);

				$connections = new MongoClient();
		        $db = $connections->db_agency;
				$this->session->set_userdata('result', $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($agencycode)),array("_id"=>0)));
				if(!$this->session->userdata('result'))
				{
					redirect('error404');
				}
				$data['agencyData'] = $this->session->userdata('result');
				$whereData['agency_code'] = base64_encode($agencycode);
				$res          = $db->agency_menu_setup->find($whereData,array("_id"=>0));
				$arrMenu      = iterator_to_array($res);
				$result       = $this->treeview($arrMenu);
				$data['menu'] = $result;
				redirect("");
		}
    }


	public function createAccount()
	{
		$this->load_view('hotel/create_account',$this->session->userdata('result'));
	}

	public function usernameChecker()
	{
		$connections = new MongoClient();
		$db     = $connections->db_agency;
		$res    = $_REQUEST['username'];
		$result = $db->agency_users_credentials->findOne(array("user_name"=>$res,"agency_code"=>base64_encode($this->session->userdata('agencycode'))),array("_id"=>0));
        if(!$result)
        {
        	echo(json_encode(true));
        }else{
        	echo(json_encode(false));
        }
	}


	public function emailChecker()
	{
		$connections = new MongoClient();
		$db          = $connections->db_agency;
		$res         = $_REQUEST['email'];
		$result      = $db->agency_users_credentials->findOne(array("user_email"=>$res,"agency_code"=>base64_encode($this->session->userdata('agencycode'))),array("_id"=>0));
        if(!$result)
        {
        	echo(json_encode(true));
        }else{
        	echo(json_encode(false));
        }
	}


	public function saveAccount()
	{

		$connections = new MongoClient();
        $db = $connections->db_agency;

        $id = get_next_id('booking');

        $whereData = array(
        	"agency_code" 		=> base64_encode($this->session->userdata('agencycode')),
        	"user_name"			=> $_POST['username'],
        	"user_email"		=> $_POST['email'],
        	"user_password" 	=> md5($_POST['password_confirm']),
        	"user_id"			=> $id,
    		"firstname"			=> "",
    		"middlename"		=> "",
    		"lastname"			=> "",
    		"birthday"			=> "",
    		"gender"			=> "",
    		"contact"			=> "",
    		"cityAdd"			=> "",
    		"country"			=> "",
    		"zip"				=> "",
    		"profile_picture"	=> "",
        	"is_active"			=> 1
        	);
        $db->agency_users_credentials->insert($whereData);
        $dataRes = array('success'=>true);

        $resultData = $db->agency_users_credentials->findOne(array("user_name"=>$whereData['user_name'],"user_password"=>$whereData['user_password'],"agency_code"=>$whereData['agency_code']),array("_id"=>0));
        $session_data = array(
				"user_id"    => $resultData['user_id'],
				"user_email" => $resultData['user_email'],
				"is_login"   => 1
        	);
        $this->session->set_userdata('customerData', $session_data);
        print json_encode($dataRes);
	}


	public function customerLogin()
	{
		$connections = new MongoClient();
        $db = $connections->db_agency;

        if(filter_var($_POST['username'], FILTER_VALIDATE_EMAIL))
        {
			$data = array(
				"agency_code"   => base64_encode($_POST['agencyCode']),
				"user_email"    => $_POST['username'],
				"user_password" => md5($_POST['password']),
				"is_active"     =>(int)1
			);
			$res = $db->agency_users_credentials->findOne($data,array("_id"=>0));
			if(!$res)
			{
				$this->load_view('hotel/login_failed',$this->session->userdata('agencycode'));
			}
			else
			{
				$session_data = array(
					"user_id" 		=> $res['user_id'],
		        	"user_email"	=> $res['user_email'],
		        	"is_login"		=> 1
					);
				$this->session->set_userdata('customerData', $session_data);
				redirect('/','refresh');
			}
    	} else {
	        $data = array(
				"agency_code"   => base64_encode($_POST['agencyCode']),
				"user_name"     => $_POST['username'],
				"user_password" => md5($_POST['password']),
				"is_active"     =>(int)1
			);
			$res = $db->agency_users_credentials->findOne($data,array("_id"=>0));
			if(!$res)
			{
				$this->load_view('hotel/login_failed',$this->session->userdata('agencycode'));
				//print_r("not");	
			} else {
				$session_data = array(
						"user_id"    => $res['user_id'],
						"user_email" => $res['user_email'],
						"is_login"   => 1
					);
				$this->session->set_userdata('customerData', $session_data);
				redirect('/','refresh');
			}
		}
	}


	public function accountChecker()
	{
		$connections = new MongoClient();
		$db     = $connections->db_agency;
		$res    = $_POST['res'];
		$userId = $this->session->userdata['customerData']['user_id'];
		$result = $db->agency_users_credentials->findOne(array("user_name"=>$res,"user_id"=>$userId,"agency_code"=>base64_encode($this->session->userdata('agencycode'))),array("_id"=>0));
        if(!$result)
        {
        	echo(json_encode(false));
        } else {
        	echo(json_encode(true));
        }
	}


	public function passwordChecker()
	{
		$connections = new MongoClient();
        $db     = $connections->db_agency;
        $res    = md5($_POST['pass']);
        $userId = $this->session->userdata['customerData']['user_id'];
        $result = $db->agency_users_credentials->findOne(array("user_password"=>$res,"user_id"=>$userId,"agency_code"=>base64_encode($this->session->userdata('agencycode'))),array("_id"=>0));
        if(!$result)
        {
        	echo(json_encode(false));
        } else {
        	echo(json_encode(true));
        }
	}


	public function saveCustomerAccount()
	{
		$username = $_POST['Nuname'];
		$password = $_POST['Cpass'];

		if(!empty($username) && empty($password))
		{
			$connections  = new MongoClient();
	        $db           = $connections->db_agency;
	        $agencycode   = $this->session->userdata('agencycode');
	        $dataToInsert = array(
					"user_name" => $username
	        	);
			$whereData                = array(); 
			$whereData['agency_code'] = base64_encode($this->session->userdata("agencycode"));
			$whereData['user_id']     = $this->session->userdata['customerData']['user_id'];
		    $db->agency_users_credentials->update($whereData,array("\$set"=>$dataToInsert),array("upsert"=>true));
		} else if($username != "" && $password != "") {
			$connections  = new MongoClient();
	        $db           = $connections->db_agency;
	        $agencycode   = $this->session->userdata('agencycode');
	        $dataToInsert = array(
					"user_name"     => $username,
					"user_password" => md5($password)
	        	);
	        $whereData                = array(); 
		    $whereData['agency_code'] = base64_encode($this->session->userdata("agencycode"));
		    $whereData['user_id']     = $this->session->userdata['customerData']['user_id'];
		    $db->agency_users_credentials->update($whereData,array("\$set"=>$dataToInsert),array("upsert"=>true));
		} else {
			$connections  = new MongoClient();
	        $db           = $connections->db_agency;
	        $agencycode   = $this->session->userdata('agencycode');
	        $dataToInsert = array(
					"user_password" => md5($password)
	        	);
			$whereData                = array(); 
			$whereData['agency_code'] = base64_encode($this->session->userdata("agencycode"));
			$whereData['user_id']     = $this->session->userdata['customerData']['user_id'];
		    $db->agency_users_credentials->update($whereData,array("\$set"=>$dataToInsert),array("upsert"=>true));
		}
		redirect("hotel/customerInfo","refresh");
	}

	public function logout()
	{
		
		$this->session->unset_userdata('customerData');
	    redirect('/','refresh');
	}


	public function customerInfo()
	{
		$connections              = new MongoClient();
		$db                       = $connections->db_agency;
		$whereData                = array(); 
		$whereData['agency_code'] = base64_encode($this->session->userdata("agencycode"));
		$whereData['user_id']     = $this->session->userdata['customerData']['user_id'];
		$resultArray['data']      = $db->agency_users_credentials->findOne($whereData,array("_id"=>0));
		$this->load_view('hotel/profile_info',$resultArray);
	}


	public function saveCustomerInfo()
	{
		//print_r($_POST);
		//die();
		$connections  = new MongoClient();
		$db           = $connections->db_agency;
		$agencycode   = $this->session->userdata('agencycode');
		$dataToInsert = array(
				"firstname"  => ucfirst($_POST['fname']),
				"middlename" => ucfirst($_POST['mname']),
				"lastname"   => ucfirst($_POST['lname']),
				"birthday"   => $_POST['birth']['month']." ".$_POST['birth']['day'].",".$_POST['birth']['year'],
				"gender"     => $_POST['gender'],
				"contact"    => $_POST['contact'],
				"cityAdd"    => ucfirst($_POST['cityAdd']),
				"country"    => ucfirst($_POST['country']),
				"zip"        => $_POST['zipCode']
        	);
		$whereData                = array(); 
		$whereData['agency_code'] = base64_encode($this->session->userdata("agencycode"));
		$whereData['user_id']     = $this->session->userdata['customerData']['user_id'];
	    $db->agency_users_credentials->update($whereData,array("\$set"=>$dataToInsert),array("upsert"=>true));
	    
	    $resultArray['data'] = $db->agency_users_credentials->findOne($whereData,array("_id"=>0));
        $this->load_view('hotel/profile_info',$resultArray);
	}


	public function uploadProfile($id)
	{
		$connections = new MongoClient();
		$db          = $connections->db_agency;

		$userId                  = $id; 
		$new_name                = time().$userId;
		$config['file_name']     = $new_name;
		$config['upload_path']   = "./uploads";
		$config['allowed_types'] = '*';

        $this->load->library('upload', $config);
 
        if (!$this->upload->do_upload())
        {
            $response = $this->upload->display_errors();
        } else {	
        	
			$response  = $this->upload->data();
			$whereData = array();
			$whereData['user_id'] = (int)$id;
			$dataToInsert         = array(
					"profile_picture" => $config['file_name']
		    	);
		    $db->agency_users_credentials->update($whereData,array("\$set"=>$dataToInsert),array("upsert"=>true));
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
	}


	public function theme ()
	{
		$this->load->view('dashboard/titlehead', array("theme" => $this->uri->segment(2)));
		$this->load->view('dashboard/header'); 
		$this->load->view('dashboard/body');
		$this->load->view('dashboard/footer');
	}


	public function bookings()
	{
		//die('asd');
		 
		//print_r($res);
		if($this->session->userdata('logged_in') != 1)
		{
			header("Location: ".base_url()."");
		}
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');
		$res        = $this->booking_model->getBookingDetails($agencyCode);
		// print_r($res);
		$data       = array("bookings"=>$res);
		$this->load_view('bookings/body',$data ); 
	}

	/**
	 * [load_view description]
	 * @param  [type]  $viewFilename [description]
	 * @param  array   $data         [description]
	 * @param  boolean $hasAngular   [description]
	 * @param  [type]  $angularJS    [description]
	 * @return [type]                [description]
	 */
	public function load_view( $viewFilename,$data=array(), $hasAngular = FALSE, $angularJS = null )
	{
		$this->load->view('dashboard/titlehead',$this->session->userdata('result')); 
		$this->load->view('dashboard/header',$data); 
		$this->load->view($viewFilename, $data);

		$footerData = array();
		$footerData['hasAngular'] = $hasAngular;
		$footerData['angularJSs'] = ((is_string($angularJS))?array($angularJS):((is_array($angularJS))?$angularJS:array()));

		$this->load->view('dashboard/footer', $footerData);

	}


	public function bookingDetails( $bookingID )
	{
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking)
		{
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result                 = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		$data                   = array();

		$data['BookingData']    = $result['bookingData'];
		$data['ItemDatas']      = $result['itemDatas'];
		$data['ReceiptDatas']   = $result['receiptDatas'];
		$data['CostingsDatas']  = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/details',$data); 
	}


	public function itemDetails( $bookingID,$itemId )
	{
		$this->load->model('booking_model');
		$agencyCode  = $this->session->userdata('agencycode');

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking)
		{
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleItemDetails($bookingID,$itemId);
		$data   = array("item"=>$result);
		$this->load_view('bookings/item_details',$data);	
	}


	public function bookingPayments( $bookingID )
	{
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');


		$data = array();

		$data['enetSuccess'] = false;
		if(isset($_POST['submit']))
		{
			if($_POST['type'] == 'enet')
			{
				$checkEnet = $this->booking_model->submitEnetReceipt($agencyCode,(int)$bookingID,$_POST);
				if($checkEnet)
				{
					$data['enetSuccess'] = true;
				}
			}
			if($_POST['type'] == 'bank')
			{
				$_SESSION['paymentData'] = $_POST;
				redirect('dashboard/paymentVerification/'.$bookingID);
			}
			
		}

		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking)
		{
			header("Location: ".base_url()."dashboard/bookings");
		}
		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		$data['BookingData']    = $result['bookingData'];
		$data['ItemDatas']      = $result['itemDatas'];
		$data['ReceiptDatas']   = $result['receiptDatas'];
		$data['CostingsDatas']  = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/payments',$data); 
	}


	public function paymentVerification( $bookingID )
	{
		$this->load->model('booking_model');
		$agencyCode  = $this->session->userdata('agencycode');
		$data        = array();
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking)
		{
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		$data['pay_data']       = $_SESSION['paymentData'];
		$data['BookingData']    = $result['bookingData'];
		$data['ItemDatas']      = $result['itemDatas'];
		$data['ReceiptDatas']   = $result['receiptDatas'];
		$data['CostingsDatas']  = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/paymentCCverify',$data); 
	}

	/**
	 * [searchHotel description]
	 * @return [type] [description]
	 */
	public function hotel_search (  )
	{
		$_agencyCode=isset($_GET['agencyCode'])?$_GET['agencyCode']:AGENCY_CODE;
		if( !empty($_agencyCode) )
		{
			$connection = new MongoClient();
			$db         = $connection->db_agency;
			
			$result     = $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($_agencyCode)),array("_id"=>0));
			$this->session->set_userdata('agencycode', $_agencyCode);
			$this->session->set_userdata('result', $result);
			$connection->close();
		}

#print_r($_POST);
#die();
		$_POST['searchType'] = 1;
		$this->nativesession->set( SYSTEM_TYPE . 'search_time', get_next_id('log_model'));
		$this->nativesession->set('session_post', $_POST);

		$this->form_validation->set_error_delimiters('', '');

		if( $this->form_validation->run('hotel_SEARCH') == FALSE )
		{
			$errors = array();
			$msg = form_error('location[name]');
			if( ! empty($msg) )
			{
				$errors['location'] = $msg;
			}
			$msg = form_error('location[id]');
			if( ! empty($msg) )
			{
				$errors['location'] = $msg;
			}
			$msg = form_error('start_date');
			if( ! empty($msg) )
			{
				$errors['start_date'] = $msg;
			}
			$msg = form_error('end_date');
			if( ! empty($msg) )
			{
				$errors['end_date'] = $msg;
			}

			$this->nativesession->set('searchErrors', array('hotel' => $errors));
			redirect('hotel');
		} else {
	        $_notif_log = array(
				'description'                => 'Searching',
				'isSearching'                => 1,
				'notificationLoginReference' => $this->session->userdata('loginNotificationReference'),
				'subSystem'                  => 'hotel',
				'xml_reference'              => $this->nativesession->get( SYSTEM_TYPE . 'search_time')
	        );

			$_notif_log['description'] = 'The user is searching for a location on ';
			$_notif_log['description'] .= $_POST['location']['name'] . 'in the city of ' . $_POST['location']['city'];
			$total = $this->totalizeNumberOfAdults( $_POST['rooms'] );

			$_notif_log['description'] .= ' with ' . $total['rooms'] . ' rooms,';
			$_notif_log['description'] .= ' ' . $total['adults'] . ' adults,';
			$_notif_log['description'] .= ' ' . $total['childrens'] . ' childs';
			$_notif_log['description'] .= ' within a date range of ' . $_POST['start_date'] . ' up to ' . $_POST['end_date'];

			$this->notification_model->log( $_notif_log );

			$startDate           = $_POST['start_date'];
			$endDate             = $_POST['end_date'];
			$arr                 = explode("/",  $_POST['start_date']);
			$_POST['start_date'] = $arr[2].'-'.$arr[1].'-'.$arr[0];
			$arr                 = explode("/",  $_POST['end_date']);
			$_POST['end_date']   = $arr[2].'-'.$arr[1].'-'.$arr[0];

			$curl = wrapp_xml( $this->xml_model->destinationRooms( $_POST ) );
			$curl = xml_to_array( $curl );

			if( ! isset($curl['response']) )
			{
				$this->index(array('errorHotel' => array('Failed to search')));
			} else {
				$_POST['end_date']   = $endDate;
				$_POST['start_date'] = $startDate;
				if($curl['response'][0]['success'] == 1)
				{
		        	$this->session->set_userdata('search_sess', $curl['response'][0]['session']);
			        $this->session->set_userdata('cron_session_search_result_status', 'failed');
			        $this->nativesession->set('searchErrors', FALSE);

			        redirect('hotel/result');
		        } else {
		        	$this->nativesession->set('searchErrors', array('hotel' => $curl['response'][0]['content']['error'][0]['content']));
		        	redirect('hotel');
		        }
			}
		}
	}

	/**
	 * [valid_date description]
	 * @param  [type] $str [description]
	 * @return [type]      [description]
	 */
	public function valid_date ( $str )
	{
		$date_regex = '/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/';

		if( preg_match($date_regex, $str) )
		{
			return TRUE;
		}

		$this->form_validation->set_message('valid_date', 'The %s field is not valid date format');
		return FALSE;
	}

	/**
	 * [searchResult description]
	 * @return [type] [description]
	 */
	public function searchResult ()
	{
		$session = $this->session->userdata('search_sess');
#print_r($session);
#die();
		if( ! empty($_POST) )
		{
			$data['request'] = $_POST;
		} else {
			$data['request'] = $this->nativesession->get('session_post');
		}
		if( count($data['request']) > 0 )
		{
			$data['json'] = str_replace("\"", "'", json_encode($data['request']['rooms']));
		}

		$data['token'] = generate_token( 'hotelToken' );

		$this->load_view('hotel/searchhotel', $data, TRUE, 'app/hotel/app-search-result.js');
	}


	public function hotelDetails (  )
	{
		$data = array();

		$hotelInfo   = $this->result_model->getHotel($_GET['id']);
		$search_data = $this->nativesession->get('session_post');

		$xml         = wrapp_xml( $this->xml_model->requestRooms( $this->session->userdata('search_sess'), $_GET['id'] ) );
        $xml         = xml_to_array($xml);

		$xml         = $this->hotel_model->parseRoomsData( $xml );
		if( $xml === FALSE )
		{
			redirect( 'hotel' );
			die();
		}
		$data['token'] = generate_token('hotelToken');
		$data['hotel']         = $hotelInfo;
		$data['rooms']         = $xml;
		$data['numberOfGuest'] = $this->totalizeNumberOfAdults($search_data['rooms']);

		$this->load_view('hotel/searchdetails',$data, TRUE, array('app/hotel/app-details.js') ); 
	}

	/**
	 * [totalizeNumberOfAdults description]
	 * @param  [type] $rooms [description]
	 * @return [type]        [description]
	 */
	private function totalizeNumberOfAdults ( $rooms )
	{
		$adults     = 0;
		$childrens  = 0;
		$countRooms = count($rooms);
		foreach ($rooms as $key => $room)
		{
			$adults    += intval($room['adult']);
			$childrens += intval($room['child']);
		}

		return array('adults' => $adults, 'childrens' => $childrens, 'rooms' => $countRooms);
	}

	/**
	 * BOOKER FORM
	 * @param  array  $data [description]
	 * @return [type]       [description]
	 */
	public function bookerInformation ($data = array())
	{
		// $this->checkifLogin();
		$redirectPage = '. If the page did not redirected <a href="' . base_url() . 'hotel?' . random_string('alnum', 15) . '">Click here.</a>';


		if ( ! isset( $_POST ) || ( isset( $_POST['token'] ) && $_POST['token'] != $this->nativesession->get('hotelToken') ) )
		{
			# Code here
			echo 'Please choose a hotel to book';
			echo $redirectPage;

			d_redirect('hotel/searchResult');
		} // end of if statement

		if( count($_POST) == 0 || !isset($_GET['id']) )
		{
			redirect('hotel');
		}
		if( isset($_GET['id']) )
		{
			// Check if the hotelId exist
			$exist = $this->hotel_model->isExist( $_GET['id'] );
			if( ! $exist )
			{
				echo 'Hotel does not exist';
				echo $redirectPage;
				d_redirect( 'hotel/searchResult' );
			}
		}
		
		$this->session->set_userdata('paymentIsRequired', TRUE);
		$data['country_codes'] = array('AF' => 'Afghanistan','AX' => 'Aland Islands','AL' => 'Albania','DZ' => 'Algeria','AS' => 'American Samoa','AD' => 'Andorra','AO' => 'Angola','AI' => 'Anguilla','AQ' => 'Antarctica','AG' => 'Antigua And Barbuda','AR' => 'Argentina','AM' => 'Armenia','AW' => 'Aruba','AU' => 'Australia','AT' => 'Austria','AZ' => 'Azerbaijan','BS' => 'Bahamas','BH' => 'Bahrain','BD' => 'Bangladesh','BB' => 'Barbados','BY' => 'Belarus','BE' => 'Belgium','BZ' => 'Belize','BJ' => 'Benin','BM' => 'Bermuda','BT' => 'Bhutan','BO' => 'Bolivia','BA' => 'Bosnia And Herzegovina','BW' => 'Botswana','BV' => 'Bouvet Island','BR' => 'Brazil','IO' => 'British Indian Ocean Territory','BN' => 'Brunei Darussalam','BG' => 'Bulgaria','BF' => 'Burkina Faso','BI' => 'Burundi','KH' => 'Cambodia','CM' => 'Cameroon','CA' => 'Canada','CV' => 'Cape Verde','KY' => 'Cayman Islands','CF' => 'Central African Republic','TD' => 'Chad','CL' => 'Chile','CN' => 'China','CX' => 'Christmas Island','CC' => 'Cocos (Keeling) Islands','CO' => 'Colombia','KM' => 'Comoros','CG' => 'Congo','CD' => 'Congo, Democratic Republic','CK' => 'Cook Islands','CR' => 'Costa Rica','CI' => 'Cote D\'Ivoire','HR' => 'Croatia','CU' => 'Cuba','CY' => 'Cyprus','CZ' => 'Czech Republic','DK' => 'Denmark','DJ' => 'Djibouti','DM' => 'Dominica','DO' => 'Dominican Republic','EC' => 'Ecuador','EG' => 'Egypt','SV' => 'El Salvador','GQ' => 'Equatorial Guinea','ER' => 'Eritrea','EE' => 'Estonia','ET' => 'Ethiopia','FK' => 'Falkland Islands (Malvinas)','FO' => 'Faroe Islands','FJ' => 'Fiji','FI' => 'Finland','FR' => 'France','GF' => 'French Guiana','PF' => 'French Polynesia','TF' => 'French Southern Territories','GA' => 'Gabon','GM' => 'Gambia','GE' => 'Georgia','DE' => 'Germany','GH' => 'Ghana','GI' => 'Gibraltar','GR' => 'Greece','GL' => 'Greenland','GD' => 'Grenada','GP' => 'Guadeloupe','GU' => 'Guam','GT' => 'Guatemala','GG' => 'Guernsey','GN' => 'Guinea','GW' => 'Guinea-Bissau','GY' => 'Guyana','HT' => 'Haiti','HM' => 'Heard Island & Mcdonald Islands','VA' => 'Holy See (Vatican City State)','HN' => 'Honduras','HK' => 'Hong Kong','HU' => 'Hungary','IS' => 'Iceland','IN' => 'India','ID' => 'Indonesia','IR' => 'Iran, Islamic Republic Of','IQ' => 'Iraq','IE' => 'Ireland','IM' => 'Isle Of Man','IL' => 'Israel','IT' => 'Italy','JM' => 'Jamaica','JP' => 'Japan','JE' => 'Jersey','JO' => 'Jordan','KZ' => 'Kazakhstan','KE' => 'Kenya','KI' => 'Kiribati','KR' => 'Korea','KW' => 'Kuwait','KG' => 'Kyrgyzstan','LA' => 'Lao People\'s Democratic Republic','LV' => 'Latvia','LB' => 'Lebanon','LS' => 'Lesotho','LR' => 'Liberia','LY' => 'Libyan Arab Jamahiriya','LI' => 'Liechtenstein','LT' => 'Lithuania','LU' => 'Luxembourg','MO' => 'Macao','MK' => 'Macedonia','MG' => 'Madagascar','MW' => 'Malawi','MY' => 'Malaysia','MV' => 'Maldives','ML' => 'Mali','MT' => 'Malta','MH' => 'Marshall Islands','MQ' => 'Martinique','MR' => 'Mauritania','MU' => 'Mauritius','YT' => 'Mayotte','MX' => 'Mexico','FM' => 'Micronesia, Federated States Of','MD' => 'Moldova','MC' => 'Monaco','MN' => 'Mongolia','ME' => 'Montenegro','MS' => 'Montserrat','MA' => 'Morocco','MZ' => 'Mozambique','MM' => 'Myanmar','NA' => 'Namibia','NR' => 'Nauru','NP' => 'Nepal','NL' => 'Netherlands','AN' => 'Netherlands Antilles','NC' => 'New Caledonia','NZ' => 'New Zealand','NI' => 'Nicaragua','NE' => 'Niger','NG' => 'Nigeria','NU' => 'Niue','NF' => 'Norfolk Island','MP' => 'Northern Mariana Islands','NO' => 'Norway','OM' => 'Oman','PK' => 'Pakistan','PW' => 'Palau','PS' => 'Palestinian Territory, Occupied','PA' => 'Panama','PG' => 'Papua New Guinea','PY' => 'Paraguay','PE' => 'Peru','PH' => 'Philippines','PN' => 'Pitcairn','PL' => 'Poland','PT' => 'Portugal','PR' => 'Puerto Rico','QA' => 'Qatar','RE' => 'Reunion','RO' => 'Romania','RU' => 'Russian Federation','RW' => 'Rwanda','BL' => 'Saint Barthelemy','SH' => 'Saint Helena','KN' => 'Saint Kitts And Nevis','LC' => 'Saint Lucia','MF' => 'Saint Martin','PM' => 'Saint Pierre And Miquelon','VC' => 'Saint Vincent And Grenadines','WS' => 'Samoa','SM' => 'San Marino','ST' => 'Sao Tome And Principe','SA' => 'Saudi Arabia','SN' => 'Senegal','RS' => 'Serbia','SC' => 'Seychelles','SL' => 'Sierra Leone','SG' => 'Singapore','SK' => 'Slovakia','SI' => 'Slovenia','SB' => 'Solomon Islands','SO' => 'Somalia','ZA' => 'South Africa','GS' => 'South Georgia And Sandwich Isl.','ES' => 'Spain','LK' => 'Sri Lanka','SD' => 'Sudan','SR' => 'Suriname','SJ' => 'Svalbard And Jan Mayen','SZ' => 'Swaziland','SE' => 'Sweden','CH' => 'Switzerland','SY' => 'Syrian Arab Republic','TW' => 'Taiwan','TJ' => 'Tajikistan','TZ' => 'Tanzania','TH' => 'Thailand','TL' => 'Timor-Leste','TG' => 'Togo','TK' => 'Tokelau','TO' => 'Tonga','TT' => 'Trinidad And Tobago','TN' => 'Tunisia','TR' => 'Turkey','TM' => 'Turkmenistan','TC' => 'Turks And Caicos Islands','TV' => 'Tuvalu','UG' => 'Uganda','UA' => 'Ukraine','AE' => 'United Arab Emirates','GB' => 'United Kingdom','US' => 'United States','UM' => 'United States Outlying Islands','UY' => 'Uruguay','UZ' => 'Uzbekistan','VU' => 'Vanuatu','VE' => 'Venezuela','VN' => 'Viet Nam','VG' => 'Virgin Islands, British','VI' => 'Virgin Islands, U.S.','WF' => 'Wallis And Futuna','EH' => 'Western Sahara','YE' => 'Yemen','ZM' => 'Zambia','ZW' => 'Zimbabwe');
		$data['request']       = $this->nativesession->get('session_post');

		if ( is_null($data['request']) )
		{
			# Code here
			echo 'There was an error occured';
			echo $redirectPage;
			d_redirect( 'hotel/searchResult' );
		} // end of if statement

		if( isset($_POST['selectedRooms']) )
		{
			$post = $this->formatSelectedRooms($_POST['selectedRooms']);
			$this->nativesession->set('session_selected_rooms', $post);
			$data['result_ids'] = $post;
		} else {
			$data['result_ids'] = $this->nativesession->get('session_selected_rooms');
		}
		
		$data['booking_info'] = $this->xml_model->bookingInfo($this->session->userdata('search_sess'), $data['result_ids']);

		if ( ! $data['booking_info'] )
		{
			# Code here
			// $this->log_model->debug('Booking Info boolean ' . get_class($this) . ' in Line 238');
			echo 'Error while fetching result';
			echo $redirectPage;
			d_redirect( 'hotel/searchResult' );
		} // end of if statement

		if( empty($data['booking_info']) )
		{
			echo 'Error while fetching result';
			echo $redirectPage;
			d_redirect( 'hotel/searchResult' );
		}

		$data['booking_info'] = wrapp_xml($data['booking_info']);
		$data['booking_info'] = xml_to_array($data['booking_info']);

		if( isset($data['booking_info']['response'][0]['success']) && $data['booking_info']['response'][0]['success'] == 0 )
		{
			echo $data['booking_info']['response'][0]['content']['error'][0]['content'] . PHP_EOL;
			echo $redirectPage;
			redirect('hotel/searchResult');
		}
		$data['booking_info'] = $this->getCurrency($data['booking_info']);
		?>
		<script type="text/javascript">console.log( <?=json_encode($data) ?> )</script>
		<script type="text/javascript">console.log( <?=json_encode($_POST) ?> )</script>
		<?php
		$this->load_view('hotel/booking-payment', $data, TRUE, array('app/hotel/app-payment.php') );
	}

	/**
	 * [formatSelectedRooms description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function formatSelectedRooms ( $data )
	{

		$clean = array();

		foreach ( $data as $key => $room )
		{
			$size = intval($room['qty']);
			// unset($room['qty']);
			if( $size > 1 )
			{
				for ( $i=0; $i < $size; $i++ )
				{
					array_push($clean, $room);
				}
			} else {
				array_push($clean, $room);
			}
		}

		return $clean;
	}

	/**
	 * This function is only available for booking information and booking validation methods
	 * @param  [type] $bookInfo [description]
	 * @return [type]           [description]
	 */
	private function getCurrency ( $bookInfo )
	{

		$getCurrencyExchanged = $this->hotel_model->getConvertCurrencyEx();

		$allPolicies = array();
		if( ! isset($bookInfo['response'][0]['success']) || $bookInfo['response'][0]['success'] == 0 )
		{
			if ( isset($bookInfo['response'][0]['content']['error']) )
			{
				return FALSE;
			}
		}
		$books = $bookInfo['response'][0]['content']['booking-options'][0]['content']['booking-option'];
		$bookingTotalPrice     = 0;
		$bookingOriginalPrice  = 0;
		$bookingTotalCurrrency = FALSE;
		foreach ( $books as $keyBook => $book )
		{
			$itemPrice            = ( isset($book['content']['room'][0]['price'][0]['content']) )? $book['content']['room'][0]['price'][0]: $book['content']['room'][0]['content']['price'][0];
			$price                = ((float)$itemPrice['content']) * $getCurrencyExchanged[ $itemPrice['currency'] ];
			$calculation          = $this->hotel_model->calculateTotalCost( $price, $itemPrice['currency'], (float)$itemPrice['content'] );
			$bookingTotalPrice    += floatval($price);
			$bookingOriginalPrice += (float)$itemPrice['content'];

			if( ! $bookingTotalCurrrency )
			{
				$bookingTotalCurrrency = $itemPrice['currency'];
			}
			if( isset($book['content']['room'][0]['content']['price'][0]) )
			{
				$book['content']['room'][0]['content']['price'][0]['new_content']  = number_format($price, 2, '.','');
				$book['content']['room'][0]['content']['price'][0]['calculation']  = $calculation;
				$book['content']['room'][0]['content']['price'][0]['new_currency'] = CONVERTION_CURRENCY;
			} else {
				$book['content']['room'][0]['price'][0]['new_content']  = number_format($price, 2, '.','');
				$book['content']['room'][0]['price'][0]['calculation']  = $calculation;
				$book['content']['room'][0]['price'][0]['new_currency'] = CONVERTION_CURRENCY;
			}

			if( isset($book['content']['cancellation'][0]['frame']) )
			{
				$bookingCancellations = $book['content']['cancellation'][0]['frame'];
				$bookingCancellations = $this->booking_model->parseCancellation( $bookingCancellations );

				array_push($allPolicies, array(
					'name'         => ( isset($book['content']['room'][0]['name'][0]) )? $book['content']['room'][0]['name'][0]: $book['content']['room'][0]['content']['name'][0],
					'cancellation' => $bookingCancellations
				));
			} else {
				array_push($allPolicies, array(
					'name'         => ( isset($book['content']['room'][0]['name'][0]) )? $book['content']['room'][0]['name'][0]: $book['content']['room'][0]['content']['name'][0],
					'cancellation' => array()
				));
			}

			$books[$keyBook] = $book;
		}

		$bookInfo['response'][0]['content']['byRoomPolicies']                                  = $allPolicies;
		$bookInfo['response'][0]['content']['total_price']                                     = $bookingTotalPrice;
		$bookInfo['response'][0]['content']['calculation']                                     = $this->hotel_model->calculateTotalCost( $bookingTotalPrice, $bookingTotalCurrrency, $bookingOriginalPrice );
		$bookInfo['response'][0]['content']['booking-options'][0]['content']['booking-option'] = $books;

		return $bookInfo;
	}


	/**
	 * [returnPayWayResponse description]
	 * @return [type] [description]
	 */
	public function returnPayWayResponse (  )
	{
		$this->load->model('booking_data');
		$this->load->library('PaymentWestpac', 'paymentwestpac');
		if ( isset( $_GET['testing'] ) && trim($_GET['testing']) == 'true' )
		{
			# Code here
			$reference                 = $this->nativesession->get('payway_reference_for_credit_card');
			$data                      = $this->booking_data->get( $reference );
			$payment                   = $data['payment'];
			$payment['card_type']      = 'VISA';
			$payment['payment_amount'] = 1000;
			$this->booking_data->update( $reference, array('payment' => $payment) );

			$data = $this->booking_data->get( $reference );
			?>
<p>Reference:: <?=$reference ?></p>
<p>This is a test. Integrating on how booking will be created.</p>
<p>It will automatically redirected in a few seconds. <a href="<?=base_url() ?>hotel/createBooking/approved?payway=success&id=<?=$data['hotelId'] ?>">Click here</a> if not.</p>

<pre><?php print_r($data) ?></pre>
			<?php
		    d_redirect('hotel/createBooking/approved?payway=success&id=' . $data['hotelId'], 100);
		    exit();
		} // end of if statement

		if( isset($_GET['Signature']) && isset($_GET['EncryptedParameters']) )
		{
			$encryptionKey           = $this->paymentwestpac->getEncryptionKey();
			$signatureText           = $_GET['Signature'];
			$encryptedParametersText = $_GET['EncryptedParameters'];
			$parameters = $this->paymentwestpac->decrypt_parameters( $encryptionKey, $encryptedParametersText, $signatureText );
			#$parameters = json_encode( $parameters );
			
			#$this->log_model->something( 'payway-test.json', json_encode( $parameters ), 1 );
			
			/**
			
				TODO:
				- Update the Database for the payments card type
				- Redirect to create booking
			
			 */
			$data                      = $this->booking_data->get( $parameters['payment_reference'] );
			$payment                   = $data['payment'];
			$payment['card_type']      = $parameters['card_type'];
			$payment['payment_amount'] = $parameters['payment_amount'];

			$this->booking_data->update( $parameters['payment_reference'], array('payment' => $payment) );
			$data = $this->booking_data->get( $parameters['payment_reference'] );

			?>
If not automatically redirected. <a href="<?=base_url() ?>hotel/createBooking/approved?payway=success&id=<?=$data['hotelId'] ?>">Click here</a>
			<?php
			redirect('hotel/createBooking/approved?payway=success&id=' . $data['hotelId']);
			exit();	
		}

		?>
<p>There was an error while processing your request.</p>
<p>If not automatically redirected. <a href="<?=base_url() ?>">Click here</a></p>
		<?php
	}

	/**
	 * [createBooking description]
	 * @param  [type] $paymentStatus [description]
	 * @return [type]                [description]
	 */
	public function createBooking ( $paymentStatus )
	{
		if( TEST_BOOKING )
		{
			if( TEST_BOOKING_ERROR )
			{
				response(array(
					'success' => FALSE,
					'error'   => 'hotel id is not defined, Your payment reference was been sent to your E-mail',
					'isModal' => TRUE
				), 403);
			}
			response(array(
				'success' => FALSE,
				'redirectLink'  => '123123123',
				'bookingId'     => '123123213',
				'itemId'        => '123123',
				'reservationId' => '1231231'
			));
		}

		$this->log_model->something( 'log.TR', 'Hotel ID: ' . $_GET['id'], 1 );
		_POST();

		if ( isset( $_GET['payway'] ) && trim($_GET['payway']) == 'success' )
		{
			# Code here
			$this->load->model('booking_data');
			$_POST = $this->booking_data->getSessionedData();
			if ( empty( $_POST ) )
			{
				# Code here
				?>
				<p>Sorry, Your session expired.</p>
				<p>If not redirected <a href="<?=base_url() ?>">Click here</a></p>
				<?php
				die();
			} // end of if statement
		} // end of if statement

		$post  = $_POST['pax'];

		$post['references']  = isset($_POST['payment']['reference'])?$_POST['payment']['reference']: date('mdhis');

		$this->log_model->something( 'log.TR', 'Voucher E-mail: ' . $post['voucher']['email'], 1 );
		if( ! isset($_GET['id']) || (int)$_GET['id'] == 0 )
		{
			if( $paymentStatus == 'approved' )
			{
				$this->notification_model->failureToReserve( $post );
				response(array(
					'success' => FALSE,
					'error'   => 'hotel id is not defined, Your payment reference was been sent to your E-mail',
					'isModal' => TRUE
				), 403);
			}
			response(array('success' => FALSE, 'message' => 'hotel id is not defined'), 403);
		}

		$s   = $this->xml_model->booking( $this->session->userdata('search_sess'), $post );
		$xml = wrapp_xml($s);
		$xml = xml_to_array($xml);
		

		$data = array();

		if( ! isset($xml['response'][0]['success'])  )
		{
			response(array(
				'success' => FALSE,
				'error'   => 'Network is buzy. Please try again',
				'isModal' => TRUE
			), 403);
		}

		if (  $xml['response'][0]['success'] == 0 )
		{
			response(array(
				'success' => FALSE,
				'error'   => $xml['response'][0]['content']['error'][0]['content']
			), 403);
		} else {

			$search_post = $this->nativesession->get( 'session_post' );
			$pax         = $this->totalizeNumberOfAdults( $search_post['rooms'] );

			// TODO
			// 1. Compile the Board types, cancellation
			// 2. get the status
			// 3. Sum up all prices, Board Types and Room names
			$s_date = explode('/', $search_post['start_date']);
			$e_date = explode('/', $search_post['end_date']);
			$served               = array();
			$served['totalPax']   = $pax;
			$served['voucher']    = $post['voucher'];
			$served['start_date'] = $s_date[1] . '/' . $s_date[0] . '/' . $s_date[2];
			$served['end_date']   = $e_date[1] . '/' . $e_date[0] . '/' . $e_date[2];

			$getCurrencyExchanged = $this->hotel_model->getConvertCurrencyEx();

			if( isset($xml['response'][0]['content']['reservation'][0]) )
			{
				if( $xml['response'][0]['content']['reservation'][0]['status'] == 'failed' )
				{
					// Send email if the user paid
					if( $paymentStatus == 'approved' )
					{
						// Send user notification
						$this->notification_model->failureToReserve( $post );
						$this->log_model->paymentReference( $post['references'], 1 );

						response(array(
							'success' => FALSE,
							'error'   => 'Failure to process your request, Your payment reference was been sent to your E-mail',
							'isModal' => TRUE
						), 403);
					}
					$this->log_model->something( 'post.json', json_encode($post), 1 );

					response(array(
						'success' => FALSE,
						'error'   => 'Failure to process your request',
						'isModal' => TRUE
					), 403);
				}

				// if booking session exist..
				// Add the item to the existing booking
				// Should check if the book still exist
				$bookExist =  $this->booking_model->checkBooking( $this->session->userdata('bookingId') );
				if( $post['forNewBook'] == TRUE )
				{
					// Add new booking
					// User ID is not being used
					$bookingId = $this->booking_model->newBooking( $this->session->userdata('agencycode'), $this->session->userdata('user_id') );
				} else {
					if( $bookExist )
					{
						$bookingId = intval($this->session->userdata('bookingId'));
					} else {
						// Add new booking
						$bookingId = $this->booking_model->newBooking( $this->session->userdata('agencycode'), $this->session->userdata('user_id') );
					}
				}
				$this->log_model->something( 'log.TR', 'Booking ID: '. $bookingId, 1 );
				
				$reservation             = $xml['response'][0]['content']['reservation'][0];
				$served['status']        = $reservation['status'];
				$served['price']         = 0;
				$served['quantity']      = 0;
				$served['originalPrice'] = 0;
				$served['priceCurrency'] = FALSE;
				$served['roomNamed']     = '';
				$served['remarks']       = '';
				$served['currency']      = CONVERTION_CURRENCY;
				$served['cancellation']  = array();
				$reservationId           = $reservation['id'];

				////////////////////
				// Get the agency //
				////////////////////
				
				$agency     = _AGENCY( $this->session->userdata('agencycode') );

			    ////////////////////////
	    		// END                //
			    ////////////////////////

	    		$guests = array();
	    		// Loop through the items
				foreach ( $reservation['content']['item'] as $key => $item )
				{
					$served['remarks']   .= ( !empty($item['content']['remarks'][0]) )?$item['content']['remarks'][0]:'';
					$served['quantity']  += intval($item['content']['quantity'][0]);
					$served['roomNamed'] .= ( empty($served['roomNamed']) )?'':', ';
					$served['roomNamed'] .= $item['content']['name'][0] . ' (' . $item['content']['board'][0]['content'] . ')';

					// Convert the price first
					$originalPrice   = ((float) $item['content']['booking'][0]['content']['price'][0]['amount']);
					$price           = $originalPrice * $getCurrencyExchanged[ $item['content']['booking'][0]['content']['price'][0]['currency'] ];
					$served['price'] += $price;

					$served['originalPrice'] += $originalPrice;
					if( ! is_string($served['priceCurrency']) )
					{
						$served['priceCurrency'] = $item['content']['booking'][0]['content']['price'][0]['currency'];
					}
					$served['provider']        = $item['content']['source'][0]['provider'];
					$served['isBillable']      = intval($item['content']['booking'][0]['billable']);
					$served['bookingCurrency'] = $item['content']['booking'][0]['content']['price'][0]['currency'];
					$served['reference']       = $item['content']['source'][0]['content']['reference'][0];

					// Check the policy if the type already exist
					$checkInDate    = explode("/", $served['start_date']);
					$checkOutDate   = explode("/", $served['end_date']);
					$refundableDate = '';
					$item['content']['cancellation'][0]['frame'] = $this->booking_model->parseCancellation( $item['content']['cancellation'][0]['frame'] );
					foreach ( $item['content']['cancellation'][0]['frame'] as $key => $policy )
					{
						if( $policy['type'] == 'fully-refundable' )
						{
							$refundableDate = date('Y-m-d', strtotime($policy['endTime']. ' -1 day'));
							continue;
						}
						if ( ! in_array($policy, $served['cancellation']) && $policy['type'] == 'non-refundable' )
						{
							$dateTo   = null;
							$dateFrom = null;
							// Check in date
							if( ! empty($refundableDate) )
							{
								if( strtotime($policy['endTime']) < strtotime('now') )
								{
									$dateTo   = $checkOutDate[2]."-".$checkOutDate[0]."-".$checkOutDate[1];
									$dateFrom = date('Y-m-d', strtotime("now"));
								} else {
									$dateTo   = date('Y-m-d', strtotime($policy['endTime']. ' -1 day'));
									$dateFrom = $checkInDate[2]."-".$checkInDate[0]."-".$checkInDate[1];
								}
							} else {
								$dateTo   = $checkOutDate[2]."-".$checkOutDate[0]."-".$checkOutDate[1];
								$dateFrom = date('Y-m-d', strtotime('now'));
							}

							
							$p = array();
							$p['amount']           = 0;
							$p['dateTo']           = $dateTo;
							$p['percent']          = 100;
							$p['dateFrom']         = ( !empty($refundableDate) )?$refundableDate: $dateFrom;
							$p['remarks']          = (isset($policy['content'][0]['remarks']))?$policy['content'][0]['remarks']: '';
							$p['typeCancellation'] = 'Percentage';

							// array_push($served['cancellation'], $p);
							if( ! in_array($p, $served['cancellation']) )
							{
								array_push($served['cancellation'], $p);
							}
						}
					} // end of cancellation policy

					//////////////////
					// COMPLE GUEST //
					//////////////////
					
					$paxGroups = $item['content']['pax-groups'][0]['pax-group'];

					foreach ( $paxGroups as $key => $paxGroup )
					{
						if( isset($paxGroup['content']['pax']) )
						{
							$itemPax = $paxGroup['content']['pax'];
							foreach ( $itemPax as $key => $value )
							{
								$guest = $this->bookinglib->buildGuest( $value['content']['title'][0], $value['content']['firstName'][0], $value['content']['lastName'][0], (int)$value['content']['age'][0], $agency['agency_name'] );
								array_push($guests, $guest);
							}
						}	
					} // end of pax groups
				} // end of foreach of items
				$served['calculation'] = $this->hotel_model->calculateTotalCost( $served['price'], $served['priceCurrency'], (float)$served['originalPrice'] );
			} // end of checking reservation
			///////////////////////////
			// GET THE HOTEL DETAILS //
			///////////////////////////
			$connection = new MongoClient();
			$db         = $connection->db_instant;

			// Get the hotel details
			$served['hotelDetails'] = $db->hotels->findOne(array('id' => intval($_GET['id'])), array('_id' => 0));
			$city                   = $db->cities->findOne(array('cid' => intval($search_post['location']['id'])), array('_id' => 0));
			$served['city']         = $city;

			$connection->close();
			#########################
			
			try {
				$parsed = $this->booking_model->parseReservationItemFields( $served, $bookingId, $reservationId );
				if( ! $parsed )
				{

					$cancelled = $this->xml_model->cancelBooking( $xml['response'][0]['content']['reservation'][0]['id'], 'Supplier was not found' );
					$this->xml_model->cancelBooking( $reservationId, 'Supplier does not exist' );

					if( $paymentStatus == 'approved' )
					{
						// Send user notification
						$this->notification_model->failureToReserve( $post );
						response(array(
							'success' => FALSE,
							'error'   => 'Supplier does not exist, Your payment reference was been sent to your E-mail',
							'isModal' => TRUE
						), 403);
					}

					response(array(
						'success' => FALSE,
						'error'   => 'Supplier does not exist',
						'isModal' => TRUE
					), 403);
				}

				$parsed['application'] = _APPCODE('H');
				$parsed = $this->bookinglib->buildItemArray( $parsed );
				// New Item on the booking
				$param = array('action' => 'addItemToBooking', 'postDetails' => json_encode($parsed));

				// Send the data through CURL
				$result = cURL('execute_item_manager', $param);
				$itemId = intval($result['item_id']);

				$this->bookinglib->init( $bookingId, $itemId );

				$this->log_model->something('log.TR', 'Item ID: ' . $itemId, 1);

		        if ( isset($result['status']) && $result['status'] )
		        {
		        	///////////////////////////
		        	// Insert the item guest //
			        ///////////////////////////
			        $paxIds = $this->bookinglib->executeItemGuests( $guests );
			        // Allocate item guest
			        $this->bookinglib->executeAllocateGuests( $paxIds['paxIds'] );

			        #$result = cURL( 'execute_pax_item', $params );
			        $result = $this->bookinglib->executeConvertItem(  );
			        
			        if( $paymentStatus == 'approved' )
			        {
			        	$payment = $_POST['payment'];

			        	$receiptParams = $this->bookinglib->buildCCReceipt(array(
							'receiptTypeCode'       => 'EFTB',
							'receiptTypeName'       => 'EFT (Bank)',
							'receiptDescription'    => 'B2B: Credit Card Payment',
							'receiptCCTypeCode'     => $payment['card_type'],
							'receiptCCTypeName'     => $payment['card_type'],
							'receiptCCNumber'       => $payment['no_credit_card'],
							'receiptCCExpiryDate'   => $payment['dt_expiry_month'] . '/' . $payment['dt_expiry_year'],
							'receiptCCSecurityCode' => $payment['no_cvn'],
							'receiptDisplayAmout'   => 0,
							'receiptAmout'          => 0,
							'receiptDepositedAmout' => $payment['payment_amount'],
							'receiptRefundAmout'    => 0
			        	));
						$receiptResult = $this->bookinglib->executeReceipt( $receiptParams );
						// $receiptResult = cURL( 'execute_receipt_manager2', $receiptParams );
						// Update the item receipt
						if( $receiptResult['status'] == TRUE )
						{
							// Must allocate the data
							# $allocations = $this->booking_model->allocationFields( $bookingId, $receiptResult['receipt_id'], $itemId, $payment['payment_amount'] );
							$allocations = $this->bookinglib->buildAllocationFields( $receiptResult['receipt_id'], $payment['payment_amount'] );


							# $allocationResult = cURL( 'execute_receipt_manager2', $allocations );
							$allocationResult = $this->bookinglib->executeReceiptAllocation( $allocations );

							$this->log_model->something( 'log.TR', 'Receipt ID: ' . $receiptResult['receipt_id'], 1 );


							if( array_key_exists('status', $allocationResult) && $allocationResult['status'] == TRUE )
							{
								$this->bookinglib->updateItems( $receiptResult['receipt_id'], 1 );
							}
						}
						
						$this->bookinglib->executeVoucher(  );
						// $this->bookinglib->clear();

						// Send notification email for the payment
				        // Data needed are:
				        // 1. Item purchased
				        // 2. if required send also the total amount
				        // $this->notification_model->emailPayment( $notifData );
						$paxCounts = $this->totalizeNumberOfAdults( $this->nativesession->get( 'session_post' )['rooms'] );
				        $this->bookinglib->sendVoucher(array(
							#'paidAmount'  => (float)$payment['payment_amount'],
							'emailTo'  => ( EMAIL_IS_TEST?EMAIL_TEST: $post['voucher']['email'] ),
							'content'  => 'template/payment/credit-card',
							'bookType' => 'Hotel',
							'voucher'  => array(
								'name'  => $post['voucher']['firstname'],
								'email' => $post['voucher']['email']
							),
							'payment'  => array(
								'type'      => 'credit card',
								'paid'      => (float)$payment['payment_amount'],
								'receiptId' => (int)$receiptResult['receipt_id']
							),
							'pax'      => array(
								'adults' => $paxCounts['adults'],
								'childs' => $paxCounts['childrens'],
								'rooms'  => $paxCounts['rooms']
							),
							'data' => array(
								'VOUCHER'     => $post['voucher'],
								'voucherName' => $post['voucher']['firstname'] . ' ' . $post['voucher']['lastname'],
								'paidAmount'  => (float)$payment['payment_amount'],
								'isPayable'   => TRUE,
								'isFullyPaid' => TRUE,
							)
						));
			        }

			        ///////////////////////////
			        // Insert a notification //
			        ///////////////////////////
			        /*$this->notification_model->log(array(
			        	'notificationLoginReference' => $this->session->userdata('loginNotificationReference'),
						'bookingId' => $this->bookinglib->getBookingId(),
						'itemId'    => array( $this->bookinglib->getItemIds() ),



			        ));*/
			        $this->log_model->something('log.TR', 'Reservation ID: ' . $xml['response'][0]['content']['reservation'][0]['id'], 1);

			        ################## B2C #############
			       	$reservationId = $xml['response'][0]['content']['reservation'][0]['id'] . '' . '?booking_id=' . $bookingId;
			        $redirectLink  = base_url() . 'reservation-details/' . $reservationId;

			        generate_token( 'hotelToken' );

			        /* Redirected from PayWay link */
			        if ( isset( $_GET['payway'] ) && trim($_GET['payway']) == 'success' )
					{
						# Code here
						redirect( $redirectLink );
						#redirect( 'hotel/searchResult' );
						exit();
					} // end of if statement

			        response(array(
			        	'success' => TRUE,
						'redirectLink'  => $redirectLink,
						'bookingId'     => $this->bookinglib->getBookingId(),
						'itemId'        => $this->bookinglib->getItemIds(),
						'reservationId' => $reservationId
					));

		        } else {
		        	response(array('success' => FALSE), 403);
		        }
			} catch ( Exceptopn $e ) {
				response(array('success' => FALSE, 'message' => $e), 403);
			}
		} // end of checking whether success
	} // end of creating booking

	/**
	 * [bookingValidation description]
	 * @return [type] [description]
	 */
	public function bookingValidation ()
	{
		_POST();

		$data                   = array();
		$data['errors']         = array();
		$data['errors']['main'] = array();

		$data['booking_info'] = $this->xml_model->bookingInfo($this->session->userdata('search_sess'), $this->nativesession->get('session_selected_rooms'));
		$data['booking_info'] = wrapp_xml($data['booking_info']);
		$data['booking_info'] = xml_to_array($data['booking_info']);
		$data['booking_info'] = $this->getCurrency($data['booking_info']);

		if( ! $data['booking_info'] )
		{
			response(array( 'main' => array('Network error, please try searching again.') ), 401);
		}
		$dueAmount   = number_format($data['booking_info']['response'][0]['content']['calculation']['calculation']['due_amount'], 2, '.', '');
		
		$roomRules   = $this->roomRules();
		$validations = $roomRules;
		$validations = array_merge($validations, $this->config->item('hotel_booking_VOUCHER'));

		if ( isset($_POST['payment']) )
		{
			# Code here
			$validations = array_merge($validations, $this->config->item('hotel_booking_CREDIT_CARD'));
		} // end of if statement

		$this->form_validation->set_rules($validations);
		$this->form_validation->set_error_delimiters('', '');

		$isPass = $this->form_validation->run();
		if( ! array_key_exists('forNewBook', $_POST) )
		{
			array_push($data['errors']['main'], 'Network error.');
			$isPass = FALSE;
		}

		if( $isPass === FALSE )
		{
			$data['errors']['voucher'] = array();
			foreach ( $_POST['voucher'] as $key => $value )
			{
				array_push($data['errors']['voucher'], form_error('voucher[' . $key . ']'));
			}
			$data['errors']['voucher'] = array_filter($data['errors']['voucher'], array(__CLASS__, 'removeEmpty'));
			$data['errors']['voucher'] = array_values($data['errors']['voucher']);
			
			if( ! empty($data['errors']['voucher']) )
			{
				array_push($data['errors']['main'], 'There was an error at the Vouchers fields');
			}

			$msg = form_error('policy_read');
			if( ! empty($msg) )
			{
				array_push($data['errors']['main'], 'Please read the cancellation policy and toggle the checkbox');
			}
			foreach ( $roomRules as $key => $rule )
			{
				$msg = form_error($rule['field']);
				if ( ! empty($msg) )
				{
					array_push($data['errors']['main'], 'There was an error at the guest names, please fill up properly');
					break;
				}
			}

			if( isset($_POST['payment']) )
			{
				$data['errors']['payment'] = array();

				foreach ( $_POST['payment'] as $key => $value )
				{
					array_push($data['errors']['payment'], form_error('payment[' . $key . ']'));
				}
				$data['errors']['payment'] = array_filter($data['errors']['payment'], array(__CLASS__, 'removeEmpty'));

				if( ! empty($data['errors']['payment']) )
				{
					array_push($data['errors']['main'], 'There an error at the Payment fields');
				}
			}

			response($data['errors'], 403);
		} else {
			/* Payway parameters */
			/*$parameters                             = array();
			$parameters['username']                 = PAYWAY_USERNAME;
			$parameters['password']                 = PAYWAY_PASSWORD;
			$parameters['biller_code']              = PAYWAY_BILLER_CODE;
			$parameters['merchant_id']              = PAYWAY_MERCHAND_ID;
			$parameters['payment_amount']           = (float)number_format($data['booking_info']['response'][0]['content']['calculation']['calculation']['due_amount'], 2, '.', '');
			$parameters['surcharge_rates']          = 'VI/MC=1.8,AX=3.8,DC=3.8';
			$parameters['return_link_url']          = base_url() . 'hotel/payWayCallBackTest';
			$parameters['payment_reference']        = time();
			$parameters['return_link_redirect']     = 'true';
			$parameters['payment_reference_change'] = 'false';*/

			$price   = (float)number_format($data['booking_info']['response'][0]['content']['calculation']['calculation']['due_amount'], 2, '.', '');
			$product = array();

			foreach ( $data['booking_info']['response'][0]['content']['booking-options'][0]['content']['booking-option'] as $key => $bookingOption )
			{
				$itemPrice = ( isset($bookingOption['content']['room'][0]['price'][0]) )? $bookingOption['content']['room'][0]['price'][0]: $bookingOption['content']['room'][0]['content']['price'][0];
	            $itemName  = ( isset($bookingOption['content']['room'][0]['name'][0]) )? $bookingOption['content']['room'][0]['name'][0]: $bookingOption['content']['room'][0]['content']['name'][0];
	            $itemBoard = ( isset($bookingOption['content']['room'][0]['board'][0]['content']) )? $bookingOption['content']['room'][0]['board'][0]['content']: $bookingOption['content']['room'][0]['content']['board'][0]['content'];

	            $product[$itemName . ' (' . $itemBoard . ')'] = '1,'. number_format($itemPrice['calculation']['calculation']['due_amount'], 2, '.', '');
			}

			/* Generating Payway token */
			/*$parameters = array_merge($parameters, $product);
			$token = 'kjabhfiaje';
			if( ! PAYWAY_IS_TEST ) { $token = $this->paymentwestpac->getToken( $parameters ); }*/
			/*response(array(
				'success'    => TRUE,
				'handOffUrl' => (PAYWAY_IS_TEST)?$parameters['return_link_url']:PAYWAY_BASE_URL . 'MakePayment',
				'token'      => $token
			));*/
			response(array(
				'success'  => TRUE,
				'products' => $product,
				'price'    => $price
				#'handOffUrl' => (PAYWAY_IS_TEST)?$parameters['return_link_url']:PAYWAY_BASE_URL . 'MakePayment',
				#'token'      => $token,
			));
		}
	} // end of booking validation

	/**
	 * [roomRules description]
	 * @return [type] [description]
	 */
	private function roomRules ()
	{
		$validations = array();

		foreach ( $_POST['rooms'] as $keyRoom => $room )
		{
			// Loop on the adult
			foreach ( $room['adult'] as $keyAdult => $adult )
			{
				foreach ($adult as $key => $value)
				{
					array_push($validations, array(
						'field' => 'rooms[' . $keyRoom . '][adult][' . $keyAdult . '][' . $key . ']',
						'label' => 'Required',
						'rules' => 'required'
					));
				}
			}
			if( isset($room['child']) )
			{
				foreach ( $room['child'] as $keyChild => $child )
				{
					foreach ( $child as $key => $value )
					{
						array_push($validations, array(
							'field' => 'rooms[' . $keyRoom . '][child][' . $keyChild . '][' . $key . ']',
							'label' => 'Required',
							'rules' => 'required'
						));
					}
				}
			}
		}
		return $validations;
	}

	/**
	 * [removeEmpty description]
	 * @param  [type] $var [description]
	 * @return [type]      [description]
	 */
	private function removeEmpty ($var)
	{
		return ( ! empty($var));
	}

	public function flight()
	{
		$data = array();
		$this->load_view('flight/searchflight',$data); 
	}

	public function flightDetails()
	{
		$data = array();
		$this->load_view('flight/searchdetails',$data); 
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
