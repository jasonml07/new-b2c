<?php


/**
* 
*/
class Database extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
    public function test(){
        $connection = new MongoClient();
            $db = $connection->db_system;
        $agency = $db->agency->findOne(array('agency_code' => 'EET'), array('_id' => 0, 'agency_code' => 0));
        $connection->close();
        print_r($agency);
    }
    public function status (  )
    {
        # Code here
        if ( !@$_GET['callback'] )
        {
            # Code here
            response(array('SUCCESS' => FALSE, 'MESSAGE' => 'Callback is not defined.'));
        } // end of if statement

        echo $_GET['callback'], '(', json_encode($this->nativesession->timeout_get('ETLOGIN')), ')';
    } // end of status

    public function triggerLogin (  )
    {
        # Code here
        if ( ! @$_GET['password'] || ! @$_GET['username'] )
        {
            # Code here
            response(array('SUCCESS' => FALSE, 'MESSAGE' => 'One of the password or username is not defined.'), 403);
        } // end of if statement
        $this->load->model('agency_model', 'AM');

        $consultant = $this->AM->user($_GET['password'], $_GET['username']);

        if( ! empty( $consultant ) )
        {
            $this->nativesession->timeout_set('ETLOGIN', array('ID' => (string)$consultant['_id'], 'CODE' => $consultant['agency_code'], 'name' => $consultant['fname'] . ' ' . $consultant['lname']));
            response(array('SUCCESS' => TRUE));
        }
    } // end of triggerLogin

    public function agencyLogout($token){
        $connection=new MongoClient();
        $db=$connection->db_system;
        $db->token->remove(array('_id'=>new MongoId($token)));
        $connection->close();

        response(array('success'=>TRUE,'msg'=>'Token removed'));
    }
    public function agencyStatus (  )
    {
        if (!isset($_GET['token'])) {
            $data=array('success'=>FALSE,'msg'=>'Undefined token');
            if (isset($_GET['callback'])) {
                echo $_GET['callback'], '(', json_encode($data), ')';
                exit();
            }
            response($data);
        }
        $token=$_GET['token'];
        $connection=new MongoClient();
        $db=$connection->db_system;
        $data=$db->token->findOne(array('_id'=>new MongoId($token)));
        $connection->close();
        if (!$data) {
            $data=array('success'=>FALSE,'msg'=>'Token Expired');
            if (isset($_GET['callback'])) {
                echo $_GET['callback'], '(', json_encode($data), ')';
                exit();
            }
            response($data);
        }
        $data['token']=$data['_id']->{'$id'};
        if ( !isset($_GET['callback']) )
        {
            # Code here
            response(array('success' => true, 'data'=>$data));
        } // end of if statement
        echo $_GET['callback'], '(', json_encode(array('success'=>TRUE,'data'=>$data)), ')';
    } // end of status
    public function agencyLogin (  )
    {
        # Code here
        // response(array('success'=>FALSE,'msg'=>'test','data'=>$_POST));
        if ( ! @$_POST['password'] || ! @$_POST['username'] )
        {
            # Code here
            response(array('success' => FALSE, 'msg' => 'One of the password or username is not defined.'));
        } // end of if statement
        $this->load->model('agency_model', 'AM');

        $consultant = $this->AM->signIn($_POST);

        if( ! empty( $consultant ) )
        {
            $data=array('ID' => (string)$consultant['_id'], 'CODE' => $consultant['agency_code'], 'name' => $consultant['fname'] . ' ' . $consultant['lname']);
            // $this->nativesession->timeout_set('ETLOGIN', $data);
            $connection=new MongoClient();
            $db=$connection->db_system;
            $data['expiresAt']=new MongoDate(strtotime('now +24 hours'));
            $db->token->ensureIndex(array('expiresAt'=>1),array('expireAfterSeconds'=>0));
            $db->token->insert($data);
            $connection->close();

            $data['token']=$data['_id']->{'$id'};
            unset($data['_id']);
            $this->nativesession->timeout_set('ETLOGIN', $data);
            response(array('success' => TRUE,'data'=>$data));
        } else {
            response(array('success' => FALSE,'msg'=>'Invalid Account.'));
        }
    } // end of triggerLogin

    public function triggerLogout (  )
    {
        # Code here
        #print_r($this->nativesession->get('ETLOGIN'));
        $this->nativesession->delete('ETLOGIN');
    } // end of triggerLogout

    /**
     * [searchCity description]
     * @return [type] [description]
     */
	public function searchCity (  )
    {
        header('Access-Control-Allow-Origin: *');
        
		$address = $_GET['address'];

        $connection = new MongoClient();
        $db         = $connection->db_instant;
        $search     = array(
            '$or' => array(
                array('cityname'    => new MongoRegex("/^ $address/i")),
                array('countryname' => new MongoRegex("/^ $address/i"))
            )
        );

        $cities = $db->cities_new->find( $search, array('_id' => 0) )->sort(array('priority' => -1));
        $cities = iterator_to_array($cities);

        $connection->close();

        response($cities);
    }

    public function searchCity2 (  )
    {
        header('Access-Control-Allow-Origin: *');
        
        $address = $_GET['address'];

        $connection = new MongoClient();
        $db         = $connection->db_instant;
        $search     = array(
            '$or' => array(
                array('cityname'    => new MongoRegex("/^ $address/i")),
                array('citystate'   => new MongoRegex("/^ $address/i")),
                array('countryname' => new MongoRegex("/^ $address/i"))
            )
        );

        $cities = $db->cities->find( $search, array('_id' => 0) )->limit(10);
        $cities = iterator_to_array($cities);

        $connection->close();

        response($cities,200,"myfunction");
    }

    /**
     * [searchFlightInCity description]
     * @return [type] [description]
     */
    public function searchFlightInCity ( )
    {
        header('Access-Control-Allow-Origin: *');
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, 'https://reservations.ifly.net.au/citySearch.aj?ida=true&preferredCountry=AU&sn=reservations&term=' . $_GET['term']); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        // curl_setopt($ch, CURLOPT_POST, true); 
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $result = curl_exec($ch);
        print_r($result);
        // echo json_decode($result, TRUE);
        exit();
    }

    /**
     * [widgetStyle description]
     * @return [type] [description]
     */
    public function widgetStyle ()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-type: text/css");
        echo $this->load->view('widget/style', array(), TRUE);
        exit();
    }

    /**
     * [widgetScript description]
     * @return [type] [description]
     */
    public function widgetScript ()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-type: text/javascript");
        echo $this->load->view('widget/script', array(), TRUE);
        exit();
    }
}