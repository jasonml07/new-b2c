<?php

/**
* 
*/
class Cron extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model(array(
            'xml_model', 'hotel_model', 'result_model', 'agency_model'
        ));
	}

    /**
     * [index_get description]
     * @return [type] [description]
     */
    public function search ()
    {
        $xml = wrapp_xml( $this->xml_model->destinationRooms() );
        $xml = xml_to_array( $xml );


        if( (int) $xml['response'][0]['success'] == 1 )
        {
            if( $xml['response'][0]['content']['search'][0]['status'] == 'working' )
            {
                $this->session->set_userdata('cron_session_distination_search', ''. $xml['response'][0]['session']);
            }
            $this->session->set_userdata('cron_session_distination_search_status', ''. $xml['response'][0]['content']['search'][0]['status']);
        }

        response($xml['response'][0]);
    }

    /**
     * [searchResult description]
     * @param  [type] $doAlreadyHasData [description]
     * @return [type]                   [description]
     */
	public function searchResult ( $doAlreadyHasData )
    {
        // The porpuse of ob_start() is to make sure that the header was sent after the characters printed

        $xml = wrapp_xml( $this->xml_model->pollInitiate($this->session->userdata('search_sess')) );
        $xml = xml_to_array( $xml );

        if( ! $xml )
        {
            response($xml, 404);
        }

        // The cron_session_search_result_status was been initialize at the hotel_search method
        // this variable will only be set/initialize when the user search
        // In this senario when the use refresh the page it only check if the status is already done
        // else the error message occured
        // also the cron_session_search_result_status will be only reintialized on the failed block
        // that handles the session status
        if( $this->session->userdata('cron_session_search_result_status') == 'done' )
        {
            $d = $this->result_model->getAllResult();
            // $d['curl'] = $xml;

            if( ! is_array($xml['response'][0]['content']['results'][0]['content']) )
            {
                response(array('success' => FALSE, 'message' => 'Content is not an array'), 401);
            }
            // Return the data from the database
            response($d);
        } else {
            // Please handle the error
            if( isset($xml['response'][0]['session']) )
            {
                $this->session->set_userdata('cron_session_search_result', ''. $xml['response'][0]['session']);
                $this->session->set_userdata('cron_session_search_result_status', ''. ( isset($xml['response'][0]['content']['job']) ) ? $xml['response'][0]['content']['job'][0]['status']: 'failed');
                $xml = $xml['response'][0];
            } else {
                response($xml, 404);
            }
            // if( intval($xml['success']) == 1 && !empty($xml['content']['results'][0]['content']) && $xml['content']['job'][0]['status'] == 'done' )
            if( intval($xml['success']) == 1 && !empty($xml['content']['results'][0]['content']) && $xml['content']['job'][0]['status'] == 'done' )
            {

                // Checking if the result is exist
                // This block handles the content when its empty
                // back then it will return a result of initials in order to view
                // to the user that the result are empty
                if( isset($xml['content']['results'][0]['content']['result']) )
                {
                    $result = $xml['content']['results'][0]['content']['result'];
                    if( is_array($result) && count($result) > 0 )
                    {
                        $result = $this->getExtraData($result);
                        // Insert to the database
                        $counts = $this->result_model->saveResult($result);
                        $xml['content']['results'][0]['content']['result'] = $result;
                        $xml['content']['results'][0]['content']['counts'] = $counts;
                    }
                } else {
                    // Initialize result;
                    $xml['content']['results'][0]['content']['result'] = array();
                    $xml['content']['results'][0]['content']['counts'] = array(
                            'price'      => array('max'=>0,0),
                            'hotels'     => array(0,0),
                            'ratings'    => array(0,0,0,0,0,0),
                            'boardtypes' => array('AI'=>0, 'FB'=>0, 'HB'=>0, 'BB'=>0, 'RO'=>0)
                        );
                }
                if( $doAlreadyHasData == 0 )
                {
                    $xml['content']['results'][0]['content']['isDatabase'] = false;
                }
                response($xml['content']['results'][0]['content']);
            } else {
                // response( array('success' => FALSE, 'message' => 'FAILED', 'XML' => $xml), 403 );
                $error = array();
                $error['success'] = intval($xml['success']);

                // When not in success get the error
                if( $error['success'] == 0 )
                {
                    $error['message'] = $xml['content']['error'][0]['content'];
                }

                $error['content'] = array();
                $error['content']['job'] = array(array());
                if( $error['success'] == 1 )
                {
                    $error['content']['job'][0]['status'] = $xml['content']['job'][0]['status'];

                    // Check if the message is string
                    // Some of the content are array type which has a value of URL
                    $error['content']['job'][0]['message'] = null;
                    if( is_string($xml['content']['results'][0]['content']) )
                    {
                        $error['content']['job'][0]['message'] = $xml['content']['results'][0]['content'];
                    }
                }

                // Think of the logic which will reduce the time or that check if the initial data is already given on the font end
                /*if( $doAlreadyHasData == 0 )
                {
                    if( isset($xml['content']['results'][0]['content']['result']) && is_array($xml['content']['results'][0]['content']['result']) )
                    {
                        $lenght = count($xml['content']['results'][0]['content']['result']);
                        // $result = array_splice($xml['content']['results'][0]['content']['result'], 0,(($lenght > 12)?12:$lenght),true);
                        $result = $xml['content']['results'][0]['content']['result'];
                        $error['result'] = $this->getExtraData($result);
                        $error['counts'] = $this->result_model->saveResult($error['result']);
                    }
                }*/

                if( isset($xml['content']['results'][0]['content']['result']) && is_array($xml['content']['results'][0]['content']['result']) )
                {
                    $length = count($xml['content']['results'][0]['content']['result']);
                    // $result = array_splice($xml['content']['results'][0]['content']['result'], 0,(($lenght > 12)?12:$lenght),true);
                    if( $length > intval($_GET['length']) ) {
                        $result = $xml['content']['results'][0]['content']['result'];
                        $error['result'] = $this->getExtraData($result);
                        $error['counts'] = $this->result_model->saveResult($error['result']);
                    } else {
                        unset($error['result']);
                        unset($error['counts']);
                    }
                }
                response($error, 403);
            }
        }
    }

    /**
     * [requestRoom description]
     * @param  [type]  $hotel_id [description]
     * @param  boolean $isArray  [description]
     * @return [type]            [description]
     */
    public function requestRoom ( $hotel_id, $isArray = FALSE )
    {
        $xml = $this->xml_model->requestRooms( $this->session->userdata('search_sess'), $hotel_id );
        $xml = wrapp_xml( $xml );
        $xml = xml_to_array( $xml );

        // The price must be converted to AUD
        if( isset($xml['response'][0]['content']) && is_array($xml['response'][0]['content']) )
        {
            if( $xml['response'][0]['content']['job'][0]['status'] == 'failed' )
            {
                if( $isArray === TRUE )
                {
                    return FALSE;
                    die();
                }
                unset($xml['response'][0]['content']['result']);
                response($xml['response'][0]['content']);
            }

            if( ! $xml['response'][0]['content'] )
            {
                if( $isArray === TRUE )
                {
                    return FALSE;
                    die();
                }
                response(array('success' => FALSE, 'message' => 'Network error'), 401);
            }

            
            $xml = $this->hotel_model->parseRoomsData( $xml );

            if( $xml === FALSE )
            {
                response( array('success' => FALSE, 'message' => 'Error in parsing data'), 401 );
            }
            
            if( $isArray === TRUE )
            {
                return $xml;
                die();
            }
            response( $xml['response'][0]['content'] );
        } else {
            if( $isArray === TRUE )
            {
                return FALSE;
                die();
            }
            response( array('success' => FALSE, 'message' => 'Network is buzy'), 401 );
        }
    }

    /**
     * To collect extra data from the data base
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    private function getExtraData ( $data )
    {
        if( empty($data) )
        {
            return FALSE;
        }

        // TODO
        // Loop through results and get the extra data from the hotel
        // Base on the hotel id
        $hotelIds = array(); 
        foreach ( $data as $key => $value )
        {
            array_push($hotelIds,(int)$value['id']);
        }

        $this->agency_model->updateItemCosting( $this->session->userdata('agencycode') );
        
        $agency_costing       = $this->nativesession->get('costing_calculation');
        $extraHotelDetails    = $this->hotel_model->extraHotelDetails($hotelIds);
        $getCurrencyExchanged = $this->hotel_model->getConvertCurrencyEx();
        $details              = array();

        foreach ($data as $key => $value)
        {
            if( isset($extraHotelDetails['details'][(int)$value['id']]) )
            {
                try {
                    $thisResData = array();

                    if( isset($extraHotelDetails['facilities'][(int)$value['id']]) )
                    {
                        $thisResData['facilities']  = $extraHotelDetails['facilities'][(int)$value['id']]['facilities'];
                    }
                    $curr         = '';
                    $price        = 0;
                    $priceContent = array();
                    if( isset($value['content']['combinations']) )
                    {
                        $curr  = $value['content']['combinations'][0]['combination'][0]['currency'];
                        $price = $value['content']['combinations'][0]['combination'][0]['price'];
                    } else {

                        if( isset($value['content']['price']) && isset($value['content']['non-billable-price']) )
                        {
                            $p        = $value['content']['price'][0];
                            $billable = $value['content']['non-billable-price'][0];

                            // Check the smallest value
                            if ( (float) $p['minCommissionablePrice'] < (float)$billable['minCommissionablePrice'] )
                            {
                                # Code here
                                $price        = (float)$p['minCommissionablePrice'];
                                $curr         = $p['currency'];
                                $priceContent = $p;
                            } else {
                                $price        = (float)$billable['minCommissionablePrice'];
                                $curr         = $billable['currency'];
                                $priceContent = $billable;
                            } // end of else if statement
                        } else if( isset($value['content']['non-billable-price']) ) {
                            $curr         = $value['content']['non-billable-price'][0]['currency'];
                            $price        = (float)$value['content']['non-billable-price'][0]['minCommissionablePrice'];
                            $priceContent = $value['content']['non-billable-price'][0];
                        } else {
                            $curr         = $value['content']['price'][0]['currency'];
                            $price        = (float)$value['content']['price'][0]['minCommissionablePrice'];
                            $priceContent = $value['content']['price'][0];
                        }
                    }
                    $thisResData['numberOfRoom'] = (isset($value['content']['combinations']))?count($value['content']['combinations'][0]['combination'][0]['content']['rooms'][0]['room']):(int)$priceContent['content']['min-rooms'][0]['min-room'][0]['roomCount'];
                    
                    $thisResData['id']           = $value['id'];
                    $thisResData['price']        = (float)$price*$getCurrencyExchanged[$curr];
                    
                    $thisResData['calculation']  = $this->hotel_model->calculateTotalCost( $thisResData['price'], $curr, $price );
                    
                    $thisResData['request']      = $value;
                    $thisResData['currency']     = $curr;
                    $thisResData['details']      = $extraHotelDetails['details'][(int)$value['id']];

                    array_push($details,$thisResData);
                } catch ( Exception $e ) {
                    print_r($value);
                    die();
                }
            }
        }

        return $details;
    }
}