<?php
	class Index extends CI_Controller {

		public $View;

		public function __construct()
	    {
	        parent::__construct();
	        $this->load->model('login_model');


	    }


	    ########### PAGES VIEWS ################
		public function index(){
			echo $_GET['agencyCode'];

			if($this->session->userdata('logged_in') == 0) { //Session does not exists

				$this->load_view('login');

			}else if($this->session->userdata('logged_in') == 1){  // Session exists
				redirect('dashboard');
			}else{
				$viewFilename = "login";
				$this->load_view('login');
			}


			/*

			1. if is not set session for any agency . setUP 

			agency code decode to base64 erx: asdfasd= to EET
			once decode, check themes configoration in mongo, findo ONe then store to session

			2. if set, then display proper themes
			
			*/



		}

		public function load_view($viewFilename , $data = array()){


    		echo $this->load->view('titlehead'); 
			echo $this->load->view('header'); 
			echo $this->load->view($viewFilename, $data);
			echo $this->load->view('footer');
	    
		}

		public function verifylogin(){

		
		   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		 
		   if($this->form_validation->run() == FALSE)
		   {
		     	//Field validation failed.  User redirected to login page
     	        $this->form_validation->set_message('check_database', 'Invalid username or password');
				$this->load_view('login'); 
		   } else {

		   	   //Field validation succeeded.  Validate against database
			   $username = $this->input->post('username');
			   $password = $this->input->post('password');

			 	$connection = new MongoClient();

				$db = $connection->db_system;
			

				#echo md5($_POST['password']);
				$whereData = array("username"=>$username,"password"=> $password ,"is_active"=>(int)1);
				$result = $db->agency_consultants->findOne($whereData);
		
		
			   //query the database
			  // $result = $this->login_model->checkDatabase($username, $password);
			   //print_r($result);
			  // die();
			   if($result){


				 	$sess_array = array(
						'user_id'    => (string)$result['_id'],
						'username'   => $result['username'],
						'agencycode' => $result['agency_code'],
						
						'logged_in'  => 1
			      	 );
			        $this->session->set_userdata($sess_array);
				 	redirect('dashboard');

			 	}else{
			 		//Display error message to Login page
		 			$this->session->set_flashdata('loginError', 
		 				'<div id="alert-message" class="alert alert-danger" style="border: 0; background-color:#F44336;color:rgba(255, 255, 255, 0.843137)">
		 					<a href="#" class="close" data-dismiss="alert">×</a>
		 					Invalid Username or Password! 
		 				</div>
		 				<style>
		 				#username, #password{
		 				    box-shadow: 0 0 5px red;
    						border: 1px solid red;
    					}
		 				</style>
		 				');	

			 		 $sess_array = array(
			         	'logged_in' => 0
			      	 );
			        $this->session->set_userdata($sess_array);
					redirect('index');


			 	}
		    } // End of If Else Statement for Form Validation
		}

		public function logout(){

			$this->session->sess_destroy();
			redirect('index');
		}

	}
?>