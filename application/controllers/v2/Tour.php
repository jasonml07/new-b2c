<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tour extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }
  /**
   * [index description]
   * @return [type] [description]
   */
  public function index()
  {
    $this->load->view('v2/tour/index.html'); 
  }
}