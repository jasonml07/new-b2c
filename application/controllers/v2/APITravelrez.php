<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class APITravelrez extends CI_Controller
{

  // protected $SESSION = [];
  // protected $AGENCY = null;
  // protected $PAYMENT_TYPES = [];
  // protected $PAYMENT_TYPES_TEXT = [];

  function __construct()
  {
    parent::__construct();
    $this->lang->load('travelrez');
    $this->load->model('travelrezuser_model');

  }
  /**
   * [authenticated description]
   * @return [type] [description]
   */
  private function authenticated()
  {
    $user = $this->travelrezuser_model->findOne(['api_token' => $_SERVER['HTTP_AUTHORIZATION']]);
    if(!$user || strtotime('now') > $user['api_token_expires_at']->sec) {
      response(array(
        'message' => $this->lang->line('EXPIRED_TOKEN')
      ), 403);
      return false;
    }

    return true;
  }
  /**
   * [getUser description]
   * @return [type] [description]
   */
  private function getUser()
  {
    return $user = $this->travelrezuser_model->findOne(['api_token' => $_SERVER['HTTP_AUTHORIZATION']]);
  }

  /**
   * [login description]
   * @return [type] [description]
   */
  public function login()
  {
    _POST();
    // $this->load->helper('date');
    $this->load->model('notification_model');
    $email = $_POST['email'];
    $user = $this->travelrezuser_model->findUser($email);
    if(!$user) {
      response(array(
        'errors' => array(
          'email' => $this->lang->line('USER_NOT_FOUND')
        )
      ), 422);
    } else {
      $pin = random_string('numeric', 6);
      $token = random_string('alnum', 20);
      // log_message('debug', date('Y-m-d H:i:s', strtotime('now +5 minutes')));
      $this->notification_model->sendMail(array(
        'email' => array(
          'content' => 'emails/travelrez/one-time-pin',
          'to' => $user['email'],
          'from' => '',
          'name' => 'TravelRez',
          'subject' => 'One-time-pin'
        ),
        'information' => array(
          'pin' => $pin
        )
      ));
      $this->travelrezuser_model->update(
        [
          '_id' => new MongoId($user['_id'])
        ],
        [
          'otp' => $pin,
          'otp_hash' => $token,
          'otp_expires_at' => new MongoDate(strtotime('now +5 minutes'))
        ]
      );
      response(array(
        'message' => $this->lang->line('LOGIN'),
        'hash' => $token
      ), 200);
    }
  }
  /**
   * [otp description]
   * @return [type] [description]
   */
  public function otp()
  {
    _POST();
    
    $user = $this->travelrezuser_model->findOne(['otp_hash' => $_POST['hash'], 'otp' => $_POST['pin']]);
    if(!$user) {
      response(array(
        'errors' => array(
          'pin' => $this->lang->line('INVALID_PIN')
        )
      ), 422);
    } else {
      if(strtotime('now') > $user['otp_expires_at']->sec) {
        response(array(
          'errors' => array(
            'pin' => $this->lang->line('PIN_EXPIRED')
          )
        ), 422);
      } else {
        $token = random_string('alnum', 30);
        $expiresAt = new MongoDate(strtotime('now +120 minutes'));
        $this->travelrezuser_model->update([
          '_id' => $user['_id']
        ], [
          'api_token' => $token,
          'api_token_expires_at' => $expiresAt,
          'otp' => null,
          'otp_hash' => null,
          'otp_expires_at' => null
        ]);
        response(array(
          'token' => $token
        ));
      }
    }
  }
  /**
   * [check description]
   * @return [type] [description]
   */
  public function check()
  {
    $user = $this->travelrezuser_model->findOne(['api_token' => $_SERVER['HTTP_AUTHORIZATION']]);

    // log_message('debug', json_encode($_SERVER['HTTP_AUTHORIZATION']));
    if(!$user) {
      response(array(
        'message' => $this->lang->line('EXPIRED_TOKEN')
      ), 422);
    } else {
      if(strtotime('now') > $user['api_token_expires_at']->sec) {
        response(array(
          'message' => $this->lang->line('EXPIRED_TOKEN')
        ), 422);
      } else {
        response(array(
          'message' => $this->lang->line('CONTINUE')
        ));
      }
    }
  }
  /**
   * [discounts description]
   * @return [type] [description]
   */
  public function discounts()
  {
    if($this->authenticated()) {
      _POST();
      $this->load->model('travelrezdiscount_model');
      $discounts = $this->travelrezdiscount_model->find([], [], ['created_at' => -1]);

      response($discounts);
    }
  }
  /**
   * [discountCreate description]
   * @return [type] [description]
   */
  public function discountCreate()
  {
    if($this->authenticated()) {
      _POST();
      $this->load->model('travelrezdiscount_model');
      
      $discount = $this->travelrezdiscount_model->findOne(['code' => $_POST['code']]);
      if($discount) {
        response(array(
          'errors' => array(
            'code' => $this->lang->line('DISCOUNT_CODE_EXISTS')
          )
        ), 422);
      } else {
        $user = $this->getUser();
        $discount = [
          'code' => $_POST['code'],
          'agencies' => $_POST['agencies'],
          'startDate' => new MongoDate(strtotime($_POST['availabilityDate']['start'])),
          'endDate' => new MongoDate(strtotime($_POST['availabilityDate']['end'])),
          'category' => $_POST['category'],
          'type' => $_POST['type'],
          'value' => $_POST['value'],
          'pax' => $_POST['numberOfPax'],
          'products' => $_POST['products'],
          'message' => $_POST['message'],
          'created_by' => $user['_id'],
          'created_at' => new MongoDate(strtotime('now')),
        ];
        $this->travelrezdiscount_model->create($discount);
        response($discount);
      }
    }
  }
  /**
   * [discountDelete description]
   * @return [type] [description]
   */
  public function discountDelete()
  {
    $id = new MongoId($_GET['id']);
    if($this->authenticated()) {
      $this->load->model('travelrezdiscount_model');
      $this->travelrezdiscount_model->remove(['_id' => $id]);
    }
  }
}