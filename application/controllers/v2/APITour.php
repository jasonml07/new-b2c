<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class APITour extends CI_Controller
{

    protected $SESSION = [];
    protected $AGENCY = null;
    protected $PAYMENT_TYPES = [];
    protected $PAYMENT_TYPES_TEXT = [];

  function __construct()
  {
    parent::__construct();
    $this->PAYMENT_TYPES = json_decode(PAYMENT_TYPES, TRUE);
    $this->PAYMENT_TYPES_TEXT = json_decode(PAYMENT_TYPES_TEXT, TRUE);

    $getToken = isset($_GET['token']) ?$_GET['token'] :'';
    $sessionToken = isset($_SESSION['token']) ?$_SESSION['token'] :'';
    $agencyCode = isset($_GET['agency']) ?$_GET['agency'] : AGENCY_CODE;
    $SESSION = ['CODE' => $agencyCode];
    if(!!$getToken || !!$sessionToken) {
        if(!$getToken) {
            $getToken = $sessionToken;
        }
        $connection=new MongoClient();
        $db=$connection->db_system;
        $token=$db->token->findOne(array('_id'=>new MongoId($getToken)));
        if(!!$token) {
            $SESSION = $token;
        }
        $connection->close();
        $_SESSION['token'] = $getToken;
    }
    $AGENCY = _AGENCY( $agencyCode );
    // log_message('info', json_encode($this->AGENCY));
    if($this->AGENCY) {
        $SESSION['CODE'] = $AGENCY['agency_code'];
    }
    $this->SESSION = $SESSION;
    $this->AGENCY = $AGENCY;
    if(ENVIRONMENT == 'development') {
        log_message('info', 'agency');
        log_message('debug', json_encode($AGENCY));
        log_message('info', 'session');
        log_message('debug', json_encode($SESSION));
    }
  }
    /**
     * [detail description]
     * @return [type] [description]
     */
    public function detail()
    {
        ob_start();
        $status=200;
        $data = [];
        // log_message()
        if(empty( $this->SESSION ) || $this->AGENCY == null || empty($this->AGENCY)) {
            $status=400;
            $data = [
                'code' => 'AGENCY_NOT_EXISTS',
                'message' => 'Agency does not exist.'
            ];
        // } else if (! is_item_exists( 'products_inventory', 'productId', (int)$_GET['id'] )) {
        //     $status=400;
        //     $data = [
        //         'code' => 'PRODUCT_NOT_EXISTS',
        //         'message' => 'The product is not available.'
        //     ];
        } else {
            try {
                $this->load->model('externaltours_model');
                $_GET['agencyCode'] = $_GET['agency'];
                $product = $this->externaltours_model->getTour($_GET['id'], ( ! empty( $SESSION ) ));
                // log_message('debug', json_encode($product['productAvailabilityFinal']));
                if(count($product['productAvailabilityFinal']) == 0) {
                    $status=400;
                    $data = array(
                        'code' => 'NO_AVAILABILITY',
                        'message' => 'Product has no availability.'
                    );
                } else {
                    $departures = json_decode(DEPARTURE_MONTH, true);
                    $airlines = json_decode(AVAILABLE_AIRLINES, true);
                    $status=200;
                    $data = array(
                        'agency' => isset($_GET['agency']) ?$_GET['agency'] :'EET',
                        'departures' => $departures,
                        'airlines' => $airlines,
                        'product' => [
                            'id' => $product['productId'],
                            'name' => $product['productName'],
                            'summary' => $product['productSumarryDescription'],
                            'detailed' => $product['productDetailedDescription'],
                            'inclusion' => $product['productInclusionDescription'],
                            'exclusion' => $product['productExclusionDescription'],
                            'condition' => $product['productConditionsDescription'],
                        ],
                        'tours' => $product['productAvailabilityFinal']
                    );
                }
            } catch (Exception $e) {
                log_message('error', $e);
                $status=500;
                $data=array(
                    'code' => 'SERVER_ERROR',
                    'message' => 'Server error!'
                );
            }
        }
        $this->output
            ->set_content_type('application/json;charset=UTF-8')
            ->set_status_header($status)
            ->set_output(json_encode($data));
    }
    /**
     * [book description]
     * @return [type] [description]
     */
    public function book ()
    {
        // response(array(
        //     'code' => 'NO_AGENT',
        //     'message' => 'No agency was defined.'
        // ), 400);
        // return;
        if(empty($this->SESSION)) {
            response(array(
                'code' => 'NO_AGENT',
                'message' => 'No agency was defined.'
            ), 400);
        } else {
            $this->load->model('booking_model');
            $this->load->model('travelrezdiscount_model');
            $this->load->library('nativesession');
            $this->load->library('bookinglib');
            $this->nativesession->set('agencyRealCode', $this->SESSION['CODE']);
            $this->nativesession->set('agency_name', $this->AGENCY['agency_name']);

            _POST();
            $paymentMethod = $this->PAYMENT_TYPES[$_POST['paymentMethod']];
            $productId = $_POST['productId'];
            $tour = $_POST['tour'];
            $hasFlight = $_POST['hasFlight'];
            $flight = $_POST['flight'];
            $guests = $_POST['guests'];
            $voucher = $_POST['voucher'];
            $creditCard = $_POST['creditCard'];
            $beds = $_POST['beds'];
            $extrabills = $_POST['extrabills'];
            $packages = $_POST['packages'];
            $remainingDays = date_difference($tour['fromDayText']);
            $discount = $this->travelrezdiscount_model->findOne(['code' => $_POST['discountCode']]);
            if($discount) {
                if($this->travelrezdiscount_model->expired($discount)) {
                    $discount = null;
                }
            }

            $bookingId = $this->booking_model->newBooking(
                $this->SESSION['CODE'],
                TRUE,
                FALSE, // Pass booking ID when needed
                'tour',
                1,
                0,
                1,
                ($paymentMethod == 'BANKDEPOSIT' ?'Bank Deposit' :$creditCard),
                [
                    'productId' => $productId,
                    'tour' => $tour,
                    'paymentMethod' => $paymentMethod,
                    'guests' => $guests,
                    'voucher' => $voucher,
                    'beds' => $beds,
                    'extrabills' => $extrabills,
                    'packages' => $packages,
                    'creditCard' => $creditCard
                ]
            );

            $connections = new MongoClient();
            $db = $connections->db_system;
            $product = $db->products_inventory->findOne(
                array('productId' => (int)$productId), array('_id' => 0)
            );
            $connections->close();
            $dateDiffStart = date_create( $tour['fromDayText'] );
            $dateDiffEnd = date_create( $tour['toDayText'] );
            $dateDiff = date_diff( $dateDiffStart, $dateDiffEnd );

            // log_message('debug', $paymentMethod);

            $item = $this->_roomItem(
                $bookingId,
                $product,
                $tour,
                $voucher,
                $dateDiff,
                $guests,
                $paymentMethod
            );

            $costing = [
                'fromCurrency' => '',
                'toCurrency' => '',
                'basePrice' => 0,
                'convertedPrice' => 0,
                'grossPrice' => 0,
                'rate' => 0,
                'netAmnt' => 0,
                'markupPercentAmnt' => 0,
                'commissionPercentAmnt' => 0,
                'discountPercent' => 0
            ];
            $inclusions = '';
            foreach($beds as $index => $bed) {
                $inclusions .= $bed['quantity'].' x '.$bed['name'].'<br />';
                if(!$costing['fromCurrency']) {
                    $costing['fromCurrency'] = $bed['costing']['fromCurrency'];
                }
                if(!$costing['toCurrency']) {
                    $costing['toCurrency'] = $bed['costing']['toCurrency'];
                }
                if(!$costing['rate']) {
                    $costing['rate'] = $bed['costing']['rate'];
                }
                if(!$costing['markupPercentAmnt']) {
                    $costing['markupPercentAmnt'] = $bed['costing']['markupPercentAmnt'];
                }
                if(!$costing['commissionPercentAmnt']) {
                    $costing['commissionPercentAmnt'] = $bed['costing']['commissionPercentAmnt'];
                }
                $costing['basePrice'] += $bed['costing']['basePrice'] * $bed['quantity'];
                $costing['convertedPrice'] += $bed['costing']['convertedPrice'] * $bed['quantity'];
                $costing['grossPrice'] += $bed['costing']['grossPrice'] * $bed['quantity'];
                $costing['netAmnt'] += $bed['costing']['netAmnt'] * $bed['quantity'];
            }
            
            
            $totalCosting = item_costing(
                $costing['convertedPrice'],
                $costing['commissionPercentAmnt'],
                $costing['markupPercentAmnt'],
                $this->travelrezdiscount_model->percent($costing['grossPrice'], $discount)
            );
            $item['itemInclusionDescription'] = $inclusions;
            $item['itemCostings'] = $this->_costing(array_merge($costing, $totalCosting));
            $cancellations = $this->_cancellation(
                $paymentMethod,
                $item['itemCostings'],
                $tour
            );
            $item['itemCancellation'] = $cancellations['cancellations'];
            $item['itemStartTime'] = $cancellations['startTime'].':00';
            $item['itemEndTime'] = $cancellations['endTime'].':00';

            $item = $this->bookinglib->buildItemArray($item);
            $response = $this->_executeAddItemToBooking($item);
            // log_message('debug', json_encode($response));
            
            if(count($extrabills)) {
                foreach($extrabills as $index => $extrabill) {
                    $subItem = $item;
                    $subItem['itemName'] = $extrabill['name'];
                    $subItem['itemServiceCode'] = 'Service Fee';
                    $subItem['itemServiceName'] = 'Service Fee';
                    $subItem['itemSupplements'] = array();
                    $subConsting = $extrabill['costing'];
                    $subConsting['basePrice'] = $subConsting['basePrice']*$extrabill['quantity'];
                    $subConsting['convertedPrice'] = $subConsting['convertedPrice']*$extrabill['quantity'];
                    $subConsting['grossPrice'] = $subConsting['grossPrice']*$extrabill['quantity'];
                    $subConsting['netAmnt'] = $subConsting['netAmnt']*$extrabill['quantity'];

                    $subItem['itemCostings'] = $this->_costing(
                        array_merge(
                            $subConsting,
                            item_costing(
                                $subConsting['convertedPrice'],
                                $subConsting['commissionPercentAmnt'],
                                $subConsting['markupPercentAmnt'],
                                $this->travelrezdiscount_model->percent($subConsting['grossPrice'], $discount)
                            )
                        )
                        
                    );
                    // log_message('debug', json_encode($subItem));
                    $this->bookinglib->buildItemArray( $subItem );
                    $this->bookinglib->executeAddItemToBooking( $subItem );
                }
            } // end of extra bills

            // log_message('debug', 'Add On/Packages:: '.count($packages));
            // if(count($packages)) {
            //     foreach($packages as $index => $package) {
            //         $subItem = $item;
            //         $subItem['itemName'] = $package['name'];
            //         $subItem['itemServiceCode'] = 'Service Fee';
            //         $subItem['itemServiceName'] = 'Service Fee';
            //         $subItem['itemSupplements'] = array();
            //         $subItem['itemCostings'] = $this->_costing(
            //             array_merge(
            //                 $package['costing'],
            //                 item_costing(
            //                     $package['costing']['convertedPrice'],
            //                     $package['costing']['commissionPercentAmnt'],
            //                     $package['costing']['markupPercentAmnt']
            //                 )
            //             )
                        
            //         );
            //         log_message('debug', json_encode($subItem));
            //         $this->bookinglib->buildItemArray( $subItem );
            //         $this->bookinglib->executeAddItemToBooking( $subItem );
            //     }
            // } // end of extra bills
            $dateCreated = strtotime('now');
            // FLIGHT ITEM
            if($hasFlight) {
                // log_message('debug', 'Creating flight item. . .');
                $totalPrice = $flight['price'] * $flight['passengers'];
                $dateDiffStart = date_create( date('Y-m-d', strtotime(str_replace('/', '-', $flight['departureDate']))) );
                $dateDiffEnd   = date_create( date('Y-m-d', strtotime(str_replace('/', '-', $flight['returnDate']))) );
                $_dateDiff     = date_diff( $dateDiffStart, $dateDiffEnd );

                $flightItem = $item;
                $flightItem['itemName']        = 'Tour Flight - ' . $flight['departureCity'];
                $flightItem['itemServiceCode'] = 'FLT';
                $flightItem['itemServiceName'] = 'Flights';
                $flightItem['itemStatus']      = 'Quote';
                $flightItem['itemIsConfirmed'] = 0;

                $flightItem['itemStartDate'] = $flight['departureDate'];
                $flightItem['itemEndDate']   = $flight['returnDate'];
                $flightItem['itemStartTime'] = '12:00:00';
                $flightItem['itemEndTime']   = '12:00:00';
                $flightItem['itemNumDays']   = $_dateDiff->days;
                $flightItem['itemNumNights'] = $_dateDiff->days;
                
                $flightItem['itemFromCity']  = $flight['departureCity'];
                $flightItem['itemSupplements']=array();
                $flightItem['itemCostings'] = array(
                    'fromCurrency'      => 'AUD',
                    'toCurrency'        => 'AUD',
                    'toRate'            => 1,
                    
                    'originalAmount'    => (float)$totalPrice,
                    'orginalNetAmt'     => 0,
                    'grossAmt'          => (float)$totalPrice,
                    
                    'netAmountInAgency' => 0,
                    'markupInAmount'    => 0,
                    'markupInPct'       => 0,
                    
                    'commInPct'         => 0,
                    'commInAmount'      => 0,
                    'commAmt'           => 0,
                    
                    'totalLessAmt'      => (float)0,
                    'totalDueAmt'       => (float)$totalPrice,
                    'totalProfitAmt'    => (float)0,

                    'discInPct'         => 0,
                    'discInAmount'      => 0,
                    'discAmt'           => 0
                );
                $flightItem['itemCostings']['discInPct']=($flightItem['itemCostings']['discInAmount']/$flightItem['itemCostings']['originalAmount'])*100;
                $flightItem['itemCostings']['totalLessAmt']=$flightItem['itemCostings']['commInAmount']+$flightItem['itemCostings']['discInAmount'];
                $flightItem['itemCostings']['totalDueAmt']=$flightItem['itemCostings']['grossAmt']-$flightItem['itemCostings']['totalLessAmt'];
                // $flightItem['itemCostings']['totalProfitAmt']=$flightItem['itemCostings']['markupInAmount']-$flightItem['itemCostings']['totalLessAmt'];

                $flightItem  = $this->bookinglib->buildItemArray( $flightItem );
                // log_message('debug', 'Executing flight item. . .');
                $this->bookinglib->executeAddItemToBooking( $flightItem );
            }
            $this->bookinglib->init( $bookingId, $response['item_id'] );
            
            $bookingGuests = array();
            foreach($guests as $index => $guest) {
                array_push($bookingGuests, $this->bookinglib->buildGuest($guest['title'], $guest['firstName'], $guest['lastName'], 33, $this->SESSION['CODE']));
            }
            $paxIds = $this->bookinglib->executeItemGuests( $bookingGuests );
            // log_message('info', 'Executed guests.');
            // log_message('debug', json_encode($paxIds));
            $allocatedGuest = $this->bookinglib->executeAllocateGuests( $paxIds['paxIds'] );
            // log_message('info', 'Guests allocated');
            // log_message('debug', json_encode($allocatedGuest));
            // log_message('info', 'Execute Itinerary');
            $this->bookinglib->executeItirenary(  );
            // log_message('info', 'Execute Voucher');
            $this->bookinglib->executeVoucher(  );
            // log_message('debug', SYS_ADMIN_IP);
            // log_message('debug', json_encode($item));
            // log_message('debug', json_encode($response));
            if($hasFlight) {
                // Send flight mail
                // log_message('debug', 'Sending flight mail. . .');
                $this->bookinglib->sendFlighInfo([
                    'created_at' => date_format(date_create("now"),"F j, Y"),
                    'discount' => 0,
                    'subtotal' => $totalPrice,
                    'city' => $flight['departureCity'],
                    'class' => $flight['preferredClass'],
                    'departure' => $flight['departureDate'],
                    'return' => $flight['returnDate'],
                    'quantity' => $flight['passengers'],
                    'VOUCHER'  => [
                        'firstname' => $voucher['firstName'],
                        'lastname' => $voucher['lastName'],
                        'email' => $voucher['emailAddress'],
                        'phonenum' => $voucher['phoneNumber']
                   ]
                ]);
            }
            log_message('debug', 'Sending voucher mail. . .');
            $voucherMail = [
                'emailTo' => $voucher['emailAddress'],
                'content' => $paymentMethod == 'E-NETT' ?'emails/tour/payment/e-nett'
                    : ($paymentMethod == 'CREDITCARD' ?'emails/tour/payment/credit-card' :'emails/tour/payment/booked'),
                'bookType' => 'External Tours',
                'voucher' => [
                    'name' => $voucher['firstName'].' '.$voucher['lastName'],
                    'email' => $voucher['emailAddress']
                ],
                'payment' => [
                    'type' => $this->PAYMENT_TYPES_TEXT[$paymentMethod],
                    'paid' => 0,
                    'receiptId' => 0,
                ],
                'pax' => [
                    'adults' => count($guests)
                ],
                'data' => [
                    'voucherName' => $voucher['firstName'],
                    'paidAmount' => $totalCosting['gross_amount'],
                    'payment_type' => $this->PAYMENT_TYPES_TEXT[$paymentMethod],
                    'isBooked' => TRUE,
                    'startDate' => $remainingDays < 67 ?date('M d, Y l', strtotime($tour['fromDayText'].' -15 day')) :date('M d, Y l', strtotime('now +7 day')),
                    'isFullPayment' => $remainingDays < 67,
                    'isPayable' => false,
                    'costing' => [
                        'commission' => $totalCosting['commission_amount'],
                        'due_amount' => $totalCosting['due_amount'],
                        'gross_amount' => $totalCosting['gross_amount']
                    ],
                    'isAgent' => isset($this->SESSION['ID']),
                    'agencyCode' => $this->SESSION['CODE'],
                    'PAYMENT_TYPE' => $paymentMethod,
                    'TODAYS_DATE' => date('Y-m-d H:i:s', strtotime('now')),
                    'agencyName' => $this->AGENCY['agency_name'],
                    'productType' => 'Tour',
                    'returnFlight' => $flight
                ]
            ];
            $AGENCY_CODENAMES = json_decode(AGENCY_CODENAMES, TRUE);
            if($voucherMail['data']['isAgent'] && !!$AGENCY_CODENAMES[$voucherMail['data']['agencyCode']]) {
                $voucherMail['data']['agencyName'] = $AGENCY_CODENAMES[$voucherMail['data']['agencyCode']];
            }
            // log_message('info', 'Voucher mail data!');
            // log_message('debug', json_encode($voucherMail));
            $this->bookinglib->sendVoucher($voucherMail, false, $this->SESSION['CODE']=='EET');
            // Must send voucher mail

            // Save booking
            $this->load->model('tour_model');
            $reference = $this->tour_model->getBookRef();
            $booked = $this->tour_model->saveBooking([
                'reference' => $reference,
                'bookingId' => $bookingId,
                'agencyCode' => $this->AGENCY['agency_code'],
                'agencyName' => $this->AGENCY['agency_name'],
                'consultantName' => isset($this->SESSION['name']) ?$this->SESSION['name'] :'',
                'consultantId' => isset($this->SESSION['name']) ?$this->SESSION['name'] :'',
                'voucherTitle' => $voucher['title'],
                'voucherFirstname' => $voucher['firstName'],
                'voucherLastname' => $voucher['lastName'],
                'voucherEmailAddress' => $voucher['emailAddress'],
                'voucherPhoneNumber' => $voucher['phoneNumber'],
                'paymentMethod' => $paymentMethod,
                'bookedDate' => new MongoDate(strtotime("now"))
            ]);
            $this->tour_model->saveBookTour(array_merge($tour, ['bookedId' => $booked['_id']]));
            $this->tour_model->saveBookProduct([
                'bookedId' => $booked['_id'],
                'id' => $product['productId'],
                'name' => $product['productName'],
                'tag' => $product['productTag'],
                'serviceCode' => $product['productServiceCode'],
                'serviceName' => $product['productServiceName'],
                'supplierCode' => $product['productSupplierCode'],
                'supplierName' => $product['productSupplierName'],
                'summaryDescription' => $product['productSumarryDescription'],
                'detailedDescription' => $product['productDetailedDescription'],
                'inclusionDescription' => $product['productInclusionDescription'],
                'exclusionDescription' => $product['productExclusionDescription'],
                'conditionDescription' => $product['productConditionsDescription'],
                'fromCountry' => $product['productFromCountry'],
                'fromCity' => $product['productFromCity'],
                'fromAddress' => $product['productFromAddress'],
                'toCountry' => $product['productToCountry'],
                'toCity' => $product['productToCity'],
                'toAddress' => $product['productToAddress'],
            ]);
            $beds = array_map(function ($item) use ($booked) {
                $item['bookedId'] = $booked['_id'];
                return $item;
            }, $beds);
            $this->tour_model->saveBookBeds($beds);
            $extrabills = array_map(function($item) use ($booked) {
                $item['bookedId'] = $booked['_id'];
                return $item;
            }, $extrabills);
            $this->tour_model->saveBookExtraBills($extrabills);
            $guests = array_map(function($item) use ($booked) {
                $item['bookedId'] = $booked['_id'];
                return $item;
            }, $guests);
            $this->tour_model->saveBookGuests($guests);
            if($hasFlight) {
                $this->tour_model->saveBookFlight(array_merge($flight, ['bookedId' => $booked['_id']]));
            }
            response(array(
                'code' => 'SUCCESS',
                'message' => 'Booking was successful!',
                'reference' => $reference,
                'response' => $response
            ), 200);
        }
    }
    /**
     * [_roomItem description]
     * @param  [type] $bookingId   [description]
     * @param  [type] $product     [description]
     * @param  [type] $tour        [description]
     * @param  [type] $voucher     [description]
     * @param  [type] $dateDiff    [description]
     * @param  [type] $guests      [description]
     * @param  [type] $paymentType [description]
     * @return [type]              [description]
     */
    private function _roomItem($bookingId, $product, $tour, $voucher, $dateDiff, $guests, $paymentType)
    {
        $item = array();
        $item['bookingId']       = $bookingId;
        $item['itemId']          = 'New';
        $item['itemName']        = $product['productName'];
        $item['itemCreatedType'] = 'DB - External Tours';
        $item['itemCode']        = "ET-" . $product['productId'];
        $item['itemServiceCode'] = $product['productServiceCode'];
        $item['itemServiceName'] = $product['productServiceName'];

        $item['itemSupplierCode'] = $product['productSupplierCode'];
        $item['itemSupplierName'] = $product['productSupplierName'];

        // $item['itemStatus']              = ( $_POST['payment_type'] == 'PAYPAL' ) ? 'Confirmed' : 'Quote';
        $item['itemStatus']              = 'Quote';
        $item['itemSuplierConfirmation'] = ( isset( $_payment['reference'] )? $_payment['reference']: '' );
        // $item['itemSuplierOtherConfirmation'] = date('d F, Y', strtotime('now'));

        $item['itemFromCountry'] = $product['productFromCountry'];
        $item['itemFromCity']    = $product['productFromCity'];
        $item['itemFromAddress'] = $product['productFromAddress'];
        $item['itemToCountry']   = $product['productToCountry'];
        $item['itemToCity']      = $product['productToCity'];
        $item['itemToAddress']   = $product['productToAddress'];
        
        $item['itemStartDate']     = date('d/m/Y', strtotime($tour['fromDayText']) );
        $item['itemEndDate']       = date('d/m/Y', strtotime($tour['toDayText']) );
        $item['itemStartTime']     = '12:00:00';
        $item['itemEndTime']       = '12:00:00';
        $item['itemNumDays']       = $dateDiff->days;
        $item['itemNumNights']     = $dateDiff->days;
        $item['itemIsLeadPax']     = 1;
        $item['itemIsFullPax']     = 1;
        $item['itemIsVoucherable'] = 1;
        $item['itemIsAutoInvoice'] = 1;
        $item['itemIsInvoicable']  = 1;
        $item['itemIsIterary']     = 1;
        $item['itemAdultMin']      = 1;
        $item['itemAdultMax']      = count( $guests );

        #$item['itemChildMax']       = ( ( isset( $_passengers['childs'] ) )? count( $_passengers['childs'] ): 0 );
        $item['itemIsPaxAllocated'] = 1;
        $item['itemIsLive']         = 1;
        // $item['itemIsConfirmed']    = ( $_POST['payment_type'] == 'PAYPAL' ) ? 1 : 0;
        $item['itemIsConfirmed']    = 0;
        $item['application']        = _APPCODE('ET');
        $item['PAYMENT_TYPE']       = $paymentType;
        $item['VOUCHER_EMAIL']      = $voucher['emailAddress'];

        $item['itemSumarryDescription']    = $product['productSumarryDescription'];
        $item['itemDetailedDescription']   = $product['productDetailedDescription'];
        $item['itemExclusionDescription']  = $product['productExclusionDescription'];
        $item['itemConditionsDescription'] = $product['productConditionsDescription'];
        // $item['itemInclusionDescription']  = $_POST['inclusion'];
        $item['itemInclusionDescription']  = '';

        $item['itemCancellation'] = array();

        return $item;
    }
    /**
     * [_cancellation description]
     * @param  [type] $paymentType [description]
     * @param  [type] $costing     [description]
     * @param  [type] $tour        [description]
     * @return [type]              [description]
     */
    private function _cancellation($paymentType, $costing, $tour)
    {
        $cancellations = array();
        $startTime = '12:00:00';
        $endTime = '12:00:00';
        if($paymentType == 'BANKDEPOSIT') {
            $startDate = strtotime( 'now' );
            $endDate = strtotime( 'now +1 day' );
            array_push( $cancellations, array(
                'dateFrom'         => date('Y-m-d', $startDate),
                'dateTo'           => date('Y-m-d', $endDate),
                'typeCancellation' => 'Percentage',
                'percent'          => 100,
                'amount'           => (float)$costing['totalDueAmt']
            ));
            $startTime = date('H:i', $startDate);
            $endTime = date('H:i', $endDate);
        } else {
            $remainingDays = date_difference( $tour['fromDayText'] );
            if($remainingDays >= 67) {
                array_push( $item['itemCancellation'], array(
                    'dateFrom'         => date('Y-m-d', strtotime( 'now' )),
                    'dateTo'           => date('Y-m-d', strtotime( 'now +7 day' )),
                    'typeCancellation' => 'Percentage',
                    'percent'          => 30,
                    'amount'           => ((float)$costing['totalDueAmt'] * 0.30)
                ));
            }
            array_push( $item['itemCancellation'], array(
                'dateFrom'         => date('Y-m-d', strtotime( 'now +7 day' )),
                'dateTo'           => $tour['fromDayText'],
                'typeCancellation' => 'Percentage',
                'percent'          => 100,
                'amount'           => (float)$costing['totalDueAmt']
            ));
        }

        return array(
            'cancellations' => $cancellations,
            'startTime' => $startTime,
            'endTime' => $endTime
        );
    }
    /**
     * [_costing description]
     * @return [type] [description]
     */
    private function _costing($totalCosting)
    {
        // log_message('debug', json_encode($totalCosting));
        return [
            'fromCurrency'      => $totalCosting['fromCurrency'],
            'toCurrency'        => $totalCosting['toCurrency'],
            'toRate'            => (float)$totalCosting['rate'],
            
            'originalAmount'    => (float)$totalCosting['basePrice'],
            'orginalNetAmt'     => (float)$totalCosting['net_amount_agency'],
            'grossAmt'          => (float)$totalCosting['grossPrice'],
            
            'netAmountInAgency' => (float)$totalCosting['net_amount_agency'],
            'markupInAmount'    => (float)$totalCosting['markup_amount'],
            'markupInPct'       => (float)$totalCosting['markup_pct'],
            
            'commInPct'         => (float)$totalCosting['commission_pct'],
            'commInAmount'      => (float)$totalCosting['commission_amount'],
            'commAmt'           => (float)$totalCosting['commission_amount'],
            
            'totalLessAmt'      => (float)$totalCosting['total_less_amount'],
            'totalDueAmt'       => (float)$totalCosting['due_amount'],
            'totalProfitAmt'    => (float)$totalCosting['profit_amount'],

            'discInPct'         => (float)$totalCosting['discount_pct'],
            'discInAmount'      => (float)$totalCosting['discount_amount'],
            'discAmt'           => (float)$totalCosting['discount_amount']
        ];
    }
    /**
     * [_executeAddItemToBooking description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    private function _executeAddItemToBooking($data)
    {
        $code='EET';
        switch ($this->SESSION['CODE']) {
          case 'EET':
            $code=$this->SESSION['CODE'];
            break;
          case 'EuropeTravelDeals':
            $code='EH';
            break;

          case 'ifly':
            $code='ifly';
            break;
        }
        $data['descType']=$code;
        $params = array('action' => 'addItemToBooking', 'postDetails' => json_encode($data));
        $result = $this->_cURL('execute_item_manager', $params);
      
      return $result;
    }
    /**
     * [_cURL description]
     * @param  [type] $url       [description]
     * @param  [type] $params    [description]
     * @param  string $customURL [description]
     * @return [type]            [description]
     */
    private function _cURL($url, $params, $customURL = '')
    {
        $mainURL = SYS_ADMIN_IP . '_ajax/' . $url . '.php?agency_code=' . $this->SESSION['CODE'];
        if(!!$customURL) {
            $mainURL = $customURL;
        }
        log_message('debug', $mainURL);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $mainURL); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            log_message('error', $error_msg);
        }
        log_message('debug', $result);
        $result = json_decode($result, TRUE);
        curl_close($ch);
        return $result;
    }
    /**
     * [bookingDetail description]
     * @return [type] [description]
     */
    public function bookingDetail()
    {
        if(!isset($_GET['ref'])) {
            response([
                'code' => 'REFERENCE_NOT_DEFINED',
                'message' => 'Invalid reference!'
            ], 400);
        } else {
            $this->load->model('tour_model');
            $bookingDetails = $this->tour_model->getBookingDetails($_GET['ref']);
            if(!$bookingDetails) {
                response([
                    'code' => 'BOOKING_DETAIL_NOT_EXISTS',
                    'message' => 'Booking details not exists!'
                ], 400);
            } else {
                response($bookingDetails, 200);
            }
        }
    }
    /**
     * [discountCheck description]
     * @return [type] [description]
     */
    public function discountCheck()
    {
        _POST();
        $code = $_POST['code'];
        $this->load->model('travelrezdiscount_model');
        $this->lang->load('tour');
        $discount = $this->travelrezdiscount_model->findOne(['code' => $code]);

        if(!$discount) {
            response(array(
                'message' => $this->lang->line('DISCOUNT_NOT_EXISTS')
            ), 403);
        } else {
            if($this->travelrezdiscount_model->expired($discount)) {
                response(array(
                    'message' => $this->lang->line('DISCOUNT_NOT_AVAILABLE')
                ), 403);
            } else {
                response($discount);
            }
        }
    }
}