<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$s_id = session_id();
if(empty($s_id)) {
	session_start();
}
/**
 * 
 */
class Dashboard extends CI_Controller {

	
	public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('PaymentWestpac', 'paymentwestpac');
        $this->load->model(array(
            'xml_model', 'result_model', 'hotel_model', 'agency_model', 'booking_model', 'notification_model'
        ));
        $this->load->library('session');
        
    }

    /**
     * [reservationDetails description]
     * @param  [type] $reservationid [description]
     * @return [type]                [description]
     */
    public function reservationDetails ( $reservationid ) {

		$getCurrencyExchanged = $this->hotel_model->getConvertCurrencyEx();
		$xml = $this->xml_model->reservationDetail($reservationid);
		$xml = wrapp_xml($xml);
		$xml = xml_to_array($xml);


		// Check first if the reservation already exist
		// Convert the price with its price
		///////////////////////////////////////
		// TODO                              //
		// 1. Insert the Booking information //
		// 2. Add the items                  //
		// 3. Guest on the items             //
		///////////////////////////////////////
		if( isset($xml['response'][0]['content']['reservation']) ) {
			$reservations = $xml['response'][0]['content']['reservation'];

			foreach ($reservations as $key => $reservation) {
				if( isset($reservation['content']['item']) ) {
					$items = $reservation['content']['item'];

					foreach ($items as $keyItem => $item) {
						$booking = $item['content']['booking'];
						$price = ((float)$booking[0]['content']['price'][0]['amount']) * $getCurrencyExchanged[ $booking[0]['content']['price'][0]['currency'] ];
						$booking[0]['content']['price'][0]['new_amount']   = $price;
						$booking[0]['content']['price'][0]['new_currency'] = CONVERTION_CURRENCY;
						$booking[0]['content']['price'][0]['calculation']  = $this->hotel_model->calculateTotalCost( $price, $booking[0]['content']['price'][0]['currency'] );

						$item['content']['booking'] = $booking;
						$items[$keyItem] = $item;
					}
					$reservation['content']['item'] = $items;
					$reservations[$key] = $reservation;
				}
			}
			$xml['response'][0]['content']['reservation'] = $reservations;
		}
		$data['booker_info'] = $xml;

		$this->load_view('customer-success-booking', $data);
	}


    ########## PAGES VIEWS ###############
	public function index()
	{
		
		//die();
		//print_r($_GET['agencyCode']);
		//die(); 
		$this->nativesession->set('SYSTEM', 'HOTEL');
		$agencycode = 'EET';
		if(!$this->session->userdata('agencycode')){
		//$this->load_view('dashboard/body');
		
			if( isset($_GET['agencyCode']) ) {
				$agencycode = $_GET['agencyCode'];
			}
			
		}else{
			
		//	header('Location: '.base_url());
		//	redirect('dashboard');
		}
		$data = array();

		$data['searchErrors'] = $this->nativesession->get('searchErrors');
		$post = $this->nativesession->get('session_post');
		
		if( $data['searchErrors'] ) {
			switch ( intval($post['searchType']) ) {
				case 1:
					$data['hotel_post'] = $post;
					break;
				
				case 1:
					$data['flight_post'] = $post;
					break;
			}
		}

		$this->session->set_userdata('loginNotificationReference', $agencycode);
		$this->session->set_userdata('agencycode',$agencycode);

		$connections = new MongoClient();
        $db = $connections->db_agency;

		//$data['agencycode'] = base64_encode($agencycode);
		//$data = array();
		
		$this->session->set_userdata('result', $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($agencycode)),array("_id"=>0)));
		$connections->close();
		// $this->load_view('dashboard/body',$this->session->userdata('result'));
		// Width angularJS loaded
		$this->load_view('dashboard/body',$data, TRUE, array('app/hotel/app-search.js'));

	}
	/*public function execAgency($agencycode){
		$this->session->set_userdata('agencycode',$agencycode);
		$connections = new MongoClient();
        $db = $connections->db_agency;

		$data['agencycode'] = base64_encode($agencycode);
		//$data = array();
		
		$data['result'] = $db->agency_b2c_setup->findOne(array("agency_code"=>base64_encode($agencycode)),array("_id"=>0));

		//print_r($data);
		//die();
		$this->load->view('dashboard/titlehead',$data); 
		$this->load->view('dashboard/header',$data); 
		$this->load->view('dashboard/body', $data);
		$this->load->view('dashboard/footer');
	}*/
	public function theme () {
		$this->load->view('dashboard/titlehead', array("theme" => $this->uri->segment(2)));
		$this->load->view('dashboard/header'); 
		$this->load->view('dashboard/body');
		$this->load->view('dashboard/footer');
	}
	public function bookings(){
		$this->load_view('bookings/body',array());
		//$this->load_view('bookings/body',$data ); 
	}
	public function load_view($viewFilename,$data=array(), $hasAngular = FALSE, $angularJS = null){
		

		$this->load->view('dashboard/titlehead',$this->session->userdata('result')); 
		$this->load->view('dashboard/header',$this->session->userdata('result')); 
		$this->load->view($viewFilename, $data);


		$footerData = array();
		$footerData['hasAngular'] = $hasAngular;
		$footerData['angularJSs'] = ((is_string($angularJS))?array($angularJS):((is_array($angularJS))?$angularJS:array()));

		$this->load->view('dashboard/footer', $footerData);

	
    
	}
	public function bookingDetails($bookingID){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		//print_r($result);
		$data = array();

		$data['BookingData'] = $result['bookingData'];
		$data['ItemDatas'] = $result['itemDatas'];
		$data['ReceiptDatas'] = $result['receiptDatas'];
		$data['CostingsDatas'] = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/details',$data); 
	}
	public function itemDetails($bookingID,$itemId){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleItemDetails($bookingID,$itemId);
		$data = array("item"=>$result);
		$this->load_view('bookings/item_details',$data); 
	
		
	}
	public function bookingPayments($bookingID){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');


		$data = array();

		$data['enetSuccess'] = false;
		if(isset($_POST['submit'])){
			if($_POST['type'] == 'enet'){
				

				$checkEnet = $this->booking_model->submitEnetReceipt($agencyCode,(int)$bookingID,$_POST);
				if($checkEnet){

					$data['enetSuccess'] = true;

				}
			}
			if($_POST['type'] == 'bank'){
				$_SESSION['paymentData'] = $_POST;
				redirect('dashboard/paymentVerification/'.$bookingID);
			}
			
		}

		
		

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		//print_r($result);
		



		
		$data['BookingData']    = $result['bookingData'];
		$data['ItemDatas']      = $result['itemDatas'];
		$data['ReceiptDatas']   = $result['receiptDatas'];
		$data['CostingsDatas']  = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/payments',$data); 
	}
	public function paymentVerification($bookingID){
		$this->load->model('booking_model');
		$agencyCode = $this->session->userdata('agencycode');


		$data = array();

		

		
		$checBooking = $this->booking_model->checkOwnBooking($agencyCode,$bookingID);
		if(!$checBooking){
			header("Location: ".base_url()."dashboard/bookings");
		}

		$result = $this->booking_model->getSingleBookingDetails($agencyCode,$bookingID);
		//print_r($result);
		

		$data['pay_data'] = $_SESSION['paymentData'];


		$data['BookingData']    = $result['bookingData'];
		$data['ItemDatas']      = $result['itemDatas'];
		$data['ReceiptDatas']   = $result['receiptDatas'];
		$data['CostingsDatas']  = $result['costingsDatas'];
		$data['ItineraryDatas'] = $result['itineraryDatas'];
		$this->load_view('bookings/paymentCCverify',$data); 
	}
	public function hotel(){
		$data = array();
		$this->load_view('hotel/searchhotel',$data); 
	}
	public function hotelDetails(){
		$data = array();
		$this->load_view('hotel/searchdetails',$data); 
	}
	public function flight(){
		$data = array();
		$this->load_view('flight/searchflight',$data); 
	}
	public function flightDetails(){
		$data = array();
		$this->load_view('flight/searchdetails',$data); 
	}



	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
