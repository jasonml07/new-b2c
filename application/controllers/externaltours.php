<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Externaltours extends CI_Controller {

	
    public function __construct()
    {
        parent::__construct();
    }  
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		#echo '<script>console.log(' . json_encode(_AGENT_INFO( $this->nativesession->get('agencyRealCode') )) . ')</script>';
		$this->nativesession->set('SYSTEM', 'TOURS');
		if( isset( $_GET['agencyCode'] ) )
		{
			$this->nativesession->delete('ETLOGIN');
		}
		$SESSION = $this->nativesession->get('ETLOGIN');
		if (!isset($_GET['agencyCode'])) {
			if (defined('AGENCY_CODE')) {
				$_GET['agencyCode']=AGENCY_CODE;
			}
		}
		if (isset($_GET['token'])) {
			$connection=new MongoClient();
			$db=$connection->db_system;
			$SESSION=$db->token->findOne(array('_id'=>new MongoId($_GET['token'])));
			if (isset($_GET['agencycode'])) {
				$SESSION['CODE']=$_GET['agencycode'];
			}
			$this->nativesession->timeout_set('ETLOGIN', $SESSION);
			$connection->close();
		}
		$_AGENCY = _AGENCY( (isset($_GET['agencyCode'])?$_GET['agencyCode']:$SESSION['CODE']) );
		if ( (! isset( $_GET['agencyCode'] ) && empty( $SESSION )) || $_AGENCY == null || empty( $_AGENCY ))
		{
			?>
			Agency does not exist.
			<br>
			If the page is not redirected. <a href="<?=base_url()?>">Click here</a>
			<?php
			d_redirect('');
			exit;
		}
		$paramAGENCYCODE = ( isset( $_GET['agencyCode'] ) )? $_GET['agencyCode']: $SESSION['CODE'];

		if ( empty( $paramAGENCYCODE ) ){
			# Code here
			show_404();
			exit;
		} // end of if statement

		if ( ! is_item_exists( 'products_inventory', 'productId', (int)$_GET['id'] ) )
		{
			$message='The product is not available.';
			if ($paramAGENCYCODE=='EET') {
				$message='The product is not available.';
			}
			echo $message;
			echo "<br />";
			echo 'This page will be redirected to your home page :)';

			d_redirect('');
		} // end of if statement
		$this->load->model('externaltours_model');

		//print_r($resTours);
		$data = array();
		
		$resTours = $this->externaltours_model->getTour($_GET['id'], ( ! empty( $SESSION ) ));

		if ( empty($resTours['productAvailabilityFinal']) )
		{
			# Code here
			echo 'There are no available products to be book. <br>' . PHP_EOL;
			echo '<a href="' . base_url() . '">Click here</a> to return to homepage.';
			exit;
		} // end of if statement

		$agencycode = base64_encode($paramAGENCYCODE);
		$this->session->set_userdata('agencycode',$paramAGENCYCODE);
		$this->nativesession->set('agencyRealCode', $paramAGENCYCODE);

		$connections = new MongoClient();
        $db = $connections->db_agency;

        $agencyRes = $db->agency_b2c_setup->findOne(array("agency_code"=>( ( ! empty( $SESSION )? base64_encode('EET'): $agencycode ) )),array("_id"=>0));
        //$data['agencyData'] = $this->session->userdata('result');
		$whereData['agency_code'] = ( ( ! empty( $SESSION )? base64_encode('EET'): $agencycode ) );
		$res = $db->agency_menu_setup->find($whereData,array("_id"=>0));
        $arrMenu = iterator_to_array($res);
		$result = $this->treeview($arrMenu);
        #print_r($result);
        #die();

		$data['menu'] = $result;
        $data['agency'] = _AGENT_INFO($this->nativesession->get('agencyRealCode'));
        $data['product'] = $resTours;
        
		$data['SESSION']   = $SESSION;
		$data['DISCOUNTS'] = json_decode(DISCOUNTS, TRUE);
#print_r($resTours['productAvailabilityFinal']);
#die();
		$db_system = $connections->db_system;
		$agency    = $db_system->agency->findOne(array('agency_code' => $paramAGENCYCODE), array('_id' => 0));
        $this->nativesession->set('agency_name', $agency['agency_name']);
        $connections->close();

        $SESSION = $this->nativesession->get('ETLOGIN');
        $data['IS_AGENT'] = $SESSION && is_array( $SESSION );
        #$data['IS_AGENT'] = TRUE;

		#$this->nativesession->set('agency_name', );
		#print_r($data);
		#die();
		$this->load_view('externalTours/body',$data);

	}

	public function template ( $fileName )
	{
		# Code here
		return $this->load->view( 'externalTours/template/' . $fileName, TRUE );
	} // end of template

    /**
     * [validate_promo description]
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function validate_promo ( $code )
    {
    	# Code here
    	$discounts = json_decode(DISCOUNTS, TRUE);
    	$discount = array();
    	$_discounts=array();
    	foreach ($discounts as $key => $item) {
    		$_discounts[strtolower($key)]=$item;
    	}
    	$discounts=$_discounts;
    	try{
    		/*if (in_array(strtolower($code), $discountKeys)===FALSE) {
    			throw new Exception("Code is not available", 1);
    		}*/
    		if ( !isset($discounts[ strtolower($code) ]) ){
	    		throw new Exception("Code not found", 1);
	    	}
	    	$discount = $discounts[ strtolower($code) ];
    		if ( is_in_range_date( $discount['availabilities']['from'], $discount['availabilities']['to'], date('d-m-Y', strtotime('now'))) == false) {
    			throw new Exception("Promo already expired", 1);
    		}
    		if (isset($discount['products'])) {
  				$list=$discount['products'];
  				if (!in_array(intval($_GET['product']), $list)) {
  					throw new Exception("Promo is not available in the product", 1);
  				}
  			}
  			if (isset($discount['agent'])) {
  				$agent=$this->nativesession->get('ETLOGIN');
  				if (!in_array($this->nativesession->get('agencyRealCode'), $discount['agent'],true)) {
  					throw new Exception("Promo is not available for agent", 1);
  				}
  			}

    		if (isset($discount['nihgts'])) {
    			// n+
    			if (preg_match("/^\d{1,2}\+/", $discount['nihgts'])) {
    				$nights=intval($discount['nihgts']);
    				if (intval($_GET['duration'])<$nights) {
    					throw new Exception("Promo is not available", 1);
    				}
    			}
    			// n-n
    			if (preg_match("/^\d{1,2}-\d{1,2}/", $discount['nihgts'])) {
    				$nights=explode('-', $discount['nihgts']);
    				if (intval($nights[0])>intval($nights[1])) {
    					$nihgts=array_reverse($nihgts);
    				}
    				if (intval($_GET['duration'])<$nights[0]||intval($_GET['duration'])>$nights[1]) {
    					throw new Exception("Promo is not available", 1);
    				}
    			}
    		}
    		response(array(
	    		'SUCCESS' => TRUE,
	    		'DISCOUNT' => $discounts[ strtolower($code) ],
	    	));
    	} catch (Exception $e) {
    		response(array(
	    		'SUCCESS' => FALSE,
	    		'MESSAGE' => $e->getMessage()
	    	), 403);
    	}
    } // end of validate_promo

	public function treeview($array, $id = 0){
	/*public function treeview($menus, $id = 0){
		$mainMenu = array();
		$subMenu  = array();

        foreach ( $menus as $menu )
        {
        	# code...
        	if ( intval( $menu['parent_id'] ) )
        	{
        		# Code here
        		if ( isset( $subMenu[ $menu['parent_id'] ] ) )
        		{
        			# Code here
        			array_push($subMenu[ $menu['parent_id'] ], $menu);
        			continue;
        		} // end of if statement
        		$subMenu[ $menu['parent_id'] ] = array( $menu );
        		continue;
        	} // end of if statement
        	array_push($mainMenu, $menu);
        }

        
        $finalMenu = array();
        foreach ( $mainMenu as $key => $menu )
        {
        	# code...
			$data            = array();
			$data['val']     = $menu;
			$data['sub_arr'] = null;

        	if ( isset( $subMenu[ $menu['id'] ] ) )
        	{
        		# Code here
        		$data['sub_arr'] = $subMenu[ $menu['id'] ];
        	} // end of if statement

        	array_push($finalMenu, $data);
        }

        return $finalMenu;*/

        $return = array();
        for ($i = 0; $i < count($array); $i++)
        {

           if($array[$i]['parent_id']==$id) {
              $return[] = array(
                   'val' => $array[$i],
                   'sub_arr' => $this->treeview( $array, $array[$i]['id'] )
               );	

           }
        }

        return empty( $return )? null: $return;
        
    }


    /**
     * [signIn description]
     * @return [type] [description]
     */
    public function signIn()
	{
		$ET = $this->nativesession->timeout_get('ETLOGIN');

		#print_r( $ET );
		#die();

		if ( ! is_null( $ET ) && ! empty( $ET ) && is_array( $ET ) && !@$_GET['login'] )
		{
			# Code here
			echo '<script type="text/javascript">window.opener.location.href = \'' . base_url() . 'externaltours?id=' . $_GET['product'] . '\'; window.close()</script>';
			exit();
		} // end of if statement
		$this->load->view('externalTours/sign-in', array('product' => $_GET['product'], 'login' => @$_GET['login']));
	}

	/**
	 * [signIn description]
	 * @return [type] [description]
	 */
	public function verify (  )
	{
		# Code here


		$this->form_validation->set_error_delimiters('', '');

		$this->load->model('agency_model', 'AM');
		$consultant = $this->AM->signIn( $_POST );

		if ( $this->form_validation->run('SYSTEM_ROUTE_LOGIN') === FALSE || empty( $consultant ) )
		{
			# code...
			$this->session->set_flashdata('logError', 'Invalid account');

			redirect( 'externaltours/signIn?product=' . $_GET['product'] );
		}

		$this->nativesession->timeout_set('ETLOGIN', array('ID' => (string)$consultant['_id'], 'CODE' => $consultant['agency_code'], 'name' => $consultant['fname'] . ' ' . $consultant['lname']));

		if ( @$_POST['login'] )
		{
			# Code here
			echo '<script type="text/javascript">window.close()</script>';
			exit();
		} // end of if statement
		redirect( 'externaltours/signIn?product=' . $_GET['product'] );

	} // end of verify


public function load_view($viewFilename,$data=array()){
		
	$this->load->view('externalTours/titlehead',$data); 
	$this->load->view('externalTours/header',$data); 
	$this->load->view($viewFilename, $data);
	$this->load->view('externalTours/footer',$data);

}	

	public function booked (  )
	{
		# Code here
		$data = array();
		$agencycode =$this->session->userdata('agencycode');

		$connections = new MongoClient();
        $db = $connections->db_agency;

        $agencyRes = $db->agency_b2c_setup->findOne(array("agency_code"=>$agencycode),array("_id"=>0));
        //$data['agencyData'] = $this->session->userdata('result');
		$whereData['agency_code'] = $agencycode;
		$res = $db->agency_menu_setup->find($whereData,array("_id"=>0));
        $arrMenu = iterator_to_array($res);
		$result = $this->treeview($arrMenu);
		$data['menu'] = $result;
        $data['agency'] = $agencyRes;
		/*print_r($agencycode);
		die();*/
		// print_r($data);
		// die();
		$this->load->view('externalTours/titlehead',$data); 
		$this->load->view('externalTours/header',$data); 
		$this->load->view('externalTours/booked');
		$this->load->view('externalTours/addGuestFooter');
		
	} // end of booked

	/**
	 * [review description]
	 * @param  [type] $token [description]
	 * @return [type]        [description]
	 */
    public function review (  )
    {
    	# Code here
    	
    	$connection = new MongoClient();
			$db         = $connection->bookings;
			$collection = $db->EXTERNAL_TOURS->findOne(array('BOOKING_ID' => intval($_GET['book']), 'ITEM_ID' => intval($_GET['item'])));
			$connection->close();

#print_r($this->headerSetup());
#die();
		if ( empty( $collection ) )
		{
			# Code here
			echo 'The booking you were searching does not exists.';
			exit();
		} // end of if statement
		$this->load->view('externalTours/titlehead', $this->headerSetup() );
		$this->load->view('externalTours/header', $this->headerSetup() );
#print_r($collection);
#die();
		#echo '<script>console.log(', json_encode($collection), ')</script>';

		$message = '';

		if ( $collection['IS_PAYABLE'] )
		{
			# Code here
			$payment_type = '';
			switch ( $collection['PAYMENT_TYPE'] )
			{
				case 'CREDITCARD':
					# code...
					$payment_type = 'Credit Card';
					break;
					
				case 'PAYPAL':
					# code...
					$payment_type = 'PayPal';
					break;

				case 'E-NETT':
					# code...
					$payment_type = 'E-Nett';
					break;
			}
			$message = 'Thank you for your booking under reference ' . $collection['BOOKING_ID'] . ' made on ' . date('F d, Y', $collection['DATE_CREATED']->sec) . '. <br>' . PHP_EOL;
			$message .= 'Your payment is being processed via your selected ' . $payment_type;
			if (isset($collection['payment']['reference'])) {
				//$message.=' with a reference number of <strong>'.$collection['payment']['reference'].'</strong>';
				$message.='';
			}
			$message.=' in the amount of <strong><span class="fa fa-usd"></span>' . number_format($collection['payment']['amount'], 2) . '</strong>. <br>' . PHP_EOL;
		} else {
			switch ( $collection['PAYMENT_TYPE'] ) {
				case 'BANKDEPOSIT':
					# code...
					$message = 'Thank you for your booking under reference ' . $collection['BOOKING_ID'] . ' made on ' . date('F d, Y', $collection['DATE_CREATED']->sec) . '. It now on request. <br><br>' . PHP_EOL;
					$message .= '&#9;Customer must pay a full or deposit a payment before the end of ' . date('l h:i A, d M Y', strtotime($collection['policy'][0]['dateTo'] . ' ' . $collection['endTime'])) . '. <br>' . PHP_EOL;
					$message .= 'Deposit your payment through the account given below. <br><br>' . PHP_EOL;

					$message .= ''
							. '<strong>Name: </strong>Westpac BSB  034-215 <br>'
							. '<strong>ACCN: </strong>462 080 <br>'
							. '<strong>SWIFT: </strong>WPACAU2S <br>'
							. '';
					break;

					case 'PAYLATER':
						# code...
						$message = 'Thank you for your booking with ' . $collection['agentName'] . '. Your booking reference number is ' . $collection['BOOKING_ID'] . '. It will be actioned by our staff and confirmed with an invoice shortly. <br><br>' . PHP_EOL;
						$message .= 'Please pay a deposit of ' . $collection['policy'][0]['percent'] . '% of the total booking price before the end of ' . date('F d, Y', strtotime($collection['policy'][0]['dateTo'])) . '. ' . PHP_EOL;
						break;
				
				default:
					# code...
					break;
			}
		} // end of else if statement

		$collection['message'] = $message;
    	$this->load->view('externalTours/review/index', $collection);
    	$this->load->view('externalTours/review/footer');
    } // end of testing

    /**
     * [headerSetup description]
     * @return [type] [description]
     */
    private function headerSetup (  )
    {
    	# Code here
    	$agencycode =base64_encode($this->session->userdata('agencycode'));

		$connections = new MongoClient();
        $db = $connections->db_agency;

        $agencyRes = $db->agency_b2c_setup->findOne(array("agency_code"=>$agencycode),array("_id"=>0));
        //$data['agencyData'] = $this->session->userdata('result');
		$whereData['agency_code'] = $agencycode;
		$res = $db->agency_menu_setup->find($whereData,array("_id"=>0));
        $arrMenu = iterator_to_array($res);
		$result = $this->treeview($arrMenu);
		$data['menu'] = $result;
        $data['agency'] = $agencyRes;

		return $data;
    } // end of headerSetup
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
