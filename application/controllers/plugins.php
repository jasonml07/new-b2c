<?php

class Plugins extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
    	$connections = new MongoClient();
        $db = $connections->db_system;

    	$productID = $_GET['productID'];

    	$res = $db->products_inventory->find(array("productId"=>(int)$productID),array("_id"=>0));
    	$newRes = iterator_to_array($res);
    	if(!empty($newRes[0]['productAvailability'])){
        foreach ($newRes as $key => $value) {
            foreach ($value['productAvailability'] as $key => $value2) {
                if(strlen($value2['isMultiPrice'] > 0)){
                    foreach ($value2['multiPrice'] as $key => $value3) {
                        $result[] = $value3['basePrice']; 
                    }    
                }
                else{
                        $result[] = $value2['basePrice'];
                }
            }
            
        }
    	$curr = $db->currency_ex->findOne(array("curr_from"=>$newRes[0]['productAvailability'][0]['fromCurrency']),array("_id"=>0));
    	//die();
    	//$data['productName'] = $newRes[0]['productName'];
    	//$pct = (int)$newRes[0]['productAvailability'][0]['markupPct']."%";
    	//$mark_up = str_replace('%', '', $pct) / 100;
    	$gross = min($result);
        $convert = (float)$gross * (float)$curr['rate_to'];
        //$mark_up_total = (float)$convert * (float)$mark_up;
        //print_r($gross.'<br/>');
        //print_r((float)$curr['rate_to'].'<br/>');
        //print_r($curr);
        //print_r((float)$mark_up.'<br/>');
        //print_r((float)$convert.'<br/>');
        //print_r((float)$mark_up_total.'<br/>');

    	$net_price = (float)$convert;
    	$data['price'] = "$".number_format((float)$net_price, 2, '.', ',');
        }else{
        $data['price'] = "No available prices yet!";    
        }
    	if(isset($_GET['fontSize'])){
    		$data['fontsize'] = $_GET['fontSize'];
    	}else{
    		$data['fontsize'] = "";
    	}
    	if(isset($_GET['button'])){
    		$data['button'] = $_GET['button'];
    	}else{
    		$data['button'] = "";
    	}
    	if(isset($_GET['hover'])){
    		$data['hover'] = $_GET['hover'];
    	}else{
    		$data['hover'] = "";
    	}
    	
    	
    	
    	
    	
    	//print_r($_GET['hover']);
    	//die();
    	$this->load->view('widgets',$data);
    }
}
?>