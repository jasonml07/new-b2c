<?php 

/**
* 
*/
class Payway extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('booking_data');
	}

	public function index (  )
	{
		# Code here
		?>
TEST
		<?php
	} // end of index

	/**
	 * Save post data needed for creating a book
	 * @return [type] [description]
	 */
	public function saveBookingData (  )
	{
		# Code here
		_POST();

		$reference          = trim($_GET['reference']);
		$_POST['reference'] = $reference;
		$_POST['expireAt']  = new MongoDate(strtotime('now +50 minutes'));
		$this->booking_data->insert( $_POST );

		$this->nativesession->set('payway_reference_for_credit_card', $reference);

		response(array('SUCCESS' => TRUE, 'REFERENCE' => $reference));
	} // end of saveBookingData

	/**
	 * Get a unique id need for inserting data for booking
	 * @return [type] [description]
	 */
	public function uniqueBookingId (  )
	{
		$reference = $this->booking_data->reference();

		response(array('reference' => $reference));
	} // end of uniqueBookingId

	/**
	 * Generate PayWay token with the reference given from "app-payment.js:getPayWayToken"
	 * @return [type] [description]
	 */
	public function getToken (  )
	{
		_POST();

		$products = array();
		foreach ( $_POST['products'] as $name => $quantityAndPrice )
		{
			# code...
			$products[ str_replace('-', ' ', $name) ] = $quantityAndPrice;
		}

		$this->load->library('PaymentWestpac', 'paymentwestpac');

		if ( isset( $_GET['testing'] ) )
		{
			# Code here
			$_POST['products'] = $products;
			$response = array(
				'success'    => TRUE,
				'handOffUrl' => base_url() . $_POST['returnURL'] . '?testing=true',
				'token'      => random_string('unique'),
				'POST'       => $_POST
			);
			response( $response );

			exit();
		} // end of if statement

		

		$parameters                             = array();
		$parameters['paypal_email']             = '';
		$parameters['payment_amount']           = (float)number_format($_POST['price'], 2, '.', '');
		$parameters['surcharge_rates']          = 'VI/MC=1.8,AX=3.8,DC=3.8';
		$parameters['return_link_url']          = base_url() . $_POST['returnURL'];
		$parameters['return_link_redirect']     = 'true';
		$parameters['payment_reference']        = trim($_GET['reference']);
		$parameters['payment_reference_change'] = 'false';

		$parameters = array_merge($parameters, $products);

		$token = $this->paymentwestpac->getToken( $parameters );

		$response = array(
			'success'    => TRUE,
			'handOffUrl' => $this->paymentwestpac->getPayWayBaseURL() . 'MakePayment',
			'token'      => $token
		);
		response( $response );
	} // end of getToken
}