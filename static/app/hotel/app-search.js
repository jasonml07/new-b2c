angular.module('HotelApplication', ['ui.bootstrap', 'ngSanitize', 'custom.utility'])
.controller('HotelController', ['$scope', '$http', '$q', '$sce', 'config',
	function( $scope, $http, $q, $sce, config ) {
		console.log('HotelController deployed');


		$scope.data = {
            baseUrl: '',
            hotel: {},
            flight: {},
            transfer: {}
        };

		$scope.data.hotel = {};

        

		$scope.data.modelOptions = {
			debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
		};

        $scope.data.hotel.countRetry         = 0;
        
        $scope.data.hotel.search             = {};
        $scope.data.hotel.search.rooms       = {};
        // $scope.data.hotel.search.location = '183311';
        $scope.data.hotel.search.checkIn     = 'Date';
        $scope.data.hotel.search.checkOut    = 'Date';
        $scope.data.hotel.numberOfRooms      = '1';
        $scope.data.hotel.rooms              = [];

		var _selected;


        // ##########
        var range = function range ( n ) {
            var arr = [];
            for(var i=1; i<=parseInt(n); i++) {
                arr.push(i);
            }
            return arr;
        };

		var getHotelLocation = function getHotelLocation ( val ) {
			if( val.length > 2 ) {
                return $http.get(config.URL + 'api/search-city', {
                        params: {
                            address: val
                        }
                    }).then(function(response){
                        // console.log(response);
                        return response.data.map(function(item){
                            return {
                                'name'       : item.cityname + item.countryname,
                                'id'         : item.id,
                                'city'       : (item.cityname).trim(),
                                'countrycode': (item.countrycode).trim()
                            };
                        });
                    });
            }
		};
		var hotelCitySelected = function hotelCitySelected ( val ) {
			if (arguments.length) {
                if(val != undefined) {
                    document.getElementById('location[id]').value           = val.id;
                    document.getElementById('location[city]').value         = val.city;
                    document.getElementById('location[country_code]').value = val.countrycode;
                }
                // $scope.hotelCitySelected = value;
                _selected = val;
            } else {
                return _selected;
            }	
		};
        // ##############
        $scope.getHotelLocation  = getHotelLocation;
        $scope.hotelCitySelected = hotelCitySelected;
        $scope.range             = range;
}])
.filter('convertToNumber', [function () {
    return function (input) {
        return parseInt(input);
    };
}])
;