$(window).scrollTop(500);
(function ($) {
    console.log('Adjustments');
    $( document ).ready(function () {
        $( 'body' ).prepend('<div class="floating-button floating-primary back-to-top"><button type="button"><span class="fa fa-arrow-up"></span></button></div>')
        var amountScrolled = 300;

        $( window ).scroll(function () {
            // console.log( $( window ).scrollTop() )
            if( $( window ).scrollTop() > amountScrolled )
            {
                $( 'div.back-to-top' ).fadeIn('slow');
            } else {
                $( 'div.back-to-top' ).fadeOut('slow');
            }
        });
        // jQuery(window).scrollTop(543);
        var $slideMenuButton = $( 'div#slide-menu-button' );
        $slideMenuButton.find('button').click(function () {
            var $this = $( this );
            var status = $this.data('status');
            var rotate = 180;

            switch( status ) {
                case 'up':
                    $( 'div.page-sidebar' ).slideUp('slow');
                    $( 'html, body' ).stop().animate({scrollTop: 800}, 'slow' )
                    $this.data('status', 'down');
                    rotate = 0;
                    break;
                case 'down':
                    rotate = 180;
                    // $( 'html, body' ).stop().animate({scrollTop: 1660}, 'slow' )
                    $( 'div.page-sidebar' ).slideDown('slow');
                    $this.data('status', 'up');
                    break;
            }
            $slideMenuButton.find('span').stop().animate(
            {rotation: rotate},
            {
                duration: 500,
                step: function (now, fx) {
                    $( this ).css({
                        '-webkit-transform': 'rotate(' + now + 'deg)',
                        '-moz-transform'   : 'rotate(' + now + 'deg)',
                        '-ms-transform'    : 'rotate(' + now + 'deg)',
                        '-o-transform'     : 'rotate(' + now + 'deg)',
                        'transform'        : 'rotate(' + now + 'deg)'
                    });
                }
            });
        });

        $( 'div.back-to-top' ).click(function () {
            $('html, body').stop().animate({
                scrollTop: 0
            }, 500)
        });

        var width = $( window ).width();
        condition( width );

        $( window ).resize(function () {
            width = $( window ).width();
            condition( width );

            if( width <= 768 )
            {
                // $( 'div.back-to-top' ).css({'visibility': 'visible'})
                /*$( 'div.back-to-top' ).click(function () {
                    $('html, body').stop().animate({
                        scrollTop: 0
                    }, 500)
                });*/
                $( 'info-window' ).attr('max-width', 200);
            } else {
                // $( 'div.back-to-top' ).css({'visibility': 'hidden'})
            }
        });

        function condition ( width )
        {
            // $( window ).scrollTop(913);
            if( width <= 767 )
            {
                console.log( '0px to 767px' );
                $slideMenuButton.find('button').data('status', 'down');
                $slideMenuButton.find('button').trigger('click');
            } else if ( width >= 768 && width <= 979 ) {
                console.log( '768px to 979px' );
                $slideMenuButton.find('button').data('status', 'up');
                $slideMenuButton.find('button').trigger('click');
            }
        }
    });
})(jQuery)

angular.module('Application', ['ngResource', 'ui.bootstrap', 'ngSanitize', 'angularUtils.directives.dirPagination', 'custom.utility', 'angular.filter', 'pasvaz.bindonce', 'ngMap', 'infinite-scroll'])
.controller('AppController', ['$scope', '$http', '$timeout', '$q', '$sce', '$filter', 'config', '$interval', 'Comparators', 'NgMap',
    function ($scope, $http, $timeout, $q, $sce, $filter, config, $interval, Comparators, NgMap) {
        console.info('HotelSearch deployed');
        $scope.initialize             = {};
        $scope.data                   = {};
        $scope.data.currentPage       = 1;
        $scope.data.countAttempt      = 0;
        $scope.data.hotels            = [];
        $scope.data.counts            = {};
        $scope.data.status            = 'working';
        $scope.data.results           = [];
        $scope.data.error             = {};
        $scope.data.search            = {};
        $scope.data.filters           = {};
        $scope.data.isAllItem         = false;
        $scope.data.showRequestStatus = false;
        $scope.data.dumpHotels        = [];
        $scope.data.todayDate         = config.todayDate;
        $scope.data.viewByMap         = false;
        $scope.data.map               = null;
        $scope.data.currentHotel      = {};
        $scope.data.mapTimeOut        = false;
        $scope.data.markerCluster     = null;
        $scope.data.mapCircles        = []
        $scope.data.landmarkMarkers   = []
        $scope.data.itemsOnPage       = [];
        $scope.data.filtered          = [];

        $scope.data.notification = {
            isHotel: false,
            message: 'Updating prices and looking for more hotels . . .'
        };

        $scope.data.page = {
            size : 6,
            shown: 1
        };
        
        $scope.data.filters.price           = 'price';
        $scope.data.filters.prices          = [0,10000];
        $scope.data.filters.rating          = [];
        $scope.data.filters.hoteltype       = [];
        $scope.data.filters.boardtypes      = [];
        $scope.data.filters.landmarks       = {};
        $scope.data.filters.activeLandmarks = {selectedRegions: [], region: {}};
        
        $scope.data.options = {
            model: {debounce: 500}
        };

        $scope.data.image = {
            APARTLE  : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAyCAYAAADWU2JnAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkCBC8D8ZXa0wAADoJJREFUWMO1mHlsHNd9xz/vzczOznJ3uVzeh0TxXF4iRZ2W4kuOY1txmsCpYxdI26TphSAp0hZtiv7Vf1I0QFskgdumRQrbQRxbtlMfsVPHdn3Ikixbki1SMkVSpC5TvI8l997ZmXn9YynJiiVTctHf4mFnsDvv+32/9/0d8wQ3YkoxobB8jlfpzc00yZV4i5HPBfKZrOlI6desACIUXlDh0uOJ0tIPYm8OJMXd2657en2tPwwWMvTe/D3OvvzdaODcuR3u2bFbU6fG+wtTU83u0mKNSiWll7eFkLoUJRYyGrW1yuoPzWjk14tdnS8tp5MnlwPBpTKUXSrkJ2KJa/3wE6X4oyNv8l5zRzg0ObVTPzn8u4XjQ3dkBweq8tOTukokcPM2KIVQHghQAtB1NH8JRlmk4G9snA1s2Taibd/+pmxc97OGxuYP40DZRwkIcSWZRCKBaZpIWWT+jy++xbRmiW/31rRqIye/ae/f/0Dm4MG6/MSEUHYBoWuoUAgi5YhwKcLyoTwXMllUPI5ajiNyWaQCLVyK2deXDe3a+XLwllt+pPdsOpSFfP1VyOgAtm1jmqbUNI0Tywlv8713yIZzY7fJAwf/NvXiC7dmBo6aXjYDVhDRFkPftAkZi2HU1qKHImg+Hc/zcHN5nPgi7ofnsT/4AO/4IN7cHIUDB6zCmTNfcufmO6xc5gcLNY1PVOTjyYfMDR/fpvn5ecLhMD6fj+8Lwe+Nj+3K7d/3TytPPnFTZvSUkEIgWtrR7/gsgc/sxN9QjxUI4NNNNF1HCoFSCqUUjuOQz9ukV1ZIjw5jv/Ea3oGDiPgChMOU7r5zzvf5L/x4YV3jjyKZdLxz65ara2b69ddxWpqavNfe+EF8789/KzM2IjEDiB07sb5yP+GubkLhMJZpYRgaUmoIIS/PohTKUziei23bpDJZkvNzJN96i8Kz/4V3Zgw9ECS8Z088+MD93zW27XrUBadhdasuRdOJiTMsmv7yknfe/cuVXz53T2bslMRfgvG5uyh58KtEWloIlfgxTT+6piPktbSvkJ6GbuiYPh8By4//3s+zWFVB/uFHcE99QPLVV8pkKPytYCg42tDRu39w6jx9dY1oAEdOHGciWqlXjZ36/eyLL/5F5tDbQYFA3n4b4a99g/LWVkpDAUz/WkSKzhaiODRdx9B1DMOHqKrGKSvDGT+Nmp/BW16ulhWVdX8cLn13Q3x66fv//jASoC63TE8mG5PDw3+YP3w4opwCor2Dkt9+kEhTM+FgAJ9pomvaFer/xJyxSkjXNSzLIhqOELlpJ8YX78MrjWBPTIj8oXd3+2fmvzzUt0s+l1bIb33/HxjceovUJifvsY8c6XVnZ5HhCMbn7yHS3UWopATD8KFJjUw2x+z8IslUGqXUdZGCoocsyyRSVkZ49270LdsARX7gfVMbHf5CdSpV32kvoz94x52Yo6O1hdNjd+eHThhKeshYF4Htu9ANk/cHh5BC0tTYwMuvH+D4yVM0NzZw3xfupDxaRiKZIp3J4joOUkoCAYtwKIhl+ZGrXhSApmlYfj+h2lqyt3+W/MAAbjxO/vhgV/CWnduaBRN6ZTiM8LyN7tkzvYWlRYRpom/dSri+ntm5RR594nlc12XXtk28/MbbLMaXGR07RyBgYds2J0fPkEpncBwXTZOUBCxqqivobGtmc18X6xtq0XUdIQSGYVBiBbB6esi3tqOOHiZ/ejxqnjn7uVdbW/5H7yDFjGdULp47a1FwoaoKszOG37LI5eZYiq/gKY/lRAKl1KXVzi8uMTQ8zuT03G+KhZGxs7x9eIDX97/LF+/Zze23bMd1PTLZLH7Th1VRTrqzA2fgGM7ivChMTvet6+is1Wdim3X51pvN7sJ8AAVUVuOrrcXQdTRNIgApJO0tG9B1nYETIzTU1RAJh/D5fFRXVVy1wClgaTnBq2++TbSslPGzEwyNjPOlPbezYUMDemMTjhWAdApjbtaKlJb59RLQc8sr5SKT0aUEKirQS0KXiAB4yiOdyRItK6W3O0YwEMBxHNpaGteUrxSSk6fO8O57xxk7fZ7WDfU0Na3DKK8gWxJAxDM48QUzvTAb0CfACGUzlnJcECCDJUifUSyaq1uiPMXQ8BgnhsfJ24UrPHExpsQ13COlpLO9mVwujwAc10EIEH4fmAbCU3i5fDAXX47oSx9OWKaQESUECIEopq0rCoUQYFl+KsrLsO3CZazVeoQoeuCqnpGSYDDASiKJWn0GQEJRf1IgPM82pMzp5evXpf1H9Mm01DyBJlUqhec4KHXlsrf29/DAfXtQnrrkEdu2sW0bQ9fxmeYlcV+hZymJx1d45PFnEQJ0w0AgUDkbCgXQNTR/IFMSjSb0FiHSy88/fxK/mUGIoFpewk2n8Dz3kneUUkRKQ8Ram64AyufzZLNZfD4flmVdMztfmJpF0yRKgSY1XM/FiS9CLovUfWjhcLIkWp3UfcfeQQm5pNdUFOxzp9Hm5nGmZyg0NVNRHmVjdzu2bVNdWf4xEF3X8fv9aJr2iSIuDQXp7W5H13WaNjTg2jbu+XOIXA4tHEJVlM0u+vwJfcgfRHrektbYlJNHByCZIjcyTK5/M1UVUf706w+gPI+a6sqPgWiahpRyzXoVDAZ44L49JJMpgsEAyakp3NFRhKfQolHHXN84vujXEnIpbZPSzWG9qXVYLytHFWzc946QnbyA67nUVFVQX1dzzdVfT+EUQhCNlNJQV4MCMsePoc6eRmg6vqb2Fa2l8WD9sf0Zeeuff4vB9vZZ34aW58zW5rSQIMbHybxzgHQqTaHgFKNmTchrm0Lhui65XI70hUkK+95ApZNo4SD+3o0nvJp1A5nGGJIDh/jKvpc9f6z1eX9P936jLIrKpLBfe53E8BDZfJ6CU8DD+1SElFJ4roddcEglEiT3vYZ3YgihJEZXZ8Zt2fB0oTQ6sRioKvYzS5u3cqi88kPZv+MXvr6tK9IwkGfPkX3qSZZPnyKby+A6DsrzPhUZu1AgmUqyfGA/zou/QmRS+Gpr8e3cdSjV0fTfgRPvev2WKJJpDlfQd26ClYqKV3y33fqK0dKmlFC4779H6vEnWB4bJ5PJUSg4uJ4Hn9DLFBOhh6c8HLfYnCcSCeIH95Hf+xjMz6IFgvh27Tpjbtr00AdVdR9OByMAXFLlhbr1fPtL9ybssvCo5jqdzuRko7eyItTkBezJC+RLw6hwKVLXVt/WrhSoUgpPKVwUnqtwCg7ZXI6VmSlWXnoJ+6m9MDEBho5/500pY8+eH7jbdz1Wn8w4nTUNxXk+OunI1BixujYmho7c5T7zq4dSL73U7tg5hJCIujqMW24lsOtmzA0bMAMlGIaBJosFVQHKc3E9sO0CufgS2ZND2G++RuG9w5DOIxToLU2u/6u/89P8F/f8jXXq9EJTrO8S/hXv2h11bZyNT+JGqvZbU3O/0EdH/sodHfVJIWBqisKzz7Jy+DCyuxtfewyzoR4ZLkUaPjxP4eWyuIuLFM6exz55AnVqFBVfQihVrHuWH3Pr5jO0x37aufeZhbduvvlKD19t32fmp0F53bmnn34k9YtntrmZNEKsSsXzQEow/RAKIQMlYBrgKVQuh0qnIJWBQr5IQkpAoJSH2dGZLP36174ndn/uIVZWsjWRyBW4Vz2FOF1RRi3mSGDz5sfsodGe3HtHLYQCoRBCQ6EQdhaxkAcWUBSjTCJQQhQlpUnERZ0r0ENh/DtuOk7fxie95dlsXVnNx3CvWvc/I/yUHD7kurGOX/m2b39fL48WuQiJkAIpJEJoCCmLoFJHSB1P04otweqn2LgAQuDr6cpqW7Y8txituTBqWleNxGsemOSbY6TKKs+ZfT2P+ru7F5CXO7+LAEqs9hnFJghxMS1evF+dXauswty2/bDe1vJMw7GD7u5A6Y2RWf/Dv6M6sehqsbZn/f39T+tV1a5EIaRESomUYvX74vVH72XxHRyB0DTMTf0Jo6//6US06nyyvvVakNcmw9//C5HSCnKPP7Xo9vY9bW7unxGGcXnZSly+/s2hVrtGBb7aeozNWw5mw9EX/OfOu+t2P/kpyFzU3ratxGvr3tf6+l41GpvVxTBFSATXGEIilUD4TMz+/gtua8u//fXhIxMvvfMOnPzONbHWPNOb0Qy2Hz+2cqYt9ri+ZdttzvRkk5fPF1ehPp4ZlFAIwPMUZnOrktt3vOz1bHzjh8GAamhq/USsNT2zo2cjIxv7SXV0H/D19vzEaGxKCwApVrvqy0NpArEazzIYQPb3jdkNjT9reOLh9JK01oJamwxAZ0UFkdnJrBuLPalv3XFCKwkXE6AQVwyBQgmBKwRGa3vB6el6/rz0jh7duJnexvo1cbS1qRQtf/f9rPR1J9rtQkgsLn7GnZr0/WYCV6ui1cIR9LvuGsx1dn6v0mdN9H6nE8bXxrguzwD8+K5dfGPwqGvEYnt9PZ0v6OVRV3DZO0IIJMUk6OvpyrqtLT8/2RT7YN/JU/Dr68O4bjIATl8/qW9+bcrf3fWvemvriDKKb51ylYxQHnplDV5//2CiquKXO8cG3D+49+7rnn/NaPqoBYXOyNw0C8HQQMnU1D59errTnZiUCFGsV7qG3NSfsFvanxqbTZxP69d3ynXRrlszF62wPsbXyzOFZG2bpydWPuucOxtSXrE/1tatU/6773ou3xb7567m9StfvvM+FuMz1z33DW0TwH/82Z8wHVyH3dy+X/ZufFJvanaUUkjTj9W3Zdjs6Hio5sBb09V+i9HTAzc09w2TAciU1xB97ZW0aOt4wti0aViWWBgbGnN6d89jcn3TEXfHzmscS/w/kGkNhJnu6ma6unbQ6Or6T9+m/rRv05ZB2dv7rO/YQGFq3do55Wp2QwL+qHXWNTCTjNuivW1vSDfanWBgfKGm+rTfNOgTn2qN/3fLewWxnEnUzCzPVSileOaFvTf0/MUzHqWU+F9UPRkBYe7ppwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wOS0wMlQwNDo0NzowMy0wNDowMKsE0UsAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDktMDJUMDQ6NDc6MDMtMDQ6MDDaWWn3AAAAAElFTkSuQmCC',
            HOTEL    : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAyCAYAAADWU2JnAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAZAAzADMMDFAtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkCBDAGTKUgwgAAD2dJREFUWMOtWWlwHMd1/l5Pz8zeAHYB4iBBEAcBckkQAEmQBCmKpARSoqQ4sS3JSZSUHeVwJVVSyvGPuFwpl53YP+KUnViJpbhiJ2VbBynLVnTZiiheEkjaJEXxEkAKPECCuBdY7L07R7/8WAACIepy8qa6pnem5/V7X3/9+nUvdXdvJxSFAPDMHfPqrJSidCrF6Y07+Mm//7ovoolFYnpymUwkGnU778tncqYrhEd4faBgMOb6g2fHpHF+Q2UkqQmiaLQFlVVVmKd7Vv9NzyRulrkXRETZTJaPHP0N8KW/4tNf+kq4vFDYyKeO3l7ov9xhDw/Xu1OTVUinhLJsgtCE8PtAZWFLW7RosDxc+quxY2++qpa39A44PNVVVWlt2rieA8EAAWBmBhHd5Dh1d28XANh1XQAgTdMAANlkgo8ef5v39/eVVqWSXUbfu39on3vnjvzZM4uskSGpkgmogg0wg1gV3RAAaxKaxw+9rNT21NWNedd1XtA6Ow+5dUueOrOs6dqfhfzo3NxFXq8P6XRqPhAsATAR0YoVK2GaJs6dO8sTY2NYsv0+cfanTzd5+vu/aPf0PDh95GiNdWOQ2LJBUgMHg8DiCChUAvIaYOUC2RwQj8OZjsMdGdKt4aElmXfeWWKeeXtLYHPX+q7btjx2PJE8uiHgtX7v9/+A0unUHBVmh4mEEOju3imE0PDjJ/7N/dbh46Ir6NtmHj/+lczLL9+eO/OWqXJZwBsALW+BbG+HaGmBXl0NGSyFZkgopeDmC3Dik3CvX4N1/jzU2TNQ4+Owe3q89pUrv6vGJ1Z4C/l/fvnt83s2t7Qku+/cJkDEc9To7t4uiAi1tUuRy2Vxctsu3rdtyxb72JF/Sjz7zMbcxXdJkAA1NkPecSd8W7rgWbIYXp8PhjShSQlBBGYGM8NxHBQKFjKJBDIX+2Ad3A/VcwQUjwGhEEp2dI/r99z370Pli763Y11HfOfOHWBmmjWGAEC5Lg4cfJP7L/Y2yp5j35ne+/TvZPsvCDJ9oI1d8D5wP0LRVQiGQvCaXui6Bk2TECTAoog0KUApF7brwrIspLM5pCbGkXrjDdjP/xzqSj+kL4DQ7t3xwIP3/22sc/N/3RX0uG2buggAtIaGZYKIaP/+w3ykv68i2Nv3lcTePQ9m+97RyeODvusuBD//MCKr16CstAR+fwCmWUQknc0jly+gULCKxbYgpQ7DMCB1CY9hwAwEoNUugVVdDTU4BDV2A86NIa+mm7VlleHz39z92euB5/bAHwySBMD79h3Et198QQ+PjT+QOXTwodyFXlNoErT1NgQe+mOEGxoR9HuhGwakJpFKZ9B78TL2HTqGZDIFEAHM0DQNG9a2oquzHYsqItBMDZqU0IQANt+GuKuQ+8ETcK9fRebQgTZRV/vV01u2PdL+zsVL3dXVJGfYrD61qrWFDu57OH/8RCk7NkTLKvg/+zmU1jcgGPDBMAwIEgCA2GQczz7/Kk6/cxFKqbnwSACuDQ4jGPTjjkUREBGk1OD1ehGGAG/qgjMyisJP/xPW4CAVjv1mh3d59DOPxdPfeeGz9yjJzPjGiZOaOTZ2d/rEiTVqbAxaqBT6PXejdFUUAb8fhm5AE9pcQCgJBqHrOlataAQBMAwdmUwOHo+JVDqL8nDZHKmJCJqU8HqB0rIyWDt2IN57DnzoAAqnT5m+jV333R2NPvPogTcG5f79h/lrUquxL/bdZfWe11koaC1R+DZsRiAQhMfQMRsI34vOgKFLRFuisB0XJaEARsdjKA+X4dSZXmiaeC+SzRqkafB6PAhWVyO3/U4UTp+GG4+jcPZMNLB1UyczDwpm5nIpW9XVK6325CSEaUKuX49gTQ0M04RYYMisKMUYHo1hdDyGG8NjmIjFMTQyjkLBmmc03VSXUsLv9cK7ejXQ1AxWjMLlS2H7ysDOF8+eKhUAUMpuhTVw1cuOC5RGYK5sgcfnhTETQ94vBCEIVZXlqKqIYHF1JcrDpaipqoBpGgtQfO97IQR0w4A3EoG+cgWE1OFMTpA9NNLWGCyplAct6CsnJxtUbMJHDKCiEkZ1NXQpIYTAB4lSjInYFJRSyOUtTE0n4SpGwbJv2b44XIDUCKbXC1lXD8frAzJp6ONj3vKyiEfWGJCUSESQy0khACovh/QHoWnaTV4tUA0ShEi4FK6rUBL0QykX5ZEyDA2PzuUGt0JICAEpJWQ4Avb7QPEsnHjMzMTGfHIyPml4crniEBEgAn4IQ/+A4ZlnjlKYiiegWMG2bUwn0wARbMfBh31JEBCCIDwGYOogxVD5QiA/PV0ms6MTHotECRMBRCAAhGL9w4SEQLg0BFcxQkE/HMdBebgUgzdGP9yJGZQEqOiwIJBSlk5aTnrCZVlTaMNZoSlAE5xOQzkOmPnDlSqFeCIFNbM4JlIZkBCwHef9/GKG67qQmgYmgItoAJYFSA3S48v6w2VJubWQTA8bei88Zo6I/Dw9BTeThlJqLkZ8EDIloQAcx0UoGEC+UEBpKAhdvj8UEADHcYoRnAHHdeDEp8CFPIQ0IEKhlD9cmZa8dDkPxSantMqIZQ1c9ovxCTgjo7CX1X8wOkSwChZ+/tK+OWNnDS8PlwELWENE8JgmAIJlWbDzebjXByDyeWihIFR56fgIU1IAwJQ0p7S6hjxJE0ilkb/Qh3w+D8dxb20QA8xAPm/B5/Wga30bahdXI1+wimvVLeYTEYGhYLs2CpNTUBcvghRDloUds7auf5iRFEREcUV9clljnywNg20L7lsnkB8agm3bYHUrxUXnmRnr2qJ49It/hPs/tRMBvxeMW6PJzHAdF/mChey50+Crl0GahNGwPOEuXXxsWziQFTu7t9PPEtNjel39C57lDRkIAJcuIfvrHmQyGdgzZJ7fRSabQzaXBxFg2Q5uDI8hm8tD0zRYBQvJVGaOc0UgGUqpYgZ4Ywj24YPgTApaKACjtfV8whs6CxSTK9rztW+oL//D10dFLLbGHRhocpMJqEQS3NAAY1ElNI1AGqF4Ael0Fo7rormpDpUV5cjm8rBtB0sXV6GpYSnqltSgsiIC13UhhCgaYttIJRKY/tXLcPcfAFkOzLXtWbW567Efx7L7zeFLxd1BJOijRxdHBv+lvfPnxuVrXe6RQyW4OoDcs3sxXRICNTXCSz6QkCAhUFVVgc99+u4Z+ItDAMJcoCQS0DQBIQSYGZZtI51OI9HzBpyXXwFl09CrayA3bvr1SHXkV/8Y7XR2fdcoZksdmzbQ8/CriWDJPn3b1tf0xuXMxHBOvYX0089guv8Sstmi965SECiuwFJK6LqEYegwdH3mmQZNAIoVXOWiULCQTCYRP3IYhT1PARNj0HwB6F1dV3jFysd/cqDnml8XxMwzmR4DO+/cRq9fvn598I0D39ImJyqS6fQ2e3SE3J7DSKamUbj/AYRWtyFQEoKh6dA0ASKaK8wMxQwFBtxikCtYBaTHx5A6cADWKy8CQzdAuoSxYX0aW2/74fXwol/+6JG/cWY2kkXOAGASgsx0El8tTI09umvXiJHJbXQHrkVc2wKPjsLt64OVmIajG7BNAy4XI6tSCq7jwHFdOLYF23KQzeWRGh9H4uQJZJ/bC+u1XwJTCRAEtGXLXHHv7j2TGzu+vX1pY2J2RwvM7JvmLxuvv36IT6bGvJVHj/9d6idPfrnw7ruGmEm4YZjA4sUQq1bBaG6BuWQxRKgEQjegFEPlc3AnJ2FfvQar9xz43Yvg+BRIMRgEmCY89+7uV7t3//nKtnWHb79tE5le700b/5tOA7q7t2N9sDI7ODTwtKdrU7czPNKpshlAEODYoKtXoK4NIL9/P/LBIITPD5g6oBicz4MzaSCdBewCiBkQAkoQWDHM+mWpwPr1P0rWNZ8gIpp3AjJnzJwhs+hs3rAOx2oqLmzp6HjK7r24Ov/WW14mBojBpAFgkJWDiBXAiAFQM0qK/FEEQBAU09xzPRiEd9Oms+7q6N5oWSi7c+cOsSC6s1iwkBAA9pWE8AB8TqGh8RW9c8MpLRIGcXHKkiAQCYA0QAiwJgAhASGhNA0sitGISYDEbHuCsTqa0zrWvnCA5Y3tt2+mhYYAoPl8ee/OTEREB6ecAb01+mNPNBqDEDO5TnE5ICIwcXEqzrygWTXFNAUkin7Kigp4OjuPc13tL75QXePopnmrVIDFfETm17u7t9Ofrqp3rSU1z3vaO57TFy1yCTzj7YzHN9Xn/xZF9AAITYPZ1pHUWtue64lNXyOi+ajM75NmpxUvMIoA4M47bqeGytqYs7r1Z0ZHxyjpOnh2UeDZnPC9axYiYgITgRgwqmog1647mvSFXn5ozRpnIWnny/z0nxbcQUIwEdFgwH9KrFmzT6+rZ8Fc7IgECGJGRbHMJJMACQgmCMOA0bH2htNY//iXvvfdwdu3blpIi5vO+D54LzLTeHNXJ7Y2NU/bjc3P6GvXD5DPO8OZhbjMXLPpMzP0+iYWnZ3/k2ioP/Tq9x9Xptd300RZ2Nd8Y/hWxef3g5kRW7qsR1/T+h96XX2mOMAEKoIwV6ARiLh4vBfwQXa091tL6n66NjORWr+2dWZFvYkWN/UlX3/90Idn3ijOnGsjN7K8YsVeY/3GT2FoeJOTSb8vPyYUh1CB4Glqtp3W6As3JJ2MTaRx8tS5j+xH+6gGs1K4634k2lYlmy07SJOTW9zhIWNhrjtLWi1UCrlr15n8ypXfrDC8g2v+eiVw6aP7+CjOzMkTuzbj4TMnXb2lZY+xeuVLMhJ2iwdxM/stIggIQBMwVkdzblPjU731LecP974LvPrx+vjYxgCA09aB9F9+ftizKvp92dR0gXUBUHEzRkQgVpAVVVAdHWeSi8pf7Oo/7f7JvXd9bP3yY7cEECCJC+MjiAWCp/3Dw4flyMhKd3BIgAgMBkkNor0jaTU2P9s/lryWkfRJ1H98zsyKvbQFX4hk7VT1ciWTiTudgatBVgoMQKutZc9du/67sLzlO9GGpYnPdH8ak/HRj637Ew0TAPzgkb/ASKAWVkPzm2JN615Z3+AwM4TpgbdtXZ+5YsW/VvW8MVLp8eLi5dOfSPcnNgYAspEqhPe/lqHlK57R29v7hN8LfVldXq5a/aRYWn/C3dj1kQcH/2/GNPlCGImuwkhl9Rk9Gv2h0d6RMdrXnRFr1jxvvH3aHq5d/Nuo/WQEni8ra5ZgNBW3qHn5nqDUm52A71KsqvKyx9TRRr+Vj/93KSibprPJqtHp8XJmxi9e2vOJvp/9z4GZ6X8BtTLqYR8YkvYAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDktMDJUMDQ6NDg6MDYtMDQ6MDAIN6VhAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTA5LTAyVDA0OjQ4OjA2LTA0OjAweWod3QAAAABJRU5ErkJggg==',
            CLUSTER  : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkCBDAYtqodoQAABpdJREFUWMO9mFlsXFcZx3/fuXc2j5OZTOx4kiZ2apOWqFlwEUQtRS1WH6tK0EoIhFkfEAghKvECbyCBEIsEiLVIkFJAbKJSVQRS09JGSZNCceq4NWDFcR2n3rcZz3i2e8/h4d4ZzzgznhlT8UlXHp+7nN/5f8v97hF2YQ8+OVb+GQQS/hEBBMgBq/5RADg3fLLtOWQXQAIkgTPAe4HjQI8Phg+2CPwbuABcAuYB3Q5gS2BVCnUDDwHDwDuAGKAa3KaBDWAU+BXwNLDQqoJNwaqgTgJfBB4G4gDagAhYIij/ScaAYwzGUBkD0sCfgW8BV1qB2xHMh1LAPcDXgfsAZQBbxBzcEzDHEhHdHw+beMRGgPW8w9R6XiZW8jK7UVSOMSJbCr4MfBk4TxPXShMogEHg+8B7ADEGEhHbDB2N6aHb46Y3FpKwXevNvKPNTLqgn59Kmeem1q3VnGP7dAb4O/B5/29D5ewmnkwCX6pAAUdiQf3RUwfMPYf3EraV1Ftc2FZyLBFRvbGQfntXpHR2dEHPpItB8a4946v2OeBmo4mtHdSygY8BnwGCxkB31NafGkzq+/tiKmApBYijtZpPb8rkclrm0pu4WkskYIulRGwlqi8WVvs7bGd8KUe2qC1fuT5gGfhH/wc+q68/9ePmrqxy4R3Ak8C7DRBUYj54V5f7oRPdKmh5St1cz/DMa9NcmJxlJVvAYEh0hLm3P8nDJ46a3sQeAEqucX83vlT6zdhSqKhNOSeu4GX36/VcupMrHwBOlSPjSCykh47GpQw1s5bhu38b5eL1eYquW7lpcSPH5HKKiYV1eWzoNLfv32sCllhDR+PuSzNpd2Ilr3w57gKGgHE/9mqsUQ3q8MHC4JWE0z1Rc2hPUADJlxz+cOUaFybncLRGidQcrjZcfmOB349cY7PoCECyM2ANJjt1VQkJ+nN01gNoBNbjrwiAgBJzLBEWS3kR8sbqBhevz1PSuqHcjta8dH2eyeUUgCgRdUciIkFLVatzHDjUDlgS6Cr/E7IViUigcvLmepbVbB7ZoQwKwlquwMxapjK0L2KrsC2miizRLlgcCIHnfOVX97KVXBdtDDuagNaGkrulqqVASc1iQnivtZbBLPyMFaDoGjJFp3KyuzNCNBTA0BjOGEM0FOBAZ6Qyli1qKbpatulcV/ZGYJtAhaTgaplOFSoYA10xjvfso5ndeSDO2w5sCTKdKpBzdDVIDq+etQy2BFSCQ2sYW9yUVN4xAPs6QjwyOEBfYg/af2FXlAK0MRyOd/Lo4ADdvmIbRVdfXczi1or8JjDdEpgASpizhJnqwf+s5OSVuYwxfs0509fDY+87zZm+HqKhrXLYEbB555FuvvDAKe7tT1bWNjKXMeNLm9vn+yder3aL3VJgg5bQEVDrmaIecY25vwy7UXTlmYlV1R8Pm/59YbGUcN/AQQa6YozNrjCzlsHgKXXq0H4OxaPlQDfTqYL79MSKShfc6vjaAF4Eii2BhSzFdKrodHXYz4kwbIxXNgQYX9qUJ0YX+ORgUvfGQqJE5LZ4lNvi0UqWVmedAX0zXXDOji6o1xZvUWsMuNwglG59iXc99Gn2Bi2MIW0Mg8CdNUGxUZTpVIG9QcvsjwQIWB6IiCBbUCbvaGdkLuM8MbpgvTKbsbSpyb4ScBaveazbl9VN1aFfXmV8bZETiZ4PAz8AalLQAImwbe5ORs3dBzvpjYWIBiwQyBZdfSNV0CPzGTMyl7XX8k69DuZ14CPAq1C/J2vWKHYBjwPv336+nFxhS4iHbRMNevNnS65ZzzmSd400mCAPfBWvzXZ22yguAz8D3gUcrreigmuYz5TEUCqPi8iOPfsLwK+pqpP1rFEdq5b3PPBb8GeuYyLea0uJ93sHexP4EXBj2xytg1VZ1lftUgvX7mQlvMbzXDOopmDnhk/iOA4iMoH3QTLP7u0y8HMg18p3ZVPFXvjEIMarUX8FfoEXvO3aLPA9MNdavaEVV5ZlzwI/AZ5tE8rB+xL/C4hpdZugJbAquwF8E69Pb9VexCs5m+3sXbQMVvXQS3g1aLGF26aA7wCTbQrQnmI+nAv8ES9Td4q3TeCnwPPbFvbWg1VNkAF+CDxF/ULpAn/Cy8LCbvbH2garsjngG8DFOudexnPh0m4fviuwigIiV4GvAP+qOj0FfE1r/WrNtf8vxc4Nn8Tvqc/jbVHNAivAt4FnlVK7hoI2tzrrmd+FRICP433BPw5s/C9QbwnYNjihzXrVyP4L5eCIoe4w5TwAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDktMDJUMDQ6NDg6MjQtMDQ6MDDdjbM1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTA5LTAyVDA0OjQ4OjI0LTA0OjAwrNALiQAAAABJRU5ErkJggg==',
            LANDMARKS: {
                museums : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGAy0s4l8E2gAAF3lJREFUaN7tmnl0HdWd5z/3VtXbn/bVkrVZlmR5wxjbeAF5AUzTIQNN2uSQCZlAOs3MpIdOyCFzJgPd9HACE9KBQCaEhO4mgaa3QIBucKCxgRgcwAvCljfZ2q19e5LeXlX3zh9PErYx4I0kf/TvnFLpqkr3/r712+/vCs6R9uzZAyABcnJydE1NjXYcB8uyznXK8yLx21pIa33ielqIC7v0ec+2/d+eRStHKmEWNtQvKsnPLyruHxtdIrxWji8cMtLaNbVAeAwLQ2vLiSfdqdGxCI77ntf0vO/1iX7bdu2qBY2/GyD9777Hrh/+hGsevC+vZzyyaSIx9a1JqSqPuylPx8RYaGgqJidSCWKug6MUlmEQ9Hgo8AUpCQapysqNVQdzekp8oVfNVPofRtqO7mvctCH6t//3r7jlW3d/+kC6DzSj7ISRVVA5L2V6ru9ITfzhrsHei3f3dAWPjA0yEJ8i5bqgQGgBWiA0gJ62KA0CAl6LimA2FxWX64tLyofrcgveLsT7WHB04jXt9yVKFtV+ekB27nqHdCqZXVVZdVu/k/zqq91tVS+3tcqe8THSykUbEkMDWqIQaK3RQs+uIrVESIlQgBA4wkGjCRgWiwtLubp6/tiassqnq32hh7ML84+iQcgzY/GM3up7p5ljqy6isWegtk8nv/3a8Y7P//Jgs+9YZARHGghhoLVGui5SgNc0CZoW2R4vQWlgAApB1E4TcVJM2Q620mg0hpRIF1LCxfAKVhVVqhvqlzZvLKv8kT0y/OzchgXjX/6Ta3ni8X87PyCqexTRlhIjCz1XHpoYvfdnLbsvebWzVThORgJojVCCbK+PeaFsFmcXMD+cRZHfT5bXj1caGFrgCk3CdZhMxulLxGmdmqQlMkRnbJIp10YIA4FEuZpcv4ctjYsTW2oW/msgnv6LigV1h3/60A/46tf//NyA7H5zB1OjI6HG1Zd//t3h/m8/tPv1qgND/XikQGuwkRT5vKzPK2V1aQUNWfmEfV6kz4NpWkhpIAQIkXlfa4XrujiOjZtKMx5LcGBimB2DXeyMDBFNaDwIkqaNgebqeQv0Vxater0kZv/P2z/3vXf/z8//jCWrLjo7IC3v7SO155DMuvLSO3fFxv73D3a8GuyZimIJAxcXyzBYV1DKZ+fW0JBfRNAfwvB78JkWPtNCGAbSkCBgxtbRGuW6aMchpRxiaRuVdpmKxtg31Mcz3UdonhjC1RqJQUoomsoruX3F+oNl0vfN9RVlW19qPsjCiz7sqs3TgXjjyafJWdKIUVK87PXRvv/y8DuvB7ujk1hS4GiXEk+Az1XUcnV5NTk5OXgDAXxeC4/Hh2mYSMNASI2Y+U4nGK3SCqUUHsch4Chs2yHo9ZLlt6jLzuaXnW38a28bU46NF8nOzm5M+WbjN1ZvfLB5YKw3LfS+M5ZIZGSCbMs3Z2v/kYfu/822zx0bGxeWNEnhMD8Q5ivzlrKitJxwOEQgGMTn82GaJqZhIKQEIT5WZzU649GUzqiabZNIJonHY0Qmptje087P21oYtNNIIXGVzfV1i/RXl6x6LH3w6B0yPye+fOWKk+Y0Tl3keGcHvmQs2KHSf/Xw+zu/sLfvuOEVJi6aCr+fr9Wv4NKySrJysgiHw/j9fjweD6ZpIqVEnAJCa008HkdKiZRy+usJhBCz7xumiWEYmKaJZZmU+cMUGD4OTAwSUy4eZXJ4ckiEA/6aDY3L2ufXz2/ZeOkanvj7p06vWi+9+CILqqrF4b7+G19qP/ClHd2dlk97SUmXAsviT2uWcWlpGb7cIOFAGK/Xi5QSw/jQ9wBgfHycrVu38vzzz7N8+XJuvPFGKioqODHPklKitcbj8cyCFQI2qBpidoofd7xPAo3rwrMH9+VdnDfnfy1LJporFzQcOq1EtNYkUylu+6//bU27m/7+j997qyQST4CU+KTmixUL+YPqOgL5IbKCYbzejDoJITg1AYzH42zfvp177rmHRx55hL1797Jjxw7eeecdpJSUl5cTCARO1vFpCRmGgYFASCjyhYgnkhyZHMTUBmNpG9twi1ZXzPeaY2Ovfu3rX3ce/uEjJwO59eZbyBGW18zJ+otfHGvZuL2zDUMaODisyyvipoYlFObmEQiF8Hp8GIbxIRC2bbN3716++93vcv/99/Puu++SSqUAcF2X7u5utm3bxv79+wmHw8yZMwePxzM7z8xlSIEWIDUUegMcGR1gKJ3CQtIbnaShsKS6yO97V/h97TdffTU/eeqpTPYD4DEt/MHg/J741MZtHYfwYKAR5FoW11QtoCg7F2/Ij9frxTSMWf0GUErR2trKvffey0033cRjjz3GwMDAadUtGo3ywgsvcMstt3D77bezc+dO0un0SaomLQMr6CcYDDAvO5+r5zZgeAwkBvGUw9b2A7lpj/eKF/7km3IgkfpAIm4qxv0/286qxRU3be1tu+HFriPS0CYauKyglD+qWUh2dhaBUBBrxr1Og+jr6+PJJ5/krrvu4plnnmFkZGSm9vhYisfjNDc3s23bNkZGRpgzZw55eXnTDkGgEbgGaO2SJUwOjg1xPJnEACbScZaUVqrrbtzyQrCkKP7wgw9mjL3n0DH+R1Nt3qSdvnZnf5eJltiGICwsNhZXEw4HMPxeLMPEEBIlYGp8nF+9/DKPP/44b731Fslk8hOZP5W01nR2dvLAAw/w0ksvcfPNN7NlyxbKysowhcRnWmivh+JwiPUFczkSmcAVMJpIsG+4r35lTnG9Mq2RWa+VTLq4WpW1TURqW8dHsRA4KOYG/TTkF2B5vfjMjFcRhqTv+HHu+cu/5IUXXmBychIhBH6//6yBnAjoyJEj3HPPPWzdupX77ruPFStWgDZxLS+Wz8ey/CLyjpsMJdIoFIdHhwqTC421RwLBtw6+twtzz55dmP4wiamJBX2JqYLJWBwpBJaCupwCcsNhPKY1axcAfp+PYDBIMBikvLwc0zTPGcSJYMbHx7Esi3A4DDAbYyzLQ1FuLpWhbIZigxiGpDcyJgamIuuuiMce7Q9lT8l4PM6x9haPaxmbumITgbTrotAYAurDufgsD4ZpzXopgIKCAu644w42bNhANBpF60yEPvUO4DjOGd1jsRgVFRXcfffd1NfXfwDEMLAsi5DlpTaUAzJTsA0mYowk4xcbObk1eQWFGF+59VZMwyzMKSj89ssDXUWtI0MIQ+IzTT5bPp+q/Dw8gQDeaTc5Q9nZ2SxYsIBjx46xZMkSvF4vy5cvRynFpZdeilKKdevWEYvFZgFv2rSJeDzOpk2bGB8f56qrrmJycpJNmzYRjUa58847ueyyy2YlPyMpx3XRaZv+qQneHu1HIEjhsrJkrlUuPW+klTpkFhcWY0np1aGAL5pOZlJvLQkLSa4vE7mt0wQ9gNraWhobG2loaODQoUMsXboUgDVr1qC1Zt26dUxMTHD55ZczPj5OU1MTkUiEpqYm+vv7aWpqYmRkhKamJgYHB1m2bNlJIGaCpJQSaQpyPH68wiApXKSCSCohI5OTPuHzYtqOjZSmUo6Tsu00TDPssSw8HgspTw9i5mtprVEqk9HOjE+9Zp6f+s7p/n46mgmUlmlhCAEClIZoOokypFICTNdxcYR2UT6ttCZTOAikEEghZ4FdSDqTOHMSkBOkM1vbCHCUshzl5iklyVQJWiOlQE4nf0IIbNedNcRPA8xZIyGT5kxjQAqJZZoCpU2JwOzp7cVNpnSlr1FbVsagTa2JaUU8bWdE7mbE/lEqdrrE8ePGp/4+M/6o+ZUGV0M8ncJFI5BIrQh7vOmsgH9MWRamx+fF1tpBqXSB148EpBA4jstIKoFyVGam0y2gMjW4bds4joNt22c0TqfTJ/19Zjzjsk8lrRSOqxhOJ3CVBikwtCA3EEiFQsFR1zAwDcvEcYx4MhFrrwxkLRezYlR0xCZIuw6udmeN8cREcevWrbz66qs0NzcTiURoaWlhaGiIjo4OBgYG6OzspLu7m6GhIbq6uhgdHZ0dt7W1MTk5SUdHB2NjY7S2tvLUU09x2223fZDiT8cjx3VI2TYdsQlcpRESsjxeSoLhSHYoeDzmOphaSlZesiLW1dn59Jxw9iaPaea5rkJpOBYZIZpIEgo7eFz3JNf461//mu985zsMDg4yPj4OwPDwMMDseObe0tICwP79+wF4//33EULM3vft20cqleLRRx8lGAxy6623YpomatouHMcmGovTORlBy0wFmh8MkWd5j4729PRElIu5fu06Og4fozyctyMp7H2lgeD6zmgUUwjaopMMTE5RkJuN6/pmUxEhBD09PSSTSYqLiy+IPYdCIeLxOEePHsV1XAzDwNUKx3VRts2x8VF64nEMIXG1pjY7nxwldn+jcVHky88/n0kaXaHJ239wrGT18l0LCorXd0xEkNJiKBln53AXdUVF2D4by7Jmy9prrrmGhoYGlFIXBMgMlZaW4vF6MprluGjbIZ5I8cZIN0nbRhhgSlhYWDKuY/HX7jraqrNdKwOkoq6KHl9Ae223dV1ptf16Z6vlugpHCl4f6mZTRR3zg35sy5otqPLz88nPz7+gIGZIo3EdF8d2SKZTHBgb4p2RXrTIxKAyf4D63IJXC/DsHE47lC2sy1SIHmEhUzamo3Yuyi7srM4vQEmNB4Pu6CSvdR0hHotjp9OZvOcsA9pZkwLluKTSKcbiUf69u5XxZApMgUZzSUV1ojor57lEw/ypba+8BPBBqdt5vIs3d7zZ6rWTv9xcVa+EBENrUCYv9nWwf2AAO54kZac+VTBKKVxHkbLTpONx3j7ew1sDfZjaxEFRFPSzoaq+pcTy7jSGhvjvX//myUDWbWiiZH6lMzo8/NTqvPJjjUXFxKWNEILhRJInOvbRNzZBKh7HtlOfmB+dC83kXyknSTqW4PDwCE+37yfq2GgpMFy4qqIuviy3+Cexu+7qnBgdnf3fE1JN6B8cYG1TU0up5f/RF+YvS4dMiVQuUpo0jwzyt617GB2bIJFIkEwmZ2uP8wUzA8BxHJLJJIl4nM6xcX56eC9d4+NIIUniUJ+Xz+a59W8FRieed75xJw2NH+wByxMnvP766+k91qE9Qv3DytzS12+YtxTXyHg1ieTl/jb+7vBuRkYjJOMJUukUrut8CMjZwlJKY9s2yWSSeDxO9+Awjx58m3dH+hFCorQgz/DyxcUXx0ot62e5DTXDr72z46Q5PrRF+OcP3M3wGwdjSjjt1XPLF/cnYmXdExFMKTCU5nA0wngsztxAFllIlFBoIYHMjqEm444/tmurAaVRKuOd0naKdDJBKhanZbCfn7Ts4c2RfiwhUBK8QnLjwovdK+fO+5vUyND/i0xOpjdeceXHA3nw3u9St3YJ195wQ89od19zfUnZxe2R4TlDiRgCA9sQHImO0jo+RJb0kmv5EUKjtItUKtNyy+jLrNqcWoM4SuFojZNOkUjHSSZTjE5G2dbZyo8PvkPz5Bhe18Q1BI5Q/KeaRr64+JJt/rT7TSucPVJTO+9D3+a0m7Yv/+oVmqoXsmrzhj4isd5gVtaVu/q6gjYSU4MHyXAiwbtjvQxOTZKjLQIYSCXQ04WS67qzRdNMQjiTODqOTSqZJJ1IMz4Rpfl4D08c2s2znYcZSdmYSASZeDInHOK2i9f0zhHyjqRX7stL2dz3g4c+xPPHFhrNb+/B6h/xiEsWf+/hvW/82fauNqQpEUphKBNHSBAO+R6LJYWlrCmcS31uAfnBAD7Lg2mYGDLDlNI6k3I4LjE7wVB0isMjI+wcPk7L6CCTtouWJqZSaOFiILCF5ktLV6S/VL7o7r6+w9/zZxe5ixYtPi2vn1gxtba243HcVXtU9Bd//Zvt5ZGUjdAKNf3PWoBCg1b4pEGRL0xFIERZKEhuIETQ9GJJScp1iDppxuJRjk9O0JOIM5SKkVYKY7oSnWlja2GC69JYmMe31l71yqIpcVPMa4wWza/4SD4/cUOqp72Nif7+5kWr176yrnr+Lc8f3ochjBlLQAAmAqRBSsDx+CTdsSn0qECiMQCJwAVcrXGlxFBuRqcNiRcJMy5CZH4INNISXFu/JFYdyvtpqtA/erBl78fyKfkE0skYjYuWpdKDo89uLq6OlIbDaHVKASQEQkikFpmixxBYQmCITErhohCAIQQeDYaUaDG9g4D+sLt2HZaXlLG6tOIlIxL599hgP+tXrj4/IFdcdx09vV0cbT/0erm2fnFNVSPKELNdJzEtG60UUmvkdNMzw56e3sSY/spCg9AfdLW0xgVOzJ+11vh9Hj5Ts6i9zAr8tZGdPzGmo5/E5icDAbjyus+w7PINMb/X98BlVfN21eflI5zpeIFA6Jm6e7qlJgRy+hkz18xzQGideS4EUoCBRIiZNoVm7dwqvTA39x/3P/3i7sMt+1has+jCAAFIxeLk1FW2VgdzHr5m3uK4tDJM2YZg9pTFCa7jxNhxIs2+Ik6QqGC2D18QCrK5urHFTKaezLt2tXvp+rVnxN8ZA2lY0sBYRxdiMrrtspKKA8tKy9BKY6qMIZ9KJ3agTjf+EDgpMDRcUVUXW5Jf/MiC+oYjPQdaz5S9T/ZaJ1IoFCS/sGBgbN/BZ66rWXzRwZERK5lykVLgCmaj+ScdKpt5PiutTKBhTl4eGyvr/7k8lPOPE5EpHcoJnzFvZywRgIKiQg4079fpaPSniwqK/nlteRUObsaAT2HyTGhWSkgMqbmiorZ9aajgh73RqamzAXHWQAAWXrSYvJqqsRzDeuCq2saj+SEfaXEGieLHkFI2C4tK2Di3boccibRET6gzPjUgQgiiExP0t3e1LAvnP725pt6R57j/IETGdQcMybXVC0ZyUu6z6ZL89PPbXv70gQAsaKgjv6LEtVLxv99YMb+5JisPpdVHGvNpQUzftVJcUl6llpaU/Xz40NFX2g4f4ltf+9pvBwjAwaNH+N6j328rSLkvXV1Vp6XIdGIzm8yZOCIRp70QAqkz7+Z6vWyuaeyo8If+rnLtsuSKNSvPiZ9zBrIifwWfuXaLGuzt+8UlReUHGguLUTiZyC6mAyWc9rIUOBKk67K2qpaGrMLtyb7e1uh45FzZOXcgOUtz6Ovr47KNG/aX+oIPfrZ2YSwgxGwy+XGkBBhKUxwOsrm0pssZHH18xDXS8+vqfvtAALZs2UJHWwfSTj+/srD8NyvKqkjhZg5ocpp2wwljJeGKeY3OPH/2jx9auXHXYFvb+bBydgHxdJRwk0wNp0dNL09dUVG3rnlowBdL24gTUhOR0bWM/WhwtaAmL48ry2rfyEq4P/tO615dVDv3vPg4L4kALKxvZDAxRsfx7udqvFkvXza3BqWd6XOM07lWpuRAanAlGAb8QXVjT2l23n16QVV/W3v7+bJx/kAAxvq7qKqundDjkz/aPKdmfE4whJrupQghZo8CagG4mqWFJawtr3qmeIQ3nLZeVl/V9PsB5PM3f5mh4930Hm19q8T0PrdxXqMrpJjtPcrpHqsjIGR6uGb+4tFy03puMJxwimrLLwQLFwYIQLAsl+VXbojlBPw/WD+3cv+8vHxcpTEBLSWGzlSEK6srWZSTv/3I7r27O48du1DLXzggq5atIpVMUJrr25eV4p8+M7fe9RiAkGg0ccOlJBjg2uqF49mm528aLm+KHRvo/f0DAlBVXc1vdrXqsfbj/7I4p2D/4uISUrgoAR6l2FA1n4Zgzq/s8Ym3xnp7+c9/fOPvJxCA/sgQazavb/Pa6Uf/sGphKmx6EK5LVTCHy+fWtqLc79tBf7S6ft75L/ZpArnhj66nreUAvlT6meWFpW9eWl4JQrOprtEtUOKJ79/+p3u624+ec8r/UfSpHGmIRiP0DU0R8phf2B8df+yV1pbgdfVLB9yB4c3CY+1rWr3qgq95wSUCEArl4BUCkVbP1ecW/NOXl67EOzH1diIabVOJsz8y+DsDAlBZXY7hlbGA5hFrcmq7dpO/zApZsdzcsythz5Q+1dMy7ugE0uOTA+MDxXEnFRcwUVNz7hnuf9B/0O+A/j/g0WcvaVKm0QAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wOS0wNlQwMzo0NTo0NC0wNDowMEdee90AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDktMDZUMDM6NDU6NDQtMDQ6MDA2A8NhAAAAAElFTkSuQmCC',
                monument: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGBDkhtw+5twAAGD5JREFUaN7tm2l0XdWV53/n3OGNmmdLsgbbkizJI2BjTIJtBhMIhKlJqtPdCdWVVKo6c0jSK0mlV1WnIJ2QASpVqYSETiUpOk4lGCoFIQQMLGMbsAFbnmVLsjXP0pPedN+995z+8CRZNjY2GFL5UHuts57evfftvf9nn73P3vtcCd4ivfLKKwASID8/X9fX12vP87As662yvCgSfyhBWuv58rQQb6/oi+a27d8eQStPKmGWNDW2lhcVlZYNjI8tFwErP5gTNTLaN7VA2IaFobXlJdP+9Nj4JJ7/WsC09wWCYsB1fbd2afO/D5CBl19j9/d+yA3fubewZ2Ly6lhq+otTUtX0+o7dFRuPDk8nZMxJkfA9PKWwDIOIbVMcjFAeiVCbW5Coi+T3lAejT5tO5v+Ndhxra756Y/yh//M3/OkXv/rOA+k+uBflpozc4ppFjmnf2uXEbtw91Ld6T8/JyNHxIQaT0zi+DwqEFqAFQgPoGY/SICAcsFgYyWNlWZVeXV410lBQ/GIJgR9ExmLP6lAwVd66+J0DsnP3S2ScdF5tTe3HBrz0R5/u7qj9XUe77JkYJ6N8tCExNKAlCoHWGi30nBSpJUJKhAKEwBMeGk3YsFhWUsH1dUvGr6isebguGH0gr6ToGBqEvDAVL+ip/pf2cnztSpp7Bhf36/SXn+3t+sDWQ3uDxydH8aSBEAZaa6TvIwUETJOIaZFnB4hIAwNQCOJuhknPYdr1cJVGozGkRPrgCB8jIFhbWqNub1yxd1NlzT+4oyOPVDctnbjrIzfxkx/928UBUd1jiA5HjLbY1x6OjX3tnw7sufTpE+3C87IWQGuEEuQFgiyK5rEsr5glObmUhkLkBkIEpIGhBb7QpHyPqXSS/lSS9ukpDkwOcyIxxbTvIoSBQKJ8TUHI5s7mZak761t+E05m/tfCpQ1HHvzu/Xz0M59+a0D2vLCd6bHRaPO6d3/g5ZGBL393z3O1B4cHsKVAa3CRlAYDbCisYF3FQppyi8gJBpBBG9O0kNJACBAi+7zWCt/38TwX38kwkUhxMDbC9qGT7JwcJp7S2AjSpouB5vpFS/Wfta59rjzh/s9P3XHfy//7p59g+dqVbw7IgdfacF45LHOvvfwLuxPjX7l/+9ORnuk4ljDw8bEMgyuLK7i5up6molIioShGyCZoWgRNC2EYSEOCgFlf10qhfYX2PRzlkci4qIzPdDxB23A/v+4+yt7YML7WSAwcobiqqoZPXbbhUKUM3r1hYeVvn9h7iJaVrw/V5tlAPP+zh8lf3oxRXrbqubH+Dz/w0nOR7vgUlhR42qfcDnPHwsVcX1VHfn4+gXCYYMDCtoOYhok0DITUiNl50qemTGuN0grb8wh7Ctf1iAQC5IYsGvLy2Hqig9/0dTDtuQSQ7DzRjSlfaP7suk3f2Ts43pcRuu2CLTI5GiPPCi747cDR73591zN3HB+fEJY0cfBYEs7hzxat4LIFVeRGo4QiEYLBIKZpYhoGQkoQ4g3XrEZnI5rSKN/H8zzSTppEIsHE5BTbejr5accBhtwMUkh85XJrQ6v+6PK1P8gcOvY5WZSfvGTNZafxNM4U0nuii2A6EelSmb95YN/OD77a32sEhImPZmEoxMcbL+PyqhoKCgvIyc0lGAxi2zamaSKlRJwFhFIqO2szaYlAIIRASomUEtOysO0ApmliGQaVoRyKjSAHY0MklI+tTI5MDYuccKh+Y/OqziWNSw5suvwKfvLPP5+TIecLfOLxx1laWyemg+H3P9F1+EPbu09YQR0gIzSFtsWf16/i8opKIgURcnNzCYVCWJaFYRicK3eKxWIcPnwY3/fP7qTiFKhgMEg0J4eC/Bw2VtfzoZpWIobAMzSeD48cait8dXzwS4MnupfWLG06jc8cEK01VQsX8nLb/nWHpye/tPVoWxTPx5easIAPVDVxZVUtwYIoeeEcLMuas8A5l5DWPPnkk7z40ksYhsEbkRACwzAIBAJEQxFy86JcXdvAjWX1GDpDQAv64kn+petAazIY+KyMxUIdR9tfD6Sns5tyOxrIKSr+yHPdxxd1TkwghcTFZU1BMdfU1hHNjRIORbHswNxMvhGQeDzO448/wdDQEG1tbezfvx/Xdc8LJhgMEIqEKMyJcnP9Upoi+fgoggheONHFnpH+252QfSWhAHt+9+TpQGzTIhSJLOlJTm96puswNgYaQYFlcUPtUkrzCghEQwQCAYzzWGKWPM9jdGyMLVt+yZ/8yQe5667/zr59+wBIJpOzqf3rl5ppYEVCRCJhFuUVcX11E4ZtIDFIOh6/7TxYkLED1/zrR+6WgynnFBDfSfDAr3eQkeqal4d7F5xMToOQCA1rCspZWVRBKBQiEAxiymxkOheQ+cpFIhFWLF/OwYMHOXz4EPFEHM/32b9/Pzt27JwLAmcCkdLAkCYyEsDOCXBFRTXN0QLSQiMRvDbYx7HE9LoPP/S9wqbVq08B6Tl8nE9etbhwys3ctHPgpImWuIYgYtpsKqsjJyeMEQpgGSaGkKg3MMZkLMbevXuZnp7Gtm3uuuvD3HzTzeTlF3DJ6tV0dXZxzz334vv+uf1GCEwhCJoWgYBNWU6UDcXVhAUYEsZSKdpG+htT0KjMbEVqAqTTPr5WlR2xycXtE2NYCDwU1ZEQTUXFWIEAQdNGSglSnB7qzqCAbbNjxw4efPBHrFmzhpqahdx22y1Mx6d5/IknePqZZ7jj9tu5/PK15/YVwJAGaPCtAFYwyKqiUgp7TYZTGRSKI2PDJekWY/3RcGTHodd2Y77yym7MUA6p6djS/tR08VQiiRQCS0FDfjEFOTnYpoVpGOeNUgDhcJjbbruN7u77+dKXv0Imk8GQkrSTJhQKc8ftt3H33Z8jPz//vD4mhMAwTSzLprSggJpoHsOJIQxD0jc5LganJ6+8Jpn4/kA0b1omk0mOdx6wfcu4+mQiFs74PgqNIaAxp4CgZWOY1nnD53yqqKjgxvfeiG3bjI4MU1lVyRc+/3l+/rOfcs89f0tNTc0F8ZmNYpZlEbUCLI7mg8wWbEOpBKPp5Gojv6C+sLgEUwpJOJJbGIxGr+jrn0ZpjRASyzSpCOVgmhJpXpg1Zh1dCEF7ezuDg4OAYNXKlXzmM58mFAq97rkLASMNg5CZrSgNQ6IVJHyXkUS8cGxicrGyzH1mWUkZlpQBHQ0H45l0NvXWkhwhKQgGkFJinWe/mKXe3l4effRRJmNTHD1yBAEIKeno7OS++76VTednfODyy9eycePGN7T0/DRGmoJ8O0RAGKSFj1Qw6aTk5NRUUAQDmK7nIqWplOc5rpuBGYVty8K2LaS8MBCQDbeOk+GhHz9Ed0/PnDLbt7/A9u0voLXGtm1ueM/1XHfdtRfMd3bjtUwLQwgQoDTEM2mUIZUSIH3Px3NdXymtldbMVA9IIZBCzgG7ECosLOQv//IveM97rj9tj9A6m+0qpSgrK+XzX/g8a9asyUbBCwEyzzqztQ0CPKUsT/mFnsqW2KA1UmbX4uwMuDPp9cyFcwpRSp2mdCAQIBwOn3O2TdMkGomcBtL3/bPu8qchgexzM19l1o8FSpsSgezp6+NYR4dOJ9PasmyEEJhak9CKZMbNzqavzyooHo/zwx8+yJYtvzwFGlBao5XKfs4bzFhlPq+Ojg7u/frXee211849WRp8DcmMg49GIJFak2MHMrnh0Hg0EMC0gwFcrT2UyhQHQsiZZeV5PqNOCuWpLKezUDqd5tHHHkMrzapVq6iqqsSyLIqLiqheWI3jZGbS92wkNE2T6upqTMvCcRzi8TjPbNvG97//AxYvWszqmXTjTNJK4fmKkUwKX2mQAkMLCsJhJxqNjPmGgWlYJp5nJNOpRGdNOPcSMWdGRVciRsb38LU/N6vzl0xeXh7Lly/n6d8/zd/ecw/RaITiomJ83+eW970v+6wQM78DpTSWabLlF1uYmp5icjLGyMgoZWVlNDU1nQVBdtl5vofjunQlYvhKIyTk2gHKIzmTedFIb8L3MLWUrLn0ssTJEyceXpCTd7VtmoW+r1Aajk+OEk+lieZ42L5/dufUmvfdcjN33H47vb299PX109HZgZNx8H0F85aRlBKCQfLy8mhd1kp1VTUZN8M3vvFNtH59Aqlm/MLzXOKJJCemJtEyW4EWRaIUWoFjYz09PZPKx9yw/kq6jhynKqdwe1q4bRXhyIYT8TimEHTEpxicmqa4IA/fz9bls8Fg1lFT6TT5Bfm0tLTQ0tKC7/vcf/8DbNv2LKFQGNu2kVLguS6pVJrCokI++YlPsGRJtiV6/PhxHMfBcZzTJkcDvlZ4vo9yXY5PjNGTTGIIia81i/OKyFdiz2ebWyfveuyxbNLoC03h/kPj5esu2b20uGxDV2wSKS2G00l2jpykobQUN+jOlbWz5Ps+yWSSdCqNUmrGYprevj5e3LULxBkW1Iq6+nrS6dTcJcdxSKVSryu4tNZoz0e7HsmUw/Oj3aRdF2GAKaGlpHxCJ5LP/tWxdp3nW9lEdmFDLT2XrNAB12+/sqLODRsClMITgueGu+mZjuFmMriue1rEUUqRTqdJpVLzrs9UjtI4tSvPDMTs5yk/y7guqWSK9DyLaJHl7bke6YzDwfFhXhrtQ4sswAXBEI0FxU8Xm4GdMuNR2VSXBWILC+m4mJ7a2ZpXcqKuqBglNTYG3fEpnj15lGQiiZvJ4M2L+Vpr0qk06bQzd00pdc5Gw9x979R9z3VxHAc3M88iCpTn42QcxpNxft/dzkTaAVOg0Vy6sC5Vl5v/aKppyfQzTz2R9b/Z357oPckL219oD7jprZtrG5WQYGgNyuTx/i72Dw7iJtM4roOnfHzf5+WXX+bI0aPsa9vHgQMHiMfjbN26lW3btqGVj/JPH2jF0NAQDz30EB0dHcRiMZ599jn6+vt5/vnnGR0dzeLwFY6bIZNM8mJvDzsG+zG1iYeiNBJiY23jgXIrsNMYHuZ/fOZuYF6n8cqNV/Hirl3e2MjIz9eVVd2yrbSsYe/QAEFtMZJK85OuNqqj+VRYAlNKMo7La3v3sXLlCqSQ7N23j2AwyKuvvkZLSwvLWls5W/9Pa83U1BRtbW0sWrSIvr4+rt+8mampKTo7OykuLsbxHDKJFEdGRnm4cz9xz0VIA8OH6xY2JFcVlP0w8VdfORH71Cfn+J4maevWrdx6662ip6P7k7umhr7x1y8+aSsHtGHi43NjZT0fa7mUkpJCIuHoaamFYRiYpkkmk+FCyJgp1GadXGud7VaaJlNTMToHhvlO2y72DPUipUEKn+aCYr586abfL8X6YFKKkarG+lP85jPfsmULfce7sCRdZeG81Y7Wi/aPD6GEQCLoTIyRyqRpiBRimxaWbWHbNnYgMNfnsizrgoZpmnNFUxaUwHM9EokEJweH+PtDL7NruB+JQCPJMy3+4tJ1iaZQztcqGupf/NcnfsPWrY+eHQjAp7/5VUaeP5RQwuusq65aNpBKVHbHJjGlwFCaI/FJJhJJqkI55GiBP5P9gMyGTE5vj56VNKA0Smn8Gad2UklS8QRtA338cP8edowOYAmBkhAQkve3rPavrV70Y2d0+O8np6Yym6659nQLnynjO1/7Bg3rl3PT7bf3jHX3720sr1zdOTmyYDiVQGDgGoKj8THaJ4bJlQEKrBBCaJT2kUplj9yya2VuycwfSik8pfC0xss4pDJJ0mmHsak4206084+HXmLv9DgB38Q3BJ5QvK++mf+67NJnQhn/bisnb7R+8aLXL9WzTdjvnnyKq+paWLt5Yz+Tib5Ibu61u/tPRlwkpgYbyUgqxcvjfQxNT5GvLcIYSCWyWe9MCFbz/vZnygLXdfE8FyedJpPKMBGLs7e3h58c3sMjJ44w6riYSATZrv2CnCgfW31F3wIhP5cOyLZCx+Xe+7/7Op3fsGra++IrWAOjtrh02X0PvPr8J7ad7ECaEqEUhjLxhAThUWRbLC+p4IqSahoLiimKhAlaNqZhZruSZFN7Xys8zyfhphiOT3NkdJSdI70cGBtiyvXR0sRUCi18DASu0HxoxWWZD1W1frW//8h9obxSv7V12Vl1PW/5197eie35a19R8V99a9e2qknHRWiFmvmxFqCyx1EEpUFpMIeF4SiV0QgF4SgRM4AlJY7vEfcyjCfj9E7F6EklGXYSZJTCmKlEZ4+xtTDB92kuKeSL6697qnVa/OdEwBgrXbLwnHqa5wPS09lBbGBgb+u69U9dWbfkTx870oYhjFlPQAAmAqSBI6A3OUV3Yho9JpBoDEAi8AFfa3wpMZSfXdOGJEA2P1Nz0yqyccoS3NS4PFEXLXzQKQmNHTrw6hvqed6iWacTNLeucjJDY49sLqubrMjJQaszUhAhEEIitcgWPYbAEgJDZFMKH5XtHgqBrcGQEi1mOghoXle2+R6XlFeyrmLhE8bk5O8TQwNsWLPu4oBcc8st9PSd5Fjn4eeqtPWrG2qbUYaYO3USM7bRSiG1Rmpm0vCsktkmxswsCw1CnzrV0hofmF+JaK0JBW3eW9/aWWmFv2XkFcXGdfx8ap4fCMC1t7yXVe/emAgFgt98V+2i3Y2FRQhvZr9AIPRsy2jm9Elk+8NZdWfG7H1AaJ29LwRSgIFEiNkGoGZ9da1uKSj4xf6HH99z5EAbK+pb3x4gAE4iSX5DTXtdJP+BGxYtS0orq5RrCObespgXOk5rOsyjuUfEPIsK5s7hi6MRNtc1HzDTzs8Kb1rnX75h/QXpd8FAmpY3Md51EjEVf+Zd5QsPrqqoRCuNqbKOfCadeaL1RidcAmYaCnBNbUNieVHZ3y1tbDrac7CdC6XzRq35FI1GKCopHhxvO/TrW+qXrTw0OmqlHR8pBb5gbjc/Xwdxfqk8h0RpFhQWsqmm8ZdV0fxfxCandTQ/54J1u2CLABSXlnBw736diccfbC0u/eX6qlo8/KwDn6HkhdCclZAYUnPNwsWdK6LF3+uLT0+/GRBvGghAy8plFNbXjucb1jevW9x8rCgaJCMuIFF8A1LKpaW0nE3VDdvl6OSB+NjYm+bxpoEIIYjHYgx0njywKqfo4c31jZ5Ub5bLKV4aCBuSm+qWjuY7/iOZ8qLMY8/87p0HArC0qYGiheW+5ST/edPCJXvrcwtRWp33uPo0EDOfWikurapVK8orfzpy+NhTHUcO88WPf/wPAwTg0LGj3Pf9b3cUO/4T19c2aCkEmtnzj+w+IhFnHQiB1NlnCwIBNtc3dy0MRf9vzfpV6cuuWPOW9HnLQC4ruoz33nSnGurr/9WlpVUHm0vKUHjZnV3MbJRw1mEp8CRI32d97WKacku2pfv72uMTk29VnbcOJH9FPv39/bxr08b9FcHId25e3JIICzGXTL4RKQGG0pTlRNhcUX/SGxr70ahvZJY0NPzhgQDceeeddHV0Id3MY2tKqnZdVlmLg599QRNe7zPzvisJ1yxq9haF8v7xu2s27R7q6LgYVd7chng2SvlppkcyY2aAn1+zsOHKvcODwUTGRcxLTUR2rWX9R4OvBfWFhVxbufj53JT/T/e0v6pLF1dflB4XZRGAlsZmhlLjdPV2P1ofyP3du6rrUdqbeY9xJtfKlhxIDb4Ew4D31DX3VOQV3quX1g50dHZerBoXDwRgfOAktXWLY3pi6h82L6ifWBCJzhxzzyylmfd3tQB8zYqSctZX1f66bJTnvY4+1l131R8HkA/8t7sY7u2m71j7jnIz8OimRc2+kGKuWS1nzlg9AVHT5oYly8aqTOvRoZyUV7q46u1Q4e0BAhCpLOCSazcm8sOh+zdU1+xfVFiErzQmoKXE0NmKcE1dDa35RduO7nl1z4njx98u8W8fkLWr1uKkU1QUBNtyHba8t7rRtw1ASDSapOFTHglzU13LRJ5p/7jp3Vcljg/2/fEBAaitq2PX7nY93tn7L8vyi/cvKyvHwUcJsJViY+0SmiL5T7oTsR3jfX38l//0/j9OIAADk8NcsXlDR8DNfP/G2hYnx7QRvk9tJJ93Vy9uR/nfdiOheF3joosX9k4Cuf22W+k4cJCgk/n1JSUVL1xeVQNCc3VDs1+sxE++/ak/f6W789hbTvnPRe/IvybF45P0D08Ttc0P7o9P/OCp9gORWxpXDPqDI5uFbbVdtW7txQs5g952iwBEo/kEhEBk1KONBcVb7lqxhkBs+sVUPN6hUul3QuQ7AwSgpq4KIyATYc3fWVPT27Sf3pobtRIFBW+uhL1Qekf/680fiyHtoBycGCxLek5SQKy+/q1nuP9B/0H/DvT/ATcxOAPjc/trAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE2LTA5LTA2VDA0OjU3OjMzLTA0OjAwMDRF7wAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNi0wOS0wNlQwNDo1NzozMy0wNDowMEFp/VMAAAAASUVORK5CYII=',
                stadium : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGAx8zgiddXgAAGYNJREFUaN7tm2l0XNWV73/n3HtrUFVpKo3WLFuSLctjPCBjPMgmEIwJZIAk5CWdpp3OTAJZJAs3HcjLCnkdQggZaJImjyaElxCmhA4JGBMTg8F4wJaNbMuWZEnWPEs1173nvA8lCRtssDGk86H3WlelWnVr7/0/Z8/nluAd0p49ewAkQGZmpq6srNS2bWNZ1jtleV4k/laCtNYny9NCvLuiz5vbc//1GFrZUgkzd3ZNXUEwmJffMzw0X7itTE/AbyS0Y2qBcBkWhtaWHYk5E0PDo9jOq27Ttd/tET3JpJMsn1P73wOk55VX2fWTn3PZD2/P7hwZXTcWnfjGuFRlJ5y4q21s2N8/EZZj8Shhx8ZWCssw8Llc5Hh8FPh8lKdnhSt8mZ0FHv+zZjzx/wZbjjbWrlsb+uX/+Tb/+I1/fe+BdLy2D5WMGuk5ZTPjpuuqtvjYhl19XYt3d7b7jgz30RuZIO44oEBoAVogNICe9CgNAtLcFqW+DBbmF+vFBcUD1Vk5L+fivtc3NPYX7fVEC+pmvXdAduzaSSIeyygvK/9cjx377LMdLeVPtzTLzpFhEspBGxJDA1qiEGit0UJPS5FaIqREKEAIbGGj0aQZFvNyC7m0omp4RVHZQxUe/90ZucGjaBDy7FQ8q7u6d+7j2PKF1Hb2zurWsc1/OdH2sceb9nmOjQ5iSwMhDLTWSMdBCnCbJj7TIsPlxicNDEAhCCUTjNpxJpI2SaXRaAwpkQ7EhYPhFizPK1Mfrlmwr6Go7GfJwYHHSmbPGfnMpo3c/x//dX5AVMcQoiUuBue6Lj40NvSd/zy4e8mzx5uFbad2AK0RSpDh9jDTn8G8jByqAunkeb2ku724pYGhBY7QRB2b8ViE7miE5olxDo72czw8zoSTRAgDgUQ5miyvi6tr50Wvrpz7ZFok8a3SOdWHf3HXj/js1776zoDsfmE7E0OD/tr6VR97ZaBn8127t5W/1t+DSwq0hiSSPI+bNdmF1BeWMjs9SMDjRnpcmKaFlAZCgBCp+7VWOI6DbSdx4glGwlFeGxtge187O0b7CUU1LgQxM4mB5tKZc/Q/1S3fVhBOfvP6j9zxyv9+4MvMX77w3IAcfLWR+J5DMv3iC27aFR7+lx9tf9bXORHCEgYODpZhsDKnkCtKKpkdzMPn9WN4XXhMC49pIQwDaUgQMOXrWim0o9COTVzZhBNJVMJhIhSmsb+bRzuOsG+sH0drJAZxoVhdXMb1S9c0FUnP19eUFv3pqX1NzF345lBtng7E8796iMz5tRgF+Yu2DXX/w907t/k6QuNYUmBrhwJXGh8pncWlxRVkZmbiTkvD47ZwuTyYhok0DITUiKl10q8vmdYapRUu2ybNViSTNj63m3SvRXVGBo8fb+HJrhYm7CRuJDuOd2DKF2pvqG/44b7e4a6E0I1nvSOjg2NkWJ4Zf+o5ctf3Xtr6kWPDI8KSJnFsqtIC/FPlApbOKCI9PR2f34/b7cYwDEzDQEgJQrylzWp0KqIpjeM4OEmbWDJOOBRiZHScrZ2t/KrlIH3JBFJIHJXkquo6/dn5y+9NNB29UQYzI+9btvQUnsYbhZw43oYnFva1qcS3796/49q93ScMtzBx0JR6vXypZimrZ1aTFcxGCMGxY8cIBAL4/X6klIi3AZFaPYEQAiklUkpi8ThHjhzB5/OTk51NZXqQTEwOjvUSVg4uZXJ4vF8E0ryVa2sXtVbVVB1suGAF9//6wWme8mQBT/3xj8wprxATnrRrnmo79OntHcctj3aTEJpsl8U/Vy6ifkYx2XnZtLa2cuutt7Jp0yZefvll3mntNLUYX/ziF9m8eTO79+whvyCXhtKZfLqsDp8hsA2N7cBjTY3Ze4d7b+493jGnbM7sU/hMA9FaU1xayiuNB+oPTYze/PiRRj+2gyM1aQI+VjyblcXleLL8oBQPPPAr7rnnHqSU1NTUnFFRpVKRSik1VTi+iUpLS8nNzeXBBx/kpz/9KeGJEOmZftaVV7MhvxJDJ3BrQVcowu/aDtZFPO4b5NiYt+VI8zSPaWfvbO2gwOV3J/yeTduads9sHRnBkAZJkqzMymd9eQX+dD8+Xzotx1p45pmnAbj88supqKgAIBQK0dXVRVtbG+3t7fT29jI+Pk4ymcQ0Tfx+P3l5eZSWllJZWUlJSQmBQIBgMMiHPvQhtm7dyo4dO9izdw8rVtQTjCe5onIOh0Z7aQpN4MHgheNt7C7t/vBib9rD0uPesvvpP7PkkktfB+IyLbxus6olMtqwte0QLgxsBFmWxWXlc8jLyMLtT8MyLZ599llCoRCLFy9mw4YNtLe38/TTT7NlyxYaGxvp6+sjkUiknNAwTtkdrTWmaRIMBpk7dy5r167lAx/4AKtWreKiiy7i0KFD/PFPT7H8ohWk+XzMTAS5tGQ2R1r2IhIGkbjNn1pfy6pbuHL9M5u+vrXq859U0zvixMPc8tOnuPFjF61/pf/EjPbIBC5hIbRmWVYBC4OFeL1e3B4PKMWxY8f4xCc+wWWXXca2bdv4zW9+w+HDh/H5fFRXV7Nq1SqqqqooLCwkIyMDy7JwHIfx8XH6+vpoaWmhqamJvXv3snXrVu69914++tGPctNNN9HU1MRf//pXkokklt+Ny3azorCELX1tvDoyhqkFr/Z2cTQ8Uf8Pv/xJ9pCTGJwG0nnoGF9ZPSt7PJnYuKOn3URLkoYgICwa8isIBNIwvG5cpolyHLQQDA0Ncd999/HII4+Qk5PDpk2bqK6uZvny5SxevBiARCKBbds4joOUEsuysCwLKSVHjx7lueeeo7OzkyeeeIIf/OAH7N+/n7q6OhACA4nHtFBuF/kBP2tySjgyOoYjYCgapXGgu2ZZZn6NMq3XgcRiDo5WRS1jo7OaR4awENgoSnxeZgdzsNxuPKYLKSVaa4TWPPzwwyQSCdatW8fmzZupra3ltttu46WXXqKuro729naGh4eJxWIopZBS4na7ycjIoKysjJ6eHkZHR7nzzju59tprueOOO/jd737H9u3bWbNmDYaUWNLEZbmxPB4WBfPIPmHSH02gUBwe6s+NzTUuPJLme7Hp1V2Ye/bswvQGiE6MzemOTuSMhyNIIbAUVGfmkBUI4DKtVLITYrJu0oRCIdatW8d3v/tdBgcHufnmm3nxxRc5fvw4sViMCy64gBUrVhAIBDBNE8dxCIVCDAwM0NjYyJNPPkkwGOSb3/wmV155JTfffDMADzzwAI7jTMsyTBPLcpGXlUWZP4P+cB+GIekaHRa9E6Mr10fC9/T4MybMSCTCQM8JV+GM8nXto2NpCcdBGgJTCGoCWXgsF4ZpYUwCmQrVpmmyePFifvnLX/Lqq69SX1/P7bffzn333cfSpUu56aabzphbHnroITo7O7n++us5cOAAP/7xjykpKWH27NkEAoHpMC2EwDAMLMvCb7mZ5c9k10AfQgv6omEGY5HFRlF5ZbZy9ptSSNJ86dkev39FV/cESmuEkFimSaE3gGlKpGkg5eu5UwiBbds8/PDDNDQ0cNddd7FgwQJM06Snp4cHH3yQhQsX4jgOY2NjJJNJDMMgPT2dQCDAo48+yooVK7j88su54ooruOaaa7j33nu5//77iUQipyyAEAJpGHjNVEdpGBKtIOwkGQiHsodGRmcpy9xv5ufmY0np1v40TygRS5XeWhIQkiyPO+Wkk9v8RjIMg02bNlFXV8fu3bvZtm0bL774Ivv37+erX/0qM2fOxOv1YhgGSilisRidnZ20tbUxNDTEbbfdxurVq1m+fDlf+MIXeOGFFzh69OipGXuyjJGmINPlxS0MYsJBKhiNR+Xo+LhHeNyYSTuJlKZSth1PJhMwqbDLsnC5LKQUZzSRzs5OvvWtb5Gbm0tbWxvV1dVs2LCBWbNm0dzczN13301ubu60X4XDYTZv3sy8efNoaGhg165d3HrrreTl5SGE4MCBA2csY4QQWKaFIQQIUBpCiRjKkEoJMB3bwRbaQXm00pqpmlsKgRRyGtjpKJlMsmXLFoQQrFq1iiuuuIKVK1cyNjZGU1MTRUVFeDye6ft9Ph8f//jHCQaDVFRUUFpaSn9/P3/4wx+wbfuMcsSUiUk53dsgwFbKspWTrZTEFCnvRcqULU6tQNJxXmf+NgWh1prt27dz4MABqqqqqK2tpbq6muHhYdLT07EsC9u2p6PWsWPHaGpq4siRIwwODuI4zlvynyqnHceZbm1kyo8FSpsSgdnZ1YUTi+syT622LBdCCEytCWtFJJFM9Q1Oqn94qwpXKcXw8DA7d+5k586dSCkxTRPTNKfzj23bJJNJlFKcCykNjoZIIo6DRiCRWhFwuRPpad5hZVmYLo+bpNY2SiVy3F4kIIXAth0G41GUrVKczpGUUiQSiema63xIK4XtKAYSURylQQoMLchKS4v7/b4hxzCQhmUiLCMSi4Zby9LSp63IcRRt4TESjo2jnTOW4O81OY6D7djEk0nawmM4KmUdAZebAl9gNMPvO+F2mUgtJcuWLA1biIdmBDKGXaaJEmADx0YHCUVjJJ1UvTRlXu/1xN2yrFSkQ09PXULhCMfHR9GTA7ugz0+25T461NnZ2XviBHLNhSsZ6R+mOJC9vTI9u7EwzYc9GbFaQuP0jk9gJxPTQAzDoKKiAtM0z1PdM1N5eTlutxtbKWzHQSWTHBsZojMSwRCpKeasjCCZSuy+obZutLPleKpDdIQm+0DTcIEvfdecnHy0YyOFoD8WYcdAO4lo4hQnbWhooKqq6qxWdt68eXzwgx9k2bJlp4TiM1F+fj4bLtuAZVlo20EnbSLROM8PdhBLJhFoTAlzcwtGdDjyl1uONuu5NfNTQEqry+l83wLtTjrNKwsrkmmGAKWwhWBbfwedE2MkE6+Dqaur44YbbiA3N/eMCpmmyac+9SkeeeQRfv3rX/Poo49y44034vV6z/idtLQ0Pv/5z7N6zeqUSSVtYok4rw33s3OwCy1SoX6Gx0tNVs6zOaZ7h0zYFM2uSAFxCQsZT2LaakddRu7ximAOSmpcGHSExvlL+xEi4QjJRALbcTCkwSc/+Um+973vUVNTc9qwXFVVxQ033EB1dTU+n4/i4mK+/OUvU19ff1oQRUVF3HLLLXzta1/D4/Hg2DbxRJzhSIgtHc2MxOJgpvxmSWlFtCI984no7KqJrc88BZw0DvrQVVdx+EDTSEFBcAYed/2u3k5hKoHWkq7YBDW+IIVpfrBS+cFlWcyfP58VK1ZgGAYDAwOEw+Fp81uyZAnXXXcdbrd7WlmPx8Mrr7zC7t27gVQdlZ+fz8aNG/n2t7/NNddcg8/nQytNLB4lFgqxreM4j7QcwnEECanIT/PyqQX1++p8GXfa4dBow/r3pyxgSsjKtat5+aWX7KGBgQfr84uvfC4vv3pfXw8ebTEQjXF/WyMl/kwKLYHLspDChWEYLFmyhLq6Oq677jq2bdvGjh07aGpqIjc395SKGVIVQ3Z2NlVVVVRVVbF8+XLWrl3LwoULCQQCQMp0bMcmHopxeGCQh1oPELKTCGlgOPD+0urIoqz8n4dv+ZfjY9d/5XXeJwt6/PHHueqqq0RnS8dXXhrv+7fbXv6zS8VBGyYODhuKKvnc3CXk5QXxuL3TbevJDVc4HGZgYADbtqmsrDxl+KC1pqOjg3g8Tn5+PoFAYBrs1NgomUwSi8U43jfAnft3sLvvBFIaRHGozcph85KGLXOwro1IMVBcUznN+5RJ429/+1u6jrVhSdry0zIWx7WeeWC4DyUEEkFreIhoIkZ1WjaWNBBGqrgU4nUwLpeLrKwsgsHgaXckMzOTYDCIx+OZ9i3HUdh2kng8TiQSoaOvn581vcKO/m4kAo0kw7T4/JL68Gxv4DuF1ZUv/+GpJ3n88SdODwTgq9//VwaebworYbdWlBTP64mGizrGRjGlwFCaw6FRRsIRStLSSUeihEILCaTqKY2aVvqMpAGlUUrj2A6JZJxELEo8HOFgXw8/P7iHFwZ7sIRASXALyTVzFzsXl8y8Lz7Y/9PR8fFEw/qLT2H5JiA//M6/UX3hfDZ++MOdQx3d+2oKiha3jg7M6I+GERgkDcGR0BDNI/2kSzdZlhchNEo7SKVSR24pO5o2p5MvpVQq0WmNnYgTTUSIxeIMjYfYeryZf2/ayb7xYdyOiWMIbKH4YGUt/2vekq3ehPN1K5AxWDlr5pvWxjjdgj3952dYXTGX5Zes7WY03OVLT794V3e7L4nE1OBCMhCN8spwF30T42RqizQMpBJopU4Zk07970y2BclkMmVGsRiJaIKRsRD7TnRy/6HdPHb8MIPxJCYSQWpqPyPg53OLV3TNEPLGmFs2ZseT3P6ju96k81s2Gvte3oPVM+gSS+bdcffe57/8XHsL0pQIpTCUiS0kCJugy2J+biErckuoycoh6EvDY7kwDRNDppRSWuNohW07hJNR+kMTHB4cZMfACQ4O9TGedNDSxFQKLRwMBEmh+fSCpYlPF9f9a3f34Tu8GXlOXd280+r6tiP05uZWXLazfI8KPfKDl54rHo0nEVqhJr+sBajUcRQeaZDnCVCa5qfI7yMrzY/PdGNJSdyxCdkJhiMhToyP0RmN0B8Pk1AKY7ITnTrG1sIEx6E2N5tvXPj+Z+omxCfCbmMor6r0jHq+beXX2drCWE/Pvrr6C59ZWVH1j78/3IghjClPQAAmAqRBXMCJyDgd4Qn0kECiMQCJwAEcrXGkxFBOyqYNiRsJUyFCpP4INNISbKyZH67wZ/8inusdajq49y31lLwN6ViY2rpF8UTf0GOX5FeMFgYCaPWG1nQyBEstUk2PIbCEwJgqxVEIwBAClwZDSrSYnCCgeVOn49i8r6CI+sLSp4zR0S3hvh7WLKt/Sz3fFsj6K6+ks6udo62HthVr65HLymtRhpg+dRKTe6OVQmqN1IDWk+rpySHG5CoLDUK/fqqlNQ5wcuOrtcbrcXF5ZV1rkZX2AyMjODasQ2+n5tsDAbj4ystZtGpt2Ov2fP+i8pm7arKDCHsyXyAQempkNHmkJgRy8jOmrqnPAaF16nMhkAIM5HRSBc2FJeV6blbWbw489Mfdhw82sqCy7t0BAhAPR8isLmuu8GXefdnMeRFppZRKGillJlGdsrJT18k0fYs4aUcF0+fwOX4fl1TUHjRj8V9lb6x3Llhz4Vnpd9ZAZs+fzXBbO2I8tPWigtLXFhUWoZXGVClHfiNNlSxTGf6N798ETgoMDevLq8Pzg/k/nlMz+0jna82cLZ1Tv+r3+wjm5vQONzY9emXlvIVNg4NWLO4gpcARTGfztzsYPXkYPo1EaWZkZ9NQVvNwsT/zN2OjE9qfGThr3c56RwBy8nJ5bd8BnQiFflGXk/fwhcXl2DgpB36DkmdD07uExJCa9aWzWhf4c37SFZqYOBcQ5wwEYO7CeWRXlg9nGtb33z+r9mjQ7yEhzqJQfAtSKsncvAIaSqq3y8HRg6GhoXPmcc5AhBCExsboaW0/uCgQfOiSyhpbntvg8BReGkgzJBsr5gxmxp3HEgXBxO+3Pv3eAwGYM7uaYGmBY8Ujv24ordpXmZ6N0uqMznxaEJOvWimWFJerBQVFDwwcOvpMy+FDfONLX/rbAAFoOnqEO+65syUn7jx1aXm1lkKgJ5NjyupTpcnpLoRA6tS9WW43l1TWtpV6/f+37MJFsaUrlr0jfd4xkKXBpVy+8WrV19X9yJK84tdqc/NR2KnMLiYTJZz2shTYEqTjcGH5LGan5z4X6+5qDo2MvlN13jmQzAWZdHd3c1HD2gOFHt8Pr5g1N5wmxHQx+VakBBhKkx/wcUlhZbvdN/Qfg46RqKqu/tsDAbj66qtpa2lDJhO/X5Zb/NLSonLiOKkHNOHNPnPSeyVh/cxae6Y349/vWtawq6+l5XxUObeEeDqKOjEmBhJDppsH15dWr9zX3+sJJ5KIk0oTkbK1lP9ocLSgMjubi4tmPZ8edf7zu817dd6skvPS47x2BGBuTS190WHaTnQ8UelOf/qikkqUtqfHQ1pPPu+rQWpwJBgGfKCitrMwI/t2Pae8p6W19XzVOH8gAMM97ZRXzBrTI+M/u2RG5cgMn3/ymHvSlCaf39UCcDQLcgu4sLj80fxBnrdbuqh//+q/DyAf+9Rn6D/RQdfR5hcLTPcTDTNrHSHF9NmjnDxjtQX4TReXVc0bKjatJ/oCUTtvVvG7ocK7AwTAV5TF+y5eG85M8/5oTUnZgZnZQRylMQEtJYZOdYTLKsqoyww+d2T33t3Hjx17t8S/e0CWL1pOPBalMMvTmB7nt5eX1DguAxASjSZiOBT40thYMXckw3TdN3vV6vCx3q6/PyAA5RUVvLSrWQ+3nvjdvMycA/PyC4jjoAS4lGJteRWzfZl/To6MvTjc1cUnP3rN3ycQgJ7RflZcsqbFnUzcs6F8bjxguhCOQ7kvk1Uls5pRzp1JnzdUUTPz/IW9l0A+/KGraDn4Gp544tH35Ra+cEFxGQjNuupaJ0eJ+++8/p/3dLQefccl/5noPflpUig0Snf/BH6Xee2B0Mi9zzQf9F1Zs6DX6R24RLisxtX1y991me/6jgD4/Zm4hUAk1BM1WTm//cyCZbjHJl6OhkItKhp7L0S+N0AAyiqKMdwynKb5sTU+8Zx2Yo+n+61wVta5tbBnS+/pr96coTGkyyN7R3rzI3Y8ImCssvKdV7j/Q/9D/w30/wHREW5hVEYuDQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wOS0wNlQwMzozMTo1MS0wNDowMPkc4R0AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDktMDZUMDM6MzE6NTEtMDQ6MDCIQVmhAAAAAElFTkSuQmCC',
                theater : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGBDog6yXa4gAAGO5JREFUaN7tm3l4HcWV6H9V3X37bpKudLVakrVYlmRJXgE7xg5ewDg4MQ8exIlJ8ognCTP5SF5IhgwkPCbflxleeEmGECaP7IQwhCGMwfBYgs1iwEAAGUex5U22LFuyJGvXlXSXvr3U++PKwsZgDDaZ/DHn+1pb9606vzqn6pxT1RJ8QHnzzTcBJEAkElHV1dXKcRwMw/igTZ6ViL9UR0qpE/tTQpzbrs+6teefeATlOdITekF9XVNxNFpY1Ds8NEeYRsSfFdbSytWVQPg0A00pw0mk3PGh4VEc90+m7vuz6Re9tu3albMa/nNAet/4E80/+QVrfvS9vK6R0YtjyfGbxqRXcdS1fB2x4XD/eFzGrCRx18HxPAxNI+Tzke8PURwKUZmdG68KRbqK/eFndSv974PtB3Y2XLxi4p7/813+5qZ//PBBOne34NlJLTu/Yoal+67ssGIfb+7rXrC960ho/3AfxxLjWK4LHgglQAmEAlCTM0qBgKBpMD2Uw7yiMrWguGygNjf/tQLMn4eGYltVwJ8sbqr58EBebX6dtJXKqayo/LteJ3Xds53tlZvb22TXyDBpz0VpEk0BSuIhUEqhhJrqRSqJkBLhAULgCAeFIqgZzC4o4WNVM4cvLK14oMofviunIHoABUKemYpn9FTP6y0cXDSPhq5jNT0qdcvWox2f3rSnxX9wdBBHagihoZRCui5SgKnrhHSDHJ9JSGpogIdgwk4z6liM2w62p1AoNCmRLljCRTMFiworvKvq5rasLK242x4ceKS8ftbIhi+t5d5fPXF2IF7nEKLdEoONvlV7Y0P//NvW7ec/e7hNOE7GAiiF8AQ5pp8Z4Rxm5+QzMyubwkCAbDOAKTU0JXCFIuk6jKUS9CQTtI2P0Traz+H4GOOujRAaAonnKnIDPtY1zE6uq258PJhIf2f6rNp9v7zzx1z39Rs+GMj2l7cxPjQYblh80affGOi95c7tL1Tu7u/FJwVKgY2k0G+yPK+ExSXTqc+OkuU3kX4fum4gpYYQIETmeaU8XNfFcWxcK81IPMnu2ADb+o7w6mg/E0mFD0FKt9FQfGzGLPXFpkUvFMftm7929Q/f+Kf7vsqcRfPeH0jrn3ZivblXZq/6yD80x4f/14+3PRvqGp/AEBouLoamsTS/hMvLq6mPFhIKhNECPvy6gV83EJqG1CQIOD7XleehXA/lOlieQzxt46Vdxifi7Ozv4eHO/bTE+nGVQqJhCY9lZRV87YLle0ql/8bl00v/8FTLHhrnnbpU6+8E8eK/PUBkTgNacdH8F4Z6Pn/X6y+EOifGMKTAUS7FviBXT6/hY2VVRCIRzGAQv2ng8/nRNR2paQipEMfHSb01ZEopPOXhcxyCjodtO4RMk+yAQW1ODpsOt/N4dzvjjo2J5NXDnejy5YZvLF75o5Zjw91poXaesUVGB2PkGP5pf+jdf+ftf3zu6oPDI8KQOhYOM4NZfHHGXC4oKSM7O4tAKETA70fTNHRNQ0gJQpzWZxUqs6J5Cs/1cByHVDpFMp5geDTGc53t3NfeSp+dRgqJ69lcWdukrpuz6OfpPQf+XkYjifMWXnBSm9rbOzl6uAN/Kh7q8NLfvevPr35mR89RzRQ6LorpgQBfqbuAxWUV5ORGyI5ECAWDGIaBpmlIKRHvAZEZPYEQAiklUkp0Q8c0TXymiekzmBbIIipNdsf6iHsuPk9n31i/yAoGqlc0zD80s25m68qPXMi9v7v/nV3rqSefZFZlldjX0/uppw7tvnZb52HDr0ws6ZJvGPxt9Xw+UlJGKJpDTigbw2dkFD+LvOnEz/p8PkRWFgjBSm8GcdviZx1/JonCdeGRPTvzFuRN+/b8VLKlYlb93hPbkVPmVoqy6dN5Y+euxXvHR7+9af/OMI6LKxVBAZ8uq2dpWSWB3DA5WVn4TN9ZQ7wdSAiBYRhkhcPk5GZzcWUtHy+qRlNpTCXonkjwHx2tTQm/+Q0ZiwXa97edCtJ1qJNiX9jMiuZ/6YXOgzMOjYwghcTGZmFuPpdUVhHODhMIhtE145TRPNcwgWCAvKwwl1fPoj4UwcXDj+Dlwx1sH+i5ygr4lhIw2b756ZNBfLpBIBSa2ZUYX/lcx158aCgEuYbBmspZFObkYoYDmD4f8gwt0d7ezgMPPMCxY8feFwxCYAT8hEJBZuRE+Vh5PZpPQ6KRsBz+cGh3btpnXvL/vnSjPJa03gJxrTh3PfwKaeld8kb/0WlHEuMgJELBwtxi5kVLCAQCmH4/utRAyvdUaN++fXzrW9/iq1/9Kps3b0YpxY4dO9i2bRuO45weBpBSQ4ZMfFkmF5aU0xDOJSUUEsGfjnVzID6++PP3/CSvfsGCt0C69h7kfy6ryRuz02tf7T2ioyS2JgjpPlYWVZGVFUQLmBiajiZkZunMFEqniGVZbN26lRtuuIFNmzYxPDzM888/T0tLCzfddBNf//rXaW1tfS+zoCPw6wam6aMoK8zy/HKCAjQJQ8kkOwd66pJQ5+kZN9cBUikXV3ml7bHRmraRIQwEDh7loQD10XwM08Sv+9A0DdfzeOrxp+jq6qKpqYnS0lL8fj+WZXHkyBG2bNnCxo0b6ejowDRNPM9jx44dPPHEEzQ3NzM+Ps6vf/1rbrvtNrKzs9+VRUqJoXRcw8Tw+5kfLSTvqE5/Mo2Hx76h/oJUo7ZkfzD0yp4/NaO/+WYzeiCL5HhsVk9yPH8snkAKgeFBbSSf3KwsfLqBPhknUqkUv/vd79i0aRP5+flEo1ECgQCWZdHf38/AwADhcJjPfe5zrFixgq1bt/L0008Tj8eJRCLEYjEefPBBGhsb2bBhA6Zpnna+aLqOYfgozM2lIpxDf7wPTZN0jw6LY+OjSy9JxH/aG84Z1xOJBAO9R30l0yovPjIaC6ZdF6kJdCGoy8rFb/jQ9EzAAzAMg8LCQmzbpre3l3g8TjAYJBaLkUwmEUKwbt06br/9dgD27t3LxMQEhmHw7W9/mxdffJEnn3yS2267DcdxWL9+PdFo9N1BNA3DMAgbJjXhCM0DfQgl6EvGGUwlFmilldV5nvtnXQpJMJSd5w+HL+zuGcdTCiEkhq5TEshC1yVSz1jjOEhTUxOmabJgwQJuuOEGKisreemll7j99tsZHh4mmUyyadMmtmzZwubNm4nH41iWxTXXXMMnP/lJysvLueOOO7j11lvZtm0bl19+ObNnzyY/Px/TNDEMg1AoNJUpSE0joGcqSk2TKA/irs1AfCJvaGS0xjP0P+tFBUUYUpoqHPRPpFOZ1FtJsoQk129mfPVty+2cOXMoKiqitraWpqYmNE0jGo1imiZKKR566CE2btyIZVlTi0J/fz8PPfQQc+bMYfny5fzmN7+Z+tuTTz7JtGnTKCoqIhgMkp+fzze/+U3mzZs3lcZIXRDxBTCFRkq4SA9GraQcHRvzC7+Jbjs2Uuqe5ziWbadhUmGfYeDzGUh5asyoqamhoaGBhx9+mObmZpRS9Pf3Mzw8DEA6nT7FTZRSPPPMM9x5551Eo1FisRimadLY2EgwGGT//v0cOHAAgPLycq6//vqTXEwIgaEbaEKAAE/BRDqFp0nPE6C7josjlIvnV55SHM+5pRBIIafATpRoNMqqVavYunUre/bsedfJqus60WiU4eFhwuEwPT097Nq1a+p+bW0td9xxB7NmzeKll17illtuoa2tjRUrVtDY2HhSXDmeZB6vbRDgeJ7heG6e50kypb1SSJnxxeMjYLvuW4HrbTBSSi677DIaGt59LyoSibBu3TrWr19PVlYW+fn5DA4OnvRMb28v999/Py0tLVRWVlJRUUFJSQnr168nJyfnhFmf+ea67lRpIzPzWOApXSLQu7q7cVOWqvA3KMPIJIK6UsSVRyJtZ4KfmwmAJ7pYbW0tn/3sZ9m/fz9CCObPn59xSZ+PmpoaLrroIoqKivj+979PJBLh/PPPJx6P09vby+HDh3Fdl/Hxce655x4effRRIpEIAwMDXHvttSxduvQkYE+BqyCRtnBRCCRSeWT5zHR2MDDsGQa6z29iK+Xgeel8M4AEpBA4jsuglcRzvExLbxNN01i/fj3Nzc08/vjjNDQ0sGbNGqLRKLZt09rayt1338327dv5whe+gN/vZ+3ataxZs4Z7772XjRs3kkwm8TyPwcFBBgcHufDCC7nuuusIh8Mnzy/Pw3E9BtJJXE+BFGhKkBsMWuFwaMjVNHTN0HEcLZFKxg9VBLPPE1Nm9OiIx0i7Dq5yp9KSE61SUlLCrbfeimVZPPjggzzzzDOEw2FisRh9fX2k02lWrlzJsmXL+O53v8vAwADXXHMN119/Pbquc9999+G6LgAzZszg5ptvPtldlcpsVrgOlm3TEY/hegohIdtnUhzKGs0Jh47GXQft81/8AufNnWePj8bslGlcuq3nSMBTCldBWJMsKiglGAyg6zqapp2yghUUFLBw4UJM06S9vZ3Ozk5isRjBYJC1a9dy88038+yzz7Jx40YGBgZ47bXXSCaTXHrppbz++uuMjY1RVVXFd77zHa644gp0/a1azwMc2yaVSjI0PMrDHfsYsC0EUJqdwyeq67YbQ8O/GozFUvryJUvp2HeQsqy8bSlh7ywJhpYfnphAF4L2iTGOjY2Tn5uD6/qnOnk7TFVVFbfccgvXXHMNbW1tDA0NEY1GWbRoEbm5uTQ3NxOJRBgaGiKVSvH73/+eWCxGIBBgyZIl3HjjjVx22WX4fL5JS2Tqeld5OK6LZ9scHBmiK5FAExJXKWpyokQ8sf0bDU2jGx57LJM0ukKRt2vPcPHi85pn5Rct74iNIqVBfyrBqwNHqC0sxPbbU7X5O4lpmtTX11NfX3/KvQ0bNhAOh3niiSc4evQonuchpeTLX/4ya9asoaamZipzmFqlFCjHRdkOiaTFi4OdpGwboYEuobGgeETFE1tvPdCmclwjAzK9tpIuf1CZttu2tKTKfuFwm+G6Ho4UvNDfycXTa5kZCmAbxlTa8H4kJyeHDRs2cPXVVxOLxVBKkZ2dTU5OzrsOjOu6OLZDKm2xe7if1we7USITWEsDQepy85/Nx/fqQNqhtLE2U4/4hIG0bHTHe7Upp+BwVTQfTyp8aHROjLH1yH4S8QR2Oo3juu9ai5xONE0jEolQUVFBZWUleXl57wqhlMJzXay0xXBigmc62xhJWaALFIrzp1clq7IjjybrZ44/t+Up4IRS9/DRI7y87eU2005tWl1Z5wkJmlLg6TzZ08GuY8ewEyks2/rAMGcinufhOh7pdJp0IsFrR7t45VgPutJx8CgMBVhRWddabJivav39XP/1G08GWbpiGcUzK5yhgYH7F+eVHWwoLCIhbYQQDCRT3Nuxk57hGFYigW1beJ532krxg4hSCs/zsOwUVjzJvoFBHji0iwnHRkmB5sKl02sT83OLfhG/9dbDsaGhqc++NcME9PYdY8myZa0lRuDuz8ycnw7rEum5SKnTMtjHPW1vMjScqTtSqVQmZTgHMMcBHMchlUqRTCQ4PDzCL/ft4Mjkbk4Kh7q8KKvL614JDsUec77xD9SfEHNO2kW48sor6T7YoXzC+/eFuSUvXDVjLq6WWdUkks297fxm33YGh0ZJJZJYaQvXdU4Beb9YnqewbZtUKkUikaCzb4Cf7nmNNwZ7EULiKUGeZvK52QviJYbx29z66oGtr287qY1TZtsNP/hHBl7cE/eEc6iqvGx2bzJe2hkbRZcCzVPsmxhlJJ6gPJhNNhJPeCghgclNCbyMgU+3sinAU3iewnVc0rZFOpXEiido7evlF61v8vJgL4YQeBJMIflU4wJ3VfmMX1uD/f93dGwsvfKSVacH+dE/f5/aJXNYe9VVXUOdPS11xaULDo0OTOtPxhFo2Jpg/8QQbSP9ZEuTXCOAEApPuUjPyxy5Zfxlym1OvDzPw/E8HKVw0hbJdIJUymJobILnDrfxsz2v0zI2jOnquJrAER7/rbqBz80+/7lA2r3RyMoZrK6ZccrYvOP6t/npLSyramTR6hU9jMa7Q9nZq5p7joRsJLoCH5KBZJI3hrvpGx8jogyCaEhPoDwvs/K4Lt4JP7uTZYFt2ziOjZVKkU6mGYlN0HK0i3v3bueRw/sYtGx05GRMVEzLCvN3Cy7snibk36dMuTPPsvnej+88RefTRraW197E6B30ifNn//CuHS9+9fkj7UhdIjwPzdNxhAThEPUZzCko4cKCcupy84mGgvgNH7qmo8mMUpn8zcNxXOJ2kv6JcfYNDvLqwFFah/oYs12U1NE9DyVcNAS2UFw794L0tWVN/9jTs++HgZxCt6lp9jvq+p4huq3tED7HXfSmN7HxX/74fNmoZSOUhzf5YSXAyxxH4Zcahf4spgfDlIZD5AbDhHQTQ0os12HCSTOcmODoWIyuZIJ+K07a89AmK9Hjx9hK6OC6NBTkcdOSS7c0jYtr4qY2VDhz+rvqqb8XSNehdmK9vS1Ni5dsWVo1828e27cTTWjHZwIC0BEgNSwBRxNjdMbHUUMCiUIDJAIXcJXClRLNczM+rUlMJBxfIkTmi0AhDcHaujnxqnDeL62CwNCe1h2n1fM9N3FVKk5D03wr3Tf0yOqiqtGSrCyU577NrgIhJFKJTNGjCQwh0EQmpXDxEIAmBD4FmpQoMbmDgDp1uXYdzisuZXHJ9Ke00dFn4n29LF+4+OxALrniCrq6j3Dg0N4XypSxcU1lA54mpk6dxKRtlOchlUIqQKlJ9dTkJsbkKAsFQr11qqUULkwu2JMDpxQBv49PVDcdKjWC/6LlRGPDauK91HxvEIBVV3yC+RetiAdM/w8+WjmjuS4vinAm4wUCoY5vGU0eqQmBnLzH8ev4fUAolbkvBFKAhkSI41m1Ykl5pWrMzX1w1wNPbt/XupO51U3nBgTAiieI1Fa0VYUid62ZMTshjYxStiaYesvihKXjxNhxokw9Ik6wqGDqHD4/HGJ1VUOrnrL+LW/tYvcjy5eckX5nDFI/p57hjiOIsYnnPlo8fff8klKUp9C9zER+uxzfVDse4d/++ylwUqApuKSyNj4nWvSvs+rq93ftbuNM5T1XrRMlHA4RLcg/Nrxzz8NXVM+et2dw0EhZLlIKXMFUNH+vwuv4/SlrZQIN0/LyWFlR91BZOPJgbHRchSNZZ6zbGVsEIL+wgN0tu1R6YuKXTfmFDy0pq8TBzUzgtyl5JjJlJSSaVFwyvebQ3HD+T7onxsffD8T7BgFonDebvOrK4Yhm/ODSmoYD0bCftDiDRPE04nk2jYXFrCyv3SYHR1snTqgzPjQQIQQTsRi9h460zs+KPrC6us6R3vtt5a22FBDUJGurZg1GLPeRdHE0/dhzmz98EIBZ9bVEpxe7hpX43crpM1uqs/PwlPe+zt2n3tL0PM4vq/TmFpfeN7D3wJb2fXu56Stf+cuAAOw5sJ8f/vSO9nzLfepjlbVKCoGaDI4Zr8+kJu90IQRSZZ7NNU1WVzd0TA+Ef1OxZH7qggsXfiB9PjDIBdEL+MTadV5fd8/G8wvLdjcUFOHhZCK7mAyU8I6X4YEjQbouSyprqM8ueD7V0902MTL6QdX54CCRuRF6enr46MoVu0r8oR9dXtMYDwoxlUyeTjwBmqcoygqxuqT6iNM39KtBV0vPrK39y4MArFu3jo72DqSdfmxhQdkfLyitxMLNvKAJp86ZE373JFwyo8GZEcj52Z0LVzb3tbefjSrvLyC+kyTdFOMD6SHd5P5Lptcubek/5o+nbcQJqYnI+Fpm/ihwlaA6L49VpTUvZifd3/7vth2qsKb8rPQ4K4sANNY10JccpuNo56PVZvbmj5ZX4yln8j3GyVwrU3IgFbgSNA0uq2roKsnJ+56aVdnbfujQ2apx9iAAw71HqKyqiamRsbtXT6semRYKTx5zT7rS5Pu7SgCuYm5BMUvKKh8uGuRFp72bxZcu++sA+fT/2ED/0U66D7S9Uqybj66c0eAKKabOHuXkGasjIKz7WDNz9lCZbjzal5V0CmvKzoUK5wYEIFSay3mrVsQjwcCPl5dX7JqRF8X1FDqgpERTmYpwYVUFTZHo8/u379h++ODBc9X9uQNZNH8RVipJSa5/Z7bF7z9RXuf6NEBIFIqE5lIcCrK2qnEkR/f9uv6iZfGDx7r/+kAAKquq+GNzmxo+dPQ/Zkfyd80uKsbCxRPg8zxWVM6kPhR52h6JvTLc3c1nP/mpv04QgN7Rfi5cvbzdtNM//Xhlo5Wl+xCuS2UowkXlNW147h12KDBRVTfj7Dv7MEGu+u9X0t66G7+Vfvi8gpKXP1JWAUJxcW2Dm++Je+/42t++2XnowLl/H/JcgwBMTIzS0z9O2Kd/ZtfEyM+3tLWGrqibe8w9NrBa+IydyxYvOud9nnOLAITDEUwhEGnv0brc/N9vmLsQMzb+WnJiot1Lpj6MLj8cEICKqjI0U8aDin81xsafV25qU3bYiOfmvr8S9kzlQ/2vN3cohvT55bGRY0UJx0oIiFVXf/AM97/kv+Q/Qf4/cIcu0G52QuMAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDktMDZUMDQ6NTg6MzItMDQ6MDBnSBXWAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTA5LTA2VDA0OjU4OjMyLTA0OjAwFhWtagAAAABJRU5ErkJggg==',
                historic: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGBCg7GbRj3QAAGGVJREFUaN7tm3l0XfV17z+/3znnzlfS1SwkWYNlSZZleQDPJjYeMCUhD0IeyUpo0oQm6ctLmzZlNVkt8NYKXS/vvVBCHBqXhFJCU2jTMGQlQEwCmEUwBA8YWXiQfSVbszVeSXc+5/x+748rycYzU5I/utc60rrnHv32/u7pt/f+HQneJe3btw9AAhQUFOj6+nrtOA6WZb3bJd8Tid8VI631mfy0EO8v6/e82gu/eAKtHKmEWdLc1FpeVFRaNjg+1ia8VoEvHDKy2jW1QHgMC0Nry0mm3emx8RiO+4bX9Lzp9YlB23bt2oUtvx8gg6+/wZ77f8D13/lWYe9EbPNkavrrU1LV9LkZT/fkeGh4OiEnMykSroOjFJZhEPR4KPYFKQ8Gqc2LJOqCBb3lvtCvzUz2sdHosfaWzdfEH/q/3+TzX7/rgwfS89YBlJ0y8opr5mdMz03dmckP7znVv3xv78ng0fFTDCWnybguKBBagBYIDaBnIkqDgIDXYl4wn6VlVXp5edVIY6T4tRK8DwTHJl/Ufl+qvLXhgwOye89vyWbS+bU1tX826KS/+OueaO3OaKfsnRgnq1y0ITE0oCUKgdYaLfQcF6klQkqEAoTAEQ4aTcCwWFxSwXV1C8bXVtY8WucLbc8vKTqGBiEvT8TLemrgtwc4vmopLb1DDQM6/Xcv9nV/8slDB3zHY6M40kAIA6010nWRArymSdC0yPd4CUoDA1AI4naWmJNh2nawlUajMaREupARLoZXsKq0Rt3ctOTApsqa79ujI09UNy+c+NwXbuDhB3/x3oConjFENCNGF3m2Hp4c+/sfdey96tcnOoXj5CyA1gglyPf6mB/KZ3F+MQvCeZT6/eR5/XilgaEFrtCkXIepdJKBVJLO6Sk6YsOcSEwx7doIYSCQKFcT8Xu4pWVx6pb6RT8PJLP/a97CxiM/vO+7fPGv/vLdAdn7m5eZHhsNtaz50CdfHxn8u/v27qp9a3gQjxRoDTaSUp+XjYUVrKmYR3NeEWGfF+nzYJoWUhoIAULkntda4boujmPjZrJMJFK8NTnCy6dOsjs2TDyl8SBImzYGmuvmL9R/2rpqV3nC/sZXP37P63c/8ue0rVr6zoB0vNFOZt9hmbd19d/sSYzf8d2Xfx3snY5jCQMXF8swWF9cwUer62kuKiXoD2H4PfhMC59pIQwDaUgQMBvrWim0q9CuQ0Y5JLI2KusyHU/QPjzA4z1HOTA5jKs1EoOMUGyoquGrKzYeqpS+2zfOq3z2mQOHWLT03FRtng/ES//6KAVtLRjlZct2jQ38yfbf7gr2xKewpMDRLuWeAB+f18B1VXUUFBTgDQTweS08Hh+mYSINAyE1YlZP+rTKtNYorfA4DgFH4dgOQa+XfL9FY34+T56I8vP+KNOOjRfJ7hM9mPI3LV9bs+k7B4bG+7NCt1+2RWKjk+RbviueHTx63/959fmPHx+fEJY0yeCwIBDmT+cvYUVFFeFwiEAoRCgYxDAMhBCInC9d1Gc1OrfT6xwwrRTpTIZYLMZEbJIXert4JNrBKTuLFBJX2dzU2Kq/2LbqgeyhY38tiwqSV65c8bY1jbOZ9J3oxpdOBLtV9pvb39z96f0DfYZXmLho5vn9fKVpBasra8gryCMcDhMIBLAsCynlHJBLZRCBmHtWSok0DEzTxDAMTMOg0h+m2PDx1uQpEsrFo0yOTA2LcMBff03Lsq4FTQs6Nq1ey8P/9uO5NeWZDJ55+mkW1taJaV/gE890H/7syz0nLJ/2khWaQo/Fl+qXsbqikmAkSDgcxu/3Y5om76Vucl2XdDoNgN/vJxwOEykIc011PZ+taSVoCBxD47jwxKH2wv3jQ387dKJnYc3C5retMwdEa03VvHm83n5wzeHp2N8+ebQ9hOPiSk1AwCermllfVYsvEiIvGMLr9b4NhNZ6tjA8vztpjVKzWcvBdV3i8Tjt7e3s378f13UxDCMHJhAiLz/E5tpGPlxWj6GzeLWgP57kP7s7WpM+79fk5KQ/erRzbv25YO/t6qHcE/JmQ74v7Dq0d37XxASGNLCxWR8pY0ttHaG8EP5AGK/PjxSSQ4cOsXPnTvx+P6tXr6ajo4PBwcE5wV3Xfds1CyCbzVJVVcX69euJRCIopeYUYhgGPr8PRzkUOi4frV/I4dgQh+LT+DD4zYlu9s4buHm5P/AT6fP+au/OX3LVtutOA/GYFn6vuSCajG16vvswHgwcBBHL4vrahZTmR/CE/Hg9XkzDZGBggG984xs8/fTTGIbBtddey6JFi3jwwQeZnJx8myXOtlQoFOLWW2/l85///Bw4KU97uTAEVtBP0FHMzxZxXXUzR6P7EVmDZMbh2a63Iq1L12957gu3P7/gf9yq5lzLzSTY/vgrZKXa8vpw3xUnk9MgJELDykg5S4sq8Pv9+Hw+zBmGe/fuZffu3bS2ttLW1sauXbtQSrF69WqUUnPXmSCEEKxdu5YdO3Zw2223IaUkGo1SVFT0tjgTwsCQJjLoxRP2sraimpZQhLTQSARvDPVzLDG95k8eur+wefny0zHSe/g4f7GhoXDKzt6we/CkiZbYhiBoethUVkc4HMDwe7EME1PkgPT29FBaWsrHPvYxtm7dSmNjI6+//jptbW0YxjnJkKKiItauXcs999zDlVdeSWlpKa7rcvz48bmMdwYSTCHwmRZer4eycIiNxdUEBBgSxlIp2kcGmlLQpEzrdIyk0y6uVpXRyVhD58QYFgIHRXXQT3NRMZbXi8/05Mw/YxFjJl3u2rULrTU1NTUcOXIEj8eDx+MhlUoBkJ+fT2trK7feeiv5+fkkk0m01lRWVvL888/zwAMP0NzcTElJyRnpGQxpgAbX8mL5fCwrKqWwz2Q4lUWhODI2XJJeZKw7Ggi+cuiNPch9+/Zg5oVJSRYOpKaLpxJJpBBYChoLiomEw3hMC9MwckBmFNfc3MzExAQvvfQSAJlMBq/XSyqVwnEcAEpLS7n77rvZvn07W7ZswbIswuEwK1asIJ1O8+ijj3LgwAH6+vrOm+mEEBimiWV5KI1EqAnlg1IYQtIfGxdD07H1W0aHw75QPjKZTHK8q8PjWsbmk4nJQNZ1UWgMAU3hCD7Lg2Fa57jL8uXL+cxnPsPSpUtZs2YN4+PjNDc3Mzw8jG3btLS0cNddd/GlL32JQCDAnj17qKysJBKJ0NHRwd13383TTz+NUopMJnNhIIaBZVmELC8NoQKQuYbtVCrBaDq53CiI1BcWl2BKIQkE8wp9odDa/oFplNYIIbFMkwp/GNOUSNM4x4/D4TB33HEHX/7yl0mlUmzcuBG/38/tt99OU1MT9913Hy0tLRw8eJDjx4+zcuVKKisriUaj7Nixg4cffhjbtsnPz6ewsPCC+48QAmkY+M1cR2kYEq0g4dqMJOKFYxOxBmWZb5plJWVYUnp1KOCLZ9O50ltLwkIS8XmRUmLN1lBnUSAQIBgMAtDY2MhTTz1Fb28vd955J1dffTU7d+4kmUxy9dVXU1xczMmTJ3nkkUd48sknsW0bgIqKCmpray8MQsrcZQoKPH68wiAtXKSCWCYlY1NTPuHzYtqOjZSmUo6Tse0szAjssSw8Hgspzw/i7HvpdJqf//zn5Ofns2XLFvbs2YPjOKxevZrdu3ezc+dOXnvtNY4dOzYHYtZFKysruRjN1mWWaWEIAQKUhng2jTKkUgJM13FxhHZRPq20ZrbmlkIghZwDdik6fvw4L7/8MsXFxUxNTZFIJNi8eTMHDx7kscce49lnnz0nFnw+Hxs2bJiz6gWBnGGd2d4GAY5SlqPcQqUkpgDQGilzvjirAXumpJi5cUkgr7zyCr29vWzevJlsNktxcTE7duzgoYceIhqNnrcOq6ysZMWKFZdcezZTuq4719rIXBwLlDYlArO3vx83ndE1vhZtWR6EEJhak9CKZNbOlRhursy4UJWbSqV49dVXSafTeDwe6urqeO6559i+fTtDQ0MXlK+hoYGamppL4lAaXA3JbAYXjUAitSLs8WbzAv5xZVmYHp8XW2sHpbLFXj8SkELgOC6jmRTKUbmVLkKxWIxjx44B8Oabb9Ld3c1DDz10URCQC/RAIHBJIFopHFcxkk3hKg1SYGhBJBDIhELBMdcwkIZlIiwjmU4lumoCeXNe5LqK7sQkWdfB1e5Fy/RkMsn09DSQi5XHH3+c9vb2Swrouu5FS39mKmjHdcjYNt2JSVyVkyPs8VIeDMfyQ8E+r8dEailZedWKhIV49Ipw/rjHNFECHOB4bJR4Ko3t5irUC5HX653TbCaT4eDBg3MlysUoGo0yNjZ2YZeaAes4NvFEkhNTMfTMwK4oGKLQ8h4b6+3tHerrQ25ct56J4XGqwoUv1+cVtlcEgjgzGSsan2JoahrHzs5p73waLCoqoq2tLcdcKaampi4KfJY6Ojp48cUXZ5R/xrozfFytcFwXZdscnxijN5nEELkpZkN+EQVK7P1aS2usN3pipowXmsKDh8bLg3l7FhaXoV0HKQTD6SS7R06STWWxbRul1HkF8vv93HLLLVRXV1+yUzyTpqamuP/++3nzzTfPSSRaa7Tjom2HZCrDS6M9pG0bgcaUsKikfEInki/eeaxTL2pqyw0f7vnevQwEw3iVrnGl/KNX+6OGVmBLwZST4sriSgr8fgzDmJuWnE1VVVX4/X72799PKpUiFotdFqCBgQGi0ShtbW2UlZXlbgpQrsLO2qTSKd4Y6ufR6EFSjkKjqfQH+FhT2y8apPcfU46TrW9pzFnEIyxkxsZ01O7W/JITdUXFKKnxYNATn+LFk0dJJpLY2SzOBQLUsixuu+02vvnNbxIKhfD7/YRCIcLhMPn5+UQiEQoLCykuLqasrIyKigqqq6upqamhq6uLe++9l87OzpySFCjHJZPNMJ6M86ueTibSGTAFGs1V8+pSdXkFT6WaF0w//9wzwBk9+4m+k4z2DnY2r2h7cltt0+3HxndLw9W42uTpgW6WFFezyuNFWDmLmGdZRgiBz+fjU5/6FMuWLaOrqwvI9eCmaWJZFpZlzfUrlmXNWXi2zS0qKsoNKVxNxs6STSZ5ra+XV4YGMLVJCkV50M81tU0d5ZZ3d3x4mP/5V7e/Hcj6azbw2quvOmMjIz9eU1Z14wulZY0HTg3i0xYjqTQPd7dTHSqgwhIYQiCFb06AMwFJKWltbaW1tfWy4uTsuHBdl4ydIZtIcWRklEe7DhJ3bIQ0MFy4dl5jclmk7AeJO+84MfnVvzjN97RKYfDUEOs2bOiosPzf//SCZdmQKZHKRUqTA6OneKhzH2Pjk6RSKdLp9Hkz2budcWmtcRyHdDpNKpnkxPgEPzyyn5MTE0ghSePQVFjEtuqmVwJjkz9zvvY3NLecngG/bUB300030X+8W3uEemxlpGLXzfOX4Bq5rCaR7ByM8i9H9jI6FiOdTJHJZnBd55yYubycdZpcV2Fns6TTaZLJJD2nRthx6DVeHx1ECInSgkLDyx8vXp6osKwfRZrrR1787ctvW+OcKcFffvsuRl46lFDC6aqrrlo8mEpU9kzGMKXAUJoj8RgTiSTVgTzykCih0EICMmcd1KUtowGlUUrjOi5ZO0MmnSKdSNJxapAfdOzjN6ODWEKgJHiF5BOLlrtbq+f/c2Z0+B9jU1PZTVu2XhzId/7+/9G4ro0bbr65d6xn4EBTeeXyrtjIFcOpBAID2xAcjY/ROTFMnvQSsfwIoVHaRSqVO3LL+cqcy5x5KaVwlMLRGiebIZVNkk5nGJuK88KJTv7p0G85MDWO1zVxDYEjFP+tvoU/XnzV8/6se7sVzh+tb5h/jm6M8yls5y+fY0PdIlZtu2aAWKI/mJe3dc/AyaCNxNTgQTKSSvH6eD+npqco0BYBDKQS6Jl5luu6c7OtMyeNtm3jODaZdJpsKsvEZJwDfb08fHgvT5w4wmjGxkQiyE3trwiH+LPla/uvEPKv017ZXpix+dZ37ztH5otG5oHX9mENjnrEVYvv2b7/pT9/4WQUaUqEUhjKxBEShEORx6KtpIK1JdU0RYopCgbwWR5Mw8SQOaHUbMnhuCTsFMPxaY6MjrJ7pI+OsVNM2S5amphKoYWLgcAWms8uWZH9bFXrXQMDR+7x55e6ra2LzyvrJVNMZ2cXHsddtU/Ff/oPr75QFcvYCK1QM3+sBajccRQ+aVDqCzMvEKIyFCQSCBE0vVhSknEd4k6W8WScvqlJelNJhjMJsjPjHcTpY2wtTHBdWkoK+fq6a59rnRafSniNsdIF8y4op3kpIL1dUSYHBw+0rln33Pq6BZ//2ZF2DGHMRgICMBEgDTIC+pJT9CSm0WMCicYAJAIXcLXGlRJDuTmfNiReJMymCJH7IdBIS3BDU1uiLlT4w0yJf+xQx/6Lyim5BOl0gpbWZZnsqbEntpXVxSrCYbQ6q7IVAiEkUotc02MILJHbODUaF5WbHgqBR4MhJVrMTBDQ56Zr1+HK8krWVMx7xojFfpU4NcjGlWveG5AtN95Ib/9JjnUd3lWlrZ9eX9uCMsTpU6cZ22ilkFojNbkyPHd3Zogxo2WhQejTp1pa4wJn1tRaa/w+Dx+pb+2qtAL/YOQXTY7r+KXEvDQQgK03foRlH7om4ff6vn117fw9TYVFCGdmv0Ag9OzIaOY4TQjkzHfMXrPfA0Lr3PdCIAUYSISYHQBq1lXX6kWRyL8ffPTpvUc62llSf+ly57KAAGQSSQoaazrrggXbr5+/OCmtnFC2kRNmBtXbNHu+3mTuEXGGRQVz5/DFoSDb6lo6zHTmXwtvWOOu3rjusuS7bCDNbc2Md59ETMWfv7p83lvLKirRSmOqXCCfTXMHozM7/NmfzwEnBYaGLbWNibaisu8tbGo+2vtWJ5dLl8xaZ1IoFKSopHhovP3Q4zfWL156aHTUSmdcpBS4grnd/FKF45nnjnNIlOaKwkI21TT9pCpU8O+TsWkdKghftmyXbRGA4tIS3jpwUGfj8R+2Fpf+ZF1VLQ5uLoDPEvJy6PRxtsSQmi3zGrqWhIrv749PT78TEO8YCMCipYsprK8dLzCsb1/b0HKsKOQjKy6jULwIKWWzqLScTdWNL8vRWEf8IpOV9w2IEIL45CSDXSc7loWLHt1W3+RI9U5XOb2WBgKG5Ia6haMFGfeJbHlR9mfP7/zggQAsbG6kaF65a2WS/7Zp3oID9XmFKK0uGMznBTHzWyvFVVW1akl55SMjh489Fz1ymK9/5Su/GyAAh44d5Z4d90aLM+4z19U2aikEemZzzHl9rjQ534UQSJ17NuL1sq2+pXueP/QvNeuWpVesXfmu5HnXQFYUreAjN9yiTvUP/PSq0qq3WkrKUDi5nV3MbJRw3stS4EiQrsu62gaa80peSA/0d8YnYu9WnHcPpGBJAQMDA1y96ZqDFb7gdz7asCgREGKumLwYKQGG0pSFg2yrqD/pnBp7cNQ1sgsaG3/3QABuueUWuqPdSDv7s5UlVa+uqKwlg5t7QRPOjZkzPisJW+a3OPP9+f9038pNe05Fo+9FlHe2IZ6PUm6a6ZHsmOnlx1vmNa4/MDzkS2RtxJmTlZyv5eJHg6sF9YWFbK1seCkv5f7of3fu16UN1e9JjvdkEYBFTS2cSo3T3dfzVL03b+fV1fUo7cy8xzhTa+VaDqQGV4JhwB/VtfRW5Bd+Sy+sHYzODPN+r0AAxgdPUlvXMKknpr6/7Yr6iSuCoZlj7hlXmnl/VwvA1SwpKWddVe3jZaO85ET7WXPthj8MIJ/8zOcY7uuh/1jnK+Wm96lN81tcIcXc2aOcOWN1BIRMD9cvWDxWZVpPnQqnnNKGqvdDhPcHCECwMsKVW69JFAT8391YXXNwfmERrtKYgJYSQ+c6wpV1NbQWFL1wdO/+vSeOH3+/2L9/QFYtW0UmnaIi4mvPy/AfH6lucj0GICQaTdJwKQ8GuKFu0US+6fnn5g9tSBwf6v/DAwJQW1fHq3s69XhX338uLig+uLisnAwuSoBHKa6pXUBzsOCX9sTkK+P9/dz63z/xhwkEYDA2zNptG6NeO7vjw7WLMmHTg3BdaoMFfKi6oRPl3msH/fG6pvnvndkHCeTmj91EtOMtfJns41eWVPxmdVUNCM3mxha3WImH7/3ql/b1dB17T2+mno8+kH9NisdjDAxPE/KYnz4Yn3jguc6O4I1NS4bcoZFtwmO1b1iz6n3n+b5bBCAUKsArBCKrnmqKFP/H55asxDs5/VoqHo+qVPqDYPnBAAGoqavC8MpEQPM9a2r6Be2mn8wLWYlI5J21sJdLH+h/vbljk0iPTw5NDJUlnUxSwGR9/buvcP+L/ot+D/T/ARyCHo4eolTvAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE2LTA5LTA2VDA0OjQwOjU5LTA0OjAwcXnXnwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNi0wOS0wNlQwNDo0MDo1OS0wNDowMAAkbyMAAAAASUVORK5CYII=',
                shopping: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGBDseM1/2CAAAF/tJREFUaN7tm3l0XuV95z/Pc5d3177asrXZkizLlndjy4mNMZhhyUChhDOZDk0npByGDm2aZU7bZE4IZzIzpIEASUhoU5KmtCWsQ2NiGgwEMIttMN4tW/tma30lvfu993nmj1cSMja2sU2aP/o75+rV1b3vfX7f57f/fleCC6Q9e/YASIC8vDxdU1OjXdfFsqwLfeRFkfhtLaS1nr2eFuLSLn3RT9vxL0+jlSuVMIsb6pvKCgtLSgdGR5YKn5Xnj4SNjPZMLRC2YWFobbmJlDc5MhrF9d7zmfb7Pr8YcBzPqVrU+G8DZOCd99j18I+55v5vF/SMRa8YT05+bUKqyl4vbXeMj4YHJ+NyPJ0k7rm4SmEZBiHbpsgfoiwUoionP14dyusp84d/baYz/zjcdmxf4xWXx37yf+7hj772jU8eSPfBvSgnaeQUVdamTfvGjvT4tbtO9q3Y3dMVOjp6khOJSdKeBwqEFqAFQgPoKYvSICDos5gfymVZaYVeUVYxVJdf9FYxvh+FRsZf1gF/sqxpwScHZOeut8mkU7lVlVV3DLipL/66u61qe1ur7BkbJaM8tCExNKAlCoHWGi30zCpSS4SUCAUIgStcNJqgYbGkuJyrqxeOrp9b+Xi1P/xgbnHhMTQIeX4sntdd/W/v5fjaZTT2nFjQr1N/+XJvx63PHNrrPx4dxpUGQhhorZGehxTgM01CpkWu7SMkDQxAIYg5GaJumknHxVEajcaQEulBWngYPsHakkp1U33z3s1zK3/gDA89Pa9h0djnb7+ex/7mXy4OiOoeQbSlxfBi+8rD4yP3/vTA7lW/7mwVrpuVAFojlCDX56c2nMuS3CIWRnIoCQTI8QXwSQNDCzyhSXouE6kE/ckErZMTHIgO0hmfYNJzEMJAIFGeJj9gc0vjkuQtNYufDyYy/3P+orojjz7wPb74Z396YUB2v/4akyPD4cZ1n771naGBv3xg9ytVBwcHsKVAa3CQlPh9bCooZ135fBpyCon4fUi/jWlaSGkgBAiRvV9rhed5uK6Dl84wFk9ycHyI1052sTM6SCypsRGkTAcDzdW1i/QXmta+UhZ3/sfdN3/nnW/97E9YunbZxwNy4L19pPccljlXXvbVXfHRv/rea78O9UzGsISBh4dlGGwoKucz82poKCwhFAhjBGz8poXftBCGgTQkCJi2da0U2lNozyWtXOIZB5XxmIzF2TfYz1PdR9k7PoinNRKDtFBsrKjk7tWbDs2V/i9vmj/3hW17D7F42emu2jwTiFf//nHyljZilJUuf2Wk/w8ffPuVUHdsAksKXO1RZge5ef4Crq6oJi8vD18wiN9nYdt+TMNEGgZCasT0PukPtkxrjdIK23UJugrHcQn5fOQELOpyc3mms43n+9qYdB18SHZ2dmPK1xu/tG7z/XtPjPZlhN533hKJDo+Ta/nnvDBw9IH//eZLNx8fHROWNEnjsjAY4Qu1zawuryASCRMMhfD7/ZimiWkYCClBiLPqrEZnPZrSWVVzHJKpFIlEnOj4JDt62vlZ2wFOOhmkkHjK4ca6Jv3FpWt/lDl07M9lYV5i5ZrVpzzT+PAivZ0d+FPxUIfK3PPg+zs/925/r+ETJh6a+YEAd9Wv5rK588nJyyESiRAIBLBtG9M0kVIizgEiu3sCIcTM/YZpYhgGpmliWSZzAxGKDD8Hx08SVx62MjkyMSgiwUDN5Y3L2xfWLzyw+bL1PPYPPz+zam375S9ZVFUtjvQPfHZb+8HbXuvutPzaR1p6FFkWf1yznHVzKsgtKSTgD84wL6XkQklKidZ6ZjMsy0IIweWqhriT5pGO90mi8Tx4+tC+ghUFc/5ieSq5t3JRw+FTnjMjbq2pmD+fd/btX3d4MvoXzxzdF8b18KQmKODWigZaKqoI5EcIhyNZVZoCcrE0LR3TNPH5fISDQXJyw1xRVce1pTUYOoNPC/piCX7RcaAp4fd9SY6PB9qOtp4OpKe9mzI77IsUFt3+Svfx2vaxMaSQODisyS9iS1U1OblhEAYPPfQwd955J9///vd54403iEajFw1mNii/z0cgFKAgEuYzNYtoCOXhofAjeL2zg91D/TelA/YGAj52b//VqUBs0yIQCi3sSUxufqnjMDYGGkG+ZXFN1SJKcvPxhUNopXhh2zYeffRR7r77bm644Qbuu+8+0un0JQMiTAMrFCAUClKbW8jV8xowbAOJQSLt8kL7wfyM7dvy/27/sjyRTH8AxEvHefCpN8hIteWdwd45XYlJEBKhYU1+GcsKy7NG7bMpzC/g/gce4OGHH+a6665jbGyM5557jp6enksGREoDQ5rIkA874mN9+Twaw/mkhEYieO9EH8fik+v+8CcPFzSsWAFMGXvP4eP8940LCiaczPU7B7pMtMQxBBFhsbm0mkgkiBHwYRkmljRoWryYpsWL2bRpE0eOHKG9vZ2nnnqK9evXo5S6IADTBl9fX09BYSGmEPhNC+2zKY2E2VQ0j6PRcTwBI8kk+4b669fkldYr0xqeAZJKeXhazW0bjy5oHRvBQuCimBcK0FBYhOXz4TftrGHPykarqqpYtmwZR48e5Zvf/Ca2bZ+ys9MMfnjHZ/999rkQgjvuuIN77rknWzJr8Cwflt/P8sISCnpNBpMZFIojI4PFqcVGy9Fg6I1D7+1C7tmzCzMnQlKyqD85WTQRTyCFwFJQl1dEfiSCbVqYhpEFMqtEDYVCtLS0YJomyWQSrTWWZeF5HtFolImJCYQQWJaFZVm4rks0GmVycnLGS6VSKaLRKJlMhoKCAgoLC2fATccYy7Ipyc+nMpwLSmEISV90VJyYjG7YMjwY8YdzkYlEguPtB2zPMq7oio8HM56HQmMIqI/k47dsDNPCME6LnQCsW7eOkpISLMviK1/5Ctu3b+fee+8lGAxSVFTEQw89xAsvvMC2bdu47bbbAFi6dClPPPEEzzzzDBs2bADg1ltv5cUXX+Suu+7CNM0PgBgGlmURtnwsCOeBzBZsJ5NxhlOJFUZefk1BUTFSCkkwlFPgD4fX9yUmUVqjhcCyTMoDEUxTIk1jJgp/mOrq6li+fDmO4zA8PMyyZcu4+uqrmTNnDslkkvz8fFatWkVzczOpVAqAtWvXsnnzZioqKhgYGMDn83HVVVdRW1uL3+8/3fgNg4CZrSgNQ2IIQdxzGIrHCkbGogviiSSytLiUyopKny8S8scyqWzqrSURIcn3+5BSYgnBR3U9IpEIGzduBGDnzp0MDAxQXl5OXV0dsViM1tZs0BofH+fw4cOYpsnatWuRUvLWW29x/PhxampqWL169WnPng6UUkqkKcizA/iEAQKkgmg6KaMTE/7JeBzpuA6e6ynXddOOk5mxAduysG0LKT8axPRil112GcXFxRw7doz9+/eTk5PD0qVL0VrT2tqK53n09/fT3d1NcXExS5YswfM8duzYQTqdZt26dVRUVJx1DSEElmlhCAECFBDLpFCGVI4A6bkeruN4SmmttGaqekAKgRSnGvdHUV1dHYsWLSIajfL666/jOA5NTU3Ytk1rayvRaJTjx48zOjpKTU0Nc+bMobOzk7fffhvbttm8eTM+n++jgcySznRtgwBXKctVXoGrPEyR9X1ImdXF6R1wPA/Xdae35KxAioqKWLNmDb/5zW94/PHH6e7uZmhoCK01e/fu5c4776S3t5dUKkVvby9f/epXGR0d5ejRo1RVVbFy5cqz79TU8p7nzZQ2Ukgs0xQobUoEZk9fH14qrSv9jdqybIQQmFoT14pExsnWDZ6e8fNnIsMwaGlp4ZFHHqGjo4OOjo6Za6OjozzxxBMz511dXXR1dc2cr1q16qxqBaA0eBoSmTQeGoFEakXE9mVygoFRZVmYtt+Ho7WLUpkiXwA5pVau6zGcTqJclX3SOWjlypXU1tby/vvv09zczIYNG04BPjswap3dGL/fzy233EIoFDrrs7VSuJ5iKJPEUxqkwNCC/GAwHQ6HRjzDwDQsE9c1EqlkvL0ymLNSzIhR0REfJ+O5eNqbWfyjpFJeXs6GDRt4//33sSyLmpoacnNzsW0b27bJyckhGAwSDAaJRCIznzk5OWdBMFVBei5px6EjPo6nNEJCju2jLBSJ5oZDvXHPxdRSsmbV6nhXZ+fjcyK5V9imWeB5CqXheHSYWDJFOOJie95Za4+JiYmZdH737t3s2bMHYyobkFJiWdmgats2gUCAcDhMaWkp1157LbfffjuRSOR0lZqyC9d1iMUTdE5E0TJbgRaGwhRYvmMjPT09UeVhbmrZQMeR41RECl5LCWdfeTC0qTMWwxSCttgEJyYmKcrPxfP8p0TcD9M777zD888/D2SN/6abbiI3N/c0lRJCMDw8zJNPPsnBgwdpa2vjiiuuoLm5+RRJaMDTCtfzUI7D8bERehIJDCHxtGZBbiF5Suz+UmNT9PPPPZdNGj2hKdh/aLRs3cpdi4pKN3WMR5HSYjCVYOdQF3UlJTh+Z2ZXz0Q1NTWsX7+egwcPcvPNN3PPPfcQDofPmDSOjo5imiYvvPACLS0tlJSUnEGrNNr10I5LIpnm1eFuUo6DMMCUsLi4bEzHEy9//VirzvWsrGPLaIcT3YPYQnzhpcGeH9z75nbL8yQZCQsiIb61agsLy8oIhkL4fL7TVGya2YGBAYaHh6msrCQ3N/esBjwyMkJvby/l5eUUFxef6hjQeK5HJp0hHo/xdncH33r3FUaTLlpo5gaD/NWGrb9Yif1fh5Q72bC4MVtY2cJCph1MV+1syi3urC4sQkmNjUF3bIKXu46SiCdwMhlczzvjLgshmDNnDkuXLj0nCIDCwkKam5spKSk5TVW10ijXI51JM5qI8a/drYyl0mAKNJpV86uT1Tl5zyYbFk6+9OI2YFap29nbxeuvvd7qc1LPbK2qV0KCoTUok1/2d7D/xAmcRIq0k8ZVp4O5VKSUQrmatJMhk0jwVm8Pb5zox9QmLoqSUIDLq+oPlFm+ncbgIP/tz758KpANl2+kbGGlOzI09PN1BRXHG0tKSUgHIQRDyRSPdeyjf3ScdCKBk0mjlJpxyZeKtNYopUi7KTLxJEeGhnm8fT8x10FLgeHBVfPrEsvzS38c//rXO8dHRma++4GyCxg4eYKWjRsPlFuBH3xu4fJM2JRI5SGlyd7hk/ykdQ8jo+Mkk0lSqVQ2ZbgEYKYBuK5LKpUimUjQOTrGo0fepWuqm5PCpb6gkK3z6t8Ijow/537pqzQ0ftADPsVqb7zxRvqOd2hbqH9ck1/+yk21zXhG1qtJJNsH2vi7I7sZHomSSiRJZ9J4nnsakI8LSymN4zikUikSiQTdJ4f44aG3eGd4ACEkSgsKDB9/sGRFvNyyfprfUDP08tuvnfKM03zpn973DYZePRRXwm2vnlexZCAZn9s9HsWUAkNpjsSijMUTzAvmkINECYUWEsh2DDXZ5sNZp7YaUBqlpryTkyaTSpKOJzhwcoAfH9jD68MDWEKgJPiE5LOLV3hXzqv92/Tw4PejExOZzVuuPDuQ++/9v9S1LOX6m27qGenu31tfNndFe3RozmAyjsDAMQRHYyO0jg2SI33kWwGE0CjtIZXKjtyy+jKjNrMPpRSuUrha42bSJDMJUqk0IxMxXups5ZFDb7N3YhSfZ+IZAlco/mNNI3+wZNVLgYz3ZSuSO1yzoPa0vTljdNv+qxfZWL2YtVsv7yca7wvl5Fy5q78r5CAxNdhIhpJJ3hnt4+TkBHnaIoiBVAKtFEplBzpq1u/eVFngOA6u65BOpcgkM4yNx9jb28Njh3fzdOcRhtMOJhJBNp7MiYS5Y8X6vjlC/nnKJ/cVpB2+/b0HTuP5rIXG3rf2YA0M22LVku88+O6rf7Kjqw1pSoRSGMrEFRKES6FtsbS4nPXF86jPL6IwFMRv2ZiGiSGzTCmtsymH6xF3kgzGJjkyPMzOoV4OjJxkwvHQ0sRUCi08DASO0NzWvDpzW0XTN/r7j3wnkFviNTUtOSOv5yz/WlvbsV1v7R4Ve/Kv39xREU07CK1QU1/WAlR2HIVfGpT4I8wPhpkbDpEfDBMyfVhSkvZcYm6G0USM3olxepIJBtNxMlPtHcQHY2wtTPA8GosL+FrLVS82TYr/FPcZIyUL538kn+a5gPS0tzE+MLC3aV3LixuqF/7Rc0f2YQhj2hIQgIkAaZAW0JuYoDs+iR4RSDQGIBF4gKc1npQYysvqtCHxIWHaRYjsD4FGWoLr65fGq8MFj6aLAyOHDrx7Vj7PORPQqTiNTcvTmZMjT28trY6WRyJo5X1IrgIhJFKLbNFjCCwhMEQ2pfBQCMAQAluDISVaiKmCTZ/urj2XlWVzWVc+f5sRjf5r/OQAm9asuzggW264gZ6+Lo61H36lQltPXlPViDLEzNRJTMlGK4XUGqmZSsOzTGabGFO7LDQI/cFUS2s8YHa3WGtNwG9zXU1T+1wr+NdGbuH4qI6di81zAwG48obrWP7py+MBn/++T1XV7qovKES4U/ECgdDTLaOpkZoQyKlrTB/T1wGhdfa6EEgBBhIhphuAmpZ5VXpxfv4/7X/8l7uPHNhHc03TpQECkI4nyKurbK0O5T14Te2ShLSyTDmG+KCvPct1zI4ds2nmFjFLooKZOXxROMTW6sYDZir99wXXr/Mu29RyXvydN5CGpQ2MdnQhJmIvfaps/sHl5XPRSmOqrCF/mKZT+9kNafERHUsBUw0F2FJVF19aWPrQovqGoz0HWzlfOqfXmk3hcIjC4qITo/sOPXVDzZJlh4aHrVTaQ0qBJ5iJ5ud6qey0kUM20DCnoIDNlfVPVITz/mk8OqnDeRHOlz7WJLOopJiDe/frTCz2aFNRyRMtFVW4eFkD/hCT50MzUkJiSM2W+Qvam8NFD/fFJic/DoiPDQRg8bIlFNRUjeYZ1n1XLWg8Vhj2kxHnkSiehZRyWFxSxuZ5da/J4eiB2Kw64xMDIoQgNj7OQHvXgeWRwse31tS78sKmbdmXbYCgIbm+etFwXtp7OlNWmHnupe2fPBCARQ11FM4v86x04h82z1+4tyanAKXVRxrzGUFMfWqlWFVRpZrL5v5s6PCxF9uOHOZrd9312wECcOjYUb7zw++2FaW9bVdX1WkpBHoqOGa1PpuanOlACKTO3pvv87G1prFjfiD8d5Uty1Or16+5IH4uGMjqwtVcd/0t6mRf/5OrSioONhaXonCzkV1MBUo442EpcCVIz6OlagENOcU7Uv19rbGx6IWyc+FA8prz6O/v51ObL99f7g/d/5kFi+NBIWaSybOREmAoTWkkxNbymi735MjfDHtGZmFd3W8fCMAtt9xCR1sH0sk8t6a44s3Vc6tI42Vf0ITTbWbWuZKwpbbRrQ3kPvLAms27Tra1XQwrHy8gnomSXorJocyI6ePnW+bXbdg7eMIfzziIWamJyOpa1n40eFpQU1DAlXMXvJqT9H76v1rf1SUL5l0UHxf9as/i+kZOJkfp6O1+tsaXs/1T82pQ2p16j3Eq18qWHEgNngTDgP9Q3dhTnlvwbb2oaqCtvf1i2bh4IACjA11UVS8Y12MTP9g6p2ZsTiiMmuq8C5EVhZDZ5BBP01xcRktF1VOlw7zqtvWx7qqNvxtAbv0vn2ewt5u+Y61vlJm+ZzfXNnpCipnZo5yasboCwqbNNQuXjFSY1rMnI0m3ZEHFxS1+KYEAhObms/LKy+N5wcD3Ns2r3F9bUIinNCagpcTQ2YpwTXUlTXmFO47ufnd35/Hjl2r5Swdk7fK1pFNJyvP9+3LS/PN18+o92wCERKNJGB5loSDXVy8eyzXtv2349Mb48RN9v3tAAKqqq3lzV6sebe/9xZK8ov1LSstI46EE2EpxedVCGkJ5v3LGxt8Y7evjP//+Z383gQAMRAdZv3VTm8/J/PDaqsXpiGkjPI+qUB6fnregFeV91wkFYtX1tRe/2CcJ5Kbfu5G2AwfxpzNPrSwuf/2yikoQmivqGr0iJR777t1/vKe7/dgFp/wfRZ/IvybFYlH6BycJ2+bn9sfGfvRi64HQDfXNJ7wTQ1uFbe3buG7tJV/zkksEIBzOwycEIqOerc8v+ufPN6/BNz75VjIWa1PJ1Cex5CcDBKCyugLDJ+NBzUPWxOQO7aWeyQlb8fz8j1fCni99ov/15o2MI22/PDF2ojThphMCxmtqLjzD/Xf6d/o3oP8PukzNPbiWwDcAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDktMDZUMDQ6NTk6MzAtMDQ6MDAfFW/BAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTA5LTA2VDA0OjU5OjMwLTA0OjAwbkjXfQAAAABJRU5ErkJggg==',
                casino  : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGBQAbfkWHiAAAGd1JREFUaN7tm3l0XFed5z/3vvdqL1WVdln7bsu2bGezHSfEdpxl3ISJSQh9QhMOmelhORm6D9OnOWdIAwM0mWmWJDQDdKCBdNPpNAOB0MGBNDYJiZ3EjhPHlhVZtixr36qkKqn2eu/e+aMkRU5M4mw0f/TvnHekOvXe/f2+97ff3yvBm6QjR44ASIBwOKxbWlq0bdtYlvVml3xLJH5fjLTWK/lpId5e1m95tf2PPIRWtlTCrFjdua66rKyyamI21i3cVtgTDBh57ZhaIFyGhaG1ZaezzkJsNo7tvOA2XS+6PWKiUHAKTWu6/n2ATBx6gcPfuI/dd99VOjIXvzqRWfjUvFSNo07ONZiYDUwvpGQilyHl2NhKYRkGfpeLco+far+fppJIqtkfHqn2BH5t5vL/HB04dazr6h3J7/2fz3P7pz7zzgMZPnEUVcgYJeWNrTnTtWcwl/ijw1NjFz03MuQ/OTvFZHqBnOOAAqEFaIHQAHrRozQI8LktGvwhNlbV6Yuq62Y6IuXPVOD+O38s8Rvt9WSq17W9c0AOHn6WfC4bamps+uiEnf1vvx4eaPrVQL8cmZslrxy0ITE0oCUKgdYaLfQyF6klQkqEAoTAFjYajc+wWF9Rw/XN7bOX1zY+0OwJfD1UUXYKDUJemIgXdNf4s0c5vXkjXSOTbeM6++nfjA7+8U97j3pOx6PY0kAIA6010nGQAtymid+0CLnc+KWBASgEyUKeuJ1joWBTUBqNxpAS6UBOOBhuwebKRnVT54ajO2sbv1mIzjxUv3rN3If/9AZ+8N1H3hyQxShDKvAR/I/9LxFtt655KRH74v09z13y67P9wraLGkBrhBKE3B5aAyHWh8ppD5ZQ6fVS4vbilgaGFjhCk3Fs5rNpxjNp+hfm6YlPczY1z4JTQAgDgUQ5mojXxS1d6zO3tKz9V186/9mGNR19i7KKom1SDIErIt/rAhnse6kyUF793menxz91z3OPN52YnsAlBVpDAUmlx8320hq21jSwuqSMoMeN9LgwTQspDYQoMtQatFY4joNtF3CyeebSGU4kZnhyaoiD8WmSGY0LQdYsYKC5vnWN/q/rNj++KqfurO1oPSmlrAWiwPgFAzl98hTu2XkjX1/16WfnZ/7y3id/7R9ZSGIJAwcHyzC4oryG99S3sLqsEr83gOF14TEtPKaFMAykIUEsbp8GtEY5Cu3Y5JRNKp9H5RwWkimOTY/zk+GTHE1M42iNxCAnFFfVNfLnl27vbXAFPltbVdEjpEgDwxcE5Il/fIDWW2/BmI5d/Hhs/IF7n328Yyg+iyUEjhZUu3zc3NDG9XXNhMNh3D4fHreFy+XBNEykYSCkRrCSkSiC0hqtNY7j4DgOhbxNNpshnVpgam6On54d4F/HBliwCwgESmve1dLCJ7fuPNlu+D8Qqoy8CNivBGKcD8g93/keFY6x6sno0BfvefY3Vw7OzQmXtMgLRZs/yMc6NrKzsZVIJIy/JEjA78fj8eB2WZiWiTRk0YlXXEIKhBCL/0sMw8AyTSyXhdvtwuV243W76QiEKTVdnJ6fJakdpGEwOBcll8+VtZVV6LFDL+ybnJstrKqtPUfmVwEZPTuIJ5vyD6r857/+4sEPPD8+ariFiYOmwevljs5L2VLbSEm4hGAwiM/rxeVyYZpmUUhR1EMulyMejxONRkkkEuTzeQzDwDTNotcKsXwZhoHL5cLlcuNxu6jzBimVLk4kpkgpB5cy6ZufFkGft2X7mg1n2jraenZuuZwf/NMPl+U2V4LY+4tfsKapWfSNT7x/75kTH3py+Kzl0W5y0qHcsvhIyya21NTiifgJ+oK43W4Mw0BKCYBSivHxcQ4ePMgzzzzD6dOnicfjaK0JhUK0tLSwefNmtm3bRn19PYZhLJuHEAK3241pmhimybVCkrbzfOvMUTJoHAce6j1WelHpqv+5KZs52rhm9UvnDb9aa4739GBpLh9zm//wuacebR2MxzGEiUsqbm9az/va1+EvD1DiDxb9wTSXhUgkEjzyyCPcf//9HD58mPn5eZRSixFLL98XDAbZuHEjt912G3v27KG0tPRVpu04DslkkvGpGb7z4jM8NNqH0BYZYHdnB3devPO71vT0JwoeT6a1s+Nc0/ovt91OWFhuM1zy2R+f7tm5/+wAhjSwsbmitJJbV3dTESnFFwjgcXuXd1MIwczMDHfffTd33XUXx48fRylFMBikrKyM8vJyIpEIfr8f0zTJZDKcOXOGAwcOkEgk6OrqIhQKnQNELvqQKSRlppu+mQkm8zlcSMaS86yuqG6u9HoOCa/nzG3XX899P/zhy6blMi28brN9IB3fuW/wJVwY2AgilsXupjVUhiK4A96iOcmXQSwsLPCNb3yDe++9l2QySSgUoqamhrKyMoLBIF6vFyEEmUyGRCLBzMwMMzMzzM3Ncd999yGl5NOf/jSRSORc5zVN/H4f7WVVXN+whr7TRxB5g3TO5tEzJyLrNl6x67E//Yt97R/7E7WsESeX4n/fv5/N6xtufXRs4KZfDJ2UhjbRwJXlNby3ZS2hUAm+gB+XaRXD66LJPPLII/z1X/81s7OzhMNh6uvrWbNmDe3t7XR3d9Pd3U17ezurVq3C7/fjcrmwLItCoUAikaC/v5/Gxka6u7vPCacIEFKihSaoJT2xSUazWQwgkU/TXdOobnz/LT/3V1emv3733UWNjLx0mk9c1VY6X8jfcHBiyERLCoYgKCx2VjUTDPowvG4sw8QQspjggGg0yo9+9CPGx8fx+/3U1tbS0tJCV1cXGzZsQGvN6OgoWmsaGhpoaGigvLwcr9dLPp8nnU4TjUZ58MEH2bVrF7UrQqpAYEiJx+OmJhRiR0U9J+MJHAGxTIZjM+Odl4WrOpVpRZejVjbr4GhVO5CIt/XPxbAQ2Cjq/V5Wl5Vjud14TBeGYYB4Oc2dOnVqqeUlFApRWVlJc3MzGzduZGZmhm9/+9ucOnUKrTWtra3cfvvtbNq0iWQySSwWIxaLkU6neeGFF+jt7T0HyBJZpoXX7+eiimpKR/qYzuRRKPpi0xXZtca2kz7/gd4XDiOPHDmMWRIkI1kznlkon0+lkUJgKegIlxMJBnGZFuZSqFyh/dHRUeLxOF6vl1AoRDgcpqGhgUAgwIMPPsixY8fI5XLk83l6e3t58MEHkVJSV1dHJBIhFArh9XrJZDIMDw8zMzPDsWPHyGazy1FOSInL46a6tIzGQAiUwhCSsfismFyIX7ErOh30BEKY6XSamYlRV82qpquH4glf3nGQhsAUgs5gBI/lwjCtojZeQVprKioqsG0bn89HKpWit7eXoaEhFhYWaG5uXrZ7rTWZTIa9e/cu+4fX66WmpmY5kQ4ODrJv3z7q6+vxeDzLYCzTIuTx0R6McHhmCqEFU5kU0Wz6IqO2qaVUOS+aUkh8/pJSTyBw+dj4AkprhJBYpkmNN4hpSqRpLDNbSclkkkgkwq5du/B6vcv5QinFe97znnMSntYapRRaawzDoKGhASEE+Xye/fv3E4/HKSsrY82aNfh8vpd9RQikIfG5XDT4wxiGRCtIOQVmUsnS2Fy8TVnmi2ZVRRWWlG4d8HmS+Wyx9NaSoJBEPG6klFjnAbGUuFpbW9m9ezfz8/NorRFC0NjYSFlZGb29vWQyGQB8Ph9dXV0kEglOnz69fG8wGGRychKlVJGXZb2KlxQCwzSIuH24hUFWOEgF8VxGxufnPcLjxizYBaQ0lbLtXKGQh8VFXJaFy2UhF4u985EQgkKhwAMPPMDhw4dxu904jsPNN9/Mli1b+PznP08ulwOgoqKCO++8k8OHD/P9738ft9uN1pp169aRzWaJxWJMTU2hlDofI4QhcVkWhpAgHJSGZD6LMqRSAkzHdrCFdlAerfRS8yWQQiCFXE58v4scx6GtrY2rr76a2tpajh8/TiqVQmvN7t27ueSSSzAMg9/+9rfk83kikQgf+9jH6OzsJBqNcujQIc6ePctTTz3F8PAwHR0dbN++HZfLdS4WxGJNp5dExFbKspVTqpSk2NprjZQCuejQQggKjoNt20u56XeSlJL5+XkeeughpqamGBkZIRqNYpomJ06cYO/eveTzeU6ePIlt2ziOw6OPPsqJEydIJBIMDw8jhCCVStHT08OTTz5JNBo9N6gASjnYhQJqUR5Z9GOB0qZEIEfGxjg1MKCz6ay2LBdCCEytSWlFOl98UGtek3p7exkdHaW8vJwjR46Qy+XIZrMcOHCAQCDA/Pw8/f39CCEYGxvj6NGjy9qbmZlBCIHH4+Hiiy/m1ltvpaKi4pz1laPIF2yS+RwOGoFEak3Q5c6X+LyzAbcb0+VxU9DaRql8uduLXHQu23aI5jIo2wH12ki01ni9Xp5++ml6e3vp6upCKYXL5SKbzfLYY48Rj8cRQqCUIhAI0N/fz8GDB5ej3bZt27jjjjuora1drqqX1rYdm1y+wEwujaM0SIGhBRGfLxcI+GOOYSANy0RYRjqbSZ1p9JUs+TqOoxhMJSg4Dkqr5dD6SjIMg/b2drLZLL/85S+Xy5RwOMzq1as5dOgQfX19dHd3EwqFaGhoIBwOs3fvXgzDYP369ZimSWVlJXV1deeAWAJSKBTI5vMMphI4qtgqB11uqv3BeCjgH3W7TEwtJZddcmlq6OzZB1YFQ1e7TLPUcRRKw+l4lGQmS0g5uM8DQimFUoqbbrqJ973vfcshNRAI4Ha7+dznPodt28tdYDgcZsuWLaxdu7Z4DiYlSinuvffe80erxWCSz+dJptKcnY+jZbFEKvMHKLXcp2IjIyNx5WBu33YFg32nqQuWPpkVhWM1Pv/2s8kkphAMJOeZnF+guqJsOc6vJJ/Px8DAAF/60peKLeyKJmrp/pXPrGywlsi2bQYGBti4ceOrN2rRrFQ+x+m5GCPpNIaQOFrTFiojrMRzn+xaF//www8Xi0ZHaEqP985Wb7348Jryqu2DiThSWkxn0xyYHmJNTQ0er31OpgbweDwUCgWmp6cJhUJMT09TW1vL6OgoNTU1xGIxJicnzxEuEAiQz+fJ5XLLJb3jOOdk86VIpZWDkyuQyuR4IjpMtlBAGGBKWFtRPadT6d/81al+HXKs4rFyQ0cTIxdv0O6C039FTXPBZwhQClsIHp8eYig+Sy6bxbbtc3wlkUgwMDBAXV0dW7ZsIRgMsmPHDoLBIFdeeSWVlZUMDQ2dc0kpqa6uJhKJUFNTg8vlYnBwkLm5uXOBKEWhYJPJ5TgxO82z0TH04nHSKo+Xzkj5r8tN90GZt6ld3VzUiEtYjPafxTTNg+tCFWeby8rb+6MxXJgMJ+fZN9THqpKS5ZOSpQKyu7ubD37wg/T39zMxMcHc3Bw///nPicVi7N27l/n5eRoaGs5r+8FgEK01pmly8803c+mll55blzmKXC7LXHqBfxvuZy6bA9NEO4pLGpozzSXhn2VW1S7su+erxaCztPB79+yh73jvXHV12So87q2HJ0eEqQRaS8YyC3T4yqj2+sFlYEiJQFBXV0dbWxs9PT2k02lKSkqW6yfDMPD7/ZSXl59zLfXuS5tSVVXFxz/+cbZt27YCCOTyOdILCzw+NMiPB17CcQR5qajyebltw9aj6/yhr9mpZHznrmuBFcdBV+y4imeeftqOzcz8cGtV3Y37K6s6jk5N4NEWM5ksPxg8Rn0gTI0FMgAuq3gU1NzczBe+8AUcx+HNjNOklJSWlp6jDdsukEum6ZuO8sCZ4yTtAkIaGA5c29CR3hSpui/1V3eeTfzZJ5bXeTloC5j46SR79uzpGRkY/uYH2jf9zenYlEvlHLRhcjQ6xff6j/BR6xKklGgPuN1uLMs6b2f3ZqgIwiaTTjMYm+U7fUcYmptDSoMMNl2l5VxX33nAF0s8nP7kX7K6s+XlDVm50J49exg7PahdQv3zZZGax29q3YBjFKOaRPKriQG+3/ccM7E4mXSaXD6/3GO8XSDS6TRnJ6f45omnORSdQAiJ0oJSw80H11+UqrGs+yOrW2Z+8+yT5zz/qrbvz7/8GWae6E0pYZ9prq9bP5FJ1Q4n4phSYChNXzLOXCpNg6+EkGEiTWPR+ZdGF0udwIWZmUajlcbO50knU7w4Osy3jx3iqegElhAoCW4hef/ai5xr6lv/Phed/r/x+fn8zl3XvDaQu7/4N3Rs6+aGm24aiQ2PH+2srr3oTHxm1XQmhcCgYAhOJmOcmpsmKF1ELA+gcVSxJnOERhWHIcs7vfJSSmFrhaMU9mI+yWQyTM/FeexUL9888QwvzM/idkwcQ2ALxX9u6eKD6y/Z5807f2EFQ9GWttZXbch5T+N/9cvHuKp5LZuv2zFOPDXmLym55vD4kL+AxNTgQjKdyXAoNsZEIk7QkXiUQDgaZTsoVRzoLJUwSyME27YpFArYhQK5TJZcOkd0LsHzQ2f53olD/OTsS0SzeUzkon41q4IBPnrR5WOrhPwfWbc8VporcNe997xK5tfU/9FnjmBNRF3ikvVf+frzT/z3/UMDSFMilMJQJraQIGzKXBbdFTVcXlFPZ6ScMr8Pj+XCNMzFUF0sNxytsG2HVCHDdHKBvmiUgzOj9MSmmC84aGliKoUWDgaCgtB8aMOl+Q/VrfvM+HjfV7yhSmfduvXnlfV1Dbm//wwu29l8RCV//NWn99fFcwWEVssNjhag0KAVHmlQ6QnS4AtQG/AT8QXwm24sKck5Nkk7z2w6yeh8gpFMmulcivzi8Q7i5TG2FiY4Dl0VpXxq27WPrVsQt6bcRqyyveF3ymm+HpCRMwMkJiaOrtu67bErmttvf7jvGIYwEEuODZgIkAY5AaPpeYZTC+iYQKIxAInAARytcaTEUE7Rpg2Jm2L7qpa3VSDQSEtwQ2d3qjlQ+p1chTfW2/P8a8opeR3S2RRd6zbl8lOxh66rao7XBINo5bxCrwIhJFKLYtNjCCwhMIRAo3FQCMAQApcGQ0q0EIsNm+ZVwduxubi6lq01DXuNePzfUlMTbL9s61sDsuvGGxkZG+LUmZcer9PWj3c3daEMgWBx4rSoG60UUmvk4tBTL06Ri4cYi7ssNAi9PNVCaxxgZSeitcbrcfHulnVnai3fV41QWWJWJ19PzNcHAnDNje9m07t2pLxuz5evbGo93FlahrAVS4Yg9NJJy+KcUAjk4nfL4/Gl7wGhdfF7IZACDCRCLJ2dabbVN+m1kciDxx/4xXN9PcfY0LLu7QECkEulCXc09jf7w1/f3bo+La2iUAVDsPyWxYrQsTJ3rKTlW8QKjQqW5/DlAT/XNXf1mNncP5besNXZsn3bBcl3wUBWd69mdnAIMZ/cd2V1w4lNNbVopTFV0ZFfSSuHnef7/CpwUmBo2NXUkeouq/rbNZ2rT46c6L9Q8V4/aq2kQMBPWUX55Oyx3p/c2LJ+Y280amVzDlIKHMFyNn+9KnhlpbuMRGlWlZays7HzR3WB8IOJ+IIOhIMXLNsFawSgvLKCE0eP63wy+Z115ZU/2lbXhI1TdOBXCHkhtKwlJIbU7GpoO7MhUP6NseTCwhsB8YaBAKzduJ7SlqbZsGF9+dq2rlNlAQ95od4wiJWkVIG1ldXsrO94UkbjPclY7A2v8YaBCCFIJhJMnBnq2RQse+C6lk5bqje6ystracBnSG5oXhMN55yH8tVl+Yf3/eqdBwKwZnUHZQ3VjpVL/9POhvajLSWlKK1e98D7HBCLf7VSXFLXpDZU1/7DzEunHhvoe4lP3XHH7wcIQO+pk3zlW18bKM85e69v6tBSCPRicixafbE0Od+FEEhdvDfidnNdS9dggzfw/cZtm7KXXn7Zm5LnTQO5tOxS3n3DLWpqbPzHl1TWneiqqEJhFzO7WEyUcN7LUmBLkI7DtqY2VpdU7M+Oj/Un5+JvVpw3DyS8Icz4+DhX7txxvMbjv/s9bWtTPiGWi8nXIiXAUJqqoJ/ralqG7KnYd6OOkW/v6Pj9AwG45ZZbGBwYRBbyD19WUff0pbVN5HCKL2jCq31mxWclYVdrl93qDX37nst2Hp4aGHgroryxhHg+yjhZFmbyMdPND3c1dFxxdHrSk8oXECtKk+WXzgChwdGCltJSrqlte6Ik49z/pf7ndWVb/VuS4y1pBGBtZxdTmVkGR4d/1uIu+dWV9S0obS8faGu9+L6vBqnBkWAY8J+au0ZqQqV36TVNEwNnzrxVMd46EIDZiSGamtsSem7+m9etaplb5Q8sjrkXTWnx/V0tAEezoaKabXVNP6mK8oQ9MMbWa6/6wwDyx7d9mOnRYcZO9R+oNt0/29na5Qi5VLqz2KOALSBgutjdvj5WZ1o/mwpm7Mq2urdDhLcHCIC/NsLF1+xIhX3ee7fXNx5vLS3DURoT0FJi6GJHeFlzI+vCZftPPvf8c2dPn3672L99QDZv2kwum6Em4jlWkuNf3l3f6bgMQEg0mrThUO33cUPz2rmQ6fr71e+6KnV6cuwPDwhAU3MzTx/u17NnRv/f+nD58fVV1eRwUAJcSrGjqZ3V/vAvC3OJA7NjY/zJ+97/hwkEYCI+zeXXbR9wF/Lf+qOmtbmg6UI4Dk3+MO+qb+tHOV8r+L3J5s7Wt87snQRy03v3MNBzAk8u/5OLK2qe2lLXCEJzdUeXU67ED772Zx85Mnzm1Jsu+X8XvSM/TUom44xPLxBwmR84npz7u8f6e/w3dm6YdCZnrhMu69hVWze/7Tzfdo0ABAJh3EIg8upnnZHyf/nwhstwJxaeySSTAyqTfSdYvjNAABqb6zDcMuXT/K01v7BfO9mflgSsVCTyxlrYC6V39FdvTiyBdHnk5NxkVdrOpQUkWlrefIX7H/Qf9O9A/x9j6KO5R4iUFwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wOS0wNlQwNTowMDoyNy0wNDowMEbMD9wAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDktMDZUMDU6MDA6MjctMDQ6MDA3kbdgAAAAAElFTkSuQmCC',
                medical : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABLCAYAAAAyEtS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AkGBQEE6la7PAAAGFFJREFUaN7tm3l0XdV97z97n3PufDWPlmxrsCVZnrDxbBMP4JiX2ry4JCQ06aPJCmnXCg0tySpvtQ+yVl7WS8gEIXlMSdNAUtpSMJABDMUMC2MG2+BBnmTLmqx5uld3vuecvd8fVxIWNmBsk74/+lvrSLr37LP377t/8/4dCS6S9u/fDyABCgoKdF1dnXYcB8uyLnbKSyLxx1pIa332elqIy7v0Jc/24u93oJUjlTBLmxoXVBQXl5X3jY4sEl6rwBcOGVntmlogPIaFobXlJNNubGQ0guO+4zU9B70+0Wfbrl0zr/k/B0jfW++w92cP8am7v1vUPRa5OpqK3T4u1ewzbsbTHh0NDcYSMppJkXAdHKWwDIOgx0OJL0hFMEhNXmGiNljQXeELvWBmsv8y3HbyUPPVG+O/vOvbfPn2Oz9+IF1HDqDslJFXMrs+Y3q2t2eif7J3oGfpvu7O4InRAfqTMTKuCwqEFqAFQgPoCYvSICDgtZgVzOeK8mq9tKJ6qKGw5I1SvA8GR6Ivab8vVbFgzscHZM/eN8lm0vk1s2v+qs9Jf/WFrraa59paZffYKFnlog2JoQEtUQi01mihp1aRWiKkRChACBzhoNEEDIuFpZVcWzt3dE3V7EdrfaF780uLT6JByAtj8YJG9b55gFMrr6C5u39Or07/w0tn2j//5NEDvlORYRxpIISB1hrpukgBXtMkaFrke7wEpYEBKARxO0vEyRCzHWyl0WgMKZEuZISL4RWsLJutrm9cfGBT1ez77OGhHTOb5o196eZt/OoXv780IKprBNGWEcPzPZuPRUe+83DLvmUvdLQKx8lJAK0RSpDv9VEfymdhfglzw3mU+f3kef14pYGhBa7QpFyH8XSS3lSS1tg4LZFBOhLjxFwbIQwEEuVqCv0ebmhemLqhbv7vAsnst2bNazj+83t+wlf/9m8uDsi+3a8SGxkONa/+xOffGur7h3v2vVxzZLAPjxRoDTaSMp+XDUWVrK6cRVNeMWGfF+nzYJoWUhoIAULkxmutcF0Xx7FxM1nGEimORId4daCTPZFB4imNB0HatDHQXFs/T39lwcqXKxL2/7z1Mz98638/8tcsWnnFRwPS8s4hMvuPybzNq/5ub2L0f/3k1ReC3bE4ljBwcbEMg3UllVw3s46m4jKC/hCG34PPtPCZFsIwkIYEAZO2rpVCuwrtOmSUQyJro7IusXiCQ4O9PNF1ggPRQVytkRhkhGJ99WxuXb7haJX0fXPDrKpnnzlwlPlXnOuqzfOBeOXXj1KwqBmjonzJyyO9f3Hvmy8Hu+LjWFLgaJcKT4DPzJrDtdW1FBQU4A0E8HktPB4fpmEiDQMhNWJyn/S7W6a1RmmFx3EIOArbdgh6veT5LRry83myo43f9bQRc2y8SPZ0dGHK3c23rd5094H+0Z6s0IcuWCKR4Sj5lm/Gs30n7vne67s+c2p0TFjSJIPD3ECYr9QvZnllNeFwiEAwiM/nwzRNTMNASAlCfKDOanTOoymdUzXbJpVOk0wmiERjvNh9mkfaWhiws0ghcZXN9oYF+quLVj6YPXryG7K4IHnliuXT5jTeu8iZjnZ86USwXWW/fe/BPV94u/eM4RUmLppZfj+3NC5nVdVs8gryCIfD+P1+PB4PpmkipUR8CIjc7gmEEFPjDdPEMAxM08SyTKr8YUoMH0eiAySUi0eZHB8fFOGAv25j85LTcxvntmxatYZf/fNvzq9az/zhD8yrqRXHe/s+98zpIze92tVh+bSXjHQpsSz+sm4Jqyqr8BUGCQfCeL1epJQYhsHFkpQSrTUejwcp5QQ42KjqSNgZHmg/SAqN68KOo4eKlhbN+Psl6dSB2fOajp3XRrTWHG5p4a1Dh1cfi0X+/skTh0I4Lq4UBAR8vrqJddU1+ApD5AVDeDxeTPNcExsfH6e7uxvXddFaI4TAsqwJb+VMSaG4uJjKykomk0chBKZp5iSqNORrrq5poDsZ48me45jaoiee5N/bWxY0XrnpNmtw8OttJ1pT9Y0N04F0n+6iwhPyZkO+m18+uq/+9NgYhjSwsVlXWM41NbWE8kL4/SEsKyeJSQYmyXVd7rvvPh544AFc16W+vp5t27bR3NzMyMgIO3fu5PXXXyeTybB06VLuv/9+qqqqpoHRWuPzeXG1S5Hjcl3dPI5F+jkaj+HDYHdHO/tm9V6/1B94TPq8/7HvuZ0s23JtLvsB8JgW/mBwbncytmlX+zE8GGgEhZbFp2rmUZZfiDfkx+v1YhrG1M6eTbZtc/jwYTo7OwmHw9xxxx1cf/319Pf3U19fz3e+8x3WrFlDT08PBw4cYGRk5LyqJkwDK+gnGAxQn1/MtTObMDwGEoNkxuHZ00cKsx7vNb+9+ZuyP5XJPQfgZhLc+8RrZKW65q3BMzM6kzEQEqFhRWEFVxRX4vf78fp8mNJAGAbvV09MGvFXvvIVmpub2blzJ9/4xjd44IEHSKfT3HLLLdTU1EzWJ+d93pAGhmEig148YS9rKmfSHCokLTQSwTv9PZxMxFb/xS9/VtS0dOm7qtV97BRfXz+naNzObtvT12miJbYhCAuLTeW1hMMBDL8XyzAxhCSRShKPjqOUmqYWk4YbDodZtGgRDz/8MHfddRfRaJTHHnuMgYEB7rnnHmpra2ltbSWTyTA0NITjOFN2CuD1esnLz8NvWmivh/JwiA0lMzkRieIKGEmlODTU27iioLxRmdbwFJB02sXVqqotGpnTOjaChcBBMTPop6m4BMvrxWd6MAyDTDbL3T/6Eb99+rdTxjzJwHXXXYfruti2TTQa5YorrmDDhg0888wzNDU1sXnzZrTWxGIxpJS89dZb3H777YyPj09tiFKK8vJyvv3tb3PllVfiWF4sn48lxWUUnTEZTGVRKI6PDJam5xtrTwSCrx19Zy/m/v17Mf1hUrHovN5UrGQ8kUQKgaWgoaCEwnAYj2lN2UUikWDXC7vYt2/fOWqxePFiAJLJJDt27OCnP/0pJSUlpNNptm7dys0338wjjzzC8ePHKSkpIRKJsH//fsbHx6fN4/V6ufHGG1m2bBmGaWJZHsoKC5kdymcwMYBhSHoio6I/Fll3TTJxf18oP2Ymk0mG+s54KmfUXN0ZiQayros0BKYQNIYL8VkeDNOaFitc16WpqYmtW7eye/duVq1axRtvvDFN75966inC4TBf/OIX+f73v4/jODz44IPcfffdxONxSkpKALAsi+3bt1NQUEBnZyeLFy/miSeewHVdAAzDwLIsQpaXOaEC9g4NILRgIJVgOJ1calTV1BUp96AphSQQzCvyhUJrenpjKK0RQmKZJpX+MKYpkaYx5W4nqbS0lFWrVtHe3s7y5cvp7u6edj+ZTPKLX/yC3//+91RWVhKLxejq6iKVSk1z20IImpqaKCsrA2DFihXs3LlzuvMwDPxmrqI0DIlWkHBthhLxopGxyBxlmQfN8tJyLCm9OhTwxbPpXOqtJWEhKfTl4oUlxDQvJYSgra2NRx55hGPHjpHJZDhx4gRXXXXVNDCu69LT00NPTw/vR0opdu3aRTAYZGhoiKGhIUZHR6fWm4z20hQUePx4hUFauEgFkUxKRsbHfcLnRdqOjeu4ynGcjG1nYWICj2Xh8VhIKc5xtVprmpubue2221i5ciVf//rXWbVq1fu61A8iwzDYvn07N910Exs2bODWW2+lrKwMpdS0jRNCYJkWhhAgQAHxbBplSGULMF3HxRHaRfm00prJnFsKgRRyCth7fX1LSwt33XUXra2tDA0NcerUKdavX3/OuPeq5KTun/358ccfJxAIMDIywsmTJxkYGJj2nDhrrsnaBgGOUpaj3CKlJKbIbTFS5nRxkgF7Ijea+OIcidTU1PDZz36WHTt2sG3bNn73u9+dI5GlS5dy4403YlkWQgi6u7t56KGHiEajU2OklKxbt47S0lKOHDnCxo0b6ejomD6XeBf0ZGkjc3YsUNqUCMzunh7cdEbP9jVry/LkkjetSWhFMmvn6gZXT8WMSaCpVIr+/n7i8TgDAwMkEglKS0unAWlsbORrX/saPp8PgHfeeYdHH310GhCtNWNjYwDEYjEGBgawbXu6HWlwNSSzGVw0AonUirDHm80L+EeVZWF6fF5srR2UypZ4/UhACoHjuAxnUihH5WZ6j8ocPHiQI0eO4Louu3fvxnEcvvzlL58jubN39r0SE0Lgui6//vWvp7KCZ599dkqCU88pheMqhrIpXKVBCgwtKAwEMqFQcMQ1DKRhmQjLSKZTidOzA3lTWuS6ivZElKzr4Gr3vIbsOA5a6ykVvNjzXK1zlaJSair9P9uGHNchY9u0J6K4Krc5YY+XimA4kh8KnvF6TKSWkhXLlicsxKMzwvmjHtNECXCAU5Fh4qk0tuNMM9L3Y/i9YCclMvn9+90/H02uMXnqEk8k6RiPoCcO7IqDIYos78mR7u7u/jNnMDesXUf78VNUh4teTQv7UGUguKEjHscUgrb4OP3jMUoK83EcL3v27OH555+nq6vrvIu//fbb0wAfPHiQO++8c6pg6u/vn0pHIpEIL774Iul0+px5XNflqaefxtWKbVu3oW2HU2MjdCeTGELias2c/GIKlNh3W/OCyJeefjqXNLpCU3T46GjF6iv3zisp39AejSClxWA6yZ6hThrLy7C9Nrt27eJ73/ve++7iwYMHp30+duwYx44dO+/YsbExXnrppfdV2Sd37GBwoJ+rP7EBheCV4S7Sto0wwJQwv7RiTCeSL91xslXnu1YOyKyGGrp9Ae213dZ1lbX2yx2tlusqHCl4ebCLq8cbmBvws27dOr71rW+dExsuN2nAdRxmVM5AGJIjg/28OdyDFjl1rPIHaCwseaEEz56hrEPV/IYcEI+wONPagWmaexbkl3bUFpfMbR0ewYNJV3yclzpPUBXOY/Wq1WzctAlrQlU+NiBKY9tZkskkA9Exnu86wVg6A6aJdhXLZtWmavMKnkrNqIrtuudHAO+Wuh1nOtn96u5Wr51+cktNoxISDK1Bmfyht53D/f3YyRQZJ4Oj3ItKRy6Ecp5Lkc1mySSTvHGmm9f6ezG1iYOiLOhnY01jS4Xl3WMMDvK1v/3mdCDrNq6nYu5sZ2Ro6Deri6pPNZeVk5Q2QgiGUml+1X6I3tEomUQSO5tBKfWBXueiJKE1SikydppMIsXxoWEePX2YuGOjpcBw4ZOzGpJLCssfStxxR0f0rJp/WkLTN9DP2vXrWyot/31fmLskGzIlUrlIaXJgeIBftu5nZDRKKpUinU5P+fxLBTMJwHEc0uk0qWSSjtExfn78bTrHxpBCksahsaiYLTMbXwuMRJ92bvs7mprfPQOeZrXbt2+n51S79gj1LysKK1++vn4xrpHzahLJc31t/NPxfQyPREgnU2SyGVzXOTc+fGR10ti2TTqdJplM0jUwxP1H3+Ct4T6EkCgtKDK8/PnCpYlKy3q4sKlu6KU3X502xzlHhH/zgzsZeuVoQgnndO3M6oV9qURVVzSCKQWG0hyPRxhLJJkZyCMPiRIKLSSQSzE0ufT7A52BBpRGKY3ruGTtDNl0ikwiSctAHw+17Gf3cB+WECgJXiH53Pyl7uaZ9f+YGR78v5Hx8eymazZ/MJC7v/N9GtYuYtv113ePdPUeaKyoWno6MjRjMJVAYGAbghPxEVrHBsmTXgotP0JolHaRSuVabjl9mVKbsy+lFI5SOFrjZDOksknS6Qwj43F2dbTywNE3OTA+itc1cQ2BIxT/va6ZP1+4bJc/637TCucP182pP2dvznto+9zO51lfO5+VWzb2Ekn0BPPyNu/t7QzaSEwNHiRDqRRvjfYwEBunQFsEMJBKoJWaypnUWX9PHpnato3j2GTSabKpLGPROAfOdPOrY/vY0XGc4YyNiUSQO7WfEQ7xV0vX9MwQ8htprzxUlLH57k/uOYfnDwwGB97Yj9U37BHLFv7w3rdf+esXO9uQpkQohaFMHCFBOBR7LBaVVrKmdCaNhSUUBwP4LA+mYWLIHFNKa1ytcByXhJ1iMB7j+PAwe4bO0DIywLjtoqWJqRRauBgIbKG5afHy7E3VC+7s7T3+Q39+mbtgwcLz8vqhUa219TQex125X8Uf/9HrL1ZHMjZCK9TEw1qAyrWj8EmDMl+YWYEQVaEghYEQQdOLJSUZ1yHuZBlNxjkzHqU7lWQwkyCrFMZEJTrZxtbCBNelubSI29d+8vkFMfFnCa8xUjZ31vvyaX4YkO7TbUT7+g4sWL32+XW1c7/89PFDGMKYtAQEYCJAGmQEnEmO05WIoUcEEo0BSAQu4GqNKyWGcnM6bUi8SJh0ESL3Q6CRlmBb46JEbajo55lS/8jRlrc/kM8PTZp0OkHzgiWZ7MDIji3ltZHKcBitptfdCIEQEqlFrugxBJYQGEKg0bgoBGAIgUeDISVaiImCTZ/rrl2HKyuqWF056xkjEvmPxEAfG1asvjQg13z603T3dHLy9LGXq7X1+KdqmlGGmOo6iQnZaKWQWiM1oPUEe3riEGNil4UGod/tammNC6iz1tNa4/d52Fq34HSVFfiRkV8cHdXxD2Pzw4EAbP70VpZ8YmPC7/X94Kqa+r2NRcUIZyJeIBB68shooqUmBHLiHpPX5H1AaJ27LwRSgIFEiMk2hWbtzBo9v7DwXw8/+od9x1sOsbhuweUBApBJJClomN1aGyy491P1C5PSyjFlGzlmJlBN29nzpS9TQ8RZEhVM9eFLQkG21Da3mOnMr4u2rXZXbVh7QfxdMJCmRU2MtncixuO7rqqYdWRJZRVaaUyVM+T30uSh2tknL0KI80Z8ARMHCnBNTUNiUXH5T+c1Np3oPtJ6oex9uNc6m0KhIMWlJf2jh44+8em6hVccHR620hkXKQWuYCqaf1itMnl/Slq5QMOMoiI2zW58rDpU8K/RSEyHCsIXzNtHKvVKyko5cuCwzsbjP19QUvbY2uoaHNycAb+HyQuhKSkhMaTmmllzTi8OlfysJx6LfRQQHxkIwPwrFlJUVzNaYFg/+OSc5pPFIR9ZcQGJ4geQUjbzyyrYNLPhVTkcaYmfp7d42YEIIYhHo/Sd7mxZEi5+dEtdoyPVR53l3bk0EDAk22rnDRdk3B3ZiuLs07ue+/iBAMxraqB4VoVrZZL/vGnW3AN1eUUord7XmM8LYuK3Vopl1TVqcUXVI0PHTj7fdvwYt99yyx8HCMDRkyf44f0/bivJuM9cW9OgpRDoieCY0/pcanK+CyGQOje20OtlS11z+yx/6J9mr12SXr5mxUXxc9FAlhcvZ+u2G9RAT+/jy8qqjzSXlqNwcpFdTARKOO9lKXAkSNdlbc0cmvJKX0z39rTGxyIXy87FAylYXEBvby9Xbdp4uNIXvPu6OfMTASGmkskPIiXAUJrycJAtlXWdzsDIL4ZdIzu3oeGPDwTghhtuoL2tHWlnn15RWv368qoaMri5FzThXJs567OScE19s1Pvz3/gnhWb9g60tV0KKx8tIJ6PUm6a2FB2xPTym2tmNaw7MNjvS2RtxFmpicjpWs5+NLhaUFdUxOaqOa/kpdyH/0/r27pszsxL4uOSzz7nNzYzkBql/UzXU3XevOeumlmH0s7UiwRaT7zvq0FqcCUYBvy32ubuyvyi7+p5NX1tp09fKhuXDgRgtK+Tmto5UT02ft+WGXVjM4KhiTb3hCpNvL+rBeBqFpdWsLa65onyYV5x2npY/cn1l8zDZQHy+f/xJQbPdNFzsvW1CtP71Kb6ZldIMdV7lBM9VkdAyPTwqbkLR6pN66mBcMopm1N9OVi4PEAAglWFXLl5Y6Ig4P/JhpmzD9cXFeMqjQloKTF0riJcUTubBQXFL57Y9/a+jlOnLtfylw/IyiUryaRTVBb6DuVl+LetMxtdjwEIiUaTNFwqggG21c4fyzc9/9j0ifWJU/09l7zuZQcCUFNby+t7W/Xo6TP/vrCg5PDC8goyuCgBHqXYWDOXpmDBTnss+tpoTw9f/Ozn/v8EAtAXGWTNlg1tXjt7/5/UzM+ETQ/CdakJFvCJmXNaUe6P7aA/XttYf+mLfZxArv/T7bS1HMGXyT5xZWnl7lXVs0Form5odkuU+NWPb/3L/V2nT172RtHH0naKxyP0DsYIecwvHI6PPfh8a0vw042L+93+oS3CYx1av3rlZV/zY2kGhkIFeIVAZNVTjYUl//alxSvwRmNvpOLxNpVKX/oCfywgALNrqzG8MhHQ/NQaj72o3fSTeSErUVj40UrYC6WP9b/e3JEo0uOT/WP95UknkxQQrau7+Az3v+i/6D+B/h8fYXbahnY+WgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wOS0wNlQwNTowMTowNC0wNDowMNrDeQIAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDktMDZUMDU6MDE6MDQtMDQ6MDCrnsG+AAAAAElFTkSuQmCC',
            }
        };
        
        $scope.data.search.roomsData   = {};
        $scope.comaprator              = Comparators;

        $scope.showDetail = function showDetail ( e, hotel ) {
            // console.log( $scope.data.map );
            $scope.data.currentHotel = hotel;
            $scope.data.map.showInfoWindow('hotel-info', hotel.id);
        };

        var sumAllSearchValues = function sumAllSearchValues (searchData) {
            var adults = 0,
                child  = 0,
                rooms  = 0;

            rooms = searchData.length;

            for (var i=0; i<searchData.length; i++) {
                adults += parseInt(searchData[i].adult);
                child  += parseInt(searchData[i].child);
            }
            return {adults: adults, childrens: child, rooms: rooms};
        };
        var checkIfAllFlase = function checkIfAllFlase (item) {
            var clearArr = true;
            for(var i=0; i<item.length; i++) {
                try {
                    if (item[i].hasOwnProperty('value') && item[i].value) {
                        clearArr = false;
                        break;
                    }
                } catch (e) { }
            }

            return clearArr;
        };
        // var clearSelections = function clearSelections ( rooms, id ) {
        var clearSelections = function clearSelections ( rooms, hotelId ) {
            console.log( 'Clear selection' );
            angular.forEach( rooms, function (room, key) {
                if( room.isAdded == true ) {
                    var id   = 'room-' + hotelId + '-' + room.resultid.replace('.', '');
                    var jEle = jQuery('select#' + id);
                    jEle.val('0');
                    $timeout(function () {
                        jEle.change();
                    });
                }
            });
        };

        var onPageChange = function onPageChange ( isInitialLoad = false ) {
            if( $scope.data.viewByMap ) { return; }
            jQuery( 'div.item-price-more' ).waitMe('hide');
            // console.log( $scope.data.itemsOnPage );
            // var $wrapper = jQuery( 'div#table-wrapper' )
            // $wrapper.waitMe({
            //     effect : 'pulse',
            //     text : 'Updating prices',
            //     bg : 'rgba(255,255,255,0.7)',
            //     color : '#000',
            //     maxSize : '',
            //     source : ''
            // });
            $timeout(function () {
                // console.log( $scope.data.itemsOnPage );
                angular.forEach($scope.data.itemsOnPage, function ( hotel, key ) {
                    // console.log( 'hotel.workingOnFetchingRooms', hotel.workingOnFetchingRooms );
                    if( typeof hotel.rooms == 'undefined' ) {

                        if( typeof hotel.workingOnFetchingRooms != 'undefined' && hotel.workingOnFetchingRooms )
                        {
                            showLoading( 'div#hotel-price-' + hotel.id, 'Updating prices' );
                        } else if( ! hotel.fetchingRooms && (typeof hotel.workingOnFetchingRooms == 'undefined' || ! hotel.workingOnFetchingRooms) ) {
                            showLoading( 'div#hotel-price-' + hotel.id, 'Updating prices' );
                            hotel.workingOnFetchingRooms = true;
                            // $timeout(function () {
                            //     hotel.fetchingRooms = true;
                            //     hotel.workingOnFetchingRooms = false;
                            //     hideLoading( 'div#hotel-price-' + hotel.id );
                            // }, 10000);
                            $scope.getHotelRooms( hotel, true );
                        }
                    }
                });
                // $wrapper.waitMe('hide');
                // getHotelRooms
            },0);
        };

        var showLoading = function showLoading ( id, message ) {
            $timeout(function () {
                var $wrapper = jQuery( id );
                $wrapper.css({height: '175px'})
                $wrapper.waitMe({
                    effect : 'pulse',
                    text   : message,
                    bg     : 'rgba(255,255,255,0.7)',
                    color  : '#000',
                    maxSize: '',
                    source : ''
                });
            }, 0);
        };

        var hideLoading = function hideLoading ( id ) {
            $timeout(function () {
                var $wrapper = jQuery( id );
                $wrapper.waitMe('hide');
                $wrapper.removeAttr('style');
            }, 100);
        };

        var getTheSmallestPrice = function getTheSmallestPrice ( hotel, providerKey ) {
            hotel.activeProvider = providerKey;
            console.log( providerKey, hotel );

            return false;
        };

        var updateHotels = function updateHotels () {
            $timeout(function () {
                jQuery( 'button#filter-hotels' ).trigger('click');
                // $scope.onPageChange();
            }, 500);
        };
        var updateMap = function updateMap (  ) {
            if( $scope.data.viewByMap )
            {
                $timeout(function(  ) { jQuery( 'button#map-search-filteration' ).trigger('click'); }, 500);
            }
        };

        var getMap = function getMap ( hotels ) {
            // if( ! $scope.data.viewByMap )
            // {
            //     $scope.data.mapTimeOut = false;
            //     return;
            // }

            NgMap.getMap('search-map').then(function(map) {
                $scope.data.map = map;
                var dynMarkers = [];

                if( $scope.data.markerCluster != null )
                {
                    $scope.data.markerCluster.clearMarkers();
                }
                if( $scope.data.mapCircles.length )
                {
                    angular.forEach( $scope.data.mapCircles, function (circl, cirkey) {
                        circl.circle.setOptions({fillOpacity: 0, strokeOpacity: 0});
                    });
                }
                if( $scope.data.landmarkMarkers.length ) {
                    angular.forEach( $scope.data.landmarkMarkers, function( marker, key ) {
                        marker.setMap( null );
                    });
                    $scope.data.landmarkMarkers = [];
                }

                $timeout(function () {
                    // angular.forEach( $scope.data.hotels, function ( hotel, key ) {
                    angular.forEach( hotels, function ( hotel, key ) {
                        // console.log(hotel.details.latitude, hotel.details.longitude);
                        // console.log( key, hotel );
                        var latLong = new google.maps.LatLng( hotel.details.latitude, hotel.details.longitude );
                        var marker = new google.maps.Marker({
                                position: latLong,
                                icon    : (hotel.details.propertytype == 1)?$scope.data.image.HOTEL:$scope.data.image.APARTLE,
                                title   : hotel.details.hotelname
                            });
                        // addMarker( marker, hotel );
                        marker.addListener('click', function () {
                            $scope.data.currentHotel = hotel;
                            map.showInfoWindow('hotel-info', this);
                        });
                        dynMarkers.push( marker );
                        $scope.data.mapTimeOut = true;
                    });
                    $scope.data.markerCluster = new MarkerClusterer(map, dynMarkers, {maxZoom: 11, styles: [{anchorText:[5,-5], height: 53, url: $scope.data.image.CLUSTER, width: 53}]});

                    if( $scope.data.filters.activeLandmarks.selectedRegions.length ) {

                        angular.forEach( $scope.data.filters.activeLandmarks.selectedRegions, function ( region, key ) {
                            // console.log( val );
                            // return;
                            var latLng = new google.maps.LatLng( region.location.coordinates[1], region.location.coordinates[0] );
                            // var latLng = new google.maps.LatLng( 2.296894, 48.864644 );
                            var marker = new google.maps.Marker({
                                position: latLng,
                                icon    : $scope.data.image.LANDMARKS[ region.type ],
                                title   : region.name,
                                map     : map
                            })

                            marker.addListener('click', function (  ) {
                                // console.log( $scope.data.mapCircles );
                                angular.forEach( $scope.data.mapCircles, function (circl, cirkey) {
                                    // console.log( 'IsVisible', circl.circle.getVisible() );
                                    if( circl.id == region.id ) {
                                        // console.log( 'Bounds of Circle', circl.circle.getBounds() );
                                        angular.forEach( $scope.data.markerCluster.getMarkers(), function ( mark, markKey ) {
                                            if( google.maps.geometry.spherical.computeDistanceBetween( circl.circle.getCenter(), mark.getPosition() ) <= 5000 ) {
                                                mark.setVisible( true );
                                            } else {
                                                mark.setVisible( false );
                                            }
                                        });
                                        circl.circle.setOptions({fillOpacity: 0.1, strokeOpacity:0.3});

                                    } else {
                                        circl.circle.setOptions({fillOpacity:0, strokeOpacity:0});
                                    }
                                    // $scope.data.mapCircles[ key ].circle.setVisible( false );
                                });
                                map.setZoom(12);
                                map.setCenter(marker.getPosition());
                            });
                            $scope.data.landmarkMarkers.push( marker );
                            if( ! $filter('some')( $scope.data.mapCircles, 'id == ' + region.id ) ) {
                                var circle = new google.maps.Circle({
                                    strokeColor  : '#FF0000',
                                    strokeOpacity: 0,
                                    strokeWeight : 2,
                                    fillColor    : '#FF0000',
                                    fillOpacity  : 0,
                                    map          : map,
                                    center       : {lat: region.location.coordinates[1], lng: region.location.coordinates[0]},
                                    radius       : 5000.00,
                                    visible      : true
                                });
                                $scope.data.mapCircles.push( {
                                    id    : region.id,
                                    circle: circle
                                } );
                            }
                            if( $scope.data.filters.activeLandmarks.selectedRegions.length == (key + 1) ) {
                                new google.maps.event.trigger( marker, 'click' );
                            }
                        });
                    }
                }, 1000);
            }).catch(function (resp) {
                console.log(resp);
            });
        };
        var assignLandmarkFilter = function assignLandmarkFilter ( menu, type ) {
            // console.log( menu, type, typeof $scope.data.filters.activeLandmarks.selectedRegions[ type ] );
            menu.isSelected             = (menu.isSelected)?false:true;
            menu.type                   = type;
            $scope.data.filters.activeLandmarks.region = menu;
            menu.location.type = type;
            // console.log( $scope.data.filters.activeLandmarks.selectedRegions );
            if( menu.isSelected ) {
                $scope.data.filters.activeLandmarks.selectedRegions.push( menu );
                // $scope.data.filters.activeLandmarks.selectedRegions[ type ] = $filter('concat')($scope.data.filters.activeLandmarks.selectedRegions[ type ], [menu.id])
            } else {
                $scope.data.filters.activeLandmarks.selectedRegions = $filter('removeWith')($scope.data.filters.activeLandmarks.selectedRegions, {id: menu.id});
            }
            // console.log( $scope.data.filters.activeLandmarks.selectedRegions );
            $scope.updateHotels();
            // $scope.onPageChange();
        }

        var paginationLimit = function paginationLimit (  ) {
            return $scope.data.page.size * $scope.data.page.shown;
        }
        var hasMoreItemToShow = function hasMoreItemToShow (  ) {
            return $scope.data.page.shown < ( $scope.data.hotels.length / $scope.data.page.size );
        };
        var showMoreItems = function showMoreItems (  ) {

            if( ! $scope.data.viewByMap ) {
                $scope.data.page.shown = $scope.data.page.shown + 1;
            }
        };

        $scope.hasMoreItemToShow    = hasMoreItemToShow;
        $scope.showMoreItems        = showMoreItems;
        $scope.paginationLimit      = paginationLimit;
        $scope.updateHotels         = updateHotels;
        $scope.assignLandmarkFilter = assignLandmarkFilter;
        $scope.showLoading          = showLoading;
        $scope.hideLoading          = hideLoading;
        $scope.onPageChange         = onPageChange;
        $scope.updateMap            = updateMap;
        $scope.getMap               = getMap;
        $scope.clearSelections      = clearSelections;

        jQuery( '.price-slider' ).slider({
            change: function ( event, ui ) {
                $timeout(function () {
                    $scope.data.filters.prices = ui.values;
                    $scope.updateMap();
                    $scope.$apply();
                }, 500);
            }
        });
        console.log( $scope );

        // $scope.data.hotels = Hotels.getAll();
        $timeout(function () {
            $scope.init();
            $scope.data.searchValues = sumAllSearchValues(angular.copy($scope.data.search.roomsData));
            // console.log('Search Data', $scope.data.search, 'Search Values', $scope.data.searchValues);
        }, 1000);
        // Filter with Price Range
        $scope.priceRange = function priceRange (  ) {
            return function ( item ) {
                if( ! $scope.data.filters.prices || $scope.data.hotels.length == 0 ) {
                    return true;
                }
                var price = (item.calculation.calculation.gross_amount > item.calculation.calculation.due_amount)?item.calculation.calculation.gross_amount: item.calculation.calculation.due_amount;
                return (parseFloat(price) >= $scope.data.filters.prices[0] && parseFloat(price) <= $scope.data.filters.prices[1]);
            };
        };
        // Filter with Ratings
        $scope.ratingRange = function ratingRange (filter) {
            return function (item) {
                if( filter.length == 0 || $scope.data.hotels.length == 0 ) {
                    return true;
                }
                // if all values are false return true which means the it will show all results
                var clearArr = checkIfAllFlase(filter);
                if(clearArr) { return true; }

                try {
                    return (typeof filter[item.details.rating] != undefined && filter[item.details.rating].hasOwnProperty('value') && filter[item.details.rating].value);
                } catch (e) { return false; }
                
            };
        };

        /**
         * [filterByLandmarks description]
         * @return {[type]} [description]
         */
        $scope.filterByLandmarks = function filterByLandmarks ( landmark, regions ) {
            // var latLng = new google.maps.LatLng( val.location.coordinates[1], val.location.coordinates[0] );
            // if( google.maps.geometry.spherical.computeDistanceBetween( circl.circle.getCenter(), mark.getPosition() ) <= 5000 ) {
            try {
                // console.log( regions );
                // *landmark
                // location.coordinates [lng, lat]
                // console.log( landmark);
            } catch ( e ) {}
            return function ( item ) {
                // console.log( item )
                // *item
                // details.location.coordinates [lng, lat]
                // Check if the items belongs to the
                if( regions.length == 0 ) { return true; }
                try {
                    var itemLocation = item.details.location.coordinates;
                    var itemLatLng   = new google.maps.LatLng( itemLocation[1], itemLocation[0] );
                    var isBelong     = false;

                    angular.forEach( regions, function ( region, key ) {
                        var regionLocation = region.location.coordinates;
                        var regionLatLng   = new google.maps.LatLng( regionLocation[1], regionLocation[0] );
                        if( google.maps.geometry.spherical.computeDistanceBetween( regionLatLng, itemLatLng ) <= 5000 ) {
                            isBelong = true;
                            return;
                        }
                    });
                    // console.log( isBelong );
                    return isBelong;
                } catch ( e ) {
                    // console.error( e );
                    return false;
                }
            }
        };

        // Filter by Board Types
        $scope.boardTypes = function boardTypes (filter) {
            return function (item) {
                if( filter.length == 0 || $scope.data.hotels.length == 0 ) {
                    return true;
                }
                // console.log('item', item);
                // console.log('boardtypes', filter);

                // Check if all BoardTypes values are false
                var clearArr = checkIfAllFlase(filter);
                if(clearArr) { return true; }

                // Check if the Board type exist on the item
                try {
                    var exist = false;
                    for(var i=0; i<filter.length; i++) {
                        if(typeof filter[i] != 'undefined' && filter[i].hasOwnProperty('value') && item.request.availableBoards.search(filter[i].value) != -1) {
                            exist = true;
                            break;
                        }
                    }
                    return exist;
                } catch (e) { return false; }
            };
        };
        // Filter hotel by propertyType
        $scope.hotelType = function hotelType (filter) {
            return function (item) {
                if( filter.length == 0 || $scope.data.hotels.length == 0 ) {
                    return true;
                }

                // if all values are false return true which means the it will show all results
                var clearArr = checkIfAllFlase(filter);
                if(clearArr) { return true; }

                try {
                    var exist = false;
                    for( var i=0; i<filter.length; i++ ) {
                        if ( typeof filter[i] != 'undefined' && filter[i].hasOwnProperty('value') && filter[i].value == item.details.propertytype ) {
                            exist = true;
                            break;
                        }
                    }

                    return exist;
                } catch (e) { return false; }
            };
        };
        // Clear the Filter Value
        $scope.clearFilter = function clearFilter () {
            try {
                for ( var i=0 ; i<$scope.data.filters.rating.length; i++ ) {
                    if( typeof $scope.data.filters.rating[i] != 'undefined' ) {
                        $scope.data.filters.rating[i].value = false;
                    }
                }
                for ( var i=0 ; i<$scope.data.filters.hoteltype.length; i++ ) {
                    if( typeof $scope.data.filters.hoteltype[i] != 'undefined' ) {
                        $scope.data.filters.hoteltype[i].value = false;
                    }
                }
                for ( var i=0 ; i<$scope.data.filters.boardtypes.length; i++ ) {
                    if( typeof $scope.data.filters.boardtypes[i] != 'undefined' ) {
                        $scope.data.filters.boardtypes[i].value = false;
                    }
                }
            } catch (e) {
                console.info(e);
            }
        };
        // Getting the search hotel data
        $scope.getHotelLocation = function getHotelLocation (val) {
            return $http.get( config.URL + 'api/search-city', {
                    params: {
                        address: val
                    }
                }).then(function(response){
                    // console.log(response);
                    return response.data.map(function(item){
                        return {
                            'name'       : item.cityname + item.countryname,
                            'id'         : item.id,
                            'city'       : item.cityname,
                            'countrycode': item.countrycode
                        };
                    });
                });
        };
        var _selected;
        $scope.hotelCitySelected = function hotelCitySelected (value) {
            // console.log(value);
            if (arguments.length) {
                if(value != undefined) {
                    document.getElementById('location[id]').value           = value.id;
                    document.getElementById('location[city]').value         = value.city;
                    document.getElementById('location[country_code]').value = value.countrycode;
                } else {
                    document.getElementById('location[id]').value           = '';
                    document.getElementById('location[city]').value         = '';
                    document.getElementById('location[country_code]').value = '';
                }
                // $scope.hotelCitySelected = value;
                _selected = value;
            } else {
                return _selected;
            }
        };



        $scope.calculateHotelSelections = function calculateHotelSelections (room, hotel) {
            // console.log( 'providerKey', providerKey );
            // console.log( 'room', room );
            // console.log( 'hotel', hotel );
            // return false;

            ////////////////////
            // CODE FOR INPUT //
            ////////////////////
            // console.log('Room', room);
            // console.log('Hotel', hotel);
            // console.log('Hotel', hotel);

            var quantity = parseInt(room.selectedQuantity);
            // Get the room quantity
            // Keep the maximum mavlue if the user exceed to the maximum quantity
            if( room.maxquantity != room.minquantity ) {
                if ( quantity > parseInt(room.maxquantity) ) {
                    quantity = parseInt(room.maxquantity);
                }
                if ( quantity < parseInt(room.minquantity) ) {
                    quantity = 0;
                }
            }

            if( !angular.isNumber( quantity ) ) {
                console.info('quantity is not number', quantity);
                room.selectedQuantity = '0';
                room.previous         = {};
                return false;
            }
            if( typeof room.previous == 'undefined' ) {
                console.info('undefined previous', quantity);
                room.previous = {};
                if( angular.isNumber( quantity ) ) {
                    $scope.calculateHotelSelections( room, hotel );
                } else {
                    room.selectedQuantity = '0';
                    room.previous         = {};
                }
                return false;
            }

            console.log( quantity );

            
            var totalPrice = room.content.price[0].calculation.calculation.due_amount,
                adults         = parseInt(room.content.pax[0].adults[0]),
                children       = ((room.content.pax[0].children)?parseInt(room.content.pax[0].children[0]):0),
                calculation    = room.content.price[0].calculation;

            //////////////////////////////////////////////////////////////////////////////////////////
            // TODO                                                                                 //
            // 1. Check the Paxes if the selected quantity was already exceed to the required paxes //
            // 2. Calculate the total price per quantity                                            //
            // 3. Update the selected rooms total prices                                            //
            //////////////////////////////////////////////////////////////////////////////////////////

            if( typeof room.originalDueAmount == 'undefined' ) {
                room.originalDueAmount = totalPrice;
            } else {
                totalPrice = room.originalDueAmount;
            }

            adults     *= quantity;
            children   *= quantity;
            totalPrice *= quantity;

            var prevoiusHotel = {};
            prevoiusHotel.adults   = hotel.roomOptions.adults - ((typeof room.previous.adults == 'undefined')? 0:room.previous.adults);
            prevoiusHotel.children = hotel.roomOptions.children - ((typeof room.previous.children == 'undefined')? 0: room.previous.children);

            if( (((adults + prevoiusHotel.adults) > $scope.data.searchValues.adults) || ((children + prevoiusHotel.children) > $scope.data.searchValues.childrens)) ) {
                // in order to recalculate the values
                room.selectedQuantity = '0';
                room.previous         = {};
                // console.log('Error occured', ((adults + prevoiusHotel.adults) > $scope.data.searchValues.adults), ((children + prevoiusHotel.children) > $scope.data.searchValues.childrens));
                return false;
            }

            if( typeof calculation.backup == 'undefined' ) {
                calculation.backup = angular.copy(calculation.calculation);
            }

            // Do the logics
            // When the value is not equal to 0
            // this means that the item is selected
            if( quantity > 0 ) {
                calculation.calculation.gross_amount = calculation.backup.gross_amount * quantity;
                calculation.calculation.commission   = calculation.backup.commission * quantity;
                calculation.calculation.due_amount   = calculation.backup.due_amount * quantity;
                ///////////////////////////////////////////////
                // TODO                                      //
                // 1. Multiply the price by selectedQuantity //
                ///////////////////////////////////////////////

                // Check if the data are exceed

                // if the room is not added
                if( !room.isAdded ) {
                    // update the hotel selected rooms pax

                    hotel.roomOptions.adults     += adults;
                    hotel.roomOptions.children   += children;
                    hotel.roomOptions.totalPrice += totalPrice;
                    hotel.roomOptions.totalRooms += quantity;

                    room.previous.adults           = adults;
                    room.previous.children         = children;
                    room.previous.totalPrice       = totalPrice;
                    room.previous.selectedQuantity = quantity;
                    hotel.roomOptions.selectedRooms.push(room);
                    room.isAdded = true;
                } else {
                    // Update the valuesl
                    // Must record the previous add
                    // in order to update
                    // Remove the prevous selection values
                    hotel.roomOptions.adults     -= room.previous.adults;
                    hotel.roomOptions.children   -= room.previous.children;
                    hotel.roomOptions.totalPrice -= room.previous.totalPrice;
                    hotel.roomOptions.totalRooms -= room.previous.selectedQuantity;

                    // Add the new selections
                    hotel.roomOptions.adults     += adults;
                    hotel.roomOptions.children   += children;
                    hotel.roomOptions.totalPrice += totalPrice;
                    hotel.roomOptions.totalRooms += quantity;

                    room.previous.adults           = adults;
                    room.previous.children         = children;
                    room.previous.totalPrice       = totalPrice;
                    room.previous.selectedQuantity = quantity;
                }
            } else if( quantity == 0 && !angular.equals({}, room.previous) ) {
                // console.log('room.previous', room.previous);
                // remove the room from the selected rooms.
                hotel.roomOptions.adults     -= room.previous.adults;
                hotel.roomOptions.children   -= room.previous.children;
                hotel.roomOptions.totalPrice -= room.previous.totalPrice;
                hotel.roomOptions.totalRooms -= room.previous.selectedQuantity;

                room.isAdded             = false;
                room.previous.totalPrice = room.originalDueAmount;
                                                                      
                calculation.calculation.gross_amount = calculation.backup.gross_amount;
                calculation.calculation.commission   = calculation.backup.commission;
                calculation.calculation.due_amount   = calculation.backup.due_amount;

                angular.forEach(hotel.roomOptions.selectedRooms, function (value, key) {
                    if( value.resultid == room.resultid ) {
                        hotel.roomOptions.selectedRooms.splice(key, 1);
                    }
                });
            }
            room.content.price[0].calculation.calculation = calculation.calculation;
            hotel.roomOptions.readyToSubmit = $scope.checkForEqualization(hotel);
        };
        $scope.checkForEqualization = function checkForEqualization (hotel) {
            // console.log('Selected Rooms', hotel, 'Search Data', $scope.data.searchValues);
            return ( hotel.roomOptions.adults == $scope.data.searchValues.adults &&
                hotel.roomOptions.children == $scope.data.searchValues.childrens &&
                hotel.roomOptions.totalRooms == $scope.data.searchValues.rooms);
        };
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };

        $scope.hoteStartDate = function hoteStartDate () {
            var split = $scope.data.hotel.startDate.split('/');
            
            var checkIn = new Date(split[1] + '/' + split[0] + '/' + split[2]);
            checkIn.setDate(checkIn.getDate()+2);
            var format = (checkIn.toISOString().substr(0,10)).split('-');   
            $scope.data.hotel.endDate = format[2] + '/' + format[1] + '/' + format[0];
        };

        $scope.getHotelRooms = function getHotelRooms ( hotel, hideRooms = false ) {
            // console.log(hotel);
            $timeout(function () {
                if( typeof hotel == 'undefined' ) {
                    return false;
                }
                if( typeof hotel.rooms != 'undefined' ) {
                    // console.log( 'has rooms' );
                    hotel.showRooms     = ((!hotel.showRooms)?true:false);
                    hotel.showError              = false;
                    hotel.isDisableRoomButton    = false;
                    hotel.fetchingRooms          = true;
                    hotel.workingOnFetchingRooms = false;
                    return false;
                }
                hotel.showRooms           = ((!hotel.showRooms)?true:false);
                // hotel.fetchingRooms       = true;
                hotel.isDisableRoomButton = true;
                if( hideRooms )
                {
                    hotel.showRooms = false;
                }
                if( ! hotel.rooms || hotel.rooms.length == 0 && hotel.showRooms ) {
                    $http.get(config.URL + 'api/hotel-rooms/' + hotel.id)
                        .success(function (success) {
                            if( success.job[0].status == 'failed' ) {
                                hotel.showRooms = false;
                                $timeout( function () { $scope.getHotelRooms( hotel, hideRooms ); }, 5000 );
                                return false;
                            }

                            /*success.result[0].content.rooms[0].room = success.result[0].content.rooms[0].room.map(function (item) {
                                item.content.provider = item.content.provider[0];
                                return item;
                            });*/

                            // console.log( success.result[0].content.rooms[0].room[0] );
                            hotel.rooms                  = [];
                            hotel.rooms                  = success.result[0].content;
                            // hotel.calculation            = $filter('orderBy')( hotel.rooms.rooms[0].room, $scope.comaprator.price, false )[0].content.price[0].calculation;
                            hotel.showError              = false;
                            hotel.isDisableRoomButton    = false;
                            hotel.fetchingRooms          = true;
                            hotel.workingOnFetchingRooms = false;
                            // hideLoading( 'div#hotel-price-' + hotel.id );
                        })
                        .error(function (error, status) {
                            $timeout( function () { $scope.getHotelRooms( hotel, hideRooms ); }, 5000 );
                            // console.log('Status', status);
                            // // console.error(error);
                            // if( typeof hotel.rooms == 'undefined' || hotel.rooms.length == 0 ) {
                            //     hotel.showError = true;
                            //     if( status == 401 ) {
                            //         hotel.errorMessage = 'Network is buzy';
                            //         hotel.errorInRetreiving = true;
                            //     }
                            // }
                            // // jQuery('#' + hotel.id).waitMe('hide');
                            // hotel.fetchingRooms       = false;
                            // hotel.isDisableRoomButton = false;
                        });
                } else {
                    // jQuery('#' + hotel.id).waitMe('hide');
                    hotel.fetchingRooms       = false;
                    hotel.isDisableRoomButton = false;
                }
            }, 0);
        };
        $scope.loadAllHotels = function loadAllHotels () {

            if( $scope.data.isAllItem ) {
                $scope.data.showRequestStatus = false;
            }

            jQuery('#load-hotels').switchClass('fa-check', 'fa-refresh fa-spin');
            $scope.data.counts = $scope.data.dumpHotels.counts;
            $scope.data.status = 'load-more-hotels';


            jQuery('.price-slider').slider("option", "min", 0);
            jQuery('.price-slider').slider("option", "max", $scope.data.counts.price.max);

            /*jQuery('#table-wrapper').waitMe({
                effect : 'pulse',
                text : 'Please wait to load all hotels . . .',
                bg : 'rgba(255,255,255,0.7)',
                color : '#000',
                maxSize : '',
                source : ''
            });*/
            $scope.data.notification.isHotel = false;
            $scope.data.notification.message = 'Looking for more hotels . . .';
            $scope.data.hotels = $scope.data.dumpHotels.result;
            $scope.updateHotels();
            // Test for adjusting ng-repeat performance in order to escape from hanging
            // var interval = $interval(function () {
            //     console.log('Interval is running', $scope.data.dumpHotels.result.length, $scope.data.hotels.length);
            //     if( $scope.data.dumpHotels.result.length == 0 ) {
            //         jQuery('#table-wrapper').waitMe('hide');
            //         jQuery('#load-hotels').switchClass('fa-refresh fa-spin', 'fa-check');
            //         $interval.cancel(interval);
            //     }

            //     $scope.data.hotels = $scope.data.hotels.concat(angular.copy($scope.data.dumpHotels.result.splice(0,50)));
            //     $scope.data.showRequestStatus = false;
            // }, 1000);
        };
        // $scope.showHotelCombinations = function (hotel) {};

        $scope.init = function init (hasData = false) {

            $scope.data.countAttempt++;
            // console.log('Attempts: ' + $scope.data.countAttempt);

            // Retrieving search result
            $http.get( config.URL + 'api/search-result/initial/' + ((hasData)?1:0) + '?length=' + $scope.data.hotels.length)
                .success(function (response) {
                    // TODO
                    // Put the data from the hotels
                    
                    // If the data came from the database display in instance
                    // else backup the data and let the use load all the data.
                    if( typeof response.isDatabase != 'undefined' ) {
                        // Hiding buttons
                        $scope.data.showRequestStatus = false;
                        $scope.data.hotels = response.result;
                        $scope.data.counts = response.counts;
                        jQuery('.price-slider').slider("option", "min", 0);
                        jQuery('.price-slider').slider("option", "max", $scope.data.counts.price.max);
                    } else {
                        $scope.data.dumpHotels = response;

                        var countDef = (response.result.length - $scope.data.hotels.length);

                        if( countDef == 0 ) {
                            $scope.data.notification.message = 'All hotels are ready';
                        } else {
                            $scope.data.notification.message = 'There are ' + countDef + ' new hotels found';
                            $scope.loadAllHotels();
                        }
                        $scope.data.notification.isHotel = true;
                    }
                    
                    $scope.data.isAllItem = true;
                    $scope.data.status = 'done';
                    $scope.updateHotels();
                })
                .error(function (error, status) {
                    // console.error(error, status);
                    if( !angular.equals({}, error) )
                    {
                        if ( status == 401 )
                        {
                            $scope.data.status        = 'error';
                            $scope.data.error.message = 'Session expired, please search again';
                        } else {
                            if (  typeof error.success != 'undefined' && error.success == 0 )
                            {
                                $scope.data.status        = 'error';
                                $scope.data.error.message = error.message;
                            } else {
                                if( typeof error.content != 'undefined' )
                                {
                                    if( $scope.data.hotels.length == 0 )
                                    {
                                        $scope.data.status = error.content.job[0].status;
                                    }

                                    if( angular.isString(error.content.job[0].message) )
                                    {
                                        $scope.data.status = 'error';
                                        $scope.data.error.message = 'No result match, please try again later.';
                                    } else {
                                        if ( typeof error.result != 'undefined')
                                        {
                                            if( $scope.data.hotels.length == 0 ) {
                                                $scope.data.showRequestStatus = true;
                                                $scope.data.hotels            = error.result;
                                                $scope.data.counts            = error.counts;
                                                jQuery('.price-slider').slider("option", "min", 0);
                                                jQuery('.price-slider').slider("option", "max", $scope.data.counts.price.max);
                                                $scope.data.status = 'done';
                                                $scope.updateHotels();
                                            } else {
                                                // console.log( error.result.length, $scope.data.hotels.length );
                                                if( error.result.length > $scope.data.hotels.length ) {
                                                    var countDef = (error.result.length - $scope.data.hotels.length);
                                                    $scope.data.notification.message = 'There are ' + countDef + ' new hotels found';
                                                    $scope.data.notification.isHotel = true;

                                                    $scope.data.dumpHotels = error;
                                                    $scope.loadAllHotels();
                                                }
                                            }
                                        }
                                        $timeout(function () {
                                            init( ($scope.data.hotels.length > 0) );
                                        }, 8000);
                                    }
                                } else {
                                    if( typeof error == 'object' && error.length == 0 && $scope.data.hotels.length == 0 )
                                    {
                                        $scope.data.status        = 'error';
                                        $scope.data.error.message = 'No result match, please try again later.';
                                    } else if ( $scope.data.hotels.length == 0 ) {
                                        $scope.data.status        = 'error';
                                        $scope.data.error.message = 'Network is buzy, please try searching again';
                                    }
                                }
                            }
                        }
                    }
                    var hasData = ($scope.data.hotels.length == 0);
                    /*if( hasData )
                    {

                    } else {
                        $timeout(function () {
                            init( ($scope.data.hotels.length > 0) );
                        }, 8000);
                    }*/
                });
        };
}])
.directive('combinationDescription', [function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            pax: '='
        },
        template: '<div style="font-size: 1.2em; padding-left:10px; padding-right:10px; ">'
                        +'Good for <strong>{{ pax.rooms }} rooms</strong>, '
                        +'<strong>{{ pax.adults }} Adult/s</strong> and <strong>{{ pax.childrens }} Child/s</strong>'
                    +'</div>'
    };
}])
.filter('registerData', [function () {
    return function (input, mainValue) {
        mainValue += parseInt(input);
        return mainValue;
    };
}])
.filter('increment', [function () {
    return function (input) {
        return input++;
    };
}])
.filter('convertToNumber', [function () {
    return function (input) {
        return parseFloat(input);
    };
}])
.filter('converToDate', [function () {
    return function (input, moveDate = 0) {
        // console.log(input);
        try {
            var date = input.split(' ')[0];
            date = new Date(date);
            if ( typeof moveDate != 'undefined' && moveDate > 0 ) {
                date.setDate(date.getDate() - 1);
            }

            return new Date(date.toISOString().substr(0,10));
        } catch ( e ) {
            return new Date();
        }
    };
}])
.filter('mainImage', [function () {
    return function (input) {
        var main = '';
        angular.forEach(input, function (value, key) {
            if( value.ismain == 1 ) {
                main = value;
                return false;
            }
        });
        return main;
    };
}])
.service("Comparators", function() { 
    /*this.price = function( room1, room2 ) {
        console.log( room1, room2 );
        var r1 = (room1.previous.totalPrice)?room1.previous.totalPrice:room1.content.price[0].calculation.calculation.due_amount;
        var r2 = (room2.previous.totalPrice)?room2.previous.totalPrice:room2.content.price[0].calculation.calculation.due_amount;

        // if(r1.planYear === r2.planYear) {
        //     if (r1.extent === r2.extent) return 0;
        //         return r1.extent > r2.extent ? 1 : -1;
        if(!r1 || !r2) {
            return !r1 && !r2 ? 0 : (!r1 ? 1 : -1);
        }
        return r1 > r2 ? 1 : -1;
    };*/
    this.pax = function pax ( room ) {
        try {
            var adults = room.content.pax[0].adults[0];
            var childs = (room.content.pax[0].hasOwnProperty('children'))?room.content.pax[0].children[0]:0;

            return (parseInt(adults) + parseInt(childs));
        } catch ( e ) {
            console.warn( e );
            return 0;
        }
    };
    this.price = function price ( room ) {
        try {
            var v = room.content.price[0].new_content;
            // console.log( v );
            return v;
        } catch ( e ) {
            console.log( room );
            console.warn( '', e );
            return 999999;
        }
    };
    this.quantity = function ( room1, room2 ) {
        var r1 = parseInt(room1.maxquantity);
        var r2 = parseInt(room2.maxquantity);
        // console.log( price1, price2 );

        // if(r1.planYear === r2.planYear) {
        //     if (r1.extent === r2.extent) return 0;
        //         return r1.extent > r2.extent ? 1 : -1;
        if(!r1 || !r2) {
            return !r1 && !r2 ? 0 : (!r1 ? 1 : -1);
        }
        return r1 > r2 ? 1 : -1;
    };
})
/**
 * Author: Eric Ferreira <http://stackoverflow.com/users/2954747/eric-ferreira> ©2015
 *
 * This filter will sit in the filter sequence, and its sole purpose is to record 
 * the current contents to the given property on the target object. It is sort of
 * like the 'tee' command in *nix cli.
 */
.filter('record', function() {
    return function(array, scope) {
        if (scope) {
            scope.data.itemsOnPage = array;
        }
        return array;
    }
})
.filter('orderBySpecialDeals', function () {
    return function ( items ) {
        return items.sort(function (a,b) {
            if( typeof a.request.content['special-deals'] == 'undefined' ) {
                return 1;
            }
            return -1;
        });
    };
})
.filter('orderByRating', function () {
    return function ( items ) {
        return items.sort(function (a, b) {
            if( parseInt(a.details.rating) < parseInt(b.details.rating) )
                return 1;
            
            // if( parseInt(a.details.rating) < parseInt(b.details.rating) )
            //     return -1;

            return -1;
        });
    };
})
;