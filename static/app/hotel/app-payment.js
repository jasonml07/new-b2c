

angular.module('PaymentApplication', ['custom.utility'])
.controller('PaymentController', ['$scope', '$http', 'config', '$q',
	function ( $scope, $http, config, $q ) {
		console.log( 'PaymentApplication deployed' );

		$scope.data               = {};
		$scope.data.pax           = {};
		$scope.data.errors        = {};
		$scope.data.creditError   = {};
		$scope.data.handOfURLTEST = config.URL + 'hotel/payWayCallBackTest';
		$scope.data.isNewBook     = false;
		$scope.data.isPayable     = false;
		$scope.data.isTest        = false;
		$scope.data.paywayData    = {};
		$scope.data.hotelId       = 0;

		$scope.data.voucherOwner  = {};

		$scope.closePaymentModal = function closePaymentModal () {
			jQuery('div#payment-modal').modal('hide');
			jQuery('#waiters').waitMe('hide');
		};

		if( config.TEST_INPUTS ) {
			$scope.data.payment       = {
				no_cvn: '847',
				// nm_card_holder: 'TEST',
				no_credit_card: '4564710000000004'
			};
			$scope.data.payment.nm_card_holder = 'TEST';
			jQuery.each( jQuery('#paxForm input'), function ( key, ele ) {
				var val = '';
				var type = jQuery( ele ).attr('type');
				if( type == 'text' || type == 'email' ) {
					switch ( jQuery( ele ).data('value') ) {
						case 'name':
							val = faker.name.firstName();
							break;
						case 'email':
							val = faker.internet.email();
							break;
						case 'address':
							val = faker.address.streetAddress();
							break;
						case 'phone':
							val = faker.phone.phoneNumber();
							break;
						case 'city':
							val = faker.address.city();
							break;
					}
					jQuery( ele ).val( val );
				}
			} );
		}

		$scope.data.isPaymentApproved = false;

		angular.element(document).ready( function () {
			$scope.submitForm = function submitForm ( isBookExist ) {

				if( isBookExist ) {
					BootstrapDialog.confirm(
						{
							title         : '<span class="fa fa-info-circle"></span> ALERT!!!',
							message       : 'Are you sure you want to add this item to your existing booking?',
							draggable     : true,
							closable      : true,
							cssClass      : 'adjust-bootstrap-dialog',
							btnCancelLabel: 'No',
							btnOKClass    : 'btn-success',
							btnOKLabel    : 'Yes',
							callback      : function ( result ) {
								$scope.data.isNewBook = (result)?false:true;
								$scope.submitForm( false );
								$scope.$apply();
							}
						}
					);
				} else {
					// console.log( jQuery('#ship-to-different-address-checkbox').prop('checked') );

					// Validate the inputs
					var data = jQuery('#paxForm').serializeObject();
					data.policy_read = jQuery('#ship-to-different-address-checkbox').prop('checked');
					data.forNewBook  = $scope.data.isNewBook;

					$scope.data.formData = angular.copy(data);

					jQuery('#waiters').waitMe({
						effect : 'pulse',
						text   : 'Please wait while validating your inputs',
						bg     : 'rgba(255,255,255,0.7)',
						color  : '#000',
						maxSize: '',
						source : ''
					});

					if( $scope.data.isPayable ) {
						var paymentData = jQuery('#paymentForm').serializeObject();
						data.payment = {
							dt_expiry_month: paymentData.dt_expiry_month,
							dt_expiry_year : paymentData.dt_expiry_year,
							nm_card_holder : paymentData.nm_card_holder,
							no_credit_card : paymentData.no_credit_card,
							no_cvn         : paymentData.no_cvn,
						};
					}

					// Validate the data inputs
					$http.post( config.URL + 'hotel/booking-validation', data )
						.success( function ( response, status ) {
							if( status == 200 ) {
								if( response.success ) {
									// Before submitting must validate the input first
									if( $scope.data.isPayable ) {

										$scope.creditCardBooking( response );
										/* Everything is payable via credit card */
										// Submit the form manualy
										// Add the action link
										/*jQuery('div#payment-modal').modal('show');
										jQuery('iframe#my_iframe').parent().waitMe({
											effect : 'pulse',
											text   : '',
											bg     : 'rgba(255,255,255,0.7)',
											color  : '#000',
											maxSize: '',
											source : ''
										});

										jQuery('#paymentForm input[name="token"]').val( response.token );
										jQuery('#paymentForm').attr( 'action', ($scope.data.isTest)?$scope.data.handOfURLTEST:response.handOffUrl );
										jQuery('#paymentForm').submit();

										jQuery('iframe#my_iframe').on('load', function () {
											jQuery('iframe#my_iframe').parent().waitMe('hide');
										});*/
									} else {
										// if the user dont want to pay..
										// submit call the validateInputs( $scope.data.isPayable );
										$scope.createBooking( 'declined', $scope.data.hotelId, {pax: data} );
									}
								}
							}
						})
						.error( function ( error, status ) {
							console.error(error, status);
							jQuery('#waiters').waitMe('hide');
							if( angular.isObject(error) && error.hasOwnProperty('main') ) {
								$scope.data.errors = error;
							}
							// Handle the creation of booking error
						});
				}
			};

			$scope.close = function close () {
				jQuery('#credit-modal').modal('hide');
			};

			/**
			 * [creditCardBooking description]
			 * @param  {[type]} response [description]
			 * @return {[type]}          [description]
			 */
			$scope.creditCardBooking = function creditCardBooking ( response )
			{
				console.info( 'creditCardBooking', response );
				/*===================================
				=            Credit Card            =
				===================================*/

				/**
				
					TODO:
					- Get reference "getDataReference"
					- Get PayWay token "getPayWayToken"
					- Save the Post Data for creating booking "saveBookingData"
					- Submit the form
				
				 */
				
				getDataReference().then(function ( data ) {
					// console.log( 'Deferred:: getDataReference', data );
					var reference = data.reference;
					getPayWayToken( reference, response ).then(function ( data ) {
						// console.log( 'Deferred:: getPayWayToken', data );

						var payway      = data;
						var paymentData = jQuery('#paymentForm').serializeObject();
						var pay         = {
							dt_expiry_month: paymentData.dt_expiry_month,
							dt_expiry_year : paymentData.dt_expiry_year,
							nm_card_holder : paymentData.nm_card_holder,
							no_credit_card : paymentData.no_credit_card,
							no_cvn         : paymentData.no_cvn,
							card_type      : '', /* response.card_type Retrieve from return link of PayWay */
							payment_amount : '', /* response.payment_amount Retrieve from return link of PayWay */
							reference      : reference /* response.payment_reference Also can be retrieve from return link of PayWay */
						};
						var post_data = {
							hotelId     : $scope.data.hotelId,
							isCreditCard: true,
							pax         : $scope.data.formData,
							payment     : pay
						};

						saveBookingData( reference, post_data ).then(function ( data ) {
							// console.log( 'Deferred:: saveBookingData', data );
							
							/*----------  Add token to the Form and Action then Submit  ----------*/
							jQuery('#paymentForm input[name="token"]').val( payway.token );
							jQuery('#paymentForm').attr( 'action', payway.handOffUrl );
							jQuery('#paymentForm').submit();
						});
					});
				});

				return false;
			};


			/**
			 * Generate token for PayWay
			 * @return {[type]} [description]
			 */
			var getDataReference = function getDataReference ( )
			{
				console.info( 'getDataReference' );

				var deferred = $q.defer();

				/* Get reference from database */
				$http.get( config.URL + 'Payway/uniqueBookingId' )
				.success(function ( response, status ) {
					deferred.resolve( response );
				})
				.error(function ( error, status ) {
					deferred.reject( error );
				});

				return deferred.promise;
			};

			/**
			 * Generate PayWay token
			 * @param  {[type]} data 	  products and prices
			 * @param  {[type]} reference 
			 * @return {[type]}           [description]
			 */
			var getPayWayToken = function getPayWayToken ( reference, data )
			{
				console.info( 'getPayWayToken' );

				var deferred = $q.defer();

				data.returnURL = 'hotel/returnPayWayResponse';

				/* testing is to reply a static data */
				// $http.post( config.URL + 'Payway/getToken?testing=true&reference=' + reference, data )
				$http.post( config.URL + 'Payway/getToken?reference=' + reference, data )
				.success(function ( response, status ) {
					// console.log( 'response', response, 'status', status );
					deferred.resolve( response );
				})
				.error(function ( error, status ) {
					deferred.reject( error );
				});

				return deferred.promise;
			}

			/**
			 * [saveBookingData description]
			 * @param  {[type]} reference [description]
			 * @param  {[type]} data      [description]
			 * @return {[type]}           [description]
			 */
			var saveBookingData = function saveBookingData ( reference, data )
			{
				console.info( 'saveBookingData' );

				var deferred = $q.defer();

				$http.post( config.URL + 'Payway/saveBookingData?reference=' + reference, data )
				.success(function ( response, status ) {
					// console.log( 'response', response, 'status', status );
					deferred.resolve( response );
				})
				.error(function ( error, status ) {
					deferred.reject( error );
				});

				return deferred.promise;
			}

			$scope.createBooking = function createBooking ( status, hotelId, data ) {
				console.log('Creating Booking');

				$http.post( config.URL + 'hotel/createBooking/' + status+ '?id=' + hotelId, data )
					.success( function ( response )  {
						// console.log(response);
						$scope.data.reservationId = response.reservationId;
						$scope.data.bookingId     = response.bookingId;
						jQuery('#waiters').waitMe('hide');
						jQuery('#modal').modal('show');
					})
					.error( function ( error ) {
						console.error(error);
						jQuery('#waiters').waitMe('hide');
						if( angular.isObject( error ) ) {
							if( typeof error.isModal != 'undefined' ) {
								BootstrapDialog.show(
									{
										title          : '<span class="fa fa-times-circle-o"></span> ALERT!!!',
										type           : BootstrapDialog.TYPE_DANGER,
										message        : error.error,
										closeByBackdrop: false,
										closable       : false,
										cssClass       : 'adjust-bootstrap-dialog',
										buttons        : [{
											label   : 'Return to search result',
											cssClass: 'btn-primary',
											action  : function (dialog) {
												window.location.href = config.URL + 'hotel/searchResult'
											}
										}]
									}
								);
							} else {
								$scope.data.errors = {
									main: [error.error]
								};
							}
						}
					});
			};
		});
	}])
;


function paymentValidation ( response ) {

	var sel = '[ng-controller="PaymentController"]';
	var $scope = angular.element( sel ).scope();

	$scope.data.isPaymentApproved = response.payment_status;
	$scope.$apply();

	if( $scope.data.isPayable ) {
		jQuery('div#payment-modal').modal('toggle');
	}

	if( $scope.data.isPaymentApproved == 'approved' ) {
		var paymentData = jQuery('#paymentForm').serializeObject();
		var pay = {
			dt_expiry_month: paymentData.dt_expiry_month,
			dt_expiry_year : paymentData.dt_expiry_year,
			nm_card_holder : paymentData.nm_card_holder,
			no_credit_card : paymentData.no_credit_card,
			no_cvn         : paymentData.no_cvn,
			card_type      : response.card_type,
			payment_amount : response.payment_amount,
			reference      : response.payment_reference
		};
		// Create the bookinge
		// Send the pax and create booking
		$scope.createBooking( 'approved', $scope.data.hotelId, {pax: $scope.data.formData, payment: pay} );
	} else {
		jQuery('#waiters').waitMe('hide');
		BootstrapDialog.show({
			type           : BootstrapDialog.TYPE_DANGER,
			title          : '<span class="fa fa-times-circle"></span> <span class="text-center" style="text-transform: uppercase">Sorry your payment was <strong>' + response.payment_status + '</strong></span>',
			message        : response.response_text,
			cssClass       : 'adjust-bootstrap-dialog',
			closable       : true,
			closeByBackdrop: false
		});
	}
}