angular.module('DetailApplication', ['ui.bootstrap', 'ngSanitize', 'custom.utility'])
.controller('DetailController', ['$scope', '$http', '$timeout', 'config',
	function ( $scope, $http, $timeout, config ) {
		console.log( 'DetailApplication deployed' );

		$scope.data              = {};
        $scope.data.accordion    = {};
        $scope.data.rooms        = [];
        $scope.data.hasSelection = false;
        $scope.data.totalPrice   = 0;
        $scope.data.selectRoom   = {
            room: 0,
            childs: 0,
            adults: 0,
            readyToBook: false
        };

        var checkEqualization = function checkEqualization () {
            // console.log( 'checkEqualization' );
            return ( $scope.data.selectRoom.adults == $scope.data.numberOfGuest.adults &&
                $scope.data.selectRoom.childs == $scope.data.numberOfGuest.childrens &&
                $scope.data.selectRoom.room == $scope.data.numberOfGuest.rooms );
        };
        var clearAllSelection = function clearAllSelection ( rooms ) {
            if( rooms.length > 0 ) {
                angular.forEach( rooms, function ( room, key ) {
                    var id   = 'room-' + room.resultid.replace('.', '');
                    var jEle = jQuery('select#' + id);
                    jEle.val('0');
                    $timeout(function () {
                        jEle.change();
                    });
                });
            }
        };
        var selectRoom = function selectRoom (roomId, room) {
            // Problem
            // * When selecting room in decending order, bug will occured cause a disfunction of the selection
            // Posible solutions are:
            // 1. When the selected quantity is equals to 0 then the previous property will return to empty
            //      * this will include the updation of total pax which will be decreased depends on the previous selection
            //      * also the total price should be decreased depending on the previous selection
            //      
            // 2. Only cater the selected quantity value which is greater than 0
            //      * Update the selected quantity
            //      * update the total prices
            
            // console.log( roomId, room );
            // console.log( accordion );
            // console.log( $scope.data.accordion );
            // return false;
            var quantity = parseInt(room.selectedQuantity);
            // console.log(roomId, room);
            if( !angular.isDefined(roomId) ) {
                console.error( 'Room Id is not defined' );
                return false;
            }
            if( !angular.isDefined(room) ) {
                console.error( 'Room is not defined' );
                return false;
            }
            /*if( !angular.isDefined(accordion) ) {
                console.error( 'accordion is not defined' );
                return false;
            }*/

            /*if( typeof accordion.rooms[ roomId ] != 'object' ) {
                console.error( 'Room is not defined' );
                return false;
            }*/

            if( room.maxquantity != room.minquantity ) {
                if ( quantity > parseInt(room.maxquantity) ) {
                    quantity = parseInt(room.maxquantity);
                }
                if ( quantity < parseInt(room.minquantity) ) {
                    quantity = 0;
                }
            }

            if( !angular.isNumber( quantity ) ) {
                console.info('quantity is not number', quantity);
                room.selectedQuantity = '0';
                room.previous = {};
                return false;
            }

            if( typeof room.previous == 'undefined' ) {
                console.info('undefined previous', quantity);
                room.previous = {};
                if( angular.isNumber( quantity ) ) {
                    $scope.selectRoom( roomId, room );
                } else {
                    room.selectedQuantity = '0';
                    room.previous = {};
                }
                return false;
            }

            //////////////////////////
            // SELECTION LOGIC HERE //
            //////////////////////////

            var adult = parseInt(room.content.pax[0].adults[0]),
                child = ((room.content.pax[0].children)?parseInt(room.content.pax[0].children[0]):0),
                price = room.content.price[0].calculation.calculation.due_amount,
                calculation    = room.content.price[0].calculation;

            if( typeof room.originalDueAmount == 'undefined' ) {
                room.originalDueAmount = price;
            } else {
                price = room.originalDueAmount;
            }


            child *= quantity;
            adult *= quantity;
            price *= quantity;

            var prevoiusHotel = {};
            prevoiusHotel.adults   = $scope.data.selectRoom.adults - ((typeof room.previous.adults == 'undefined')? 0:room.previous.adults);
            prevoiusHotel.children = $scope.data.selectRoom.childs - ((typeof room.previous.children == 'undefined')? 0: room.previous.children);

            if( (((adult + prevoiusHotel.adults) > $scope.data.numberOfGuest.adults) || ((child + prevoiusHotel.children) > $scope.data.numberOfGuest.childrens))  ) {
                // in order to recalculate the values
                room.selectedQuantity = '0';
                room.previous = {};
                // $scope.selectRoom(roomId, room);
                // console.log('Error occured', ((adults + prevoiusHotel.adults) > $scope.data.searchValues.adults), ((children + prevoiusHotel.children) > $scope.data.searchValues.childrens));
                return false;
            }

            if( typeof calculation.backup == 'undefined' ) {
                calculation.backup = angular.copy(calculation.calculation);
            }

            if( quantity > 0 ) {
                // Update the calculation from the backup ones
                calculation.calculation.gross_amount = calculation.backup.gross_amount * quantity;
                calculation.calculation.commission   = calculation.backup.commission * quantity;
                calculation.calculation.due_amount   = calculation.backup.due_amount * quantity;
                // Set status for the room
                if( !room.isAdded ) {
                    $scope.data.totalPrice        += price;
                    $scope.data.selectRoom.room   += quantity;
                    $scope.data.selectRoom.childs += child;
                    $scope.data.selectRoom.adults += adult;


                    // Record the prevous selection
                    room.previous.adults           = adult;
                    room.previous.children         = child;
                    room.previous.totalPrice       = price;
                    room.previous.selectedQuantity = quantity;
                    room.isAdded = true;
                } else {
                    // Remove the prevoius Record
                    $scope.data.totalPrice        -= room.previous.totalPrice;
                    $scope.data.selectRoom.room   -= room.previous.selectedQuantity;
                    $scope.data.selectRoom.adults -= room.previous.adults;
                    $scope.data.selectRoom.childs -= room.previous.children;


                    // Add the new selection
                    $scope.data.totalPrice        += price;
                    $scope.data.selectRoom.room   += quantity;
                    $scope.data.selectRoom.adults += adult;
                    $scope.data.selectRoom.childs += child;


                    // Record the prevoius record
                    room.previous.adults           = adult;
                    room.previous.children         = child;
                    room.previous.totalPrice       = price;
                    room.previous.selectedQuantity = quantity;
                }

            } else if ( quantity == 0 && !angular.equals({}, room.previous) ) {
                // Return to the default value
                $scope.data.totalPrice        -= room.previous.totalPrice;
                $scope.data.selectRoom.room   -= room.previous.selectedQuantity;
                $scope.data.selectRoom.adults -= room.previous.adults;
                $scope.data.selectRoom.childs -= room.previous.children;


                room.isAdded             = false;
                room.previous.totalPrice = room.originalDueAmount;

                calculation.calculation.gross_amount = calculation.backup.gross_amount;
                calculation.calculation.commission   = calculation.backup.commission;
                calculation.calculation.due_amount   = calculation.backup.due_amount
            }
            // console.log( room );
            room.content.price[0].calculation.calculation = calculation.calculation;
            // Check if there are selections of the rooms
            // $scope.data.rooms[ roomId ] = room;
            $scope.data.selectRoom.readyToBook = $scope.checkEqualization( );
            $scope.roomDataIsEmpty( );
        };
        

        var roomDataIsEmpty = function roomDataIsEmpty () {
        	var hasSelection = false;
            angular.forEach($scope.data.rooms, function (val, key) {
                if( typeof val.isAdded != 'undefined' && val.isAdded) {
                    // If the room is click then there is click
                    hasSelection = true;
                    return false;
                }
            });

            $scope.data.hasSelection = hasSelection;
        };

		$scope.checkEqualization = checkEqualization;
		$scope.selectRoom        = selectRoom;
		$scope.roomDataIsEmpty   = roomDataIsEmpty;
        $scope.clearAllSelection = clearAllSelection;
	}])