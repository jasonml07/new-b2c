(function ( $ ) {
	var $passengers = $( '.passengers' );
	var PASSENGERS = {
		eleContainer: $passengers,
		eleForm     : $passengers.find('#passengers'),

		init: function () {
			console.log('PASSENGERS', this.eleForm.serializeObject() );

			this.eleForm.find('[data-bind]').change(function () {
				var $this = $( this );

				VOUCHER.eleForm.find('[name="' + $this.data('bind') + '"]').val( $this.val() );
			});
		},
	}
	var $voucher = $( '.voucher' );
	var VOUCHER = {
		eleContainer: $voucher,
		eleForm     : $voucher.find('#voucher'),

		init: function () {
			console.log('VOUCHER', this.eleForm.serializeObject() );
		},
	};

	var $payment = $( '.payment' );
	var PAYMENT = {
		eleContainer: $payment,
		eleForm     : $payment.find('#payment-credit-card'),

		init: function () {

		},
	};
	var TEST_INPUTS = {

		init: function (  ) {
			console.info('Test mode: ON');
			PASSENGERS.eleForm.find('[name]').each(function ( key, ele ) {
				var $this = $( ele );

				if ( $this.get(0).tagName.toLowerCase() == 'select' )
				{
					var $options = $this.find('option');
                    var $options = $.grep( $options, function ( n, i ) {
                        return ( $( n ).val() != '' );
                    });

                    var choices = [];
                    $.each( $options, function ( key, option) {
                        var $ele = $( option );
                        choices.push( $ele.val() );
                    });

                    try {
                        var random = Math.floor(Math.random() * choices.length) + 1;

                        $this.val( choices[ ( random - 1 ) ] );
                    } catch ( e ) { }
				} else {
					$this.val( faker.name.firstName() );
				}
				if ( $this.data('bind') != undefined ) { $this.change(); }
			});
			VOUCHER.eleForm.find('[name="voucher[email]"]').val( faker.internet.email() );
			VOUCHER.eleForm.find('[name="voucher[phone]"]').val( faker.phone.phoneNumber() );

			PAYMENT.eleForm.find('#credit-card-cardholder-name').val('TEST TEST');
			PAYMENT.eleForm.find('#credit-card-card-number').val('1234567890123456');
			PAYMENT.eleForm.find('#credit-card-verification-number').val('123');
			PAYMENT.eleForm.find('#credit-card-expiry-month').val('02');
			PAYMENT.eleForm.find('[name="dt_expiry_year"]').val('20');
		}
	};

	function init () {
		PASSENGERS.init();
		VOUCHER.init();

		<?php if ( TEST_INPUTS===TRUE ) : ?>
		TEST_INPUTS.init();
		<?php endif; ?>
	}
	init();
})(jQuery);
