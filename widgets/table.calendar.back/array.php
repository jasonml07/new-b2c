<?php

//702, 705, 715
//no prices 176
$connection = new MongoClient();
$db         = $connection->db_system;
$exampleID  = 705;	
$markUp     = 0.33;

$arrayResults = $db->products_inventory->find( array( "productId" => $exampleID, "is_active" => 1 ) );
foreach ( $arrayResults as $key => $value ) {
	# code...
	echo '<script>var arrayFromPhp = ' . json_encode($value) . ';</script>';
}

$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
  $refCurr[$val['curr_from']] = (float)$val['rate_to'];
}

echo '<script>var refCurrArr = ' . json_encode($refCurr) . ';</script>';

?>
