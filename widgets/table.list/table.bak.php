<?php 

$arrayExample = array(

    array(
      'productSupplements' => '[
        {
            "supplement_name": "Half board 6",
            "fromCurrency": "EUR",
            "toCurrency": "AUD",
            "basePrice": 121.5,
            "markupPct":
            {
                "$numberLong": "33"
            },
            "commPct":
            {
                "$numberLong": "25"
            },
            "minguest": 1,
            "maxguest": 1,
            "discount": 0
        },
        {
            "supplement_name": "e-bike",
            "fromCurrency": "EUR",
            "toCurrency": "AUD",
            "basePrice": 132.3,
            "markupPct":
            {
                "$numberLong": "33"
            },
            "commPct": 0,
            "minguest": 1,
            "maxguest": 1,
            "discount": 0
        },
        {
            "supplement_name": "7/21-speed bike",
            "fromCurrency": "EUR",
            "toCurrency": "AUD",
            "basePrice": 62.1,
            "markupPct":
            {
                "$numberLong": "33"
            },
            "commPct": 0,
            "minguest": 1,
            "maxguest": 1,
            "discount": 0
        }]',
    'productAvailability' => '[
        {
            "dateFrom" : "15/03/2020",
            "dateTo" : "23/03/2020",
            "fromCurrency" : "EUR",
            "toCurrency" : "AUD",
            "basePrice" : 0,
            "markupPct" : 0,
            "commPct" : 0,
            "discount" : 0,
            "isMultiPrice" : 1,
            "multiPrice" : [
                {
                    "minGuest" : 2,
                    "supplement_avail_name" : "Double Price - Comfort Hotels",
                    "basePrice" : 620.1,
                    "supplement" : {
                        "type" : "VALUE",
                        "sub_supplement_avail_name" : "Comfort Hotel - Single Supplement",
                        "sub_fromCurrency" : "EUR",
                        "sub_toCurrency" : "AUD",
                        "sub_basePrice" : 248.82,
                        "sub_markupPct" : 0,
                        "sub_commPct" : 0,
                        "sub_is_mandatory" : 0,
                        "sub_discount" : 0
                    },
                    "fromCurrency" : "EUR",
                    "toCurrency" : "AUD",
                    "markupPct" : 0,
                    "commPct" : 0,
                    "discount" : 0,
                    "hasSupplement" : true
                },
                {
                    "minGuest" : 2,
                    "supplement_avail_name" : "Double Price - Superior Hotels",
                    "basePrice" : 655.2,
                    "fromCurrency" : "EUR",
                    "toCurrency" : "AUD",
                    "markupPct" : 0,
                    "commPct" : 0,
                    "discount" : 0,
                    "hasSupplement" : true
                }
            ],
            "supplemultiPrice" : [ 
                {
                    "sub_supplement_avail_name" : "Single Supplement - Classic Cabins Price",
                    "sub_basePrice" : 364.875,
                    "sub_fromCurrency" : "EUR",
                    "sub_toCurrency" : "AUD"
                }
            ]
        },
        {
            "dateFrom" : "15/03/2020",
            "dateTo" : "23/03/2020",
            "fromCurrency" : "EUR",
            "toCurrency" : "AUD",
            "basePrice" : 0,
            "markupPct" : 0,
            "commPct" : 0,
            "discount" : 0,
            "isMultiPrice" : 1,
            "multiPrice" : [
                {
                    "minGuest" : 2,
                    "supplement_avail_name" : "Double Price - Comfort Hotels",
                    "basePrice" : 620.1,
                    "supplement" : {
                        "type" : "VALUE",
                        "sub_supplement_avail_name" : "Comfort Hotel - Single Supplement",
                        "sub_fromCurrency" : "EUR",
                        "sub_toCurrency" : "AUD",
                        "sub_basePrice" : 248.82,
                        "sub_markupPct" : 0,
                        "sub_commPct" : 0,
                        "sub_is_mandatory" : 0,
                        "sub_discount" : 0
                    },
                    "fromCurrency" : "EUR",
                    "toCurrency" : "AUD",
                    "markupPct" : 0,
                    "commPct" : 0,
                    "discount" : 0,
                    "hasSupplement" : true
                },
                {
                    "minGuest" : 2,
                    "supplement_avail_name" : "Double Price - Superior Hotels",
                    "basePrice" : 655.2,
                    "fromCurrency" : "EUR",
                    "toCurrency" : "AUD",
                    "markupPct" : 0,
                    "commPct" : 0,
                    "discount" : 0,
                    "hasSupplement" : true
                }
            ],
            "supplemultiPrice" : [ 
                
            ]
        }
    ]'
  )
);


$arrayExample[0]['productAvailability']=json_decode($arrayExample[0]['productAvailability'],true);
$arrayExample[0]['productSupplements']=json_decode($arrayExample[0]['productSupplements'],true);

?>



<html>
<head>
  <title>Table Legacy</title>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<?php
//26
//68 = old add on array
//228 = no single
//69 = no single
//715
//arrayExample
$connection = new MongoClient('192.168.1.103:27017');
$db         = $connection->db_system;
$exampleID  = 715;
$markUp     = 0.33;

$arrayResults             = $db->products_inventory->find( array( "productId" => $exampleID, "is_active" => 1 ),array('_id'=>0,'productSupplements'=>1,'productAvailability'=>1) );
$arrayExample=iterator_to_array($arrayResults);
?>
<script type="text/javascript">console.log(<?=json_encode(iterator_to_array($arrayResults))?>)</script>
<?php
$tableHeaderArray         = array();
$supplementNamePriceArray = array();
$supplemultiPrice;
$SingleSuppheader         = array();
$supplementNotEmpty = 0;
$productSupplementsArray = array();
$supplementInc = 1;


//change array each
foreach  ( $arrayExample as $key => $value ) {
  # code...
  if ( !empty($value['productSupplements']) ) {
    # code...
    $productSupplementsArray = $value['productSupplements'];
  } else {
    $productSupplementsArray = array();
  }
  foreach ( $value['productAvailability'] as $key => $value ) {
    # code...
    if ( empty($value['supplemultiPrice']) ){
      $supplemultiPrice = 1;
      array_push( $supplementNamePriceArray, array( 'productSupplements' => $productSupplementsArray, 'dateFrom' => $value['dateFrom'], 'dateTo' => $value['dateTo'], 'fromCurrency' => $value['fromCurrency'],  'Prices' => array( 'multiPrice' => $value['multiPrice'] ) ) );
    } else {
      $supplemultiPrice = 0;
      array_push( $supplementNamePriceArray, array( 'productSupplements' => $productSupplementsArray, 'dateFrom' => $value['dateFrom'], 'dateTo' => $value['dateTo'], 'fromCurrency' => $value['fromCurrency'],  'Prices' => array( 'multiPrice' => $value['multiPrice'], 'supplemultiPrice' => $value['supplemultiPrice'] ) ) );
      /*array_push( $supplementNamePriceArray, array( 'dateFrom' => $value['dateFrom'], 'dateTo' => $value['dateTo'], 'fromCurrency' => $value['fromCurrency'],  'Prices' => $value['multiPrice'], 'singleSupp' => $value['supplemultiPrice'] ) );*/
    }
    foreach ( $value['multiPrice'] as $key => $value ) {
      # code...
      $tableHeaderArray[$key]['supplement_avail_name'] = $value['supplement_avail_name'];
      if ( !empty($value['supplement']) ) {
          # code...
            $supplementNotEmpty = 1;
            $tableHeaderArray[$key]['sub_supplement_avail_name'] = "Single Supplement";
      }
    }
  }
}

/*=============================================
=            Section comment block            =
=============================================*/

$i  = -1;
$ii = 0;
$countDays = 1;
$dailyDates;
foreach ( $supplementNamePriceArray as $key => $value ) {
  # code...

    $i++;
    $ii++;

    $dateFrom1 = $supplementNamePriceArray[$ii]['dateFrom'];
    $dateFrom2 = $supplementNamePriceArray[$i]['dateFrom'];

    $Date1    = str_replace('/', '-', $dateFrom1);
    $DateCon1 = date('Y-m-d', strtotime($Date1));

    $Date2    = str_replace('/', '-', $dateFrom2);
    $DateCon2 = date('Y-m-d', strtotime($Date2));

    $FirstDate  = new DateTime($DateCon1);
    $SecondDate = new DateTime($DateCon2);

    $interval  = $SecondDate->diff($FirstDate);
    $totalDays = $interval->format('%a');

    if ( $totalDays == 1 ) {

      $countDays++;
      if ( $countDays == 4 ) {
         $dailyDates = 1;
         break;
      }

    } else {
      $dailyDates = 0;
      break;
    }

    error_reporting(E_ERROR | E_PARSE);

}

if ( $dailyDates == 1 ) {

  $arrayInc = 1;
  $daysCompare = 1;
  $i  = -1;
  $ii = 0;
  foreach ( $supplementNamePriceArray as $key => $value ) {
    # code...

      $i++;
      $ii++;

      $dateFrom1 = $supplementNamePriceArray[$ii]['dateFrom'];
      $dateFrom2 = $supplementNamePriceArray[$i]['dateFrom'];

      $Date1    = str_replace('/', '-', $dateFrom1);
      $DateCon1 = date('Y-m-d', strtotime($Date1));

      $Date2    = str_replace('/', '-', $dateFrom2);
      $DateCon2 = date('Y-m-d', strtotime($Date2));

      $FirstDate  = new DateTime($DateCon1);
      $SecondDate = new DateTime($DateCon2);

      $interval  = $SecondDate->diff($FirstDate);
      $totalDays = $interval->format('%a');

      if ( $daysCompare == $totalDays ) {
        $arr2['array'.$arrayInc][$key] = $value;
        unset($supplementNamePriceArray[$key]);

      } else {

        $arr2['array'.$arrayInc][$key] = $value;
        $arrayInc++;
        unset($supplementNamePriceArray[$key]);

      }

      foreach ( $value['Prices'] as $key => $value ) {
        # code...
        foreach ($value['multiPrice'] as $key => $value) {
          # code...
          $tableHeaderArray[$key]['supplement_avail_name'] = $value['supplement_avail_name'];
        }
      }

      error_reporting(E_ERROR | E_PARSE);

  }

  $newArrDatePrice = array();
  for ( $arrayCount = 1; $arrayCount <= count($arr2); $arrayCount++ ) {
    # code...
    $arr2 = array_map('array_values', $arr2);

    $arrCurrentDate  = current($arr2['array'.$arrayCount])['dateFrom'];
    $dateFromCurrent = DateTime::createFromFormat( 'd/m/Y', $arrCurrentDate );

    $arrEndDate = end($arr2['array'.$arrayCount])['dateFrom'];
    $dateFromEnd = DateTime::createFromFormat( 'd/m/Y', $arrEndDate );

    $resultDate = $dateFromCurrent->format('M d').' - '.$dateFromEnd->format('M d');
    array_push( $newArrDatePrice, array(
      'Date'          => $resultDate, 
      'fromCurrency'  => current($arr2['array'.$arrayCount])['fromCurrency'], 
      'Prices'        => current($arr2['array'.$arrayCount])['Prices'],
    ));

  }

  foreach ( $newArrDatePrice as $newArrDatePricekey => $newArrDatePricevalue ) {
    # code...
    foreach ( $newArrDatePricevalue['Prices']['supplemultiPrice'] as $key => $value ) {
      # code...
      $SingleSuppheader[$key]['sub_supplement_avail_name'] = $value['sub_supplement_avail_name'];
    }
  }

}

/*=====  End of Section comment block  ======*/


/*====================================
=            FOR CURRENCY            =
====================================*/

$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
  $refCurr[$val['curr_from']] = (float)$val['rate_to'];
}

/*=====  End of FOR CURRENCY  ======*/

?>

<table>
  <tbody>
    <tr id="myRow">
      <?php 

        if ( $dailyDates == 1 ) {
          ?> <td><h5>Departure Dates:</h5></td> <?php
        } else {
          ?> <td><h5>Departure Dates:</h5></td><td><h5>End Dates:</h5></td> <?php
        }
      ?>
      
    <?php 

        /*====================================
        =            Table header            =
        ====================================*/

        if ( $dailyDates == 1 ) {
          # code...
          foreach ( $tableHeaderArray as $key => $value ) {
            # code...
            echo "<td><h5>".$value['supplement_avail_name'].":</h5></td>";
          }

          foreach ( $SingleSuppheader as $key => $value ) {
            # code...
            echo "<td><h5>".$value['sub_supplement_avail_name'].":</h5></td>";
            break;
          }
        } else {
          if ( $supplementNotEmpty == 1 ) {
              # code...
              foreach ( $tableHeaderArray as $key => $value ) {
                  # code...
                  $tableHeaderArray[$key]['sub_supplement_avail_name'] = "Single Supplement";
              }
              foreach ( $tableHeaderArray as $key1 => $value1 ) {
                  # code...
                  if ( preg_match( '[-]', $value1['supplement_avail_name'] ) ) {
                      # code...
                      $HeadNameString = strrpos( $value1['supplement_avail_name'], "-" );
                      $HeadnameFinal  = substr( $value1['supplement_avail_name'], $HeadNameString + 2 );

                      echo "<td><h5>".$HeadnameFinal.":</h5></td>";
                      echo "<td><h5>".$value1['sub_supplement_avail_name'].":</h5></td>";

                  } else {
                      echo "<td><h5>".$value1['supplement_avail_name'].":</h5></td>";
                      echo "<td><h5>".$value1['sub_supplement_avail_name'].":</h5></td>";
                  }
              }
          } else {

              if ( $supplemultiPrice == 0 ) {
                  # code...
                  foreach ( $supplementNamePriceArray as $supplementNamePriceArraykey => $supplementNamePriceArrayvalue ) {
                    # code...
                    foreach ( $supplementNamePriceArrayvalue['Prices']['supplemultiPrice'] as $key => $value ) {
                      # code...
                     $SingleSuppheader[$key]['sub_supplement_avail_name'] = "Single Supplement";
                    }
                  }
              }

              foreach ( $tableHeaderArray as $tableHeaderArraykey => $tableHeaderArrayvalue ) {
                  # code...
                  if ( preg_match( '[-]', $tableHeaderArrayvalue['supplement_avail_name'] ) ) {
                      # code...
                      $HeadNameString = strrpos( $tableHeaderArrayvalue['supplement_avail_name'], "-" );
                      $HeadnameFinal  = substr( $tableHeaderArrayvalue['supplement_avail_name'], $HeadNameString + 2 );

                      echo "<td><h5>".$HeadnameFinal.":</h5></td>";
                  } else {

                      echo "<td><h5>".$tableHeaderArrayvalue['supplement_avail_name'].":</h5></td>";

                  }
                  
                  foreach ( $SingleSuppheader as $key => $value ) {
                    # code...
                    echo "<td><h5>".$value['sub_supplement_avail_name'].":</h5></td>";
                    break;
                  }

              }
          }
        }

        /*=====  End of Table header  ======*/


        /*====================================
        =            TABLE PRICES            =
        ====================================*/


        if ( $dailyDates == 1 ) {

        foreach ( $newArrDatePrice as $key => $newArrDatePriceValue ) {
          # code...
          echo "<tr>";

            echo "<td>".$newArrDatePriceValue['Date']."</td>";
            foreach ( $newArrDatePriceValue['Prices']['multiPrice'] as $key => $value ) {
              # code...
              $BPAmount    = (string)$value['basePrice'];
              $convertCurr = $BPAmount * $refCurr[$value['fromCurrency']];
              $markTotal   = $markUp * $convertCurr;
              $amount      = round( $markTotal + $convertCurr );
              echo "<td>$" . $amount . ".00</td>";
            }

            if ( $supplemultiPrice == 0 ) {
              # code...
              foreach ( $newArrDatePriceValue['Prices']['supplemultiPrice'] as $key => $value ) {
                # code...
                $BPAmount    = (string)$value['sub_basePrice'];
                $convertCurr = $BPAmount * $refCurr[$value['sub_fromCurrency']];
                $markTotal   = $markUp * $convertCurr;
                $amount      = round( $markTotal + $convertCurr );
                echo "<td>$" . $amount . ".00</td>";

              }
            }

          echo "</tr>";
        }        
        

      } else {

        foreach ( $supplementNamePriceArray as $key => $DatePricevalue ) {
          # code...
          $dateFrom_date = $DatePricevalue['dateFrom'];
          $dateFrom = DateTime::createFromFormat( 'd/m/Y', $dateFrom_date );

          $dateTo_date = $DatePricevalue['dateTo'];
          $dateTo = DateTime::createFromFormat( 'd/m/Y', $dateTo_date );
          echo "<tr>";

            echo "<td>" . $dateFrom->format('M d Y') . "</td>";
            echo "<td>" . $dateTo->format('M d Y') . "</td>";

            /*============================================
            =            TABLE FOR BASE PRICE            =
            ============================================*/


            if ( $supplemultiPrice == 1 ) {
                # code...
                foreach ( $DatePricevalue['Prices']['multiPrice'] as $key2 => $value2 ) {
                    # code...
                    $BPAmount2    = (string)$value2['basePrice'];
                    $convertCurr2 = $BPAmount2 * $refCurr[$value2['fromCurrency']];
                    $markTotal2   = $markUp * $convertCurr2;
                    $amount2      = round( $markTotal2 + $convertCurr2 );
                    echo "<td>$" . $amount2 . ".00</td>";
                    if ( !empty($value2['supplement']) ) {
                      # code...
                      $BPAmount3   = (string)$value2['supplement']['sub_basePrice'];
                      $convertCurr3 = $BPAmount3 * $refCurr[$value2['fromCurrency']];
                      $markTotal3   = $markUp * $convertCurr3;
                      $amount3      = round( $markTotal3 + $convertCurr3 );
                      echo "<td>$" . $amount3 . ".00</td>";
                    }
                }
                if ( empty($DatePricevalue['Prices']['supplemultiPrice']) ) {
                  # code...
                  foreach ( $DatePricevalue['Prices']['multiPrice'] as $key => $value ) {
                    # code...
                    if ( !empty($value['supplement']) ) {
                      # code...
                      $supplementInc++;
                    }
                  }
                  if ( $supplementInc > 1 ) {
                    # code...
                    echo "<td>-</td>";
                  }
                  
                } else {
                  foreach ( $DatePricevalue['Prices']['supplemultiPrice'] as $key4 => $value4 ) {
                    # code...
                    $BPAmount5    = (string)$value4['sub_basePrice'];
                    $convertCurr5 = $BPAmount5 * $refCurr[$value4['sub_fromCurrency']];
                    $markTotal5   = $markUp * $convertCurr5;
                    $amount5      = round( $markTotal5 + $convertCurr5 );
                    echo "<td>$" . $amount5 . ".00</td>";
                  }
                }
            } else {

              foreach ( $DatePricevalue['Prices']['multiPrice'] as $key => $value ) {
                # code...
                $BPAmount    = (string)$value['basePrice'];
                $convertCurr = $BPAmount * $refCurr[$value['fromCurrency']];
                $markTotal   = $markUp * $convertCurr;
                $amount      = round( $markTotal + $convertCurr );
                echo "<td>$" . $amount . ".00</td>";
                if ( !empty($value['supplement']) ) {
                  # code...
                  $BPAmount1    = (string)$value['supplement']['sub_basePrice'];
                  $convertCurr1 = $BPAmount1 * $refCurr[$value['fromCurrency']];
                  $markTotal1   = $markUp * $convertCurr1;
                  $amount1      = round( $markTotal1 + $convertCurr1 );
                  echo "<td>$" . $amount1 . ".00</td>";
                }
              }
              if ( empty($DatePricevalue['Prices']['supplemultiPrice']) ) {
                # code...
                  echo "<td>-</td>";
              } else {
                foreach ( $DatePricevalue['Prices']['supplemultiPrice'] as $key3 => $value3 ) {
                  # code...
                  $BPAmount4    = (string)$value3['sub_basePrice'];
                  $convertCurr4 = $BPAmount4 * $refCurr[$value3['sub_fromCurrency']];
                  $markTotal4   = $markUp * $convertCurr4;
                  $amount4      = round( $markTotal4 + $convertCurr4 );
                  echo "<td>$" . $amount4 . ".00</td>";
                }
              }
            }
          echo "</tr>";
            
            
            /*=====  End of TABLE FOR BASE PRICE  ======*/        
        }

      }
        
        /*=====  End of TABLE PRICES  ======*/
  echo "</tbody>";
echo "</table>";


foreach ( $productSupplementsArray as $key => $value ) {
  # code...
  if ( $value['maxguest'] == 1 ) {
    # code...
    $BPAmount    = (string)$value['basePrice'];
    $convertCurr = $BPAmount * $refCurr[$value['fromCurrency']];
    $markTotal   = $markUp * $convertCurr;
    $amount      = round( $markTotal + $convertCurr );
    echo $value['supplement_name'].": $".$amount.".00 </br>";

  } else {

    $convertCurr = $BPAmount * $refCurr[$value['fromCurrency']];
    echo $value['supplement_name'].": $".$convertCurr.".00 </br>";

  }
}
