<?php 
$connection = new MongoClient();
$db = $connection->db_system;

$agencyData = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'],"is_active_b2c"=>1,"tours"=>1));

if(empty($agencyData)){
	die();
}

//$agencyData


$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
	$refCurr[$val['curr_from']] = (float)$val['rate_to'];
}

$dataArray = $db->products_inventory->findOne(array("is_active"=>1,"productId"=>(int)$_GET['productId']),array("_id"=>0,"productAvailability"=>1,"productId"=>1,"productName"=>1));

$resArray = array();

$prodName = utf8_encode(htmlspecialchars($dataArray['productName'], ENT_QUOTES));
$productName = str_replace('"', '', $prodName);
$productName = str_replace(',', ' ', $prodName);

$resArray['productName'] = $productName;
$amountContainer = array();
if(!empty($dataArray['productAvailability'])){
	foreach($dataArray['productAvailability'] as $key2=>$val2){
		if($val2['isMultiPrice'] == 1){
			foreach($val2['multiPrice'] as $key3=>$val3){
				$amount = (float)$val3['basePrice']*$refCurr[$val3['fromCurrency']];
				array_push($amountContainer, $amount);
			}

		}else{
			$amount = (float)$val2['basePrice']*$refCurr[$val2['fromCurrency']];
			array_push($amountContainer, $amount);
		}
	}

	$markUp = $agencyData['toursConfig']['markup_pct'];
	$newAmountTemp = min($amountContainer);
	$newMarkUp = $markUp/100;
	$newAmount = $newAmountTemp + ($newAmountTemp*$newMarkUp);
	unset($resArray['productAvailability']);
	$resArray['price'] = $newAmount;
	$resArray['priceTxt'] = number_format($newAmount, 2, '.', ',');
	$resArray['priceTxtAUD'] = "$ ".number_format($newAmount, 2, '.', ',');
}else{
	unset($resArray['productAvailability']);
	$resArray['price'] = 0;
	$resArray['priceTxt'] = number_format(0, 2, '.', ',');
	$resArray['priceTxtAUD'] = "$ ".number_format(0, 2, '.', ',');
}

$type = $_GET['type'];
switch ($type) {
	case "name":{
			echo "retrivedTours('".$resArray['productName']."',".$_GET['elem'].");";
	   		exit;
	  	break; 
	}
	

	case "priceTxt":{

			echo "retrivedTours('".$resArray['priceTxt']."',".$_GET['elem'].");";
	   		
	   		exit;
	  	break; 
	}
	case "price":{

			echo "retrivedTours('".$resArray['price']."',".$_GET['elem'].");";
	   		
	   		exit;
	  	break; 
	}
	case "priceTxtAUD":{

			echo "retrivedTours('".$resArray['priceTxtAUD']."',".$_GET['elem'].");";
	   		
	   		exit;
	  	break; 
	}
	
	/* ENDLOGIN*/
}



?>
