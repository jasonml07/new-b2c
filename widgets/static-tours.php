<?php 
ini_set('memory_limit', '-1');
error_reporting(E_ALL ^ E_WARNING);
$connection = new MongoClient();
$db = $connection->db_system;

$agencyData = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'],"is_active_b2c"=>1,"tours"=>1));

if(empty($agencyData)){
	die();
}

//$agencyData

$today = strtotime('now +3 days');

$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
	$refCurr[$val['curr_from']] = (float)$val['rate_to'];
}

$resCol = $db->products_inventory->find(array("is_active"=>1),array("_id"=>0,"productAvailability"=>1,"productId"=>1,'productName'=>1,'productSumarryDescription'=>1));
$dataArray = iterator_to_array($resCol);

/*echo '<pre>';
print_r($dataArray);
echo '</pre>';
exit()*/;

$datasResult = array();
foreach($dataArray as $key=>$val){
	
	#$dataArray[$key]['productName'] = htmlentities($val['productName'], ENT_QUOTES);
	#$dataArray[$key]['productName'] =  utf8_encode(htmlspecialchars($val['productName'], ENT_QUOTES));
	//$prodName = utf8_encode(htmlspecialchars($val['productName'], ENT_QUOTES));
	//$productName = str_replace('"', '', $prodName);
	//$productName = str_replace(',', ' ', $prodName);

	//$dataArray[$key]['productName'] = $productName;

	
	if(!empty($val['productAvailability'])){
		$hasDouble = false;
		$amountContainer = array(); 
		$amountContainerETC = array(); 

		foreach($val['productAvailability'] as $key2=>$val2){
			try{
				$tempStr = '20';
				$datearr = explode('/', $val2['dateFrom']);
				if (!isset($datearr[2])) {
					continue;
				}
				$yearDate = $datearr[2];
				if(strlen($datearr[2])<=2){
					$yearDate = $tempStr.$datearr[2];
				}
				$dateSrc = $yearDate."-".$datearr[1]."-".$datearr[0];

			

				$dateTime = strtotime(date_format(date_create($dateSrc), 'Y-m-d'));
				if($today<$dateTime){
					if($val2['isMultiPrice'] == 1){
						foreach($val2['multiPrice'] as $key3=>$val3){
							if (!isset($val3['minGuest'])) {
								continue;
							}
							if($val3['minGuest'] == 2){
								$hasDouble = true;
								$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];
								array_push($amountContainer, $amount);
							}else{
								$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];
								/*if ($val['productId']==360) {
									$dataArray[$key]['amount'][]=array(
										'base'=>$val3['basePrice'],
										'rate'=>$refCurr[$val3['fromCurrency']],
										'fromCurrency'=>$val3['fromCurrency'],
										'refCurr'=>$refCurr
									);
								}*/
								array_push($amountContainerETC, $amount);
							}
							
						}

					}else{
						$amount = (float)$val2['basePrice']*$refCurr[trim($val2['fromCurrency'])];
						array_push($amountContainer, $amount);
					}
				}
			}catch (Exception $e) {
				$dataArray[$key]['error'][]=$e->getMessage();
			}
			
			
		}

		$markUp = $agencyData['toursConfig']['markup_pct'];
		if($hasDouble){
			$newAmountTemp = min($amountContainer);
		}else{
			$newAmountTemp = min($amountContainerETC);
		}
		$ids=array(947,948);
		if (in_array($val['productId'], $ids)) {
			$dataArray[$key]['amountContainer']=$amountContainer;
			$dataArray[$key]['amountContainerETC']=$amountContainerETC;
		}

		$newMarkUp = $markUp/100;
		$newAmount = ($newAmountTemp*$newMarkUp) + $newAmountTemp;
		unset($dataArray[$key]['productAvailability']);
		//$dataArray[$key]['price'] = $newAmount;
		//$dataArray[$key]['priceTxt'] = number_format($newAmount, 2, '.', ',');
		$dataArray[$key]['priceTxtAUD'] = "$ ".number_format($newAmount, 0, '.', ',');
		$dataArray[$key]['productName'] = htmlspecialchars($val['productName'],ENT_QUOTES);
		$dataArray[$key]['productSumarryDescription'] = htmlspecialchars(preg_replace('/\n|<b>|<h5(.*)>|<p(.*)>|<font(.*)>|<\/font>|<br \/>|<div(.*)>|<\/div>|<span(.*)>|<\/span>/',"",$val['productSumarryDescription']),ENT_QUOTES);
	}else{
		// $dataArray[$key]['empty']=true;
		unset($dataArray[$key]['productAvailability']);
		//$dataArray[$key]['price'] = 0;
		//$dataArray[$key]['priceTxt'] = number_format(0, 2, '.', ',');
		$dataArray[$key]['priceTxtAUD'] = "$ ".number_format(0, 0, '.', ',');
		$dataArray[$key]['productName'] = htmlspecialchars($val['productName'],ENT_QUOTES);
		$dataArray[$key]['productSumarryDescription'] = htmlspecialchars(preg_replace('/\n|<b>|<h5(.*)>|<p(.*)>|<font(.*)>|<\/font>|<br \/>|<div(.*)>|<\/div>|<span(.*)>|<\/span>/',"",$val['productSumarryDescription']),ENT_QUOTES);
	}
	$datasResult[$val['productId']] = $dataArray[$key];	
}

//print_r($dataArray);
echo "var retrievedJSON = '".json_encode($datasResult)."';";
?>




function goToTours(link){
	window.top.location = link;  

}

function goToTours2(link){
	var x = screen.width/2 - 700/2;
		var y = screen.height/2 - 450/2;
		signinWin = window.open(link, "_blank", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + x + ",top=" + y);
		signinWin.focus();

}


document.addEventListener('DOMContentLoaded', function() {
var AGENCY_CODE='<?=$_GET["agencyCode"]?>'
var All = document.getElementsByTagName('*');
var toursArray = JSON.parse(retrievedJSON);
var attrButeStr = "tz-plugin";
var attrButeStr2 = "tz-plugin2";
var attrButeStr3 = "tz-plugin3";
var agencyCode = "<?=$_GET['agencyCode'];?>";
var url = "http://<?=$_SERVER['HTTP_HOST'];?>/externaltours?agencyCode=<?=$_GET['agencyCode'];?>&id=";
var url2 = "http://<?=$_SERVER['HTTP_HOST'];?>/externaltours/signIn?product=";

/**
 * [addClickEvent description]
 * @param {[type]} el   [description]
 * @param {[type]} tour [description]
 */
function addClickEvent(el, tour){
	el.addEventListener('click',function(){
		// console.log('Clicked')w
		dataLayer.push({
			'event': 'addToCart',
			'ecommerce': {
				'currencyCode': 'AUD',
				'add': {
					'products': [{
						'name':tour.productName,
						'id':tour.productId,
						'price':tour.priceTxtAUD.replace(/\$\s+/g,'').replace(',',''),
						'brand':'Europe Holidays',
						'category': 'Tours',
						'variant': tour.productSumarryDescription,
						'quantity': 1
					}]
				}
			}
		})
		window.top.location = url+el.getAttribute(attrButeStr); 
	})	
}

for (var i = 0; i < All.length; i++)       {
	if (All[i].getAttribute(attrButeStr)) {
		
		var tag = All[i].tagName;
		switch (tag) {
				case 'A':
						All[i].href=url+All[i].getAttribute(attrButeStr);
						
						break;
				case 'BUTTON':
					if(toursArray[All[i].getAttribute(attrButeStr)]&&toursArray[All[i].getAttribute(attrButeStr)].priceTxtAUD !='$ 0'){
						All[i].innerHTML='Book Online';
						var tour=toursArray[All[i].getAttribute(attrButeStr)],
							hasDataLayer=false,
							eventLayer=null
						// All[i].setAttribute("onclick","javascript:goToTours('"+url+All[i].getAttribute(attrButeStr)+"');");
						// Adding datalayer here
						// console.log('Tour:: ', toursArray[All[i].getAttribute(attrButeStr)])
						for (var x = 0; x < dataLayer.length; x++) {
							dataLayer[x].event='PDP View'
							if (dataLayer[x].event=='PDP View'&&dataLayer[x].ecommerce) {
								eventLayer=dataLayer[x]
								break;
							}
						}
						// console.log('eventLayer', eventLayer)
						if (!eventLayer) {
							addClickEvent(All[i],tour)
							dataLayer.push({
								'event': 'PDP View',
								'ecommerce': {
									'detail': {
										'actionField': {'list': 'Tours'},    // Variable to change to tours, island hopping etc.
										'products': [{
											'name': tour.productName,             // VARIABLE - Name or ID is required.
											'id': tour.productId,                                            // VARIABLE - ID of the product
											'price': tour.priceTxtAUD.replace(/\$\s+/g,'').replace(',',''),                                            // VARIABLE - Total Price
											'brand': 'Europe Holidays',
											'category': 'Tours',                                       // VARIABLE - Change to Tours etc.
											'variant': tour.productSumarryDescription                      // VARIABLE - Duration
										 }]
									 }
								 }
							});
						} else {
							var products=eventLayer.ecommerce.detail.products,
								alreadyExist=false
							// console.log('products', products)

							for (var x = 0; x < products.length; x++) {
								if(products[x].id==tour.productId){
									alreadyExist=true
									break
								}
							}
							if (!alreadyExist) {
								addClickEvent(All[i],tour)
								eventLayer.ecommerce.detail.products.push({
									'name': tour.productName,             // VARIABLE - Name or ID is required.
									'id': tour.productId,                                            // VARIABLE - ID of the product
									'price': tour.priceTxtAUD.replace(/\$\s+/g,'').replace(',',''),                                            // VARIABLE - Total Price
									'brand': 'Europe Holidays',
									'category': 'Tours',                                       // VARIABLE - Change to Tours etc.
									'variant': tour.productSumarryDescription                      // VARIABLE - Duration
								})
							}
						}

					}
						
					 
						break;
				
				default:
					var tagType = All[i].getAttribute('tz-type');
				switch (tagType) {
						case 'name':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr)].productName;
								break;
						
						case 'priceTxt':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr)].price;
								 break;
						case 'price':
							All[i].innerHTML=(toursArray[All[i].getAttribute(attrButeStr)]||{priceTxtAUD:'Contact Us'}).priceTxtAUD;
							if (All[i].innerHTML=='$ 0') {
								All[i].innerHTML='Contact Us'
							}
								break;
						case 'div':
							if(toursArray[All[i].getAttribute(attrButeStr)].priceTxtAUD =='$ 0'){
									if (AGENCY_CODE=='EuropeTravelDeals') {
										All[i].parentElement.innerHTML='<small style="line-height: 4;">Limited Spots available! Call to save your spot!</small>'
									} else {
										All[i].innerHTML='Contact Us';
									}
									}else{
										All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr)].priceTxtAUD;
								}
							break;
						default:

							try {

								if(toursArray[All[i].getAttribute(attrButeStr)].priceTxtAUD =='$ 0'){
									if (AGENCY_CODE=='EuropeTravelDeals') {
										All[i].parentElement.innerHTML='<small style="line-height: 4;">Limited Spots available! Call to save your spot!</small>'
									} else {
										All[i].innerHTML='Contact Us';
									}
									}else{
										All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr)].priceTxtAUD;
								}

						}
						catch(err) {
								console.log(All[i].getAttribute(attrButeStr));
						} 
							

							 
								
				}
					
		}


	}

	if (All[i].getAttribute(attrButeStr2)) {
		
		var tag = All[i].tagName;
		switch (tag) {
				case 'A':
						All[i].href='#';
						All[i].setAttribute("onclick","javascript:goToTours2('"+url2+All[i].getAttribute(attrButeStr2)+"');");
						break;
				case 'BUTTON':
					if(toursArray[All[i].getAttribute(attrButeStr2)].priceTxtAUD !='$ 0'){
						All[i].innerHTML='Book Online';
					 
							All[i].setAttribute("onclick","javascript:goToTours2('"+url2+All[i].getAttribute(attrButeStr2)+"');");
					}
						
					 
						break;
				
				default:
					var tagType = All[i].getAttribute('tz-type');
				switch (tagType) {
						case 'name':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr2)].productName;
								break;
						
						case 'priceTxt':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr2)].price;
								 break;
						case 'price':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr2)].priceTxt;
								break;
						
						default:
							if(toursArray[All[i].getAttribute(attrButeStr2)].priceTxtAUD =='$ 0'){
								All[i].innerHTML='Contact Us';
								}else{
									All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr2)].priceTxtAUD;
							}
								
								
				}
					
		}


	}

	if (All[i].getAttribute(attrButeStr3)) {
		
		var tag = All[i].tagName;
		switch (tag) {
				case 'A':
						All[i].href='#';
						All[i].setAttribute("onclick","javascript:goToTours2('"+url2+All[i].getAttribute(attrButeStr3)+"');");
						break;
				case 'BUTTON':
					if(toursArray[All[i].getAttribute(attrButeStr3)].priceTxtAUD !='$ 0'){
						All[i].innerHTML='Book Online';
					 
						All[i].setAttribute("onclick","javascript:goToTours2('"+url2+All[i].getAttribute(attrButeStr3)+"');");
						
					}
					 
						break;
				
				default:
					var tagType = All[i].getAttribute('tz-type');
				switch (tagType) {
						case 'name':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr3)].productName;
								break;
						
						case 'priceTxt':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr3)].price;
								 break;
						case 'price':
							All[i].innerHTML=toursArray[All[i].getAttribute(attrButeStr3)].priceTxt;
								break;

						case 'blank':

							if(toursArray[All[i].getAttribute(attrButeStr3)].priceTxtAUD && toursArray[All[i].getAttribute(attrButeStr3)].priceTxtAUD =='$ 0'){
								All[i].innerHTML='';
							}
							
								break;
						default:
							if(toursArray[All[i].getAttribute(attrButeStr3)]&&toursArray[All[i].getAttribute(attrButeStr3)].priceTxtAUD =='$ 0'){
								All[i].innerHTML='';
								}else{
									All[i].innerHTML=(toursArray[All[i].getAttribute(attrButeStr3)]||{priceTxtAUD:''}).priceTxtAUD;
							}
								
								
				}
					
		}


	}
	
}

});
