<?php

// agency
ini_set('memory_limit', '-1');
error_reporting(E_ALL ^ E_WARNING);

$AGENCY=$_GET['agency'];
$PRODUCT_IDS=$_GET['products'];

$CONNECTION = new MongoClient();
$DB = $CONNECTION->db_system;

$AGENT_DATA = $DB->agency_b2b_b2c->findOne(array("agency_code"=>$AGENCY,"is_active_b2c"=>1,"tours"=>1));

if (empty($AGENT_DATA)) {
	die();
}

$TODAY=strtotime('now +3 days');

$currencies = $DB->currency_ex->find(array(),array("_id"=>0));
$currencies = iterator_to_array($currencies);

$CURRENCY_EXCHANGE = array();
foreach($currencies as $key=>$val){
	$CURRENCY_EXCHANGE[$val['curr_from']] = (float)$val['rate_to'];
}

$PRODUCTS = $DB->products_inventory->find(array("is_active"=>1),array("_id"=>0,"productId"=>1));
$PRODUCTS = iterator_to_array($PRODUCTS);

echo "<pre>";
print_r($PRODUCTS);
echo '</pre>';