<?php 
ini_set('memory_limit', '-1');
error_reporting(E_ALL ^ E_WARNING);
function removeSpecialChar($string) {

	//$decode = str_replace("\n", "", str_replace("\n", "", $string));
	$decode = str_replace("\n", "", str_replace("\n", "",htmlspecialchars($string,ENT_QUOTES)));

		
    return  str_replace("	","&nbsp;&nbsp;&nbsp;&nbsp;",str_replace("'","\'", $decode));
}



function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}

$connection = new MongoClient();
$db = $connection->db_system;

$agencyData = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'],"is_active_b2c"=>1,"tours"=>1));

if(empty($agencyData)){
	die();
}

$where = array("is_active"=>1,"is_live"=>1,"productId"=>(int)$_GET['id']);

$selectionArray = array("_id"=>0);
foreach (explode('|', $_GET['display']) as $key => $value) {
		$selectionArray[$value]=1;
}


$resCol = $db->products_inventory->findOne($where,$selectionArray);

$datasResult = array();
foreach (explode('|', $_GET['display']) as $key => $value) {
		$datasResult[$value]=removeSpecialChar($resCol[$value]);
}

echo $_GET['el'].'(\''.json_encode($datasResult,JSON_FORCE_OBJECT).'\');';
//print_r($datasResult);

die();


//print_r($dataArray)
//echo json_encode($datasResult);
?>

