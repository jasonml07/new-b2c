var today = new Date();
var currentMonth = today.getMonth();
var currentYear = today.getFullYear();
var selectYear = document.getElementById("year");
var selectMonth = document.getElementById("month");

var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

var monthAndYear = document.getElementById("monthAndYear");
showCalendar(currentMonth, currentYear);

function next() {
    currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
    currentMonth = (currentMonth + 1) % 12;
    showCalendar(currentMonth, currentYear);

    var values = arrayFromPhp.productAvailability.map(function(elt) {
        return elt.dateFrom; 
    });

    var newGetDateArr1 = [];
    values.forEach(function(element) {
      var datearray    = element.split("/");
      var newdate      = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
      var newDate      = new Date(newdate);
      var arr          = {};
      arr.newGetDate   = newDate.getDate();
      arr.newGetMonth  = newDate.getMonth() + 1;
      arr.newGetYear   = newDate.getFullYear();
      if ( currentYear <= arr.newGetYear ) {
        newGetDateArr1.push(arr);
      }
    });
    
    var monthValues = newGetDateArr1.map(function(elt) {
        return elt.newGetMonth;
    });

    var maxMonth          = Math.max.apply(null, monthValues);
    var currentMonthValue = currentMonth + 1;

    var yearValues = newGetDateArr1.map(function(elt) {
        return elt.newGetYear;
    });

    var maxyear = Math.max.apply(null, yearValues);

    if ( maxMonth > currentMonthValue && maxyear >= currentYear ) {
        document.getElementById("next").disabled = false;
    } else {
        document.getElementById("next").disabled = true;
    }
}

function previous() {

    currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
    currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
    showCalendar(currentMonth, currentYear);

    var values = arrayFromPhp.productAvailability.map(function(elt) {
        return elt.dateFrom; 
    });

    var newGetDateArr1 = [];
    values.forEach(function(element) {
      var datearray    = element.split("/");
      var newdate      = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
      var newDate      = new Date(newdate);
      var arr          = {};
      arr.newGetDate   = newDate.getDate();
      arr.newGetMonth  = newDate.getMonth() + 1;
      arr.newGetYear   = newDate.getFullYear();
      if ( currentYear <= arr.newGetYear ) {
        newGetDateArr1.push(arr);
      }
    });
    
    var monthValues = newGetDateArr1.map(function(elt) {
        return elt.newGetMonth;
    });

    var minMonth          = Math.min.apply(null, monthValues);
    var currentMonthValue = currentMonth + 1;

    if ( minMonth <= currentMonthValue ) {
        document.getElementById("next").disabled = false;
    } else {
        document.getElementById("next").disabled = true;
    }

}

function showCalendar(month, year) {
    //Array
    var newGetDateArr = [];
    var markUp = 0.33;

    var firstDay = (new Date(year, month)).getDay();
    var daysInMonth = 32 - new Date(year, month, 32).getDate();

    var tbl = document.getElementById("calendar-body");

    tbl.innerHTML = "";

    monthAndYear.innerHTML = months[month] + " " + year;
    selectYear.value = year;
    selectMonth.value = month;

    var date  = 1;
    for (var i = 0; i < 6; i++) {
        var row = document.createElement("tr");
        for (var j = 0; j < 7; j++) {
            if (i === 0 && j < firstDay) {
                var cell = document.createElement("td");
                var cellText = document.createTextNode("");
                cell.appendChild(cellText);
                row.appendChild(cell);
            }
            else if (date > daysInMonth) {
                break;
            }

            else {

                arrayFromPhp.productAvailability.forEach(function(element) {
                  var datearray    = element.dateFrom.split("/");
                  var newdate      = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                  var newDate      = new Date(newdate);
                  var arr          = {};
                  arr.newGetDate   = newDate.getDate();
                  arr.newGetMonth  = newDate.getMonth() + 1;
                  arr.newGetYear   = newDate.getFullYear();
                  arr.dateFrom     = element.dateFrom;
                  arr.dateTo       = element.dateTo;
                  arr.fromCurrency = element.fromCurrency;
                  arr.multiPrice   = element.multiPrice;
                  newGetDateArr.push(arr);
                });

                var obj  = newGetDateArr.find(function(elem){
                    return elem.newGetDate == date && elem.newGetMonth == month + 1 && elem.newGetYear == year
                });
                if ( obj ) {
                    var values = obj.multiPrice.map(function(elt) { 
                        return elt.basePrice; 
                    });
                    var min         = Math.min.apply(null, values);
                    var BPAmount    = min;
                    var convertCurr = BPAmount * refCurrArr[obj.fromCurrency];
                    var markTotal   = markUp * convertCurr;
                    var amount      = Math.round( markTotal + convertCurr );
                    var cell        = document.createElement("td");
                    var cell1       = document.createElement("p");
                    var cell2       = document.createElement("p");
                    var cell3       = document.createElement('a');
                    var cellText    = document.createTextNode(date);
                    var cellText1   = document.createTextNode("$" + amount + ".00");

                    cell3.target='_top'
                    var datearray    = obj.dateFrom.split("/");
                    var newdate      = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
                    var newDate      = new Date(newdate);
                    var newDateGetMonth = newDate.getMonth() + 1;
                    if ( 10 > newDate.getMonth() ) {
                        newDateGetMonth = '0' + newDateGetMonth;
                    } else {
                        newDateGetMonth = newDateGetMonth;
                    }

                    if ( 10 > newDate.getDate() ) {
                        var newDateGetDate = '0' + newDate.getDate();
                    } else {
                        var newDateGetDate = newDate.getDate();
                    }
                    var postDate = newDateGetMonth + '-' + newDateGetDate + '-' + newDate.getFullYear();
                    if (_REF) {
                      cell3.href      = 'http://192.168.1.103:4444/externaltours?agencyCode=EET&id=' + currentID + '&date=' + postDate;
                    }
                    cell.className  = "class-date";
                    cell1.className = "class-date-day";
                    cell2.className = "class-date-day-price";

                    cell.appendChild(cell3);
                    cell3.appendChild(cell1);
                    cell3.appendChild(cell2);
                    cell1.appendChild(cellText);
                    cell2.appendChild(cellText1);
                    row.appendChild(cell);
                    date++
                } else {
                    var cell     = document.createElement("td");
                    var cell1    = document.createElement("p");
                    var cellText = document.createTextNode(date);

                    cell1.className = "class-date-day1";
                    cell.appendChild(cell1);
                    cell1.appendChild(cellText);
                    row.appendChild(cell);
                    date++;
                }

            }
        }
        tbl.appendChild(row);
    }

    var values1 = arrayFromPhp.productAvailability.map(function(elt) {
        return elt.dateFrom; 
    });

    var newGetDateArr2 = [];
    values1.forEach(function(element) {
      var datearray    = element.split("/");
      var newdate      = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
      var newDate      = new Date(newdate);
      var arr          = {};
      arr.newGetDate   = newDate.getDate();
      arr.newGetMonth  = newDate.getMonth() + 1;
      arr.newGetYear   = newDate.getFullYear();
      if ( currentYear <= arr.newGetYear ) {
        newGetDateArr2.push(arr);
      }
    });

    if ( newGetDateArr2.length == 0 ) {
        $('#w3-red').show();
        document.getElementById("next").disabled = true
        document.getElementById("previous").disabled = true;
    } else {
        $('#w3-red').hide();
        var yearValues1 = newGetDateArr2.map(function(elt) {
            return elt.newGetYear;
        });

        var minyear1          = Math.min.apply(null, yearValues1);
        var currentYearValue1 = year;

        var monthValues1 = newGetDateArr2.map(function(elt) {
            return elt.newGetMonth;
        });

        var minmonth1          = Math.min.apply(null, monthValues1);
        var currentMonthValue1 = currentMonth + 1;

        if ( currentYearValue1 >= minyear1 && currentMonthValue1 >= minmonth1 ) {
            document.getElementById("previous").disabled = false;
        } else {
            next();
            document.getElementById("previous").disabled = true;
        }
    }
    
}