<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Calendar</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</head>
<body>
<?php include 'array.php';?>
<div class="container col-12 px-0">
    <div class="card">
        <h3 class="card-header" id="monthAndYear"></h3>
        <table class="table table-bordered table-responsive-sm" id="calendar">
            <thead>
            <tr>
                <th>Sun</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
            </tr>
            </thead>

            <tbody id="calendar-body">

            </tbody>
        </table>

        <div class="form-inline">
            <button class="btn btn-outline-primary col-sm-6" id="previous" onclick="previous()">Previous</button>
            <button class="btn btn-outline-primary col-sm-6" id="next" onclick="next()">Next</button>
        </div>
        <br/>
        <form class="form-inline">
            <span name="month" id="month"></span>
            <span name="year" id="year"></span>
        </form>
    </div>
</div>

<div class="w3-red" id="w3-red">
  <p>No prices to display.</p>
</div>

<script type="text/javascript">
    var _REF=false;
    <?php if (isset($_GET['token'])) :?>
        _REF=true;
    <?php endif; ?>
</script>
<script src="js/calendar.js"></script>

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>


</body>
</html>