<?php 
ini_set('memory_limit', '-1');
error_reporting(E_ALL ^ E_WARNING);
function removeSpecialChar($string) {

	//$decode = str_replace("\n", "", str_replace("\n", "", $string));
	$decode = str_replace("\n", "", str_replace("\n", "",htmlspecialchars($string,ENT_QUOTES)));

		
    return  str_replace("	","&nbsp;&nbsp;&nbsp;&nbsp;",str_replace("'","\'", $decode));
}



$connection = new MongoClient();
$db = $connection->db_system;

$agencyData = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'],"is_active_b2c"=>1,"tours"=>1));

if(empty($agencyData)){
	die();
}

$int_arr=array();
if(isset($_GET['ids'])){
	foreach (explode(',', $_GET['ids']) as $key => $value) {
		array_push($int_arr, (int)$value);
	}
}

//$agencyData

$today = strtotime('now +3 days');

$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
	$refCurr[$val['curr_from']] = (float)$val['rate_to'];
}

$countryArr = array();
$countryArr2 = array();

if(isset($_GET['country']) && $_GET['country'] !=''){

	foreach (explode(',', $_GET['country']) as $key => $value) {
		array_push($countryArr, new MongoRegex("/".ltrim($value)."/i"));
		array_push($countryArr2, new MongoRegex("/".$value."/i"));
	}


	if(isset($_GET['filter']) && $_GET['filter'] !=''){
		$where = array("is_active"=>1,"is_live"=>1,"productTag"=>new MongoRegex("/".$_GET['filter']."/i"),'$or'=>array(
			array("productFromCountry"=>array('$in'=>$countryArr)),
			array("countriesFilter"=>array('$in'=>$countryArr2))
			)
		);

	}else{
		$where = array("is_active"=>1,"is_live"=>1,'$or'=>array(
			array("productFromCountry"=>array('$in'=>$countryArr)),
			array("countriesFilter"=>array('$in'=>$countryArr2))
			)
		);
	}
	
	
	
}elseif(isset($_GET['destination']) && $_GET['destination'] !=''){

	if(isset($_GET['filter']) && $_GET['filter'] !=''){
		$where = array("is_active"=>1,"is_live"=>1,
			"productTag"=>new MongoRegex("/".$_GET['filter']."/i"),"productFromAddress"=>new MongoRegex("/".$_GET['destination']."/i")
		);

	}else{
		$where = array("is_active"=>1,"is_live"=>1,
			"productFromAddress"=>new MongoRegex("/".$_GET['destination']."/i")
		);
	}

	

}
elseif(isset($_GET['keyword']) && $_GET['keyword'] !=''){
	$where = array("is_active"=>1,"is_live"=>1,"productKeywords"=>new MongoRegex("/".$_GET['keyword']."/i")
	);
	if(isset($_GET['filter']) && $_GET['filter'] != ''){
		$where = array("is_active"=>1,"is_live"=>1,"productKeywords"=>new MongoRegex("/".$_GET['keyword']."/i"),
			"productTag"=>new MongoRegex("/".$_GET['filter']."/i")
		);
		
	}else{
		$where = array("is_active"=>1,"is_live"=>1,"productKeywords"=>new MongoRegex("/".$_GET['keyword']."/i")
	);
	}
}
elseif(isset($_GET['filter']) && $_GET['filter'] !=''){
	$where = array("is_active"=>1,"is_live"=>1,'$or'=>array(
		array("productTag"=>new MongoRegex("/".$_GET['filter']."/i")),
		array("countriesFilter"=>new MongoRegex("/".$_GET['filter']."/i"))
		));

}else{
	$where = array("is_active"=>1,"is_live"=>1,"productId"=>array('$in'=>$int_arr));
}


//print_r($where);
//"productId"=>array('$in'=>$int_arr)


//$whereData['$or'] = array();

//print_r($where);
//die();

$selectionArray = array("_id"=>0,"productAvailability"=>1,"productId"=>1,'productName'=>1,'productSumarryDescription'=>1,'productDetailedDescription'=>1,'productFromAddress'=>1,'productInclusionDescription'=>1,'productConditionsDescription'=>1,'productExclusionDescription'=>1,'productFromCountry'=>1,'productImages'=>1,'productCountries'=>1,'productTag'=>1,'productKeywords'=>1,"productAltPrice"=>1);



$resCol = $db->products_inventory->find($where,$selectionArray);
$dataArray = iterator_to_array($resCol);


$datasResult = array();
foreach($dataArray as $key=>$val){
	
	#$dataArray[$key]['productName'] = htmlentities($val['productName'], ENT_QUOTES));
	#$dataArray[$key]['productName'] =  utf8_encode($val['productName'], ENT_QUOTES));
	//$prodName = utf8_encode($val['productName'], ENT_QUOTES));
	//$productName = str_replace('"', '', $prodName);
	//$productName = str_replace(',', ' ', $prodName);

	//$dataArray[$key]['productName'] = $productName;

	$ctrImage = 0;
	$ctrImageMap = 0;
	$ctrImagePrimary = 0;


	foreach ($val['productImages'] as $key3 => $value3) {
		if($value3['imageIsMap']){

			$dataArray[$key]['imageMap'.$ctrImageMap] = $value3['imageUrl'];
			if(isset($value3['imageTitle'])){
				$dataArray[$key]['imageMapTitle'.$ctrImageMap] = $value3['imageTitle'];
			}
			$ctrImageMap++;
		}
		elseif($value3['imageIsPrimary']){

			$dataArray[$key]['imagePrimary'.$ctrImagePrimary] = $value3['imageUrl'];
			if(isset($value3['imageTitle'])){
				$dataArray[$key]['imagePrimaryTitle'.$ctrImagePrimary] = $value3['imageTitle'];
			}
			$ctrImagePrimary++;

		}
		else{
			$dataArray[$key]['image'.$ctrImage] = $value3['imageUrl'];
			if(isset($value3['imageTitle'])){
				$dataArray[$key]['imageTitle'.$ctrImage] = $value3['imageTitle'];
			}
			$ctrImage++;

		}

		
		
	}

	$ctrCountries=0;
		foreach($val['productCountries'] as $key4 => $value4){
			if($value4['isPrimary']){
				$dataArray[$key]['countriesPrimary'] = $value4['countries'];
			}else{
				$dataArray[$key]['countries'.$ctrCountries] = $value4['countries'];
				$ctrCountries++;
			}

		}
	if(!empty($val['productAvailability'])){
		$hasDouble = false;
		$amountContainer = array(); 
		$amountContainerETC = array(); 




		if(!isset($_GET['noPrice'])){
			foreach($val['productAvailability'] as $key2=>$val2){
				if (strtotime(str_replace('/', '-', $val2['dateFrom']))<strtotime('01-07-2020')) {
					continue;
				}
				try{
					$tempStr = '20';
					$datearr = explode('/', $val2['dateFrom']);
					if (!isset($datearr[2])) {
						continue;
					}
					$yearDate = $datearr[2];
					if(strlen($datearr[2])<=2){
						$yearDate = $tempStr.$datearr[2];
					}
					$dateSrc = $yearDate."-".$datearr[1]."-".$datearr[0];

				

					$dateTime = strtotime(date_format(date_create($dateSrc), 'Y-m-d'));
					if($today<$dateTime){
						if($val2['isMultiPrice'] == 1){
							foreach($val2['multiPrice'] as $key3=>$val3){
								if (!isset($val3['minGuest'])) {
									continue;
								}
								if($val3['minGuest'] == 2){
									$hasDouble = true;
									$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];
									array_push($amountContainer, $amount);
								}else{
									$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];
									/*if ($val['productId']==360) {
										$dataArray[$key]['amount'][]=array(
											'base'=>$val3['basePrice'],
											'rate'=>$refCurr[$val3['fromCurrency']],
											'fromCurrency'=>$val3['fromCurrency'],
											'refCurr'=>$refCurr
										);
									}*/
									array_push($amountContainerETC, $amount);
								}
								
							}

						}else{
							$amount = (float)$val2['basePrice']*$refCurr[trim($val2['fromCurrency'])];
							array_push($amountContainer, $amount);
						}
					}
				}catch (Exception $e) {
					$dataArray[$key]['error'][]=$e->getMessage();
				}
				
				
			}
		}

		

		
		$markUp = $agencyData['toursConfig']['markup_pct'];
		if($hasDouble){
			$newAmountTemp = min($amountContainer);
		}else{
			$newAmountTemp = min($amountContainerETC);
		}
		$ids=array(947,948);
		if (in_array($val['productId'], $ids)) {
			$dataArray[$key]['amountContainer']=$amountContainer;
			$dataArray[$key]['amountContainerETC']=$amountContainerETC;
		}

		$newMarkUp = $markUp/100;
		$newAmount = ($newAmountTemp*$newMarkUp) + $newAmountTemp;
		unset($dataArray[$key]['productAvailability']);
		//$dataArray[$key]['price'] = $newAmount;
		//$dataArray[$key]['priceTxt'] = number_format($newAmount, 2, '.', ',');
		$dataArray[$key]['price'] = number_format($newAmount, 0, '.', ',');


		
		$dataArray[$key]['availability'] = 1; 
		if($newAmount==0){
			$dataArray[$key]['availability'] = 0;
		}

		$dataArray[$key]['title'] = removeSpecialChar($val['productName']);
		unset($dataArray[$key]['productName']);

		$dataArray[$key]['altPrice'] = removeSpecialChar($val['productAltPrice']);
		unset($dataArray[$key]['productAltPrice']);

		$dataArray[$key]['keywords'] = removeSpecialChar($val['productKeywords']);

		unset($dataArray[$key]['productKeywords']);
		$dataArray[$key]['tags'] = removeSpecialChar($val['productTag']);
		unset($dataArray[$key]['productTag']);

		$dataArray[$key]['duration'] = removeSpecialChar($val['productSumarryDescription']);
		 unset($dataArray[$key]['productSumarryDescription']);


		 $dataArray[$key]['itinerary'] = removeSpecialChar($val['productDetailedDescription']);
         unset($dataArray[$key]['productDetailedDescription']);


		 $dataArray[$key]['destination'] = removeSpecialChar($val['productFromAddress']);
                unset($dataArray[$key]['productFromAddress']);

		 
		 $dataArray[$key]['inclusion'] = removeSpecialChar($val['productInclusionDescription']);
                unset($dataArray[$key]['productInclusionDescription']);

		 $dataArray[$key]['condition'] = removeSpecialChar($val['productConditionsDescription']);
                unset($dataArray[$key]['productConditionsDescription']);

		 $dataArray[$key]['exclusion'] = removeSpecialChar($val['productExclusionDescription']);
                unset($dataArray[$key]['productExclusionDescription']);

		 $dataArray[$key]['country'] = removeSpecialChar($val['productFromCountry']);
                unset($dataArray[$key]['productFromCountry']);
         $dataArray[$key]['numImages'] = $ctrImage;
         $dataArray[$key]['numImagesMap'] = $ctrImageMap;
          $dataArray[$key]['numImagesPrimary'] = $ctrImagePrimary;
           $dataArray[$key]['numCountries'] = $ctrCountries;

         //$dataArray[$key]['images'] = $imageArr;
         	unset($dataArray[$key]['productImages']);
         	unset($dataArray[$key]['productCountries']);

	}else{
		// $dataArray[$key]['empty']=true;
		unset($dataArray[$key]['productAvailability']);
		//$dataArray[$key]['price'] = 0;
		//$dataArray[$key]['priceTxt'] = number_format(0, 2, '.', ',');
		$dataArray[$key]['price'] = number_format(0, 0, '.', ',');

		$dataArray[$key]['availability'] = 0; 
		

                $dataArray[$key]['title'] = removeSpecialChar($val['productName']);
                unset($dataArray[$key]['productName']);

                $dataArray[$key]['altPrice'] = removeSpecialChar($val['productAltPrice']);
				unset($dataArray[$key]['productAltPrice']);

                $dataArray[$key]['duration'] = removeSpecialChar($val['productSumarryDescription']);
                 unset($dataArray[$key]['productSumarryDescription']);


                $dataArray[$key]['itinerary'] = removeSpecialChar($val['productDetailedDescription']);
                unset($dataArray[$key]['productDetailedDescription']);

                $dataArray[$key]['destination'] = removeSpecialChar($val['productFromAddress']);
                unset($dataArray[$key]['productFromAddress']);

                $dataArray[$key]['inclusion'] = removeSpecialChar($val['productInclusionDescription']);
                unset($dataArray[$key]['productInclusionDescription']);

                $dataArray[$key]['condition'] = removeSpecialChar($val['productConditionsDescription']);
                unset($dataArray[$key]['productConditionsDescription']);

                $dataArray[$key]['exclusion'] = removeSpecialChar($val['productExclusionDescription']);
                unset($dataArray[$key]['productExclusionDescription']);
                $dataArray[$key]['country'] = removeSpecialChar($val['productFromCountry']);
                unset($dataArray[$key]['productFromCountry']);

                $dataArray[$key]['tags'] = removeSpecialChar($val['productTag']);
		unset($dataArray[$key]['productTag']);
				$dataArray[$key]['keywords'] = removeSpecialChar($val['productKeywords']);
				unset($dataArray[$key]['productKeywords']);

                $dataArray[$key]['numImages'] = $ctrImage;
         		$dataArray[$key]['numImagesMap'] = $ctrImageMap;
         		 $dataArray[$key]['numImagesPrimary'] = $ctrImagePrimary;
         		 $dataArray[$key]['numCountries'] = $ctrCountries;

         		 unset($dataArray[$key]['productCountries']);

             //   $dataArray[$key]['images'] = $imageArr;
         	unset($dataArray[$key]['productImages']);
        }
        $datasResult[$val['productId']] = $dataArray[$key];
}

//echo json_encode($datasResult);
//print_r($datasResult);
//die();

echo $_GET['el'].'(\''.json_encode($datasResult,JSON_FORCE_OBJECT).'\');';
//print_r($datasResult);

die();


//print_r($dataArray)
//echo json_encode($datasResult);
?>

