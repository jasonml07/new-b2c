<?php
// print_r($_GET);
error_reporting(0);
$connection = new MongoClient();
$db = $connection->db_system;

$agencyData = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'],"is_active_b2c"=>1,"tours"=>1));
// print_r($agencyData);
if(empty($agencyData)){
	die();
}

//$agencyData

$today = strtotime('now +3 days');

$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
	$refCurr[$val['curr_from']] = (float)$val['rate_to'];
}
$query=array("is_active"=>1);
if (isset($_GET['ids'])||isset($_POST['ids'])) {
	$_ids=isset($_GET['ids'])?$_GET['ids']:$_POST['ids'];
	$query['productId']=array('$in'=>array_map('intval', $_GET['ids']));
}
$resCol = $db->products_inventory->find($query,array("_id"=>0,"productAvailability"=>1,"productId"=>1));
$dataArray = iterator_to_array($resCol);

$datasResult = array();

if (count($dataArray)) {
	foreach($dataArray as $key=>$val){
		$debugs=array();
		
		if(!empty($val['productAvailability'])){
			$hasDouble = false;
			$amountContainer = array(); 
			$amountContainerETC = array(); 

			foreach($val['productAvailability'] as $key2=>$val2){
				try{
					$tempStr = '20';
					$datearr = explode('/', $val2['dateFrom']);
					
					$yearDate = $datearr[2];
					if(strlen($datearr[2])<=2){
						$yearDate = $tempStr.$datearr[2];
					}

					$dateSrc = $yearDate."-".$datearr[1]."-".$datearr[0];

				

					$dateTime = strtotime(date_format(date_create($dateSrc), 'Y-m-d'));
					if ($dateTime<strtotime(date("Y-m-d", strtotime("now +3 days")))) {
						continue;
					}
					if($val2['isMultiPrice'] == 1){
							foreach($val2['multiPrice'] as $key3=>$val3){
								if(isset($val3['minGuest'])&&$val3['minGuest'] == 2){
									$hasDouble = true;
									$amount = (float)$val3['basePrice']*$refCurr[$val3['fromCurrency']];
									array_push($amountContainer, $amount);
									if (!isset($debugs['amounts'])) {
										$debugs['amounts']=$val3['basePrice'];
									} else {
										array_push($debugs['amounts'], $val3['basePrice']);
									}
									if (!$amount) {
										if (!isset($debugs[$val['productId']]['zero-price'])) {
											$debugs['zero-price']=array($val3);
										} else {
											array_push($debugs['zero-price'], $val3);
										}
									}
								}else{
									$amount = (float)$val3['basePrice']*$refCurr[$val3['fromCurrency']];
									array_push($amountContainerETC, $amount);
								}
								
							}

						}else{
							$amount = (float)$val2['basePrice']*$refCurr[$val2['fromCurrency']];
							array_push($amountContainer, $amount);
						}
				}catch (Exception $e) {
				}
				
				
			}

			$markUp = $agencyData['toursConfig']['markup_pct'];
			if($hasDouble){
				$newAmountTemp = min($amountContainer);
			}else{
				$newAmountTemp = min($amountContainerETC);
			}
			

			$newMarkUp = $markUp/100;
			$newAmount = ($newAmountTemp*$newMarkUp) + $newAmountTemp;
			unset($dataArray[$key]['productAvailability']);
			//$dataArray[$key]['price'] = $newAmount;
			//$dataArray[$key]['priceTxt'] = number_format($newAmount, 2, '.', ',');
			// $dataArray[$key]['amounts']=$amountContainer;
			$dataArray[$key]['priceTxtAUD'] = "$ ".number_format($newAmount, 0, '.', ',');
			$dataArray[$key]['debug']=$debugs;
		}else{
			unset($dataArray[$key]['productAvailability']);
			//$dataArray[$key]['price'] = 0;
			//$dataArray[$key]['priceTxt'] = number_format(0, 2, '.', ',');
			$dataArray[$key]['priceTxtAUD'] = "$ ".number_format(0, 0, '.', ',');
		}
		$datasResult[$val['productId']] = $dataArray[$key];
		
	}
}

//print_r($dataArray);
echo "var retrievedJSON = '".json_encode($datasResult)."';";
?>




function goToTours(link){
	window.top.location = link;  

}

function goToTours2(link){
	var x = screen.width/2 - 700/2;
    var y = screen.height/2 - 450/2;
    signinWin = window.open(link, "_blank", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + x + ",top=" + y);
    signinWin.focus();

}
var _tz={
	code:"<?=$_GET['agencyCode'];?>"
	,host:"http://<?=$_SERVER['HTTP_HOST'];?>/externaltours"
};
<?php if(isset($_GET['callback'])) :?>
	<?=$_GET['callback']?>(_tz);
<?php endif; ?>