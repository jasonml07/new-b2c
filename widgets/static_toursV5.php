<?php 
ini_set('memory_limit', '-1');
error_reporting(E_ALL ^ E_WARNING);


$connection = new MongoClient();
$db = $connection->db_system;

$agencyData = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'],"is_active_b2c"=>1,"tours"=>1));

if(empty($agencyData)){
	die();
}
$markUp = $agencyData['toursConfig']['markup_pct'];


$today = strtotime('now +3 days');

$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
	$refCurr[$val['curr_from']] = (float)$val['rate_to'];
}



$resData = $db->products_inventory->findOne(array("is_active"=>1,"productId"=>(int)$_GET['id']),array("_id"=>0,"productAvailability"=>1,"productId"=>1,'productName'=>1,'productSumarryDescription'=>1, 'productFromAddress'=>1));



//$dataArray = iterator_to_array($resCol);

//print_r($resData);
$datasResult = array();
$datasResult['Status']='Error';
$datasResult['Status_Msg']='No Available Data';

if(!empty($resData['productAvailability'])){
	$hasDouble = false;
	$amountDouble = array(); 
	$amountMultiple = array(); 
	$datasResult['productAvailability'] = array();
	foreach($resData['productAvailability'] as $key2=>$val2){
		if (strtotime(str_replace('/', '-', $val2['dateFrom']))<strtotime(date('d-m-Y'))) {
			continue;
		}
		try{
			$tempStr = '20';
			$datearr = explode('/', $val2['dateFrom']);
			$datearr2 = explode('/', $val2['dateTo']);
			if (!isset($datearr[2])) {
				continue;
			}
			$yearDate = $datearr[2];
			if(strlen($datearr[2])<=2){
				$yearDate = $tempStr.$datearr[2];
			}
			$dateSrc = $yearDate."-".$datearr[1]."-".$datearr[0];
			$dateTime = strtotime(date_format(date_create($dateSrc), 'Y-m-d'));


			$dataAvailability = array();
			$dataAvailability['dateFrom'] = $datearr[2]."-".$datearr[1]."-".$datearr[0];
			$dataAvailability['dateTo'] = $datearr2[2]."-".$datearr2[1]."-".$datearr2[0];
			$dataAvailability['data'] = array();
			//array_push($datasResult['productAvailability'], $dataAvailability);

			if($today<$dateTime){
				if($val2['isMultiPrice'] == 1){
					$hasDouble2 = false;
					$amountDoubleTemp = array(); 
					$amountMultipleTemp = array(); 
					
					foreach($val2['multiPrice'] as $key3=>$val3){
						$tempData = array();
						if (!isset($val3['minGuest'])) {
							continue;
						}
						if($val3['minGuest'] == 2){
							$hasDouble = true;
							$hasDouble2 = true;
							$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];

							$tempData['name'] = $val3['supplement_avail_name'];
							$newMarkUp2 = $markUp/100;
							$newAmount2 = ($amount*$newMarkUp2) + $amount;
							$tempData['priceAUD'] = number_format($newAmount2, 0, '.', ',');

							array_push($amountDouble, $amount);
							array_push($amountDoubleTemp, $amount);
						}else{
							$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];

							$tempData['name'] = $val3['supplement_avail_name'];
							$newMarkUp2 = $markUp/100;
							$newAmount2 = ($amount*$newMarkUp2) + $amount;
							$tempData['priceAUD'] = number_format($newAmount2, 0, '.', ',');

							array_push($amountMultiple, $amount);
							array_push($amountMultipleTemp, $amount);
						}
						array_push($dataAvailability['data'], $tempData);

					}
					if($hasDouble2){
						$newAmountTemp2 = min($amountDoubleTemp);
					}else{
						$newAmountTemp2 = min($amountMultipleTemp);
					}
					
					

					$newMarkUp2 = $markUp/100;
					$newAmount2 = ($newAmountTemp2*$newMarkUp2) + $newAmountTemp2;
					$dataAvailability['priceAUD'] = number_format($newAmount2, 0, '.', ',');



				}else{
					$amount = (float)$val2['basePrice']*$refCurr[trim($val2['fromCurrency'])];
					array_push($amountDouble, $amount);

					$newMarkUp2 = $markUp/100;
					$newAmount2 = ($amount*$newMarkUp2) + $amount;
					$dataAvailability['priceAUD'] = number_format($newAmount2, 0, '.', ',');

				}


			}

			array_push($datasResult['productAvailability'], $dataAvailability);


			$datasResult['Status']='Success';
			$datasResult['Status_Msg']='Data found';
			
		}catch (Exception $e) {
			$datasResult['Status']='Error';
			$datasResult['Status_Msg']=$e->getMessage();
		}
	}

	if(empty($datasResult['productAvailability'])){
		$datasResult['Status']='Success';
		$datasResult['Status_Msg']='No Available Dates';
		$datasResult['productId'] = $resData['productId'];
		$datasResult['productFromAddress'] = $resData['productFromAddress'];
		$datasResult['agencyCode'] = $_GET['agencyCode'];
		$datasResult['productName'] = htmlspecialchars($resData['productName'],ENT_QUOTES);
		$datasResult['productSumarryDescription'] = htmlspecialchars(preg_replace('/\n|<b>|<h5(.*)>|<p(.*)>|<font(.*)>|<\/font>|<br \/>|<div(.*)>|<\/div>|<span(.*)>|<\/span>/',"",$resData['productSumarryDescription']),ENT_QUOTES);
	}

	if($datasResult['Status'] == 'Success'){
		
		if($hasDouble){
			$newAmountTemp = min($amountDouble);
		}else{
			$newAmountTemp = min($amountMultiple);
		}
		
		$newMarkUp = $markUp/100;
		$newAmount = ($newAmountTemp*$newMarkUp) + $newAmountTemp;
		$datasResult['priceTotalAUD'] = number_format($newAmount, 0, '.', ',');
		$datasResult['productId'] = $resData['productId'];
		$datasResult['productFromAddress'] = $resData['productFromAddress'];
		$datasResult['agencyCode'] = $_GET['agencyCode'];
		$datasResult['productName'] = htmlspecialchars($resData['productName'],ENT_QUOTES);
		$datasResult['productSumarryDescription'] = htmlspecialchars(preg_replace('/\n|<b>|<h5(.*)>|<p(.*)>|<font(.*)>|<\/font>|<br \/>|<div(.*)>|<\/div>|<span(.*)>|<\/span>/',"",$resData['productSumarryDescription']),ENT_QUOTES);
	}
	



}else{
	$datasResult['agencyCode'] = $_GET['agencyCode'];
	$datasResult['productId'] = $resData['productId'];
	$datasResult['productName'] = htmlspecialchars($resData['productName'],ENT_QUOTES);
	$datasResult['productFromAddress'] = $resData['productFromAddress'];
	$datasResult['priceTotalAUD'] = number_format(0, 0, '.', ',');
	$datasResult['productSumarryDescription'] = htmlspecialchars(preg_replace('/\n|<b>|<h5(.*)>|<p(.*)>|<font(.*)>|<\/font>|<br \/>|<div(.*)>|<\/div>|<span(.*)>|<\/span>/',"",$resData['productSumarryDescription']),ENT_QUOTES);
	$datasResult['Status']='Error';
	$datasResult['productAvailability'] = array();
	$datasResult['Status_Msg']='No Available Dates';


}

echo json_encode($datasResult);
?>

