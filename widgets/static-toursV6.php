<?php 
ini_set('memory_limit', '-1');
error_reporting(E_ALL ^ E_WARNING);

function getDisplayScript($el,$id,$action,$datasResult){


	//print_r($action);
	//return;
	//$action = (isset($_POST['type'])) ? $_POST['type'] : $_GET['type'];
		
		switch ($action) {
			case "price":{
	$res = "document.getElementById('".$el."').innerHTML = '".$datasResult[$id]['priceTxtAUD']."';";

			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "title":{
	$res = "document.getElementById('".$el."').innerHTML = '".$datasResult[$id]['productName']."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "itinerary":{
	$res = "document.getElementById('".$el."').innerHTML = '".html_entity_decode($datasResult[$id]['productDetailedDescription'])."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "destination":{
	$res = "document.getElementById('".$el."').innerHTML = '".html_entity_decode($datasResult[$id]['productFromAddress'])."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "duration":{
	$res = "document.getElementById('".$el."').innerHTML = '".html_entity_decode($datasResult[$id]['productSumarryDescription'])."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "condition":{
	$res = "document.getElementById('".$el."').innerHTML = '".html_entity_decode($datasResult[$id]['productConditionsDescription'])."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "inclusion":{
	$res = "document.getElementById('".$el."').innerHTML = '".html_entity_decode($datasResult[$id]['productInclusionDescription'])."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "exclusion":{
	$res = "document.getElementById('".$el."').innerHTML = '".html_entity_decode($datasResult[$id]['productExclusionDescription'])."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
			case "country":{
	$res = "document.getElementById('".$el."').innerHTML = '".html_entity_decode($datasResult[$id]['productFromCountry'])."';";
			return $res;
	  	   		exit;
	  	  	break; 
			}
	}
	

}

$connection = new MongoClient();
$db = $connection->db_system;

$agencyData = $db->agency_b2b_b2c->findOne(array("agency_code"=>$_GET['agencyCode'],"is_active_b2c"=>1,"tours"=>1));

if(empty($agencyData)){
	die();
}

$int_arr=array();

foreach (explode(',', $_GET['ids']) as $key => $value) {
	array_push($int_arr, (int)$value);
}
//$agencyData

$today = strtotime('now +3 days');

$resColCurr = $db->currency_ex->find(array(),array("_id"=>0));
$dataArrayCurr = iterator_to_array($resColCurr);

$refCurr = array();
foreach($dataArrayCurr as $key=>$val){
	$refCurr[$val['curr_from']] = (float)$val['rate_to'];
}

$resCol = $db->products_inventory->find(array("is_active"=>1,"productId"=>array('$in'=>$int_arr)),array("_id"=>0,"productAvailability"=>1,"productId"=>1,'productName'=>1,'productSumarryDescription'=>1,'productDetailedDescription'=>1,'productFromAddress'=>1,'productInclusionDescription'=>1,'productConditionsDescription'=>1,'productExclusionDescription'=>1,'productFromCountry'=>1));
$dataArray = iterator_to_array($resCol);

/*echo '<pre>';
print_r($dataArray);
echo '</pre>';
exit()*/;

$datasResult = array();
foreach($dataArray as $key=>$val){
	
	#$dataArray[$key]['productName'] = htmlentities($val['productName'], ENT_QUOTES);
	#$dataArray[$key]['productName'] =  utf8_encode(htmlspecialchars($val['productName'], ENT_QUOTES));
	//$prodName = utf8_encode(htmlspecialchars($val['productName'], ENT_QUOTES));
	//$productName = str_replace('"', '', $prodName);
	//$productName = str_replace(',', ' ', $prodName);

	//$dataArray[$key]['productName'] = $productName;

	
	if(!empty($val['productAvailability'])){
		$hasDouble = false;
		$amountContainer = array(); 
		$amountContainerETC = array(); 

		foreach($val['productAvailability'] as $key2=>$val2){
			if (strtotime(str_replace('/', '-', $val2['dateFrom']))<strtotime('01-07-2020')) {
				continue;
			}
			try{
				$tempStr = '20';
				$datearr = explode('/', $val2['dateFrom']);
				if (!isset($datearr[2])) {
					continue;
				}
				$yearDate = $datearr[2];
				if(strlen($datearr[2])<=2){
					$yearDate = $tempStr.$datearr[2];
				}
				$dateSrc = $yearDate."-".$datearr[1]."-".$datearr[0];

			

				$dateTime = strtotime(date_format(date_create($dateSrc), 'Y-m-d'));
				if($today<$dateTime){
					if($val2['isMultiPrice'] == 1){
						foreach($val2['multiPrice'] as $key3=>$val3){
							if (!isset($val3['minGuest'])) {
								continue;
							}
							if($val3['minGuest'] == 2){
								$hasDouble = true;
								$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];
								array_push($amountContainer, $amount);
							}else{
								$amount = (float)$val3['basePrice']*$refCurr[trim($val3['fromCurrency'])];
								/*if ($val['productId']==360) {
									$dataArray[$key]['amount'][]=array(
										'base'=>$val3['basePrice'],
										'rate'=>$refCurr[$val3['fromCurrency']],
										'fromCurrency'=>$val3['fromCurrency'],
										'refCurr'=>$refCurr
									);
								}*/
								array_push($amountContainerETC, $amount);
							}
							
						}

					}else{
						$amount = (float)$val2['basePrice']*$refCurr[trim($val2['fromCurrency'])];
						array_push($amountContainer, $amount);
					}
				}
			}catch (Exception $e) {
				$dataArray[$key]['error'][]=$e->getMessage();
			}
			
			
		}

		$markUp = $agencyData['toursConfig']['markup_pct'];
		if($hasDouble){
			$newAmountTemp = min($amountContainer);
		}else{
			$newAmountTemp = min($amountContainerETC);
		}
		$ids=array(947,948);
		if (in_array($val['productId'], $ids)) {
			$dataArray[$key]['amountContainer']=$amountContainer;
			$dataArray[$key]['amountContainerETC']=$amountContainerETC;
		}

		$newMarkUp = $markUp/100;
		$newAmount = ($newAmountTemp*$newMarkUp) + $newAmountTemp;
		unset($dataArray[$key]['productAvailability']);
		//$dataArray[$key]['price'] = $newAmount;
		//$dataArray[$key]['priceTxt'] = number_format($newAmount, 2, '.', ',');
		$dataArray[$key]['priceTxtAUD'] = number_format($newAmount, 0, '.', ',');
		$dataArray[$key]['productName'] = htmlspecialchars($val['productName'],ENT_QUOTES);
		$dataArray[$key]['productSumarryDescription'] = htmlspecialchars($val['productSumarryDescription'],ENT_QUOTES);
		 $dataArray[$key]['productDetailedDescription'] = htmlspecialchars($val['productDetailedDescription'],ENT_QUOTES);
		 $dataArray[$key]['productFromAddress'] = htmlspecialchars($val['productFromAddress'],ENT_QUOTES);
		 $dataArray[$key]['productDetailedDescription'] = htmlspecialchars($val['productDetailedDescription'],ENT_QUOTES);
		 $dataArray[$key]['productFromAddress'] = htmlspecialchars($val['productFromAddress'],ENT_QUOTES);
		 $dataArray[$key]['productInclusionDescription'] = htmlspecialchars($val['productInclusionDescription'],ENT_QUOTES);
		 $dataArray[$key]['productConditionsDescription'] = htmlspecialchars($val['productConditionsDescription'],ENT_QUOTES);
		 $dataArray[$key]['productExclusionDescription'] = htmlspecialchars($val['productExclusionDescription'],ENT_QUOTES);
		 $dataArray[$key]['productFromCountry'] = htmlspecialchars($val['productFromCountry'],ENT_QUOTES);

	}else{
		// $dataArray[$key]['empty']=true;
		unset($dataArray[$key]['productAvailability']);
		//$dataArray[$key]['price'] = 0;
		//$dataArray[$key]['priceTxt'] = number_format(0, 2, '.', ',');
		$dataArray[$key]['priceTxtAUD'] = number_format(0, 0, '.', ',');
                $dataArray[$key]['productName'] = htmlspecialchars($val['productName'],ENT_QUOTES);
                $dataArray[$key]['productSumarryDescription'] = htmlspecialchars($val['productSumarryDescription'],ENT_QUOTES);
                $dataArray[$key]['productDetailedDescription'] = htmlspecialchars($val['productDetailedDescription'],ENT_QUOTES);
                $dataArray[$key]['productFromAddress'] = htmlspecialchars($val['productFromAddress'],ENT_QUOTES);
                $dataArray[$key]['productInclusionDescription'] = htmlspecialchars($val['productInclusionDescription'],ENT_QUOTES);
                $dataArray[$key]['productConditionsDescription'] = htmlspecialchars($val['productConditionsDescription'],ENT_QUOTES);
                $dataArray[$key]['productExclusionDescription'] = htmlspecialchars($val['productExclusionDescription'],ENT_QUOTES);
                $dataArray[$key]['productFromCountry'] = htmlspecialchars($val['productFromCountry'],ENT_QUOTES);
        }
        $datasResult[$val['productId']] = $dataArray[$key];
}




if(isset($_GET['customDisplay']) && !empty($_GET['customDisplay'])){

	$varPathValue = explode('|', $_GET['customDisplay']);
	$str = "";
	foreach($varPathValue as $key => $value){
		$DetvarPathValue = explode('-', $value);

		//print_r($DetvarPathValue);
		//print_r($DetvarPathValue); 

		//print_r($DetvarPathValue[0],$_GET['ids'],$DetvarPathValue[1],$datasResult);
		$res = getDisplayScript($DetvarPathValue[0],$_GET['ids'],$DetvarPathValue[1],$datasResult);

		$str .=$res;
		
	}

	echo $str;

}else{

	echo getDisplayScript($_GET['el'],$_GET['ids'],$_GET['type'],$datasResult);

	//echo $res;
}



//print_r($dataArray)
//echo json_encode($datasResult);
?>

