<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		
		case "view": {

			$connection = new MongoClient();
			$db = $connection->db_system;
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['guest_first_name'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			$dataCollections = $db->guest->find($whereData);


			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			$arrayResults = array();
			foreach($dataArray as $key => $row){


				$data = array();
				$data['id_agency_guest'] = $key;
				$data['guest_first_name'] = $row['guest_first_name'];
				$data['guest_last_name'] = $row['guest_last_name'];
				$data['guest_agency_code'] = $row['guest_agency_code'];
				$data['guest_agency_name'] = $row['guest_agency_name'];
				$data['guest_business'] = $row['guest_business'];
				$data['guest_fax'] = $row['guest_fax'];
				$data['guest_home'] = $row['guest_home'];
				$data['guest_mobile'] = $row['guest_mobile'];
				$data['guest_age'] = $row['guest_age'];

/*				if($row['is_active'] == 1){
					$data['is_active'] = 'Yes';
				}else if($row['is_active'] == 0) $data['is_active'] = 'No';*/
				$data['is_active'] = $row['is_active'];
				
				array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewAgencyGuest" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;

  	  	break; 
  	  	

		}

		case "addGuest":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			$resPOST = json_decode($_POST['postDetails'], true);
			$id = $resPOST['agency_guest_id'];

			if($id == 0){
				
				unset($resPOST['agency_guest_id']);
				$tool = new Tools();
				$res = $db->guest->insert($resPOST);
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			}else{

				$mongoID = new MongoID($resPOST['agency_guest_id']);


				unset($resPOST['agency_guest_id']);
				$res = $db->guest->update(array("_id"=> $mongoID), array("\$set" => $resPOST));
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}	
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}


		case "getAgencyGuestDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['agency_guest_id']);
			$res = $db->guest->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['agency_guest_id'] = $_POST['agency_guest_id'];
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_agency_guest']);
			
			$db->guest->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 1)));
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_agency_guest']);
			
			$db->guest->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 0)));
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}


	
	}
?>