<?php
session_start();
set_time_limit(0);
ini_set('memory_limit', '1024M');
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
define('SYS_MARKUP', 35);
define('SYS_COMM', 15);
define('SYS_DISC', 0);
define('SYS_GST', 0);
define('SYS_CANCEL_PCT', 5);
require_once('../_classes/tools.class.php');
require_once('../_classes/hotelsPro.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		
		
		case "doSearchHP":{
			$dataGET = json_decode($_GET['postDetails']);
			
			$connection = new MongoClient();
			$db = $connection->db_system;


			
			$hpObj = new HotelsProClient();
			$hotels = $hpObj->get_available_hotels($dataGET);
			if(!is_object($hotels)){
				$response = array ( "success" => true, "total" => 0, "viewHotelsHP" => array());
  	  		
	  	  		print json_encode($response);
	  	  		$connection->close();
	  	  		exit;
			}

			if (is_object($hotels->availableHotels)) {
				$hotel_results_Hpro[] = $hotels->availableHotels;
			} else {
				$hotel_results_Hpro = $hotels->availableHotels;
			}

			$searchID = $hotels->searchId;
			#$ctrHotelFound = 0;
			$hotesProCode = array();
			foreach ((array)$hotel_results_Hpro as $hnum => $hotel) {
				if(!in_array($hotel->hotelCode, $hotesProCode)){
					array_push($hotesProCode, $hotel->hotelCode);
				}
				#$ctrHotelFound++;
			}
			
			$resDataCollection = $db->hp_hotels->find(array("HotelCode"=> array("\$in"=>$hotesProCode)),array("_id"=>0));
			$dataArray = iterator_to_array($resDataCollection);
			$hotelCollection = array();
			
			foreach($dataArray as $key => $row){
				$hotelCollection[$row['HotelCode']] =  $row;
				
			}

			$currEx = array();
			
			
			$ctrHotelFound = 0;
			foreach ((array)$hotel_results_Hpro as $hnum => $hotel) {
				
				if(array_key_exists($hotel->hotelCode,$hotelCollection)){

					if(!array_key_exists($hotel->currency,$currEx)){
						$resCurr = $db->currency_ex->findOne(array("curr_from"=> $hotel->currency), array("_id"=>0));

						$hotel = (array) $hotel;
						$currEx[$hotel->currency] = (float) $resCurr['rate_to'];

						



						$originalAmount = (float)$hotel['totalPrice'];
						$orginalNetAmt = (float)$hotel['totalPrice']*$currEx[$hotel->currency] ;
						$netAmountInAgency = round($orginalNetAmt);
						$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
						$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
						$commInAmountTemp = $grossAmt*(SYS_COMM/100);
						$commAmt = round($commInAmountTemp);
						$discInAmountTemp = $grossAmt*(SYS_DISC/100);
						$discAmt = round($discInAmountTemp);
						$totalLessAMOUNTT = round($commAmt+$discAmt);
						$totaldueamt = $grossAmt-$totalLessAMOUNTT;
						$totalProfitamt = $totaldueamt-$netAmountInAgency;
						$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
						$gstAmt = $totalProfitamt*(SYS_GST/100);

						$hotel['currTo'] = 'AUD';
						$hotel['convertedTotalPrice'] = $grossAmt;


						$calculatedPrice = array(
							"fromCurrency"=> $hotel->currency,
							"fromRate"=> 1,
							"toCurrency"=> $resCurr['curr_to'],
							"toRate"=> $resCurr['rate_to'],
							"originalAmount"=> $originalAmount,
							"orginalNetAmt"=> $orginalNetAmt,
							"netAmountInAgency"=> $netAmountInAgency,
							"markupInAmount"=>$markupInAmountTemp,
							"markupInPct"=>SYS_MARKUP,
							"grossAmt"=> $grossAmt,
							"commInPct"=>SYS_COMM,
							"commInAmount"=>$commInAmountTemp,
							"commAmt"=>$commAmt,
							"discInPct"=>SYS_DISC,
							"discInAmount"=>$discInAmountTemp,
							"discAmt"=>$discAmt,
							"totalLessAmt"=> $totalLessAMOUNTT,
							"totalDueAmt"=> $totaldueamt,
							"totalProfitAmt"=> $totalProfitamt,
							"gstInPct"=>SYS_GST,
							"gstInAmount"=>$gstInAmountTemp,
							"gstAmt"=>$gstAmt


						);
						$hotel['calculatedPrice'] = $calculatedPrice;
						$hotel = (object) $hotel;
						
					}else{
						$hotel = (array) $hotel;
						$originalAmount = (float)$hotel['totalPrice'];
						$orginalNetAmt = (float)$hotel['totalPrice']*$currEx[$hotel->currency] ;
						$netAmountInAgency = round($orginalNetAmt);
						$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
						$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
						$commInAmountTemp = $grossAmt*(SYS_COMM/100);
						$commAmt = round($commInAmountTemp);
						$discInAmountTemp = $grossAmt*(SYS_DISC/100);
						$discAmt = round($discInAmountTemp);
						$totalLessAMOUNTT = round($commAmt+$discAmt);
						$totaldueamt = $grossAmt-$totalLessAMOUNTT;
						$totalProfitamt = $totaldueamt-$netAmountInAgency;
						$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
						$gstAmt = $totalProfitamt*(SYS_GST/100);

						$hotel['currTo'] = 'AUD';
						$hotel['convertedTotalPrice'] = $grossAmt;


						$calculatedPrice = array(
							"fromCurrency"=> $hotel->currency,
							"fromRate"=> 1,
							"toCurrency"=> 'AUD',
							"toRate"=> $currEx[$hotel->currency],
							"originalAmount"=> $originalAmount,
							"orginalNetAmt"=> $orginalNetAmt,
							"netAmountInAgency"=> $netAmountInAgency,
							"markupInAmount"=>$markupInAmountTemp,
							"markupInPct"=>SYS_MARKUP,
							"grossAmt"=> $grossAmt,
							"commInPct"=>SYS_COMM,
							"commInAmount"=>$commInAmountTemp,
							"commAmt"=>$commAmt,
							"discInPct"=>SYS_DISC,
							"discInAmount"=>$discInAmountTemp,
							"discAmt"=>$discAmt,
							"totalLessAmt"=> $totalLessAMOUNTT,
							"totalDueAmt"=> $totaldueamt,
							"totalProfitAmt"=> $totalProfitamt,
							"gstInPct"=>SYS_GST,
							"gstInAmount"=>$gstInAmountTemp,
							"gstAmt"=>$gstAmt


						);
						$hotel['calculatedPrice'] = $calculatedPrice;
						$hotel = (object) $hotel;
					}

					if(isset($hotelCollection[$hotel->hotelCode]['roomsAvailable'])){
						array_push($hotelCollection[$hotel->hotelCode]['roomsAvailable'],$hotel);
						if($hotelCollection[$hotel->hotelCode]['priceSort']>(float)$hotel->totalPrice){
							$hotelCollection[$hotel->hotelCode]['priceSort'] = (float)$hotel->totalPrice;
						}
					}else{
						$hotelCollection[$hotel->hotelCode]['searchID'] = $searchID;
						$hotelCollection[$hotel->hotelCode]['roomsAvailable'] = array();
						array_push($hotelCollection[$hotel->hotelCode]['roomsAvailable'],$hotel);

						$hotelCollection[$hotel->hotelCode]['priceSort'] = (float)$hotel->totalPrice;
					}
					$ctrHotelFound++;
				}
			}

			$arrayHotelsResults = array();
			foreach($hotelCollection as $key => $val){
				$val['agency_code'] = $dataGET->agency_code;
				array_push($arrayHotelsResults,$val);
			}
			
			$response = array ( "success" => true, "total" => $ctrHotelFound, "viewMainHotelsHP" => $arrayHotelsResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		break; 
		}

		case "doAllocateHP":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$response = array ( "status" => true);
  	  		$hpObj = new HotelsProClient();
			$hotelsAllocate = $hpObj->get_allocated_hotel($_GET['searchID'],$_GET['hotelCode']);
			//print_r($hotelsAllocate);
			if(!is_object($hotelsAllocate)){
				$response = array ( "success" => true, "total" => 0, "viewAllocateHotelsHP" => array());
  	  		
	  	  		print json_encode($response);
	  	  		
	  	  		exit;
			}
			$arrayHotelsResults = array();
			$resID =$hotelsAllocate->responseId;
			$hotCode =$hotelsAllocate->hotelCode;
			$currEx = array();
			foreach($hotelsAllocate->availableHotels as $key => $val){

				if(!array_key_exists($val->currency,$currEx)){
					$resCurr = $db->currency_ex->findOne(array("curr_from"=> $val->currency), array("_id"=>0));

					$val = (array) $val;
					$currEx[$val->currency] = (float) $resCurr['rate_to'];

					

					$originalAmount = (float)$val['totalPrice'];
					$orginalNetAmt = (float)$val['totalPrice']*$currEx[$val->currency] ;
					$netAmountInAgency = round($orginalNetAmt);
					$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
					$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
					$commInAmountTemp = $grossAmt*(SYS_COMM/100);
					$commAmt = round($commInAmountTemp);
					$discInAmountTemp = $grossAmt*(SYS_DISC/100);
					$discAmt = round($discInAmountTemp);
					$totalLessAMOUNTT = round($commAmt+$discAmt);
					$totaldueamt = $grossAmt-$totalLessAMOUNTT;
					$totalProfitamt = $totaldueamt-$netAmountInAgency;
					$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
					$gstAmt = $totalProfitamt*(SYS_GST/100);

					$val['currTo'] = 'AUD';
					$val['convertedTotalPrice'] = $grossAmt;


					$calculatedPrice = array(
							"fromCurrency"=> $val['currency'],
							"fromRate"=> 1,
							"toCurrency"=> $resCurr['curr_to'],
							"toRate"=> $resCurr['rate_to'],
							"originalAmount"=> $originalAmount,
							"orginalNetAmt"=> $orginalNetAmt,
							"netAmountInAgency"=> $netAmountInAgency,
							"markupInAmount"=>$markupInAmountTemp,
							"markupInPct"=>SYS_MARKUP,
							"grossAmt"=> $grossAmt,
							"commInPct"=>SYS_COMM,
							"commInAmount"=>$commInAmountTemp,
							"commAmt"=>$commAmt,
							"discInPct"=>SYS_DISC,
							"discInAmount"=>$discInAmountTemp,
							"discAmt"=>$discAmt,
							"totalLessAmt"=> $totalLessAMOUNTT,
							"totalDueAmt"=> $totaldueamt,
							"totalProfitAmt"=> $totalProfitamt,
							"gstInPct"=>SYS_GST,
							"gstInAmount"=>$gstInAmountTemp,
							"gstAmt"=>$gstAmt


						);
					$val['calculatedPrice'] = $calculatedPrice;

					$val = (object) $val;
					
				}else{
					$val = (array) $val;
					$originalAmount = (float)$val['totalPrice'];
					$orginalNetAmt = (float)$val['totalPrice']*$currEx[$val->currency] ;
					$netAmountInAgency = round($orginalNetAmt);
					$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
					$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
					$commInAmountTemp = $grossAmt*(SYS_COMM/100);
					$commAmt = round($commInAmountTemp);
					$discInAmountTemp = $grossAmt*(SYS_DISC/100);
					$discAmt = round($discInAmountTemp);
					$totalLessAMOUNTT = round($commAmt+$discAmt);
					$totaldueamt = $grossAmt-$totalLessAMOUNTT;
					$totalProfitamt = $totaldueamt-$netAmountInAgency;
					$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
					$gstAmt = $totalProfitamt*(SYS_GST/100);

					$val['currTo'] = 'AUD';
					$val['convertedTotalPrice'] = $grossAmt;


					$calculatedPrice = array(
							"fromCurrency"=> $val['currency'],
							"fromRate"=> 1,
							"toCurrency"=> 'AUD',
							"toRate"=> $currEx[$val->currency],
							"originalAmount"=> $originalAmount,
							"orginalNetAmt"=> $orginalNetAmt,
							"netAmountInAgency"=> $netAmountInAgency,
							"markupInAmount"=>$markupInAmountTemp,
							"markupInPct"=>SYS_MARKUP,
							"grossAmt"=> $grossAmt,
							"commInPct"=>SYS_COMM,
							"commInAmount"=>$commInAmountTemp,
							"commAmt"=>$commAmt,
							"discInPct"=>SYS_DISC,
							"discInAmount"=>$discInAmountTemp,
							"discAmt"=>$discAmt,
							"totalLessAmt"=> $totalLessAMOUNTT,
							"totalDueAmt"=> $totaldueamt,
							"totalProfitAmt"=> $totalProfitamt,
							"gstInPct"=>SYS_GST,
							"gstInAmount"=>$gstInAmountTemp,
							"gstAmt"=>$gstAmt


						);
					$val['calculatedPrice'] = $calculatedPrice;
					$val = (object) $val;
				}
				$data = array();
				$data['agencyCode'] = $_GET["agencyCode"];
				$data['responseId'] = $resID;
				$data['hotelCode'] = $hotCode;
				$data['processId'] = $val->processId;
				$data['availabilityStatus'] = $val->availabilityStatus;
				$data['totalPrice'] = $val->totalPrice;
				$data['currency'] = $val->currency;
				$data['boardType'] = $val->boardType;
				$data['currTo'] = $val->currTo;
				$data['convertedTotalPrice'] = $val->convertedTotalPrice;
				$data['calculatedPrice'] = $val->calculatedPrice;
				$data['roomDetails'] = $val->rooms;
				
				//$cancellationPolicy = $hpObj->get_cancellation_policy($val->processId);
				//$data['cancellationPolicy'] = $cancellationPolicy->cancellationPolicy;
				
				array_push($arrayHotelsResults,$data);
				
			}
			$response = array ( "success" => true, "total" => $ctrHotelFound, "viewAllocateHotelsHP" => $arrayHotelsResults);
			print json_encode($response);
			$connection->close();
  	  		exit;
		break; 
		}
		case "addHotelsProRoomToBooking":{
			$tools = new Tools; 
			$connection = new MongoClient();
			$db = $connection->db_system;
			$paxDet = json_decode($_POST['postDetailsPax']);
			$hpObj = new HotelsProClient();
			$hotelsMaked = $hpObj->makeBooking($paxDet,$_POST['processId']);
			if(!is_object($hotelsMaked)){
				$response = array ( "status" => false);
  	  		
	  	  		print json_encode($response);
	  	  		
	  	  		exit;
			}
			
			if(isset($hotelsMaked->hotelBookingInfo) && $hotelsMaked->hotelBookingInfo->bookingStatus ==1){
				if($_POST['bookingID'] =='New'){
					$agency = $db->agency->findOne(array('agency_code'=>$_POST['agencyCode']),array("_id"=>0));
					$bookingID = $tools->getNextID("booking");
					$dt = new DateTime(date('Y-m-d'));
					$ts = $dt->getTimestamp();
					$today = new MongoDate($ts);

					$insertData = array(
						"booking_id"=>(int)$bookingID,
						"booking_consultant_code"=> $_SESSION['sys_consultant_code'],
						"booking_consultant_name"=>$_SESSION['sys_consultant_name'],
						"booking_agency_code"=>$agency['agency_code'],
						"booking_agency_name"=>$agency['agency_name'],
						"booking_creation_date"=>$today,
						"booking_source"=>5,
						"booking_status"=>'Quote',
						"booking_final_voucher_date"=>"",
						"booking_final_payment_date"=>"",
						"booking_services_start_date"=>"",
						"booking_services_end_date"=>"",
						"booking_agency_consultant_code"=>"",
						"booking_agency_consultant_name"=>""
						);
					$db->booking->insert($insertData);
					$itemID = $tools->getNextID("item");
					$hotelInfo = $db->hp_hotels->findOne(array("HotelCode"=>$hotelsMaked->hotelBookingInfo->hotelCode),array("_id"=>0));
					$roomStr = "";
					$isFirst = true;
					$adultMax = 0;
					$childMax = 0;
					$guestData = array();
					$isFirstGuest = true;
					foreach($hotelsMaked->hotelBookingInfo->rooms as $key => $val){
						if($isFirst){
							$roomStr .= $val->roomCategory;
							$isFirst=false;
						}else{
							$roomStr .= " & ".$val->roomCategory;
						}
						foreach($val->paxes as $key2 => $val2){
							if($val2->paxType =="Adult"){
								$adultMax++;
							}
							if($val2->paxType =="Child"){
								$childMax++;
							}
							$guest= array(
									"guest_title" => $val2->title,
									"guest_last_name" => $val2->lastName,
									"guest_agency_code" => $agency['agency_code'],
									"guest_agency_name" => $agency['agency_name'],
									"guest_first_name" => $val2->firstName,
									"guest_age" => (int)$val2->age
								);
							$guestCollection = $db->guest->findOne($guest);
							if($guestCollection){
								if($isFirstGuest){
									
									$lead = 1;
									$isFirstGuest = false;
								}else{
									$lead = 0;
								}
								$guestPax = array(
									"item_id" => (int)$itemID,
									"guest_id" => (string)$guestCollection['_id'],
									"guest_title" => $guestCollection['guest_title'],
									"guest_last_name" => $guestCollection['guest_last_name'],
									"guest_agency_code" => $guestCollection['guest_agency_code'],
									"guest_agency_name" => $guestCollection['guest_agency_name'],
									"guest_business" => $guestCollection['guest_business'],
									"guest_fax" => $guestCollection['guest_fax'],
									"guest_first_name" =>$guestCollection['guest_first_name'],
									"guest_home" => $guestCollection['guest_home'],
									"guest_mobile" => $guestCollection['guest_mobile'],
									"guest_age" =>$guestCollection['guest_age'],
									"guest_is_lead" =>$lead
								);
								$db->item_guest->insert($guestPax);
								unset($guestPax['guest_is_lead']);
								$guestPax['booking_id'] = (int)$bookingID;
								$db->booking_guest->insert($guestPax);
							}else{
								$db->guest->insert($guest);
								$guestCollection2 = $db->guest->findOne($guest);
								if($isFirstGuest){
									$lead = 1;
									$isFirstGuest = false;

								}else{
									$lead = 0;
								}
								$guestPax = array(
									"item_id" => (int)$itemID,
									"guest_id" => (string)$guestCollection2['_id'],
									"guest_title" => $guestCollection2['guest_title'],
									"guest_last_name" => $guestCollection2['guest_last_name'],
									"guest_agency_code" => $guestCollection2['guest_agency_code'],
									"guest_agency_name" => $guestCollection2['guest_agency_name'],
									"guest_business" => $guestCollection2['guest_business'],
									"guest_fax" => $guestCollection2['guest_fax'],
									"guest_first_name" =>$guestCollection2['guest_first_name'],
									"guest_home" => $guestCollection2['guest_home'],
									"guest_mobile" => $guestCollection2['guest_mobile'],
									"guest_age" =>$guestCollection2['guest_age'],
									"guest_is_lead" =>$lead
								);
								$db->item_guest->insert($guestPax);
								unset($guestPax['guest_is_lead']);
								$guestPax['booking_id'] = (int)$bookingID;
								$db->booking_guest->insert($guestPax);
							}
							
							
						}
						
					}
					$conditionStr = "";
					$testPolicy =array();
					foreach($hotelsMaked->hotelBookingInfo->cancellationPolicy as $key => $val){
						if(!in_array($val->remarks, $testPolicy)){
							$conditionStr .= "<p>".$val->remarks."</p>";
							array_push($testPolicy,$val->remarks);
						}
						
					}

					$d1=new DateTime($hotelsMaked->hotelBookingInfo->checkIn);
					$ts1 = $d1->getTimestamp();
					$startDate = new MongoDate($ts1);

					$d2=new DateTime($hotelsMaked->hotelBookingInfo->checkOut);
					$ts2 = $d2->getTimestamp();
					$endDate = new MongoDate($ts2);


					$resCurr = $db->currency_ex->findOne(array("curr_from"=> $hotelsMaked->hotelBookingInfo->currency), array("_id"=>0));

					$originalAmount = (float)$hotelsMaked->hotelBookingInfo->totalPrice;
					$orginalNetAmt = (float)$hotelsMaked->hotelBookingInfo->totalPrice*(float)$resCurr['rate_to'];
					$netAmountInAgency = round($orginalNetAmt);
					$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
					$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
					$commInAmountTemp = $grossAmt*(SYS_COMM/100);
					$commAmt = round($commInAmountTemp);
					$discInAmountTemp = $grossAmt*(SYS_DISC/100);
					$discAmt = round($discInAmountTemp);
					$totalLessAMOUNTT = round($commAmt+$discAmt);
					$totaldueamt = $grossAmt-$totalLessAMOUNTT;
					$totalProfitamt = $totaldueamt-$netAmountInAgency;
					$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
					$gstAmt = $totalProfitamt*(SYS_GST/100);
					$detailsITEM = array(
						"itemId"=> (int)$itemID,
						"bookingId"=> (int)$bookingID,
						"itemCode"=> "HP-".$hotelsMaked->hotelBookingInfo->hotelCode,
						"itemName"=> $hotelInfo['HotelName'],
						"itemPhone"=> $hotelInfo['HotelPhoneNumber'],
						"itemRatings"=> (int)$hotelInfo['StarRating'], 
						"itemPropertyCategory"=> '',
						"itemSeq"=> 0,
						"itemStockType"=> '',
						"itemStockTypeName"=> '',
						"itemSupplierCode"=> 'Hotels Pro Euro',
						"itemSupplierName"=> 'Hotels Pro Euro',
						"itemServiceCode"=> 'ACC',
						"itemServiceName"=> 'Accommodation',
						"itemSumarryDescription"=> "<p>".$hotelInfo['HotelInformation']['HotelDescription']."</p><p>".$hotelInfo['HotelInformation']['HotelLocation']."</p>",
						"itemDetailedDescription"=> '',
						"itemCostingsDescription"=> '',
						"itemInclusionDescription"=> $hotelsMaked->hotelBookingInfo->boardType." (".$roomStr.")",
						"itemExclusionDescription"=> '',
						"itemConditionsDescription"=> '',
						"itemStatus"=> 'Quote',
						"itemSuplierConfirmation"=> $hotelsMaked->trackingId.'|'.$hotelsMaked->hotelBookingInfo->confirmationNumber,
						"itemSuplierResReq" =>$hotelsMaked->trackingId,
						"itemFromCountry"=> $hotelInfo['Country'],
						"itemFromCity"=> $hotelInfo['City'],
						"itemFromAddress"=> $hotelInfo['HotelAddress'],
						"itemToCountry"=> '',
						"itemToCity"=> '',
						"itemToAddress"=> '',
						"itemStartDate"=> $startDate,
						"itemEndDate"=> $endDate,
						"itemStartTime"=>'00:00',
						"itemEndTime"=>'00:00',
						"itemIsLeadPax"=> 1,
						"itemIsFullPax"=> 1,
						"itemIsVoucherable"=> 1,
						"itemIsAutoInvoice"=> 1,
						"itemIsInvoicable"=> 1,
						"itemIsIterary"=> 1,
						"itemIsGstApplicable"=> 0,
						"itemAdultMin"=>$adultMax,
						"itemAdultMax"=>$adultMax,
						"itemChildMin"=>$childMax,
						"itemChildMax"=>$childMax,
						"itemInfantMin"=>0,
						"itemInfantMax"=>0,
						"itemIsCancelled"=>0,
						"itemIsPaxAllocated"=>1,
						"itemIsReceipt"=>0,
						"itemIsInvoiced"=>0,
						"itemIsPaid"=>0,
						"itemIsConfirmed"=>0,
						"itemIsBlank"=> 0,
						"itemIsLive"=>1,
						"itemCancellation"=>array(),
						"itemSupplements"=>array(),
						"itemCostings"=> array(
							"fromCurrency"=> $resCurr['curr_from'],
							"fromRate"=> 1,
							"toCurrency"=> $resCurr['curr_to'],
							"toRate"=> $resCurr['rate_to'],
							"originalAmount"=> $originalAmount,
							"orginalNetAmt"=> $orginalNetAmt,
							"netAmountInAgency"=> $netAmountInAgency,
							"markupInAmount"=>$markupInAmountTemp,
							"markupInPct"=>SYS_MARKUP,
							"grossAmt"=> $grossAmt,
							"commInPct"=>SYS_COMM,
							"commInAmount"=>$commInAmountTemp,
							"commAmt"=>$commAmt,
							"discInPct"=>SYS_DISC,
							"discInAmount"=>$discInAmountTemp,
							"discAmt"=>$discAmt,
							"totalLessAmt"=> $totalLessAMOUNTT,
							"totalDueAmt"=> $totaldueamt,
							"totalProfitAmt"=> $totalProfitamt,
							"gstInPct"=>SYS_GST,
							"gstInAmount"=>$gstInAmountTemp,
							"gstAmt"=>$gstAmt


						)

					);

					
					

					foreach($hotelsMaked->hotelBookingInfo->cancellationPolicy as $key => $val){
						if((int)$val->cancellationDay>=250){
							$cancelData = array(
								"dateFrom"=>$today,
								"dateTo"=>$startDate,
								"typeCancellation"=>"Percentage",
								"percent" =>100,
								"amount"=>0
								);
							array_push($detailsITEM["itemCancellation"],$cancelData);
						
						}elseif((int)$val->cancellationDay>0 && (int)$val->cancellationDay<250){
							$d1=new DateTime($hotelsMaked->hotelBookingInfo->checkIn);
							$d1->sub(new DateInterval('P'.$val->cancellationDay.'D'));
							$ts1 = $d1->getTimestamp();
							$dateFrom = new MongoDate($ts1);
							if($val->feeType == 'Percent'){
								$cancelData = array(
									"dateFrom"=>$dateFrom,
									"dateTo"=>$startDate,
									"typeCancellation"=>"Percentage",
									"percent" =>100,
									"amount"=>0
									);
								array_push($detailsITEM["itemCancellation"],$cancelData);
							}else{


								$amountCancelTemp = ((float)$val->feeAmount*(float)$resCurr['rate_to']);
								$amountCancelTempTemp = $amountCancelTemp*(SYS_CANCEL_PCT/100);
								$amountCancel = $amountCancelTemp+$amountCancelTempTemp;
								$cancelData = array(
									"dateFrom"=>$dateFrom,
									"dateTo"=>$startDate,
									"typeCancellation"=>"Amount",
									"percent" =>0,
									"amount"=>$amountCancel
									);
								array_push($detailsITEM["itemCancellation"],$cancelData);
							}
						}
						
						
					}
					$db->items->insert($detailsITEM);

					

				}else{//if booking id exist
					$bookingID = (int)$_POST['bookingID'];
					$itemID = $tools->getNextID("item");
					$hotelInfo = $db->hp_hotels->findOne(array("HotelCode"=>$hotelsMaked->hotelBookingInfo->hotelCode),array("_id"=>0));
					$roomStr = "";
					$isFirst = true;
					$adultMax = 0;
					$childMax = 0;
					$guestData = array();
					$isFirstGuest = true;
					foreach($hotelsMaked->hotelBookingInfo->rooms as $key => $val){
						if($isFirst){
							$roomStr .= $val->roomCategory;
							$isFirst=false;
						}else{
							$roomStr .= " & ".$val->roomCategory;
						}
						foreach($val->paxes as $key2 => $val2){
							if($val2->paxType =="Adult"){
								$adultMax++;
							}
							if($val2->paxType =="Child"){
								$childMax++;
							}
							$guest= array(
									"guest_title" => $val2->title,
									"guest_last_name" => $val2->lastName,
									"guest_agency_code" => $agency['agency_code'],
									"guest_agency_name" => $agency['agency_name'],
									"guest_first_name" => $val2->firstName,
									"guest_age" => (int)$val2->age
								);
							$guestCollection = $db->guest->findOne($guest);
							if($guestCollection){
								if($isFirstGuest){
									
									$lead = 1;
									$isFirstGuest = false;
								}else{
									$lead = 0;
								}
								$guestPax = array(
									"item_id" => (int)$itemID,
									"guest_id" => (string)$guestCollection['_id'],
									"guest_title" => $guestCollection['guest_title'],
									"guest_last_name" => $guestCollection['guest_last_name'],
									"guest_agency_code" => $guestCollection['guest_agency_code'],
									"guest_agency_name" => $guestCollection['guest_agency_name'],
									"guest_business" => $guestCollection['guest_business'],
									"guest_fax" => $guestCollection['guest_fax'],
									"guest_first_name" =>$guestCollection['guest_first_name'],
									"guest_home" => $guestCollection['guest_home'],
									"guest_mobile" => $guestCollection['guest_mobile'],
									"guest_age" =>$guestCollection['guest_age'],
									"guest_is_lead" =>$lead
								);
								$db->item_guest->insert($guestPax);
								unset($guestPax['guest_is_lead']);
								$guestPax['booking_id'] = (int)$bookingID;
								$db->booking_guest->insert($guestPax);
							}else{
								$db->guest->insert($guest);
								$guestCollection2 = $db->guest->findOne($guest);
								if($isFirstGuest){
									$lead = 1;
									$isFirstGuest = false;

								}else{
									$lead = 0;
								}
								$guestPax = array(
									"item_id" => (int)$itemID,
									"guest_id" => (string)$guestCollection2['_id'],
									"guest_title" => $guestCollection2['guest_title'],
									"guest_last_name" => $guestCollection2['guest_last_name'],
									"guest_agency_code" => $guestCollection2['guest_agency_code'],
									"guest_agency_name" => $guestCollection2['guest_agency_name'],
									"guest_business" => $guestCollection2['guest_business'],
									"guest_fax" => $guestCollection2['guest_fax'],
									"guest_first_name" =>$guestCollection2['guest_first_name'],
									"guest_home" => $guestCollection2['guest_home'],
									"guest_mobile" => $guestCollection2['guest_mobile'],
									"guest_age" =>$guestCollection2['guest_age'],
									"guest_is_lead" =>$lead
								);
								$db->item_guest->insert($guestPax);
								unset($guestPax['guest_is_lead']);
								$guestPax['booking_id'] = (int)$bookingID;
								$db->booking_guest->insert($guestPax);
							}
							
							
						}
						
					}
					$conditionStr = "";
					$testPolicy =array();
					foreach($hotelsMaked->hotelBookingInfo->cancellationPolicy as $key => $val){
						if(!in_array($val->remarks, $testPolicy)){
							$conditionStr .= "<p>".$val->remarks."</p>";
							array_push($testPolicy,$val->remarks);
						}
						
					}

					$d1=new DateTime($hotelsMaked->hotelBookingInfo->checkIn);
					$ts1 = $d1->getTimestamp();
					$startDate = new MongoDate($ts1);

					$d2=new DateTime($hotelsMaked->hotelBookingInfo->checkOut);
					$ts2 = $d2->getTimestamp();
					$endDate = new MongoDate($ts2);


					$resCurr = $db->currency_ex->findOne(array("curr_from"=> $hotelsMaked->hotelBookingInfo->currency), array("_id"=>0));

					$originalAmount = (float)$hotelsMaked->hotelBookingInfo->totalPrice;
					$orginalNetAmt = (float)$hotelsMaked->hotelBookingInfo->totalPrice*(float)$resCurr['rate_to'];
					$netAmountInAgency = round($orginalNetAmt);
					$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
					$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
					$commInAmountTemp = $grossAmt*(SYS_COMM/100);
					$commAmt = round($commInAmountTemp);
					$discInAmountTemp = $grossAmt*(SYS_DISC/100);
					$discAmt = round($discInAmountTemp);
					$totalLessAMOUNTT = round($commAmt+$discAmt);
					$totaldueamt = $grossAmt-$totalLessAMOUNTT;
					$totalProfitamt = $totaldueamt-$netAmountInAgency;
					$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
					$gstAmt = $totalProfitamt*(SYS_GST/100);
					$detailsITEM = array(
						"itemId"=> (int)$itemID,
						"bookingId"=> (int)$bookingID,
						"itemCode"=> "HP-".$hotelsMaked->hotelBookingInfo->hotelCode,
						"itemName"=> $hotelInfo['HotelName'],
						"itemPhone"=> $hotelInfo['HotelPhoneNumber'],
						"itemRatings"=> (int)$hotelInfo['StarRating'], 
						"itemPropertyCategory"=> '',
						"itemSeq"=> 0,
						"itemStockType"=> '',
						"itemStockTypeName"=> '',
						"itemSupplierCode"=> 'Hotels Pro Euro',
						"itemSupplierName"=> 'Hotels Pro Euro',
						"itemServiceCode"=> 'ACC',
						"itemServiceName"=> 'Accommodation',
						"itemSumarryDescription"=> "<p>".$hotelInfo['HotelInformation']['HotelDescription']."</p><p>".$hotelInfo['HotelInformation']['HotelLocation']."</p>",
						"itemDetailedDescription"=> '',
						"itemCostingsDescription"=> '',
						"itemInclusionDescription"=> $hotelsMaked->hotelBookingInfo->boardType." (".$roomStr.")",
						"itemExclusionDescription"=> '',
						"itemConditionsDescription"=> $conditionStr,
						"itemStatus"=> 'Quote',
						"itemSuplierConfirmation"=> $hotelsMaked->trackingId.'|'.$hotelsMaked->hotelBookingInfo->confirmationNumber,
						"itemSuplierResReq" =>$hotelsMaked->trackingId,
						"itemFromCountry"=> $hotelInfo['Country'],
						"itemFromCity"=> $hotelInfo['City'],
						"itemFromAddress"=> $hotelInfo['HotelAddress'],
						"itemToCountry"=> '',
						"itemToCity"=> '',
						"itemToAddress"=> '',
						"itemStartDate"=> $startDate,
						"itemEndDate"=> $endDate,
						"itemStartTime"=>'00:00',
						"itemEndTime"=>'00:00',
						"itemIsLeadPax"=> 1,
						"itemIsFullPax"=> 1,
						"itemIsVoucherable"=> 1,
						"itemIsAutoInvoice"=> 1,
						"itemIsInvoicable"=> 1,
						"itemIsIterary"=> 1,
						"itemIsGstApplicable"=> 0,
						"itemAdultMin"=>0,
						"itemAdultMax"=>$adultMax,
						"itemChildMin"=>0,
						"itemChildMax"=>$childMax,
						"itemInfantMin"=>0,
						"itemInfantMax"=>0,
						"itemIsCancelled"=>0,
						"itemIsPaxAllocated"=>1,
						"itemIsReceipt"=>0,
						"itemIsInvoiced"=>0,
						"itemIsPaid"=>0,
						"itemIsConfirmed"=>0,
						"itemIsBlank"=> 0,
						"itemIsLive"=>1,
						"itemCancellation"=>array(),
						"itemCostings"=> array(
							"fromCurrency"=> $resCurr['curr_from'],
							"fromRate"=> 1,
							"toCurrency"=> $resCurr['curr_to'],
							"toRate"=> $resCurr['rate_to'],
							"originalAmount"=> $originalAmount,
							"orginalNetAmt"=> $orginalNetAmt,
							"netAmountInAgency"=> $netAmountInAgency,
							"markupInAmount"=>$markupInAmountTemp,
							"markupInPct"=>SYS_MARKUP,
							"grossAmt"=> $grossAmt,
							"commInPct"=>SYS_COMM,
							"commInAmount"=>$commInAmountTemp,
							"commAmt"=>$commAmt,
							"discInPct"=>SYS_DISC,
							"discInAmount"=>$discInAmountTemp,
							"discAmt"=>$discAmt,
							"totalLessAmt"=> $totalLessAMOUNTT,
							"totalDueAmt"=> $totaldueamt,
							"totalProfitAmt"=> $totalProfitamt,
							"gstInPct"=>SYS_GST,
							"gstInAmount"=>$gstInAmountTemp,
							"gstAmt"=>$gstAmt


						)

					);

					
					

					foreach($hotelsMaked->hotelBookingInfo->cancellationPolicy as $key => $val){
						if((int)$val->cancellationDay>=250){
							$cancelData = array(
								"dateFrom"=>$today,
								"dateTo"=>$startDate,
								"typeCancellation"=>"Percentage",
								"percent" =>100,
								"amount"=>0
								);
							array_push($detailsITEM["itemCancellation"],$cancelData);
						
						}elseif((int)$val->cancellationDay>0 && (int)$val->cancellationDay<250){
							$d1=new DateTime($hotelsMaked->hotelBookingInfo->checkIn);
							$d1->sub(new DateInterval('P'.$val->cancellationDay.'D'));
							$ts1 = $d1->getTimestamp();
							$dateFrom = new MongoDate($ts1);
							if($val->feeType == 'Percent'){
								$cancelData = array(
									"dateFrom"=>$dateFrom,
									"dateTo"=>$startDate,
									"typeCancellation"=>"Percentage",
									"percent" =>100,
									"amount"=>0
									);
								array_push($detailsITEM["itemCancellation"],$cancelData);
							}else{


								$amountCancelTemp = ((float)$val->feeAmount*(float)$resCurr['rate_to']);
								$amountCancelTempTemp = $amountCancelTemp*(SYS_CANCEL_PCT/100);
								$amountCancel = $amountCancelTemp+$amountCancelTempTemp;
								$cancelData = array(
									"dateFrom"=>$dateFrom,
									"dateTo"=>$startDate,
									"typeCancellation"=>"Amount",
									"percent" =>0,
									"amount"=>$amountCancel
									);
								array_push($detailsITEM["itemCancellation"],$cancelData);
							}
						}
						
						
					}
					$db->items->insert($detailsITEM);

				}
				$response = array ( "status" => true,"booking_id"=>$bookingID);
				print json_encode($response);
			}else{
				$response = array ( "status" => false);
  	  		
	  	  		print json_encode($response);
			}
			$connection->close();
			exit;

		break; 
		}
		case "getCancellationPolicy":{
			$hpObj = new HotelsProClient();
			$cancelPolicy = $hpObj->get_cancellation_policy($_POST['processID']);
			$cancellationString = "Refundable";
			if($cancelPolicy->trackingId){
				foreach($cancelPolicy->cancellationPolicy as $key=>$val){
					if((int)$val->cancellationDay>=250){
						$cancellationString = "Non-refundable";
						break;
					}
				}
				$response = array("status"=>true,"remarks"=>$cancellationString);
			}else{

				$response = array("status"=>false,"remarks"=>"");
			}
			print json_encode($response);
		exit;

		break; 
		}
		/* ENDLOGIN*/
	}

?>