<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateMainDestination":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			
			

			
		
			$id = $_POST['main_destination_id'];
			
				
			
			
			
			
			if($id==0){
				
				
			
				$insertData = array();

				$insertData['DestinationCode'] = $_POST['DestinationCode'];
				$insertData['CountryName'] = $_POST['CountryName'];
				$insertData['CityName'] = $_POST['CityName'];
				$insertData['Subdivision'] = $_POST['Subdivision'];
				$insertData['is_active'] = $_POST['main_des_is_active'];

				$res = $db->sys_destination->insert($insertData);
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
			
			}else{

				

				$mongoID = new MongoID($_POST['main_destination_id']);
				$updateData = array();

				$updateData['DestinationCode'] = $_POST['DestinationCode'];
				$updateData['CountryName'] = $_POST['CountryName'];
				$updateData['CityName'] = $_POST['CityName'];
				$updateData['Subdivision'] = $_POST['Subdivision'];
				$updateData['is_active'] = $_POST['main_des_is_active'];

				$res = $db->sys_destination->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			
			
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("DestinationCode"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("CountryName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("Subdivision"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("CityName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			//print_r($whereData);
			$dataCollections = $db->sys_destination->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(1000);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_main_destination'] = $key;
				$data['DestinationCode'] = $row['DestinationCode'];
				$data['CountryName'] = $row['CountryName'];
				$data['Subdivision'] = $row['Subdivision'];
				$data['CityName'] = $row['CityName'];
				$data['is_active'] = $row['is_active'];

			 	array_push($arrayResults,$data);
			}

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewMainDestination" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['main_destination_id']);
			
			$db->sys_destination->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> "Y")));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['main_destination_id']);
			
			$db->sys_destination->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> "N")));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		
		/* ENDLOGIN*/
	}
?>