<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateLWDestination":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			
			

			
		
			$id = $_POST['lowcostbeds_destination_id'];
			
				
			

			
			if($id==0){
				
				
			
				$insertData = array();

				
				$insertData['CountryID'] = $_POST['CountryID'];
				$insertData['Country'] = $_POST['Country'];
				$insertData['RegionID'] = $_POST['RegionID'];
				$insertData['Region'] = $_POST['Region'];
				$insertData['ResortID'] = $_POST['ResortID'];
				$insertData['Resort'] = $_POST['Resort'];
			

				$res = $db->lw_destination->insert($insertData);
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
			
			}else{

				

				$mongoID = new MongoID($_POST['lowcostbeds_destination_id']);
				$updateData = array();

				$updateData['CountryID'] = $_POST['CountryID'];
				$updateData['Country'] = $_POST['Country'];
				$updateData['RegionID'] = $_POST['RegionID'];
				$updateData['Region'] = $_POST['Region'];
				$updateData['ResortID'] = $_POST['ResortID'];
				$updateData['Resort'] = $_POST['Resort'];
			
				
				

				$res = $db->lw_destination->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
		
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("Country"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("Region"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("Resort"=> new MongoRegex("/^".$_GET["query"]."/i")));
				
			}
			//print_r($whereData);
			$dataCollections = $db->lw_destination->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(1000);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_lw_destination'] = $key;
				$data['CountryID'] = $row['CountryID'];
				$data['Country'] = $row['Country'];
				$data['RegionID'] = $row['RegionID'];
				$data['Region'] = $row['Region'];
				$data['ResortID'] = $row['ResortID'];
				$data['Resort'] = $row['Resort'];

			 	array_push($arrayResults,$data);
			}

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewLWDestination" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		
		
		/* ENDLOGIN*/
	}
?>