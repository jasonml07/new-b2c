<?php
session_start();
require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "addAgency":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			$resPOST = json_decode($_POST['postDetails'], true);

			$id = $resPOST['agency_id'];



			if($id == 0){
				
				unset($resPOST['agency_id']);
				$tool = new Tools();
				$resPOST['agency_code'] = $tool->codeGeneRatorAndChecker("agency",$resPOST['agency_code']);
				$res = $db->agency->insert($resPOST);
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			}else{

				$mongoID = new MongoID($resPOST['agency_id']);


				unset($resPOST['agency_id']);
				
				

				$res = $db->agency->update(array("_id"=> $mongoID), array("\$set" => $resPOST));
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}	
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}

		case "view": {

			$connection = new MongoClient();
			$db = $connection->db_system;
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['agency_name'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			$dataCollections = $db->agency->find($whereData);

			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_agency'] = $key;
				$data['agency_code'] = $row['agency_code'];
				$data['agency_name'] = $row['agency_name'];
				$data['is_active'] = $row['is_active'];
				$data['address'] = $row['address'];
				$data['city'] = $row['city'];
				$data['state'] = $row['state'];
				$data['country'] = $row['country'];
				$data['email'] = $row['email'];
				$data['phone'] = $row['phone'];
				$data['fax'] = $row['fax'];
				$data['postCode'] = $row['postCode'];
				$data['b_first_name'] = $row['b_first_name'];
				$data['b_last_name'] = $row['b_last_name'];
				$data['b_address'] = $row['b_address'];
				$data['b_city'] = $row['b_city'];
				$data['b_state'] = $row['b_state'];
				$data['b_country'] = $row['b_country'];
				$data['b_email'] = $row['b_email'];
				$data['b_fax'] = $row['b_fax'];
				$data['currency'] = $row['currency'];

				$dataCollectionsResults2 = $db->agency_consultants->find(array("agency_code" => $row['agency_code']),array("_id" => 0));
				try {
			  	 	$dataArray = iterator_to_array($dataCollectionsResults2);
				}catch (Exception $e){
					$dataArray = array();

				}
				$data['agency_consultant'] = $dataArray;
				array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewAgency" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;

  	  	break; 
		}

		case "getAgencyDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_SESSION['sys_agency_id_B2B']);
			$res = $db->agency->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['agency_id'] = $_SESSION['sys_agency_id_B2B'];
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['id_agency']);
			
			$db->agency->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 1)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['id_agency']);
			
			$db->agency->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 0)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}

	}
?>