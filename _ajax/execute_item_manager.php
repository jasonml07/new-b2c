<?php
session_start();

require_once('../_classes/tools.class.php'); 
require_once('../_classes/item.class.php'); 
require_once('../_classes/hotelBeds.class.php');
require_once('../_classes/paymentWestpac.class.php');
require_once('../_classes/_westPac_config.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "addItemToBooking":{

			$resPOST = json_decode($_POST['postDetails'], true);
			
			$tools = new Tools; 
			
			if(isset($resPOST['itemId']) && $resPOST['itemId']=='New'){

				//print_r($resPOST);

				$itemID = $tools->getNextID("item");
				$resPOST['itemId'] = (int)$itemID;
				$resPOST['bookingId'] = (int)$resPOST['bookingId'];

				if($resPOST['itemStartDate'] !=''){
					
					$xDate = explode("/", $resPOST['itemStartDate']);
					$resPOST['itemStartDate'] = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 12:00:00"));
				}
				if($resPOST['itemEndDate'] !=''){
					
					$xDate = explode("/", $resPOST['itemEndDate']);
					$resPOST['itemEndDate'] = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 12:00:00"));
				}
				
				//print_r($resPOST['itemCancellation']);
				//die();
				foreach($resPOST['itemCancellation'] as $key2 => $row2){
					$resPOST['itemCancellation'][$key2]["dateFrom"] = new MongoDate(strtotime($row2['dateFrom']." 12:00:00"));
					
					$resPOST['itemCancellation'][$key2]["dateTo"] = new MongoDate(strtotime( $row2['dateTo']." 12:00:00"));


				}


				unset($resPOST['action']);
				$connection = new MongoClient();
				$db = $connection->db_system;

				$db->items->insert($resPOST);
				$response =  array("status"=>true,"booking_id"=> $resPOST['bookingId']);
				print json_encode($response);
				$connection->close();
				

			}else{
				//print_r($resPOST);
				

				$connection = new MongoClient();
				$db = $connection->db_system;
				$resPOST['itemId'] = (int)$resPOST['itemId'];
				$resPOST['bookingId'] = (int)$resPOST['bookingId'];

				$oldItemData = $db->items->findOne(array("itemId"=>(int)$resPOST['itemId']));
				if($resPOST['itemStartDate'] !=''){
					$xDate = explode("/", $resPOST['itemStartDate']);
					$resPOST['itemStartDate'] = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 12:00:00"));
				}
				if($resPOST['itemEndDate'] !=''){

					$xDate = explode("/", $resPOST['itemEndDate']);
					$dt = new DateTime($xDate[2]."-".$xDate[1]."-".$xDate[0]);
				
					$ts = $dt->getTimestamp();
					$resPOST['itemEndDate'] = new MongoDate($ts);
				}

				
				foreach($resPOST['itemCancellation'] as $key2 => $row2){
					$xDate = explode("/", $resPOST['itemEndDate']);
					$resPOST['itemEndDate'] = new MongoDate(strtotime($xDate[2]."-".$xDate[1]."-".$xDate[0]." 12:00:00"));


				}
				unset($resPOST['action']);
				//print_r($resPOST);
				$db->items->update(array('itemId'=>$resPOST['itemId']),array('$set' =>$resPOST));
				

				$item = new Item();

				$item->reEvaluate($resPOST['itemId'],$oldItemData,$resPOST);

				$response =  array("status"=>true,"booking_id"=> $resPOST['bookingId']);
				print json_encode($response);
				$connection->close();
			}
			
			

			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0);

			if(isset($_GET['query'])){
				if($_GET['query'] =='onlyNonAllocated'){
					$whereData['itemIsPaxAllocated'] = 0;

				}
				if($_GET['query'] =='onlyNonFee'){
					$whereData['itemServiceCode'] = array("\$ne"=>"SFEE");

				}
				if($_GET['query'] =='allowCancelled'){
					unset($whereData['itemIsCancelled']);
				}

			}
			//print_r($whereData);
			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0))->sort(array('itemSeq' => 1));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		
			   		
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   $dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;

					foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}

					$strFrom = strtotime($row['otherDetails']['dateTimeCreated']);
					$dateNow = date('d-m-Y H:i:s');
					$strTo = strtotime($dateNow);

					
					
					$seconds_diff = $strTo - $strFrom;
					$milliseconds_diff = $seconds_diff * 1000;

					if((float)$milliseconds_diff<(float)$row['otherDetails']['expireTime']){
						$timeExpiry = (float)$row['otherDetails']['expireTime'] - (float)$milliseconds_diff ;
					}else{
						$timeExpiry = 0;
					}
					
					$dataArray[$key]['timeExpiry'] = $timeExpiry;
			   }

			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetails" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "viewToInvoice":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0,"itemIsInvoiced"=>0,"itemIsPaxAllocated"=>1,"itemIsConfirmed"=>1);

			if(isset($_GET['query'])){
				if($_GET['query'] =='onlyNonAllocated'){
					$whereData['itemIsPaxAllocated'] = 0;

				}

			}

			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   $dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;
					foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}
			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetailsToInvoice" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "viewToCostings":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0,"itemIsPaxAllocated"=>1);

			if(isset($_GET['query'])){
				if($_GET['query'] =='onlyNonAllocated'){
					$whereData['itemIsPaxAllocated'] = 0;

				}

			}

			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){

			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   $dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;
					foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}

			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetailsToCostings" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "viewToItinerary":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0,"itemIsPaxAllocated"=>1,"itemIsVoucherable"=>1);

			if(isset($_GET['query'])){
				if($_GET['query'] =='onlyNonAllocated'){
					$whereData['itemIsPaxAllocated'] = 0;

				}

			}

			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   $dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;
					foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}

			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetailsToItinerary" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "viewToQuote":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0,"itemIsVoucherable"=>1);

			if(isset($_GET['query'])){
				if($_GET['query'] =='onlyNonAllocated'){
					$whereData['itemIsPaxAllocated'] = 0;

				}

			}

			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   $dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;
					foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}

			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetailsToQuote" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "viewToPayment":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0,"itemIsPaxAllocated"=>1,"itemIsConfirmed"=>1,"itemIsPaid"=>0,"itemIsReceipt"=>0);

			if(isset($_GET['query'])){
				if($_GET['query'] =='onlyNonAllocated'){
					$whereData['itemIsPaxAllocated'] = 0;

				}

			}

			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   //$dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;

					$dataCollectionsResults3 = $db->item_payments->find($whereData);
					$totalAmountPaid = 0;
					try {
						$dataArray3 = iterator_to_array($dataCollectionsResults3);
						 foreach($dataArray3 as $key3 => $row3){
						 	$totalAmountPaid += (float)$row3['amount_paid'];

						 }
					} catch (Exception $e) {
					   //$dataArray = array();
					}
					//print_r($row['itemCostings']);
					$dataArray[$key]['itemOutStanding'] = (float)$row['itemCostings']['totalDueAmt']-$totalAmountPaid;
			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetailsToPayment" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}


		case "viewToReceipt":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0,"itemIsPaxAllocated"=>1,"itemIsConfirmed"=>1,"itemIsReceipt"=>0,"itemIsInvoiced"=>1);

			
			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}

			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   //$dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;

					$dataCollectionsResults3 = $db->item_receipt->find(array('item_id'=>(int)$row['itemId'],"is_refunded"=>0));
					$totalAmountPaid = 0;
					try {
						$dataArray3 = iterator_to_array($dataCollectionsResults3);
						 foreach($dataArray3 as $key3 => $row3){
						 	$totalAmountPaid += (float)$row3['amount_paid'];

						 }
					} catch (Exception $e) {
					   //$dataArray = array();
					}
					//print_r($row['itemCostings']);
					$dataArray[$key]['itemOutStandingReceipt'] = (float)$dataArray[$key]['itemCostings']['totalDueAmt']-$totalAmountPaid;
					$dataArray[$key]['itemTotalAmountReceipt'] = $totalAmountPaid;
			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetailsToReceipt" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "viewToVoucher":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('bookingId'=>(int)$_GET['booking_id'],"itemIsCancelled"=>0,"itemIsPaxAllocated"=>1,"itemIsConfirmed"=>1,"itemIsReceipt"=>1,"itemIsInvoiced"=>1,"itemIsVoucherable"=>1);

			
			$dataCollectionsResults = $db->items->find($whereData,array("_id"=>0));

			
			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   //$dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;


					$voucherArray = array();
			   		$whereData = array('itemId'=>(int)$row['itemId']);

					$dataCollectionsResults3 = $db->item_vouchers->find($whereData,array("_id"=>0));
					try {
						$dataArray3 = iterator_to_array($dataCollectionsResults3);
						 foreach($dataArray3 as $key3 => $row3){
						 	$row3['voucherId'] = $key3;
						 	array_push($voucherArray, $row3);

						 }
						// print_r($voucherArray);
					} catch (Exception $e) {
					   //$dataArray = array();
					}

					$dataArray[$key]['itemVouchers'] = $voucherArray;
					
			   }


			} catch (Exception $e) {
			   $dataArray = array();
			}
			


			//$dataArray = array($dataArray);
			//$dataArray = iterator_to_array($dataCollectionsResults);
			
			
			
			$response = array ( "success" => true, "total" => count($dataArray), "viewItemGridDetailsToVoucher" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "getItemDetailsSingle":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array('itemId'=>(int)$_GET['itemId']);

			$dataCollectionsResults = $db->items->findOne($whereData,array("_id"=>0));

			$dataCollectionsResults['action']='addItemToBooking';
			
			$dataCollectionsResults['itemStartDate'] = date('d/m/Y', $dataCollectionsResults['itemStartDate']->sec);
			$dataCollectionsResults['itemEndDate'] = date('d/m/Y', $dataCollectionsResults['itemEndDate']->sec);
			
			
			
			$response = array ( "status" => true, "itemDetails" => $dataCollectionsResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "reservedSelected":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			//var_dump(json_decode($_POST['itemIDs']));
			$whereData = array('bookingId'=>(int)$_POST['bookingID'],"itemIsCancelled"=>(int)0,"itemIsPaxAllocated"=>(int)1,"itemIsInvoiced"=>(int)0,"itemIsConfirmed"=>(int)0,"itemId"=>array("\$in"=>json_decode($_POST['itemIDs'])));

			

			$dataCollectionsResults = $db->items->find($whereData)->sort(array("itemSeq"=>1));

			try {
				$dataArray= iterator_to_array($dataCollectionsResults);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}
			//print_r($dataArray);
			$dataItemToInvoice = array();

			$message = "";
			foreach($dataArray as $key => $row){
				if($row['itemSupplierCode']!="HBBEDLIVE"){
					continue;
				}
				//only non refundable can be reserved
				if($row["otherDetails"]['cancellationType'] == "NRF"){
					continue;
				}

				foreach($row['itemSupplements'] as $key2 => $row2){
					if((int)$row2['supplement_qty'] >0){

						$row['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
						$row['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
						$row['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
						$row['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
						$row['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
						$row['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
						$row['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
						$row['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
						$row['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
						$row['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
						$row['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
							
					}
		            
				}
				
				//print_r($row);
				

				
				//echo $message;

				$hbObj = new HotelBedsClient();
				if($row['itemServiceCode'] =='ACC'){

					$strFrom = strtotime($row['otherDetails']['dateTimeCreated']);
					$dateNow = date('d-m-Y H:i:s');
					$strTo = strtotime($dateNow);
					$seconds_diff = $strTo - $strFrom;
					$milliseconds_diff = $seconds_diff * 1000;

					if((float)$milliseconds_diff<(float)$row['otherDetails']['expireTime']){
						$purchaseConfirm = $hbObj->purchaseConfirm($row);

						if(!(array)$purchaseConfirm->Purchase->ServiceList->Service->ErrorList->Error){

							$confimartion = (string)$purchaseConfirm->Purchase->Reference->IncomingOffice->attributes()->code."-".(string)$purchaseConfirm->Purchase->Reference->FileNumber;
							$db->items->update(array("itemId" => (int) $row['itemId']),array('$set' => array('itemIsConfirmed' => 1,"itemSuplierConfirmation"=>$confimartion)));

							$message .= "Item #".$row['itemId']." has been confirmed.</br>";


							if((int)$row['itemIsInvoicable'] == 1 && (int)$row['itemIsAutoInvoice'] == 1 ){
								array_push($dataItemToInvoice,$row);
								
							}
						}else{
							$message .= "Item #".$row['itemId']." unable to reserved.</br>";
						}

					}


					
				}else{
					//this is for transfer
					
				}

				
			}
			if(count($dataItemToInvoice) !=0){
				$itemObj = new Item();
				$isInvoiced = $itemObj->createInvoiced((int)$_POST['bookingID'],$dataItemToInvoice);

				if($isInvoiced){
					//print_r($dataItemToInvoice);
					foreach($dataItemToInvoice as $row){

						$db->items->update(
						    array("itemId" => (int)$row['itemId']),
						    array('$set' => array('itemIsInvoiced' => 1))
						);
						$message .= "Item #".$row['itemId']." has been invoiced.</br>";
						//echo $message;
					}
					
					
				}	
			}
			if($message !=""){
				$response = array ( "status" => true, "message" => $message,"booking_id"=>(int)$_POST['bookingID']);
			}else{
				$response = array ( "status" => false, "message" => "No Item has been reserved.");
			}
			
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "allSequence":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			

			

			$whereData = array('bookingId'=>(int)$_POST['bookingID']);

			

			$dataCollectionsResults = $db->items->find($whereData)->sort(array("itemStartDate"=>1));
			//print_r($dataCollectionsResults);
			try {
				$dataArray= iterator_to_array($dataCollectionsResults);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}
			//print_r($dataArray);
			$dataItemToInvoice = array();

			$message = "auto sequence";
			$ctr = 10;
			foreach($dataArray as $key => $row){
				if($row['itemServiceCode'] == "SFEE"){
					$db->items->update(
						    array("itemId" => (int)$row['itemId']),
						    array('$set' => array('itemSeq' => 1000))
						);
				}elseif($row['itemIsCancelled'] == (int)1){
					$db->items->update(
						    array("itemId" => (int)$row['itemId']),
						    array('$set' => array('itemSeq' => 2000))
						);
				}else{
					$db->items->update(
						    array("itemId" => (int)$row['itemId']),
						    array('$set' => array('itemSeq' => $ctr))
						);
					$ctr +=10;
				}
			}
			
			if($message !=""){
				$response = array ( "status" => true, "message" => $message,"booking_id"=>(int)$_POST['bookingID']);
			}else{
				$response = array ( "status" => false, "message" => "No Item has been sequeced.");
			}
			
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "reservedItem":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			

			

			$whereData = array('itemId'=>(int)$_POST['itemId']);

			

			$dataCollectionsResults = $db->items->find($whereData);
			//print_r($dataCollectionsResults);
			try {
				$dataArray= iterator_to_array($dataCollectionsResults);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}
			//print_r($dataArray);
			$dataItemToInvoice = array();

			$message = "";
			foreach($dataArray as $key => $row){
				if($row['itemSupplierCode']!="HBBEDLIVE"){
					continue;
				}
				//only non refundable can be reserved
				if($row["otherDetails"]['cancellationType'] == "NRF"){
					continue;
				}
				foreach($row['itemSupplements'] as $key2 => $row2){
					if((int)$row2['supplement_qty'] >0){

						$row['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
						$row['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
						$row['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
						$row['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
						$row['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
						$row['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
						$row['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
						$row['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
						$row['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
						$row['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
						$row['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
							
					}
		            
				}


				//print_r($row);
				

				
				//echo $message;

				$hbObj = new HotelBedsClient();
				if($row['itemServiceCode'] =='ACC'){

					$strFrom = strtotime($row['otherDetails']['dateTimeCreated']);
					$dateNow = date('d-m-Y H:i:s');
					$strTo = strtotime($dateNow);
					$seconds_diff = $strTo - $strFrom;
					$milliseconds_diff = $seconds_diff * 1000;

					if((float)$milliseconds_diff<(float)$row['otherDetails']['expireTime']){
						$purchaseConfirm = $hbObj->purchaseConfirm($row);

						if(!(array)$purchaseConfirm->Purchase->ServiceList->Service->ErrorList->Error){

							$confimartion = (string)$purchaseConfirm->Purchase->Reference->IncomingOffice->attributes()->code."-".(string)$purchaseConfirm->Purchase->Reference->FileNumber;
							$db->items->update(array("itemId" => (int) $row['itemId']),array('$set' => array('itemIsConfirmed' => 1,"itemSuplierConfirmation"=>$confimartion)));

							$message .= "Item #".$row['itemId']." has been confirmed.</br>";


							if((int)$row['itemIsInvoicable'] == 1 && (int)$row['itemIsAutoInvoice'] == 1 ){
								array_push($dataItemToInvoice,$row);
								
							}
						}else{
							$message .= "Item #".$row['itemId']." unable to reserved.</br>";
						}

					}


					
				}else{
					//this is for transfer
					
				}


			

			}

			if(count($dataItemToInvoice) !=0){
				$itemObj = new Item();
				$isInvoiced = $itemObj->createInvoiced((int)$_POST['bookingID'],$dataItemToInvoice);

				if($isInvoiced){
					//print_r($dataItemToInvoice);
					foreach($dataItemToInvoice as $row){

						$db->items->update(
						    array("itemId" => (int)$row['itemId']),
						    array('$set' => array('itemIsInvoiced' => 1))
						);
						$message .= "Item #".$row['itemId']." has been invoiced.</br>";
						//echo $message;
					}
					
					
				}	
			}
			if($message !=""){
				$response = array ( "status" => true, "message" => $message,"booking_id"=>(int)$_POST['bookingID']);
			}else{
				$response = array ( "status" => false, "message" => "No Item has been reserved.");
			}
			
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "cancelItem":{
			$tools = new Tools; 
			$connection = new MongoClient();
			$db = $connection->db_system;

			
			$today = new MongoDate(strtotime("now"));
			
			$ts = strtotime("now");
			$whereData = array('itemId'=>(int)$_POST['itemId']);

			

			$dataCollectionsResults = $db->items->findOne($whereData);

			$dataItem = array();
			if($dataCollectionsResults){

				//checkIF live
				if($dataCollectionsResults["itemIsLive"]==1){
					if($dataCollectionsResults["itemSupplierCode"] == 'HBBEDLIVE' && $dataCollectionsResults["itemIsConfirmed"] == 1){

						$hbObj = new HotelBedsClient();

						$purchaseCancel = $hbObj->purchaseCancel($dataCollectionsResults["itemSuplierConfirmation"]);

						if(!(array) $purchaseCancel->Purchase->Status && (string)$purchaseCancel->Purchase->Status != "CANCELLED"){
							$response = array ( "status" => false, "message" => "Unable to cancel this live product","booking_id"=>(int)$_POST['bookingID']);
			
  	  		
				  	  		print json_encode($response);
				  	  		$connection->close();
				  	  		exit;
						}
						
					}
				}

				//check cancellation policy

				$hasCancellation = false;
				if($dataCollectionsResults["itemIsConfirmed"] == 1){
					foreach($dataCollectionsResults['itemCancellation'] as $key => $row){
						/*
						1440633600
						1440806400
						1440630000
						*/
						if($row['dateFrom']->sec <= $ts && $row['dateTo']->sec >= $ts){
							$hasCancellation = true;
							 $itemID = $tools->getNextID("item");
							$dataItem = array(
							"itemId"=> $itemID,
							"bookingId"=> (int)$_POST['bookingID'],
							"itemCode"=> '',
							"itemName"=> 'Cancellation Fee',
							"itemPhone"=>'',
							"itemRatings"=> '',
							"itemPropertyCategory"=> '',
							"itemSeq"=> 0,
							"itemStockType"=> '',
							"itemStockTypeName"=> '',
							"itemServiceCode"=> 'SFEE',
							"itemServiceName"=> 'Service Fee',
							"itemSupplierCode"=> '',
							"itemSupplierName"=> '',
							"itemSumarryDescription"=> '',
							"itemDetailedDescription"=> '',
							"itemCostingsDescription"=> '',
							"itemInclusionDescription"=> '',
							"itemExclusionDescription"=> '',
							"itemConditionsDescription"=> '',
							"itemStatus"=> 'Quote',
							"itemSuplierConfirmation"=> '',
							"itemFromCountry"=> "",
							"itemFromCity"=> "",
							"itemFromAddress"=> "",
							"itemToCountry"=> '',
							"itemToCity"=> '',
							"itemToAddress"=> '',
							"itemStartDate"=> '',
							"itemEndDate"=> '',
							"itemStartTime"=>'00:00',
							"itemEndTime"=>'00:00',
							"itemIsLeadPax"=> 0,
							"itemIsFullPax"=> 0,
							"itemIsVoucherable"=> 0,
							"itemIsAutoInvoice"=> 1,
							"itemIsInvoicable"=> 1,
							"itemIsIterary"=> 0,
							"itemIsGstApplicable"=> 0,
							"itemAdultMin"=>0,
							"itemAdultMax"=>0,
							"itemChildMin"=>0,
							"itemChildMax"=>0,
							"itemInfantMin"=>0,
							"itemInfantMax"=>0,
							"itemIsCancelled"=>0,
							"itemIsPaxAllocated"=>1,
							"itemIsReceipt"=>0,
							"itemIsInvoiced"=>1,
							"itemIsPaid"=>0,
							"itemIsConfirmed"=>1,
							"itemIsBlank"=> 1,
							"itemIsLive"=>0,
							"itemCancellation"=>array(),
							"itemSupplements"=>array(),
							

						);
							if($row['typeCancellation'] =="Percentage"){
								//$dataItem["itemCostings"] = $dataCollectionsResults['itemCostings'];

								$dataItem["itemCostings"] =  array(
										"fromCurrency"=> 'AUD',
										"fromRate"=> 1,
										"toCurrency"=> 'AUD',
										"toRate"=> 1,
										"originalAmount"=> 0,
										"orginalNetAmt"=> 0,
										"netAmountInAgency"=> 0,
										"markupInAmount"=>0,
										"markupInPct"=>0,
										"grossAmt"=> (float)$dataCollectionsResults['itemCostings']["totalDueAmt"]*((float)$row["percent"]/100),
										"commInPct"=>0,
										"commInAmount"=>0,
										"commAmt"=>0,
										"discInPct"=>0,
										"discInAmount"=>0,
										"discAmt"=>0,
										"totalLessAmt"=> 0,
										"totalDueAmt"=> (float)$dataCollectionsResults['itemCostings']["totalDueAmt"]*((float)$row["percent"]/100),
										"totalProfitAmt"=> 0,
										"gstInPct"=>0,
										"gstInAmount"=>0,
										"gstAmt"=>0


									);
							}else{
								$dataItem["itemCostings"] =  array(
										"fromCurrency"=> 'AUD',
										"fromRate"=> 1,
										"toCurrency"=> 'AUD',
										"toRate"=> 1,
										"originalAmount"=> 0,
										"orginalNetAmt"=> 0,
										"netAmountInAgency"=> 0,
										"markupInAmount"=>0,
										"markupInPct"=>0,
										"grossAmt"=> (float)$row['amount'],
										"commInPct"=>0,
										"commInAmount"=>0,
										"commAmt"=>0,
										"discInPct"=>0,
										"discInAmount"=>0,
										"discAmt"=>0,
										"totalLessAmt"=> 0,
										"totalDueAmt"=> (float)$row['amount'],
										"totalProfitAmt"=> 0,
										"gstInPct"=>0,
										"gstInAmount"=>0,
										"gstAmt"=>0


									);
							}
							break;
						}

					}
				}
				
				
				$item = new Item();
				if($hasCancellation){

					$db->items->insert($dataItem);
					
				}else{
					$itemID = $tools->getNextID("item");
					$dataItem = array(
							"itemId"=> $itemID,
							"bookingId"=> (int)$_POST['bookingID'],
							"itemCode"=> 'CF',
							"itemName"=> 'Cancellation Fee',
							"itemPhone"=>'',
							"itemRatings"=> '',
							"itemPropertyCategory"=> '',
							"itemSeq"=> 0,
							"itemStockType"=> '',
							"itemStockTypeName"=> '',
							"itemServiceCode"=> 'SFEE',
							"itemServiceName"=> 'Service Fee',
							"itemSupplierCode"=> '',
							"itemSupplierName"=> '',
							"itemSumarryDescription"=> '',
							"itemDetailedDescription"=> '',
							"itemCostingsDescription"=> '',
							"itemInclusionDescription"=> '',
							"itemExclusionDescription"=> '',
							"itemConditionsDescription"=> '',
							"itemStatus"=> 'Quote',
							"itemSuplierConfirmation"=> '',
							"itemFromCountry"=> "",
							"itemFromCity"=> "",
							"itemFromAddress"=> "",
							"itemToCountry"=> '',
							"itemToCity"=> '',
							"itemToAddress"=> '',
							"itemStartDate"=> '',
							"itemEndDate"=> '',
							"itemStartTime"=>'00:00',
							"itemEndTime"=>'00:00',
							"itemIsLeadPax"=> 0,
							"itemIsFullPax"=> 0,
							"itemIsVoucherable"=> 0,
							"itemIsAutoInvoice"=> 1,
							"itemIsInvoicable"=> 1,
							"itemIsIterary"=> 0,
							"itemIsGstApplicable"=> 0,
							"itemAdultMin"=>0,
							"itemAdultMax"=>0,
							"itemChildMin"=>0,
							"itemChildMax"=>0,
							"itemInfantMin"=>0,
							"itemInfantMax"=>0,
							"itemIsCancelled"=>0,
							"itemIsPaxAllocated"=>1,
							"itemIsReceipt"=>0,
							"itemIsInvoiced"=>1,
							"itemIsPaid"=>0,
							"itemIsConfirmed"=>1,
							"itemIsBlank"=> 1,
							"itemIsLive"=>0,
							"itemCancellation"=>array(),
							"itemSupplements"=>array(),
							"itemCostings" =>  array(
										"fromCurrency"=> 'AUD',
										"fromRate"=> 1,
										"toCurrency"=> 'AUD',
										"toRate"=> 1,
										"originalAmount"=> 0,
										"orginalNetAmt"=> 0,
										"netAmountInAgency"=> 0,
										"markupInAmount"=>0,
										"markupInPct"=>0,
										"grossAmt"=> 0,
										"commInPct"=>0,
										"commInAmount"=>0,
										"commAmt"=>0,
										"discInPct"=>0,
										"discInAmount"=>0,
										"discAmt"=>0,
										"totalLessAmt"=> 0,
										"totalDueAmt"=> 0,
										"totalProfitAmt"=> 0,
										"gstInPct"=>0,
										"gstInAmount"=>0,
										"gstAmt"=>0


									)
							

						);

				}

				$item->reEvaluateCancellation($dataCollectionsResults, $dataItem);

				
				$db->items->update(
						    array("itemId" => (int)$_POST['itemId']),
						    array('$set' => array('itemIsCancelled' => 1,"itemSeq"=>2000))
						);

				

				

			}
			
			$response = array ( "status" => true, "message" => $message,"booking_id"=>(int)$_POST['bookingID']);
			
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		

		case "itemSequence":{

			$itemID = $_POST['itemID'];
			$seqNum = $_POST['seq'];

			$connection = new MongoClient();
			$db = $connection->db_system;
			
			
			$db->items->update(array('itemId'=>(int)$itemID),array('$set' =>array("itemSeq"=>(int)$seqNum)));
				
			$response =  array("status"=>true);
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}

		case "checkItemForPayment":{
			$connection = new MongoClient();
			$db = $connection->db_system;


			$whereData = array('bookingId'=>(int)$_POST['bookingID'],"itemId"=>(int)$_POST['itemId']);
			
			$item = $db->items->findOne($whereData);

			if($item){
				if($item['itemSupplierCode']=="HBBEDLIVE"){
					if($item["itemIsCancelled"] == 0 && $item["itemIsConfirmed"] == 1){

						$paymentObj = new PaymentWestpac();

						$parameters = array();
				        $parameters['username'] = $username;
				        $parameters['password'] = $password;
				        $parameters['biller_code'] = $billerCode;
				        $parameters['merchant_id'] = $merchantId;
				        $parameters['payment_reference'] = (int)$_POST['bookingID'].'-'.(int)$_POST['itemId'];
				        $parameters['payment_reference_change'] = 'false';
				        $parameters['surcharge_rates'] = 'VI/MC=1.8,AX=3.8,DC=3.8';
				        $parameters['payment_amount'] = (float)$item["itemCostings"]['totalDueAmt'];
				        $parameters['return_link_url'] = 'http://208.97.188.14:7777/_paymentGateway/postPayment.php';
				        $parameters['return_link_redirect'] = 'true';
				        //print_r($parameters);
						$token = $paymentObj->getToken($parameters);
						$response =  array("status"=>true,"hasNoExpiry"=>true,"expiry"=>0,"token"=>$token,"totalAmount"=>(float)$item["itemCostings"]['totalDueAmt'],"message"=>"");
					}elseif($item["itemIsCancelled"] == 0 && $item["itemIsConfirmed"] ==0){
						$strFrom = strtotime($item['otherDetails']['dateTimeCreated']);
						$dateNow = date('d-m-Y H:i:s');
						$strTo = strtotime($dateNow);

						
						
						$seconds_diff = $strTo - $strFrom;
						$milliseconds_diff = $seconds_diff * 1000;
						//echo $milliseconds_diff.'|'.(float)$item['otherDetails']['expireTime'];
						if((float)$milliseconds_diff<(float)$item['otherDetails']['expireTime']){
							$timeExpiry = (float)$item['otherDetails']['expireTime'] - (float)$milliseconds_diff ;
							//echo $timeExpiry;
							if($timeExpiry >= 120000){ //not expired and above 2 minutes , you can do payment.

								$paymentObj = new PaymentWestpac();

								$parameters = array();
						        $parameters['username'] = $username;
						        $parameters['password'] = $password;
						        $parameters['biller_code'] = $billerCode;
						        $parameters['merchant_id'] = $merchantId;
						        $parameters['payment_reference'] = (int)$_POST['bookingID'].'-'.(int)$_POST['itemId'];
						        $parameters['payment_reference_change'] = 'false';
						        $parameters['surcharge_rates'] = 'VI/MC=1.8,AX=3.8,DC=3.8';
						        $parameters['payment_amount'] = (float)$item["itemCostings"]['totalDueAmt'];
						        $parameters['return_link_url'] = 'http://208.97.188.14:7777/_paymentGateway/postPayment.php';
						        $parameters['return_link_redirect'] = 'true';

								$token = $paymentObj->getToken($parameters);
								$response =  array("status"=>true,"hasNoExpiry"=>false,"expiry"=>$timeExpiry,"token"=>$token,"totalAmount"=>(float)$item["itemCostings"]['totalDueAmt'],"message"=>"");
							}else{
								$response =  array("status"=>false,"hasNoExpiry"=>false,"expiry"=>$timeExpiry,"token"=>"","totalAmount"=>0,"message"=>"No time left, to do the payment");
							}
							
						}else{
							$response =  array("status"=>false,"hasNoExpiry"=>false,"expiry"=>$timeExpiry,"token"=>"","totalAmount"=>0,"message"=>"No time left, to do the payment");;
						}
						
						
					}else{
						$response =  array("status"=>false,"hasNoExpiry"=>true,"expiry"=>0,"token"=>"","totalAmount"=>0,"message"=>"You cant pay on this item");
					}
				}else{
					$response =  array("status"=>false,"hasNoExpiry"=>true,"expiry"=>0,"token"=>"","totalAmount"=>0,"message"=>"You cant pay on this item");
				}

			}else{
				$response =  array("status"=>false,"hasNoExpiry"=>true,"expiry"=>0,"token"=>"","totalAmount"=>0,"message"=>"Unable to process");
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}
		case "finalizedItemFromPayment":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$paymentRef = explode('-',$_POST['paymentRef']);

			$whereData = array('bookingId'=>(int)$paymentRef[0],"itemId"=>(int)$paymentRef[1]);
			
			$item = $db->items->findOne($whereData);

			if($item){
				if($item['itemSupplierCode']=="HBBEDLIVE"){
					if($item["itemIsCancelled"] == 0 && $item["itemIsConfirmed"] == 1){

						$datas = array();
						
						$receiptData = array();
						$receiptData['receiptTypeCode'] = 'ISW';
						$receiptData['receiptTypeName'] = 'Inhouse Swipe Machine';
						$receiptData['receiptDescription'] = 'WestPac Payment B2B - Bank Ref: '.$_POST['bank_reference'];
						$receiptData['receiptReferenceNumber'] = $_POST['paymentRef'];
						$receiptData['receiptChequeNumber'] = "";
			            $receiptData['receiptABAnumber'] ="";
			           	$receiptData['receiptAccountChequeNumber'] ="";
			            $receiptData['receiptDrawersName'] ="";
			            $receiptData['receiptBankCode'] ="";
			            $receiptData['receiptBankName'] ="";
			            
			            if($_POST['card_type'] == "VISA"){
			            	$receiptData['receiptCCTypeCode'] = 'VC';
			            	$receiptData['receiptCCTypeName'] = 'Visa Card';

			            }
			            elseif($_POST['card_type'] == "MASTERCARD"){
			            	$receiptData['receiptCCTypeCode'] = 'MC';
			            	$receiptData['receiptCCTypeName'] = 'Master Card';
			            }
			            elseif($_POST['card_type'] == "AMEX"){
			            	$receiptData['receiptCCTypeCode'] = 'AE';
			            	$receiptData['receiptCCTypeName'] = 'American Express';
			            }
			            elseif($_POST['card_type'] == "DINERS"){
			            	$receiptData['receiptCCTypeCode'] = 'DC';
			            	$receiptData['receiptCCTypeName'] = 'Dinner Club';
			            }

			            else{
			            	$receiptData['receiptCCTypeCode'] = 'DCE';
			            	$receiptData['receiptCCTypeName'] = 'Debit Card EFTPOS';
			            }
			            

			            $receiptData['receiptCCNumber'] = 'XXXXXXXXXXXXXXXX';
			            $receiptData['receiptCCExpiryDate'] = 'XXXX';
			            $receiptData['receiptCCSecurityCode'] = 'XXX';
			            $receiptData['receiptDisplayAmout'] = (float)$item['itemCostings']['grossAmt'];
			            $receiptData['receiptAmout'] = (float)$item['itemCostings']['totalDueAmt'];
			            $receiptData['receiptDepositedAmout'] = (float)$item['itemCostings']['totalDueAmt'];
			            $receiptData['receiptRefundAmout'] = 0;
			            $datas['receiptData'] = (Object)$receiptData;
						$datas['receiptItemData'] = array();

						$itemData = array();
						$itemData['itemID'] = $item["itemId"];
						$itemData['itemCode'] = $item["itemCode"];
						$itemData['itemName'] = $item["itemName"];
						$itemData['itemCostings'] = (Object)$item["itemCostings"];
						$itemData['itemServiceCode'] = $item["itemServiceCode"];
						$itemData['itemService'] = $item["itemService"];
						$itemData['itemStartDate'] = $item["itemStartDate"];
						$itemData['itemEndDate'] = $item["itemEndDate"];
						$itemData['amountPaid'] =  (float)$item['itemCostings']['totalDueAmt'];
						$itemData['itemNaturalDueAmount'] = (float)$item['itemCostings']['totalDueAmt'];
						$itemData['DisplayAmount'] = (float)$item['itemCostings']['grossAmt'];
						$itemData['itemTotalAmountToAllocate'] = 0;


						$paxArray = array();
				   		$whereData = array('item_id'=>(int)$paymentRef[1]);

						$dataCollectionsResults2 = $db->item_guest->find($whereData);
						try {
							$dataArray2 = iterator_to_array($dataCollectionsResults2);
							 foreach($dataArray2 as $key2 => $row2){
							 	$row2['guestId'] = $key2;
							 	array_push($paxArray, $row2);

							 }
						} catch (Exception $e) {
						   //$dataArray = array();
						}
						//$dataArray[$key]['itemGuests'] = $paxArray;
						$item['itemGuest'] = $paxArray;

						array_push($datas['receiptItemData'],(Object)$itemData);
						//print_r($datas);
						$itemObject = new Item();
						$itemObject->createReceipt((int)$paymentRef[0],(Object)$datas);
						
						$response =  array("status"=>true,"booking_id"=> (int)$paymentRef[0],"message"=>"Thank you for your payment.");	

					}elseif($item["itemIsCancelled"] == 0 && $item["itemIsConfirmed"] ==0){
						$whereData = array('itemId'=>(int)$paymentRef[1]);
						$dataCollectionsResults = $db->items->find($whereData);
						//print_r($dataCollectionsResults);
						try {
							$dataArray= iterator_to_array($dataCollectionsResults);
							
						} catch (Exception $e) {
						  	$dataArray = array();
						}
						//print_r($dataArray);
						$dataItemToInvoice = array();

						$allgood = true;

						foreach($dataArray as $key => $row){
							if($row['itemSupplierCode']!="HBBEDLIVE"){
								continue;
							}
							
							foreach($row['itemSupplements'] as $key2 => $row2){
								if((int)$row2['supplement_qty'] >0){

									$row['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
									$row['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
									$row['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
									$row['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
									$row['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
									$row['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
									$row['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
									$row['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
									$row['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
									$row['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
									$row['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
										
								}
					            
							}
							$hbObj = new HotelBedsClient();
							if($row['itemServiceCode'] =='ACC'){

								$strFrom = strtotime($row['otherDetails']['dateTimeCreated']);
								$dateNow = date('d-m-Y H:i:s');
								$strTo = strtotime($dateNow);
								$seconds_diff = $strTo - $strFrom;
								$milliseconds_diff = $seconds_diff * 1000;

								if((float)$milliseconds_diff<(float)$row['otherDetails']['expireTime']){
									$purchaseConfirm = $hbObj->purchaseConfirm($row);

									if(!(array)$purchaseConfirm->Purchase->ServiceList->Service->ErrorList->Error){

										$confimartion = (string)$purchaseConfirm->Purchase->Reference->IncomingOffice->attributes()->code."-".(string)$purchaseConfirm->Purchase->Reference->FileNumber;
										$db->items->update(array("itemId" => (int) $row['itemId']),array('$set' => array('itemIsConfirmed' => 1,"itemSuplierConfirmation"=>$confimartion)));

										$message .= "Item #".$row['itemId']." has been confirmed.</br>";


										if((int)$row['itemIsInvoicable'] == 1 && (int)$row['itemIsAutoInvoice'] == 1 ){
											array_push($dataItemToInvoice,$row);
											
										}
									}else{
										$allgood = false;
									}

								}


								
							}else{
								//this is for transfer
								
							}


						

						}

						if(count($dataItemToInvoice) !=0){
							$itemObj = new Item();
							$isInvoiced = $itemObj->createInvoiced((int)$paymentRef[0],$dataItemToInvoice);

							if($isInvoiced){
								//print_r($dataItemToInvoice);
								foreach($dataItemToInvoice as $row){

									$db->items->update(
									    array("itemId" => (int)$row['itemId']),
									    array('$set' => array('itemIsInvoiced' => 1))
									);
									$message .= "Item #".$row['itemId']." has been invoiced.</br>";
									//echo $message;
								}
								
								
							}	
						}
						if($allgood){


							$datas = array();
						
							$receiptData = array();
							$receiptData['receiptTypeCode'] = 'ISW';
							$receiptData['receiptTypeName'] = 'Inhouse Swipe Machine';
							$receiptData['receiptDescription'] = 'WestPac Payment B2B - Bank Ref: '.$_POST['bank_reference'];
							$receiptData['receiptReferenceNumber'] = $_POST['paymentRef'];
							$receiptData['receiptChequeNumber'] = "";
				            $receiptData['receiptABAnumber'] ="";
				           	$receiptData['receiptAccountChequeNumber'] ="";
				            $receiptData['receiptDrawersName'] ="";
				            $receiptData['receiptBankCode'] ="";
				            $receiptData['receiptBankName'] ="";
				            
				            if($_POST['card_type'] == "VISA"){
				            	$receiptData['receiptCCTypeCode'] = 'VC';
				            	$receiptData['receiptCCTypeName'] = 'Visa Card';

				            }
				            elseif($_POST['card_type'] == "MASTERCARD"){
				            	$receiptData['receiptCCTypeCode'] = 'MC';
				            	$receiptData['receiptCCTypeName'] = 'Master Card';
				            }
				            elseif($_POST['card_type'] == "AMEX"){
				            	$receiptData['receiptCCTypeCode'] = 'AE';
				            	$receiptData['receiptCCTypeName'] = 'American Express';
				            }
				            elseif($_POST['card_type'] == "DINERS"){
				            	$receiptData['receiptCCTypeCode'] = 'DC';
				            	$receiptData['receiptCCTypeName'] = 'Dinner Club';
				            }

				            else{
				            	$receiptData['receiptCCTypeCode'] = 'DCE';
				            	$receiptData['receiptCCTypeName'] = 'Debit Card EFTPOS';
				            }
				            

				            $receiptData['receiptCCNumber'] = 'XXXXXXXXXXXXXXXX';
				            $receiptData['receiptCCExpiryDate'] = 'XXXX';
				            $receiptData['receiptCCSecurityCode'] = 'XXX';
				            $receiptData['receiptDisplayAmout'] = (float)$item['itemCostings']['grossAmt'];
				            $receiptData['receiptAmout'] = (float)$item['itemCostings']['totalDueAmt'];
				            $receiptData['receiptDepositedAmout'] = (float)$item['itemCostings']['totalDueAmt'];
				            $receiptData['receiptRefundAmout'] = 0;
				            $datas['receiptData'] = (Object)$receiptData;
							$datas['receiptItemData'] = array();

							$itemData = array();
							$itemData['itemID'] = $item["itemId"];
							$itemData['itemCode'] = $item["itemCode"];
							$itemData['itemName'] = $item["itemName"];
							$itemData['itemCostings'] = (Object)$item["itemCostings"];
							$itemData['itemServiceCode'] = $item["itemServiceCode"];
							$itemData['itemService'] = $item["itemService"];
							$itemData['itemStartDate'] = $item["itemStartDate"];
							$itemData['itemEndDate'] = $item["itemEndDate"];
							$itemData['amountPaid'] =  (float)$item['itemCostings']['totalDueAmt'];
							$itemData['itemNaturalDueAmount'] = (float)$item['itemCostings']['totalDueAmt'];
							$itemData['DisplayAmount'] = (float)$item['itemCostings']['grossAmt'];
							$itemData['itemTotalAmountToAllocate'] = 0;


							$paxArray = array();
					   		$whereData = array('item_id'=>(int)$paymentRef[1]);

							$dataCollectionsResults2 = $db->item_guest->find($whereData);
							try {
								$dataArray2 = iterator_to_array($dataCollectionsResults2);
								 foreach($dataArray2 as $key2 => $row2){
								 	$row2['guestId'] = $key2;
								 	array_push($paxArray, $row2);

								 }
							} catch (Exception $e) {
							   //$dataArray = array();
							}
							//$dataArray[$key]['itemGuests'] = $paxArray;
							$item['itemGuest'] = $paxArray;

							array_push($datas['receiptItemData'],(Object)$itemData);
							//print_r($datas);
							$itemObject = new Item();
							$itemObject->createReceipt((int)$paymentRef[0],(Object)$datas);
							
							$response =  array("status"=>true,"booking_id"=> (int)$paymentRef[0], "message"=>"Thank you for your payment.");


						}else{
							$response =  array("status"=>false,"booking_id"=> (int)$paymentRef[0],"message"=>"Unable to convert");
						}
						
						
					}
				}else{
					$response =  array("status"=>false,"booking_id"=> (int)$paymentRef[0],"message"=>"Unable to process");	
				}

			}else{
				$response =  array("status"=>false,"booking_id"=> (int)$paymentRef[0],"message"=>"Unable to process");
			}

			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}
		/* ENDLOGIN*/
	}
?>