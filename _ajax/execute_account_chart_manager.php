<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			//db.sys_account_chart.find
			
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("Account"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("AccountName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("Type"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("DrCr"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			//print_r($whereData);
			$dataCollections = $db->sys_account_chart->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(1000);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_sys_account_chart'] = $key;
				$data['Account'] = $row['Account'];
				$data['AccountName'] = $row['AccountName'];
				$data['TaxCode'] = $row['TaxCode'];
				$data['Level'] = $row['Level'];
				$data['HeaderDetail'] = $row['HeaderDetail'];
				$data['Type'] = $row['Type'];
				$data['DrCr'] = $row['DrCr'];

			 	array_push($arrayResults,$data);
			}

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewAccountChart" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		
		/* ENDLOGIN*/
	}
?>