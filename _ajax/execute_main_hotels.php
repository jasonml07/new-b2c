<?php
error_reporting(E_ALL ^ E_NOTICE);
require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateHBDestination":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
		
			$id = $_POST['hotelBeds_destination_id'];
			
						
			if($id==0){
				
				
			
				$insertData = array();

				$insertData['ZoneCode'] = $_POST['ZoneCode'];
				$insertData['ZoneName'] = $_POST['ZoneName'];
				$insertData['CountryCode'] = $_POST['CountryCode'];
				$insertData['CountryName'] = $_POST['CountryName'];
				$insertData['DestinationCode'] = $_POST['DestinationCode'];
				$insertData['DestinationName'] = $_POST['DestinationName'];
				$insertData['ZoneGroupingCode'] = $_POST['ZoneGroupingCode'];
				$insertData['ZoneGroupingName'] = $_POST['ZoneGroupingName'];
			

				$res = $db->hb_destination->insert($insertData);
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
			
			}else{

				

				$mongoID = new MongoID($_POST['hotelBeds_destination_id']);
				$updateData = array();

				$updateData['ZoneCode'] = $_POST['ZoneCode'];
				$updateData['ZoneName'] = $_POST['ZoneName'];
				$updateData['CountryCode'] = $_POST['CountryCode'];
				$updateData['CountryName'] = $_POST['CountryName'];
				$updateData['DestinationCode'] = $_POST['DestinationCode'];
				$updateData['DestinationName'] = $_POST['DestinationName'];
				$updateData['ZoneGroupingCode'] = $_POST['ZoneGroupingCode'];
				$updateData['ZoneGroupingName'] = $_POST['ZoneGroupingName'];
			

				

				$res = $db->hb_destination->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;	
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("HotelDetail.hotel_name"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelDetail.ratings"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("HotelLocation.country"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("HotelLocation.city"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelLocation.fax"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelLocation.phoneResv"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelLocation.phoneMang"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelLocation.email"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("HotelLocation.address"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			//print_r($whereData);
			$dataCollections = $db->main_hotels->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(100);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_main_hotels'] = $key;
				$data['HotelDetail'] = $row['HotelDetail'];
				$data['HotelSuppliers'] = $row['HotelSuppliers'];
				$data['HotelDistances'] = $row['HotelDistances'];
				$data['HotelDescriptions'] = $row['HotelDescriptions'];
				$data['HotelLocation'] = $row['HotelLocation'];
				$data['HotelSearchLocation'] = $row['HotelSearchLocation'];
				$data['HotelFacilities'] = $row['HotelFacilities'];
				$data['HotelImages'] = $row['HotelImages'];
				$data['HotelDestination'] = $row['HotelDestination'];

			 	array_push($arrayResults,$data);
			}

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewMainHotels" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		
		
		/* ENDLOGIN*/
	}
?>