<?php

require_once('../_classes/tools.class.php');
require_once('../_classes/hotelsPro.class.php');
require_once('../_classes/restel.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		
		case "getRoomRates":{
			
			$params = 	array(
				"LoginDetails"=>array(
					"User"=>"testuser",
					"Password"=>"testpw",
					"Token"=> $_GET['token']
				), 
				"SearchDetails"=> array(
					"CheckInDate"=> "2015-06-25",
					"CheckOutDate"=> "2015-06-28", 
					#"DestinationCode": "", #"LD6J",
					"ProductCode"=> "025968", #"UKOW7F",
					"RoomRequests"=>array(
						array(
							"Adult"=> 2,
							"Child"=> 1,
							"ChildAges"=>array(
								array(
									"Age"=> 2,
								)
							)
						)
					)
				)
			);
/*
headers = {'Content-Type': 'application/json'} # set what your server accepts
payload = json.dumps(params)
r = requests.get("http://192.168.254.188:5000/api/v2/hotel_room_rates", data=payload, headers=headers)


print r.status_code
#print json.dumps(r.text)
print r.text

104.236.39.107
*/

			$payLoad = json_encode($params);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://104.236.39.107/api/v2/hotel_room_rates");
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_PORT, 5000);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $payLoad); 

			$results = curl_exec($ch); 
			echo $results;
    		curl_close ($ch);

    		print_r(json_decode($results));
  	   		exit;
  	  	break; 
		}
		case "doSearchHP":{
			$dataGET = json_decode($_GET['postDetails']);
			
			$connection = new MongoClient();
			$db = $connection->db_system;


			
			$hpObj = new HotelsProClient();
			$hotels = $hpObj->get_available_hotels($dataGET);
			if(!is_object($hotels)){
				$response = array ( "success" => true, "total" => 0, "viewHotelsHP" => array());
  	  		
	  	  		print json_encode($response);
	  	  		$connection->close();
	  	  		exit;
			}

			if (is_object($hotels->availableHotels)) {
				$hotel_results_Hpro[] = $hotels->availableHotels;
			} else {
				$hotel_results_Hpro = $hotels->availableHotels;
			}

			$searchID = $hotels->searchId;
			#$ctrHotelFound = 0;
			$hotesProCode = array();
			foreach ((array)$hotel_results_Hpro as $hnum => $hotel) {
				if(!in_array($hotel->hotelCode, $hotesProCode)){
					array_push($hotesProCode, $hotel->hotelCode);
				}
				#$ctrHotelFound++;
			}
			
			$resDataCollection = $db->hp_hotels->find(array("HotelCode"=> array("\$in"=>$hotesProCode)),array("_id"=>0));
			$dataArray = iterator_to_array($resDataCollection);
			$hotelCollection = array();
			
			foreach($dataArray as $key => $row){
				$hotelCollection[$row['HotelCode']] =  $row;
				
			}

			$currEx = array();
			
			
			$ctrHotelFound = 0;
			foreach ((array)$hotel_results_Hpro as $hnum => $hotel) {
				
				if(array_key_exists($hotel->hotelCode,$hotelCollection)){

					if(!array_key_exists($hotel->currency,$currEx)){
						$resCurr = $db->currency_ex->findOne(array("curr_from"=> $hotel->currency), array("_id"=>0));

						$hotel = (array) $hotel;
						$currEx[$hotel->currency] = (float) $resCurr['rate_to'];

						$hotel['currTo'] = 'AUD';
						$hotel['convertedTotalPrice'] = (float)$hotel['totalPrice']*$currEx[$hotel->currency] ;
						$hotel = (object) $hotel;
						
					}else{
						$hotel = (array) $hotel;
						$hotel['currTo'] = 'AUD';
						$hotel['convertedTotalPrice'] = (float)$hotel['totalPrice']*$currEx[$hotel->currency] ;
						$hotel = (object) $hotel;
					}

					if(isset($hotelCollection[$hotel->hotelCode]['roomsAvailable'])){
						array_push($hotelCollection[$hotel->hotelCode]['roomsAvailable'],$hotel);
						if($hotelCollection[$hotel->hotelCode]['priceSort']>(float)$hotel->totalPrice){
							$hotelCollection[$hotel->hotelCode]['priceSort'] = (float)$hotel->totalPrice;
						}
					}else{
						$hotelCollection[$hotel->hotelCode]['searchID'] = $searchID;
						$hotelCollection[$hotel->hotelCode]['roomsAvailable'] = array();
						array_push($hotelCollection[$hotel->hotelCode]['roomsAvailable'],$hotel);

						$hotelCollection[$hotel->hotelCode]['priceSort'] = (float)$hotel->totalPrice;
					}
					$ctrHotelFound++;
				}
			}

			$arrayHotelsResults = array();
			foreach($hotelCollection as $key => $val){

				array_push($arrayHotelsResults,$val);
			}
			
			$response = array ( "success" => true, "total" => $ctrHotelFound, "viewMainHotelsHP" => $arrayHotelsResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		break; 
		}
		case "doSearchRS":{
			$dataGET = json_decode($_GET['postDetails']);
			$connection = new MongoClient();
			$db = $connection->db_system;
			
			$rsObj = new RestelClient();
			$hotels = $rsObj->get_available_hotels($dataGET);
			/*{name: "HotelCode", mapping: "HotelCode"}, //cod
			{name: "HotelName", mapping: "HotelName"}, //nom
			{name: "Country", mapping: "Country"},
			{name: "City", mapping: "City"},
			{name: "HotelAddress", mapping: "HotelAddress"}, // dir
			{name: "StarRating", mapping: "StarRating"},
			{name: "HotelInformation", mapping: "HotelInformation"}, // desc
			{name: "HotelImages", mapping: "HotelImages"}, // foto
			{name: "HotelPostalCode", mapping: "HotelPostalCode"},
			{name: "HotelArea", mapping: "HotelArea"},
			{name: "HotelPhoneNumber", mapping: "HotelPhoneNumber"},
			{name: "HotelLatitude", mapping: "HotelLatitude"}, // lat
			{name: "HotelLongtitude", mapping: "HotelLongtitude"}, //lon
			{name: "hotelServices", mapping: "hotelServices"},
			{name: "searchID", mapping: "searchID"},
			{name: "roomsAvailable", mapping: "roomsAvailable"},
			{name: "priceSort", mapping: "priceSort",type:'float'}*/
			if(!$hotels){
				$response = array ( "success" => true, "total" => 0, "viewHotelsRS" => array());
  	  		
	  	  		print json_encode($response);
	  	  		$connection->close();
	  	  		exit;
			}

			if($hotels) {
			
				$response = array ( "success" => true, "total" => count($hotels), "viewMainHotelsRS" => $hotels);
  	  		}
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		break; 
		}
		
		
		/* ENDLOGIN*/
	}

?>