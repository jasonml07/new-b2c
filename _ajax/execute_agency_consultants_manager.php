<?php
session_start();
require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		
		case "view": {

			$connection = new MongoClient();
			$db = $connection->db_system;
			$whereData = array();
			$whereData['agency_code'] = $_SESSION['sys_agency_code_B2B'];
			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				//array_push($whereData['$or'],array("agency_name"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("agency_consultants_fname"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("fname"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("mname"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("lname"=> new MongoRegex("/^".$_GET["query"]."/i")));	
			}
			
			$dataCollections2 = $db->agency->find();
			$dataArray = iterator_to_array($dataCollections2);
			$agencyData = array();
			foreach($dataArray as $key => $row){

				$agencyData[$row['agency_code']] = $row['agency_name'];
			}

			$dataCollections = $db->agency_consultants->find($whereData);
			
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_agency_consultants'] = $key;
				$data['agency_consultants_code'] = $row['code'];
				$data['agency_consultants_fname'] = $row['fname'];
				$data['agency_consultants_mname'] = $row['mname'];
				$data['agency_consultants_lname'] = $row['lname'];
				$data['title'] = $row['title'];
				$data['password'] = $row['password'];
				$data['username'] = $row['username'];
				$data['agency_code'] = $row['agency_code'];
				$data['agency_name'] = $agencyData[$row['agency_code']];
				$data['is_active'] = $row['is_active'];
				
				array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewAgencyConsultants" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;

  	  	break; 
		}

		case "addConsultant":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			$resPOST = json_decode($_POST['postDetails'], true);
			$id = $resPOST['agency_consultants_id'];

			if($id == 0){
				
				unset($resPOST['agency_consultants_id']);
				$tool = new Tools();
				$resPOST['code'] = $tool->codeGeneRatorAndChecker("agency",$resPOST['code']);
				$resPOST['password'] = $resPOST['password'];
				$resPOST['agency_code'] = $_SESSION['sys_agency_code_B2B'];
				$resPOST['agency_name'] = $_SESSION['sys_agency_name_B2B'];
				unset($resPOST['agency_name']);
				$res = $db->agency_consultants->insert($resPOST);
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			}else{

				$mongoID = new MongoID($resPOST['agency_consultants_id']);
				unset($resPOST['agency_consultants_id']);
				unset($resPOST['agency_name']);
				$res = $db->agency_consultants->update(array("_id"=> $mongoID), array("\$set" => $resPOST));
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}	
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}


		case "getAgencyConsultantDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['agency_consultants_id']);
			$res = $db->agency_consultants->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['agency_consultants_id'] = $_POST['agency_consultants_id'];
			$res['agency_name'] = $_POST['agency_name'];
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_agency_consultants']);
			
			$db->agency_consultants->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 1)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_agency_consultants']);
			
			$db->agency_consultants->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 0)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}


	
	}
?>