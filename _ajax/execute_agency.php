<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
require_once('../_classes/tools.class.php');


$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		case "getAgency":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array();
			$whereData['is_active'] = 1;
			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("agency_name"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("agency_code"=> new MongoRegex("/^".$_GET["query"]."/i")));
				
			}
			//print_r($whereData);db.main_destination.find
			$dataCollections = $db->agency->find($whereData)->sort(array("agency_name"=>1));
			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['agency_code'] = $row['agency_code'];
				$data['agency_name'] = $row['agency_name'];

			 	array_push($arrayResults,$data);
			}
			/*$mysql = new Database; 
			$filter = "";
			if($_GET["query"] != ""){
			$filter = "and  agency_name like '%".$_GET["query"]."%'";
			}
			
			$sql = "SELECT id_agency, agency_name FROM agency where is_active = 1 $filter ORDER BY agency_name ASC;";
			$service = $mysql->select_execute_query($sql);*/
  	  		
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		

		case "getAgencyConsultant":{

			$arrayResults = array();

			if(isset($_GET["agency_code"])){

				$connection = new MongoClient();
				$db = $connection->db_system;

				$whereData = array("agency_code"=>$_GET["agency_code"],"is_active"=>(int)1);

			
				//print_r($whereData);db.main_destination.find
				$dataCollections = $db->agency_consultants->find($whereData);

				$dataArray = iterator_to_array($dataCollections);
				
				
				foreach($dataArray as $key => $row){
					$data = array();
					$data['code'] = $row['code'];
					$data['name'] = $row['title']." ".$row['fname']." ".$row['mname']." ".$row['lname'];
						

				 	array_push($arrayResults,$data);
					
					
				}

				usort($arrayResults, function($a, $b) {
				    return strcasecmp($a['name'], $b['name']);
				});
				$connection->close();

			}
			

  	  		
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		
		/* ENDLOGIN*/
	}
?>