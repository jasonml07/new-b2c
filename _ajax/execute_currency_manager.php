<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateCurr":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 


			

			$id = $_POST['curr_id'];
			
			
			
			if($id==0){
				
				
			
				#$code = $tools->generateCode($_POST['aliasname']); 
				$insertData = array();

				$insertData['curr_from_name'] = $_POST['curr_from_name'];
				$insertData['curr_from'] = $_POST['curr_from'];
				$insertData['rate_from'] = $_POST['curr_rate_from'];
				$insertData['curr_to'] = $_POST['curr_to'];
				$insertData['rate_to'] = $_POST['curr_rate_to'];

				$res = $db->currency_ex->insert($insertData);
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
			
			}else{

				

				$mongoID = new MongoID($_POST['curr_id']);
				$updateData = array();

				$updateData['curr_from_name'] =$_POST['curr_from_name'];
				$updateData['curr_from'] = $_POST['curr_from'];
				$updateData['rate_from'] = $_POST['curr_rate_from'];
				$updateData['curr_to'] = $_POST['curr_to'];
				$updateData['rate_to'] = $_POST['curr_rate_to'];


				$res = $db->currency_ex->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}
		case "getCurrDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['curr_id']);
			$res = $db->currency_ex->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['curr_id'] = $_POST['curr_id'];
			#$response = array();
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "getCurrLive":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['curr_id']);
			$res = $db->currency_ex->findOne(array("_id"=> $mongoID),array("_id"=>0));
			

			$yql_base_url = "http://query.yahooapis.com/v1/public/yql";  
			$yql_query = 'select * from yahoo.finance.xchange where pair in ("'.$res['curr_from'].'AUD")';
			$yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
			$yql_query_url .= "&env=store://datatables.org/alltableswithkeys&format=json";  
			$curl = curl_init($yql_query_url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);    
			$json = curl_exec($curl); 
			curl_close($curl);   
			//$url = 'http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in ("'.$res['curr_from'].'AUD")&env=store://datatables.org/alltableswithkeys';
			//echo $json;
			//$url = "http://rate-exchange.appspot.com/currency?from=".$res['curr_from']."&to=AUD";

			// Get cURL resource
			/*$curl = curl_init();
			curl_setopt_array($curl, array(
			    CURLOPT_RETURNTRANSFER => 1,
			    CURLOPT_URL => $url,
			));
			$resp = curl_exec($curl);*/

			
			//$json = json_encode($xml);
			#Sprint_r($json);
			//die();
			$arrRes = json_decode($json);
			//print_r($arrRes);
			
			if($arrRes->query){


				$mongoID = new MongoID($_POST['curr_id']);
				$updateData = array();
				$updateData['rate_to'] = $arrRes->query->results->rate->Rate;


				$res = $db->currency_ex->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				$response = array("status"=>"ok");
			}else{
				$response = array("status"=>"fail");
			}
			
			print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			
			
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['curr_from'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			$dataCollections = $db->currency_ex->find($whereData)->sort(array('curr_from' => 1));

			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_curr'] = $key;
				$data['rate_from'] = $row['rate_from'];
				$data['rate_to'] = $row['rate_to'];
				$data['curr_to'] = $row['curr_to'];
				$data['curr_from'] = $row['curr_from'];
				$data['curr_from_name'] =$row['curr_from_name'];

			 	array_push($arrayResults,$data);
			}

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewCurr" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		

		case "getCurrency":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array();
			if(isset($_GET["query"])){
				$whereData['curr_from'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			
			//print_r($whereData);db.main_destination.find
			$dataCollections = $db->currency_ex->find($whereData)->sort(array('curr_from' => 1));

			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['curr_val'] = $row['rate_to'];
				$data['curr_name'] = $row['curr_from'];
				

			 	array_push($arrayResults,$data);
			}
  	  		
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		
		/* ENDLOGIN*/
	}
?>