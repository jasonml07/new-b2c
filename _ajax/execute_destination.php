<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "getDestination":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			//$tools = new Tools; 
			
			//code,destination || db.streets.find( { street_name : { $regex : '^Al', $options: 'i' } } );

			$filter = "";
			$whereData = array();
			if($_GET["query"] != ""){
				$whereData['$or'] = array();
				$filter = new MongoRegex("/^".$_GET["query"]."/i");
				array_push($whereData['$or'],array("country"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("city"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			$dataCollectionsResults = $db->main_hotel_destinations->find($whereData)->sort(array('country' => 1,'city' => 1));

			
			$dataArray = iterator_to_array($dataCollectionsResults);
			$response = array();
			$cityChecker = array();
			foreach($dataArray as $key => $row){
				/*$des = $row['CountryName'].", ".$row['CityName'].", ".$row['AreaName'];
				$des2 = $row['CountryName'].", ".$row['CityName'];
				$code = $row['CountryCode']."-".$row['CityCode']."-".$row['AreaCode'];
				$code2 = $row['CountryCode']."-".$row['CityCode'];
				$forChecking = $row['CountryName'].", ".$row['CityName'];
				if(!in_array($forChecking,$cityChecker)){
					array_push($cityChecker,$forChecking);

					
					$dArray = array("code"=> $code,"destination"=>$des);
					array_push($response,$dArray);
					
					
				}else{
					$dArray = array("code"=> $code,"destination"=>$des);
					array_push($response,$dArray);
				}*/
				//$code = $row['code']
				$dArray = array("code"=> $row['code'],"destination"=>$row['text']);
				array_push($response,$dArray);

			}
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "getDestinationHB":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			//$tools = new Tools; 
			
			//code,destination || db.streets.find( { street_name : { $regex : '^Al', $options: 'i' } } );

			$filter = "";
			$whereData = array();
			if($_GET["term"] != ""){
				$whereData['$or'] = array();
				$filter = new MongoRegex("/^".$_GET["term"]."/i");
				array_push($whereData['$or'],array("CountryName"=> new MongoRegex("/^".$_GET["term"]."/i")));
				array_push($whereData['$or'],array("DestinationName"=> new MongoRegex("/^".$_GET["term"]."/i")));
				//array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			$dataCollectionsResults = $db->hb_destination->find($whereData)->sort(array('CountryName' => 1,'DestinationName' => 1));

			
			$dataArray = iterator_to_array($dataCollectionsResults);
			$response = array();
			$cityChecker = array();
			foreach($dataArray as $key => $row){
				$strDes = $row['CountryName'].', '.$row['DestinationName'];

				if(!in_array($strDes, $cityChecker)){
					$dArray = array("value"=> $row['DestinationCode'],"label"=>$strDes);
					array_push($response,$dArray);
					array_push($cityChecker,$strDes);
				}
				

			}
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}

		case "getDestinationHP":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			//$tools = new Tools; 
			
			//code,destination || db.streets.find( { street_name : { $regex : '^Al', $options: 'i' } } );

			$filter = "";
			$whereData = array();
			if($_GET["query"] != ""){
				$whereData['$or'] = array();
				$filter = new MongoRegex("/^".$_GET["query"]."/i");
				array_push($whereData['$or'],array("Country"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("CityTown"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			$dataCollectionsResults = $db->hp_destination->find($whereData)->sort(array('Country' => 1,'CityTown' => 1));

			
			$dataArray = iterator_to_array($dataCollectionsResults);
			$response = array();
			$cityChecker = array();
			foreach($dataArray as $key => $row){
				/*$des = $row['CountryName'].", ".$row['CityName'].", ".$row['AreaName'];
				$des2 = $row['CountryName'].", ".$row['CityName'];
				$code = $row['CountryCode']."-".$row['CityCode']."-".$row['AreaCode'];
				$code2 = $row['CountryCode']."-".$row['CityCode'];
				$forChecking = $row['CountryName'].", ".$row['CityName'];
				if(!in_array($forChecking,$cityChecker)){
					array_push($cityChecker,$forChecking);

					
					$dArray = array("code"=> $code,"destination"=>$des);
					array_push($response,$dArray);
					
					
				}else{
					$dArray = array("code"=> $code,"destination"=>$des);
					array_push($response,$dArray);
				}*/
				//$code = $row['code']
				$dArray = array("code"=> $row['DestinationId'],"destination"=>$row['Country'].', '.$row['CityTown']);
				array_push($response,$dArray);

			}
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "getDestinationRS":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$filter = "";
			$whereData = array();

			if($_GET["query"] != ""){
				$whereData['$or'] = array();
				$filter = new MongoRegex("/^".$_GET["query"]."/i");
				array_push($whereData['$or'],array("CountryName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("DestinationName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			$dataCollectionsResults = $db->rs_destination->find($whereData)->sort(array('CountryName' => 1, 'DestinationName' => 1));

			
			$dataArray = iterator_to_array($dataCollectionsResults);
			$response = array();
			$cityChecker = array();
			foreach($dataArray as $key => $row){
				/*$des = $row['CountryName'].", ".$row['CityName'].", ".$row['AreaName'];
				$des2 = $row['CountryName'].", ".$row['CityName'];
				$code = $row['CountryCode']."-".$row['CityCode']."-".$row['AreaCode'];
				$code2 = $row['CountryCode']."-".$row['CityCode'];
				$forChecking = $row['CountryName'].", ".$row['CityName'];
				if(!in_array($forChecking,$cityChecker)){
					array_push($cityChecker,$forChecking);

					
					$dArray = array("code"=> $code,"destination"=>$des);
					array_push($response,$dArray);
					
					
				}else{
					$dArray = array("code"=> $code,"destination"=>$des);
					array_push($response,$dArray);
				}*/
				//$code = $row['code']
				//$dArray = array("code"=> $row['DestinationId'],"destination"=>$row['Country'].', '.$row['CityTown']);
				$dArray = array("code" => $row["DestinationCode"], "destination" => $row["CountryName"] . ', ' . $row["DestinationName"]);
				array_push($response,$dArray);

			}
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "getDestinationCountry":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			//$tools = new Tools; 
			
			//code,destination || db.streets.find( { street_name : { $regex : '^Al', $options: 'i' } } );

			$filter = "";
			$whereData = array();
			if($_GET["query"] != ""){
				//$whereData['$or'] = array();
				$filter = new MongoRegex("/^".$_GET["query"]."/i");
				$whereData['CountryName'] = $filter;
				//array_push($whereData['$or'],array("CountryName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("city"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			$dataCollectionsResults = $db->main_destination->distinct('CountryName',$whereData);

			//print_r($dataCollectionsResults);
			//$dataArray = iterator_to_array($dataCollectionsResults);

			//print_r($dataCollectionsResults);
			//die();
			$response = array();
			//$cityChecker = array();
			foreach($dataCollectionsResults as $key => $row){
				
				$dArray = array("code"=> $row,"destination"=>$row);
				array_push($response,$dArray);

			}
			usort($response, function($a, $b) {
			    return strcasecmp($a['destination'], $b['destination']);
			});
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "getDestinationCity":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			//$tools = new Tools; 
			
			//code,destination || db.streets.find( { street_name : { $regex : '^Al', $options: 'i' } } );

			$filter = "";
			$whereData = array();
			if($_GET["countryCode"] != ""){
				//$whereData['$or'] = array();
				//$filter = new MongoRegex("/^".$_GET["query"]."/i");
				$whereData['CountryName'] = $_GET["countryCode"];
				//array_push($whereData['$or'],array("CountryName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("city"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			$dataCollectionsResults = $db->main_destination->distinct('CityName',$whereData);

			//print_r($dataCollectionsResults);
			//$dataArray = iterator_to_array($dataCollectionsResults);

			//print_r($dataCollectionsResults);
			//die();
			$response = array();
			//$cityChecker = array();
			foreach($dataCollectionsResults as $key => $row){
				
				$dArray = array("code"=> $row,"destination"=>$row);
				array_push($response,$dArray);

			}
			usort($response, function($a, $b) {
			    return strcasecmp($a['destination'], $b['destination']);
			});
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		
		
		
		/* ENDLOGIN*/
	}
?>