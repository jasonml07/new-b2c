<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {

		case "addSupplier":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			$resPOST = json_decode($_POST['postDetails'], true);

			$id = $resPOST['supplier_id'];

			if($id == 0){
				
				unset($resPOST['supplier_id']);
				$res = $db->main_supplier->insert($resPOST);
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			}else{

				$mongoID = new MongoID($resPOST['supplier_id']);


				unset($resPOST['supplier_id']);
				
				

				$res = $db->main_supplier->update(array("_id"=> $mongoID), array("\$set" => $resPOST));
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}	
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}


		case "view":{


			$connection = new MongoClient();
			$db = $connection->db_system;
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['supplier_name'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			$dataCollections = $db->main_supplier->find($whereData);

			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_supplier'] = $key;
				$data['supplier_code'] = $row['supplier_code'];
				$data['supplier_name'] = $row['supplier_name'];
				$data['is_active'] = $row['is_active'];
				$data['address'] = $row['address'];
				$data['city'] = $row['city'];
				$data['state'] = $row['state'];
				$data['country'] = $row['country'];
				$data['email'] = $row['email'];
				$data['phone'] = $row['phone'];
				$data['fax'] = $row['fax'];
				$data['b_first_name'] = $row['b_first_name'];
				$data['b_last_name'] = $row['b_last_name'];
				$data['b_address'] = $row['b_address'];
				$data['b_city'] = $row['b_city'];
				$data['b_state'] = $row['b_state'];
				$data['b_country'] = $row['b_country'];
				$data['b_email'] = $row['b_email'];
				$data['b_fax'] = $row['b_fax'];
				$data['currency'] = $row['currency'];
				array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewSupplier" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;

  	  	break; 
		}
		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['id_supplier']);
			
			$db->main_supplier->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 1)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['id_supplier']);
			
			$db->main_supplier->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 0)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "getSupplierDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['supplier_id']);
			$res = $db->main_supplier->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['supplier_id'] = $_POST['supplier_id'];
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "getSupplier":{
			$connection = new MongoClient();

			$db = $connection->db_system;

			$whereData = array();
			$whereData['is_active'] = 1;
			if(isset($_GET["query"])){
				$whereData['supplier_name'] = new MongoRegex("/^".$_GET["query"]."/i");
				
				
			}
			
			

			//print_r($whereData);db.main_destination.find

			$dataCollections = $db->main_supplier->find($whereData);


			$dataArray = iterator_to_array($dataCollections);

			

			$arrayResults = array();

			foreach($dataArray as $key => $row){


				$data = array();

				$data['supplier_code'] = $row['supplier_code'];

				$data['supplier_name'] = $row['supplier_name'];

				


			 	array_push($arrayResults,$data);

			}
			usort($arrayResults, function($a, $b) {
			    return strcasecmp($a['supplier_name'], $b['supplier_name']);
			});

		

			print json_encode($arrayResults);

  	   		

  	  		exit;
  	  	break; 
		}
		
		/* ENDLOGIN*/
	}
?>