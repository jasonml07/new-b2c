<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateRSDestination":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			
			

			
		
			$id = $_POST['restel_destination_id'];
			
				
			
			
			
			if($id==0){
				
				
			
				$insertData = array();

				
				$insertData['CountryCode'] = $_POST['CountryCode'];
				$insertData['CountryName'] = $_POST['CountryName'];
				$insertData['DestinationCode'] = $_POST['DestinationCode'];
				$insertData['DestinationName'] = $_POST['DestinationName'];
			

				$res = $db->rs_destination->insert($insertData);
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
			
			}else{

				

				$mongoID = new MongoID($_POST['hotelBeds_destination_id']);
				$updateData = array();

				$updateData['CountryCode'] = $_POST['CountryCode'];
				$updateData['CountryName'] = $_POST['CountryName'];
				$updateData['DestinationCode'] = $_POST['DestinationCode'];
				$updateData['DestinationName'] = $_POST['DestinationName'];
				
				

				$res = $db->rs_destination->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("CountryName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("DestinationName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				
			}
			//print_r($whereData);
			$dataCollections = $db->rs_destination->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(1000);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_rs_destination'] = $key;
				$data['CountryCode'] = $row['CountryCode'];
				$data['CountryName'] = $row['CountryName'];
				$data['DestinationCode'] = $row['DestinationCode'];
				$data['DestinationName'] = $row['DestinationName'];

			 	array_push($arrayResults,$data);
			}

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewRSDestination" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		
		
		/* ENDLOGIN*/
	}
?>