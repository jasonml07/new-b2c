<?php
require_once('../_classes/tools.class.php');


$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		case "addProduct":{
			$resPOST = json_decode($_POST['postDetails'], true);
			
			$tools = new Tools; 
			
			if(isset($resPOST['productId']) && $resPOST['productId']=='New'){

				//print_r($resPOST);

				$productID = $tools->getNextID("Product");
				$resPOST['productId'] = (int)$productID;
				

				unset($resPOST['action']);
				$connection = new MongoClient();
				$db = $connection->db_system;

				$db->products->insert($resPOST);
				$response =  array("status"=>true);
				print json_encode($response);
				$connection->close();
				

			}else{
				//print_r($resPOST);
				

				$connection = new MongoClient();
				$db = $connection->db_system;
				$resPOST['productId'] = (int)$resPOST['productId'];
				
				unset($resPOST['action']);
				//print_r($resPOST);
				$db->products->update(array('productId'=>(int)$resPOST['productId']),array('$set' =>$resPOST));
				


				$response =  array("status"=>true);
				print json_encode($response);
				$connection->close();
			}
			
			

			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;	
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("productName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelDetail.ratings"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("productId"=> new MongoRegex("/^".$_GET["query"]."/i")));
				
			}
			//print_r($whereData);
			$dataCollections = $db->products->find($whereData,array('_id'=>0));
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(100);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewProducts" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			$db->products->update(array("productId"=> (int)$_POST['id_product']),array("\$set"=> array("is_active"=> 1)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;

		
			$db->products->update(array("productId"=> (int)$_POST['id_product']),array("\$set"=> array("is_active"=> 0)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "clone":{

			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			
			$resPOST = $db->products->findOne(array("productId"=> (int)$_POST['id_product']),array("_id"=>0));
			$productID = $tools->getNextID("Product");
			$resPOST['productId'] = (int)$productID;

			$db->products->insert($resPOST);
			$response = array ( "success" => true,"productID"=>$productID);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "forwardBooking":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 

			$resPOST = $db->products->findOne(array("productId"=> (int)$_POST['id_product']),array("_id"=>0));
			$bookingId = (int)$_POST['bookingId'];
			$adultMin = (int)$_POST['adultMin'];
			$adultMax = (int)$_POST['adultMax'];
			$childMin = (int)$_POST['childMin'];
			$childMax = (int)$_POST['childMax'];
			$infantMin = (int)$_POST['infantMin'];
			$infantMax = (int)$_POST['infantMax'];
			$dataInsert = array();

				$dataInsert['bookingId'] = $bookingId;
				$dataInsert['itemId'] = $tools->getNextID("item");

				$dt1 = new DateTime($resPOST['startDate']);
				$ts1 = $dt1->getTimestamp();
				$booking_services_start_date = new MongoDate($ts1);

				$dt2 = new DateTime($resPOST['endDate']);
				$ts2 = $dt2->getTimestamp();
				$booking_services_end_date = new MongoDate($ts2);

				$dataInsert['itemStartDate'] = $booking_services_start_date;
				$dataInsert['itemEndDate'] = $booking_services_end_date;
				$dataInsert['itemName'] = $resPOST['productName'];
				$dataInsert['itemPhone'] = $resPOST['productPhone'];
				$dataInsert['itemRatings'] = $resPOST['productRatings'];
				$dataInsert['itemPropertyCategory'] = $resPOST['productPropertyCategory'];
				$dataInsert['itemServiceCode'] = $resPOST['productServiceCode'];
				$dataInsert['itemServiceName'] = $resPOST['productServiceName'];
				$dataInsert['itemSupplierCode'] = $resPOST['productSupplierCode'];
				$dataInsert['itemSupplierName'] = $resPOST['productSupplierName'];
				$dataInsert['itemSumarryDescription'] = $resPOST['productSumarryDescription'];
				$dataInsert['itemDetailedDescription'] = $resPOST['productDetailedDescription'];
				$dataInsert['itemInclusionDescription'] = $resPOST['productInclusionDescription'];
				$dataInsert['itemExclusionDescription'] = $resPOST['productExclusionDescription'];
				$dataInsert['itemConditionsDescription'] = $resPOST['productConditionsDescription'];
				$dataInsert['itemSuplierReference'] = $resPOST['productSuplierReference'];
				$dataInsert['itemFromCountry'] = $resPOST['productFromCountry'];
				$dataInsert['itemFromCity'] = $resPOST['productFromCity'];
				$dataInsert['itemPhone'] = $resPOST['productFromAddress'];
				$dataInsert['itemToCountry'] = $resPOST['productToCountry'];
				$dataInsert['itemToAddress'] = $resPOST['productToAddress'];
				$dataInsert['itemCostings'] = array();
				$dataInsert['itemCancellation'] = array();
				$dataInsert['itemAdultMax'] = $adultMax;
				$dataInsert['itemAdultMin'] = $adultMin;
				$dataInsert['itemChildMax'] = $childMax;
				$dataInsert['itemChildMin'] = $childMin;
				$dataInsert['itemInfantMax'] = $infantMax;
				$dataInsert['itemInfantMin'] = $infantMin;
				$dataInsert['itemSupplements'] = array();	
				$dataInsert['itemIsCancelled'] = 0;
				$dataInsert['itemIsPaxAllocated'] = 0;
				$dataInsert['itemIsReceipt'] = 0;
				$dataInsert['itemIsInvoiced'] = 0;
				$dataInsert['itemIsPaid'] = 0;
				$dataInsert['itemIsConfirmed'] = 0;
				$dataInsert['itemIsBlank'] = 0;
				$dataInsert['itemIsLive'] = 0;
				
				$db->items->insert($dataInsert);

				$response = array ( "status" => true,"bookingID"=>$bookingId, "productID"=>$_POST['id_product']);
				print json_encode($response);
				$connection->close();
	  	   		exit;
  	  	break;  
		}

		/* ENDLOGIN*/
	}
?>