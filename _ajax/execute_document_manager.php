<?php

require_once('../_classes/tools.class.php');
require_once('../_classes/item.class.php');
require_once('../_classes/class.phpmailer.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		
		case "getPath":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("documentId"=>(int) $_GET['document_id']);

		
			$dataCollections = $db->documents->findOne($whereData);
			$arrayResults = array("status"=>false, "path"=>"");
			if($dataCollections){
				$arrayResults = array("status"=>true, "path"=>$dataCollections['filename']);
			}
				
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		case "sendMail":{
			
			
			$mail = new PHPMailer;
			$mail->IsHTML(true);
			//Set who the message is to be sent from
			$mail->setFrom($_POST['from_doc_email']);
			//Set an alternative reply-to address
			$mail->addReplyTo($_POST['from_doc_email']);
			//Set who the message is to be sent to
			$emailToArr = explode(";",str_replace(' ', '', $_POST['to_doc_email']));
			foreach($emailToArr as $emailTo){
				$mail->addAddress($emailTo);
			}
			
			$emailCCArr = explode(";",str_replace(' ', '', $_POST['cc_doc_email']));
			foreach($emailCCArr as $emailCC){
				$mail->AddCC($emailCC);
			}
			$emailBCCArr = explode(";",str_replace(' ', '', $_POST['bcc_doc_email']));
			foreach($emailBCCArr as $emailBCC){
				$mail->AddBCC($emailCC);
			}
			//Set the subject line
			$mail->Subject = $_POST['subject_doc_email'];
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body

			$mail->msgHTML(str_replace("\n", "<br>", $_POST['msg_doc_email']));
		   	//$mail->msgHTML($tempHTML);
			//Replace the plain text body with one created manually
			//$mail->AltBody = 'This is a plain-text message body';
			//Attach an image file
			//$mail->addAttachment('../_documents/'.$_POST['doc-path']); http://208.97.188.14/_documents/voucher-60-481.pdf

			$url = 'http://208.97.188.14/_documents/'.$_POST['doc-path'];
			$binary_content = file_get_contents($url);

			// You should perform a check to see if the content
			// was actually fetched. Use the === (strict) operator to
			// check $binary_content for false.
			
			// $mail must have been created
			$mail->AddStringAttachment($binary_content, $_POST['doc-path'], $encoding = 'base64', $type = 'application/pdf');




			//send the message, check for errors
			if (!$mail->send()) {
			    $response = array ( "failure" => true);
			} else {
			    $response = array ( "success" => true);
			}

			
				
			print json_encode($response);
  	   		exit;
  	  	break; 
		}
		case "createVoucherItems":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			//var_dump(json_decode($_POST['itemIDs']));
			$whereData = array('bookingId'=>(int)$_POST['bookingID'],"itemId"=>array("\$in"=>json_decode($_POST['itemIDs'])));

			

			$dataCollectionsResults = $db->items->find($whereData);

			//print_r($dataCollectionsResults);
			try {
				$dataArray= iterator_to_array($dataCollectionsResults);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}

			try {
			   $dataArray = iterator_to_array($dataCollectionsResults);
			   
			   foreach($dataArray as $key => $row){
			   		$paxArray = array();
			   		$whereData = array('item_id'=>(int)$row['itemId']);

					$dataCollectionsResults2 = $db->item_guest->find($whereData);
					try {
						$dataArray2 = iterator_to_array($dataCollectionsResults2);
						 foreach($dataArray2 as $key2 => $row2){
						 	$row2['guestId'] = $key2;
						 	array_push($paxArray, $row2);

						 }
					} catch (Exception $e) {
					   //$dataArray = array();
					}
					//$dataArray[$key]['itemGuests'] = $paxArray;
					$dataArray[$key]['itemGuests'] = $paxArray;

					
			   }
			} catch (Exception $e) {
			   $dataArray = array();
			}
			//print_r($dataArray);

			//print_r($dataArray);
			//die();
			$dataItemToVoucher = array();

			$message = "";
			
			if(count($dataArray) !=0){
				

				foreach($dataArray as $key => $row){



						$itemObj = new Item();
						$isVoucher = $itemObj->createVoucher((int)$_POST['bookingID'],$row);
						
						$message .= "Item #".$row['itemId']." has been voucher.</br>";
						//echo $message;
					
					
				}	
			}
			if($message !=""){
				$response = array ( "status" => true, "message" => $message,"booking_id"=>(int)$_POST['bookingID']);
			}else{
				$response = array ( "status" => false, "message" => "No Item has been vouchered.");
			}
			
			
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		}
		
		
		/* ENDLOGIN*/
	}
?>