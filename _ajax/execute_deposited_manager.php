<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "save":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			
			/*action	
save
deposited_booking_id	
585
deposited_due_date	
25/08/2015
deposited_id	
0
deposited_reference	
rtest
deposited_required_amount	
123*/

			
			$id = $_POST['deposited_id'];
			
			
			
			
			
			if($id==0){
				
				
				$_POST['deposited_id'] = (int)$tools->getNextID("deposited");
				#$code = $tools->generateCode($_POST['aliasname']); 

				unset($_POST['action']);
				$xDate = explode("/", $_POST['deposited_due_date']);
				$dt = new DateTime($xDate[2]."-".$xDate[1]."-".$xDate[0]);
			
				$ts = $dt->getTimestamp();
				$_POST['deposited_due_date'] = new MongoDate($ts);
				$_POST['deposited_booking_id'] = (int)$_POST['deposited_booking_id'];
				$_POST['deposited_is_paid'] = (int)$_POST['deposited_is_paid'];
				$_POST['deposited_required_amount'] = (float)$_POST['deposited_required_amount'];
				$dt = new DateTime(date('Y-m-d'));
				$ts = $dt->getTimestamp();
				$_POST['deposited_created_datetime'] = new MongoDate($ts);
				$res = $db->deposits->insert($_POST);
			
				if($res){
					$response = array ( "success" => true);
				}else{
					$response = array ( "failure" => true);
				}
			
			
			}else{

				unset($_POST['action']);

				$xDate = explode("/", $_POST['deposited_due_date']);
				$dt = new DateTime($xDate[2]."-".$xDate[1]."-".$xDate[0]);
			
				$ts = $dt->getTimestamp();

				$_POST['deposited_due_date'] = new MongoDate($ts);
				$_POST['deposited_booking_id'] = (int)$_POST['deposited_booking_id'];
				$_POST['deposited_is_paid'] = (int)$_POST['deposited_is_paid'];
				$_POST['deposited_required_amount'] = (float)$_POST['deposited_required_amount'];
				$_POST['deposited_id'] = (int)$_POST['deposited_id'];
				$res = $db->deposits->update(array("deposited_id"=> (int)$_POST['deposited_id']), array("\$set" => $_POST));
				
			
				if($res){
					$response = array ( "success" => true);
				}else{
					$response = array ( "failure" => true);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			
			
			$whereData = array("deposited_booking_id"=>(int)$_GET['bookingId']);

			
			$dataCollections = $db->deposits->find($whereData,array("_id"=>0));
			

			try {
			   $dataArray = iterator_to_array($dataCollections);
			   
			   
			} catch (Exception $e) {
			   $dataArray = array();
			}
			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewDeposited" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "getAccounts":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("is_active"=>"Y");

			
			//print_r($whereData);db.main_destination.find
			$dataCollections = $db->sys_user->find($whereData);

			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['account_code'] = $row['account_code'];
				$data['account_name'] = $row['account_name'];
				

			 	array_push($arrayResults,$data);
			}
			/*$mysql = new Database; 
			$filter = "";
			if($_GET["query"] != ""){
			$filter = "and  agency_name like '%".$_GET["query"]."%'";
			}
			
			$sql = "SELECT id_agency, agency_name FROM agency where is_active = 1 $filter ORDER BY agency_name ASC;";
			$service = $mysql->select_execute_query($sql);*/
  	  		
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		case "cancel":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			$db->deposits->remove(array("deposited_id"=> (int)$_POST['deposited_id']),array("justOne" => true));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['user_id']);
			
			$db->sys_user->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> "N")));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		
		/* ENDLOGIN*/
	}
?>