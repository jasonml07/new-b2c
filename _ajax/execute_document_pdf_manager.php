<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		
		case "view": {

			$connection = new MongoClient();
			$db = $connection->db_system;
			$whereData = array();

			if(isset($_GET["query"])){ //search by created datetime

				$dateTemp = explode("/", $_GET["query"]);
				if(count($dateTemp)==3){
					
					//echo strtotime($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]." 00:00:00 +1 days");
					$start = new MongoDate(strtotime($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]." 00:00:00 -1 days"));
					$end = new MongoDate(strtotime($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]." 00:00:00"));
					$whereData['created_datetime']  =array('$gte' => $start, '$lte' => $end);
					//print_r($whereData);
					//$whereData['created_datetime']=new MongoDate(strtotime($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]." 00:00:00"));

				}else{
					
					$whereData['code'] =  new MongoRegex("/".$_GET["query"]."$/i");
				}
			}
			//print_r($whereData);
			$dataCollections = $db->documents->find($whereData);

			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['documentId'] = (int)$row['documentId'];
				$data['created_datetime'] = $row['created_datetime'];
				$data['filename'] = $row['filename'];
				$data['type'] = $row['type'];
				$data['code'] = $row['code'];

				array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewDocumentPdf" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;

  	  	break; 
		}

		case "addConsultant":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			$resPOST = json_decode($_POST['postDetails'], true);
			$id = $resPOST['agency_consultants_id'];

			if($id == 0){
				
				unset($resPOST['agency_consultants_id']);
				$tool = new Tools();
				$resPOST['code'] = $tool->codeGeneRatorAndChecker("agency",$resPOST['code']);
				$resPOST['password'] = md5($resPOST['password']);
				$res = $db->agency_consultants->insert($resPOST);
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			}else{

				$mongoID = new MongoID($resPOST['agency_consultants_id']);


				unset($resPOST['agency_consultants_id']);
				$res = $db->agency_consultants->update(array("_id"=> $mongoID), array("\$set" => $resPOST));
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}	
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}


		case "getAgencyConsultantDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['agency_consultants_id']);
			$res = $db->agency_consultants->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['agency_consultants_id'] = $_POST['agency_consultants_id'];
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_agency_consultants']);
			
			$db->agency_consultants->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 1)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_agency_consultants']);
			
			$db->agency_consultants->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> 0)));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}


	
	}
?>