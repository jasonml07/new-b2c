<?php
session_start();

if (!isset($_SESSION['is_logged_B2B'])) { //checks if user is logged.. if not redirects to login page.		
	die();
} 
set_time_limit(0);
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));

define('SYS_MARKUP', 35);
define('SYS_COMM', 15);
define('SYS_DISC', 0);
define('SYS_GST', 0);
define('SYS_CANCEL_PCT', 5);

require_once('../_classes/tools.class.php');
require_once('../_classes/hotelBeds.class.php');



$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		
		
		case "doSearchHB":{

			$_SESSION["SEARCH_RESULTS"] = array();
			$tools = new Tools; 
			$dataGET = json_decode($_GET['postDetails']);
			
			$connection = new MongoClient();
			$db = $connection->db_system;


			$destination = $db->hb_destination->findOne(array("DestinationCode"=> $dataGET->desitnation_code), array("_id"=>0));
			$countryName = $destination['CountryName'];
			$hbObj = new HotelBedsClient();
			$hotels = $hbObj->get_available_hotels($dataGET);
			
			if(!(array)$hotels || !(array)$hotels->ServiceHotel || count($hotels->ServiceHotel) <= 0){

				$response = array ( "success" => true, "total" => 0, "viewMainHotelsHB" => array(),"timeToExpiration"=>0,"token"=>"","flitersData"=>array("maxAmount"=>0,"minAmount"=>(float)0,"zoneList"=>array(),"categoryList"=>array()));
  	  		
	  	  		print json_encode($response);
	  	  		$connection->close();
	  	  		exit;
			}
			
			
			$echoToken = (string)$hotels->attributes()->echoToken;
			$timeToExpiration = (float)$hotels->attributes()->timeToExpiration;


			$serviceHotels = $hotels->ServiceHotel;

			$currencyVaidationList = array("Euro" =>"EUR","US Dollar"=>"USD","Australian Dollar"=>"AUD");

			$currEx = array();
			$hotelArrayResults = array();
			$numberOfRooms = count($dataGET->pax);
			$ctr = 0;
			$maxAmount =0;
			$zoneListArray = array();
			$categoryList =array();
			foreach($serviceHotels as $key=>$hotel){


				$curr = (string)$hotel->Currency->attributes()->code;

				$defaultAmount = (float) 1000000; 
				if(array_key_exists($curr,$currencyVaidationList)){
					$curr = $currencyVaidationList[$curr];
				}
				if(!array_key_exists($curr,$currEx)){
					$resCurr = $db->currency_ex->findOne(array("curr_from"=> $curr), array("_id"=>0));
					$currEx[$curr] = (float) $resCurr['rate_to'];
				}
				$dateFrom = (string)$hotel->DateFrom->attributes()->date;
				$dateTo = (string)$hotel->DateTo->attributes()->date;
				$availableToken  = (string)$hotel->attributes()->availToken;
				$contractName  = (string)$hotel->ContractList->Contract->Name;
				$contractOficeCode  = (string)$hotel->ContractList->Contract->IncomingOffice->attributes()->code;
				$classification  = (string)$hotel->ContractList->Contract->Classification;
				$classificationCode  = (string)$hotel->ContractList->Contract->Classification->attributes()->code;


				if (strpos($contractName,'NRF') !== false || strpos($classificationCode,'NRF') !== false) {
				    $classificationCode = "NRF";
				}
				

				
				if(!array_key_exists((string)$hotel->HotelInfo->Code, $hotelArrayResults)){
					$hotelDetails = array();
					//premary details
					try{
						$xmlHotel=simplexml_load_file("./ING/".$hotel->HotelInfo->Code.".xml");
						$jsonHotels = json_encode($xmlHotel);
						$fileHotels = json_decode($jsonHotels,TRUE);

					}catch(Exception $e){
						$fileHotels =  array();
					}
					
					
					

					$hotelDetails['hotelAddress']= (isset($fileHotels['locations']['address'])) ? $fileHotels['locations']['address'] : "";
					$hotelDetails['hotelPhone']= (isset($fileHotels['locations']['phoneHotel'])) ? $fileHotels['locations']['phoneHotel'] : "";
					$hotelDetails['hotelEmail']= (isset($fileHotels['locations']['email'])) ? $fileHotels['locations']['email'] : "";
					$hotelDetails['hotelFax']= (isset($fileHotels['locations']['fax'])) ? $fileHotels['locations']['fax'] : "";
					$hotelDetails['hotelDescription']= (isset($fileHotels['descriptions']['descriptionHotel']["description"])) ? $fileHotels['descriptions']['descriptionHotel']["description"] : "";

					$hotelDetails['hotelCode'] = (string)$hotel->HotelInfo->Code;
					$hotelDetails['hotelName'] = (string)$hotel->HotelInfo->Name;
					$hotelDetails['hotelImage'] = (string)$hotel->HotelInfo->ImageList->Image[0]->Url;
					$hotelDetails['hotelCategory'] = (string)$hotel->HotelInfo->Category;

					$destinationCode = (string)$hotel->HotelInfo->Destination->attributes()->code;
					$destinationType = (string)$hotel->HotelInfo->Destination->attributes()->type;

					$thisHotelCode = (string)$hotel->HotelInfo->Code;
					$zoneName = (string)$hotel->HotelInfo->Destination->ZoneList->Zone;
					$zoneCode = (string)$hotel->HotelInfo->Destination->ZoneList->Zone->attributes()->code;
					$zoneType = (string)$hotel->HotelInfo->Destination->ZoneList->Zone->attributes()->type;
					
					$hotelDetails['hotelDestination']= $countryName.', '.(string)$hotel->HotelInfo->Destination->Name;
					$hotelDetails['hotelDestinationZone']= $zoneName;

					if(!in_array($zoneName, $zoneListArray)){
						array_push($zoneListArray,$zoneName);
					}
					if(!in_array((string)$hotel->HotelInfo->Category, $categoryList)){
						array_push($categoryList,(string)$hotel->HotelInfo->Category);
					}
					$hotelDetails['hotelPosition']= array("latitude"=>(string)$hotel->HotelInfo->Position->attributes()->latitude,"longitude"=>(string)$hotel->HotelInfo->Position->attributes()->longitude);
					$hotelDetails['hotelAvailableRooms'] = array();

					$hotelDetails['hotelCurrTo'] = 'AUD';
					$hotelDetails['hotelConvertedTotalPrice'] = 100000000;

					$hotelDetails['hotelCalculatedPrice'] = array();

				}
				
				//$hotelDetails['displayAmount'] = "AUD";
				if($numberOfRooms>1){


					$arrayBoard = array();
					//$arrayRoomsComb = array();

					foreach($hotel->AvailableRoom as $key2 => $rooms){


						//$paxKey = $rooms['HotelOccupancy']['Occupancy']['AdultCount'].'-'.$rooms['HotelOccupancy']['Occupancy']['ChildCount'];
						//$roomKey = $rooms['HotelRoom']['RoomType']."|".$paxKey.'=';

						if(!array_key_exists((string)$rooms->HotelRoom->Board, $arrayBoard)){
							$arrayBoard[(string)$rooms->HotelRoom->Board] = array();
							
						}

						$arrayBoard[(string)$rooms->HotelRoom->Board][] = $rooms;
						

					}
		

					//echo $dataGET->pax[0]->adult;
					foreach($arrayBoard as $boarTypeKey=>$roomComb){
						


						$combinationResults = $tools->roomCombination($roomComb,$dataGET->pax,$numberOfRooms);
						
						#print_r($combinationResults);
						
						/*if(!$combinationResults || empty($combinationResults)){
							print_r($roomComb);
							print_r($combinationResults);
						}*/
						foreach($combinationResults as $val){
							
							$roomDetailsComb = array();
							$roomDetailsComb['boardType'] = $boarTypeKey;
							$roomDetailsComb['roomDetails'] = array();
							$roomDetailsComb['echoToken'] = $echoToken;
							$roomDetailsComb['dateFrom'] = $dateFrom;
							$roomDetailsComb['dateTo'] = $dateTo;
							$roomDetailsComb['availableToken'] = $availableToken;
							$roomDetailsComb['contractName'] = $contractName;
							$roomDetailsComb['contractOficeCode'] = $contractOficeCode;

							$roomDetailsComb['classification'] = $classification;
							$roomDetailsComb['classificationType'] = $classificationCode;

							$roomDetailsComb['hotelCode'] = $hotelDetails['hotelCode'];
							$roomDetailsComb['destinationCode'] = $destinationCode;
							$roomDetailsComb['destinationType'] = $destinationType;
							$roomDetailsComb['zoneCode'] = $zoneCode;
							$roomDetailsComb['zoneName'] = $zoneName;
							$roomDetailsComb['zoneType'] = $zoneType;




							$roomDetailsComb['paxes'] = $dataGET->pax;


							
							$priceRoom = 0;
							$pricePolicyCalculation = 0;
							$dateFromInt = 100000000;

							
							
							foreach($val as $roomAvailable){
								//$roomAvailable['HotelRoom']['@attributes']['SHRUI']
								$priceRoom += (float) $roomAvailable->HotelRoom->Price->Amount;
								$roomDet = array("name"=>(string)$roomAvailable->HotelRoom->RoomType);
								$roomDet['SHRUI'] = (string)$roomAvailable->HotelRoom->attributes()->SHRUI;
								$roomDet['onRequest'] = (string)$roomAvailable->HotelRoom->attributes()->onRequest;
								$roomDet['boardType'] = (string)$roomAvailable->HotelRoom->Board->attributes()->type;
								$roomDet['boardCode'] = (string)$roomAvailable->HotelRoom->Board->attributes()->code;
								$roomDet['roomType'] = (string)$roomAvailable->HotelRoom->RoomType->attributes()->type;
								$roomDet['roomCode'] = (string)$roomAvailable->HotelRoom->RoomType->attributes()->code;
								$roomDet['roomChar'] = (string)$roomAvailable->HotelRoom->RoomType->attributes()->characteristic;
								
								$roomDet['paxes'] = array("adult"=>(int)$roomAvailable->HotelOccupancy->Occupancy->AdultCount,"child"=>(int)$roomAvailable->HotelOccupancy->Occupancy->ChildCount);
								array_push($roomDetailsComb['roomDetails'],$roomDet);

								$amountCal = (float)$roomAvailable->HotelRoom->CancellationPolicy->Price->Amount;
								$pricePolicyCalculation += $amountCal;
								$tempDate = (int)$roomAvailable->HotelRoom->CancellationPolicy->Price->DateTimeFrom->attributes()->date;
								if($dateFromInt>$tempDate){
									$dateFromInt = (int)$tempDate;
									$calDateFrom = (string)$roomAvailable->HotelRoom->CancellationPolicy->Price->DateTimeFrom->attributes()->date;
								}
								

							}


							if($classificationCode == "NRF"){
								$roomDetailsComb['classificationType'] = 'NRF';
								$roomDetailsComb['classification'] = "Non Refundable";

								$canPolicy = array();
								$canPolicy['dateFrom'] = date('Ymd');
								$canPolicy['dateTo'] = $dateFrom;
								$canPolicy['type'] = "Percent";
								$canPolicy['value'] =100;
								$roomDetailsComb['cancellationPolicyDetails'] = $canPolicy;
							}else{
								$resDateDiff = $tools->dateDifference((string)date('Ymd'),$calDateFrom);
								
								if((int)$resDateDiff["days"]<=5){
									$roomDetailsComb['classificationType'] = 'NRF';
									$roomDetailsComb['classification'] = "Non Refundable";
									$canPolicy = array();
									$canPolicy['dateFrom'] = date('Ymd');
									$canPolicy['dateTo'] = $dateFrom;
									$canPolicy['type'] = "Percent";
									$canPolicy['value'] =100;
									$roomDetailsComb['cancellationPolicyDetails'] = $canPolicy;
								}else{
									$canPolicy = array();
									$canPolicy['dateFrom'] = $calDateFrom;
									$canPolicy['dateTo'] = $dateFrom;
									$canPolicy['type'] = "Percent";
									$canPolicy['value'] =100;
									$roomDetailsComb['cancellationPolicyDetails'] = $canPolicy;
								}
								
							}
							

							$originalAmount = (float)$priceRoom;
							$orginalNetAmt = (float)$priceRoom*$currEx[$curr];
							$netAmountInAgency = round($orginalNetAmt);
							$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
							$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
							$commInAmountTemp = $grossAmt*(SYS_COMM/100);
							$commAmt = round($commInAmountTemp);
							$discInAmountTemp = $grossAmt*(SYS_DISC/100);
							$discAmt = round($discInAmountTemp);
							$totalLessAMOUNTT = round($commAmt+$discAmt);
							$totaldueamt = $grossAmt-$totalLessAMOUNTT;
							$totalProfitamt = $totaldueamt-$netAmountInAgency;
							$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
							$gstAmt = $totalProfitamt*(SYS_GST/100);


							$roomDetailsComb['currTo'] = 'AUD';
							$roomDetailsComb['convertedTotalPrice'] = $totaldueamt;


							$calculatedPrice = array(
									"fromCurrency"=> $curr,
									"fromRate"=> 1,
									"toCurrency"=> 'AUD',
									"toRate"=> $currEx[$curr],
									"originalAmount"=> $originalAmount,
									"orginalNetAmt"=> $orginalNetAmt,
									"netAmountInAgency"=> $netAmountInAgency,
									"markupInAmount"=>$markupInAmountTemp,
									"markupInPct"=>SYS_MARKUP,
									"grossAmt"=> $grossAmt,
									"commInPct"=>SYS_COMM,
									"commInAmount"=>$commInAmountTemp,
									"commAmt"=>$commAmt,
									"discInPct"=>SYS_DISC,
									"discInAmount"=>$discInAmountTemp,
									"discAmt"=>$discAmt,
									"totalLessAmt"=> $totalLessAMOUNTT,
									"totalDueAmt"=> $totaldueamt,
									"totalProfitAmt"=> $totalProfitamt,
									"gstInPct"=>SYS_GST,
									"gstInAmount"=>$gstInAmountTemp,
									"gstAmt"=>$gstAmt


								);
							$roomDetailsComb['calculatedPrice'] = $calculatedPrice;

							#echo $hotelDetails['hotelConvertedTotalPrice'].'|'.$grossAmt."<br>";
							if(!array_key_exists((string)$hotel->HotelInfo->Code, $hotelArrayResults)){
								if((float)$hotelDetails['hotelConvertedTotalPrice']>(float)$totaldueamt){
										$hotelDetails['hotelCurrTo'] = 'AUD';
										$hotelDetails['hotelConvertedTotalPrice'] = (float)$totaldueamt;
										if($maxAmount<(float)$totaldueamt){
											$maxAmount = (float)$totaldueamt;
										}

										$hotelDetails['hotelCalculatedPrice'] = $calculatedPrice;
										
								}
								array_push($hotelDetails['hotelAvailableRooms'],$roomDetailsComb);
							}else{
								if((float)$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelConvertedTotalPrice']>(float)$totaldueamt){
										$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelCurrTo'] = 'AUD';
										$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelConvertedTotalPrice'] = (float)$totaldueamt;
										if($maxAmount<(float)$totaldueamt){
											$maxAmount = (float)$totaldueamt;
										}
										$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelCalculatedPrice'] = $calculatedPrice;
										
								}

								array_push($hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelAvailableRooms'],$roomDetailsComb);

							}


						}
						
					}
					
				}else{
					
					$priceRoom = 0;
					$pricePolicyCalculation = 0;
					$dateFromInt = 100000000;

					if(!isset($hotel->AvailableRoom[0])){
						$AvailableRoom = array($hotel->AvailableRoom);
						//print_r($AvailableRoom);
					}else{
						$AvailableRoom = $hotel->AvailableRoom;
					}

					foreach($AvailableRoom as $key2 => $rooms){
						$roomDetailsComb = array();
						$roomDetailsComb['boardType'] = (string)$rooms->HotelRoom->Board;
						$roomDetailsComb['roomDetails'] = array();
						$roomDetailsComb['echoToken'] = $echoToken;
						$roomDetailsComb['dateFrom'] = $dateFrom;
						$roomDetailsComb['dateTo'] = $dateTo;
						$roomDetailsComb['availableToken'] = $availableToken;
						$roomDetailsComb['contractName'] = $contractName;
						$roomDetailsComb['contractOficeCode'] = $contractOficeCode;
						$roomDetailsComb['classification'] = $classification;
						$roomDetailsComb['classificationType'] = $classificationCode;
						$roomDetailsComb['hotelCode'] = $hotelDetails['hotelCode'];
						$roomDetailsComb['destinationCode'] = $destinationCode;
						$roomDetailsComb['destinationType'] = $destinationType;
						$roomDetailsComb['paxes'] = $dataGET->pax;
						$roomDetailsComb['zoneCode'] = $zoneCode;
						$roomDetailsComb['zoneName'] = $zoneName;
						$roomDetailsComb['zoneType'] = $zoneType;
						

						$priceRoom += (float) (string)$rooms->HotelRoom->Price->Amount;
						$roomDet = array("name"=>(string)$rooms->HotelRoom->RoomType);
						$roomDet['SHRUI'] = (string)$rooms->HotelRoom->attributes()->SHRUI;
						$roomDet['onRequest'] = (string)$rooms->HotelRoom->attributes()->onRequest;
						$roomDet['boardType'] = (string)$rooms->HotelRoom->Board->attributes()->type;
						$roomDet['boardCode'] = (string)$rooms->HotelRoom->Board->attributes()->code;
						$roomDet['roomType'] = (string)$rooms->HotelRoom->RoomType->attributes()->type;
						$roomDet['roomCode'] = (string)$rooms->HotelRoom->RoomType->attributes()->code;
						$roomDet['roomChar'] = (string)$rooms->HotelRoom->RoomType->attributes()->characteristic;
						$roomDet['paxes'] = array("adult"=>(int)$rooms->HotelOccupancy->Occupancy->AdultCount,"child"=>(int)$roomAvailable->HotelOccupancy->Occupancy->ChildCount);
						array_push($roomDetailsComb['roomDetails'],$roomDet);

						$amountCal = (float)$rooms->HotelRoom->CancellationPolicy->Price->Amount;
						$pricePolicyCalculation += $amountCal;
						$tempDate = (int)$rooms->HotelRoom->CancellationPolicy->Price->DateTimeFrom->attributes()->date;
						if($dateFromInt>$tempDate){
							$dateFromInt = (int)$tempDate;
							$calDateFrom = (string)$rooms->HotelRoom->CancellationPolicy->Price->DateTimeFrom->attributes()->date;
						}

						if($classificationCode == "NRF"){
							$roomDetailsComb['classificationType'] = 'NRF';
							$roomDetailsComb['classification'] = "Non Refundable";

							$canPolicy = array();
							$canPolicy['dateFrom'] = date('Ymd');
							$canPolicy['dateTo'] = $dateFrom;
							$canPolicy['type'] = "Percent";
							$canPolicy['value'] =100;
							$roomDetailsComb['cancellationPolicyDetails'] = $canPolicy;
						}else{
							$resDateDiff = $tools->dateDifference((string)date('Ymd'),$calDateFrom);
							
							if((int)$resDateDiff["days"]<=5){
								$roomDetailsComb['classificationType'] = 'NRF';
								$roomDetailsComb['classification'] = "Non Refundable";
								$canPolicy = array();
								$canPolicy['dateFrom'] = date('Ymd');
								$canPolicy['dateTo'] = $dateFrom;
								$canPolicy['type'] = "Percent";
								$canPolicy['value'] =100;
								$roomDetailsComb['cancellationPolicyDetails'] = $canPolicy;
							}else{
								$canPolicy = array();
								$canPolicy['dateFrom'] = $calDateFrom;
								$canPolicy['dateTo'] = $dateFrom;
								$canPolicy['type'] = "Percent";
								$canPolicy['value'] =100;
								$roomDetailsComb['cancellationPolicyDetails'] = $canPolicy;
							}
							
						}


						$originalAmount = (float)$priceRoom;
						$orginalNetAmt = (float)$priceRoom*$currEx[$curr];
						$netAmountInAgency = round($orginalNetAmt);
						$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
						$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
						$commInAmountTemp = $grossAmt*(SYS_COMM/100);
						$commAmt = round($commInAmountTemp);
						$discInAmountTemp = $grossAmt*(SYS_DISC/100);
						$discAmt = round($discInAmountTemp);
						$totalLessAMOUNTT = round($commAmt+$discAmt);
						$totaldueamt = $grossAmt-$totalLessAMOUNTT;
						$totalProfitamt = $totaldueamt-$netAmountInAgency;
						$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
						$gstAmt = $totalProfitamt*(SYS_GST/100);


						$roomDetailsComb['currTo'] = 'AUD';
						$roomDetailsComb['convertedTotalPrice'] = $totaldueamt;


						$calculatedPrice = array(
								"fromCurrency"=> $curr,
								"fromRate"=> 1,
								"toCurrency"=> 'AUD',
								"toRate"=> $currEx[$curr],
								"originalAmount"=> $originalAmount,
								"orginalNetAmt"=> $orginalNetAmt,
								"netAmountInAgency"=> $netAmountInAgency,
								"markupInAmount"=>$markupInAmountTemp,
								"markupInPct"=>SYS_MARKUP,
								"grossAmt"=> $grossAmt,
								"commInPct"=>SYS_COMM,
								"commInAmount"=>$commInAmountTemp,
								"commAmt"=>$commAmt,
								"discInPct"=>SYS_DISC,
								"discInAmount"=>$discInAmountTemp,
								"discAmt"=>$discAmt,
								"totalLessAmt"=> $totalLessAMOUNTT,
								"totalDueAmt"=> $totaldueamt,
								"totalProfitAmt"=> $totalProfitamt,
								"gstInPct"=>SYS_GST,
								"gstInAmount"=>$gstInAmountTemp,
								"gstAmt"=>$gstAmt


							);
						$roomDetailsComb['calculatedPrice'] = $calculatedPrice;

						if(!array_key_exists((string)$hotel->HotelInfo->Code, $hotelArrayResults)){
							if((float)$hotelDetails['hotelConvertedTotalPrice']>(float)$totaldueamt){
									$hotelDetails['hotelCurrTo'] = 'AUD';
									$hotelDetails['hotelConvertedTotalPrice'] = (float)$totaldueamt;
									if($maxAmount<(float)$totaldueamt){
										$maxAmount = (float)$totaldueamt;
									}

									$hotelDetails['hotelCalculatedPrice'] = $calculatedPrice;
									
							}
							array_push($hotelDetails['hotelAvailableRooms'],$roomDetailsComb);
						}else{
							if((float)$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelConvertedTotalPrice']>(float)$totaldueamt){
									$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelCurrTo'] = 'AUD';
									$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelConvertedTotalPrice'] = (float)$totaldueamt;
									if($maxAmount<(float)$totaldueamt){
										$maxAmount = (float)$totaldueamt;
									}
									$hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelCalculatedPrice'] = $calculatedPrice;
									
							}

							array_push($hotelArrayResults[(string)$hotel->HotelInfo->Code]['hotelAvailableRooms'],$roomDetailsComb);

						}

					}
		
					
					

				}
				

				if(!array_key_exists((string)$hotel->HotelInfo->Code, $hotelArrayResults)){
					//array_push($hotelArrayResults,$hotelDetails);
					$hotelArrayResults[(string)$hotel->HotelInfo->Code] = $hotelDetails;
				}
				
			}
			$resultsHotels = array_values($hotelArrayResults);
			$_SESSION["SEARCH_RESULTS"] = array($echoToken=>$resultsHotels);

			//$connection = new MongoClient();

			
			#print_r($hotelArrayResults);
			#die();

			$response = array ( "success" => true, "total" => count($resultsHotels), "timeToExpiration"=>$timeToExpiration,"token"=>$echoToken,"viewMainHotelsHB" => $resultsHotels,"flitersData"=>array("maxAmount"=>$maxAmount,"minAmount"=>(float)0,"zoneList"=>$zoneListArray,"categoryList"=>$categoryList));
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		break; 
		}

		case "searchFilter":{
			if(!isset($_SESSION["SEARCH_RESULTS"][$_GET['token']]) || $_SESSION["SEARCH_RESULTS"][$_GET['token']] ==""){
				$response = array ( "success" => true, "total" => 0, "viewMainHotelsHB" => array(),"token"=>"","flitersData"=>array("maxAmount"=>0,"minAmount"=>(float)0,"zoneList"=>array(),"categoryList"=>array()));
  	  		
	  	  		print json_encode($response);
	  	  		$connection->close();
	  	  		exit;
			}

			$dataGET = json_decode($_GET['postDetails']);
			
			/*$connection = new MongoClient();
			$dbB2B = $connection->b2b_bookings;*/



			$hotelsArray = $_SESSION["SEARCH_RESULTS"][$_GET['token']];
			$filterGET = json_decode($_GET['filterDetails']);

			if($filterGET->refresh){
				$response = array ( "success" => true, "total" => count($hotelsArray),"viewMainHotelsHB" => $hotelsArray);
  	  		
	  	  		print json_encode($response);
	  	  		$connection->close();
	  	  		exit;
			}



			
			#print_r($filterGET);
			$hotelArrayResults = array();
			if($filterGET->priceRange){
				$minPrice = $filterGET->priceRange[0];
				$maxPrice = $filterGET->priceRange[1];


				/*$whereData = array();
				$whereData['token'] = $_GET['token'];
				$whereData['search_results'] = array("\$elemMatch"=>array("hotelConvertedTotalPrice"=>array("\$gte"=>$minPrice,"\$lte"=>$maxPrice)));
				$reuslts = $dbB2B->search_events->find($whereData,array("_id"=>0));

				$dataArray = iterator_to_array($reuslts);
				print_r($dataArray);
				die();*/
				//print_r($filterGET);
				foreach($hotelsArray as $hotels){

					if((float)$minPrice<=(float)$hotels['hotelConvertedTotalPrice'] && (float)$hotels['hotelConvertedTotalPrice']<=(float)$maxPrice){
						array_push($hotelArrayResults,$hotels);
					}

				}

			}
			if($filterGET->hotelName){
				foreach($hotelsArray as $hotels){
					if (preg_match("/".$filterGET->hotelName."/i", $hotels['hotelName'])) {
					   array_push($hotelArrayResults,$hotels);
					}
				}
			}

			if($filterGET->zoneName){
				foreach($hotelsArray as $hotels){
					if($filterGET->zoneName ==  $hotels['hotelDestinationZone']){
						array_push($hotelArrayResults,$hotels);
					}

				}


			}
			if($filterGET->catName){
				foreach($hotelsArray as $hotels){
					if($filterGET->catName ==  $hotels['hotelCategory']){
						array_push($hotelArrayResults,$hotels);
					}

				}

			}


			$response = array ( "success" => true, "total" => count($hotelArrayResults),"viewMainHotelsHB" => $hotelArrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		break; 
			


		}
		case "addHotelBedsRoomToBooking":{
			$tools = new Tools; 
			$connection = new MongoClient();
			$db = $connection->db_system;

			$roomDetails = json_decode($_POST['roomDetailsJson']); //object HotelDetails
			$HotelDetails = json_decode($_POST['HotelDetails']);
			
			$hbObj = new HotelBedsClient();
			$serviceADD = $hbObj->serviceAdd($roomDetails,$_POST['paxDetails']);

			//$dt = date('Y-m-d');
			$today = new MongoDate(strtotime("now"));

			if((array)$serviceADD->Purchase){
				if($_POST['bookingID'] =='New'){
					$agency = $db->agency->findOne(array('agency_code'=>$_SESSION['sys_agency_code_B2B']),array("_id"=>0));
					$bookingID = $tools->getNextID("booking");
					

					$insertData = array(
						"booking_id"=>(int)$bookingID,
						"booking_consultant_code"=> "STRRz",
						"booking_consultant_name"=> "B2B-SYSTEM",
						"booking_agency_code"=>$_SESSION['sys_agency_code_B2B'],
						"booking_agency_name"=>$_SESSION['sys_agency_name_B2B'],
						"booking_creation_date"=>$today,
						"booking_source"=>5,
						"booking_status"=>'Quote',
						"booking_final_voucher_date"=>"",
						"booking_final_payment_date"=>"",
						"booking_services_start_date"=>"",
						"booking_services_end_date"=>"",
						"booking_agency_consultant_code"=>$_SESSION['sys_consultant_code_B2B'],
						"booking_agency_consultant_name"=>$_SESSION['sys_consultant_name_B2B']
						);
					//print_r($insertData);
					$db->booking->insert($insertData);
					
				}else{
					$bookingID = (int)$_POST['bookingID'];
					//$bookingID = $tools->getNextID("booking");
				}


				$itemID = $tools->getNextID("item");
				//start insert guest
				$isFirstGuest = true;
				$adultMax = 0;
				$childMax = 0;
				foreach($_POST['paxDetails'] as $key=>$val){
					$ctrChild = 0;
					foreach($val as $key2=>$val2){
						if($val2['type']=="adult"){
							$adultMax++;
							$age = 25;
						}else{
							$childMax++;
							$age = $roomDetails->paxes[$key]->childAges[$ctrChild];
							$ctrChild++;
						}
						$guest= array(
							"guest_title" => $val2["title"],
							"guest_last_name" => $val2["lname"],
							"guest_aency_code" => $_SESSION['sys_agency_code_B2B'],
							"guest_agency_name" => $agency['sys_agency_name_B2B'],
							"guest_first_name" => $val2["fname"],
							"guest_age" => $age
						);
						$guestCollection = $db->guest->findOne($guest);
						if($guestCollection){
							if($isFirstGuest){
								
								$lead = 1;
								$isFirstGuest = false;
							}else{
								$lead = 0;
							}
							$guestPax = array(
								"item_id" => (int)$itemID,
								"guest_id" => (string)$guestCollection['_id'],
								"guest_title" => $guestCollection['guest_title'],
								"guest_last_name" => $guestCollection['guest_last_name'],
								"guest_agency_code" => $guestCollection['guest_agency_code'],
								"guest_agency_name" => $guestCollection['guest_agency_name'],
								"guest_business" => $guestCollection['guest_business'],
								"guest_fax" => $guestCollection['guest_fax'],
								"guest_first_name" =>$guestCollection['guest_first_name'],
								"guest_home" => $guestCollection['guest_home'],
								"guest_mobile" => $guestCollection['guest_mobile'],
								"guest_age" =>$guestCollection['guest_age'],
								"guest_is_lead" =>$lead
							);
							$db->item_guest->insert($guestPax);
							unset($guestPax['guest_is_lead']);
							$guestPax['booking_id'] = (int)$bookingID;
							$db->booking_guest->insert($guestPax);
						}else{
							$guest['guest_home'] = "";
							$guest['guest_business'] = "";
							$guest['guest_mobile'] = "";
							$guest['guest_fax'] = "";
							$db->guest->insert($guest);
							$guestCollection2 = $db->guest->findOne($guest);
							if($isFirstGuest){
								$lead = 1;
								$isFirstGuest = false;

							}else{
								$lead = 0;
							}
							$guestPax = array(
								"item_id" => (int)$itemID,
								"guest_id" => (string)$guestCollection2['_id'],
								"guest_title" => $guestCollection2['guest_title'],
								"guest_last_name" => $guestCollection2['guest_last_name'],
								"guest_agency_code" => $guestCollection2['guest_agency_code'],
								"guest_agency_name" => $guestCollection2['guest_agency_name'],
								"guest_business" => $guestCollection2['guest_business'],
								"guest_fax" => $guestCollection2['guest_fax'],
								"guest_first_name" =>$guestCollection2['guest_first_name'],
								"guest_home" => $guestCollection2['guest_home'],
								"guest_mobile" => $guestCollection2['guest_mobile'],
								"guest_age" =>$guestCollection2['guest_age'],
								"guest_is_lead" =>$lead
							);
							$db->item_guest->insert($guestPax);
							unset($guestPax['guest_is_lead']);
							$guestPax['booking_id'] = (int)$bookingID;
							
							$db->booking_guest->insert($guestPax);


						}

					}


				}
				//end insert guest

				//start Adding it to the item

				$roomStr = "";
				$isFirst =  true;
				foreach($roomDetails->roomDetails as $keyRoom =>$valRoom){
					
					if($isFirst){
						$roomStr .= $valRoom->name;
						$isFirst=false;
					}else{
						$roomStr .= " & ".$valRoom->name;
					}
				}

				$loc = explode(", ", $HotelDetails->hotelDestination);

				$Year = substr($roomDetails->dateFrom, 0,4); 
				$Month = substr($roomDetails->dateFrom, 4,2); 
				$day = substr($roomDetails->dateFrom, 6,2); 
				
				$startDate = new MongoDate(strtotime($Year.'-'.$Month.'-'.$day.' 12:00:00'));
				$Year = substr($roomDetails->dateTo, 0,4); 
				$Month = substr($roomDetails->dateTo, 4,2); 
				$day = substr($roomDetails->dateTo, 6,2); 
				$endDate =new MongoDate(strtotime($Year.'-'.$Month.'-'.$day.' 12:00:00'));


				$currencyVaidationList = array("Euro" =>"EUR","US Dollar"=>"USD","Australian Dollar"=>"AUD");

				$curr = (string)$serviceADD->Purchase->Currency->attributes()->code;
				if(array_key_exists($curr,$currencyVaidationList)){
					$curr = $currencyVaidationList[$curr];
				}
				$resCurr = $db->currency_ex->findOne(array("curr_from"=> $curr), array("_id"=>0));

				$originalAmount = (float)$serviceADD->Purchase->TotalPrice;
				$orginalNetAmt = (float)$serviceADD->Purchase->TotalPrice*(float)$resCurr['rate_to'];
				$netAmountInAgency = round($orginalNetAmt);
				$markupInAmountTemp = $netAmountInAgency*(SYS_MARKUP/100);
				$grossAmt = round($markupInAmountTemp+$netAmountInAgency);
				$commInAmountTemp = $grossAmt*(SYS_COMM/100);
				$commAmt = round($commInAmountTemp);
				$discInAmountTemp = $grossAmt*(SYS_DISC/100);
				$discAmt = round($discInAmountTemp);
				$totalLessAMOUNTT = round($commAmt+$discAmt);
				$totaldueamt = $grossAmt-$totalLessAMOUNTT;
				$totalProfitamt = $totaldueamt-$netAmountInAgency;
				$gstInAmountTemp = $totalProfitamt*(SYS_GST/100);
				$gstAmt = $totalProfitamt*(SYS_GST/100);

				$dateTimeCeated = date('d-m-Y H:i:s');

				$detailsITEM = array(
					"itemId"=> (int)$itemID,
					"bookingId"=> (int)$bookingID,
					"itemCode"=> "HB-".$HotelDetails->hotelCode,
					"itemName"=> $HotelDetails->hotelName,
					"itemPhone"=> $HotelDetails->hotelPhone,
					"itemRatings"=> (int)filter_var($HotelDetails->hotelCategory, FILTER_SANITIZE_NUMBER_INT), 
					"itemPropertyCategory"=> $HotelDetails->hotelCategory,
					"itemSeq"=> 0,
					"itemStockType"=> '',
					"itemStockTypeName"=> '',
					"itemSupplierCode"=> 'HBBEDLIVE',
					"itemSupplierName"=> (string)$serviceADD->Purchase->ServiceList->Service->Supplier->attributes()->name." VatNumber: ".$serviceADD->Purchase->ServiceList->Service->Supplier->attributes()->vatNumber,
					"itemServiceCode"=> 'ACC',
					"itemServiceName"=> 'Accommodation',
					"itemSumarryDescription"=> "<p>".$HotelDetails->hotelDescription."</p>",
					"itemDetailedDescription"=> '',
					"itemCostingsDescription"=> '',
					"itemInclusionDescription"=> $roomDetails->boardType." (".$roomStr.")",
					"itemExclusionDescription"=> '',
					"itemConditionsDescription"=> '',
					"itemStatus"=> 'Quote',
					"itemSuplierConfirmation"=> "",
					"itemSuplierResReq" =>(string)$serviceADD->attributes()->echoToken."|".(string)$serviceADD->Purchase->attributes()->purchaseToken."|".(string)$serviceADD->Purchase->ServiceList->Service->attributes()->SPUI,
					"itemFromCountry"=> strtoupper($loc[0]),
					"itemFromCity"=> strtoupper($loc[1]),
					"itemFromAddress"=> $HotelDetails->hotelAddress,
					"itemToCountry"=> '',
					"itemToCity"=> '',
					"itemToAddress"=> '',
					"itemStartDate"=> $startDate,
					"itemEndDate"=> $endDate,
					"itemStartTime"=>'00:00',
					"itemEndTime"=>'00:00',
					"itemIsLeadPax"=> 1,
					"itemIsFullPax"=> 1,
					"itemIsVoucherable"=> 1,
					"itemIsAutoInvoice"=> 1,
					"itemIsInvoicable"=> 1,
					"itemIsIterary"=> 1,
					"itemIsGstApplicable"=> 0,
					"itemAdultMin"=>$adultMax,
					"itemAdultMax"=>$adultMax,
					"itemChildMin"=>$childMax,
					"itemChildMax"=>$childMax,
					"itemInfantMin"=>0,
					"itemInfantMax"=>0,
					"itemIsCancelled"=>0,
					"itemIsPaxAllocated"=>1,
					"itemIsReceipt"=>0,
					"itemIsInvoiced"=>0,
					"itemIsPaid"=>0,
					"itemIsConfirmed"=>0,
					"itemIsBlank"=> 0,
					"itemIsLive"=>1,
					"itemCancellation"=>array(),
					"itemSupplements"=>array(),
					"itemCostings"=> array(
						"fromCurrency"=> $resCurr['curr_from'],
						"fromRate"=> 1,
						"toCurrency"=> $resCurr['curr_to'],
						"toRate"=> $resCurr['rate_to'],
						"originalAmount"=> $originalAmount,
						"orginalNetAmt"=> $orginalNetAmt,
						"netAmountInAgency"=> $netAmountInAgency,
						"markupInAmount"=>$markupInAmountTemp,
						"markupInPct"=>SYS_MARKUP,
						"grossAmt"=> $grossAmt,
						"commInPct"=>SYS_COMM,
						"commInAmount"=>$commInAmountTemp,
						"commAmt"=>$commAmt,
						"discInPct"=>SYS_DISC,
						"discInAmount"=>$discInAmountTemp,
						"discAmt"=>$discAmt,
						"totalLessAmt"=> $totalLessAMOUNTT,
						"totalDueAmt"=> $totaldueamt,
						"totalProfitAmt"=> $totalProfitamt,
						"gstInPct"=>SYS_GST,
						"gstInAmount"=>$gstInAmountTemp,
						"gstAmt"=>$gstAmt


					),
					"otherDetails"=>array(
						"dateTimeCreated"=>(string)date('d-m-Y H:i:s'),
						"expireTime"=>(float)$serviceADD->Purchase->attributes()->timeToExpiration,
						"cancellationPolicyRemarks" =>"",
						"cancellationType" =>"",
						"roomList" =>array()
					)

				);
				if($roomDetails->classificationType == "NRF"){
					$cancelData = array(
							"dateFrom"=>$today,
							"dateTo"=>$startDate,
							"typeCancellation"=>"Percentage",
							"percent" =>100,
							"amount"=>0
							);
					array_push($detailsITEM["itemCancellation"],$cancelData);
					$detailsITEM["otherDetails"]['cancellationPolicyRemarks'] = "This room is non-refundable and has no show policy.";
					$detailsITEM["otherDetails"]['cancellationType'] = "NRF";
				}else{

					$dateFromInt = 100000000;

					if(!isset($hotel->AvailableRoom[0])){
						$AvailableRoom = array($serviceADD->Purchase->ServiceList->Service->AvailableRoom);
						//print_r($AvailableRoom);
					}else{
						$AvailableRoom = $serviceADD->Purchase->ServiceList->Service->AvailableRoom;
					}
					foreach($AvailableRoom as $rooms){
						$tempDate = (int)$rooms->HotelRoom->CancellationPolicies->CancellationPolicy->attributes()->dateFrom;
						if($dateFromInt>$tempDate){
							$dateFromInt = (int)$tempDate;
							$calDateFrom = (string)$rooms->HotelRoom->CancellationPolicies->CancellationPolicy->attributes()->dateFrom;
						}
					}
					
					

					$Year = substr($calDateFrom, 0,4); 
					$Month = substr($calDateFrom, 4,2); 
					$day = substr($calDateFrom, 6,2); 
					$calDate =new MongoDate(strtotime($Year.'-'.$Month.'-'.$day.' 12:00:00'));
					$cancelData = array(
							"dateFrom"=>$calDate,
							"dateTo"=>$startDate,
							"typeCancellation"=>"Percentage",
							"percent" =>100,
							"amount"=>0
							);
					array_push($detailsITEM["itemCancellation"],$cancelData);
					$detailsITEM["otherDetails"]['cancellationPolicyRemarks'] = "Good news! We won't charge you a cancellation fee if cancelled before ".$calDateFrom.". Any cancellation received within this days prior to the arrival date will incur the full amount charge. Failure to arrive at your hotel will be treated as a No-Show and will incur the full amount charge.";
					$detailsITEM["otherDetails"]['cancellationType'] = "RF";
				}

				if(!isset($serviceADD->Purchase->ServiceList->Service->AvailableRoom[0])){
					$AvailableRoom = array($serviceADD->Purchase->ServiceList->Service->AvailableRoom);
						//print_r($AvailableRoom);
				}else{
					$AvailableRoom = $serviceADD->Purchase->ServiceList->Service->AvailableRoom;
				}

				foreach($AvailableRoom as $room){

					$arrayRoom = array();
					$arrayRoom['SHRUI'] = (string)$room->HotelRoom->attributes()->SHRUI;
					$arrayRoom['guestList'] = array();
					if(!isset($room->HotelOccupancy->Occupancy->GuestList->Customer[0])){
						$guestLIST = array($room->HotelOccupancy->Occupancy->GuestList->Customer);
							//print_r($AvailableRoom);
					}else{
						$guestLIST = $room->HotelOccupancy->Occupancy->GuestList->Customer;
					}
					foreach($guestLIST as $guest){
						$arrayGuest = array();
						$arrayGuest['CustomerType'] = (string)$guest->attributes()->type;
						$arrayGuest['CustomerId'] = (string)$guest->CustomerId;
						$arrayGuest['Age'] = (string)$guest->Age;
						$arrayGuest['Name'] = (string)$guest->Name;
						$arrayGuest['LastName'] = (string)$guest->LastName;
						array_push($arrayRoom['guestList'],$arrayGuest );

					}
					
					array_push($detailsITEM["otherDetails"]['roomList'],$arrayRoom );
				}
				//print_r($detailsITEM);
				$db->items->insert($detailsITEM);
				$response = array ( "success" => true,"booking_id"=>$bookingID);
				print json_encode($response);

			}else{
				$response = array ( "failure" => false);
				print json_encode($response);
			}
			
			$connection->close();
			exit;

		break; 
		}
		
		/* ENDLOGIN*/
	}

?>