<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateSysUser":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			
			

			$code =  $tools->generateRandomString(5);

			do {
			    $code =  $tools->generateRandomString(5);
			} while ($db->sys_user->find(array("account_code"=> $code))->count() > 0);
			
			$aliasname   = $_POST['aliasname'];
			
			$username   = $_POST['username'];
			$pass   = $_POST['pass'];
			$repass   = $_POST['repass'];
			$id = $_POST['user_system_id'];
			if($username == "" || $pass =="" || $aliasname =="" || $pass == "" || $pass != $repass){
				$response = array ( "status" => false);
				print json_encode($response);
			
				
				exit;
			}
			

			
			
			
			
			if($id==0){
				
				
			
				#$code = $tools->generateCode($_POST['aliasname']); 
				$insertData = array();

				$insertData['account_code'] = $code;
				$insertData['account_name'] = $_POST['aliasname'];
				$insertData['user'] = $_POST['username'];
				$insertData['password'] = md5($_POST['pass']);
				$insertData['is_active'] = $_POST['user_sys_is_active'];

				$res = $db->sys_user->insert($insertData);
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
			
			}else{

				

				$mongoID = new MongoID($_POST['user_system_id']);
				$updateData = array();

				$updateData['account_code'] = $code;
				$updateData['account_name'] = $_POST['aliasname'];
				$updateData['user'] = $_POST['username'];
				$updateData['password'] = md5($_POST['pass']);
				$updateData['is_active'] = $_POST['user_sys_is_active'];

				$res = $db->sys_user->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "viewUser":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			
			
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['account_name'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			$dataCollections = $db->sys_user->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_account'] = $key;
				$data['account_code'] = $row['account_code'];
				$data['account_name'] = $row['account_name'];
				$data['username'] = $row['user'];
				$data['password'] = $row['password'];
				$data['is_active'] =$row['is_active'];

			 	array_push($arrayResults,$data);
			}

			if($dataCollections->count() > 0){
				$isSuccess = true;
			}else{
				$isSuccess = false;
			}
			$response = array ( "success" => $isSuccess, "total" => $dataCollections->count(), "viewUser" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		case "getAccounts":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("is_active"=>"Y");

			
			//print_r($whereData);db.main_destination.find
			$dataCollections = $db->sys_user->find($whereData);

			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['account_code'] = $row['account_code'];
				$data['account_name'] = $row['account_name'];
				

			 	array_push($arrayResults,$data);
			}
			/*$mysql = new Database; 
			$filter = "";
			if($_GET["query"] != ""){
			$filter = "and  agency_name like '%".$_GET["query"]."%'";
			}
			
			$sql = "SELECT id_agency, agency_name FROM agency where is_active = 1 $filter ORDER BY agency_name ASC;";
			$service = $mysql->select_execute_query($sql);*/
  	  		
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['user_id']);
			
			$db->sys_user->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> "Y")));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['user_id']);
			
			$db->sys_user->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> "N")));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		
		/* ENDLOGIN*/
	}
?>