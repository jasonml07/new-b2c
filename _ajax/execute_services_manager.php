<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {

			case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['service_name'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			$dataCollections = $db->sys_service->find($whereData);

			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}
			$dataArray = iterator_to_array($dataCollectionsResults);
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_service'] = $key;
				$data['service_code'] = $row['service_code'];
				$data['service_name'] = $row['service_name'];
				$data['is_active'] = $row['is_active'];

			 	array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewServices" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "createService":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 

			$id = $_POST['service_id'];
			
			if($id==0){
				$insertData = array();

				$insertData['service_code'] = $_POST['service_code'];
				$insertData['service_name'] = $_POST['service_name'];
				$insertData['is_active'] = "N";

				$res = $db->sys_service->insert($insertData);
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
				
			
			}else{

				$mongoID = new MongoID($_POST['service_id']);
				$updateData = array();

				$updateData['service_code'] = $_POST['service_code'];
				$updateData['service_name'] = $_POST['service_name'];

				$res = $db->sys_service->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}	
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}
		
	case "getServices2":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("is_active"=>"Y");

			
			//print_r($whereData);db.main_destination.find
			$dataCollections = $db->sys_service->find($whereData);

			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['service_code'] = $row['service_code'];
				$data['service_name'] = $row['service_name'];
				

			 	array_push($arrayResults,$data);
			}

  	  		
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}


		case "getServices":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['service_id']);
			$res = $db->sys_service->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['service_id'] = $_POST['service_id'];
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
		}

		case "activate":{

			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_service']);
			
			$db->sys_service->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> "Y")));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$mongoID = new MongoID($_POST['id_service']);
			
			$db->sys_service->update(array("_id"=> $mongoID),array("\$set"=> array("is_active"=> "N")));
			
			$response = array ( "success" => true);
			print json_encode($response);
			$connection->close();
  	   		exit;
  	  	break;  
		}
		
		
	}
?>