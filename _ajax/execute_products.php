<?php
require_once('../_classes/tools.class.php');


$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		case "getHotelProducts":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			if(isset($_GET["destination_code"]) && $_GET["destination_code"] != ""){

				//$des = explode("-", $_GET["destination_id"]);
				//$whereData = array("HotelDestination.CountryCode"=>$des[0],"HotelDestination.CityCode"=>$des[1],"HotelDestination.AreaCode"=>$des[2]);
				$whereData = array("HotelDestination.destination_code"=>$_GET["destination_code"]);

				
				//print_r($whereData);db.main_destination.find
				$dataCollections = $db->main_hotels->find($whereData,array("HotelDetail"=>1,"_id"=>0));

				$dataArray = iterator_to_array($dataCollections);
				//print_r($dataArray);
				$arrayResults = array();
				foreach($dataArray as $key => $row){
					//print_r($row);
					$data = array();
					$data['hotel_code'] = $row['HotelDetail']['hotel_code'];
					$data['hotel_name'] = $row['HotelDetail']['hotel_name'];

				 	array_push($arrayResults,$data);
				}
				
  	  		}
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		case "getHotelProductsHB":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			if(isset($_GET["destination_code"]) && $_GET["destination_code"] != ""){

				//$des = explode("-", $_GET["destination_id"]);
				//$whereData = array("HotelDestination.CountryCode"=>$des[0],"HotelDestination.CityCode"=>$des[1],"HotelDestination.AreaCode"=>$des[2]);
				$whereData = array("DestinationCode"=>$_GET["destination_code"]);
				if($_GET["term"] != ""){
					$whereData['Name'] = new MongoRegex("/^".$_GET["term"]."/i");
					//array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				}
				
				//print_r($whereData);db.main_destination.find
				$dataCollections = $db->hb_hotels->find($whereData,array("Name"=>1,"_id"=>0,"HotelCode"=>1))->sort(array("Name"=>1));

				$dataArray = iterator_to_array($dataCollections);
				//print_r($dataArray);
				$arrayResults = array();
				foreach($dataArray as $key => $row){
					//print_r($row);
					$data = array();
					$data['value'] = $row['HotelCode'];
					$data['label'] = $row['Name'];
					

				 	array_push($arrayResults,$data);
				}
				
  	  		}
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		case "getHotelProductsHP":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			if(isset($_GET["destination_code"]) && $_GET["destination_code"] != ""){

				//$des = explode("-", $_GET["destination_id"]);
				//$whereData = array("HotelDestination.CountryCode"=>$des[0],"HotelDestination.CityCode"=>$des[1],"HotelDestination.AreaCode"=>$des[2]);
				$whereData = array("DestinationId"=>strtoupper($_GET["destination_code"]));

				
				$dataCollections = $db->hp_hotels->find($whereData,array("HotelName"=>1,"_id"=>0,"HotelCode"=>1));

				$dataArray = iterator_to_array($dataCollections);
				//print_r($dataArray);
				$arrayResults = array();
				foreach($dataArray as $key => $row){
					//print_r($row);
					$data = array();
					$data['hotel_code'] = $row['HotelCode'];
					$data['hotel_name'] = $row['HotelName'];
					

				 	array_push($arrayResults,$data);
				}
				
  	  		}
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		case "getHotelProductsRS":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			if(isset($_GET["destination_code"]) && $_GET["destination_code"] != ""){

				//$des = explode("-", $_GET["destination_id"]);
				//$whereData = array("HotelDestination.CountryCode"=>$des[0],"HotelDestination.CityCode"=>$des[1],"HotelDestination.AreaCode"=>$des[2]);
				$whereData = array("codesthot"=>strtoupper($_GET["destination_code"]));

				
				//print_r($whereData);db.main_destination.find
				$dataCollections = $db->rs_hotels->find($whereData,array("_id"=>0));

				$dataArray = iterator_to_array($dataCollections);
				//print_r($dataArray);
				$arrayResults = array();
				foreach($dataArray as $key => $row){
					//print_r($row);
					$data = array();
					$data['hotel_code'] = $row['hot_codcobol'];
					$data['hotel_name'] = $row['HotelName'];
					

				 	array_push($arrayResults,$data);
				}
				
  	  		}
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		

		/* ENDLOGIN*/
	}
?>