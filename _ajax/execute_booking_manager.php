<?php
session_start();
if (!isset($_SESSION['is_logged_B2B'])) { //checks if user is logged.. if not redirects to login page.		
	die();
} 
require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "saveBookingDetails":{
			
			$tools = new Tools; 
			
			$bookingID = "";

			if(isset($_POST['booking_id'])){
				$bookingID = (int)$_POST['booking_id'];
				$booking_consultant_code = $_POST['booking_consultant_code'];
				$booking_consultant_name = $_POST['booking_consultant_name'];
				$booking_source = "";
				$booking_agency_code = $_POST['booking_agency_code'];
				$booking_agency_name = $_POST['booking_agency_name'];

				$xDate = explode("/", $_POST['booking_creation_date']);
				$dt = new DateTime($xDate[2]."-".$xDate[1]."-".$xDate[0]);
				
				$ts = $dt->getTimestamp();
				$today = new MongoDate($ts);
				$booking_status = $_POST['booking_status'];
				
				$showDate = $_POST['booking_creation_date'];

				if($_POST['booking_final_voucher_date'] != ""){
					$dt = new DateTime($_POST['booking_final_voucher_date']);
					$ts = $dt->getTimestamp();
					$booking_final_voucher_date = new MongoDate($ts);

				}else{
					$booking_final_voucher_date = "";
				}

				if($_POST['booking_final_payment_date'] != ""){
					$dt = new DateTime($_POST['booking_final_payment_date']);
					$ts = $dt->getTimestamp();
					$booking_final_payment_date = new MongoDate($ts);

				}else{
					$booking_final_payment_date = "";
				}

				if($_POST['booking_services_start_date'] != ""){
					$dt = new DateTime($_POST['booking_services_start_date']);
					$ts = $dt->getTimestamp();
					$booking_services_start_date = new MongoDate($ts);

				}else{
					$booking_services_start_date = "";
				}

				if($_POST['booking_services_end_date'] != ""){
					$dt = new DateTime($_POST['booking_services_end_date']);
					$ts = $dt->getTimestamp();
					$booking_services_end_date = new MongoDate($ts);

				}else{
					$booking_services_end_date = "";
				}

				//echo $_POST['booking_services_end_date'];

				$booking_final_voucher_date_s = $_POST['booking_final_voucher_date'];
				$booking_final_voucher_date_s = $_POST['booking_final_payment_date'];
				$booking_final_voucher_date_s = $_POST['booking_services_start_date'];
				$booking_final_voucher_date_s = $_POST['booking_services_end_date'];

				$booking_source = $_POST['booking_source'];

				$booking_agency_consultant_code = $_POST['booking_agency_consultant_code'];
				$booking_agency_consultant_name = $_POST['booking_agency_consultant_name'];
				

			}else{


				$bookingID = $tools->getNextID("booking");
				$today = new MongoDate(strtotime("now"));
				$showDate = date('d/m/Y');
				$booking_consultant_code = "STRRz";
				$booking_consultant_name = "B2B-SYSTEM";
				$booking_source = 5;
				$booking_status = "Quote";
				$booking_agency_code = $_SESSION['sys_agency_code_B2B'];
				$booking_agency_name = $_SESSION['sys_agency_name_B2B'];

				$booking_final_voucher_date = "";
				$booking_final_payment_date = "";
				$booking_services_start_date = "";
				$booking_services_end_date = "";

				$booking_final_voucher_date_s = "";
				$booking_final_payment_date_s = "";
				$booking_services_start_date_s = "";
				$booking_services_end_date_s = "";

				$booking_agency_consultant_code = $_SESSION['sys_consultant_code_B2B'];
				$booking_agency_consultant_name = $_SESSION['sys_consultant_name_B2B'];

						
			}
			
			

			
			$query = array("booking_id"=>$bookingID);
			$updateData = array(
				"booking_id"=>$bookingID,
				"booking_consultant_code"=>$booking_consultant_code,
				"booking_consultant_name"=>$booking_consultant_name,
				"booking_agency_code"=>$booking_agency_code,
				"booking_agency_name"=>$booking_agency_name,
				"booking_creation_date"=>$today,
				"booking_source"=>$booking_source,
				"booking_status"=>$booking_status,
				"booking_final_voucher_date"=>$booking_final_voucher_date,
				"booking_final_payment_date"=>$booking_final_payment_date,
				"booking_services_start_date"=>$booking_services_start_date,
				"booking_services_end_date"=>$booking_services_end_date,
				"booking_agency_consultant_code"=>$booking_agency_consultant_code,
				"booking_agency_consultant_name"=>$booking_agency_consultant_name
				);
			$showData = array();
			$options = array("upsert" => true);

			$connection = new MongoClient();
			$db = $connection->db_system;

			$db->booking->update($query,$updateData,$options);





			/*$results = array();
			$results['booking_id'] = $bookingID;
			$results['booking_consultant_code'] = $booking_consultant_code;
			$results['booking_consultant_name'] = $booking_consultant_name;
			$results['booking_agency_code'] = $booking_agency_code;
			$results['booking_agency_name'] = $booking_agency_name;
			$results['booking_status'] = $booking_status;
			$results['booking_source'] = $booking_source;
			$results['booking_creation_date'] = $showDate;
			$results["booking_final_voucher_date_s"]=$booking_final_voucher_date_s;
			$results["booking_final_payment_date_s"]=$booking_final_payment_date_s;
			$results["booking_services_start_date_s"]=$booking_services_start_date_s;
			$results["booking_services_end_date_s"]=$booking_services_end_date_s;
			*/
			
			$response =  array("status"=>true,"booking_id"=> $bookingID);
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}

		case "cloneBooking":{

			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			$bookingID = $_POST['bookingID'];
			
			//Clone Booking
			$whereData1 = array('booking_id'=>(int)$bookingID);
			$bookingResults = $db->booking->findOne($whereData1,array("_id"=>0));
			$newBookingId = $tools->getNextID("booking");
			$bookingResults['booking_id'] = $newBookingId;
			$responseItem1 = $db->booking->insert($bookingResults);

			//Clone Item
			$whereData2 = array();
			//$whereData2['$and'] = array();
			$whereData2["bookingId"] = (int)$bookingID;
			$whereData2["itemIsCancelled"]=  0;
			$whereData2["itemServiceCode"]=  array("\$ne" => "SFEE");
			$itemResults = $db->items->find($whereData2,array("_id"=>0));

			$dataArray = iterator_to_array($itemResults);
			foreach($dataArray as $key => $value){
				$value['bookingId'] = $newBookingId;
				$value['itemId'] = $tools->getNextID("item");
				$value['itemIsCancelled'] = 0;
				$value['itemIsPaxAllocated'] = 0;
				$value['itemIsReceipt'] = 0;
				$value['itemIsInvoiced'] = 0;
				$value['itemIsPaid'] = 0;
				$value['itemIsConfirmed'] = 0;
				$value['itemIsBlank'] = 0;
				$value['itemIsLive'] = 0;
				$responseItem2 = $db->items->insert($value);

			}

			if ($responseItem2) $message = "true";
			else $message = "false";
			$response = array ( "status" => true, "message" => $message,"booking_id"=>(int)$newBookingId);
  	  		print json_encode($response);
  	  		$connection->close();
	  	  	exit;
	  	  	break; 
		}

		case "getBookingItemDetails":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$dataCollectionsResults = $db->booking->findOne(array("booking_id"=>(int)$_POST['booking_id'],"booking_agency_code"=>$_SESSION['sys_agency_code_B2B']));
			//print_r($dataCollectionsResults);
			//$dataArray = iterator_to_array($dataCollectionsResults);

			$response =  array("status"=>true,"details"=> array("bookingDetails"=>$dataCollectionsResults));


			print json_encode($response);
			$connection->close();

			break;
		}

		case "getBookingDetails":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array("booking_agency_code"=>$_SESSION['sys_agency_code_B2B']);

			if(isset($_GET["query"])){
				#$whereData['$or'] = array();
				$dateTemp = explode("/", $_GET["query"]);
				if(is_numeric($_GET["query"])){
					//array_push($whereData['booking_id'],array("booking_id"=> (int)$_GET["query"]));
					$whereData['booking_id']  =(int)$_GET["query"];

				}elseif(count($dateTemp)==3){

					//echo "sad";
					//$dt = new DateTime($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]);
					//$ts = $dt->getTimestamp();
					$date1 = date($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]);
					$date2 = date($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]);
					
					$start = new MongoDate(strtotime($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]." 00:00:00"));
					$end = new MongoDate(strtotime($dateTemp[2]."-".$dateTemp[1]."-".$dateTemp[0]." 23:00:00"));
					$whereData['booking_creation_date']  =array('$gte' => $start, '$lte' => $end);
					//array_push($whereData['$or'],array("booking_creation_date"=> array('$gte' => $start, '$lte' => $end)));
					//print_r($whereData);
				}else{
					$whereData['$or'] = array();
					array_push($whereData['$or'],array("booking_consultant_name"=> new MongoRegex("/^".$_GET["query"]."/i")));
					//array_push($whereData['$or'],array("booking_status"=> new MongoRegex("/^".$_GET["query"]."/i")));

					array_push($whereData['$or'],array("booking_agency_consultant_name"=> new MongoRegex("/^".$_GET["query"]."/i")));

				}

				//array_push($whereData['$or'],array("HotelLocation.phoneResv"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelLocation.phoneMang"=> new MongoRegex("/^".$_GET["query"]."/i")));
				//array_push($whereData['$or'],array("HotelLocation.email"=> new MongoRegex("/^".$_GET["query"]."/i")));
				
			}
			//print_r($whereData);
			$dataCollections = $db->booking->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->sort(array("booking_id"=>-1))->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				if(isset($_GET['guestName'])){

					$dataCollectionsResults = $dataCollections->sort(array("booking_id"=>-1));
				
				}else{
					$dataCollectionsResults = $dataCollections->sort(array("booking_id"=>-1))->limit(15);
					
				}
			}

			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['booking_id'] = $row['booking_id'];
				$data['booking_consultant_name'] = $row['booking_consultant_name'];
				$data['booking_agency_name'] = $row['booking_agency_name'];
				$data['booking_creation_date'] = $row['booking_creation_date'];
				$data['booking_agency_consultant_name'] =$row['booking_agency_consultant_name'];
				$data['booking_item_details'] = array();
				$arrItem = $db->items->find(array("bookingId"=> (int)$row['booking_id']),array("_id"=>0));

				$dataArrayItem = iterator_to_array($arrItem);
				$hasGuest = false;
				$ctr = 0;
				$statusStack = array("cancel"=>0,"quote"=>0,"confirmed"=>0);
				foreach ($dataArrayItem as $key1 => $value1) {
					
					$itemDetail = array();

					$itemDetail["itemName"] = $value1['itemName'];
					$itemDetail["itemServiceName"] = $value1['itemServiceName'];
					if($value1['itemServiceCode'] == "SFEE"){
						$itemDetail["itemStartDate"] ="";
						$itemDetail["itemEndDate"] ="";
					}else{
						$itemDetail["itemStartDate"] = date('d/m/Y', $value1['itemStartDate']->sec);
						$itemDetail["itemEndDate"] = date('d/m/Y', $value1['itemEndDate']->sec);
					}
					
					$itemDetail["itemStartTime"] = $value1['itemStartTime'];
					$itemDetail["itemEndTime"] = $value1['itemEndTime'];
					$itemDetail["itemServiceCode"] = $value1['itemServiceCode'];
					$itemDetail["itemFromCountry"] = $value1['itemFromCountry'];
					$itemDetail["itemFromCity"] = $value1['itemFromCity'];
					$itemDetail["itemFromAddress"] = $value1['itemFromAddress'];
					$itemDetail["itemToCountry"] = $value1['itemToCountry'];
					$itemDetail["itemToCity"] = $value1['itemToCity'];
					$itemDetail["itemToAddress"] = $value1['itemToAddress'];

					$itemDetail["itemStatus"] = "Quote";
					if(1==$value1['itemIsCancelled']){
						$itemDetail["itemStatus"] = "Cancelled";
						$statusStack["cancel"] = 1;
					}elseif ($value1['itemIsPaxAllocated'] ==1 && $value1['itemIsConfirmed']==0 && $value1['itemIsCancelled'] == 0) {
						$itemDetail["itemStatus"] = "Quote";
						$statusStack["quote"] = 1;
					}
					elseif ($value1['itemIsPaxAllocated'] ==1 && $value1['itemIsConfirmed']==1 && $value1['itemIsCancelled'] == 0) {
						$itemDetail["itemStatus"] = "Confirmed";
						$statusStack["confirmed"] = 1;
					}
					


					$itemDetail["itemGuest"] = array();

					$dataCollectionsResultsGuest = $db->item_guest->find(array("item_id"=> (int)$value1['itemId']),array("_id"=>0));
					$dataArrayGuest = iterator_to_array($dataCollectionsResultsGuest);
					$hasGuest = false;
					foreach ($dataArrayGuest as $key2 => $value2) {
						$strGuest = $value2['guest_title'].'. '.$value2['guest_first_name'].' '.$value2['guest_last_name'];
						if(preg_match("/".$_GET['guestName']."/i",$strGuest)){
							$hasGuest = true;
						}
						array_push($itemDetail["itemGuest"], $strGuest);
					}

					array_push($data["booking_item_details"], $itemDetail);
					$ctr++;
				}
				$data['booking_status'] = "Quote";
				if($statusStack["cancel"] == 1 && $statusStack["quote"] == 0 && $statusStack["confirmed"] == 0){
					$data['booking_status'] = "Cancelled";
				}elseif($statusStack["confirmed"] == 1 && $statusStack["quote"] == 0){
					$data['booking_status'] = "Confirmed";
				}
				

				$data['itemNumber'] = $ctr;
				if(isset($_GET['guestName']) && $_GET['guestName'] != ""){
					if($hasGuest){
						array_push($arrayResults,$data);
					}
				}else{
					array_push($arrayResults,$data);
				}
				
			 	
			}

			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewUser" => $arrayResults);
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		}
		
		/* ENDLOGIN*/
	}
?>