<?php

session_start();

require_once('../_classes/tools.class.php'); 
require_once('../_classes/item.class.php'); 
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "viewCostings":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			
			
			
			$whereData = array("bookingId"=>(int)$_GET['bookingId']);

			
			$dataCollections = $db->costings->find($whereData,array("_id"=>0));
			

			$dataArray = iterator_to_array($dataCollections);
			
			
			foreach($dataArray as $key => $row){
				$grossTotal = 0;
				$displayTotal = 0;
				$commTotal = 0;
				$nettTotal = 0;
				$whereData = array('costingsId'=>(int)$row['costingsId']);
				$invoiceItemArray = array();
				$dataCollectionsResults2 = $db->costings_items->find($whereData,array("_id"=>0));
				try {
					$dataArray2 = iterator_to_array($dataCollectionsResults2);
					 foreach($dataArray2 as $key2 => $row2){

					 	$grossTotal += (float)$row2['itemCostings']['grossAmt'];
					 	$displayTotal += (float)$row2['itemCostings']['totalDueAmt'];
					 	$commTotal += (float)$row2['itemCostings']['commAmt'];
					 	$nettTotal += (float)$row2['itemCostings']['netAmountInAgency'];
					 	array_push($invoiceItemArray, $row2);

					 }
				} catch (Exception $e) {
				   $$invoiceItemArray = array();
				}
				$dataArray[$key]['costings_items'] = $invoiceItemArray;
				$dataArray[$key]['commTotal'] = $commTotal;
				$dataArray[$key]['grossTotal'] = $grossTotal;
				$dataArray[$key]['displayTotal'] = $displayTotal;
				$dataArray[$key]['nettTotal'] = $nettTotal;

			 	
			}

			//print_r($dataArray);
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewCostings" => $dataArray);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
			
  	  	break; 
		}
		case "allCostingsItem":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			//var_dump(json_decode($_POST['itemIDs']));
			$whereData = array('bookingId'=>(int)$_POST['bookingID'],"itemId"=>array("\$in"=>json_decode($_POST['itemIDs'])));

			

			$dataCollectionsResults = $db->items->find($whereData)->sort(array("itemSeq"=>1));

			//print_r($dataCollectionsResults);
			try {
				$dataArray= iterator_to_array($dataCollectionsResults);
				foreach($dataArray as $key => $row){
					foreach($row['itemSupplements'] as $key2 => $row2){
						if((int)$row2['supplement_qty'] >0){

							$dataArray[$key]['itemCostings']['originalAmount'] += (float)$row2['supplement_costings']['originalAmount'];
							$dataArray[$key]['itemCostings']['orginalNetAmt'] += (float)$row2['supplement_costings']['orginalNetAmt'];
							$dataArray[$key]['itemCostings']['netAmountInAgency'] += (float)$row2['supplement_costings']['netAmountInAgency'];
							$dataArray[$key]['itemCostings']['markupInAmount'] += (float)$row2['supplement_costings']['markupInAmount'];
							$dataArray[$key]['itemCostings']['grossAmt'] += (float)$row2['supplement_costings']['grossAmt'];
							$dataArray[$key]['itemCostings']['commInAmount'] += (float)$row2['supplement_costings']['commInAmount'];
							$dataArray[$key]['itemCostings']['commAmt'] += (float)$row2['supplement_costings']['commAmt'];
							$dataArray[$key]['itemCostings']['totalLessAmt'] += (float)$row2['supplement_costings']['totalLessAmt'];
							$dataArray[$key]['itemCostings']['totalDueAmt'] += (float)$row2['supplement_costings']['totalDueAmt'];
							$dataArray[$key]['itemCostings']['totalProfitAmt'] += (float)$row2['supplement_costings']['totalProfitAmt'];
							$dataArray[$key]['itemCostings']['gstAmt'] += (float)$row2['supplement_costings']['gstAmt'];
								
						}
			            
					}
				}
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}
			//print_r($dataArray);

			//print_r($dataArray);
			//die();
			$dataItemToInvoice = array();

			$message = "";
			
			if(count($dataArray) !=0){
				$itemObj = new Item();
				$itemObj->createCostings((int)$_POST['bookingID'],$dataArray);
				$message ="Items has been costed";
				
			}
			if($message !=""){
				$response = array ( "status" => true, "message" => $message,"booking_id"=>(int)$_POST['bookingID']);
			}else{
				$response = array ( "status" => false, "message" => "No Item has been convert.");
			}
			
			
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
		}

		
		/* ENDLOGIN*/
	}
?>
