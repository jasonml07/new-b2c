<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreatemaindestination2":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
		
			$id = $_POST['main_destination2_id'];

			if($id==0){

				 //CHECK IF COUNTRY AND CITY EXIST
				$whereData = array();
				$whereData['$and'] = array();
				array_push($whereData['$and'],array("CityName"=> strtoupper($_POST['CityName'])));
				array_push($whereData['$and'],array("CountryName"=> strtoupper($_POST['CountryName'])));
				
				$findData = $db->main_destination->findOne($whereData);
				if(empty($findData)){

					$tools = new Tools; 
					$CountryCode = $tools->getNextID("CountryCode");
					$CityCode = $tools->getNextID("CityCode");

					$findData2 = $db->main_destination->findOne(array("CountryName" => strtoupper($_POST['CountryName'])));
					if(empty($findData2)){
						$CountryCode = $tools->getNextID("CountryCode");

					}else{
						$CountryCode = $findData2['CountryCode'];
					}
					$insertData = array();
					$insertData['CountryName'] = strtoupper($_POST['CountryName']);
					$insertData['CityName'] = strtoupper($_POST['CityName']);
					$insertData['CountryCode'] = $CountryCode;
					$insertData['CityCode'] = $CityCode;
					$insertData['AreaCode'] = "";
					$insertData['AreaName'] = "";

					$res = $db->main_destination->insert($insertData);

					if($res){
						$response = array ( "status" => true);
					}else{
						$response = array ( "status" => false);
					}
				}else $response = array("status" => false);

			}else{

				$mongoID = new MongoID($_POST['main_destination2_id']);
				$updateData = array();
				$updateData['CountryName'] = strtoupper($_POST['CountryName']);
				$updateData['CityName'] = strtoupper($_POST['CityName']);

				$res = $db->main_destination->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;
			
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("CityCode"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("CountryName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("AreaName"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("CityName"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			//print_r($whereData);db.main_destination.find
			$dataCollections = $db->main_destination->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(1000);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_main_destination2'] = $key;
				$data['CityCode'] = $row['CityCode'];
				$data['CountryCode'] = $row['CountryCode'];
				$data['AreaCode'] = $row['AreaCode'];
				$data['CityName'] = $row['CityName'];
				$data['CountryName'] = $row['CountryName'];
				$data['AreaName'] = $row['AreaName'];

			 	array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewMainDestination2" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		case "deleteDestination":{
		  		$connection = new MongoClient();
				$db = $connection->db_system;

				$mongoID = new MongoID($_POST['main_destination2_id']);
				$res = $db->main_destination->remove( array("_id" => $mongoID ) );

				print json_encode($res);
	  	  		$connection->close();
	  	  		exit;
	  	  	break; 
		  }

		case "getDestinationDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['main_destination_id']);
			$res = $db->main_destination->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['destination_id'] = $_POST['main_destination_id'];
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  		break; 
		}
		
		/* ENDLOGIN*/
	}
?>