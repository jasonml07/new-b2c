<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateBank":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 

			$id = $_POST['bank_id'];
			
			if($id==0){
				$insertData = array();

				$insertData['bank_code'] = $_POST['bank_code'];
				$insertData['bank_name'] = $_POST['bank_name'];
				$insertData['currency'] = $_POST['currency'];
				$insertData['is_active'] = $_POST['is_active'];

				$res = $db->sys_bank->insert($insertData);
				
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
				
			
			}else{

				$mongoID = new MongoID($_POST['bank_id']);
				$updateData = array();

				$updateData['bank_code'] = $_POST['bank_code'];
				$updateData['bank_name'] = $_POST['bank_name'];
				$updateData['currency'] = $_POST['currency'];
				$updateData['is_active'] = $_POST['is_active'];

				$res = $db->sys_bank->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}	
			}
			
			print json_encode($response);
			$connection->close();
			exit;
			
  	  	break; 
		}
		case "getBankDetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['bank_id']);
			$res = $db->sys_bank->findOne(array("_id"=> $mongoID),array("_id"=>0));
			$res['bank_id'] = $_POST['bank_id'];
			#$response = array();
			print json_encode($res);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}

		  case "deleteBank":{
		  		$connection = new MongoClient();
				$db = $connection->db_system;

				$mongoID = new MongoID($_POST['bank_id']);
				$res = $db->sys_bank->remove( array("_id" => $mongoID ) );

				print json_encode($res);
	  	  		$connection->close();
	  	  		exit;
	  	  	break; 
		  }

		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;


			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['bank_name'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			$dataCollections = $db->sys_bank->find($whereData);

			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(15);
			}

			$dataArray = iterator_to_array($dataCollectionsResults);
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_bank'] = $key;
				$data['bank_code'] = $row['bank_code'];
				$data['bank_name'] = $row['bank_name'];
				$data['currency'] = $row['currency'];
				$data['is_active'] = $row['is_active'];

			 	array_push($arrayResults,$data);
			}
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewBank" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		

		case "getCurrency":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array();
			if(isset($_GET["query"])){
				$whereData['curr_from'] =  new MongoRegex("/^".$_GET["query"]."/i");
			}
			
			$dataCollections = $db->currency_ex->find($whereData);

			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['curr_val'] = $row['rate_to'];
				$data['curr_name'] = $row['curr_from'];
				array_push($arrayResults,$data);
			}

			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}/* ENDLOGIN*/
	}
?>