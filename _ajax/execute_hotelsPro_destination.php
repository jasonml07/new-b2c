<?php

require_once('../_classes/tools.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateHPDestination":{
			$connection = new MongoClient();
			$db = $connection->db_system;
			$tools = new Tools; 
			
			

			
		
			$id = $_POST['hotelsPro_destination_id'];
			
				
			
			
			
			if($id==0){
				
				
			
				$insertData = array();

				$insertData['DestinationId'] = $_POST['DestinationId'];
				$insertData['Country'] = $_POST['Country'];
				$insertData['CityTown'] = $_POST['CityTown'];
				$insertData['State'] = $_POST['State'];
				$insertData['MajorDestinationId'] = "";

				$res = $db->hp_destination->insert($insertData);
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
			
			}else{

				

				$mongoID = new MongoID($_POST['hotelsPro_destination_id']);
				$updateData = array();

				$updateData['DestinationId'] = $_POST['DestinationId'];
				$updateData['Country'] = $_POST['Country'];
				$updateData['CityTown'] = $_POST['CityTown'];
				$updateData['State'] = $_POST['State'];
				$updateData['MajorDestinationId'] = "";

				

				$res = $db->hp_destination->update(array("_id"=> $mongoID), array("\$set" => $updateData));
				
			
				if($res){
					$response = array ( "status" => true);
				}else{
					$response = array ( "status" => false);
				}
			
				
				
				
			}
			
			print json_encode($response);
			$connection->close();
			
			exit;
			
  	  	break; 
		}
		case "view":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			


			
			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("Country"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("CityTown"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("State"=> new MongoRegex("/^".$_GET["query"]."/i")));
			}
			//print_r($whereData);
			$dataCollections = $db->hp_destination->find($whereData);
			if(isset($_GET['start']) && $_GET['start'] != 0){
				$dataCollectionsResults = $dataCollections->limit($_GET['limit'])->skip($_GET['start']);
			}else{
				$dataCollectionsResults = $dataCollections->limit(1000);
			}
			

			$dataArray = iterator_to_array($dataCollectionsResults);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_hp_destination'] = $key;
				$data['DestinationId'] = $row['DestinationId'];
				$data['Country'] = $row['Country'];
				$data['CityTown'] = $row['CityTown'];
				$data['State'] = $row['State'];

			 	array_push($arrayResults,$data);
			}

			
			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewHPDestination" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
  	  	break; 
		}
		
		
		/* ENDLOGIN*/
	}
?>