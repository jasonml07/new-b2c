<?php

require_once('../_classes/tools.class.php');
require_once('../_classes/item.class.php');
$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		
		case "viewGuest":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			
			
			$whereData = array();

			$whereData['booking_id'] =  (int)$_GET["booking_id"];
			$dataCollections = $db->booking_guest->find($whereData);
			
			

			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['id_guest'] = $row['guest_id'];
				$data['title'] = $row['guest_title'];
				$data['first_name'] = $row['guest_last_name'];
				$data['last_name'] = $row['guest_first_name'];
				$data['age'] = $row['guest_age'];

			 	array_push($arrayResults,$data);
			}

			$response = array ( "success" => true, "total" => $dataCollections->count(), "viewPaxItem" => $arrayResults);
  	  		
  	  		print json_encode($response);
  	  		$connection->close();
  	  		exit;
			
  	  	break; 
		}

		case "addGuestToBooking":{

			$connection = new MongoClient();
			$db = $connection->db_system;

			
			$mongoID = new MongoID($_POST['paxID']);
			
			$whereData = array();
			$whereData['_id'] =  $mongoID;


			$dataCollections = $db->guest->findOne($whereData);

			$whereData2 = array();
			$whereData2['booking_id'] = (int)$_POST['bookingID'];
			$whereData2['guest_title'] = $dataCollections['guest_title'];
			$whereData2['guest_last_name'] = $dataCollections['guest_last_name'];
			$whereData2['guest_first_name'] = $dataCollections['guest_first_name'];
			$whereData2['guest_age'] = $dataCollections['guest_age'];
			
			
			if($db->booking_guest->findOne($whereData2)){

				$response = array ( "success" => false);
				print json_encode($response);
	  	  		$connection->close();
	  	  		

			}else{
				//echo 1;
				$insertData = array();

				$insertData['booking_id'] = (int)$_POST['bookingID'];
				$insertData['guest_id'] = $_POST['paxID'];
				$insertData['guest_title'] = $dataCollections['guest_title'];
				$insertData['guest_last_name'] = $dataCollections['guest_last_name'];
				$insertData['guest_agency_code'] = $dataCollections['guest_agency_code'];
				$insertData['guest_agency_name'] = $dataCollections['guest_agency_name'];
				$insertData['guest_business'] = $dataCollections['guest_business'];
				$insertData['guest_fax'] = $dataCollections['guest_fax'];
				$insertData['guest_first_name'] = $dataCollections['guest_first_name'];
				$insertData['guest_home'] = $dataCollections['guest_home'];
				$insertData['guest_mobile'] = $dataCollections['guest_mobile'];
				$insertData['guest_age'] = (int)$dataCollections['guest_age'];

				$res = $db->booking_guest->insert($insertData);
					
				
				if($res){
					$response = array ( "success" => true);
				}else{
					$response = array ( "success" => false);
				}


				
	  	  		print json_encode($response);
	  	  		$connection->close();

			}
			

			
  	  		exit;
			
  	  	break; 
		}

		case "getPaxdetails":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array();

			if(isset($_GET["query"])){
				$whereData['guest_agency_code'] = $_GET["agencyCode"];
				$whereData['$or'] = array();
				array_push($whereData['$or'],array("guest_first_name"=> new MongoRegex("/^".$_GET["query"]."/i")));
				array_push($whereData['$or'],array("guest_last_name"=> new MongoRegex("/^".$_GET["query"]."/i")));
				
			}
			//print_r($whereData);db.main_destination.find
			$dataCollections = $db->guest->find($whereData);

			$dataArray = iterator_to_array($dataCollections);
			
			$arrayResults = array();
			foreach($dataArray as $key => $row){

				$data = array();
				$data['guest_id'] = $key;
				$data['guest_name'] = $row['guest_title']." ".$row['guest_first_name']." ".$row['guest_last_name'];
				

			 	array_push($arrayResults,$data);
			}
			/*$mysql = new Database; 
			$filter = "";
			if($_GET["query"] != ""){
			$filter = "and  agency_name like '%".$_GET["query"]."%'";
			}
			
			$sql = "SELECT id_agency, agency_name FROM agency where is_active = 1 $filter ORDER BY agency_name ASC;";
			$service = $mysql->select_execute_query($sql);*/
  	  		
			print json_encode($arrayResults);
  	   		exit;
  	  	break; 
		}
		case "addGuestToItem":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['paxID']);
			
			$whereData = array();
			$whereData['_id'] =  $mongoID;


			$dataCollections = $db->guest->findOne($whereData);
			

			/*
			parr["itemAdultMin"] = h.get('itemAdultMin');
						parr["itemAdultMax"] = h.get('itemAdultMax');
						parr["itemChildMin"] = h.get('itemChildMin');
						parr["itemChildMax"] = h.get('itemChildMax');
						parr["itemInfantMin"] = h.get('itemInfantMin');
						parr["itemInfantMax"] = h.get('itemInfantMax');
						parr["paxID"] = data.guestData.id_guest;
						parr["paxAge"] = data.guestData.age;
			*/

			$whereData = array();
			$whereData['item_id'] =  (int)$_POST['itemID'];


			$dataCollections2= $db->item_guest->find($whereData);
			$paxarr = array();
			$haslead =false;
			if($dataCollections2){
				$adult = 0;
				$child = 0;
				$infant = 0;
				$dataArray = iterator_to_array($dataCollections2);
				foreach($dataArray as $key => $row){

					if((int)$row['guest_age'] >= 16){
						$adult += 1;
						if($row['guest_is_lead'] > 0){
							$haslead =true;

						}
					}
					if((int)$row['guest_age'] < 16 && (int)$row['guest_age'] >= 4 ){
						$child += 1;
					}
					if((int)$row['guest_age'] <=3){
						$infant += 1;
					}
					
					$paxSTR =  $row['guest_title']."|".$row['guest_first_name']."|".$row['guest_last_name']."|".$row['guest_age'];
					array_push($paxarr,$paxSTR);
				}
			}
			
			$goodToAdd = true;
			if((int)$_POST['paxAge'] >= 16){
				if($adult <= (int)$_POST['itemAdultMax'] && $adult >= (int)$_POST['itemAdultMin']){
					$goodToAdd = false;
				
				}

			}
			if((int)$_POST['paxAge'] < 16 && (int)$_POST['paxAge'] >= 4 ){
				if($child <= (int)$_POST['itemChildMax'] && $child >= (int)$_POST['itemChildMin']){
					$goodToAdd = false;
				}
				
			}
			if((int)$_POST['paxAge'] <=3){
				if($infant <= (int)$_POST['itemInfantMax'] && $infant >= (int)$_POST['itemInfantMin']){
					$goodToAdd = false;
				}
				
			}

			$paxSTR2 =  $dataCollections['guest_title']."|".$dataCollections['guest_first_name']."|".$dataCollections['guest_last_name']."|".$dataCollections['guest_age'];
					
			if(in_array($paxSTR2,$paxarr)){
				$goodToAdd = false;

			}
			if($goodToAdd){
				$insertData = array();

				$insertData['item_id'] = (int)$_POST['itemID'];
				$insertData['guest_id'] = $_POST['paxID'];
				$insertData['guest_title'] = $dataCollections['guest_title'];
				$insertData['guest_last_name'] = $dataCollections['guest_last_name'];
				$insertData['guest_agency_code'] = $dataCollections['guest_agency_code'];
				$insertData['guest_agency_name'] = $dataCollections['guest_agency_name'];
				$insertData['guest_business'] = $dataCollections['guest_business'];
				$insertData['guest_fax'] = $dataCollections['guest_fax'];
				$insertData['guest_first_name'] = $dataCollections['guest_first_name'];
				$insertData['guest_home'] = $dataCollections['guest_home'];
				$insertData['guest_mobile'] = $dataCollections['guest_mobile'];
				$insertData['guest_age'] = (int)$dataCollections['guest_age'];
				if((int)$_POST['itemIsLeadPax'] == 1){
					if($haslead){
						$insertData['guest_is_lead'] = 0;
					}else{
						$insertData['guest_is_lead'] = 1;
					}
					
				}else{
					$insertData['guest_is_lead'] = 0;
				}
				

				$res = $db->item_guest->insert($insertData);
				if($res){
					$response = array ( "success" => true);
				}else{
					$response = array ( "success" => false);
				}
			}else{
				$response = array ( "success" => false);
			}
			


			


			
  	  		print json_encode($response);
  	  		$connection->close();
  	   		exit;
  	  	break; 
		}

		case "removeGuestFromItem":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$mongoID = new MongoID($_POST['guestID']);
			
			$whereData = array();
			$whereData['_id'] =  $mongoID;


			$res = $db->item_guest->remove($whereData);
			

			
			if($res){
				$response = array ( "success" => true);
			}else{
				$response = array ( "success" => false);
			}
			


			
  	  		print json_encode($response);
  	  		$connection->close();
  	   		exit;
  	  	break; 
		}

		case "removeGuestFromBooking":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			$whereData = array();

			$whereData['booking_id'] =  (int)$_POST["bookingID"];
			$whereData['guest_id'] =  $_POST["guestID"];

			


			$res = $db->booking_guest->remove($whereData);
			

			
			if($res){
				$response = array ( "success" => true);
			}else{
				$response = array ( "success" => false);
			}
			


			
  	  		print json_encode($response);
  	  		$connection->close();
  	   		exit;
  	  	break; 
		}
		case "autoAssignGuest":{
			$connection = new MongoClient();
			$db = $connection->db_system;


			$whereData = array();
			$whereData['booking_id'] =  (int) $_POST['bookingID'];


			$dataCollections = $db->booking_guest->find($whereData);

			try {
				$dataArray = iterator_to_array($dataCollections);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}

			$adultGuest = array();
			$childGuest = array();
			$infantGuest = array();
			foreach($dataArray as $key => $row){
				$row['guestID'] = $key;
				if((int)$row['guest_age'] >= 16){
					array_push($adultGuest,$row);
				}
				if((int)$row['guest_age'] < 16 && (int)$row['guest_age'] >= 4 ){
					array_push($childGuest,$row);
				}
				if((int)$row['guest_age'] <=3){
					array_push($infantGuest,$row);
				}
				
				
			}
			#print_r($adultGuest);
			
			$whereData = array();
			$whereData['bookingId'] =  (int) $_POST['bookingID'];
			$whereData['itemIsCancelled'] = array('$ne' => (int)1);
			$whereData['itemIsPaxAllocated'] = array('$ne' => (int)1);
			$whereData['itemServiceCode'] = array('$ne' => 'SFEE');
			$whereData['itemIsConfirmed'] = array('$ne' => (int)1);
			$dataCollections = $db->items->find($whereData);

			try {
				$dataArray = iterator_to_array($dataCollections);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}


			foreach($dataArray as $key => $row){


				$whereData2 = array();
				$whereData2['item_id'] =  (int) $row['itemId'];
				$dataCollections2 = $db->item_guest->find($whereData2);

				try {
					$dataArray2= iterator_to_array($dataCollections2);
					
				} catch (Exception $e) {
				  	$dataArray2 = array();
				}
				//print_r($dataArray2);
				$adult = 0;
				$child = 0;
				$infant = 0;
				$hasLead = false;
				$paxarr = array();
				foreach($dataArray2 as $key2 => $row2){
					if((int)$row2['guest_age'] >= 16){
						$adult += 1;
						if($row2['guest_is_lead'] > 0){
							$hasLead =true;

						}
					}
					if((int)$row2['guest_age'] < 16 && (int)$row2['guest_age'] >= 4 ){
						$child += 1;
					}
					if((int)$row2['guest_age'] <=3){
						$infant += 1;
					}
					$paxSTR =  $row['guest_title']."|".$row['guest_first_name']."|".$row['guest_last_name']."|".$row['guest_age'];
					array_push($paxarr,$paxSTR);

				}

				foreach($adultGuest as $aVal){
						$paxSTR2 =  $aVal['guest_title']."|".$aVal['guest_first_name']."|".$aVal['guest_last_name']."|".$aVal['guest_age'];
					
						if(!in_array($paxSTR2,$paxarr)){
							if($adult >= (int)$row['itemAdultMin']){
								continue;
							}
							$insertData = array();

							$insertData['item_id'] = (int) $row['itemId'];
							$insertData['guest_id'] = $aVal['guestID'];
							$insertData['guest_title'] = $aVal['guest_title'];
							$insertData['guest_last_name'] = $aVal['guest_last_name'];
							$insertData['guest_agency_code'] = $aVal['guest_agency_code'];
							$insertData['guest_agency_name'] = $aVal['guest_agency_name'];
							$insertData['guest_business'] = $aVal['guest_business'];
							$insertData['guest_fax'] = $aVal['guest_fax'];
							$insertData['guest_first_name'] = $aVal['guest_first_name'];
							$insertData['guest_home'] = $aVal['guest_home'];
							$insertData['guest_mobile'] = $aVal['guest_mobile'];
							$insertData['guest_age'] = (int)$aVal['guest_age'];
							$insertData['guest_is_lead'] = 0;
							if(!$hasLead){
								$insertData['guest_is_lead'] = 1;
								$hasLead = true;
							}
								
							$adult+=1;
							array_push($paxarr, $paxSTR2);
							$res = $db->item_guest->insert($insertData);


						}
						

					}
				

				foreach($childGuest as $aVal){
						$paxSTR2 =  $aVal['guest_title']."|".$aVal['guest_first_name']."|".$aVal['guest_last_name']."|".$aVal['guest_age'];
					
						if(!in_array($paxSTR2,$paxarr)){
							if($child >= (int)$row['itemChildMin']){
								continue;
							}
							$insertData = array();

							$insertData['item_id'] = (int) $row['itemId'];
							$insertData['guest_id'] = $aVal['guestID'];
							$insertData['guest_title'] = $aVal['guest_title'];
							$insertData['guest_last_name'] = $aVal['guest_last_name'];
							$insertData['guest_agency_code'] = $aVal['guest_agency_code'];
							$insertData['guest_agency_name'] = $aVal['guest_agency_name'];
							$insertData['guest_business'] = $aVal['guest_business'];
							$insertData['guest_fax'] = $aVal['guest_fax'];
							$insertData['guest_first_name'] = $aVal['guest_first_name'];
							$insertData['guest_home'] = $aVal['guest_home'];
							$insertData['guest_mobile'] = $aVal['guest_mobile'];
							$insertData['guest_age'] = (int)$aVal['guest_age'];
							$insertData['guest_is_lead'] = 0;
							
								
							$child +=1;
							
							array_push($paxarr, $paxSTR2);
							$res = $db->item_guest->insert($insertData);


						}
						

					}
				
				//for($xA =$infant;$xA<(int)$row['itemInfantMax'];$xA++){
					foreach($infantGuest as $aVal){
						$paxSTR2 =  $aVal['guest_title']."|".$aVal['guest_first_name']."|".$aVal['guest_last_name']."|".$aVal['guest_age'];
					
						if(!in_array($paxSTR2,$paxarr)){
							if($infant >= (int)$row['itemInfantMin']){
								continue;
							}
							$insertData = array();

							$insertData['item_id'] = (int) $row['itemId'];
							$insertData['guest_id'] = $aVal['guestID'];
							$insertData['guest_title'] = $aVal['guest_title'];
							$insertData['guest_last_name'] = $aVal['guest_last_name'];
							$insertData['guest_agency_code'] = $aVal['guest_agency_code'];
							$insertData['guest_agency_name'] = $aVal['guest_agency_name'];
							$insertData['guest_business'] = $aVal['guest_business'];
							$insertData['guest_fax'] = $aVal['guest_fax'];
							$insertData['guest_first_name'] = $aVal['guest_first_name'];
							$insertData['guest_home'] = $aVal['guest_home'];
							$insertData['guest_mobile'] = $aVal['guest_mobile'];
							$insertData['guest_age'] = (int)$aVal['guest_age'];
							$insertData['guest_is_lead'] = 0;
							
							$infant +=1;
							
							array_push($paxarr, $paxSTR2);

							$res = $db->item_guest->insert($insertData);


						}
						

					}
				//}
				
				
			}

			$response = array ( "success" => true);
			print json_encode($response);
  	  		$connection->close();
  	   		exit;

		}
		case "autoAssignGuest2":{
			$connection = new MongoClient();
			$db = $connection->db_system;


			$whereData = array();
			$whereData['booking_id'] =  (int) $_POST['bookingID'];


			$dataCollections = $db->booking_guest->find($whereData);

			try {
				$dataArray = iterator_to_array($dataCollections);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}

			$adultGuest = array();
			$childGuest = array();
			$infantGuest = array();
			foreach($dataArray as $key => $row){
				$row['guestID'] = $key;
				if((int)$row['guest_age'] >= 16){
					array_push($adultGuest,$row);
				}
				if((int)$row['guest_age'] < 16 && (int)$row['guest_age'] >= 4 ){
					array_push($childGuest,$row);
				}
				if((int)$row['guest_age'] <=3){
					array_push($infantGuest,$row);
				}
				
				
			}
			#print_r($adultGuest);

			//reallocate

			$whereData = array();
			$whereData['bookingId'] =  (int) $_POST['bookingID'];
			$whereData['itemIsCancelled'] = array('$ne' => (int)1);
			$whereData['itemIsPaxAllocated'] = (int)1;
			$whereData['itemServiceCode'] = array('$ne' => 'SFEE');
			//$whereData['itemIsConfirmed'] = array('$ne' => (int)1);
			$dataCollections = $db->items->find($whereData);

			try {
				$dataArray = iterator_to_array($dataCollections);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}


			foreach($dataArray as $key => $row){
				//print_r($row);
				$db->item_guest->remove(
					    array("item_id" => (int)$row['itemId'])
					);
				
			}

			
			$whereData = array();
			$whereData['bookingId'] =  (int) $_POST['bookingID'];
			$whereData['itemIsCancelled'] = array('$ne' => (int)1);
			$whereData['itemServiceCode'] = array('$ne' => 'SFEE');
			//$whereData['itemIsConfirmed'] = array('$ne' => (int)1);
			$dataCollections = $db->items->find($whereData);

			try {
				$dataArray = iterator_to_array($dataCollections);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}


			foreach($dataArray as $key => $row){
				//print_r( $row);

				$whereData2 = array();
				$whereData2['item_id'] =  (int) $row['itemId'];
				$dataCollections2 = $db->item_guest->find($whereData2);

				try {
					$dataArray2= iterator_to_array($dataCollections2);
					
				} catch (Exception $e) {
				  	$dataArray2 = array();
				}
				//print_r($dataArray2);
				$adult = 0;
				$child = 0;
				$infant = 0;
				$hasLead = false;
				$paxarr = array();
				foreach($dataArray2 as $key2 => $row2){
					if((int)$row2['guest_age'] >= 16){
						$adult += 1;
						if($row2['guest_is_lead'] > 0){
							$hasLead =true;

						}
					}
					if((int)$row2['guest_age'] < 16 && (int)$row2['guest_age'] >= 4 ){
						$child += 1;
					}
					if((int)$row2['guest_age'] <=3){
						$infant += 1;
					}
					$paxSTR =  $row['guest_title']."|".$row['guest_first_name']."|".$row['guest_last_name']."|".$row['guest_age'];
					array_push($paxarr,$paxSTR);

				}

				foreach($adultGuest as $aVal){
						$paxSTR2 =  $aVal['guest_title']."|".$aVal['guest_first_name']."|".$aVal['guest_last_name']."|".$aVal['guest_age'];
					
						if(!in_array($paxSTR2,$paxarr)){
							if($adult >= (int)$row['itemAdultMin']){
								continue;
							}
							$insertData = array();

							$insertData['item_id'] = (int) $row['itemId'];
							$insertData['guest_id'] = $aVal['guest_id'];
							$insertData['guest_title'] = $aVal['guest_title'];
							$insertData['guest_last_name'] = $aVal['guest_last_name'];
							$insertData['guest_agency_code'] = $aVal['guest_agency_code'];
							$insertData['guest_agency_name'] = $aVal['guest_agency_name'];
							$insertData['guest_business'] = $aVal['guest_business'];
							$insertData['guest_fax'] = $aVal['guest_fax'];
							$insertData['guest_first_name'] = $aVal['guest_first_name'];
							$insertData['guest_home'] = $aVal['guest_home'];
							$insertData['guest_mobile'] = $aVal['guest_mobile'];
							$insertData['guest_age'] = (int)$aVal['guest_age'];
							$insertData['guest_is_lead'] = 0;
							if(!$hasLead){
								$insertData['guest_is_lead'] = 1;
								$hasLead = true;
							}
								
							$adult+=1;
							array_push($paxarr, $paxSTR2);
							$res = $db->item_guest->insert($insertData);


						}
						

					}
				

				foreach($childGuest as $aVal){
						$paxSTR2 =  $aVal['guest_title']."|".$aVal['guest_first_name']."|".$aVal['guest_last_name']."|".$aVal['guest_age'];
					
						if(!in_array($paxSTR2,$paxarr)){
							if($child >= (int)$row['itemChildMin']){
								continue;
							}
							$insertData = array();

							$insertData['item_id'] = (int) $row['itemId'];
							$insertData['guest_id'] = $aVal['guest_id'];
							$insertData['guest_title'] = $aVal['guest_title'];
							$insertData['guest_last_name'] = $aVal['guest_last_name'];
							$insertData['guest_agency_code'] = $aVal['guest_agency_code'];
							$insertData['guest_agency_name'] = $aVal['guest_agency_name'];
							$insertData['guest_business'] = $aVal['guest_business'];
							$insertData['guest_fax'] = $aVal['guest_fax'];
							$insertData['guest_first_name'] = $aVal['guest_first_name'];
							$insertData['guest_home'] = $aVal['guest_home'];
							$insertData['guest_mobile'] = $aVal['guest_mobile'];
							$insertData['guest_age'] = (int)$aVal['guest_age'];
							$insertData['guest_is_lead'] = 0;
							
								
							$child +=1;
							
							array_push($paxarr, $paxSTR2);
							$res = $db->item_guest->insert($insertData);


						}
						

					}
				
				//for($xA =$infant;$xA<(int)$row['itemInfantMax'];$xA++){
					foreach($infantGuest as $aVal){
						$paxSTR2 =  $aVal['guest_title']."|".$aVal['guest_first_name']."|".$aVal['guest_last_name']."|".$aVal['guest_age'];
					
						if(!in_array($paxSTR2,$paxarr)){
							if($infant >= (int)$row['itemInfantMin']){
								continue;
							}
							$insertData = array();

							$insertData['item_id'] = (int) $row['itemId'];
							$insertData['guest_id'] = $aVal['guest_id'];
							$insertData['guest_title'] = $aVal['guest_title'];
							$insertData['guest_last_name'] = $aVal['guest_last_name'];
							$insertData['guest_agency_code'] = $aVal['guest_agency_code'];
							$insertData['guest_agency_name'] = $aVal['guest_agency_name'];
							$insertData['guest_business'] = $aVal['guest_business'];
							$insertData['guest_fax'] = $aVal['guest_fax'];
							$insertData['guest_first_name'] = $aVal['guest_first_name'];
							$insertData['guest_home'] = $aVal['guest_home'];
							$insertData['guest_mobile'] = $aVal['guest_mobile'];
							$insertData['guest_age'] = (int)$aVal['guest_age'];
							$insertData['guest_is_lead'] = 0;
							
							$infant +=1;
							
							array_push($paxarr, $paxSTR2);

							$res = $db->item_guest->insert($insertData);


						}
						

					}
				//}
				
				
			}

			$response = array ( "success" => true);
			print json_encode($response);
  	  		$connection->close();
  	   		exit;

		}
		case "saveGuest":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			

			$bookingID = (int) $_POST['bookingID'];
			unset($_POST['bookingID']);
			unset($_POST['action']);

			foreach($_POST as $key=>$val){

				$db->items->update(
					    array("itemId" => (int)$key),
					    array('$set' => array('itemIsPaxAllocated' => 1))
					);


				$db->item_guest->remove(
					    array("item_id" => (int)$key)
					);
				
				$guesIDs = explode(",", $val);

				foreach($guesIDs as $id){
					if($id){
						$mongoID = new MongoID($id);
			
						$whereData = array();
						$whereData['_id'] =  $mongoID;
						//print_r($whereData);
						$guestArr = $db->guest->findOne($whereData);
						//print_r($guestArr);
						$insertData = array();


						$insertData['item_id'] = (int)$key;
						$insertData['guest_id'] = (string)$guestArr['_id'];
						$insertData['guest_title'] = $guestArr['guest_title'];
						$insertData['guest_last_name'] = $guestArr['guest_last_name'];
						$insertData['guest_agency_code'] = $guestArr['guest_agency_code'];
						$insertData['guest_agency_name'] = $guestArr['guest_agency_name'];
						$insertData['guest_business'] = $guestArr['guest_business'];
						$insertData['guest_fax'] = $guestArr['guest_fax'];
						$insertData['guest_first_name'] = $guestArr['guest_first_name'];
						$insertData['guest_home'] = $guestArr['guest_home'];
						$insertData['guest_mobile'] = $guestArr['guest_mobile'];
						$insertData['guest_age'] = (int)$guestArr['guest_age'];
						$insertData['guest_is_lead'] = 0;
						
						//print_r($insertData);

						$res = $db->item_guest->insert($insertData);
					}
					
				}

			}

			$response = array ( "status" => true);
			print json_encode($response);
  	  		$connection->close();
			exit;
		}
		case "finalizedGuest":{
			$connection = new MongoClient();
			$db = $connection->db_system;

			
			$whereData = array();
			$whereData['bookingId'] =  (int) $_POST['bookingID'];
			$whereData['itemIsPaxAllocated'] =  (int) 0;
			$whereData['itemIsCancelled'] =  (int) 0;
			$dataCollections = $db->items->find($whereData);

			try {
				$dataArray = iterator_to_array($dataCollections);
				
			} catch (Exception $e) {
			  	$dataArray = array();
			}

			
			$goodItems = array();
			$badItems = array();
			$messageContainer = array();
			$allGood = true;
			foreach($dataArray as $key => $row){
				$isGoodItem = true;
				
				$whereData2 = array();
				$whereData2['item_id'] =  (int) $row['itemId'];
				$isLeadRequired = false;
				if($row['itemIsLeadPax'] > 0){
					$isLeadRequired = true;
				}

				$isFullPaxRequired = false;
				if($row['itemIsFullPax'] > 0){
					$isFullPaxRequired = true;
				}
				
				
				$dataCollections2 = $db->item_guest->find($whereData2);

				try {
					$dataArray2= iterator_to_array($dataCollections2);
					
				} catch (Exception $e) {
				  	$dataArray2 = array();
				}
				//print_r($dataArray2);
				$adult = 0;
				$child = 0;
				$infant = 0;
				$hasLead = false;
				$paxarr = array();
				foreach($dataArray2 as $key2 => $row2){
					if((int)$row2['guest_age'] >= 16){
						$adult += 1;
						if($row2['guest_is_lead'] > 0){
							$hasLead =true;

						}
					}
					if((int)$row2['guest_age'] < 16 && (int)$row2['guest_age'] >= 4 ){
						$child += 1;
					}
					if((int)$row2['guest_age'] <=3){
						$infant += 1;
					}
					$paxSTR =  $row['guest_title']."|".$row['guest_first_name']."|".$row['guest_last_name']."|".$row['guest_age'];
					array_push($paxarr,$paxSTR);

				}

				
				if($isLeadRequired != $hasLead){
					$isGoodItem = false;
					$message = "Item (".$row['itemId'].") needs a lead guest.";
					array_push($messageContainer,$message);

				}
				if($isFullPaxRequired){

					if((int)$row['itemAdultMax'] > $adult && (int)$row['itemAdultMin'] <= $adult){
						$isGoodItem = false;
						$needAdult = (int)$row['itemAdultMax'] - $adult;
						$message = "Item (".$row['itemId'].") needs ".$needAdult." more  adult guest.";
						array_push($messageContainer,$message);
					}
					if((int)$row['itemChildMax']  >  $child && (int)$row['itemChildMax'] <= $child){
						$isGoodItem = false;
						$needChild = (int)$row['itemChildMax'] - $child;
						$message = "Item (".$row['itemId'].") needs ".$needChild." more  child guest.";
						array_push($messageContainer,$message);
					}
					if((int)$row['itemInfantMax']  >  $infant && (int)$row['itemInfantMax'] <= $infant){
						$isGoodItem = false;
						$needInfant = (int)$row['itemInfantMax'] - $infant;
						$message = "Item (".$row['itemId'].") needs ".$needInfant." more  infant guest.";
						array_push($messageContainer,$message);
					}


				}

				if($isGoodItem){
					array_push($goodItems, (int) $row['itemId']);
				}else{
					//array_push($badItems,$messageContainer);
					$allGood = false;
				}
				
				
				
			}

			if($allGood){
				foreach($goodItems as $val){

					$db->items->update(
					    array("itemId" => $val),
					    array('$set' => array('itemIsPaxAllocated' => 1))
					);

				}

				$response = array ( "status" => true);
				print json_encode($response);
	  	  		$connection->close();
	  	   		exit;
			}else{

				$response = array ( "status" => false, "badItems"=>$messageContainer);
				print json_encode($response);
	  	  		$connection->close();
	  	   		exit;

			}

			

		}
		
		/* ENDLOGIN*/
	}
?>